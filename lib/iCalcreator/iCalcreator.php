<?php

// Marcel: our file, avoid useless autoloading

define( 'ICALCREATOR_VERSION', 'iCalcreator 2.22.1' );

require_once 'iCalBase.class.php';
require_once 'calendarComponent.class.php';
require_once 'iCalcreator.php';
require_once 'iCaldateTime.class.php';
require_once 'iCal.tz.inc.php';
require_once 'iCalUtilityFunctions.class.php';
require_once 'iCal.vCard.inc.php';
require_once 'iCal.XML.inc.php';
require_once 'valarm.class.php';
require_once 'vcalendar.class.php';
require_once 'vevent.class.php';
require_once 'vfreebusy.class.php';
require_once 'vjournal.class.php';
require_once 'vtimezone.class.php';
require_once 'vtodo.class.php';

// Copied from old iCalcreator (disappeared in new version 2.22 ???)
function icalcreator_date2timestamp( $datetime, $wtz=null ) 
{
	if( !isset( $datetime['hour'] )) $datetime['hour'] = 0;
	if( !isset( $datetime['min'] ))  $datetime['min']  = 0;
	if( !isset( $datetime['sec'] ))  $datetime['sec']  = 0;
	if( empty( $wtz ) && ( !isset( $datetime['tz'] ) || empty(  $datetime['tz'] )))
		return mktime( $datetime['hour'], $datetime['min'], $datetime['sec'], $datetime['month'], $datetime['day'], $datetime['year'] );
	$output = $offset = 0;
	if( empty( $wtz )) {
		if( iCalUtilityFunctions::_isOffset( $datetime['tz'] )) {
			$offset = iCalUtilityFunctions::_tz2offset( $datetime['tz'] ) * -1;
			$wtz    = 'UTC';
		}
		else
			$wtz    = $datetime['tz'];
	}
	if(( 'Z' == $wtz ) || ( 'GMT' == strtoupper( $wtz )))
		$wtz      = 'UTC';
	try {
		$strdate  = sprintf( '%04d-%02d-%02d %02d:%02d:%02d', $datetime['year'], $datetime['month'], $datetime['day'], $datetime['hour'], $datetime['min'], $datetime['sec'] );
		$d        = new DateTime( $strdate, new DateTimeZone( $wtz ));
		if( 0    != $offset )  // adjust for offset
			$d->modify( $offset.' seconds' );
		$output   = $d->format( 'U' );
		unset( $d );
	}
	catch( Exception $e ) {
		$output = mktime( $datetime['hour'], $datetime['min'], $datetime['sec'], $datetime['month'], $datetime['day'], $datetime['year'] );
	}
	return $output;
}

//! Wrapper around buggy and complex iCalcreator.
//! FIXME: replace iCalcreator with better library. As of 12/2017, nothing convincing was found.
//! This assumes timezone is set in ini PHP options (ini_set('date.timezone'))
function ical_parse($src,$srcType='url',$start=false,$end=false,$id="ical_parse",&$error=null)
{
	switch($srcType)
	{
	case 'url':
		// We download it ourselves, instead of doing it in $vcalendar->parse(),  to check for dl errors.
		require_once 'dlib/download-tools.php';
		$iCalText=dlib_curl_download($src,false,['info'=>&$dlInfo,
												 'timeout'=>180]);
		if($iCalText===false)
		{
			$error=['label'=>'dl_ical','download' =>$dlInfo];
			return false;
		}
		break;
	case 'file':
		$iCalText=file_get_contents($src);
		if($iCalText===false){return false;}
		break;
	case 'text':
	default:
		$iCalText=$src;
	}

	$iCalText=trim($iCalText);
	if(!preg_match('@^BEGIN:VCALENDAR.*END:VCALENDAR$@s',$iCalText))
	{
		$error=['label'=>'not_ical'];
		return false;
	}

	$tz=ini_get('date.timezone');
	date_default_timezone_set($tz);// this was needed by old versions of iCalcreator. Is it still needed ?
	$vcalendar = new vcalendar([
								   'unique_id'=>$id,
								   'TZID'=>$tz,
							   ] );
	// @ avoids php notices ... iCalcreator has lots of bugs :-(
	$parseRes=@$vcalendar->parse($iCalText);
	if($parseRes===false)
	{
		$error=['label'=>'ical_parse'];
		return [];
	}

	if($start===false){$start=time();}
	if($end  ===false){$end=strtotime('next year',$start);}
	$events=$vcalendar->selectComponents(date('Y',$start), date('n',$start), date('j',$start),
										 date('Y',$end  ), date('n',$end  ), date('j',$end  ),
										 "vevent");
	// selectComponents returns false for a valid ics file that has no events in desired range :-(
	if($events===false)
	{
		return [];
	}

	$res=[];
	foreach($events as $year => $year_arr)
	{
		foreach($year_arr as $month => $month_arr)
		{
			foreach($month_arr as $day => $day_arr)
			{
				foreach($day_arr as $event)
				{
					// x-current-dtstart is used for reccurences.
					// dtstart always ok, except that it can't be used for reccurences.
					// FIXME reccurrences REALY need checking. Probably dont work. time problem
					if($event->getProperty('rrule')==false) // sometimes this returns '' sometimes false :-(
					{
						$ts=icalcreator_date2timestamp($event->getProperty('dtstart'));
					}
					else
					{
						$repetition=$event->getProperty('x-current-dtstart');
						$d=iCalUtilityFunctions::_strdate2date($repetition[1]);
						$ts=icalcreator_date2timestamp($d);
					}

					$description = $event->getProperty('description');
					$description=str_replace('\\n',"\n",$description);

					$res[]=
						[
							'debug'      =>date('r',$ts).' ::: '.date('H:i',$ts),
							'dtstart'    =>$ts,
							'summary'    =>$event->getProperty('summary'),
							'location'   =>$event->getProperty('location'),
							'description'=>$description,
							'url'        =>$event->getProperty('url'),
							'uid'        =>$event->getProperty('uid'),
						];
					// Normalize any text
					foreach($res as $k=>$val)
					{
						if(is_string($val)){$res[$k]=dlib_cleanup_plain_text($val,false,false);}
					}
				}
			}
		}
	}

	return $res;
}

?>