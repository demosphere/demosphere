<?php
/**
 * Returns a list of events selected according to $options and containing
 * information described in $options.
 *
 * This is the only function that should be used to retreive a list of events.
 * Do not use demosphere_event_list_sql() directly.
 * Unfortunately many selection criteria
 * are too complex to express in sql. For example selecting events near a point (lat,lng).
 *
 * @param $options: an array of options 
 * (see demosphere_event_list_options_defaults() for a complete documented list)
 */
function demosphere_event_list(array $options)
{
	global $demosphere_config;
	$options=demosphere_event_list_options_defaults($options);
	//var_dump($options);
	//fatal('lll');


	// *************** Process options

	// address field may be needed for tooltips
	if($options['tooltips']!==false){$options['place__address']=true;}

	// if user cal info (checkboxes) needs to be displayed then we need the uc_selected field
	if($options['addUserCalInfo']!==false)
	{
		$options['uc_selected']=true;
		$options['ucUid']=$options['addUserCalInfo'];
	}

	if($options['userCalendar']!==false && $options['userCalendarNoLoad']===false)
	{
		// userCalendar sets many options from values in the user's widget configuration.
		// So we need to load them from the widget's config.
		require_once 'demosphere-widget.php';
		require_once 'demosphere-event-list-form.php';
		$calUser=User::fetch(intval($options['userCalendar']));
		$widgetConfig=val($calUser->data,'demosphere_widget_config',[]);
		// If this user has never configured the widget, we need to set default values.
		foreach(demosphere_widget_options() as $opt)
		{
			if(!isset($widgetConfig[$opt['name']])){$widgetConfig[$opt['name']]=$opt['default'];}
		}
		// Now convert widget options (which are in "form" format) into arguments
		// suitable for demosphere_event_list.
		$calOptions=demosphere_widget_options_to_list_options($widgetConfig,$calUser->id);
		// We might want override certain options:
		// For example, somebody viewing a user's cal, might use frontpage options
		// to further refine...
		if($calOptions['nearLatLng'       ]===false){unset($calOptions['nearLatLng']);}
		if($calOptions['limit'            ]===false){unset($calOptions['limit']);}
		if($calOptions['selectTopic'      ]===false){unset($calOptions['selectTopic']);}
		if($calOptions['selectVocabTerm'  ]===false){unset($calOptions['selectVocabTerm']);}
		if($calOptions['selectVocabTermOr']===false){unset($calOptions['selectVocabTermOr']);}
		if($calOptions['selectCityId'     ]===false){unset($calOptions['selectCityId']);}
		if($calOptions['mapShape'         ]===false){unset($calOptions['mapShape']);}
		if($calOptions['mostVisited'      ]===false){unset($calOptions['mostVisited']);}
		if($calOptions['maxNbEventsPerDay']===false){unset($calOptions['maxNbEventsPerDay']);}

		$options=array_merge($options,$calOptions);
	}

	if($options['url']){$options['id']=true;}

	// ************ build options for sql list fetch

	$isPureSql=true;// whether all criteria are expressed in sql or not

	// ***** Add all fields for the following classes: 'Event','Place','City'
	$allSqlFields=demosphere_event_list_class_fields();
	$allSqlFields[]='uc_selected';
	$allSqlFields[]='topics';
	
	$fields=[];
	foreach($allSqlFields as $f){if(isset($options[$f]) && $options[$f]){$fields[$f]=true;}}

	$fieldsSql=$fields;
	$optionsSql=$options;
	
	// user cal (checkboxes:selected/rejected) can override other rules
	// Example: an event with a topic that is not normallly selected, 
	// can be selected (checked) in user cal). 
	if($options['userCalendar']!==false)
	{
		$isPureSql=false;
	}

	// if this is a user cal, then we need selected/rejected info
	if($options['userCalendar']!==false)
	{
		$fieldsSql['uc_selected']=true;
	}

	if($options['maxNbEventsPerDay']!==false)
	{
		$isPureSql=false;
		$fieldsSql['visits']=true;
	}
	if($options['mostVisited']!==false)
	{
		$isPureSql=false;
		$fieldsSql['visits']=true;
	}

	// Need to know if an event is published or not when displayed to contributors (opinions).
	if($options['addForContributor']!==false)
	{
		$fieldsSql['status']=true;
		$fieldsSql['moderationStatus']=true;
	}

	// we don't express distance to a point in sql, so this criterion is not sql
	if(val($options,'nearLatLng'    )!==false || 
	   val($options,'mapShape'      )!==false || 
	   val($options,'selectFpRegion')!==false   )
	{
		$isPureSql=false;
		if(!isset($fieldsSql['place__latitude' ])){$fieldsSql['place__latitude' ]=true;}
		if(!isset($fieldsSql['place__longitude'])){$fieldsSql['place__longitude']=true;}
		if(!isset($fieldsSql['place__zoom'     ])){$fieldsSql['place__zoom'     ]=true;}
	}		
	if(val($options,'mapShape'  )!==false)
	{
		require_once 'demosphere-event-map.php';
		$mapShape=demosphere_mapshape_get($options['mapShape']);
		if($mapShape===false){dlib_bad_request_400('unknown mapShape:'.$mapShape);}
	}
	
	// If some criteria are not sql, then sql offset and limit have no meaning in sql query.
	// We have to simulate offset/limit by hand, after query.
	if(!$isPureSql)
	{
		$optionsSql['offset']=false;
		$optionsSql['limit' ]=false;
	}
	
	// needed for applying selectShowOnFrontpage in nonsql
	if($options['selectShowOnFrontpage']!==false)
	{
		$fieldsSql['showOnFrontpage']=true;
	}

	// display needs this to identify private events
	if($options['addPrivateEvents']!==false)
	{
		$fieldsSql['showOnFrontpage']=true;
	}

	// ************ 
	// ************ now build sql query, execute and fetch the list of events
	// ************ 

	$fieldsSql['startTime']=true;
	// build the actual sql query
	$query=demosphere_event_list_sql(array_keys($fieldsSql),$optionsSql);
	// execute query
	$query_result=db_query($query);
	$customOffsetAndLimit=false;
	if($options['mostVisited'      ]!==false){$customOffsetAndLimit=true;}
	if($options['maxNbEventsPerDay']!==false){$customOffsetAndLimit=true;}
	$offset=!$isPureSql && isset($options['offset']) ? $options['offset'] : false;
	$limit =!$isPureSql && isset($options['limit' ]) ? $options['limit' ] : false;
	$res=[];
	while(($row=mysqli_fetch_assoc($query_result))!== null)
	{
		// Create a "fake" Event, Place and City and fill fields that were actually fetched
		list($event,$place,$city)=Event::sqlCreatePartial($row);
		$event->setPlace($place);
		$place->setCity($city);
		$event->render=[];
		if(isset($fieldsSql['uc_selected']) && $fieldsSql['uc_selected']) {$event->render['uc_selected']=$row['uc_selected'];}
		if(isset($row['topics'])                                        ) {$event->render['topics'     ]=$row['topics'];}

		// true: keep this event even if soft rules say otherwise
		$forceKeep=false;

		// *** user calendar selected rule forces keeps or rejects
		if($options['userCalendar']!==false)
		{
			// $event->render['uc_selected'] can be '0','1',null
			if($event->render['uc_selected']==='0' ){continue;}       // explicitly rejected event
			else
			if($event->render['uc_selected']==='1' ){$forceKeep=true;}// explicitly selected event
			else
			if($options['userCalPolicy']===1){continue;}    // undefined, with strict policy=>reject
		}

		if(!$forceKeep)
		{
			// *** selectShowOnFrontpage rule
			if($options['selectShowOnFrontpage']!==false  && $optionsSql['selectShowOnFrontpage']===false)
			{
				if($event->showOnFrontpage!=$options['selectShowOnFrontpage']){continue;}
			}

			// *** distance to a point rule
			if($options['nearLatLng']!==false)
			{
				$d=$event->usePlace()->distance($options['nearLatLng']['lat'],
										   $options['nearLatLng']['lng']);
				if($d==false || $d>$options['nearLatLng']['distance']){continue;}
			}
			// *** map shape rule
			if($options['mapShape']!==false)
			{
				if(!isset($event->usePlace()->zoom) || $event->usePlace()->zoom==0){continue;}
				if(!demosphere_mapshape_point_is_inside($mapShape,
														$event->usePlace()->latitude,
														$event->usePlace()->longitude))
				{continue;}
			}
			// *** selectFpRegion rule
			if($options['selectFpRegion']!==false)
			{
				$region=$demosphere_config['frontpage_regions'][$options['selectFpRegion']];
				if(isset($region['lat']) && $region['lat']!==false)
				{
					$d=$event->usePlace()->distance($region['lat'],$region['lng']);
					if($d==false || $d>$region['distance']){continue;}
				}
				else
				if(isset($region['cityId']) && $region['cityId']!==false)
				{
					if(intval($region['cityId'])!==intval($event->usePlace()->cityId)){continue;}
				}
				else{fatal("config problem: bad city, please contact admin");}
			}
		}

		// *** offset and limit rules
		if(!$customOffsetAndLimit)
		{
			if($offset!==false && ($offset--)> 0){continue;}
			if($limit !==false && ($limit-- )<=0){break;}
		}

		$res[]=$event;
	}

	// ************ 
	// ************ We now have a list of events. Apply other sorting and selection criteria.
	// ************ 

	// maxNbEventsPerDay rule: cannot simply use dayrank, 
	// because of interactions with other selection criteria, this 
	if($options['maxNbEventsPerDay']!==false)
	{
		// create a list of events for each day
		$lastDay=false;
		$days=[];
		$nbDays=0;
		foreach($res as $k=>$event)
		{
			$day=date('z',$event->startTime);
			if($day!==$lastDay){$days[$nbDays++]=[];}
			$days[$nbDays-1][]=$k;
			$lastDay=$day;
		}
		// sort all events in each day to determine their dayrank
		foreach($days as $dayEventKeys)
		{
			$sort=[];
			foreach($dayEventKeys as $k)
			{
				$sort[$k]=(int)$res[$k]->visits;
				// private events are always shown
				//FIXME if(!oval($res[$k],'showOnFrontpage',true)	   ){$sort[$k]=100000000;}
			}
			arsort($sort);
			foreach(array_keys($sort) as $rank=>$k)
			{
				if($rank>=$options['maxNbEventsPerDay']){unset($res[$k]);}
			}
		}
	}

	// To implement the mostVisited option we need to sort all selected events and 
	// keep only the number of events given by 'mostVisited'.
	if($options['mostVisited']!==false)
	{
		// build list of keys of the most visited events
		$sort=[];
		foreach($res as $k=>$event)
		{
			$sort[$k]=(int)$event->visits;
			// extra or private events are always shown
			//if(val($event->render,'is_extra'     ) || 
			//   !oval($event,'showOnFrontpage',true)	   ){$sort[$k]=100000000;}
		}
		arsort($sort);
		$sort=array_slice($sort,0,$options['mostVisited'],true);
		// only keep events with those keys
		$res1=[];
		foreach($res as $k=>$event)
		{
			if(isset($sort[$k])){$res1[]=$event;}
		}
		$res=$res1;
	}

	// Limit and offset rules must be applied last, after any other selection
	// criteria.
	if($customOffsetAndLimit)
	{
		$res1=[];
		foreach($res as $event)
		{
			if($offset!==false && ($offset--)> 0){continue;}
			if($limit !==false && ($limit-- )<=0){break;}
			$res1[]=$event;
		}
		$res=$res1;
	}

	// ************ 
	// ************ The list of events is now complete. Postprocess certain fields.
	// ************ 

	// Note: this is overridden in demosphere_calendar_render
	if($options['url'])
	{
		global $base_url;
		$base_path=preg_replace('@^https?://[^/]*@','',$base_url).'/';
		foreach(array_keys($res) as $k)
		{
			$res[$k]->render['url']=$options['fullUrls'] ? 
				$base_url.'/'.$demosphere_config['event_url_name'].'/'.intval($res[$k]->id) :
				$base_path.$demosphere_config['event_url_name'].'/'.intval($res[$k]->id);
		}
	}

	return $res;
}


/**
 * Generate sql  statement  for retrieveing  a list  of events. 
 *
 * Caller  can choose which  fields he needs  and several  other options.
 * WARNING: This function should not be used directly.
 * To get a list of events use demosphere_event_list
 */
function demosphere_event_list_sql(array $fields,array $options)
{
	global $demosphere_config,$dlib_config;
	$defaults=['selectStartTime'=>false,
			   'endTime'=>false,
			   'selectTopic'=>false,
			   'selectVocabTerm'=>false,
			   'selectVocabTermOr'=>false,
			   'selectStatus'=>true,
			   'selectShowOnFrontpage'=>true,
			   'selectFeatured'=>false,
			   'selectModerationStatus'=>false,
			   'selectNeedsAttention'=>false,
			   'selectRepetitionGroupId'=>false,
			   'selectAuthorId'=>false,
			   'selectPlaceId'=>false,
			   'selectCityId'=>false,
			   'selectPlaceReferenceId'=>false,
			   'selectRecentChanges'=>false,
			   'orderByLastBigChanges'=>false,
			   'addPrivateEvents'=>false,
			   'addForContributor'=>false,
			   'order'=>true,
			   'limit'=>false,
			   'offset'=>false,
			   'ucUid'=>false,
			   'userCalPolicy'=>0,
			   'userCalendar'=>false,
			   'addPrivateEvents'=>false,
			   'extraCondition'=>false,
			  ];
	$options=array_merge($defaults,$options);
	extract($options);
	
	// $userCalendar implies ucUid, and cannot be different
	if($userCalendar!==false){$ucUid=$userCalendar;}
	
	$f=$fields;
	$f=array_flip($f);
	
	$needsPlaceTable=count(array_intersect(array_flip($f),Place::fields('all',['id','title','description'])))!==0 || 
		count(preg_grep('@^place__@',array_keys($f)));
	$needsCityTable=count(preg_grep('@^place__city__@',array_keys($f)))>0;
	if(isset($options['selectPlaceId'		  ])){$needsPlaceTable=true;}
	if(isset($options['selectCityId'	      ])){$needsPlaceTable=true;}
	if(isset($options['selectPlaceReferenceId'])){$needsPlaceTable=true;}

	
	// ************ SELECT ************
	$query="SELECT\n".
		// ** Event fields
		(isset($f['id'])                ? " Event.id        AS id,\n" :'').
		(isset($f['authorId'])          ? " Event.authorId AS authorId,\n" :'').
		(isset($f['created'])           ? " Event.created   AS created,\n" :'').
		(isset($f['changed'])           ? " Event.changed   AS changed,\n" :'').
		(isset($f['status'])            ? " Event.status    AS status,\n" :'').
		(isset($f['lastBigChange'])     ? " Event.lastBigChange    AS lastBigChange,\n" :'').
		(isset($f['showOnFrontpage'])   ? " Event.showOnFrontpage  AS showOnFrontpage,\n" :'').
		(isset($f['title'])             ? " Event.title AS title,\n" :'').
		(isset($f['body'])              ? " Event.body  AS body,\n" :'').
		(isset($f['moderationStatus'])  ? " Event.moderationStatus  AS moderationStatus,\n" :'').
		(isset($f['moderationMessage']) ? " Event.moderationMessage AS moderationMessage,\n" :'').
		(isset($f['needsAttention'])	? " Event.needsAttention	   AS needsAttention,\n" :'').
		(isset($f['htmlTitle'])		    ? " Event.htmlTitle AS htmlTitle,\n" :'').
		(isset($f['startTime'])		    ? " Event.startTime AS startTime,\n"	  :'').
		(isset($f['repetitionGroupId']) ? " Event.repetitionGroupId AS repetitionGroupId,\n" :'').
		(isset($f['externalSourceId'])  ? " Event.externalSourceId AS externalSourceId,\n" :'').
		(isset($f['extraData'])		    ? " Event.extraData AS extraData,\n" :'').
		(isset($f['dayrank'])			? " Event.dayrank AS dayrank,\n" :'').
		(isset($f['visits'])			? " Event.visits AS visits,\n" :'').
		// ** Place fields
		(isset($f['place__id'])         ? " place.id AS place__id,\n" :'').
		(isset($f['place__cityId'])     ? " place.cityId AS place__cityId,\n" :'').
		(isset($f['place__address'])	? " place.address AS place__address,\n" :'').
		(isset($f['place__latitude'])	? " place.latitude AS place__latitude,\n" :'').
		(isset($f['place__longitude'])	? " place.longitude AS place__longitude,\n" :'').
		(isset($f['place__zoom'])		? " place.zoom AS place__zoom,\n" :'').
		// ** City fields
		(isset($f['place__city__id'])		 ? " place__city.id AS place__city__id,\n" :'').
		(isset($f['place__city__name'])		 ? " place__city.name AS place__city__name,\n" :'').
		(isset($f['place__city__shortName']) ? " place__city.shortName AS place__city__shortName,\n" :'').
		// ** Other fields
		(isset($f['uc_selected'])       ? " user_calendar_selection.selected AS uc_selected,\n" :'').
		(isset($f['topics'])            ? " (SELECT GROUP_CONCAT(Event_Topic.topic) FROM Event_Topic WHERE Event_Topic.event=Event.id) as topics,\n" :'').
		'';
	$query=preg_replace("@,\n$@s","\n",$query);

	// ************ FROM ************
	$query.="FROM\n";

	// **** Event table
	$query.=" Event\n";
	// ** joins on Event table 
	if($needsPlaceTable){$query.="   INNER JOIN Place AS place ON Event.placeId=place.id\n";}
	if($needsCityTable ){$query.="   INNER JOIN City  AS place__city ON place.cityId=place__city.id\n";}
	if($selectFeatured ){$query.="   INNER JOIN variables AS featured ON featured.type='fp-featured' AND Event.id=featured.name\n";}

	if(isset($f['uc_selected']) || $userCalendar!==false)
	{
		$query=preg_replace("@\s*,\s*$@s"," ",$query);
		if($ucUid===false){dlib_bad_request_400("requested uc_selected but no ucUid");}
		$query.="   LEFT JOIN user_calendar_selection\n".
			    "        ON Event.id=user_calendar_selection.event\n".
			    "        AND user_calendar_selection.user=".intval($ucUid).",\n";
	}

	// ********* other FROM tables
	$query=preg_replace("@\s*,\s*$@s","\n",$query);

	// ************ WHERE ************
	$query.="WHERE\n";

	if($selectStatus!==false)
	{
		$query.=" (Event.status = ". intval($selectStatus );
		// Add unpublished publication requests for contributors (opinions).
		if($demosphere_config['enable_opinions'] && $addForContributor!==false)
		{
			// As defined in Event::access()
			$query.=" OR (Event.moderationStatus = 1   \n".// waiting 
				    "     AND needsAttention IN (50,130) \n".// publication-request
				    "    ) \n".
				    " OR (Event.moderationStatus IN (5,6) \n".// rejected
				    "     AND needsAttention IN (50,130)    \n".// publication-request
				    "     AND EXISTS (SELECT * FROM Opinion WHERE eventId=Event.id)\n".// publication-request
				    "    )\n";
		}
		$query.=") AND\n";
	}

	// ** time **
	if($selectStartTime!==false && $selectStartTime!==0)
	{$query.=" (Event.startTime >=".intval($selectStartTime).") AND\n";}
	if($endTime!==false)
	{$query.=" (Event.startTime <=".intval($endTime  ).") AND\n";}

	// ** selectRecentChanges
	if($selectRecentChanges!==false)
	{$query.=" (Event.lastBigChange >=".(time()-intval($selectRecentChanges)).") AND\n";}

	// ** userCalendar : selected overrides all of the folowing options
	if($userCalendar!==false)
	{
		$query.="((\n";
	}

	if($selectShowOnFrontpage!==false)
	{
		if($addPrivateEvents==false || $selectShowOnFrontpage===0)
		{$query.=" (Event.showOnFrontpage = ".intval($selectShowOnFrontpage).") AND\n";}
		else
		{
			$query.=
				" (Event.showOnFrontpage = ".intval($selectShowOnFrontpage)." OR".
				"  Event.authorId=".intval($addPrivateEvents).") AND\n";
		}
	}
	if($selectModerationStatus!==false)
	{
		$query.=" (Event.moderationStatus IN (".
			implode(',',array_map('intval',$selectModerationStatus)).")) AND\n";
	}
	if($selectNeedsAttention!==false)
	{
		if(is_array($selectNeedsAttention))
		{
			$query.=" (Event.needsAttention IN (".
				implode(',',array_map('intval',$selectNeedsAttention)).")) AND\n";
		}
		else
		{
			$query.=" (Event.needsAttention".
				($selectNeedsAttention>0 ? 
				 '>='.intval($selectNeedsAttention):
				 '<='.(-intval($selectNeedsAttention))." AND Event.needsAttention!=0").
				") AND\n";
		}
	}
	// FIXME: should handle case where repetitionGroupId=0
	if($selectRepetitionGroupId!==false)
	{
		$query.=" Event.repetitionGroupId=".intval($selectRepetitionGroupId)." AND\n";
	}
	if($selectPlaceId!==false)
	{
		$query.=" place.id=".intval($selectPlaceId)." AND\n";
	}
	if($selectCityId!==false)
	{
		$query.=" place.cityId=".intval($selectCityId)." AND\n";
	}
	if($selectPlaceReferenceId!==false)
	{
		$query.=" place.referenceId=".intval($selectPlaceReferenceId)." AND\n";
	}

	if($selectAuthorId!==false)
	{
		$query.=" Event.authorId=".intval($selectAuthorId)." AND\n";
	}

	if($selectTopic!==false)
	{
		if($selectTopic!=='others')
		{
			$query.=
				" EXISTS (SELECT * FROM Event_Topic WHERE\n".
				"             Event_Topic.event=Event.id AND\n".
				"             Event_Topic.topic=".intval($selectTopic).") AND\n";
		}
		else
		{
			$query.=
				" NOT EXISTS (SELECT * FROM Event_Topic WHERE\n".
				"             Event_Topic.event=Event.id) AND\n";
		}
	}

	if($selectVocabTerm!==false)
	{
		$query.=
			" EXISTS (SELECT * FROM Event_Term WHERE\n".
			"             Event_Term.event=Event.id AND\n".
			"             Event_Term.term=".intval($selectVocabTerm).") AND\n";
	}

	if($selectVocabTermOr!==false)
	{
		$query.=
			" EXISTS (SELECT * FROM Event_Term WHERE\n".
			"             Event_Term.event=Event.id AND\n".
			"             Event_Term.term IN (".
			implode(',',array_map('intval',$selectVocabTermOr))."))  AND\n";
	}

	// extraCondition
	if($extraCondition!==false){$query.=' '.$extraCondition." AND\n";}

	// ** userCalendar : selected overrides all of the previous options
	if($userCalendar!==false)
	{
		$query=preg_replace("@( AND|\nWHERE)\s*$@s","\n",$query);
		$query.=") OR user_calendar_selection.selected=1 )\n AND\n";
	}

	$query=preg_replace("@( AND|\nWHERE)\s*$@s","\n",$query);

	// ************ ORDER BY ************
	if($orderByLastBigChanges){$query.="ORDER BY lastBigChange DESC\n";}
	else
	if($order                ){$query.="ORDER BY startTime ASC\n";}
	if($limit!==false)
	{
		$query.="LIMIT ".($order!==false ? intval($offset)."," : '').
			intval($limit)."\n";
	}
	// echo "<pre>\n$query\n</pre>\n";//fatal("lkmlk");
	// if(val($dlib_config,'debug')){echo "<pre>\n$query\n</pre>\n";fatal("lkmlk");}
	return $query;
}

/**
 * Adds default values for all undefined event list options.
 *
 * Event-list options are mainly used to select events in demosphere_event_list(), 
 * and to render calendars in template_preprocess_demosphere_calendar().
 * 
 * This function defines all existing event list option and documents them.
 */
function demosphere_event_list_options_defaults(array $options)
{
	global $base_url,$demosphere_config;
	
	if(isset($options['defaultsAreIncluded'])){return $options;}

	// default start time for calendar : this morning at 0 am
	// or yesterday if it's earlier than 4am
	$defaultTime=Event::today();

	// To make life easier, user can define true options like this $opts=array(...,'example',...)
	// instead of $opts=array(...,'example'=>true,...)
	foreach(array_keys($options) as $k)
	{
		if(is_int($k))
		{
			$options[$options[$k]]=true;
			unset($options[$k]);
		}
	}

	// The full list of options and default values accepted for the following functions:
	// 1) demosphere_event_list_sql
	// 2) demosphere_event_list
	// 4) template_preprocess_demosphere_calendar
	//
	// If nothing is mentioned an option is supported by all fcts (1,2,3,4)
	// If a number is mentioned "Ex: [3]" , the option is supported by that fct and above 
	// "Ex: 3,4"

	static $defaults0=false;
	if($defaults0===false)
	{
		$defaults0=
			[
				// ******* meta options  ************
				// bool
				// which defaults to use : Only include fields specifcally requested.
				// By default several fields (startTime, title, ... see below)
				// are included. With this this option only fields specifically requested will
				// be included.
				'default_noOtherFields'=>false,

				// a list of option names that should be ignored
				// This is only used to avoid an 'unknown option' error 
				// in demosphere_event_list_options_defaults
				'ignoreOptions'=>[],

				// ********* Which events should be included ***********

				// (X) do not use
				'defaultTime'        =>$defaultTime, 

				// A timestamp saying when the calendar display will start. 
				// default(false) will start calendar today, at the begining of the day
				'selectStartTime'          =>$defaultTime, 

				// A timestamp saying when the calendar display will end.
				// default(false) will show all future events 
				'endTime'            =>false,

				// Published status (publicly visible or not) (Event field)
				// false: include events regardless of their "status" value
				// 0: only include events that are not published
				// 1: only include events that are published
				'selectStatus'=>1,

				// Promoted to front page (Event field)
				// false: include events regardless of their "showOnFrontpage" value
				// 0: only include events that are not shown on frontpage
				// 1: only include events that are shown on frontpage
				'selectShowOnFrontpage'=>1,

				// Featured, on top of frontpage
				// false: include events if they are "featured" or not
				// true: only include events that are featured
				'selectFeatured'=>false,

				// Select events based on moderation status (Event::moderationStatus)
				// false: moderation status is ignored
				// array of ints: select events that have one of these moderation statuses.
				'selectModerationStatus'=>false,

				// Select events based on needsAttention (see: Event::needsAttention)
				// false: needs attention is ignored
				// array of ints: select events that have one of these needs attention values.
				// simple int: positive: select above this val (included)
				// simple int: negative: select below this val (included) and !=0 
				'selectNeedsAttention'=>false,

				// Select events in a repetition group
				'selectRepetitionGroupId'=>false,

				// Select events authored by this user (authorId)
				'selectAuthorId'=>false,

				// (int) A topic id. Only events having this topic will be selected.
				'selectTopic'        =>false,

				// (int) A term id. Only events having this term will be selected.
				'selectVocabTerm'        =>false,

				// (int array) A list of term ids. Only events having at least one of these terms will be selected.
				'selectVocabTermOr'      =>false,

				// (int) Limit the number of events displayed in each day. 
				// The most visited events are chosen.
				'maxNbEventsPerDay'        =>false,

				// (int) Only keep this number of most visited events.
				'mostVisited'        =>false,

				// A uid (user id)
				// Show events in this user's personal calendar.
				// Many options (nearLatLng,userCalPolicy...) will be 
				// loaded from the specified user's widget config.
				// addPrivateEvents will also be set.
				// So you only need to set userCalendar to get a standard user cal.
				'userCalendar'      =>false,

				// true / false
				// Normally, demosphere_event_list will load options from widget config
				// when userCalendar is specified. However, (internal to widget config interface)
				// when widget is not saved yet, this is not desireable.
				'userCalendarNoLoad'      =>false,

				// false or 0 : "include", only events rejected by user will not be fetched.
				// 1 : "strict",  only events selected by user will be fetched.
				// This will be automatically loaded from user's widget config
				// if userCalendar is specified.
				'userCalPolicy'      =>false,

				// A uid (user id)
				// This adds "private" events (events created by this user that
				// are not shown on frontpage) to frontpage.
				// Setting userCalendar will override this value.
				'addPrivateEvents'    =>false,

				// true / false
				// Contributors can browse unpublished events to give an Opinion
				'addForContributor'    =>false,

				// Use this to select external events from a specific source.
				'externalEventsSourceId' =>false,

				// Only show events that are in this region.
				// selectFpRegion is an index (id) of an array : 
				// see frontpage_regions in demosphere-config-form.php
				'selectFpRegion'               =>false,

				// Only show events that are near this point.
				// array('lat'=>...,'lng'=>...,'distance'=>...)
				'nearLatLng'         =>false,

				// Only show events that are inside this mapShape
				// string: name that identifies this mapShape
				'mapShape'           =>false,

				// Only show events with this place 
				'selectPlaceId'=>false,

				// Only show events with this city id
				'selectCityId'=>false,

				// Only show events with this reference place 
				'selectPlaceReferenceId'=>false,

				// (int or false) Select events with lastBigChange < now()-value
				// In other words: select events that have been changed less than selectRecentChanges secs. ago.
				'selectRecentChanges'=>false,

				// Order by lastBigChange instead of startTime
				'orderByLastBigChanges'=>false,

				// (int or false) Only return the events after "offset" number of events.
				// This can, for example, be used with "limit" to implement a pager.
				'offset'    =>false,

				// (int or false) maximum number of events that will be fetched.
				// This can, for example, be used with "offset" to implement a pager.
				'limit'     =>false,

				// Extra WHERE clause for SQL query
				// This is hackish, don't overuse.
				'extraCondition'     =>false,

				// ********* what information / fields to include in each event ***********
				// Options marked [D] will also affect display (rendering)

				// A uid (user id):Add information on whether events are selected by this user.
				// Note that if userCalendar is set (not false), it will override this value
				// Note that if addUserCalInfo is set (not false), it will override this value
				'ucUid'      =>false,

				// [D] Whether to include a list of event's topics in each event.
				// This will also add the topics to the class of each event row.
				'topics'          =>true,

				// DB information not in Event,Place or City
				'uc_selected'=>false,
				// fake fields (not in database, but constructed)
				// Note: this is not necessary for events rendered with demosphere_calendar_render
				'url'=>false,

				// ********* display oriented args ***********
				// The following options are for template_preprocess_demosphere_calendar
				// and demosphere-calendar.tpl.php
				// Options marked [A] will also add fields they need to result

				// An array of get parameters that should be added to urls to events.
				'extraGetArgs'       =>[],

				// A topic id. Events that do not have this topic will be greyed-out.
				// (sel_t or xs classes will be added to the row <tr>)
				'highlightTopic'      =>false,

				// [A] (int uid or false) Adds classes to each row <tr> that allows 
				// javascript to add checkboxes
				// in user calendar edit mode.
				'addUserCalInfo'     =>false,

				// Use full urls instead of  relative/or absolute paths.
				// This is neded for calendars not displayed on this site.
				// (widgets and email subscriptions).
				'fullUrls'           =>false,

				// [A] A text that will be displayed when user hovers
				// over links in calendar.
				'tooltips'           =>$demosphere_config['front_page_tooltips'],

				// When an event-admin user is logged in special links
				// are added to calendar for faster editing.
				'showAdminLinks'     =>false,
			];
		//var_dump(count($defaults0),$defaults0);

		// ***** Add all fields for Event, Place & City classes
		$defaultSelectedFields=array_flip(['id','htmlTitle','title','startTime','place__id','place__city__id','place__city__name','place__city__shortName','dayrank','visits',]);
		$fields=demosphere_event_list_class_fields();
		foreach($fields as $field)
		{
			$defaults0[$field]=isset($defaultSelectedFields[$field]);
		}
	}
	$defaults=$defaults0;
	// Show an error if there is an unknown option (that is not ignored)
	$ignoreOptions=val($options,'ignoreOptions',[]);
	foreach($options as $k=>$v)
	{
		if(!isset($defaults[$k]) && 
		   array_search($k, $ignoreOptions)===false)
		{
			fatal('demosphere_event_list: unknown option:'.$k);
		}
	}

	//! default_noOtherFields: remove all the default fields 
	if(val($options,'default_noOtherFields'))
	{
		$fields=demosphere_event_list_class_fields();
		$fields[]='ucUid';
		$fields[]='topics';
		$fields[]='uc_selected';
		foreach($fields as $f){$defaults[$f]=false;}
	}

	$res=[];
	foreach($defaults as $k=>$v) {$res[$k]=val($options,$k,$v);}
	foreach($ignoreOptions as $k){if(isset($options[$k])){$res[$k]=$options[$k];}}

	if($res['selectStartTime']===false){$res['selectStartTime']=$defaultTime;}

	// avoid re-parsing defaults
	$res['defaultsAreIncluded']=true;

	return $res;
}

//! List of all field options from Event, Place & City classes.
function demosphere_event_list_class_fields()
{
	static $fields=false;
	if($fields!==false){return $fields;}
	$fields=[];
	foreach(['Event','Place','City'] as $class)
	{
		$schema=$class::getSchema();
		foreach($schema as $name=>$val)
		{
			$field='';
			if($class=='Place'){$field.='place__';}
			if($class=='City' ){$field.='place__city__';}
			$field.=$name;
			$fields[]=$field;
		}
	}
	return $fields;
}

/**
 * Parse a list of text options that come from the outside (for example: all GET or POST args).
 * 
 * This function should be used by all public interfaces (frontpage, json, widget-html, feed...).
 * Security aspects are handled here during parsing.
 * This function mainly manages the lists of options, the actual parsing is 
 * done by calling demosphere_event_list_parse_getpost_option() for each option.
 * This function will evolve as its usage becomes clearer.
 * @param $rawOptions: for example $_GET or $_POST
 * @param $whichOptions: defines which options we want to parse (see code). Note: this will evolve.
 *               when the use of this fct becomes clearer.
 * @param $dieOnError: true/false: whether to exit if we encounter invalid options.
 * 
 * @return an array containing:
 * - 'options': the resulting parsed options. These can be used, for example, in demosphere_event_list
 * - 'notParsed': unknown options that are not handled by this parser.
 * - 'invalid': known options, with invalid values (or not allowed)
 *
 */
function demosphere_event_list_parse_getpost_options(array $rawOptions,array  $whichOptions=[],bool $dieOnError=true)
{
	global $demosphere_config;
	$defaultWhichOptions=
		[
			'noFields'=>true,
			'onlyEventChoose'=>true,
			// options that normally would cause an error, but we want an exception
			//(note: this is a very unusual case)
			'doNotParse'=>[],
		];
	$whichOptions=array_merge($defaultWhichOptions,$whichOptions);
	if($whichOptions['onlyEventChoose']){$whichOptions['noFields']=true;}

	$allOptions=demosphere_event_list_options_defaults([]);

	// List of options that describe what information is included in an event.
	// (only fields that are safe to show publicly should be included in this list)
	$fieldsOk=
		['id','startTime','status','showOnFrontpage','title','htmlTitle','body','changed','created','dayrank','visits','moderationStatus',
		 'place__cityId','place__address','place__zoom','place__latitude','place__longitude',
		 'place__city__name','place__city__shortName',
		 'topics','url'];

	// List of options used to choose (select) events:
	$eventChooseOptions=['selectStartTime','endTime','selectModerationStatus','selectShowOnFrontpage','selectFeatured','selectTopic','selectVocabTerm','selectVocabTermOr','maxNbEventsPerDay','mostVisited','userCalendar','city','nearLatLng','mapShape','selectPlaceReferenceId','selectCityId','addPrivateEvents','selectFpRegion','offset','limit','orderByLastBigChanges','selectRecentChanges','selectRepetitionGroupId',];

	$res=[];
	$notParsed=[];
	$invalid=[];
	
	foreach($rawOptions as $name=>$val)
	{
		// Completely ignore these
		if($name==='q' || $name==='random'){continue;}
		// doNotParse (special cases)
		if(array_search($name,$whichOptions['doNotParse'])!==false)
		{
			$notParsed[$name]=$val;
			continue;
		}

		// Avoid errors when GET args contain array (hacking attempt)
		if(!is_string($val))
		{
			$invalid[$name]='non string value for option';
			continue;
		}

		// field options
		if(array_search($name,$fieldsOk)!==false)
		{
			if($whichOptions['noFields'])
			{
				$invalid[$name]='field option not allowed in this context';
			}
			else{$res[$name]=true;}
			continue;
		}

		// Event choose options. This option is invalid if:
		// 1) and it is not a custom option (if it is custom, it is added to notParsed)
		// 2) and it is not an event choose option
		// 3) and we only want event chooseoptions, 
		if(isset($allOptions[$name]) && 
		   array_search($name,$eventChooseOptions)===false &&
		   $whichOptions['onlyEventChoose'])
		{
			$invalid[$name]='option not allowed in this context';
			continue;
		}

		// actually parse the option
		list($status,$rvalue)=demosphere_event_list_parse_getpost_option($name,$val);
		switch($status)
		{
		case 'error':  $invalid  [$name]=$rvalue;break;
		case 'unknown':$notParsed[$name]=$val   ;break;
		case 'ok':     $res      [$name]=$rvalue;break;
		}
	}

	// Die with an error message for invalid options
	if(count($invalid) && $dieOnError)
	{
		echo "Invalid options:<br/>\n";
		foreach($invalid as $name=>$message)
		{
			echo "option:".ent($name).": error:".ent($message)."<br/>\n";
			echo 'found val ('.gettype($rawOptions[$name]).'): "'.ent(is_array($rawOptions[$name]) ? 'Array' : $rawOptions[$name]).'"'.
				'<br/>';
		}
		dlib_bad_request_400('Invalid options for event list');
	}

	return ['options'  =>$res,
			'notParsed'=>$notParsed,
			'invalid'  =>$invalid,
		   ];
}

/**
 * Parse a single text option that comes from the outside (for example: a single GET or POST arg).
 *
 * The returned value is in internal (parsed) format that can be used with demosphere_event_list() and demosphere_event_list_options_defaults().
 * This is the reference parser for all standard options.
 * This function should always be used to parse standard options.
 * @param $name : the name of the option (get/post notation: example: "mapShape", not "map_shape")
 * @param $val  : string with the GET/POST value of the option.
 * @return array('ok / 'error' / 'unknown' ,$value) : 
 *  if status===error, $value is an error message
 *  if status===unknown, $value is raw unparsed $val
 */
function demosphere_event_list_parse_getpost_option(string $name,string $val)
{
	global $demosphere_config,$user;
	
	$status='ok';
	$error=false;
	switch($name)
	{
	case 'selectStartTime':
	case 'endTime':
	case 'addPrivateEvents':
	case 'offset':
	case 'limit':
	case 'maxNbEventsPerDay':
	case 'mostVisited':
	case 'selectRecentChanges':
	case 'selectRepetitionGroupId':
		// positive integer valued options
		if(strlen($val)===0){$error='empty value not allowed for "'.$name.'"';break;}
		if(!preg_match('@^[0-9]+$@',$val)){$error="expecting positive integer for ".$name;break;}
		$res=intval($val);
		// special case for options where 0 is actually means false
		if($res===0 && ($name==='maxNbEventsPerDay'     || 
						$name==='selectRecentChanges'   || 
						$name==='selectRepetitionGroupId' || 
						$name==='mostVisited'           )){$res=false;}
		break;
	case 'selectShowOnFrontpage':
		// integer or false valued options
		if(strlen($val)===0){$error='empty value not allowed for "'.$name.'"';break;}
		if(!preg_match('@^([0-9]+|false)$@',$val)){$error="expecting integer or false for ".$name;break;}
		$res=intval($val);
		if($val==="false"){$res=false;}
		break;
	case 'selectPlaceId':
	case 'selectPlaceReferenceId':
		// integer valued options
		if(strlen($val)===0){$error='empty value not allowed for "'.$name.'"';break;}
		if(!preg_match('@^[0-9]+$@',$val)){$error="expecting integer for ".$name;break;}
		$res=intval($val);
		if(Place::fetch($res,false)===null){$error="invalid place id";$res=false;break;}
		break;
	case 'selectCityId':
		if(strlen($val)===0){$error='empty value not allowed for "'.$name.'"';break;}
		if(!preg_match('@^[0-9]+$@',$val)){$error="expecting integer for ".$name;break;}
		$res=intval($val);
		if(City::fetch($res,false)===null){$error="invalid city id";$res=false;break;}
		break;
	case 'selectFeatured':
	case 'fullUrls':
	case 'orderByLastBigChanges':
	case 'default_noOtherFields':
		if($val==='true'  || $val==='1' || $val===''){$res=true; }
		else
		if($val==='false' || $val==='0'             ){$res=false;}
		else
		{$error='bad boolean value';}
		break;
	case 'selectModerationStatus':
		if(!preg_match('@^[0-9]+(,[0-9]+)*$@',$val))
		{$error="bad syntax: expecting one or more comma separated numbers";break;}
		$res=array_map('intval',explode(',',$val));
		foreach($res as $status)
		{
			if($user->checkRoles('admin','moderator') ||
			   $status ==Event::moderationStatusEnum('incomplete' ) ||
			   $status ==Event::moderationStatusEnum('published'  ) ||
			   ($status==Event::moderationStatusEnum('rejected-shown') && $demosphere_config['use_rejected_shown']) 
			   )
			{;}
			else
			{$res=false;$error='Permission denied';break;}
		}
		break;
	case 'selectTopic':
		if(strlen($val)===0){$error='empty value not allowed for "'.$name.'"';break;}
		if($val==='others'){$res='others';break;}
		if(!preg_match('@^[0-9]+$@',$val)){$error="expecting integer for ".$name;break;}
		$res=intval($val);
		if(Topic::fetch($res,false)===null){$error="invalid topic id";$res=false;break;}
		break;
	case 'selectVocabTerm':
		if(strlen($val)===0){$error='empty value not allowed for "'.$name.'"';break;}
		if(!preg_match('@^[0-9]+$@',$val)){$error="expecting integer for ".$name;break;}
		$res=intval($val);
		if(Term::fetch($res,false)===null){$error="invalid term id";$res=false;break;}
		break;
	case 'selectVocabTermOr':
		$res=[];
		foreach(explode('-',$val) as $term)
		{
			if(!preg_match('@^[0-9]+$@',$term)){$error="expecting integer for ".$name;break;}
			$tid=intval($term);
			//if(Term::fetch($tid,false)===null){$error="invalid term id";break;}
			$res[]=$tid;
		}
		break;
	case 'userCalendar':
		if(strlen($val)===0){$error='empty value not allowed for "'.$name.'"';break;}
		if(!preg_match('@^[0-9]+$@',$val)){$error="expecting number for ".$name;break;}
		$userCalUid=intval($val);
		$userCalUser=User::fetch($userCalUid,false);
		if($userCalUser===null){$error='invalid user number';break;}
		if(isset($userCalUser->calendar) && $userCalUser->calendar['ispublic']!==true &&
		   !$user->checkRoles('admin'))
		{
			$error='calendar is not public for user:'.$userCalUid;
			break;
		}
		$res=$userCalUid;
		break;
	case 'nearLatLng':
		if(!preg_match('@^[0-9.-]+(,[0-9.-]+){2}$@',$val))
		{$error='bad nearLatLng: expecting 3 comma-separated numbers (lat,lng,distance)';break;}
		if(strlen($val)===0){$error='empty nearLatLng';break;}
		$latLngDist=explode(',',$val);
		if(count($latLngDist)!==3){$error='bad nearLatLng: expecting 3 comma-separated values';break;}
		$res=array_map('floatval',array_combine(['lat','lng','distance'],$latLngDist));
		break;
	case 'mapShape':
		require_once 'demosphere-event-map.php';
		if(strlen($val)===0){$error='empty mapShape not allowed';break;}
		$shape=demosphere_mapshape_get($val);
		if($shape===false){$error='unknown mapShape: '.$val;}
		else{$res=$val;}
		break;
	case 'selectFpRegion':
		if(strlen($val)===0){$error='empty region not allowed';break;}
		$region=val($demosphere_config['frontpage_regions'],$val);
		if($region===false){$error='unknown region';}
		else{$res=$val;}
		break;
	default:
		return ['unknown',false];
	}
	if($error!==false){return ['error',$error];}
	return ['ok',$res];
}

/**
 * Renders events with demosphere-calendar.tpl.php
 */
function demosphere_calendar_render(array $events,array $options,bool $isMobile,bool $preRenderOnly=false)
{
	global $user,$base_url,$demosphere_config;
	require_once 'demosphere-date-time.php';
	require_once 'demosphere-frontpage.php';

	// add default values to to the options
	$options=demosphere_event_list_options_defaults($options);

	// use absolute paths 
	$base_path=preg_replace('@^https?://[^/]*@','',$base_url).'/';
	$urlPrefix=$base_path;
	if($options['fullUrls']){$urlPrefix=$base_url.'/';}

	// performance optimization : (demosphere_frontpage_url is slow!)
	$selectCityUrl=demosphere_frontpage_url($options,['selectCityId'=>9999999,'limit'=>false]);

	// ******* optional tooltips (when mouse hovers on top of title)
	$useTooltips=($options['tooltips']!==false);
	$useTooltipAddress= $useTooltips && (strpos($options['tooltips'],'<address>')!==false);

	$nowZ=date('z');
	$tommorowZ=date('z',strtotime('tomorrow'));

	// ******* Process each event
	$eventKeys=array_keys($events);
	foreach($eventKeys as $p=>$k)
	{
		$event=$events[$k];
		$prevEvent=isset($eventKeys[$p-1]) ? $events[$eventKeys[$p-1]] : false;
		$nextEvent=isset($eventKeys[$p+1]) ? $events[$eventKeys[$p+1]] : false;
		if(!isset($event->render['dates']))
		{
			$event    ->render['dates']=explode(' ',date('n W z',$event->startTime));
		}
		if($nextEvent!==false)
		{
			$nextEvent->render['dates']=explode(' ',date('n W z',$nextEvent->startTime));
		}
		
		// *** month,week and day breaks
		$event->render['is_first_event_of_month']=$prevEvent==false ||
			$event	  ->render['dates'][0]!=
			$prevEvent->render['dates'][0];

		$event->render['is_first_event_of_week']=$prevEvent==false ||
			$event	  ->render['dates'][1]!=
			$prevEvent->render['dates'][1];

		$event->render['is_first_event_of_day']=$prevEvent==false ||
			$event	  ->render['dates'][2]!=
			$prevEvent->render['dates'][2];

		$event->render['is_last_event_of_month']=$nextEvent==false ||
			$event	  ->render['dates'][0]!=
			$nextEvent->render['dates'][0];

		$event->render['is_last_event_of_week']=$nextEvent==false ||
			$event	  ->render['dates'][1]!=
			$nextEvent->render['dates'][1];

		$event->render['is_last_event_of_day']=$nextEvent==false ||
			$event	  ->render['dates'][2]!=
			$nextEvent->render['dates'][2];
			
		// used for adding day anchors
		if($event->render['is_first_event_of_day'])
		{
			$event->render['day_transitions']=[];
			if($prevEvent!==false)
			{
				for($i=1,$time=$prevEvent->startTime;
					($d=strtotime('+'.$i.' days 1am',$time))<$event->startTime && $i<100;
					$i++)
				{
					$event->render['day_transitions'][]=$d;
				}
			}
			else{$event->render['day_transitions']=[$event->startTime];}
		}

		// *** today/tomorrow
		$event->render['is_today'   ]=$event->render['dates'][2]===$nowZ;
		$event->render['is_tomorrow']=$event->render['dates'][2]===$tommorowZ;

		// ************* row classes

		// *** topic classes
		$topics=val($event->render,'topics',false);
		$topics=strlen($topics)===0 ? [] : explode(',',$topics);

		$topicClasses =implode(' t',$topics);
		if($topicClasses !=''){$topicClasses ='t'.$topicClasses ;}

		// *** which color or icon should we use for topics

		// *** higlight a topic (if highlight is requested from GET argument)
		$highlightClass=$options['highlightTopic']===false ? '' :
			// keep sel_t space 
			(array_search($options['highlightTopic'],$topics)!==false ? ' sel_t' :'xs');
		if($options['highlightTopic']==='others'){$highlightClass=count($topics)===0 ? ' sel_t' :'xs';}

		// *** dayrank icon
		$dayrankClass='';
		if(isset($event->visits) && isset($event->dayrank))
		{
			$dayrankClass=
				$demosphere_config['dayrank_enable'] &&
				$event->visits>=$demosphere_config['dayrank_min_visits'] &&
				($event->dayrank>=1 && $event->dayrank<=2) 
				?
				'dr'.$event->dayrank :
				'';
		}

		$userCalClasses='';
		if($options['addUserCalInfo'] && !val($event->render,'is_extra'))
		{
			$userCalClasses.=
				$event->render['uc_selected']===null ? '' :
				($event->render['uc_selected']==1 ? 'uc_select_y': 'uc_select_n');
			$userCalClasses.=' eid-'.$event->id;
		}

		// *** classes for extra events
		$extraEventClasses='';
		if(val($event->render,'is_extra')!==false)
		{
			$extraEventClasses.='extra ';
			$extraEventClasses.='extra-id-'.$event->id;
		}

		// *** classes for private events (user created, not shown on frontpage)
		$privateEventClasses='';
		if(oval($event,'showOnFrontpage',1)==0)
		{
			$privateEventClasses.='private ';
		}

		$contributorClasses='';
		if(val($options,'addForContributor') && !$event->status)
		{
			$contributorClasses.='contributor ';
			if($event->getModerationStatus()==='rejected' ||
			   $event->getModerationStatus()==='rejected-shown'){$contributorClasses.='rejected ';}
		}

		$event->render['row_classes']=
			$highlightClass.' '. // must be dlib_first (faster js code)
			$topicClasses.' '.
			$dayrankClass.' '.
			$userCalClasses.' '.
			val($event->render,'addClasses','').' '.
			$extraEventClasses.' '.
			$privateEventClasses.' '.
			$contributorClasses;
		// remove useless multiple spaces in classname
		$event->render['row_classes']=trim(preg_replace('@ +@',' ',$event->render['row_classes']));
		
		// *** time
		$event->render['time']=mb_strtolower(demos_format_date('time',$event->startTime));

		// *** tooltip over title (for example to display address)
		$event->render['tooltips_text']=false;
		if($useTooltips)
		{
			$event->render['tooltips_text']=$options['tooltips'];
			$addressLines=explode("\n",$event->usePlace()->address);
			$event->render['tooltips_text']=
				str_replace('<address>',
							$addressLines[0],
							$event->render['tooltips_text']);
		}

		// *** url
		// Note: this overrides urls requested by $options['url']
		$extra=http_build_query($options['extraGetArgs']);
		$event->render['url']=$urlPrefix.''.$demosphere_config['event_url_name'].'/'.$event->id.
			($extra!=='' ? '?'.$extra : '');
		if(val($event->render,'is_extra')){$event->render['url']=filter_xss_strip_dangerous_protocols($event->url);}
	}

	if($preRenderOnly){return $events;}

	return template_render($isMobile ? 'templates/demosphere-calendar-mobile.tpl.php' : 'templates/demosphere-calendar.tpl.php',
						   [compact('events','options','base_path','selectCityUrl')
						   ]);
}



/**
 * Renders a short message saying that we are only displaying a selection of events.
 *
 * (Note: as of 2/2011 this is only used by frontpage)
 * @param $options: an array of options (see demosphere_calendar_default_options() )
 */
function demosphere_calendar_event_filter_message(array $options,array $disable=[])
{
	global $demosphere_config,$base_url,$user;
	// FIXME: added for demosphere_frontpage_url, 
	require_once 'demosphere/demosphere-frontpage.php';

	// this is necessary to add default options
	$options=demosphere_event_list_options_defaults($options);

	$out  = '';
	$messages=[];

	// Note: we don't show userCalendar messages as this is only used on frontpage where it is explicit that we are on a user calendar
		
	if($options['selectModerationStatus']!==false &&
	   array_search(Event::moderationStatusEnum('rejected-shown'),$options['selectModerationStatus'])!==false)
	{
		$out.="<h4>".ent($demosphere_config['rejected_shown_message']).'</h4>';
	}
	if($options['selectModerationStatus']!==false)
	{
		$messages['rejected-shown']='status: ';
		$mstatusLabels=Event::moderationStatusLabels();
		foreach($options['selectModerationStatus'] as $s)
		{
			$messages['rejected-shown'].=($user->checkRoles('admin','moderator') ?
										  ent($mstatusLabels[$s]['title']).'; ': ent($s).', ');
		}
	}

	if($options['limit']!==false)
	{
		$messages['limit']=t('first @nb events',
					  ['@nb'=>$options['limit']]);
	}

	if($options['selectFpRegion']!==false)
	{
		$region=$demosphere_config['frontpage_regions'][$options['selectFpRegion']];
		if(isset($region['cityId']) && $region['cityId']!==false)
		{
			$city=City::fetch($region['cityId']);
			if(strlen(val($region,'name'))==0){$region['name']=$city->name;}
		}
		$messages['selectFpRegion']=ent($region['name']);
	}

	if($options['selectTopic']!==false)
	{
		$messages['selectTopic']=ent($options['selectTopic']!=='others' ? Topic::fetch($options['selectTopic'])->name : t('others'));
	}

	if($options['selectVocabTerm']!==false)
	{
		$messages['selectVocabTerm']=ent(Term::fetch($options['selectVocabTerm'])->name);
	}

	if($options['selectVocabTermOr']!==false)
	{
		$terms=db_result('SELECT GROUP_CONCAT(name) FROM Term WHERE id IN('.
						 implode(',',array_map('intval',$options['selectVocabTermOr'])).')');
		if($terms===null){$terms='invalid terms';}
		$messages['selectVocabTermOr']=ent($terms);
	}

	if($options['selectCityId']!==false)
	{
		$messages['selectCityId']=ent(City::fetch($options['selectCityId'])->name);
	}

	if($options['nearLatLng']!==false)
	{
		$messages['nearLatLng']=t('less than @dist km from @pos',
								  ['@dist'=>$options['nearLatLng']['distance'],
								   '@pos'=>
								   substr($options['nearLatLng']['lat'],0,7).','.
								   substr($options['nearLatLng']['lng'],0,7)]);
	}

	if($options['mapShape']!==false)
	{
		$messages['mapShape']=t('inside map area called !name',
								['!name'=>ent($options['mapShape'])]);
	}

	if($options['selectPlaceId']!==false)
	{
		$place=Place::fetch($options['selectPlaceId']);
		$messages['selectPlaceId']=
			t('at !placename',
			  ['!placename'=>'<a href="'.$place->url().'">'.ent($place->makeName()).'</a>']);
	}
	if($options['selectPlaceReferenceId']!==false)
	{
		$place=Place::fetch($options['selectPlaceReferenceId']);
		$messages['selectPlaceReferenceId']=
			t('at !placename',
			  ['!placename'=>'<a href="'.$place->url().'">'.ent($place->makeName()).'</a>']);
	}

	if($options['selectRecentChanges']!==false)
	{
		$messages['selectRecentChanges']=
			t('recent changes since @h hours',['@h'=>round($options['selectRecentChanges']/3600)]);
	}

	if($options['orderByLastBigChanges']!==false)
	{
		$out.='<h4 class="event-filter-message">'.t('Sorted to display recently published (or changed) events first.').'</h4>';
	}

	if($options['maxNbEventsPerDay']!==false)
	{
		$messages['maxNbEventsPerDay']=t('max @nb events per day',
										 ['@nb'=>intval($options['maxNbEventsPerDay'])]);
	}

	if($options['mostVisited']!==false)
	{
		$messages['mostVisited']=t('@nb most visited events',
								   ['@nb'=>intval($options['mostVisited'])]);
	}

	foreach($disable as $d){unset($messages[$d]);}

	// no message
	if(count($messages)===0){return $out;}
	$removeFromResetUrl=array_keys($messages);
	if($options['limit']===$demosphere_config['front_page_limit_nb_events']){$removeFromResetUrl[]='limit';}

	$out.='<h4 class="event-filter-message">'.
		      t('Displaying only : "!messages"',['!messages'=>implode('; ',$messages)]).'. '.
		      '<a class="back" href="'.ent(demosphere_frontpage_url($options,[],$removeFromResetUrl)).'">'.t('return to full display').'</a>'.
		  '</h4>';
	return $out;
}

/**
 * Renders a list of year/month links for browsing old events.
 *
 * (Note: as of 12/2010 this is only used by frontpage)
 * @param $options: an array of options (see demosphere_calendar_default_options() )
 */
function demosphere_calendar_archive_links(array $options)
{
	// FIXME: added for demosphere_frontpage_url, 
	require_once 'demosphere/demosphere-frontpage.php';
	// this is necessary to add default options
	$options=demosphere_event_list_options_defaults($options);

	$head  = '';
	$head.='<div id="archive"><h4>';
	$head.=t('Display of archived events:').' '.
		'<strong>'.strftime('%B %Y',$options['selectStartTime']).'</strong>';
	$head.=' <span>(<a href="'.
		ent(demosphere_frontpage_url($options,[],
										['selectStartTime','endTime','showArchiveLinks'])).
		'">'.
		t('back to today').'</a>)</span></h4>';

	// Get the time of the first and last event ever published   .
	// Use speedup, because MIN/MAX queries can be quite slow (> 50ms each) on large sites.
	$q="SELECT MIN(startTime) FROM Event WHERE status=1 AND showOnFrontpage=1";
	$first=db_result($q." AND id<3000");
	if($first===null){$first=db_result($q);}
	$q="SELECT MAX(startTime) FROM Event WHERE status=1 AND showOnFrontpage=1";
	$last =db_result($q." AND id>%d",db_result("SELECT MAX(id) FROM Event")-3000);
	if($last===null){$last=db_result($q);}

	$head.='<table>';
	$firstMonth=mktime(0,0,1,date('n',$first),1,date('Y',$first));
	$lastMonth =mktime(0,0,1,date('n',$last ),1,date('Y',$last ));
	$nowMonth  =mktime(0,0,1,date('n'       ),1,date('Y'       ));
	$y=false;
	$optStartTime=val($options,'selectStartTime');
	for($year=intval(date('Y',$first));$year<=date('Y',$last);$year++)
	{
		$head.='<tr><th>'.$year.'</th>';
		for($month=1;$month<=12;$month++)
		{
			$ts=mktime(0,0,1,$month,1,$year);
			if($ts<$firstMonth || $ts>$lastMonth){$head.='<td></td>';continue;}
			$m=strftime('%B',$ts);
			if(mb_strlen($m)>8){$m=strftime('%b',$ts);}
			if($ts==$optStartTime){$head.='<td id="archive-current">'.$m.'</td>';continue;}
			$head.='<td '.
				($ts> $nowMonth ? 'class="future"' : '').
				($ts==$nowMonth ? 'id="archive-now"'    : '').'><a href="'.
				ent(demosphere_frontpage_url($options,
											 ['selectStartTime'=>$ts,
											  'endTime'=>strtotime('first day of next month 0:00:01',$ts),
											  'showArchiveLinks'=>true],
												['selectShowOnFrontpage'])).
				'">'.$m.'</a></td>';
		}
		$head.='</tr>';
	}
	$head.='</table></div>';
	return $head;
}



//! Output a JSON list of events using $_GET options.
//! Json output is simplified (flattened : no nested objects like place & city)
//! Only requested (or default) fields are output in JSON.
function demosphere_event_list_json()
{
	global $base_url,$demosphere_config,$user;
	require_once 'demosphere-date-time.php';

	$rawOpts=$_GET;
	$parseRes=demosphere_event_list_parse_getpost_options($rawOpts,
														  ['noFields'=>false,
														   'onlyEventChoose'=>false]
												   );
	
	// **** Determine list of requested fields, requested place fields and city fields
	// This is needed to remove all empty fields from partially filled Event, Place and City objects.
	$fields=demosphere_event_list_options_defaults($parseRes['options']);
	$fields=array_filter($fields,function($v){return $v===true;});
	$fields=array_keys($fields);
	$allFields=demosphere_event_list_class_fields();
	$allFields=array_merge($allFields,['url','topics','uc_selected','ucUid',]);
	$fields=array_intersect($fields,$allFields);
	$placeFields=preg_grep('@^place__(?!city__)@',$fields);
	$cityFields =preg_grep('@^place__city__@'    ,$fields);
	if(count($placeFields)){$placeFields=array_combine($placeFields,str_replace('place__'      ,'',$placeFields));}
	if(count($cityFields )){$cityFields =array_combine($cityFields ,str_replace('place__city__','',$cityFields ));}
	$fields=array_flip($fields);
	//var_dump($fields,$placeFields,$cityFields);die('oo');

	// **** actually fetch the events
	$events=demosphere_event_list($parseRes['options']);
	//var_dump($events[0]);
	if(count($parseRes['notParsed']) || count($parseRes['invalid']))
	{
		dlib_bad_request_400('demosphere_event_list_json: unknown or not-allowed options:'.
							 ent(implode(', ',array_keys($parseRes['notParsed']))));
	}

	// **** build thae actuall output array 
	// - flatten (remove place & city objects)
	// - add fields for backwards compatibility
	$res=[];
	foreach($events as $event)
	{
		$fixed=(array)$event;

		// flatten Place
		if(count($placeFields))
		{
			$place=$event->usePlace();
			foreach($placeFields as $field=>$name){$fixed[$field]=$place->$name;}
		}
		// flatten City
		if(count($cityFields))
		{
			$place=$event->usePlace();
			$city =$place->useCity();
			foreach($cityFields as $field=>$name){$fixed[$field]=$city->$name;}
		}

		// add "fake" fields
		if(isset($fields['topics'])){$fixed['topics']=val($event->render,'topics','');}
		if(isset($fields['url'   ])){$fixed['url'   ]=$event->render['url'   ];}

		// cleanup : remove all un-requested fields
		$fixed=array_intersect_key($fixed,$fields);

		// Add extra JSON-specific fields (that make life easier in js)
		if(isset($fields['startTime']))
		{
			$fixed['time']=mb_strtolower(demos_format_date('time',$event->startTime));
		}
		if(isset($fixed['place__city__name']) && 
		   isset($fixed['place__city__shortName']))
		{
			$fixed['place__city__getShort']=$city->shortName !=='' ? $city->shortName : $city->name;
		}

		$res[]=$fixed;
	}
	//var_dump($res);
	//echo '<pre>';var_dump($events);
	return ['events'=>$res];
}

?>