<?php

//! Displays the most  urgent events that need to  be moderated.
//! Many fields (status, title, date, ...) can be directly edited (ajax).
//! This relies on the demosphere_event_multiple_edit... functions.
function demosphere_pending_events()
{
	global $base_url,$currentPage;
	$vars=demosphere_event_multiple_edit_setup();
	extract($vars);

	$currentPage->title=t('Pending event list');

	// firefox incorrectly restores form elements after reload. This forces reset.
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");

	// ******** Changed places (edited by inCharge)

	$changedPlaces=Place::fetchList('WHERE inChargeId!=0 AND inChargeChanged>0');
	if(count($changedPlaces))
	{
		$out.='<table id="changed-places">';
		foreach($changedPlaces as $place)
		{
			$out.='<tr data-id="'.$place->id.'">';
			$out.=
				'<td class="edit-link link-icon-col"><a href="'.ent($place->url()).'/edit"></a></td>'.
				'<td class="view-link link-icon-col"><a href="'.ent($place->url()).     '"></a></td>'.
				'<td><button>'.ent(t('ok')).'</button></td>'.
				'<td>'.ent(t('Place edited by:')).' </td>'.
				'<td><a href="'.ent($base_url).'/user/'.$place->inChargeId.'">'.ent($place->useInCharge()->login).'</a></td>'.
				'<td><a href="'.ent($place->url()).'">'.ent($place->nameOrTitle()).'</a></td>';
			$out.='</tr>';
		}
		$out.='</table>';
	}

	$out.='<div id="pending-events">';
	

	// ******** Show events needing attention (level 2 : >=100)
	$hideNattentionDelay=3600*24*30;
	$pager=new Pager(['itemName'=>t('events'),'prefix'=>'attention','defaultItemsPerPage'=>40]);
	$events=Event::fetchPartial('SELECT SQL_CALC_FOUND_ROWS '.$fields.' FROM '.$joins.' WHERE '.
								'moderationStatus=%d OR '.
								'(needsAttention>=100 '.
								(isset($_GET['allNattention']) ? ') ' : ' AND startTime>'.(time()-$hideNattentionDelay).') ').
								'ORDER BY startTime ASC, id ASC'.
								 $pager->sql(),
								Event::moderationStatusEnum('created'));
	$pager->foundRows();
	$out.='<h2>'.t('Events that need attention').'</h2>';
	if(count($events)===0){$out.='<p>'.t('( no events found )').'</p>';}
	$ctHidden=db_result('SELECT COUNT(*) FROM Event WHERE needsAttention>=100 AND startTime<=%d',time()-$hideNattentionDelay);
	if($ctHidden>0 && !isset($_GET['allNattention']))
	{
		$out.='<p id="hidden-nattention"><a href="?allNattention">'.t('!ct hidden',['!ct'=>$ctHidden]).'</a></p>';
	}
	$out.='<table class="multiple-edit pending-nattention">';
	foreach($events as $event)
	{
		$out.=demosphere_event_multiple_edit_render($event,['full-needs-attention'=>true]);
	}
	$out.='</table>';
	$out.=template_render('dlib/pager.tpl.php',['pager'=>$pager]);

	// ******** Show future published but incomplete events
	$pager=new Pager(['itemName'=>t('events'),'prefix'=>'incomplete','defaultItemsPerPage'=>20]);
	$out.='<h2>'.t('Events that are published but are incomplete').'</h2>';
	$events=Event::fetchPartial('SELECT SQL_CALC_FOUND_ROWS '.$fields.' FROM '.$joins.' WHERE '.
								'startTime>%d AND '.
								'moderationStatus=%d '.
								'ORDER BY startTime ASC, id ASC '.
								 $pager->sql(),
								Event::today(),
								Event::moderationStatusEnum('incomplete'));
	$pager->foundRows();
	if(count($events)===0){$out.='<p>'.t('( no events found )').'</p>';}
 
	$out.='<table class="multiple-edit pending-incomplete">';
	foreach($events as $event)
	{
		$out.=demosphere_event_multiple_edit_render($event);
	}
	$out.='</table>';
	$out.=template_render('dlib/pager.tpl.php',['pager'=>$pager]);
	//if(!isset($_GET['no-limit-incomplete']) && count($events)!==$nbEvents)
	//{
	// 	$out.='<p>'.t('only shown @nb out of @tot. <a href="!url">Show more</a>.',
	// 				 array('@nb'=>count($events),'@tot'=>$nbEvents,'!url'=>'?no-limit-incomplete')).'</p>';
	//}

	// ******** Show unpublished waiting events
	$hideWaitingDelay=3600*24*30;
	$out.='<h2>'.t('Events that are not published, waiting').'</h2>';
	$pager=new Pager(['itemName'=>t('events'),'prefix'=>'waiting','defaultItemsPerPage'=>30]);
	$events=Event::fetchPartial('SELECT SQL_CALC_FOUND_ROWS '.$fields.' FROM '.$joins.' WHERE '.
								'moderationStatus=%d '.
								(isset($_GET['allWaiting']) ? '' : 'AND startTime>'.(time()-$hideWaitingDelay).' ').
								'ORDER BY startTime ASC, id ASC '.
								$pager->sql(),
								Event::moderationStatusEnum('waiting'));
	$pager->foundRows();
	if(count($events)===0){$out.='<p>'.t('( no events found )').'</p>';}
	$ctHidden=db_result('SELECT COUNT(*) FROM Event WHERE moderationStatus=%d AND startTime<=%d',Event::moderationStatusEnum('waiting'),time()-$hideWaitingDelay);
	if($ctHidden>0 && !isset($_GET['allWaiting']))
	{
		$out.='<p id="hidden-waiting"><a href="?allWaiting">'.t('!ct hidden',['!ct'=>$ctHidden]).'</a></p>';
	}
	$out.='<table class="multiple-edit pending-waiting">';
	foreach($events as $event){$out.=demosphere_event_multiple_edit_render($event);}
	$out.='</table>';
	$out.=template_render('dlib/pager.tpl.php',['pager'=>$pager]);

	//if(!isset($_GET['no-limit-waiting']) && count($events)!==$nbEvents)
	//{
	// 	$out.='<p>'.t('only shown @nb out of @tot. <a href="!url">Show more</a>.',
	// 				 array('@nb'=>count($events),'@tot'=>$nbEvents,'!url'=>'?no-limit-waiting')).'</p>';
	//}

	// ******** Show rejected future events
	$out.='<h2>'.t('Rejected events').'</h2>';
	$out.='<p>See <a href="'.$base_url.
		'/?selectModerationStatus='.Event::moderationStatusEnum('rejected-shown').','.Event::moderationStatusEnum('rejected').
		'&amp;showArchiveLinks=1&amp;selectStartTime='.strtotime('first day of this month 0:00:01').
		'">archived rejected events</a></p>';

	$pager=new Pager(['itemName'=>t('events'),'prefix'=>'rejected','defaultItemsPerPage'=>20]);
	$events=Event::fetchPartial('SELECT SQL_CALC_FOUND_ROWS '.$fields.' FROM '.$joins.' WHERE '.
								'startTime>%d AND '.
								'(moderationStatus=%d OR moderationStatus=%d) '.
								'ORDER BY startTime ASC, id ASC '.
								 $pager->sql(),
								Event::today(),
								Event::moderationStatusEnum('rejected-shown'),
								Event::moderationStatusEnum('rejected'));
	$pager->foundRows();
	if(count($events)===0){$out.='<p>'.t('( no events found )').'</p>';}
	$out.='<table class="multiple-edit pending-rejected">';
	foreach($events as $event){$out.=demosphere_event_multiple_edit_render($event);}
	$out.='</table>';
	$out.=template_render('dlib/pager.tpl.php',['pager'=>$pager]);

	// ******** Show trash (trash) events
	$out.='<h2>'.t('Events in trash bin').'</h2>';
	$pager=new Pager(['itemName'=>t('events'),'prefix'=>'trash','defaultItemsPerPage'=>20]);
	$events=Event::fetchPartial('SELECT SQL_CALC_FOUND_ROWS '.$fields.' FROM '.$joins.' WHERE '.
								'moderationStatus=%d '.
								'ORDER BY startTime ASC, id ASC '.
								$pager->sql(),
								Event::moderationStatusEnum('trash'));
	$pager->foundRows();
	if(count($events)===0){$out.='<p>'.t('( no events found )').'</p>';}
	$out.='<table class="multiple-edit pending-trash">';
	foreach($events as $event){$out.=demosphere_event_multiple_edit_render($event);}
	$out.='</table>';
	$out.=template_render('dlib/pager.tpl.php',['pager'=>$pager]);

	$out.='</div>';
	
	return $out;
}

//! This function prepares the rendering of events in an ajax-editable list.
//! It adds js, css and returns an array of variables that can be extracted.
function demosphere_event_multiple_edit_setup($forceReload=true)
{
	require_once 'demosphere-event-list.php';
	global $base_url,$demosphere_config,$currentPage;

	$currentPage->addJsConfig('demosphere',['use_rejected_shown','event_title_max_width','city_max_width','websockets_enabled','place_url_name']);

	$out=dlib_add_form_token('demosphere_event_multiple_edit');

	global $demosphere_config;
	// Load more recent version of jquery + jquery ui
	$currentPage->addJs('lib/jquery.js');
	$currentPage->addJs('lib/jquery-browser.js');

	$currentPage->addJs( 'dlib/merge-calls.js');
	$currentPage->addJs( 'demosphere/js/demosphere-event-near-date.js');
	$currentPage->addJs( 'demosphere/js/meditable.js');
	$currentPage->addJs( 'demosphere/js/demosphere-event-multiple-edit.js');
	$currentPage->addJs( 'demosphere/js/demosphere-misc.js');
	$status=Event::getSchema();
	$status=$status['moderationStatus']['values'];
	$currentPage->addJsVar('demosphere_mstatus',$status);
	$currentPage->addJsVar('demosphere_mstatus_labels',Event::moderationStatusLabels());
	$nattention=Event::needsAttentionTitles();
	$currentPage->addJsVar('demosphere_nattention',array_combine(preg_replace('@^@','' ,array_keys($nattention)),array_values($nattention)));

	$topics=Topic::getAllNames()+($demosphere_config['topics_required']!=='mandatory' ? [0=>t('others')] : []);
	$currentPage->addJsVar('demosphere_topics'    ,array_combine(preg_replace('@^@','t',array_keys($topics    )),array_values($topics    )));


	$currentPage->addCssTpl('demosphere/css/demosphere-event-multiple-edit.tpl.css');
	$currentPage->addCssTpl('demosphere/css/demosphere-highlight.tpl.css');
	$currentPage->addCssTpl('demosphere/css/meditable.tpl.css');

	$currentPage->addJs( 'lib/jquery-ui-1.12.1.custom/jquery-ui.js');
	$currentPage->addCss('lib/jquery-ui-1.12.1.custom/jquery-ui.min.css');

	// **** js text
	$currentPage->addJsTranslations(
		[
			'titleTooLong'    =>t('Too long !'),
		]);

	require_once 'dlib/find-date-time/find-date-time.php';

	$fields=Event::sqlFields('list_display+changed+changeNumber+place.address');
	$joins =Event::sqlJoins ('list_display');

	return compact('out','fields','joins');
}


//! render a single event that can be edited through ajax.
function demosphere_event_multiple_edit_render($event,$options=[])
{
	global $base_url,$demosphere_config;
	require_once 'demosphere-event-repetition.php';
	require_once 'demosphere-date-time.php';

	$eventUrl=$base_url.'/'.$demosphere_config['event_url_name'];

	// ***** Create the table row with classes and data fields
	$out='';
	$out.='<tr data-eid="'.$event->id.'" class="'.
		'status-'.intval($event->moderationStatus).' '.
		'status-'.$event->getModerationStatus().' '.
		'nattention-'.intval($event->needsAttention).' '.
		'nattention-level-'.(!$event->needsAttention ? 0:1+floor($event->needsAttention/100)).' '.
		($event->showOnFrontpage ? 'public' : 'private').' '.
		'" '.
		'data-change-number="'.intval($event->changeNumber).'" '.
		'>';

	// ***** display edit link 
	$out.='<td class="edit-link link-icon-col">';
	$out.='<a href="'.$eventUrl.'/'.$event->id.'/edit"><span>edit</span></a>';
	$out.='</td>';

	// ***** display link 
	$out.='<td class="view-link link-icon-col">';
	$out.='<a href="'.$eventUrl.'/'.$event->id.'"><span>link</span></a>';
	$out.='</td>';

	// ***** repetition copy link
	if(val($options,'repetition-copy-link'))
	{
		$out.='<td class="repetition-copy-link link-icon-col">';
		if($event->repetitionGroupId)
		{
			$out.='<a href="'.$eventUrl.'/'.$event->id.'/repetition-copy"><span>'.
				t('copy').'</span></a>';
		}
		$out.='</td>';		
	}

	// ***** repetition ref 
	if(val($options,'repetition-ref'))
	{
		$out.='<td class="repetition-ref">';
		$out.='<input type="radio" '.
			($event->getRepetitionReference()==$event->id ? 'checked="checked"':'').
			'/>';
		$out.='</td>';		
	}

	// ***** repetitions link
	if(val($options,'repetition-link',true))
	{
		$out.='<td class="repetition-link">';
		$repOut='';
		if($event->repetitionGroupId)
		{
			$reps=$event->getAllRepetitions();
			if(count($reps)>1)
			{
				$repOut='<a href="'.$eventUrl.'/'.$event->id.'/repetition">'.count($reps).'</a>';
			}
		}
		$out.=$repOut;
		$out.='</td>';
	}

	// ***** showOnFrontpage / private event
	$out.='<td class="showOnFrontpage"><span title="'.t('private').'">P</span></td>';

	// ***** opinions
	if($demosphere_config['enable_opinions'])
	{
		require_once 'demosphere-opinion.php';

		// Cahcing just for performance. Using static is not pretty :-(
		static $userReputations=false;
		if($userReputations===false){$userReputations=demosphere_opinion_all_users_reputation();}

		$eventRatingData=demosphere_opinion_event_rating_average($event->id,$userReputations);
		$out.='<td class="opinions">';
		if($eventRatingData['nbRatings']>0)
		{
			$out.='<a href="'.ent($event->url()).'#opinions" title="'.ent($eventRatingData['display']).'">';
			$out.=round($eventRatingData['average'],1).' ('.$eventRatingData['nbRatings'].')';
			$out.='</a>';
		}
		$out.='</td>';
	}

	// ***** display moderation status
	$mstatusLabels=Event::moderationStatusLabels();
	$out.='<td class="moderation-status">';
	$out.='<div class="delayed-meditable moderation-status" data-val="'.$event->moderationStatus.'">'.
		ent($mstatusLabels[$event->moderationStatus]['short']).'</div>';
	$out.='</td>';

	// ***** display needs attention
	$isSmall=!val($options,'full-needs-attention');
	$out.='<td  class="needs-attention '.($isSmall? 'small-needs-attention' :'').'">';
	$nattention=Event::needsAttentionTitles();
	$out.='<div class="delayed-meditable needs-attention" data-selected="'.$event->needsAttention.'">'.
		ent($nattention[$event->needsAttention]).'</div>';
	$out.='</td>';

	// ***** display date 
	$ts=$event->startTime;
	$unset=date('Y',$ts)<=1970;
	$dout='';
	$dout.= '<td class="date weekday demosphere_timestamp" '.
		'data-demosphere-timestamp="'.
		demos_format_date('demosphere-timestamp',$event->startTime).'" >'.
		($unset?'':ent(strftime('%a',$ts))).'</td>';
	$attrs=' data-val="'.demos_format_date('short-day-month-year',$ts).'" ';
	$day  ='<td class="date day   delayed-meditable" '.$attrs.'>'.($unset?'':ent(strftime('%e',$ts))).'</td>';
	$now=time();
	$month='<td class="date month delayed-meditable" '.$attrs.'>'.($unset?'':ent(strftime('%b',$ts))).'</td>';
	$dout.=demosphere_date_time_swap_endian($day,$month,'');
	$out.=$dout;
	$out.='<td class="date year">'.($ts!=0 && abs($ts-$now)>3600*24*30*6 ? ent(strftime('%Y',$ts)) : '').'</td>';
	$out.='<td class="time delayed-meditable">'.($unset ||strftime('%R',$ts)=='03:33' ?'':ent(strftime('%R',$ts))).'</td>';

	// ***** moderation message
	// Note: message and title are in same td, to save horizontal space when only a few events have a message.
	$out.='<td class="message-and-title"><div class="message-and-title-wrap">';
	$out.='<span class="moderation-message delayed-meditable" data-val="'.ent($event->moderationMessage).'">';
	$out.=ent($event->shortModerationMessage());
	$out.='</span>';

	// ***** display title 
	$out.='<a class="title delayed-meditable" '.
		'href="'.$eventUrl.'/'.$event->id.'/edit" '.
		'data-val="'.ent(dlib_html_entities_decode_all(preg_replace('@</?strong>@','_',$event->htmlTitle))).'" '.
		'>';
	require_once 'dlib/filter-xss.php';
	$out.=filter_xss($event->htmlTitle,['strong']);
	$out.='</a>';
	$out.='</div></td>';

	// ***** display city+place
	$out.='<td class="place delayed-meditable" data-val="'.$event->placeId.'">';
	$out.=ent($event->usePlace()->useCity()->getShort()).
		'<div class="address">'.$event->usePlace()->htmlAddress().'</div>';
	$out.='</td>';

	// ***** display topics
	$topics=$event->getTopicNames();
	$out.='<td class="topics delayed-meditable" data-val="'.implode(',',preg_replace('@^@','t',array_keys($topics))).'">';
	$out.=demosphere_event_multiple_edit_topic_icon($topics);
	$out.='</td>';


	$out.='</tr>'."\n";

	return $out;
}

function demosphere_event_multiple_edit_topic_icon($topics)
{
	require_once 'demosphere-topics.php';
	if(!count($topics)){return '';}
	$class=' t'.dlib_first_key($topics);
	return '<span title="'.ent(implode(', ',$topics)).'" class="topic-icon '.$class.'"></span>'.(count($topics)>1 ? '+':'');
}

//! This is called from js (ajax) each time a field is changed.
//! All calls contain at least $_POST['eid'],$_POST['change_number']/
//! Care is taken not to change events that might have been edited elsewhere.
//! A json array containing error descriptions, and other information is returned to js.
function demosphere_event_multiple_edit_ajax()
{
	global $demosphere_config;

	dlib_check_form_token('demosphere_event_multiple_edit');

	// Simple semaphore locking to avoid concurrency. Semaphore is automatically released when script exits.
	$semaphoreKey=ftok(getcwd().'/'.$demosphere_config['tmp_dir'],'A');
	$semaphore=sem_get($semaphoreKey,1,0600);
	$ok=sem_acquire($semaphore);
	if(!$ok){fatal('sem_acquire failed multiple edit');}

	// no need to check permissions: this is specified in menu item

	$res=[];
	// ********** Load Event 
	$event=Event::fetch(val($_POST,'eid'),false);
	if($event===null){return ['errors'=>['invalid event or eid']];}

	// *********** 
	
	require_once 'demosphere-event-ajax-edit.php';
	list($res,$changed)=demosphere_event_ajax_edit_update($event);

	if(isset($res['errors'])){return $res;}

	// *********** Add information (like HTML/text) to $res that is specific to multiple edit

	if(isset($_POST['moderationStatus']))
	{
		$mstatusLabels=Event::moderationStatusLabels();
		$res['text']=$mstatusLabels[$event->moderationStatus]['short'];
	}

	if(isset($_POST['date']))
	{
		$ts=$event->startTime;
		$res['ts']=$ts;
		require_once 'demosphere-date-time.php';
		$res['value']=demos_format_date('short-day-month-year',$ts);
		$f=['weekday'=>strftime('%a',$ts),
			'day'    =>strftime('%e',$ts),
			'month'  =>strftime('%b',$ts),
			'year'   =>strftime('%Y',$ts)];
		$res['text' ]=$f[$_POST['date_field']];
		$res=array_merge($res,$f);
	}

	if(isset($_POST['title']))
	{
		$res['html']=$event->htmlTitle;
		$res['value']=dlib_html_entities_decode_all(preg_replace('@</?strong>@','_',$event->htmlTitle));
	}

	if(isset($_POST['placeId']))
	{
		$res['html']=ent($event->usePlace()->useCity()->getShort()).
			'<div class="address">'.$event->usePlace()->htmlAddress().'</div>';
		$res['value']=$event->placeId;
	}

	if(isset($_POST['topics']))
	{
		$topics=$event->getTopicNames();
		require_once 'demosphere/demosphere-topics.php';
		$res['html']=demosphere_event_multiple_edit_topic_icon($topics);
	}

	if(isset($_POST['moderationMessage']))
	{
		$res['html']=ent($event->shortModerationMessage());
		$res['value']=$event->moderationMessage;
	}

	return $res;
}

//! Ajax client requests any events that have changed
function demosphere_event_multiple_edit_ajax_get_changes()
{
	global $demosphere_config;
	require_once 'demosphere/demosphere-topics.php';
	$checkEvents=json_decode($_POST['events'],true);
	if(count($checkEvents)==0){return [];}
	
	$now=time();

	$changeNumbers=db_one_col("SELECT id,changeNumber FROM Event WHERE  ".
							  "id IN(".implode(',',array_map('intval',array_keys($checkEvents))).")");
	$changedEids=[];
	foreach($changeNumbers as $eid=>$changeNumber)
	{
		if($changeNumber>$checkEvents[$eid]){$changedEids[]=$eid;}
	}
	if(!count($changedEids)){return [];}
	
	$fields=Event::sqlFields('list_display+changed+changeNumber+place.address');
	$joins =Event::sqlJoins ('list_display');
	$events=Event::fetchPartial('SELECT '.$fields.' FROM '.$joins.' WHERE '.
								'Event.id IN ('.implode(',',$changedEids).')');

	$options=[];
	// If we are in the repetition multiple edit page then we need to change a few options for display html
	if($_POST['isRepetition']==='true'){$options=['repetition-ref'=>true,
												  'repetition-link'=>false,
												  'repetition-copy-link'=>true];}

	$res=[];
	foreach($events as $event)
	{
		$html=demosphere_event_multiple_edit_render($event,$options);
		$res[$event->id]=['changeNumber'=>$event->changeNumber,
						  'html'=>$html
						  ];
	}
	return $res;
}
?>