<?php

function demosphere_maintenance_add_paths(&$paths,&$options)
{
	ini_set('max_execution_time', max(600,ini_get('max_execution_time')));

	$paths[]=['path'=>'maintenance/tmp-test',
			  'roles' => ['admin'],
			  'output'=> false,
			  'function' => 'demosphere_maintenance_tmp_test',
			  ];

	$paths[]=['path'=>'maintenance/import-mapshape-directory',
			  'roles' => ['admin'],
			  'output'=> false,
			  'function' => 'demosphere_maintenance_import_mapshape_directory',
			  ];

	$paths[]=['path'=>'maintenance/extract-all-documents',
			  'roles' => ['admin'],
			  'output'=> false,
			  'function' => 'demosphere_maintenance_extract_all_documents',
			  ];

	$paths[]=['path'=>'maintenance/update-place-search-word-freq',
			  'roles' => ['admin'],
			  'output'=> false,
			  'function' => 'demosphere_maintenance_update_place_search_word_freq',
			  ];

	$paths[]=['path'=>'maintenance/page-watch-test-page',
			  'roles' => ['admin'],
			  'output'=> false,
			  'function' => function(){require_once 'page-watch/page-watch.php';page_watch_controller('page/test-page');},
			  ];

	$paths[]=['path'=>'maintenance/estimate-html-display-width',
			  'roles' => ['admin'],
			  'function' => 'demosphere_maintenance_estimate_html_display_width',
			  ];

}

function demosphere_maintenance_tmp_test()
{
	global $base_url,$demosphere_config;
	require_once 'demosphere-email-subscription.php';

	require_once 'demosphere-place-search.php';

	//demosphere_place_search_long_search($event->body.' abc',40);
	//demosphere_place_search_long_search('ceci est un texte');

	fatal('abort');

	Place::checkPlaces();
	Place::checkPlaceEvent();
	Place::cleanupUnusedPlaces();

	fatal('abort');
}

function demosphere_maintenance_fix_incomplete_messages()
{
	$events=db_one_col("SELECT id,body FROM Event WHERE moderationStatus=3 AND body LIKE '%%Attention%%'");
	$nbOk=0;
	$nbBad=0;
	foreach($events as $id=>$body)
	{
		if(!preg_match('@^\s*<p\b[^>]*>\s*<strong>Attention\s*(:\s*</strong>|</strong>\s*:)(.*)<hr\b[^>]*>@sU',$body,$m)){$nbBad++;continue;}
		$nbOk++;
		//var_dump('<p>'.trim($m[2]));
		db_query("UPDATE Event SET body='%s' WHERE id=%d", trim(mb_substr($body,mb_strlen($m[0]))),$id);
		db_query("UPDATE Event SET incompleteMessage='%s' WHERE id=%d", '<p>'.trim($m[2]),$id);
	}
	echo "$nbOk $nbBad\n";
}


function demosphere_maintenance_update_place_search_word_freq()
{
	require_once 'demosphere-place-search.php';
	demosphere_place_search_word_freq();
}


function demosphere_maintenance_import_mapshape_directory()
{
	require_once 'demosphere-event-map.php';
	$dir='FIXME';
	$scanRes=scandir($dir);
	foreach($scanRes as $fname)
	{
		if(!preg_match('@[.]kml$@',$fname)){continue;}
		$name=preg_replace('@[.]kml$@','',$fname);
		$exists=demosphere_mapshape_get($name);
		if($exists!==false){fatal('map shape exists:'.$name);}
		$data=file_get_contents($dir.'/'.$fname);
		$coords=demosphere_mapshape_parse_kml_polygon($data);
		if($coords===false){echo "parse failed for $name<br/>\n";continue;}
		$shape=['type'=>'polygon',
				'isPublic'=>true,
				'coordinates'=>$coords,
			   ];
		echo "importing:$name<br/>";
		//var_dump($shape);
		demosphere_mapshape_save($name,$shape);
	}
}

//! Display a list of all documents and images referenced in all Events 
//! FIXME: this list is not complete! Missing: docs in Posts, Place descriptions and comments
function demosphere_maintenance_extract_all_documents()
{
	global $base_url;
	$qres=db_query("SELECT id,title,body FROM Event WHERE status=1;");		
	
	header('Content-Type: text/plain; charset=utf-8');
	while($event=db_fetch_assoc($qres))
	{
		//echo "nid:".$node->nid.":".(memory_get_usage()/1000000)."<br/>";flush();
		preg_match_all('@<(a|img)[^>]*(src|href)="[^"]*/?files/((docs|import-images)/([^"]*))"@',
					   $event['body'],$matches);
		$unique=[];
		foreach($matches[3] as $n=>$fname)
		{
			if(isset($unique[$fname])){continue;}
			$unique[$fname]=true;
			echo $event['id'].';'.$fname.';'.str_replace(';',',',$event['title'])."\n";
		}
	}
}

function demosphere_maintenance_estimate_html_display_width()
{
	global $currentPage;
	require_once 'dlib/html-tools.php';
	$currentPage->addJs('lib/jquery.js');
	$out='';

	$out.='<h2>Letter sizes</h2>';
	$out.='<p>Copy paste into dlib_html_estimate_display_width() definition. Those arent really line-breaks, just wrap artifacts.</p>';
	$out.='<textarea id="letter-size-out" style="width:100%;height: 20em;" ></textarea>';
	$currentPage->addJs('$(function()
{
    var l=$(\'<span style="position: absolute; font-size: 18px;font-weight: bold; padding: 0;"/>\');
    $("body").append(l);
    var s=" !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\\\]^_`abcdefghijklmnopqrstuvwxyz{|}~¡¢£¤¥¦§¨©ª«¬­®¯°±²³´µ¶·¸¹º»¼½¾¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩαβγδεζηθικλμνξοπρστυφχψωΆΈΉΊΌΎΏΐΪΫάέήίΰϊϋόύώώς";
    var res={};
    for(var i=0;i<s.length;i++)
    {
         var c=s[i];l.text("x"+c+"x");
         res[c]=l.width();l.text("xx");
         res[c]-=l.width();
    };
    $("#letter-size-out").val(
      "$letterSizes=json_decode(\'"+
      JSON.stringify(res).replace(/"\'"/,\'"\\\\\\\'"\' ).replace(/"\\\\\\\\"/,\'"\\\\\\\\\\\\\\\\"\')+
      "\',true);");
});
','inline');

	
	//    $("#letter-size-out").text(JSON.stringify(res).replace(\'"\\\\"\',\'"\\\\\\\\"\').replace(\'"\\\'"\',\'"\\\\'"\'));});');

	// Also run the js program displayed by running this program and copy the results below.

	$texts=["Formation syndicale -  agents techniques 92 - éducation - SUD éducation 92",
			"Village - Année Internationale de la Palestine",
			"Stage « Pédagogies alternatives et syndicalisme » -  SUD éducation Créteil",
			"Journée « Stop pub à l' école »",
			"Rassemblement Internationale de Squatteurs à Dublin",
			"Réunion publique unitaire, théâtre - TAFTA accord de libre échange transatlantique",
			"Festival « En Première Ligne » - rapports entre littérature et monde social.",
			"Café-Repaire du 14e de l'émission Là-bas si j'y suis - Les débats de la rentrée !",
			"Jeûne international à Paris Contre les armes nucléaires",
			"Rencontres internationales de jeunes - NPA",
			"Festival « La Belle Rouge » - Compagnie Jolie Môme",
			"Rencontre « Dans les couloirs de la maison d'arrêt de Fresnes »  prisons - Publico",
			"Rassemblement et picnic « Désarmons le 14 juillet... Faisons lui sa fête !  »",
			"Défilé du 14 juillet « La Prophétie Des Clowns »",
			"Défilé du 14 juillet - Brigade des clowns",
			"Bal des Étudiants - Solidarité avec la Palestine",
			"AlterTour 2014 « Travailler ? Pour qui ? PourQuoi ? »",
			"Théâtre « L'an 01 » -  d'après les chroniques de Gébé - Collectif du Geste Gauche",
			"Projection - débat « En la casa, en la cama y la calle » - violence sexuelle, Nicaragua",
			"Projection débat « Les balles du 14 juillet » - manifestants algériens tués à Paris en 1953",
			"Αυτόνομη συνάντηση αγώνα ενάντια στα φράγματα & την εκτροπή του Αχελώου",
			"15ο κάμπινγκ Αγωνιστικών Κινήσεων - Μαθητικής Αντίστασης",
			"Προβολή ταινίας: Καπνός",
			"12ο Αντικαπιταλιστικό Camping",
			"3ημερο φεστιβάλ ελευθερίας",
			"Προβολή ταινίας: Sunshine",
			"Θερινή punk προβολή: Wild Zero",
			"Προβολή ταινίας: Μικρά εγκλήματα μεταξύ φίλων",
			"2ο DIY Kronos Fest",
			"Προβολή ταινίας: Ο υπναράς",
			"Θερινή punk προβολή: The Runaways",
			"Προβολή ταινίας: Η καντίνα",
			"Εκδήλωση αλληλεγγύης στις εξεγερμένες ζαπατιστικές κοινότητες",
			"Hip-hop live οικ. ενίσχυσης του συλλογικού χώρου για την τροφή",
			"Προβολή: Ghost dog, ο τρόπος των σαμουράι",
			"Συγκέντρωση διαμαρτυρίας για ΒΙΟ.ΜΕ",
			"Εκδίκαση της υπόθεσης της δολοφονίας του Νικόλα Τόντι από την αστυνομία",
			"Θερινή punk προβολή: Suburbia",
			"Βίντεο-προβολή: 12 χρόνια Κατάληψη Ευαγγελισμού + γλέντι",
			"Συναυλία :ANTI,ΟΔΟΣ 55,TINKERBELL,OPIUM CURS",
			];

	$out.='<table id="">';
	foreach($texts as $i=>$text)
	{
		$out.='<tr id="row-'.$i.'">';
		$est=dlib_html_estimate_display_width($text);
		$out.='<td class="est">'.$est.'</td>';
		$out.='<td class="jss"></td>';
		$out.='<td class="cmp"></td>';
		$out.='<td class="txt">'.ent($text).'</td>';
		$out.='</tr>';
	}
	$out.='</table>';
	$out.='<p id="res"></p>';

	// Change the style of the first span to find the estimate for your font / size / weight...
	$currentPage->addJs('$(function()
{
    var l=$(\'<span style="position: absolute; font-size: 13px;font-weight: bold; padding: 0;"/>\');
    $("body").append(l);
    var sdev=0;
    var med=[];
    var avg=0;
    $("tr").each(function()
    {
         var c=$(this).find(".txt").text();
         l.text("x"+c+"x");
         var w=l.width();
         l.text("xx");
         w-=l.width();
         var est=parseFloat($(this).find(".est").text());
         var cmp=est/w;
         avg+=cmp;
         sdev+=cmp*cmp;
         med.push(cmp);
         $(this).find(".jss").text(w);
         $(this).find(".cmp").text(cmp);
    });
    avg/=$("tr").length;
    med.sort();
    $("#res").text("avg: "+avg+"  ; med:"+med[Math.floor(med.length/2)]+" ; stddev: "+Math.sqrt(sdev/med.length - avg*avg));
});
','inline');


	return $out;
	
	$js='var s='.dlib_json_encode_esc($texts).';var l=$(\'<span style="position: absolute; font-size: 13px;font-weight: bold; padding: 0;"/>\');$(\'body\').append(l);var res={};for(var i=0;i<s.length;i++){var c=s[i];l.text("x"+c+"x");res[i]=l.width();l.text("xx");res[i]-=l.width();};console.log(JSON.stringify(res));';
	echo $js."\n";
	$jsSizes=json_decode('{"0":465,"1":278,"2":458,"3":192,"4":334,"5":513,"6":459,"7":486,"8":343,"9":268,"10":327,"11":507,"12":461,"13":294,"14":246,"15":289,"16":330,"17":497,"18":523,"19":548,"20":505,"21":390,"22":161,"23":200,"24":177,"25":171,"26":207,"27":307,"28":120,"29":182,"30":237,"31":172,"32":413,"33":411,"34":289,"35":248,"36":489,"37":204,"38":383,"39":328}',true);

	//$jsSizes=json_decode('{"0":438,"1":260,"2":434,"3":179,"4":312,"5":476,"6":429,"7":458,"8":320,"9":252,"10":308,"11":472,"12":436,"13":275,"14":228,"15":273,"16":306,"17":470,"18":496,"19":516,"20":471,"21":364,"22":149,"23":184,"24":164,"25":157,"26":190,"27":287,"28":115,"29":169,"30":221,"31":161,"32":383,"33":375,"34":264,"35":232,"36":448,"37":186,"38":359,"39":323}',true);

	// var_dump($jsSizes);
	$med=[];
	$avg=0;
	foreach($texts as $i=>$text)
	{
		// bold : *.72687224669602243396
		$est=dlib_html_estimate_display_width($text);
		// not bold : *.67992424242424835052
		//$est=dlib_html_estimate_display_width($text);
		$jsSize=$jsSizes[$i];
		$med[]=$est/$jsSize;
		$avg +=$est/$jsSize;
		echo round($est,4).' : '.$jsSize.' : '.round($est/$jsSize,8)."\n";
	}
	sort($med);
	echo "avg:".($avg/count($texts))."\n";
	echo "med:".($med[count($med)/2])."\n";

	return $out;
}

?>