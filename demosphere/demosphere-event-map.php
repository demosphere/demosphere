<?php

function demosphere_get_map_url($place,$event=false)
{
	global $demosphere_config,$base_url;

	$url=$demosphere_config['event_map_link'];
	$url=str_replace('%width'   ,$demosphere_config['event_map_width' ],$url);
	$url=str_replace('%height'  ,$demosphere_config['event_map_height'],$url);
	$url=str_replace('%zoom'    ,$place->zoom						   ,$url);
	$url=str_replace('%lat'		,$place->latitude					   ,$url);
	$url=str_replace('%lng'		,$place->longitude					   ,$url);
	$url=str_replace('%gmap_key',$demosphere_config['gmap_key']        ,$url);
	
	return $url;
}

function demosphere_event_get_map_image($lat,$lng,$zoom)
{
	global $base_url,$demosphere_config;
	$path='files/maps';
	if(!file_exists($path) || !is_writable($path))
	{
		fatal('demosphere_event_get_map_image: destination image path:"'.$path.
			'" does not exist or is not writable');
	}
	$w=$demosphere_config['event_map_width'];
	$h=$demosphere_config['event_map_height'];
	$lat=preg_replace('@[^0-9.-]@','',$lat);// security
	$lng=preg_replace('@[^0-9.-]@','',$lng);// security
	$zoom=intval($zoom);
	$fname='dmap_'.$lat.'_'.$lng.'_'.$zoom.'.png';
	$fullFname=$path.'/'.$fname;
	if(!file_exists($fullFname))
	{
		build_map_image($lat,$lng,$zoom,$w,$h,$fullFname);
	}
	return ['fname'=>$fname,
			'fullFname'=>$fullFname,
			'path'=>$path,
			'url'=>$base_url.'/files/maps/'.$fname,
			'width'=>$w,
			'height'=>$h,
		   ];
	//build_map_image($coordLat,$coordLon,$zoom,500,500);
}

//! This just downloads a map image from google, uing static gmap api.
//! Previously, this used to be a complicated function that 
//! built the map by using gmap tiles.
function build_map_image($lat,$lng,$zoom,$width,$height,$destFname)
{
	global $demosphere_config;
	
	// extra space around image, so that we can crop borders
	$crop=intval($demosphere_config['event_map_crop']);
	$dlWidth=$width+2*$crop;
	$dlHeight=$height+2*$crop;

	$lang=dlib_get_locale_name();
	$region='';
	if(preg_match('@^[a-z]{2}_([A-Z]{2})\.@',setlocale(LC_TIME,"0"),$m))
	{
		$region=$m[1];
	}

	$url=$demosphere_config['event_map_static_url'];
	$url=str_replace('%width'   ,$dlWidth ,$url);
	$url=str_replace('%height'  ,$dlHeight,$url);
	$url=str_replace('%zoom'    ,$zoom    ,$url);
	$url=str_replace('%lat'     ,$lat     ,$url);
	$url=str_replace('%lng'     ,$lng     ,$url);
	$url=str_replace('%gmap_key',$demosphere_config['gmap_key'],$url);
	$url=str_replace('%language',$lang    ,$url);	
	$url=str_replace('%region'  ,$region  ,$url);	

	require_once 'dlib/download-tools.php';
	$ret=dlib_curl_download($url,$destFname,['timeout'=>5,'httpheader'=>['Referer: https://demosphere.eu']]);
	if($ret===false)
	{
		dlib_message_add(t("Event map download failed for:").ent($url),'error');
		if($zoom>18)
		{
			dlib_message_add(t("The zoom you chose for this map is greater that 18. That might be the problem."),'warning');
		}
		return;
	}
	
	// optionally crop map borders
	if($crop)
	{
		dlib_logexec("convert $destFname -crop $width"."x"."$height+$crop+$crop $destFname");
	}

	// optionally add a marker in the middle of the image
	if($demosphere_config['event_map_add_marker'])
	{
		dlib_logexec("composite -geometry +".(round($width/2)-10)."+".(round($height/2)-33).' '.
				'files/maps/marker.png '.$destFname.' '.$destFname);		
	}

	// Try reducing the size of the image using optipng
	// If optipng is not installed, this just fails harmlessly
	dlib_logexec("optipng -quiet -o5 $destFname");
}

//! Refresh old map images and delete those that are no longer used
//! Called from demosphere_place_cron() once a day
function demosphere_event_map_refresh_cron()
{
	$refreshTime=strtotime('2 years ago');
	$filenames=scandir('files/maps');
	shuffle($filenames);// just in case
	$ct=0;
	foreach($filenames as $filename)
	{
		$path='files/maps/'.$filename;
		if(!preg_match('@^dmap_(-?[0-9.]+)_(-?[0-9.]+)_(([0-9]+)).png$@',$filename,$matches)){continue;}
		if(filemtime($path)>$refreshTime){continue;}
		unlink($path);
		$lat=$matches[1];
		$lng=$matches[2];
		$zoom=$matches[3];
		$nbPlaces=db_result('SELECT COUNT(*) FROM Place WHERE ABS(latitude-%f)<.00001 AND ABS(longitude-%f)<.00001 AND zoom=%d',$lat,$lng,$zoom);
		if($nbPlaces>0)
		{
			echo "rebuild map image $lat,$lng,$zoom\n";
			demosphere_event_get_map_image($lat,$lng,$zoom);
		}
		// Don't delete / rebuild more than 40 maps a day
		// 40*100 sites = 4000. Google quota is 25000/day
		// https://developers.google.com/maps/documentation/static-maps/usage-limits
		if(++$ct>=40){break;}
	}
}

/**
 * A map shape is a geographical region.
 *
 * Each shape is defined by :
 * - isPublic : should this map shape be displayed in public predefined shape selectors.
 * - type: currently can only be 'polygon'
 * - coordinates (for polygon): an array of lng,lat numbers
 */
function demosphere_mapshape_get($name)
{
	return variable_get($name,'mapshape',false);
}

function demosphere_mapshape_save($name,$shape)
{
	variable_set($name,'mapshape',$shape);
}

function demosphere_mapshape_get_public()
{
	$mapShapes=variable_get_all('mapshape');
	$res=[];
	//var_dump($mapShapes);
	foreach($mapShapes as $name=>$shape)
	{
		if($shape['isPublic']){$res[]=$name;}
	}
	return $res;
}

function demosphere_mapshape_point_is_inside($shape,$pointLat,$pointLng)
{
	if($shape['type']!=='polygon'){fatal('unsupported mapShape type');}
	// add tiny random displacement as a simple hack to avoid numerical special casses
	$px=$pointLng+ mt_rand(1,10000)/(10000*1000000);
	$py=$pointLat+ mt_rand(1,10000)/(10000*1000000);
	$nbIntersections=0;
	$s0=$shape['coordinates'][count($shape['coordinates'])-1];
	foreach($shape['coordinates'] as $s1)
	{
		//echo 's0:'.$s0[0].','.$s0[1].' s1:'.$s1[0].','.$s1[1].'<br/>';
		if($px>min($s1[0],$s0[0]) &&
		   $px<max($s1[0],$s0[0])    )
		{
			$dsx=$s1[0]-$s0[0];
			$dsy=$s1[1]-$s0[1];
			//echo 'dsx:'.$dsx.'<br/>';
			//echo 'dsy:'.$dsy.'<br/>';
			// line: y=s0y+(s1y-s0y)*(x-s0x)/(s1x-s0x)
			// intersects with x=px & y>py
			// => y=s0y+(s1y-s0y)*(px-s0x)/(s1x-s0x)
			$y=$s0[1]+$dsy*($px-$s0[0])/$dsx;
			//echo 'y intersect:'.$y.'<br/>';
			if($y>$py)
			{
				//echo "intersects!<br/>";
				$nbIntersections++;
			}
		}
		$s0=$s1;
	}
	//echo '$nbIntersections:'.$nbIntersections.'<br/>';
	return ($nbIntersections%2)===1;
}

function demosphere_mapshape_parse_kml_polygon($kml)
{
	$ok=preg_match('@<Polygon>.*<coordinates[^>]*>([^<]*)</coordinates>@s',$kml,$matches);
	if(!$ok){return false;}
	$coordsText=$matches[1];
	$pointsText=preg_split('@[ \n]+@s',trim($coordsText));
	$coords=[];
	foreach($pointsText as $k=>$p)
	{
		if($p===''){continue;}
		$parts=explode(',',$p);
		if(count($parts)<2)
		{
			echo "error parsing kml:$k<br/>\n";
			return false;
		}
		$coords[]=[(double)$parts[0],(double)$parts[1]];
	}
	return $coords;
}

function demosphere_event_mapshape_manage()
{
	global $base_url,$currentPage;
	$currentPage->title=t('Map shapes');
	$mapShapes=variable_get_all('mapshape');
	$out='';
	$out='<h2>'.t('Map shapes:').'</h2>';
	$out.='<table>';
	$out.='<tr>'.
		'<th></th>'.
		'<th>'.t('name'  ).'</th>'.
		'<th>'.t('public').'</th>'.
		'<th>'.t('type'  ).'</th>'.
		'</tr>';
	foreach($mapShapes as $name=>$shape)
	{
		$out.='<tr>';
		$out.='<td><a href="'.$base_url.'/edit-map-shape/'.ent($name).'">'.
			'<img src="" alt="edit"/></a></td>';
		$out.='<td>'.ent($name).'</td>';
		$out.='<td>'.($shape['isPublic'] ? t('yes'):'').'</td>';
		$out.='<td>'.ent($shape['type']).'</td>';
		$out.='<td>'.count(val($shape,'coordinates',[])).' '.t('points').'</td>';
		$out.='</tr>';
	}
	$out.='</table>';
	$out.='<hr/>';


	require_once 'dlib/form.php';
	require_once 'demosphere-event-list-form.php';
	$items=demosphere_event_list_form_map_shape_upload_form();
	$items['map-shape-upload']['title']=t('Create map shape from file');
	$items['submit']['redirect']='manage-map-shapes';
	$out.=form_process($items);
	return $out;
}

function demosphere_event_mapshape_edit_form($name)
{
	global $currentPage;
	$shape=demosphere_mapshape_get($name);
	if($shape===false){dlib_not_found_404('unknown map shape');}
	$currentPage->title=t('Edit map shape');

	$items=[];
	$items['name'    ] = ['type' => 'textfield',
						  'title'=> t('Name'),
						  'default-value'=>$name,];
	$items['isPublic'] = ['type' => 'checkbox',
						  'title'=> t('is public'),
						  'default-value'=>$shape['isPublic'],];
	$items['save'    ] = ['type' => 'submit',
						  'value'=> t('save'),
						  'submit'=>function($items) use ($shape,$name)
		{
			$newName=trim($items['name']['value']);
			// if rename, delete old shape
			if($newName!==$name){variable_delete($name,'mapshape');}
			$shape['isPublic']=$items['isPublic']['value'];
			variable_set($newName,'mapshape',$shape);
		},
						  'redirect'=>'manage-map-shapes',];
	$items['delete'  ] = ['type' => 'submit',
						  'value'=> t('delete'),
						  'submit'=>function()use($name){variable_delete($name,'mapshape');},
						  'redirect'=>'manage-map-shapes',];
	
	require_once 'dlib/form.php';
	return form_process($items);
}

//! Returns full url for google map api, including key, language, region
//! Note: all maps use this key except the frontpage (uses google jsapi : google.load)
//! Note : args is not used anywhere (yet?)
function demosphere_google_map_api_url($args=[])
{
	global $demosphere_config;
	$lang=dlib_get_locale_name();
	// google map region (2 letter country name from locale)
	$region=false;
	if(preg_match('@^[a-z]{2}_([A-Z]{2})\.@',setlocale(LC_TIME,"0"),$m))
	{
		$region=$m[1];
	}
	$defaults=['v'=>3,
			   'key'=>$demosphere_config['gmap_key'],
			   'language'=>$lang,
			   ];
	if($region!==false){$defaults['region']=$region;}
	$args=array_merge($defaults,$args);
	$res='https://maps.googleapis.com/maps/api/js?';
	foreach($args as $name=>$val){$res.=$name.'='.$val.'&';}
	$res=preg_replace('@&$@','',$res);
	return $res;
}

?>