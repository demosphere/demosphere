<strong onclick="$('#pagehtml').toggle()">html (_(click))</strong>
	<?: strlen($page->getRevisionHtml()) ?>
<div id="pagehtml" style="display:none">
	<pre>$page->getRevisionHtml()</pre>
</div>
	
<? // show revisions ?>
<h3>_(revisions):</h3>
<table>
	<tr><th>id</th><th>ts</th><th>keep</th></tr>
	<?foreach($revisions as $revision){?>
		<tr>
			<td>$revision[0]</td>
			<td><a href="<?: dcomponent_safe_domain_url('page-watch/page/'.$page->id.'/display-revision?'.
								 't0='.$revision[1]) ?>">
				$revision[1]</a>
			</td>
			<td>$revision[2]</td>
		</tr>
	<?}?>
</table>
<? // *************** <object> with contents ?>
<object data="<?: dcomponent_safe_domain_url('page-watch/page/'.$page->id.'/display-revision?'.
					't0='.$page->getRevisionTS(time()))?>"
			id="displayPage" 
			type="text/html">
	_(error displaying page iframe)
</object>
