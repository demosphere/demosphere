<?php
/** \addtogroup page_watch
 * @{ */


/**
 * Page represents a web page (url) that we want to monitor for changes.
 */
class Page extends DBObject
{
	//! unique autoincremented ID. 
	public $id=0;
	static $id_           =['type'=>'autoincrement'];
	//! an arbitrary  name for this  page. 
	public $name;
	static $name_         =[];
	//! the URL of this page;
	public $url;
	static $url_          =['type'=>'url'];
	//! If the html file has been downloaded succesfully
	public $downloadStatus;
	static $downloadStatus_=['type'=>'bool'];
	//! a mix of xpath,regexps etc. to select a specific part of the page we
	//! want to montitor (see: select_html_content).
	public $htmlContentSelector;
	static $htmlContentSelector_=['type'=>'array'];
	//! Timestamp of the revision that was last ok'ed for changes.
	public $lastChecked;
	static $lastChecked_  =['type'=>'timestamp'];
	//! When was the html file last downloaded succesfully
	public $lastFetched;
	static $lastFetched_  =['type'=>'timestamp'];

	//! information for database load / save in DBObject.
	static $dboStatic=false;

	function __construct($url)
	{
		$this->url=$url;
		require_once 'dcomponent/dcomponent-common.php';
		$this->name=dcomponent_url_to_name($url,'Page');
		$this->htmlContentSelector=[];
		$this->lastChecked=false;
	}

	//! Destroy  this page  or/and it's  revisions.  
	function delete()
	{
		// delete revisions
		db_query("DELETE FROM page_revision WHERE pageId=%d",$this->id);
		$this->lastChecked=false;
		$this->save();

		parent::delete();
	}

	// accessors
	function getId(){return $this->id;}
	function getName(){return $this->name;}
	function getUrl(){return $this->url;}

	function setLastChecked($ts)
	{
		// two users reading at the same time could (rare) conflict
		if($ts!==false){$ts=max($this->lastChecked,$ts);}
		$this->lastChecked=$ts;
	}

	//! returns  a  list  of  timestamps  of  the  available  revisions
	function getRevisionList()
	{
		$res0=db_one_col("SELECT timestamp FROM page_revision WHERE ".
						  "pageId=%d",$this->id);
		$res=[];
		foreach($res0 as $ts){$res[]=intval($ts);}
		sort($res);
		return $res;
	}

	function getTitle()
	{
		$match=preg_match('@<title>(.*)</title>@isU',
						  $this->getRevisionHtml(),$parts);
		return val($parts,1,t('no title'));
	}
	
	function error($message)
	{
		global $page_watch_config;
		$fullMsg="Page::error for ".$this->id." : '".$this->name."'\n".$message;
		dlib_log($fullMsg,$page_watch_config['log_file']);
		throw new Exception($fullMsg);
	}

	//! Downloads page for current time.  If the page hasn't changed it
	//! is deleted, to save space.
	//! If successful, revision is saved to DB.
	function update()
	{
		echo "** Page::update: ".ent($this->name)." <br/>\n";
		require_once 'dlib/html-tools.php';
		global $page_watch_config;
		$this->downloadStatus=0;
		// also necesary to make sure the $this->id  is ok
		$this->save();
		// create a timestamp that will identify this revision
		$ts=time();
		require_once 'dlib/download-tools.php';
		$html=dlib_download_html_page($this->url,false,['log'=>$page_watch_config['log_file'],
														'headers'=>&$getHeaders,
														'info'=>&$info,
														'timeout'=>300]);
		if($html===false){$this->error("download failed\nLog:\n".$info['log']);}
		if(strlen($html)==0){$this->error("empty html");}

		// Apply preprocessing rules in htmlContentSelector
		$fixCharset=array_filter($this->htmlContentSelector,function($s){return $s['type']=='fixCharset';});
		$fixed=count($fixCharset)>0 ? 'charset='.$fixCharset[0]['arg'] :
			$getHeaders['content-type'];
		$html=dlib_html_convert_page_to_utf8($html,$fixed);
		$html=dlib_html_cleanup_page_with_tidy($html,false);

		if(strlen($html)==0){$this->error('html preproc and cleanup gives empty result');}

		if(count($this->htmlContentSelector))
		{
			//echo "xpath:".$feed->htmlContentSelector."<br/>\n";
			//file_put_contents('/tmp/src.html',$html); // DEBUG
			$html=dlib_html_apply_selectors($html,$this->htmlContentSelector);
			//file_put_contents('/tmp/txpath.html',$this->content);
			//fatal('test xpath');
			if(strlen($html)==0){$this->error('html content select failed');}
		}

		$html=dlib_html_cleanup_page_with_tidy($html);

		// all the download process is ok
		$this->downloadStatus=1;
		$this->lastFetched=$ts;
		$this->save();

		// now use the downloaded html to create (if necesary) a revision
		$oldTs  =$this->getRevisionTS($ts);
		$isVeryFirstRevision=($oldTs===false);
		$oldHtml=$this->getRevisionHtml($oldTs);
		// if no new html, just return, don't save new identical revision
		if($oldHtml===$html){return $oldTs;}

		// only force keep if this is the very first revision for this page.
		$keep=$isVeryFirstRevision;
		// if very first revision : considere this revision was checked
		if($isVeryFirstRevision){$this->lastChecked=$ts;$this->save();}

	   // save new revision to db
		db_query("INSERT INTO page_revision (pageId,timestamp,html,keep) ".
				   "VALUES (%d,%d,'%s',%d)",$this->id,$ts,$html,$keep);

		// delete any previous revision that does not need to be kept
		db_query("DELETE FROM page_revision WHERE ".
				   "pageId=%d AND ".
				   "keep=0 AND ".
				   "timestamp<%d",$this->id,$ts);
		return $ts;
	}

	//! Finds  the timestamp  of the  most  recent version  of the  page
	//! before or at $ts.
	function getRevisionTS($ts0)
	{
		$res=db_result_check("SELECT timestamp FROM page_revision WHERE ".
							   "pageId=%d AND ".
							   "timestamp<=%d ".
							   "ORDER BY timestamp DESC ".
							   "LIMIT 1",$this->id,$ts0);
		if($res!==false){$res=intval($res);}
		return $res;
	}

	//! This revision of this page must be kept for future references.
	//! If this is not done, it might get autodeleted.
	function keepRevision($ts0)
	{
		$ts=$this->getRevisionTS($ts0);
		if($ts===false){return false;}
		db_query("UPDATE page_revision SET keep=1 WHERE pageId=%d AND timestamp=%d",
				   $this->id,$ts);
		return $ts;
	}
	//! Returns the cleaned up html of the page downloaded before or at $ts
	function getRevisionHtml($ts=false)
	{
		if($ts===false){$ts=time();}
		$revision=$this->getRevisionTS($ts);
		if($revision===false){return false;}
		return db_result("SELECT html FROM page_revision WHERE ".
						   "pageId=%d AND timestamp=%d",$this->id,$revision);
	}
	
	//! Highlights all text in lines containing changes, by inserting
	//! open and closing tags. Works on clean (tidied) html.
	function getHighlightedDiffs($t0,$t1)
	{
		require_once 'dlib/html-tools.php';
		$html0=$this->getRevisionHtml($t0);
		$html1 =$this->getRevisionHtml($t1);
		if($html0===false && $html1!==false){return $html1;}
		if($html0!==false && $html1===false){return $html0;}
		if($html0===false || $html1===false){return false;}
		return highlight_html_diffs($html0,$html1);
	}

	// $t0,$t1: reference time and current time for diff
	// $futureDates: a list of extra dates to add,in case they were missed by highlight
	public function viewData($t0,$t1)
	{
		$res['classes']='';

		//************** title
		$res['titleUrl']=dcomponent_safe_domain_url('page-watch/page/'.$this->id.'/display-highlighted-html?'.
													't0='.$t0.'&'.
													't1='.$t1);
		$res['title']=$this->name;
		$res['finishUrl']='/page/'.$this->id.'/finish?t0='.$t0.'&t1='.$t1;

		//************** iframe
		$res['iframeUrl']=dcomponent_safe_domain_url('page-watch/page/'.$this->id.'/display-highlighted-html?'.
													 't0='.$t0.'&'.
													 't1='.$t1);
		
		$res['type']='page';
		$res['isHighlighted']=true;//FIXME
		$res['events']=[];
		$res['textMatching']=false;
		return $res;
	}
	public function getHighlightedHtml($t0,$t1)
	{
		global $base_url;
		//echo "getHighlightedHtml t0:$t0 t1:$t1<br/>";
		$highlightInfo=$this->getHighlightedDiffs($t0,$t1);
		$highlighted=$highlightInfo['content'];
		$cssAndScripts=dcomponent_dcitem_inside_frame_js_and_css('page-watch');

		// hak to disable javascript to avoid frame breakout and other problems
		$highlighted=preg_replace('@(<script[^>]*)text/javascript@','$1disabled',$highlighted);

		if(strpos($highlighted,'</head>')!==false)
		{
			$highlighted=str_replace('</head>',$cssAndScripts.'</head>',$highlighted);
		}
		else
		{
			$tag='KJMKLJIO7698769876';
			$highlighted=preg_replace('@(<body[^>]*>)@i','$1'.$tag,$highlighted);
			$highlighted=str_replace($tag,$cssAndScripts,$highlighted);
		}
		$highlighted=str_replace('<body','<body data-demosphere-src="page-'.$this->id.'" ',$highlighted);
		if($highlighted===false){echo "bad revision timestamp";}
		return $highlighted;
	}

	// Display a table with all available pages.
	static function publicList()
	{
		$out='';
		$out.= '<table id="pages">';
		$out.= '<tr>';
		//$out.= '<th>id</th>';
		$out.= '<th>'.t('name').'</th>';
		$out.= '<th>'.t('url').'</th>';
		$out.= '</tr>';
		
		$pageIds=db_one_col("SELECT id FROM Page");	
		$ct=0;
		rsort($pageIds);
		foreach($pageIds as $id)
		{
			$page=Page::fetch($id);
			$out.= '<tr class="'.((($ct++)%2)?'odd':'even').'">';
			//$out.= '<td>'.$page->id.'</td>';
			$out.= '<td>'.ent($page->name).'</td>';
			$out.= '<td>'; 
			$out.= '<a href="'.ent($page->url).'">'.ent(mb_substr($page->url,0,30)).'</a>';
			$out.= '</td>'; 
			$out.= '</tr>'; 
		}
		$out.= '</table>';
		return $out;
	}

	function cleanupOldRevisions()
	{
		db_query("DELETE FROM page_revision WHERE ".
				   "pageId=%d AND timestamp<%d",$this->id,$this->lastChecked-3600*24);
	}
	// Delete revisions one day older that lastChecked
	// This should be called regularly from a cron job.
	static function cleanupOld()
	{
		echo '<pre>***Page::cleanupOld***</pre>';
		$pageIds=db_one_col("SELECT id FROM Page");
		foreach($pageIds as $id)
		{
			$page=Page::fetch($id);
			$page->cleanupOldRevisions();
		}
	}

	// Update pages. This should be called regularly from a cron job.
	static function updatePages()
	{
		ini_set('max_execution_time', max(1200,ini_get('max_execution_time')));
		echo '<pre>***Page::updatePages***</pre>';
		$pageIds=db_one_col("SELECT id FROM Page");
		foreach($pageIds as $id)
		{
			try
			{
				$page=Page::fetch($id);
				$page->update();
			}catch (Exception $e) 
			 {
				 $page->downloadStatus=0;
				 $page->save();
				 echo 'page update failed: '.$e->getMessage()."\n";
				 continue;
			 }
		}
	}

	static function testPage()
	{
		$type=val($_GET,'type','simple');
		$badChar=val($_GET,'badChar','no');
		if(isset($_GET['ct'])){$ct=intval($_GET['ct']);}else{$ct='none';}
		
		echo '<head>';
		echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
		echo '</head>';
		if($badChar!='no'){echo '<p>badChar:'.iconv("UTF-8",'ISO-8859-1','é')."</p>\n";}
		echo "<p>this is a test page</p>\n";
		echo "<p>count:$ct</p>\n";
		echo "<p>a paragraph</p>\n";
		if($ct>1){echo "<p>added a date:".date('d/m 20:00',time()+3600*24*10)."</p>\n";}
		echo "<p>another paragraph</p>\n";
		echo '<div id="seltest"><p>paragraph in seltest</p></div>'."\n";
	}
	static function createTestScenario()
	{
		require_once 'dlib/testing.php';
		// FIXMEd Pagewatch test page
		$url0='http://demolocal/maintenance/page-watch-test-page?'.
			'type=simple&ct=0&KJ4J24';
		$url1=str_replace('ct=0','ct=1',$url0);
		$p0=new Page($url0);
		$p0->save();
		$p0->update();
		$r=$p0->keepRevision(time());
		$p0->lastChecked=$r;
		$p0->save();
		sleep(1);
		// second version
		$p0->url=$url1;
		$p0->update();
		fatal("created test over");
	}

	static function createTable()
	{
		db_query("DROP TABLE IF EXISTS Page");
		$r=db_query("CREATE TABLE Page (
  `id` int(11) NOT NULL auto_increment,
  `url` text  NOT NULL,
  `name` varchar(250)  NOT NULL,
  `downloadStatus` int(11) NOT NULL,
  `htmlContentSelector` longblob  NOT NULL,
  `lastChecked` int(11) NOT NULL,
  `lastFetched` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `name` (`name`),
  KEY `downloadStatus` (`downloadStatus`),
  KEY `lastChecked` (`lastChecked`),
  KEY `lastFetched` (`lastFetched`)
) DEFAULT CHARSET=utf8mb4;");
	}

}
/** @} */

?>