<?php
/** @defgroup page_watch Page Watch
 *  @{
 */


require_once 'dlib/template.php';

function page_watch_view($page=false,$isItemList=true)
{
	global $base_url,$currentPage;
	require_once 'dcomponent/dcomponent-common.php';	

	$menus=[];
	$menus[]=[['url'=>'page-watch/view-changes','title'=>t('show changes')],
			  ['url'=>'page-watch/page'				,'title'=>t('manage pages')],
			  ['url'=>'page-watch/admin'					    ,'title'=>t('admin')]];

	if($page!==false)
	{
		$menus[]=[['url'=>''                                     ,'title'=>$page->name],
				  ['url'=>'page-watch/page/'.$page->id.''        ,'title'=>t('display')],
				  ['url'=>'page-watch/page/'.$page->id.'/edit'   ,'title'=>t('edit')],
				  ['url'=>'page-watch/page/'.$page->id.'/update' ,'title'=>t('update')],
				  ['url'=>'page-watch/page/'.$page->id.'/configure-content-selector','title'=>t('selector')],
				 ];
	}

	dcomponent_page_css_and_js($isItemList);
	$currentPage->addCssTpl('demosphere/page-watch/page-watch.tpl.css');
	page_watch_view_title($page);
	$currentPage->dcomponentMenubar=
		template_render('dcomponent/dcomponent-menubar.tpl.php',
						['dcMenus'=>$menus]);
	return ['scriptUrl'=>$base_url.'/page-watch'];
}

function page_watch_view_title($page)
{
	global $currentPage;
	$title='pw-';
	if(preg_match('@page-watch(/(page)/\d+)?/([^/?&]+)@',$_GET['q'],$matches))
	{
		$title.=$matches[3];
	}
	if($page!==false){$title.=': '.$page->name;}
	if($title==='pw-'){$title=t('page-watch');}
	$currentPage->title=$title;
}

//! Page list, view & edit use dbobject-ui which is customized here.
function page_watch_page_class_info(&$classInfo)
{
	global $base_url,$currentPage;
	//$currentPage->addCssTpl('demosphere/feed-import/feed-import.tpl.css');

	// ******* common setup

	// ******* list setup
	$classInfo['list_setup']=function(){page_watch_view();};
	// ******* new setup
	// ******* edit form setup
	$classInfo['new']=
		['url'=>['type'=>'url',
				 'title'=>t('Url'),],
		];

	$classInfo['edit_form_alter']=
		function(&$items,&$options,$page)
		{
			page_watch_view($page);
			unset($items['downloadStatus']);
			unset($items['lastChecked']);
			unset($items['lastFetched']);
		};
	// ******* view setup
	$classInfo['view_vars']=
		function($page,$isDelete)
		{
			global $base_url;
			$top='<h1 class="page-title">'.ent($page->name).'</h1>';
			page_watch_view($page);
			return ['topText'=>$top,
					'bottomText'=>page_watch_view_page($page)];
		};

	$classInfo['delete_alter']=
		function(&$items,$page)
		{
			$items['title']['html']='<h3>'.t('Are you sure you want to delete this Page and all of its revisions ?').'</h3>';
			unset($items['ref-warning']);
			$items['delete-revisions']=
			['type'=>'submit',
			 'value'=>t('delete revisions, not page'),
			 'submit'=>function()use($page)
				{
					global $base_url;
					db_query("DELETE FROM page_revision WHERE pageId=%d",$page->id);
					$page->lastChecked=false;
					$page->save();
					dlib_message_add(t('All revisions successfully deleted for page !id',['!id'=>$page->id]));
					dlib_redirect($base_url.'/page-watch/page/'.$page->id);
				}
			];
		};
}

//! This is the main display. It show a list of emails ordered by
//! by urgency. 
function page_watch_view_changes()
{
	global $page_watch_config,$currentPage;
	require_once 'dlib/find-date-time/find-date-time.php';

	// ***** show error notices
	$badPages=Page::fetchList("WHERE downloadStatus=0 AND lastFetched<%d",
							  time()-3600*24);

	// times:
	$t1=time();
	// first choose pages that match and sort the by dates found in text
	$changes=[];
	$pages=Page::fetchList('ORDER BY id ASC');
	foreach($pages as $page)
	{
		//echo "checking for changes:".$page->id."<br/>";
		$pageT0=$page->getRevisionTS($page->lastChecked);
		$pageT1=$page->getRevisionTS($t1);

		if($page->lastChecked!=$pageT0)
		{
			fatal('strange:$page->lastChecked!=$pageT0 : '.
				  $page->lastChecked.'!='.$pageT0.' name='.ent($page->name));
		}

		$highlightInfo=$page->getHighlightedDiffs($pageT0,$pageT1);
		// skip if this page has no changes
		if($highlightInfo['diffCount']==0){continue;}

		// build list of future dates from timestamp list
		$allTimes=$highlightInfo['timestamps'];
		sort($allTimes);
		$ignore=time()+3600*4;
		$futureDates=[];
		foreach($allTimes as $time)
		{
			if($time>$ignore){$futureDates[]=$time;}
		}
			
		$firstFuture=count($futureDates) ? min($futureDates) : 0;
		$sortKey=($firstFuture>0 ? 
				  sprintf('%015d',$firstFuture) : 
				  'x')." : ".$firstFuture." : ".$page->id;
		$changes[$sortKey]=['page'=>$page,'futureDates'=>$futureDates,
							'highlightInfo'=>$highlightInfo, 
							'pageT0'=>$pageT0,'pageT1'=>$pageT1];
	}
	ksort($changes);

	$pager=new Pager(['nbItems'=>count($changes),'defaultItemsPerPage'=>5]);
	$changes=array_slice($changes,$pager->firstItem,$pager->itemsPerPage);

	// The t1 revision of each page must be kept for future referencs.
	// If this is not done, it might get auto-deleted
	foreach($changes as $change)
	{
		$change['page']->keepRevision($change['pageT1']);
	}

	return template_render('page-list.tpl.php',
						   [page_watch_view(),
							compact('changes','pager','badPages'),
						   ]);
}


function page_watch_view_page($page)
{
	$qres=db_query("SELECT id,timestamp,keep FROM page_revision WHERE ".
					 "pageId=%d ORDER BY timestamp ASC",$page->id);

	$revisions=[];
	while($line=mysqli_fetch_array($qres))
	{
		$revisions[]=$line;
	}
	return template_render('page.tpl.php',
						   [page_watch_view($page),
							compact('page','revisions'),
						   ]);	
}


function page_watch_view_page_html_diffs($page,$t0,$t1)
{
	require_once 'dlib/html-tools.php';
	require_once 'dlib/diff.php';
	$html0=$page->getRevisionHtml($t0);
	$html1=$page->getRevisionHtml($t1);
	if($html0===false || $html1===false){echo 'diff failed: missing revision';return;}
	$diffs=unix_diff($html0,$html1);
	//$htmlLines=explode("\n",$html1);
	return template_render('page-html-diffs.tpl.php',
						   [page_watch_view($page),
							compact('page','diffs'),
						   ]);	
}

function page_watch_view_page_watch_admin()
{
	return template_render('page-watch-admin.tpl.php',
						   [page_watch_view()]);	
}

function page_watch_view_html_selector_callback($pageId,$editorHtml)
{
	require_once 'page-watch.php';
	page_watch_view(Page::fetch($pageId));
	return $editorHtml;
}

/** @} */

?>