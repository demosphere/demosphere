<? // ************** info-left ******** ?>
<? begin_part('infoLeftBottom') ?>
	<li class="infoPart alink">
		<a href="$item->url">_(link)</a>
	</li>
	<li class="infoPart revision">
		<a href="<?: dcomponent_safe_domain_url('page-watch/page/'.$item->id.'/display-revision&t0='.$pageT0) ?>">_(old version)</a>
	</li>
	<li class="infoPart revision">
		<a href="<?: dcomponent_safe_domain_url('page-watch/page/'.$item->id.'/display-revision&t0='.$pageT1) ?>">_(new version)</a>
	</li>
	<li class="infoPart pubdate">
		<?: dcomponent_format_date('relative-short',$pageT0) ?>/<?:
			dcomponent_format_date('relative-short',$pageT1) ?>
	</li>
	<li class="infoPart revision">
		<a href="$scriptUrl/page/$item->id/display-html-diffs&t0=$pageT0&t1=$pageT1"
		>_(html-diffs)</a>
	</li>
	<li class="infoPart more-info">
		<a href="$scriptUrl/page/$item->id">+</a>
	</li>
<? end_part('infoLeftBottom') ?>

<? // ************** info-top-line0 ******** ?>
<? begin_part('infoTopLine0After') ?>
	<span class="infoPart ref-demosphere-events">
	</span>
<? end_part('infoTopLine0After') ?>
