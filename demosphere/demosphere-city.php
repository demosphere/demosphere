<?php

//! Setup for class information displayed/edited by dbobject-ui
function demosphere_city_class_info(&$classInfo)
{
	$classInfo['tabs']=function(&$tabs,$object)
		{
			global $base_url,$currentPage;
			$tabs['merge']=['href'=>$base_url.'/city/merge','text'=>t('Merge cities')];
			unset($tabs['add']);
		};
	$classInfo['list_top_text']=
						 '<p>'.t('Cities are handled automatically. Normally, you do not need to use this page.').'</p>'.
						 '<p>'.t('Unused cities are automatically deleted once a day. Unused or unpublished cities are not suggested to users. New cities are automatically created when you type them in a place or event form.').'</p>'.
						 '<p>'.t('If a city has been entered multiple times with different spellings, use "Merge Cities".').'</p>';
	$classInfo['list_columns']=
		function(&$columns)
		{
			$columns['nbEvents']=
			[
			 'title'=>t('nb. Events'),
			 'show'=>function($city,&$show)
					{$show['disp']=db_result('SELECT COUNT(*) FROM Event,Place WHERE Event.placeId=Place.id AND Place.cityId=%d',$city->id);}
			 ];
		};
	// Hide city id=1 (empty)
	$classInfo['list_where']='WHERE id!=1';

	$classInfo['edit_form_alter']=function(&$items,$options,$object)
		{
			$items['name'     ]['validate']=function($v)use($object){if($object->id==1 && $v!==''){return 'City with id=1 is reserved for the empty city.';}};
			$items['shortName']['validate']=$items['name']['validate'];
		};
}

function demosphere_city_merge()
{
	global $currentPage;
	$currentPage->title=t('Merge cities');

	require_once 'dlib/dbobject-ui.php';
	dbobject_ui_tabs('City',null,'merge');

	// **** city autocomplete
	$currentPage->addJs('lib/jquery.js');
	$currentPage->addJs( 'lib/jquery-ui-1.12.1.custom/jquery-ui.js');
	$currentPage->addCss('lib/jquery-ui-1.12.1.custom/jquery-ui.min.css');
	$currentPage->addJs('$(document).ready(function(){
	$("#edit-good-city,#edit-bad-city").autocomplete(
		{
			delay: 100,
			source: base_url+"/city/autocomplete?merge"
		});});','inline');


	// **** simple form
	$formOpts=[];
	$items=[];
	$items['top']=['html'=>'<h2>'.t('Merge cities').'</h2>'.
				   '<p>'.t('"Bad city" will be replaced by "Good city" in every place it is used. After that, "Bad city" will be deleted.').'</p>'.
				   '<p>'.t('"Merge cities" is typically used when a city has been entered more than once with different spellings.').'</p>'.
				   '<p>'.t('Cities are suffixed with :: followed by the city id (Example::123). Do not erase the number.').'</p>'];

	$items[ 'bad-city']=['type'=>'textfield',
						 'title'=>t('Bad city'),
						 'validate-regexp'=>'@::[0-9]+$@',
						 'required'=>true,
						];

	$items['good-city']=['type'=>'textfield',
						 'title'=>t('Good city'),
						 'validate-regexp'=>'@::[0-9]+$@',
						 'required'=>true,
						];

	$items['save']=
		['type'=>'submit',
		 'value'=>t('merge'),
		 'submit'=>function($items)
			{
				$good=$items['good-city']['value'];
				$bad =$items[ 'bad-city']['value'];
				preg_match('@::([0-9]+)$@',$good,$m);
				$goodId=intval($m[1]);
				preg_match('@::([0-9]+)$@',$bad,$m);
				$badId=intval($m[1]);
				if($goodId===$badId){dlib_bad_request_400('Cannot merge identical cities');}
				City::fetch($goodId);
				City::fetch($badId);
				db_query("UPDATE Place SET cityId=%d WHERE cityId=%d",$goodId,$badId);
				City::fetch($badId)->delete();
				dlib_message_add(t('Successfully merged city @bad to @good and deleted bad city @bad.',
								   ['@good'=>$good,'@bad'=>$bad]));
			}
		];

	require_once 'dlib/form.php';
	return form_process($items,$formOpts);
}

/**
 * Suggest cities to a user typing a city name, for example in publish or backend event forms.
 * Typed city is received in $_GET['term']
 */
function demosphere_city_autocomplete()
{
	global $demosphere_config;
	$typed=$_GET['term'];
	$typed=trim($typed);
	if($typed===''){return [];}

	$res=db_one_col("SELECT ".
					(!isset($_GET['merge']) ? 'City.id,City.name' : 
					                          "City.id,CONCAT(City.name,'::',City.id)").
					" FROM City WHERE ".
					"LOWER(name) LIKE LOWER('%%%s%%') ".
					// Only show cities that are in published places
					(!isset($_GET['merge']) ? 
					 " AND EXISTS (SELECT * FROM Event,Place WHERE Place.id=Event.placeId AND Event.status=1 AND Event.status=showOnFrontpage AND Place.cityId=City.id ) "
					 : '').
					// Show most popular cities first (helps avoid alternate spellings).
					'ORDER BY (SELECT COUNT(*) FROM Event,Place WHERE  Event.placeId=Place.id AND Event.status=1 AND Place.cityId=City.id) DESC '.
					'LIMIT 100 ',
					$typed
					);

	return $res;
}

//! Returns short city name for a city.
function demosphere_city_short()
{
	global $demosphere_config;
	$typed=$_GET['name'];
	$typed=trim($typed);

	$res=db_result_check("SELECT shortName FROM City WHERE City.name='%s'",$typed);
	return $res;
}


?>