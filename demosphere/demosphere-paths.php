<?php

//! This function returns a list path descriptions for all paths (urls) for demosphere.
//! These paths can be used by dlib_exec_request_path to actually execute the request.
//! The $requestPath parameter is only used to selectively include paths from other sources (for better performance).
function demosphere_paths($requestPath)
{
	global $user,$base_url,$demosphere_config,$custom_config;

	$options=
		['default_output_render_file'=>'demosphere/demosphere-page.php',
		 'default_output_render'=>'demosphere_page_render',
		];

	$paths = [];

	// **** frontpage related ******** 

	$paths[]=['path'=>'/',
			  'roles' => true,
			  'output'=>'html',
			  'cache'=>true,
			  'file' => 'demosphere-frontpage.php',
			  'function' => 'demosphere_frontpage',
			 ];


	// reload the main calendar via ajax, using options
	$paths[]=['path'=>'frontpage-options-calendar-update',
			  'roles' => true,
			  'output'=>'html',
			  'file' => 'demosphere-frontpage.php',
			  'function' => 'demosphere_frontpage_options_calendar_update',
			 ];

	// used to display near-lat-lng options popup on frontpage via-ajax
	$paths[]=['path'=>'frontpage-options-near-lat-lng',
			  'roles' => true,
			  'output'=>'html',
			  'file' => 'demosphere-frontpage.php',
			  'function' => 'demosphere_frontpage_options_near_lat_lng',
			 ];

	$paths[]=['path'=>'frontpage-options-more-options',
			  'roles' => true,
			  'output'=>'html',
			  'file' => 'demosphere-frontpage.php',
			  'function' => 'demosphere_frontpage_options_more_options',
			 ];

	// used to choose a featured event on frontpage via-ajax
	$paths[]=['path'=>'event-set-featured',
			  'roles' => ['admin','moderator'],
			  'output'=>'json',
			  'file'=>'demosphere-event.php',
			  'function' => 'demosphere_event_set_featured',
			 ];

	$paths[] = 
		['path'=>'@^'.$demosphere_config['frontpage_regions_url_name'].'/((?!(merge|autocomplete)).+)$@',
		 'roles' => true,
		 'output'=>'html',
		 'file' => 'demosphere-frontpage.php',
		 'function' => 'demosphere_frontpage',
		];

	// ****  ******** 

	$paths[]=['path'=>'cron',
			  'roles' => true,
			  'output'=>false,
			  'file' => 'demosphere-cron.php',
			  'function' => 'demosphere_cron',
			 ];

	$paths[]=['path'=>'search',
			  'roles' => true,
			  'file' => 'demosphere-search.php',
			  'function' => 'demosphere_search_page',
			 ];

	$paths[]=['path'=>'search-rebuild-index-form',
			  'roles' => ['admin'],
			  'file' => 'demosphere-search.php',
			  'function' => 'demosphere_search_rebuild_index_form',
			 ];

	// **** Post ******** 

	$paths[]=['path'=>'@^post/([0-9]+)()$@',
			  'access' => 'demosphere_post_access', 
			  'cache'=>true,
			  'file' => 'demosphere-post.php',
			  'function' => 'demosphere_post_view',
			 ];

	$paths[]=['path'=>'@^post/([0-9]+)/(edit)$@',
			  'access' => 'demosphere_post_access', 
			  'file' => 'demosphere-post.php',
			  'function' => 'demosphere_post_edit_form',
			 ];

	$paths[]=['path'=>'post/add',
			  'roles' => ['admin','moderator'], 
			  'file' => 'demosphere-post.php',
			  'function' => 'demosphere_post_edit_form',
			 ];

	$paths[]=['path'=>'@^built-in-pages-diff/([a-z0-9_]+)$@',
			  'roles' => ['admin','moderator'],
			  'file'=>'demosphere-built-in-pages.php',
			  'function' => 'demosphere_built_in_pages_diff',
			 ];

	$paths[]=['path'=>'@^built-in-pages-sync-form/([a-z0-9_]+)@',
			  'roles' => ['admin','moderator'],
			  'file'=>'demosphere-built-in-pages.php',
			  'function' => 'demosphere_built_in_pages_sync_form',
			 ];

	// **** Event ******** 

	$event=$demosphere_config["event_url_name"];

	$paths[]=['path'=>'@^'.$event.'/([0-9]+)()$@',
			  'access' => 'demosphere_event_access', 
			  'cache'=>true,
			  'file' => 'demosphere-event.php',
			  'function' => 'demosphere_event_view',
			 ];

	$paths[]=['path'=>'@^'.$event.'/([0-9]+)/(edit)$@',
			  // access managed in demosphere_event_edit_form() to deal with complex special case (redirect to self-edit group)
			  'roles' => true,
			  'file' => 'demosphere-event-edit-form.php',
			  'function' => 'demosphere_event_edit_form',
			 ];

	$paths[]=['path'=>'@^'.$event.'/([0-9]+)/(delete)$@',
			  'access' => function($euser,$eventId,$op){require_once 'demosphere-event.php';return demosphere_event_access($euser,$eventId,$op);},
			  'file' => 'demosphere-event-edit-form.php',
			  'function' => 'demosphere_event_edit_form_delete',
			 ];

	$paths[]=['path'=>$event.'/add',
			  'roles' => ['admin','moderator','frontpage event creator','private event creator'],
			  'file' => 'demosphere-event-edit-form.php',
			  'function' => 'demosphere_event_edit_form',
			 ];

	// backward compat : v1 path
	$paths[]=['path'=>'@^node/([0-9]+)(/edit)?$@',
			  'roles' => true,
			  'file' => 'demosphere-event.php',
			  'function' => 'demosphere_event_old_url_redirect',
			 ];

	// backward compat  : very old v1 path
	$paths[]=['path'=>'@^archive/([0-9]+)$@',
			  'roles' => true,
			  'file' => 'demosphere-event.php',
			  'function' => 'demosphere_event_old_url_redirect',
			 ];

	$paths[]=['path'=>'self-edit-ajax-update',
			  'output'=>'json',
			  'roles' => ['admin','moderator'],
			  'file' => 'demosphere-self-edit.php',
			  'function' => 'demosphere_self_edit_ajax_update',
			 ];

	// **** Carpool ******** 

	$paths[]=['path'=>'@^'.$event.'/([0-9]+)/carpool()$@',
			  'access' => function($euser,$eventId,$op){require_once 'demosphere-event.php';return demosphere_event_access($euser,$eventId,$op);},
			  'file' => 'demosphere-carpool.php',
			  'function' => 'demosphere_carpool',
			 ];

	$paths[]=['path'=>'@^carpool/add$@',
			  'roles' => true,
			  'file' => 'demosphere-carpool.php',
			  'function' => 'demosphere_carpool_form',
			 ];

	$paths[]=['path'=>'@^carpool/([0-9]+)/edit@',
			  'roles' => true,
			  'file' => 'demosphere-carpool.php',
			  'function' => 'demosphere_carpool_form',
			 ];

	$paths[]=['path'=>'@^carpool/([0-9]+)/contact@',
			  'roles' => true,
			  'file' => 'demosphere-carpool.php',
			  'function' => 'demosphere_carpool_contact',
			 ];

	$paths[]=['path'=>'@^carpool/manage@',
			  'roles' => ['admin','moderator'],
			  'file' => 'demosphere-carpool.php',
			  'function' => 'demosphere_carpool_manage',
			 ];

	// **** 

	$paths[]=['path'=>'publish',
			  'roles' => true,
			  'file' => 'demosphere-event-publish-form.php',
			  'function' =>'demosphere_event_publish_form',
			 ];

	// Save event changes from ajax public form
	$paths[]=['path'=>'event-publish-form-ajax',
			  'roles' => true,
			  'output'=>'json',
			  'file' => 'demosphere-event-publish-form.php',
			  'function' => 'demosphere_event_publish_form_ajax',
			 ];

	$paths[]=['path'=>'event-publish-form-get-change-number',
			  'roles' => true,
			  'output'=>'json',
			  'file' => 'demosphere-event-publish-form.php',
			  'function' => 'demosphere_event_publish_get_change_number',
			 ];

	// Save event changes from ajax public form
	$paths[]=['path'=>'event-publish-form-check-duplicate',
			  'roles' => true,
			  'output'=>'json',
			  'file' => 'demosphere-event-publish-form.php',
			  'function' => 'demosphere_event_publish_form_check_duplicate',
			 ];

	$paths[]=['path'=>'email-subscription-form',
			  'roles' => true,
			  'file' => 'demosphere-email-subscription.php',
			  'function' => 'demosphere_email_subscription_form',
			 ];

	$paths[]=['path'=>'@^unsubscribe/([0-9]+)$@',
			  'roles' => true,
			  'file' => 'demosphere-email-subscription.php',
			  'function' => 'demosphere_email_subscription_unsubscribe',
			 ];

	$paths[]=['path'=>'map',
			  'roles' => true,
			  'file' => 'demosphere-map.php',
			  'function' => 'demosphere_map',
			 ];

	$paths[]=['path'=>'post-on-other-site',
			  'roles' => true,
			  'file' => 'demosphere-event-post-on-other-site.php',
			  'function' => 'demosphere_event_post_on_other_site_form',
			 ];

	// **** map-shapes ******** 

	$paths[]=['path'=>'manage-map-shapes',
			  'roles' => ['admin'],
			  'file'=>'demosphere-event-map.php',
			  'function' => 'demosphere_event_mapshape_manage',
			 ];

	$paths[]=['path'=>'@^edit-map-shape/([^/]+)$@',
			  'roles' => ['admin'],
			  'file'=>'demosphere-event-map.php',
			  'function' => 'demosphere_event_mapshape_edit_form',
			 ];

	$paths[]=['path'=>'upload-map-shape',
			  'roles' => true,
			  'output'=>'html',
			  'file' => 'demosphere-event-list-form.php',
			  'function' => 'demosphere_event_list_form_map_shape_upload_popup',
			 ];

	$paths[]=['path'=>'upload-map-shape-finished',
			  'roles' => true,
			  'output'=>false,
			  'file' => 'demosphere-event-list-form.php',
			  'function' => 'demosphere_event_list_form_map_shape_upload_finished',
			 ];


	// **** widgets ******** 

	$paths[]=['path'=>'widget-config-ajax',
			  'roles' => true,
			  'output'=>'json',
			  'file' => 'demosphere-widget.php',
			  'function' => 'demosphere_widget_config_ajax',
			 ];

	$paths[]=['path'=>'widget-json',
			  'roles' => true,
			  'output'=>'json',
			  'file' => 'demosphere-widget.php',
			  'function' => 'demosphere_widget_json',
			 ];

	$paths[]=['path'=>'widget-html',
			  'roles' => true,
			  'output'=>'html',
			  'cache'=>true,
			  'file' => 'demosphere-widget.php',
			  'function' => 'demosphere_widget_html',
			 ];

	$paths[]=['path'=>'widget-js',
			  'roles' => true,
			  'output'=>'custom',
			  'cache'=>true,
			  'file' => 'demosphere-widget.php',
			  'function' => 'demosphere_widget_js',
			 ];

	$paths[]=['path'=>'widget-config',
			  'roles' => true,
			  'file' => 'demosphere-widget.php',
			  'function' => 'demosphere_widget_config_form',
			 ];

	$paths[]=['path'=>'widget-select-events-calendar',
			  'roles' => true,
			  'output'=>'html',
			  'file' => 'demosphere-widget.php',
			  'function' => 'demosphere_widget_select_events_calendar',
			 ];

	// **** user calendar ******** 

	$paths[]=['path'=>'@^(perso|usercal)/([0-9]+)$@',
			  'roles' => true,
			  'output'=>'html',
			  'file' => 'demosphere-frontpage.php',
			  'function' => 'demosphere_frontpage',
			 ];			

	$paths[]=['path'=>'user-calendar-config',
			  'roles' => true,
			  'file' => 'demosphere-user-calendar.php',
			  'function' => 'demosphere_user_calendar_config_form',
			 ];

	$paths[]=['path'=>'user-calendar-config-ajax',
			  'roles' => true,
			  'output'=>'json',
			  'file' => 'demosphere-user-calendar.php',
			  'function' => 'demosphere_user_calendar_config_ajax',
			 ];

	$paths[]=['path'=>'user-calendar-select',
			  'roles' => true,
			  'output'=>false,
			  'file' => 'demosphere-user-calendar.php',
			  'function' => 'demosphere_user_calendar_select',
			 ];

	$paths[]=['path'=>'user-calendar-external-event',
			  'roles' => true,
			  'output'=>false,
			  'file' => 'demosphere-user-calendar.php',
			  'function' => 'demosphere_user_calendar_external_event',
			 ];

	// **** Webextension (Firefox & Chrome extension)  ******** 

	$paths[]=['path'=>'webextension/create-event',
			  'output'=>'json',
			  'roles' => ['admin','moderator'],
			  'file' => 'demosphere-event-import.php',
			  'function' => 'demosphere_webextension_create_event',
			 ];

	$paths[]=['path'=>'webextension/selection-cleanup',
			  'output'=>'html',
			  'roles' => ['admin','moderator'],
			  'file' => 'demosphere-event-import.php',
			  'function' => 'demosphere_webextension_selection_cleanup',
			 ];

	$paths[]=['path'=>'webextension/highlight',
			  'roles' => ['admin','moderator'],
			  'output'=>'html',
			  'file' => 'demosphere-event-import.php',
			  'function' => 'demosphere_webextension_highlight',
			 ];

	// **** tmpdoc  ******** 

	$paths[]=['path'=>'@^tmpdoc/([0-9]+)$@',
			  'roles' => ['admin','moderator'],
			  'file' => 'demosphere-event-import.php',
			  'function' => 'demosphere_tmpdoc',
			 ];

	$paths[]=['path'=>'tmpdoc/add',
			  'roles' => ['admin','moderator'],
			  'output'=>'json',
			  'file' => 'demosphere-event-import.php',
			  'function' => 'demosphere_tmpdoc_add',
			 ];

	// **** html edit (tinymce)  ******** 

	$paths[]=['path'=>'htmledit/tidy',
			  'roles' => true,
			  'output'=>'html',
			  'file' => 'htmledit/demosphere-htmledit.php',
			  'function' => 'demosphere_htmledit_ajax_tidy',
			 ];
	$paths[]=['path'=>'htmledit/linecleanup',
			  'roles' => true,
			  'output'=>'html',
			  'file' => 'htmledit/demosphere-htmledit.php',
			  'function' => 'demosphere_htmledit_ajax_linecleanup',
			 ];
	$paths[]=['path'=>'htmledit/linewrapcleanup',
			  'roles' => true,
			  'output'=>'html',
			  'file' => 'htmledit/demosphere-htmledit.php',
			  'function' => 'demosphere_htmledit_ajax_linewrapcleanup',
			 ];
	$paths[]=['path'=>'htmledit/listcleanup',
			  'roles' => true,
			  'output'=>'html',
			  'file' => 'htmledit/demosphere-htmledit.php',
			  'function' => 'demosphere_htmledit_ajax_listcleanup',
			 ];

	$paths[]=['path'=>'dtoken-list-json',
			  'roles' => ['admin','moderator'],
			  'output'=>'json',
			  'file' => 'demosphere-dtoken.php',
			  'function' => 'demosphere_dtoken_list_json',
			 ];

	$paths[]=['path'=>'@^event-hdiff-save/([0-9]+)$@',
			  'roles' => ['admin','moderator'],
			  'output'=>'json',
			  'file' => 'demosphere-event-revisions.php',
			  'function' => 'demosphere_event_hdiff_save',
			 ];

	$paths[]=['path'=>'@^post-hdiff-save/([0-9]+)$@',
			  'roles' => ['admin','moderator'],
			  'output'=>'json',
			  'file' => 'demosphere-post.php',
			  'function' => 'demosphere_post_hdiff_save',
			 ];

	// **** multiple edit 

	$paths[]=['path'=>'pending-events',
			  'roles' => ['admin','moderator'],
			  'file'=>'demosphere-event-multiple-edit.php',
			  'function' => 'demosphere_pending_events',
			 ];

	// Save event changes from multiple edit display
	$paths[]=['path'=>'event-multiple-edit-ajax',
			  'roles' => ['admin','moderator'],
			  'output'=>'json',
			  'file' => 'demosphere-event-multiple-edit.php',
			  'function' => 'demosphere_event_multiple_edit_ajax',
			 ];

	// Ajax client requests any events that have changed
	$paths[]=['path'=>'event-multiple-edit-ajax-get-changes',
			  'roles' => ['admin','moderator'],
			  'output'=>'json',
			  'file' => 'demosphere-event-multiple-edit.php',
			  'function' => 'demosphere_event_multiple_edit_ajax_get_changes',
			 ];


	// **** other ajax

	$paths[]=['path'=>'topics-list-order',
			  'roles' => ['admin'],
			  'output'=>false,
			  'file' => 'demosphere-topics.php',
			  'function' => 'demosphere_topics_list_order',
			 ];

	$paths[]=['path'=>'topics-json',
			  'roles' => true,
			  'output'=>'json',
			  'file' => 'demosphere-topics.php',
			  'function' => 'demosphere_topics_json',
			 ];

	$paths[]=['path'=>'term-autocomplete',
			  'roles' => true,
			  'output'=>'json',
			  'file' => 'demosphere-terms.php',
			  'function' => 'demosphere_terms_autocomplete',
			 ];

	$paths[]=['path'=>'help-json',
			  'roles' => true,
			  'output'=>'json',
			  'file' => 'demosphere-help.php',
			  'function' => 'demosphere_help_json',
			 ];

	$paths[]=['path'=>'built-in-pages-json',
			  'roles' => true,
			  'output'=>'json',
			  'file' => 'demosphere-built-in-pages.php',
			  'function' => 'demosphere_built_in_pages_json',
			 ];

	$paths[]=['path'=>'remote-translation',
			  'roles' => true,
			  'output'=>false,
			  'file' => 'demosphere-misc.php',
			  'function' => 'demosphere_remote_translation',
			 ];

	$paths[]=['path'=>'event-import-src-ajax',
			  'roles' => ['admin','moderator'],
			  'output'=>'json',
			  'file' => 'demosphere-event-import.php',
			  'function' => 'demosphere_event_import_src_ajax',
			 ];


	// called from js and webextension
	$paths[]=['path'=>'ajax-events-near-date',
			  'roles' => true,
			  'output' => 'html',
			  'file' => 'demosphere-event-near-date.php',
			  'function' => 'demosphere_event_near_date',
			 ];

	$paths[]=['path'=>'event-list-json',
			  'roles' => true,
			  'output'=>'json',
			  'cache'=>true,
			  'file' => 'demosphere-event-list.php',
			  'function' => 'demosphere_event_list_json',
			 ];

	$paths[]=['path'=>'site-info',
			  'roles' => true,
			  'output'=>'json',
			  'file' => 'demosphere-external-events.php',
			  'function' => 'demosphere_site_info',
			 ];

	$paths[]=['path'=>'websockets-token',
			  'output'=>false,
			  'roles' => true,
			  'file' => 'demosphere-misc.php',
			  'function' => 'demosphere_websockets_token',
			 ];

	$paths[]=['path'=>'websockets-browser-auth',
			  'output'=>'json',
			  'roles' =>['admin','moderator'],
			  'file' => 'demosphere-misc.php',
			  'function' => 'demosphere_websockets_browser_auth',
			 ];

	$paths[]=['path'=>'ajax-event-admin-menubar',
			  'roles' =>['admin','moderator'],
			  'output'=>false,
			  'file' => 'demosphere-common.php',
			  'function' => function(){echo demosphere_event_admin_menubar(val($_POST,'where'));},
			 ];

	$paths[]=['path'=>'remote-auth-check',
			  'roles' => true,
			  'output'=>'json',
			  'file' => 'demosphere-misc.php',
			  'function' => 'demosphere_remote_auth_check',
			 ];

	$paths[]=['path'=>'remote-auth-redirect',
			  'roles' => true,
			  'output'=>'html',
			  'file' => 'demosphere-misc.php',
			  'function' => 'demosphere_remote_auth_redirect',
			 ];

	$paths[]=['path'=>'public-key',
			  'roles' => true,
			  'output'=> false,
			  'file' => 'demosphere-misc.php',
			  'function' => 'demosphere_public_key',
			 ];


	$paths[]=['path'=>'check-admin',
			  'roles' => true,
			  'output'=> false,
			  'function' => function(){global $user;header('Content-type: text/plain');echo $user->checkRoles('admin') ? 'yes' : 'no';},
			 ];
	// ** other

	$paths[]=['path'=>'email-aliases',
			  'roles' => ['admin'],
			  'file'=> 'demosphere-emails.php',
			  'function' => 'demosphere_emails_hosted_form',
			 ];

	$paths[]=['path'=>'control-panel',
			  'roles' => ['admin','moderator'],
			  'file'=>'demosphere-panel.php',
			  'function' => 'demosphere_control_panel',
			 ];

	$paths[]=['path'=>'control-panel/ajax-save',
			  'roles' => ['admin','moderator'],
			  'output' => false,
			  'file'=>'demosphere-panel.php',
			  'function' => 'demosphere_panel_ajax_save',
			 ];

	$paths[]=['path'=>'control-panel/reset-preferences',
			  'roles' => ['admin','moderator'],
			  'output' => false,
			  'file'=>'demosphere-panel.php',
			  'function' => 'demosphere_panel_reset_preferences',
			 ];


	$paths[]=['path'=>'@^safe-domain/@',
			  'roles' => true,
			  'output'=>false,
			  'file' => 'demosphere-safe-domain.php',
			  'function' => 'demosphere_safe_domain',
			 ];

	$paths[]=['path'=>'opinions',
			  'roles' => ['admin','moderator'],
			  'file'=>'demosphere-opinion.php',
			  'function' => 'demosphere_opinions',
			 ];

	$paths[]=['path'=>'@^files/images/proxy/([0-9a-f]{1,20}\.[a-z]{1,5})@',
			  'roles' =>true,
			  'output'=>false,
			  'file' => 'demosphere-misc.php',
			  'function' => 'demosphere_image_proxy_view',
			 ];

	$paths[]=['path'=>'image-proxy-ajax-remote-url-encode',
			  'output'=>'json',
			  'roles' => ['admin','moderator'],
			  'file' => 'demosphere-misc.php',
			  'function' => 'demosphere_image_proxy_ajax_remote_url_encode',
			 ];

	$paths[]=['path'=>'@^safe-domain-proxy/([0-9a-f]+-[0-9a-f]+)$@',
			  'roles' => true, 
			  'output'=>false,
			  'file' => 'demosphere-safe-domain.php',
			  'function' => 'demosphere_safe_domain_proxy_view',
			 ];
	// ****** file browser (css manager, image manager,...)  *******

	$paths[]=['path'=>'@^browse/([^/]+)$@',
			  'roles' => ['admin'],
			  'file'=>'demosphere-file-browser.php',
			  'function' => 'demosphere_file_browser',
			 ];

	$paths[]=['path'=>'@^browse/([^/]+)/delete$@',
			  'roles' => ['admin'],
			  'file'=>'demosphere-file-browser.php',
			  'function' => 'demosphere_file_browser_delete',
			 ];

	$paths[]=['path'=>'@^browse/([^/]+)/edit@',
			  'roles' => ['admin'],
			  'file'=>'demosphere-file-browser.php',
			  'function' => 'demosphere_file_browser_edit',
			 ];

	// ****** feeds: sitemap, rss, ical...  *******

	$paths[]=['path'=>'sitemap.xml',
			  'roles' => true,
			  'output'=>false,
			  'file'=>'demosphere-feeds.php',
			  'function' => 'demosphere_feeds_sitemap',
			 ];

	$paths[]=['path'=>'events.xml',
			  'roles' => true,
			  'output'=>'custom',
			  'cache'=>true,
			  'file'=>'demosphere-feeds.php',
			  'function' => 'demosphere_feeds_rss',
			 ];
	$paths[]=['path'=>'events.ics',
			  'roles' => true,
			  'output'=>'custom',
			  'cache'=>true,
			  'file'=>'demosphere-feeds.php',
			  'function' => 'demosphere_feeds_icalendar',
			 ];

	// ****** demosphere-config  *******

	$paths[]=['path'=>'cache-clear',
			  'roles' => ['admin'],
			  'file' => 'demosphere-common.php',
			  'function' => 'demosphere_cache_clear',
			 ];

	$paths[]=['path'=>'cache-clear-slow',
			  'roles' => ['admin'],
			  'file' => 'demosphere-common.php',
			  'function' => 'demosphere_cache_clear_slow_confirm',
			 ];

	$paths[]=['path'=>'selenium-testing',
			  'roles' => true,
			  'output'=>false,
			  'file' => 'test/test-demosphere-selenium.php',
			  'function' => 'demosphere_selenium_testing',
			 ];

	// ****** demosphere-config  *******

	$paths[]=['path'=>'@^configure/?((?<=/).*)?$@',
			  'roles' => ['admin'],
			  'file' => 'demosphere-config-form.php',
			  'function' => 'demosphere_config_form',
			 ];

	$paths[]=['path'=>'config-check',
			  'file' => 'demosphere-config-check.php',
			  'function' => 'demosphere_config_check_page',
			  'roles' => ['admin'],
			 ];

	// ****** event page related (map, send email, ...) *******

	$paths[]=['path'=>'@^'.$event.'/([0-9]+)/send-by-email$@',
			  'roles' => true,
			  'file'=>'demosphere-event-send-by-email.php',
			  'function' => 'demosphere_event_send_by_email_form',
			 ];

	$paths[]=['path'=>'@^'.$event.'/([0-9]+)/ical.ics$@',
			  'roles' => true,
			  'output'=>false,
			  'file'=>'demosphere-event.php',
			  'function' => 'demosphere_event_ical',
			 ];

	// ****** event revisions *******

	$paths[]=['path'=>'@^'.$event.'/([0-9]+)/revisions$@',
			  'roles' => ['admin','moderator'],
			  'file' => 'demosphere-event-revisions.php',
			  'function' => 'demosphere_event_revisions',
			 ];

	$paths[]=['path'=>'@^event-revision/([0-9]+)$@',
			  'roles' => ['admin','moderator'],
			  'file' => 'demosphere-event-revisions.php',
			  'function' => 'demosphere_event_revisions_view',
			 ];

	$paths[]=['path'=>'@^event-revision-diff/([0-9]+)@',
			  'roles' => ['admin','moderator'],
			  'file' => 'demosphere-event-revisions.php',
			  'function' => 'demosphere_event_revisions_hdiff',
			 ];

	$paths[]=['path'=>'@^event-revision-restore/([0-9]+)@',
			  'roles' => ['admin','moderator'],
			  'file' => 'demosphere-event-revisions.php',
			  'function' => 'demosphere_event_revisions_restore',
			 ];

	// ****** event repetitions *******

	$paths[]=['path'=>'@^'.$event.'/([0-9]+)/repetition$@',
			  'roles' => ['admin','moderator'],
			  'file'=>'demosphere-event-repetition.php',
			  'function' => 'demosphere_event_repetition_form',
			 ];

	$paths[]=['path'=>'@^'.$event.'/([0-9]+)/repetition-copy$@',
			  'roles' => true,
			  'file'=>'demosphere-event-repetition.php',
			  'function' => 'demosphere_event_repetition_copy_form',
			 ];

	$paths[]=['path'=>'@^repetition-group/([0-9]+)/create-auto$@',
			  'roles' => ['admin','moderator'],
			  'output'=>'html',
			  'file'=>'demosphere-event-repetition.php',
			  'function' => 'demosphere_event_repetition_group_create_auto',
			 ];

	$paths[]=['path'=>'repetition-group',
			  'roles' => ['admin','moderator'],
			  'file'=>'demosphere-event-repetition.php',
			  'function' => 'demosphere_event_repetition_groups',
			 ];

	$paths[]=['path'=>'@^repetition-group/([0-9]+)/self-edit$@',
			  'roles' => true,
			  'file'=>'demosphere-event-repetition.php',
			  'function' => 'demosphere_event_repetition_group_self_edit',
			 ];

	$paths[]=['path'=>'@^repetition-group/([0-9]+)/confirm$@',
			  'roles' => true,
			  'file'=>'demosphere-event-repetition.php',
			  'function' => 'demosphere_event_repetition_group_confirm',
			 ];

	// ****** place *******

	$place=$demosphere_config['place_url_name'];

	$paths[] = 
		['path'=>'@^'.$place.'/([0-9]+)$@',
		 'roles' => true,
		 'cache'=>true,
		 'file'=>'demosphere-place.php',
		 'function' => 'demosphere_place_view',
		];

	$paths[] = 
		['path'=>'@^'.$place.'/([0-9]+)/edit$@',
		 'access'=>'demosphere_place_form_access',
		 'file'=>'demosphere-place-form.php',
		 'function' => 'demosphere_place_form',
		];

	$paths[] = 
		[
			'path'=>'@^'.$place.'/(\d+)/merge$@',
			'roles' => ['admin','moderator'],
			'file' => 'demosphere-place.php',
			'function' => 'demosphere_place_merge',
		];

	$paths[] = 
		[
			'path'=>'@^'.$place.'/(\d+)/variants$@',
			'roles' => ['admin','moderator'],
			'file' => 'demosphere-place.php',
			'function' => 'demosphere_place_variants',
		];

	$paths[] = 
		[
			'path'=>'@^'.$place.'/(\d+)/search-variants$@',
			'roles' => true,
			'output'=>'json',
			'file' => 'demosphere-place-search.php',
			'function' => 'demosphere_place_search_variants',
		];

	$paths[] = 
		[
			'path'=>'@^'.$place.'/(\d+)/json$@',
			'roles' => true,
			'output'=>'json',
			'file' => 'demosphere-place.php',
			'function' => 'demosphere_place_json',
		];

	$paths[] = 
		[
			'path'=>'place/by-proximity',
			'roles' => ['admin','moderator'],
			'file' => 'demosphere-place.php',
			'function' => 'demosphere_place_by_proximity',
		];

	$paths[] = 
		['path'=>$demosphere_config['places_url_name'],
		 'roles' => true,
		 'file'=> 'demosphere-place.php',
		 'function' => 'demosphere_places',
		];			

	$paths[]=['path'=>'suggest-date-time-place',
			  'roles' => true,
			  'output'=>'json',
			  'file'=>'demosphere-event.php',
			  'function' => 'demosphere_event_suggest_date_time_place',
			 ];

	$paths[]=['path'=>'@^'.$place.'/([0-9]+)/in-charge-changed-ok$@',
			  'roles' => ['admin','moderator'],
			  'output'=>'json',
			  'file'=>'demosphere-place-form.php',
			  'function' => 'demosphere_place_in_charge_changed_ok',
			 ];

	// ****** place search *******

	$paths[]=['path'=>'variants-selector',
			  'roles' => true,
			  'output'=>false,
			  'file' => 'demosphere-place-search.php',
			  'function' => 'demosphere_place_search_variants_selector_ajax',
			 ];
	$paths[]=['path'=>'place-search-autocomplete',
			  'roles' => true,
			  'output'=>'json',
			  'file' => 'demosphere-place-search.php',
			  'function' => 'demosphere_place_search_autocomplete',
			 ];

	// ****** city *******

	$paths[]=['path'=>'city/autocomplete',
			  'roles' => true,
			  'output'=>'json',
			  'file' => 'demosphere-city.php',
			  'function' => 'demosphere_city_autocomplete',
			 ];

	$paths[]=['path'=>'city/merge',
			  'roles' => ['admin','moderator'],
			  'file' => 'demosphere-city.php',
			  'function' => 'demosphere_city_merge',
			 ];

	$paths[]=['path'=>'city/short',
			  'roles' => true,
			  'output'=>'json',
			  'file' => 'demosphere-city.php',
			  'function' => 'demosphere_city_short',
			 ];

	// ****** user *******

	$paths[]=['path'=>'login',
			  'roles' => true,
			  'file' => 'demosphere-user.php',
			  'function' => 'demosphere_user_login_form',
			 ];

	$paths[]=['path'=>'logout',
			  'roles' => true,
			  'file' => 'demosphere-user.php',
			  'function' => 'demosphere_user_logout',
			 ];


	$paths[]=['path'=>'user/autocomplete',
			  'roles' => ['admin','moderator'],
			  'output'=>'json',
			  'file' => 'demosphere-user.php',
			  'function' => 'demosphere_user_autocomplete',
			 ];

	$paths[]=['path'=>'user/forgot-password',
			  'roles' => true,
			  'file' => 'demosphere-user.php',
			  'function' => 'demosphere_user_forgot_password',
			 ];

	$paths[]=['path'=>'@^user/(\d+)/forgot-password$@',
			  'roles' => true,
			  'file' => 'demosphere-user.php',
			  'function' => 'demosphere_user_forgot_password_login',
			 ];

	$paths[]=['path'=>'@^user/(\d+)$@',
			  'access' => function($euser,$uid){return $euser->id==$uid || $euser->checkRoles('admin');},
			  'file' => 'demosphere-user.php',
			  'function' => 'demosphere_user_view',
			 ];

	$paths[]=['path'=>'@^user/(\d+)/edit$@',
			  'access' => function($euser,$uid){return $euser->id==$uid || $euser->checkRoles('admin');},
			  'file' => 'demosphere-user.php',
			  'function' => 'demosphere_user_edit',
			 ];

	$paths[]=['path'=>'@^user/(\d+)/opinions@',
			  'roles' => true,
			  'file' => 'demosphere-opinion.php',
			  'function' => 'demosphere_opinions_of_user',
			 ];

	$paths[]=['path'=>'@^user/(\d+)/opinion-options@',
			  'roles' => true,
			  'file' => 'demosphere-opinion.php',
			  'function' => 'demosphere_opinion_options',
			 ];

	$paths[]=['path'=>'@^user/(\d+)/emails-received-options@',
			  'roles' => ['admin','moderator'],
			  'file' => 'demosphere-emails.php',
			  'function' => 'demosphere_emails_received_options',
			 ];

	$paths[]=['path'=>'@^user/(\d+)/email-verify@',
			  'roles' => true,
			  'file' => 'demosphere-user.php',
			  'function' => 'demosphere_user_email_verify',
			 ];

	$paths[]=['path'=>'stats/users',
			  'roles' => ['admin'],
			  'file'=>'demosphere-stats.php',
			  'function' => 'demosphere_stats_users',
			 ];

	$paths[]=['path'=>'stats/daily-visits',
			  'roles' => true,
			  'output'=>'json',
			  'file'=>'demosphere-stats.php',
			  'function' => 'demosphere_stats_daily_visits_fetch',
			 ];

	$paths[]=['path'=>'stats/widget',
			  'roles' => ['admin','moderator'],
			  'file'=>'demosphere-stats.php',
			  'function' => 'demosphere_stats_widget',
			 ];

	$paths[]=['path'=>'stats/email-bounces',
			  'roles' => ['admin'],
			  'file'=>'demosphere-stats.php',
			  'function' => 'demosphere_stats_email_bounces',
			 ];

	if($requestPath===false || strpos($requestPath,'translation-backend')===0)
	{
		require_once 'dlib/translation-backend.php';
		translation_backend_add_paths($paths,$options);
	}
	if($requestPath===false || 
	   strpos($requestPath,'backend')===0 ||
	   strpos($requestPath,'feed-import')===0 ||
	   strpos($requestPath,'mail-import')===0 ||
	   strpos($requestPath,'page-watch')===0 
	   )
	{
		require_once 'dlib/dbobject-ui.php';
		dbobject_ui_add_paths($paths,$options);
		$paths['dbobject-ui-list-post']['roles'][]='moderator';
		require_once 'dlib/dbtable-ui.php';
		dbtable_ui_add_paths($paths,$options);
	}

	if($requestPath===false || 
	   strpos($requestPath,'feed-import')===0)
	{
		require_once 'feed-import/feed-import.php';
		feed_import_add_paths($paths,$options);
	}

	if($requestPath===false || 
	   strpos($requestPath,'mail-import')===0)
	{
		require_once 'mail-import/mail-import.php';
		mail_import_add_paths($paths,$options);
	}

	if($requestPath===false || 
	   strpos($requestPath,'page-watch')===0)
	{
		require_once 'page-watch/page-watch.php';
		page_watch_add_paths($paths,$options);
	}

	if($requestPath===false || 
	   strpos($requestPath,'html-selector')===0)
	{
		require_once 'html-selector/html-selector.php';
		html_selector_add_paths($paths,$options);
	}

	if($requestPath===false || 
	   strpos($requestPath,'text-matching')===0)
	{
		require_once 'text-matching/text-matching.php';
		text_matching_add_paths($paths,$options);
	}

	if($requestPath===false || 
	   strpos($requestPath,'comment/')===0)
	{
		require_once 'dlib/comments/comments.php';
		comments_add_paths($paths,$options);
	}

	if($requestPath===false || 
	   strpos($requestPath,'docconvert/')===0)
	{
		require_once 'docconvert/docconvert.php';
		docconvert_add_paths($paths,$options);
	}

	if($requestPath===false || 
	   strpos($requestPath,'maintenance/')===0)
	{
		require_once 'demosphere-maintenance.php';
		demosphere_maintenance_add_paths($paths,$options);
	}

	if(isset($custom_config['add_paths']))
	{
		$custom_config['add_paths']($paths,$options);
	}

	return [$paths,$options];
}



