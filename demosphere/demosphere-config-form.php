<?php

/**
 * Form builder for demosphere configuration form.
 *
 * Builds the form that is displayed to the user using the standard drupal
 * form building procedure. 
 * This function is called directly from drupal menu system (via drupal_get_form).
 * 
 * @see demosphere_config_form_validate()
 * @see demosphere_config_form_submit()
 * @ingroup forms
 */
function demosphere_config_form($section=false)
{
	global $base_url,$currentPage;

	if($section===''){$section=false;}
	$formOpts=[];

	// A desciption of all sections.
	// This is shown both on the main demosphere config page and at the begining 
	// of each section in the forms.
	$sections=
		['all'          =>['title'=>t('All options'),
						   'description'=>t('Show all options on a single page.'),],
		 'misc-config'  =>['title'=>t('Misc. configuration'),
						   'description'=>t('All options that do not enter in other categories.'),],
		 'frontpage'    =>['title'=>t('Front page'),
						   'description'=>t('Configuration for the site\'s front page.'),],
		 'graphics'     =>['title'=>t('Graphics and colors'),
						   'description'=>t('Configure the colors, images, and graphics used on your site. Customize !linkCss CSS here</a>. Customize !linkImages images, such as logo, here</a>',['!linkCss'=>'<a href="'.$base_url.'/browse/css">','!linkImages'=>'<a href="'.$base_url.'/browse/images">']),],
		 'backend-form' =>['title'=>t('Back-end event form'),
						   'description'=>t('Configuration of the form used by moderators to edit events.'),],
		 'maps'         =>['title'=>t('Maps'),
						   'description'=>t('Configure the large interactive map (with events on it), and other maps, like those that appear on event pages.'),],
		 'date-time'    =>['title'=>t('Date and time'),
						   'description'=>t('Configure how date and time should be managed and displayed. This is where you set the language you want to use for date/time.'),],
		 'pages'        =>['title'=>t('Built-in pages'),
						   'description'=>t('Pages such as the "about us" page, the help pages, help data, etc. These pages are created when your site is installed. You can choose to keep them up to date (synced) from a reference site.'),],
		 'emails'      =>['title'=>t('Emails'),
						  'description'=>t('Configure emails addresses for your site and also emails that you send from your site (you can also manage your <a href="!url">addresses / aliases here</a>).',['!url'=>$base_url.'/email-aliases']),],
		 'mail-import'        =>['title'=>t('Mail import'),
								 'description'=>t('Configure the interface that helps you find email containing events and import them into Demosphere.'),],
		 'feed-import'        =>['title'=>t('Feed import'),
								 'description'=>t('Configure the interface that helps you find RSS feed articles containing events and import them into Demosphere.'),],
		 'page-watch'        =>['title'=>t('Page watch'),
								'description'=>t('Configure the interface that helps you find events in web pages.'),],
		 'text-matching'      =>['title'=>t('Text matching'),
								 'description'=>t('Configure the system that helps you find duplicate information among already entered events, feed articles and received emails.'),],
		];

	// A description for each "level"
	$levels=
		[0=>['title'=>'',
			 'description'=>'',],
		 1=>['title'=>t('Advanced option'),
			 'description'=>'',],
		 2=>['title'=>t('Expert option : '),
			 'description'=>t('Only change this if you really know what you are doing.'),],
		];

	$currentPage->addJs('lib/jquery.js'  );
	$currentPage->addJs('demosphere/js/demosphere-config-form.js'  );
	$currentPage->addCssTpl('demosphere/css/demosphere-config-form.tpl.css');
	$currentPage->addJsTranslations(
		['colors-changed-warning'=>t('You have requested a change of colors on your site. All the graphics on your site will be recomputed. This will take around a minute. Please be patient and do not navigate on your site until it has finished. Once it has finished, you might also need to refresh / reload pages in your browser pages to see the results.'),]);

	// **** This is not really a form, it is the main demosphere config page, with 
	// links to each form
	if($section===false)
	{
		$currentPage->title=t('Configure Demosphere');
		$out='';
		$out.='<h1 class="page-title">'.t('Demosphere configuration').'</h1>';
		$out.='<p>'.t('Demosphere has many options, please choose one of the categories below. Advanced and expert level options will appear in a different color. Only change those if you know what you are doing.').'</p>';
		$out.='<ul>';
		foreach($sections as $name=>$section)
		{
			$out.='<li><strong><a href="'.$base_url.'/configure/'.$name.'">'.
				$section['title'].'</a></strong><br/>'.
				$section['description'].'</li>';
		}
		$out.='</ul>';
		return $out;
	}
	else
	{
		if(!isset($sections[$section])){dlib_not_found_404('invalid section');}
		$currentPage->bodyClasses[]='unPaddedContent';
		$currentPage->title=t('Configure: ').$sections[$section]['title'];
	}

	// **** choose which items to add to this form and sort them using several criteria
	// (sort by level, by section, and by order defined in description)
	$desc=demosphere_config_description();
	$items=[];
	$sort1=[];
	$sort2=[];
	$sort3=[];
	foreach($desc as $name=>$item)
	{
		if($section!==false && $section!=='all' && $item[':section']!==$section){continue;}
		$pos=count($items);
		$items[$name]=$item;
		$sort1[$pos]=$item[':level'];
		$sort2[$pos]=array_search($item[':section'],array_keys($sections));
		$sort3[$pos]=$pos;
	}
	array_multisort($sort1,SORT_ASC,$sort2,SORT_ASC,$sort3,SORT_ASC,$items);

	// **** build form for each item in description
	$form=[];
	// a list of items present in this form is stored for the validate & submit handlers
	$form['items']=['type'=>'data','data'=>array_keys($items)];
	$form['main-config-menu-link']=['html'=>'<p id="main-config-menu-link"><a href="'.$base_url.'/configure">'.t('return to main configuration menu.').'</a></p>'];
	$form['submit-reminder']=['html'=>'<p id="submit-reminder">'.t("Don't forget to press the submit button at the bottom of this page after any changes.").'</p>'];
	$ct=0;
	foreach($items as $name=>$item)
	{
		// **** display section headers
		if(!isset($prevSection) || $item[':section']!==$prevSection)
		{
			$out='<div class="section-heading">';
			if(isset($prevSection)){$out.='<hr/>';}
			$t=$sections[$item[':section']]['title'];
			if($t!==false){$out.='<h2>'.$t.'</h2>';}
			$t=$sections[$item[':section']]['description'];
			if($t!==false){$out.='<p>'.$t.'</p>';}
			$t=val($sections[$item[':section']],'more');
			if($t!==false){$out.='<p>'.$t.'</p>';}
			$out.='</div>';
			$form['section-'.$item[':section'].'-'.($ct++)]=['html'=>$out];
			$prevSection=$item[':section'];
		}
		// **** create the form item from the description item
		$form[$name]=$item;
		$f=&$form[$name]; // shortcut

		// dlib sets default length to 128. Too small.
		if(($f['type']==='textfield' || $f['type']==='url'))
		{
			$f['attributes']=val($f,'attributes',[]);
			if(!isset($f['attributes']['maxlength'])){$f['attributes']['maxlength']=10000;}
		}

		$f['prefix']=
			val($f,'prefix','').
			'<div class="form-item-wrapper level-'.$item[':level'].' '.$name.' item-type-'.$item['type'].'">'.
			'<a class="item-anchor" name="'.ent($name).'" href="'.$base_url.'/configure/'.$section.'#'.ent($name).'">⇒</a>'.
			'<div class="level-message"><span>'.
			ent($levels[$item[':level']]['title']).'</span>'.
			ent($levels[$item[':level']]['description']).'</div>';
		
		// **** compute html init display, from the actual init value
		$init=demosphere_config_get_init($item);
		$initHtml=false;
		//echo $name;var_dump($init);
		if($init!==null)
		{
			if(isset($item[':init_html'])){$initHtml=$item[':init_html']($init,$item);}
			else
			if(isset($item[':cfg_to_form_val'])){$initHtml=ent($item[':cfg_to_form_val']($init,$item));}
			else
			if($item['type']==='checkbox'){$initHtml=$init ? t('yes') : t('no');}
			else
			if($item['type']==='array_of_arrays'){$initHtml=demosphere_config_form_render_array($init);}
			else
			if(is_int($init) || is_float($init) || is_string($init)){$initHtml=ent($init);}
			else
			if($init===false){$initHtml='';}
			else
			{
				fatal('unknown default init value type for :'.$name.' : '.gettype($init));
			}

			if(is_array($initHtml)){echo "Bug: ".ent($name);fatal("argh: bug, please report");}
		}

		// **** add right column with example and default values
		$f['suffix']=
			'<div class="right-column">'.
			(!isset($item[':example']) ? '': 
			 '<div class="example"      ><strong>'.t('Example : ').'</strong> '.
			 $item[':example'].'</div>').
			($init===null             ? '': 
			 '<div class="default-value"><strong>'.t('Default : ').'</strong> '.
			 (strlen($initHtml)===0 ? t('[empty]') : $initHtml).'</div>').
			'</div>'.
			'</div>'.
			val($f,'suffix','').
			"\n";

		// **** pre fill-in the fields (default_value) from values in current config
		global $demosphere_config;		
		if(isset($demosphere_config[$name])){$f['default-value']=$demosphere_config[$name];}
		else{$f['default-value']=$init;}
		foreach(['mail_import','feed_import', 'page_watch','text_matching'] as $dcomp)
		{
			$config=$dcomp.'_config';
			global $$config;
			$conf=$$config;
			if(preg_match('@^'.$dcomp.'_(.*)$@',$name,$m) && isset($conf[$m[1]]))
			{$f['default-value']=$conf[$m[1]];}
		}

		// **** call hook for this item
		if(isset($item[':cfg_to_form_val']))
		{
			$f['default-value']=$item[':cfg_to_form_val']($f['default-value'],$item);
		}
	}

	// submit button
	$form['submit'] = [
		'type' => 'submit',
		'value' => t('Save'),
					  ];
	$formOpts=['submit'=>'demosphere_config_form_submit','id'=>'configure-form'];
	require_once 'dlib/form.php';
	return form_process($form,$formOpts);
}

function demosphere_config_form_render_array($array)
{
	$allIntKeys=count(array_filter(array_keys($array),'is_int'))===count($array);
	$res='<table class="default-array"><tr>';
	if(!$allIntKeys){$res.='<th></th>';}
	foreach($array as $line)
	{
		foreach($line as $k=>$dud){$res.='<th>'.ent($k).'</th>';}
        break;
	}
	$res.='</tr>';
	foreach($array as $k=>$line)
	{
		$res.='<tr>';
		if(!$allIntKeys){$res.='<th>'.ent($k).'</th>';}
		foreach($line as $dud=>$v){$res.='<td>'.ent($v).'</td>';}
		$res.='</tr>';
	}
	$res.='</table>';
	if(count($array)===0){$res=t('[empty]');}
	return $res;
}


/**
 * Main submit handler called when the whole configuration form is submited.
 *
 * This function calls optional hooks for item types and also for specific items.
 */
function demosphere_config_form_submit(&$form)
{
	global $demosphere_config;

	$desc=demosphere_config_description();
	$values=dlib_array_column($form,'value');
	$res=[];
	foreach($form['items']['data'] as $name)
	{
		$item=$desc[$name];
		$value=val($values,$name);

		if($value===''){$value=false;}// by default empty fields are false 

		// password fields are emptied, so don't reset the field if its empty 
		if($item['type']==='password' && $value===false){continue;}

		// copy the submited value to the result
		$res[$name]=$value;

		if(isset($item[':cfg_from_form_val']))
		{
			$res[$name]=$item[':cfg_from_form_val']($res[$name],$item);
		}
	}

	foreach($form as $name=>$item)
	{
		if(isset($item[':cfg_postprocess'])){$item[':cfg_postprocess']($res,$name,$item);}
	}

	// Now actually write the submitted values into the $demosphere_config array 
	// and save the variable
	foreach($res as $k=>$v){$demosphere_config[$k]=$v;}
	variable_set('demosphere_config','',$demosphere_config);
	dlib_message_add(t('Configuration successfully updated.'));

	foreach($form as $name=>$item)
	{
		if(isset($item[':cfg_postsave'])){$item[':cfg_postsave']($name,$item);}
	}

	demosphere_page_cache_clear_all();
}


// ******* word_list

function demosphere_config_form_word_list_to_form($v)
{
	return implode(",",$v);
}
function demosphere_config_form_word_list_from_form($v)
{
	if(trim($v)===''){return [];}
	return explode(',',trim($v));
}


// ******* duration

function demosphere_config_form_duration_to_form($v,$item)
{
	$bases=['days'=>(3600*24),'hours'=>3600];
	return round($v/$bases[$item[':duration_unit']]);
}
function demosphere_config_form_duration_from_form($v,$item)
{
	$bases=['days'=>(3600*24),'hours'=>3600];
	return $v*$bases[$item[':duration_unit']];
}

// ******* page path

function demosphere_config_form_page_path_validate($url)
{
	require_once 'dlib/download-tools.php';
	global $base_url;

	if(!preg_match('@^https?://@',$url)){$url=$base_url.'/'.$url;}

	$headers=dlib_curl_get_header($url);
	$urlOk=preg_match('@^[^ ]* [12]@',$headers['response']);

	if(!$urlOk)
	{
		return t('this does not seem to be a valid path or url (you should create it first): '.ent($url));
	}
	return true;
}

// ******* predefined emails cleanup

function demosphere_config_form_submit_emails_from_form($value)
{
	foreach($value as &$email)
	{
		$email['body']=preg_replace('@(\r\n|\r|\n)@',"\r\n",$email['body']);
	}
	return $value;
}

// ***********************************

/**
 * Returns initialisation value for a given configuration item.
 *
 * This is used both in the configuration form and in the installation profile.
 * 
 * It chooses from several values using options listed in demosphere_config_init_options 
 */
function demosphere_config_get_init($item)
{
	global $demosphere_config;
	if(!isset($item[':init'])){return null;} 
	$init=$item[':init'];
	$res=null;
	global $language;
	$options=[];
	if($demosphere_config['hosted']){$options[]='hosted';}
	$options[]='language_'.dlib_get_locale_name($demosphere_config['locale']);
	$options[]='default';

	foreach($options as $p)
	{
		if(isset($init[$p])){$res=$init[$p];break;}
	}

	return $res;
}


//! Call this from demosphere.install when you add or remove items to demosphere config.
function demosphere_config_update()
{
	global $base_url,$demosphere_config;

	$configDesc=demosphere_config_description();
	foreach(array_keys($demosphere_config) as $k)
	{
		// this is a list of entries in demosphere_config which are added dynamically (not meant to be stored)
		// We can ignore them, if they are stored.
		if(in_array($k,['tmp_dir','is_backup_site','css_override_path','image_override_path','debug','site_id','secret','file_browser'])){continue;}

		if(!isset($configDesc[$k]))
		{
			echo "found unknown (deprecated?) key in demosphere_config:$k<br/>\n";
			var_dump($demosphere_config[$k]);
		}
	}

	foreach($configDesc as $k=>$desc)
	{
		if(!isset($demosphere_config[$k]))
		{
			echo "found new key in desc:$k initializing<br/>\n";
			var_dump(demosphere_config_get_init($desc));
			$demosphere_config[$k]=demosphere_config_get_init($desc);
		}
	}
	variable_set('demosphere_config','',$demosphere_config);
}

// ***********************************
// ***********************************

// empty function : for no we don't want to translate config titles and descriptions
// (too much work) and it polutes the .po files.
function ct()
{
	$args=func_get_args();
	return call_user_func_array('t',$args);
}

function demosphere_config_color_presets()
{
	return
		[
		 'brown'=>
		 [
		  'n'=>0,
		  'color_palette_fg'    =>'#994433',
		  'color_palette_bg'    =>'#ddb58d',
		  'color_palette_bg2'   =>'#f3dec8',
		  'color_palette_flashy'=>'#aadd66',
		  ],
		 'dark-violet'=>
		 [
		  'n'=>1,
		  'color_palette_fg' =>'#672178',
		  'color_palette_bg' =>'#b2b2b2',
		  'color_palette_bg2'=>'#efe1f5',
		  'color_palette_flashy'=>'#aadd66',
		  ],
		 'yellow'=>
		 [
		  'n'=>3,
		  'color_palette_fg' =>'#994433',
		  'color_palette_bg' =>'#fecf76',
		  'color_palette_bg2'=>'#f3dec8',
		  'color_palette_flashy'=>'#aadd66',
		  ],
		 'all-green'=>
		 [
		  'n'=>4,
		  'color_palette_fg' =>'#678514',
		  'color_palette_bg' =>'#cedea9',
		  'color_palette_bg2'=>'#f7f3d8',
		  'color_palette_flashy'=>'#aadd66',
		  ],
		 'green'=>
		 [
		  'n'=>7,
		  'color_palette_fg' =>'#458000',
		  'color_palette_bg' =>'#eeeeee',
		  'color_palette_bg2'=>'#e7efdd',
		  'color_palette_flashy'=>'#aadd66',
		  ],
		 'violet'=>
		 [
		  'n'=>8,
		  'color_palette_fg' =>'#672178',
		  'color_palette_bg' =>'#eeeeee',
		  'color_palette_bg2'=>'#efe1f5',
		  'color_palette_flashy'=>'#aadd66',
		  ],
		 'light-brown'=>
		 [
		  'n'=>13,
		  'color_palette_fg' =>'#b27900',
		  'color_palette_bg' =>'#fffdd6',
		  'color_palette_bg2'=>'#f6f0e2',
		  'color_palette_flashy'=>'#aadd66',
		  ],
		 'orange-white'=>
		 [
		  'n'=>14,
		  'color_palette_fg' =>'#ea7509',
		  'color_palette_bg' =>'#ffffff',
		  'color_palette_bg2'=>'#fdedde',
		  'color_palette_flashy'=>'#aadd66',
		  ],
		 'light-blue'=>
		 [
		  'n'=>15,
		  'color_palette_fg' =>'#007fff',
		  'color_palette_bg' =>'#dff2ff',
		  'color_palette_bg2'=>'#e6f2ff',
		  'color_palette_flashy'=>'#aadd66',
		  ],
		 'light-red'=>
		 [
		  'n'=>16,
		  'color_palette_fg' =>'#932626',
		  'color_palette_bg' =>'#eeeeee',
		  'color_palette_bg2'=>'#f2e1e1',
		  'color_palette_flashy'=>'#aadd66',
		  ],
		 'dark-orange'=>
		 [
		  'n'=>17,
		  'color_palette_fg' =>'#ff6600',
		  'color_palette_bg' =>'#b2b2b2',
		  'color_palette_bg2'=>'#ffebde',
		  'color_palette_flashy'=>'#aadd66',
		  ],
		 'gray-white'=>
		 [
		  'n'=>18,
		  'color_palette_fg' =>'#353939',
		  'color_palette_bg' =>'#ffffff',
		  'color_palette_bg2'=>'#e6e4e4',
		  'color_palette_flashy'=>'#aadd66',
		  ],
		 ];

}

//! Update aliases file when changing contact or moderators email addresses
function demosphere_config_moderator_emails_postsave($name,$item)
{
	require_once 'demosphere-emails.php';
	if(demosphere_emails_hosted_domain()===false){return;}

	$oldAddress=$item['default-value'];
	$newAddress=$item['value'];
	if($newAddress===$oldAddress){return;}

	// Rename contact email in aliases
	if($name==='contact_email')
	{
		$aliases=demosphere_emails_hosted_fetch_data()['aliases'];
		if(isset($aliases[$oldAddress]))
		{
			$aliases[$newAddress]=$aliases[$oldAddress];
			unset($aliases[$oldAddress]);
			$ok=demosphere_emails_hosted_write_data(['aliases'=>$aliases]);
			if($ok!==true)
			{
				dlib_message_add('Failed to delete old alias: '.ent(val($ok,'error','').val($ok,'fatal','')),'error');
				return false;
			}
			else
			{
				dlib_message_add('Also updated contact in aliases.');				
			}
		}
	}

	// ***** Then update alias file (necessary?)
	demosphere_emails_received_update_aliases();
}

/**
 * Returns a list of all demosphere configuration items.
 *
 * Each item is similar to a standard Drupal form item, but has extra fields
 * that describe other aspects used by both the configuration form and the 
 * installation profile.
 */
function demosphere_config_description()
{
	global $base_url,$demosphere_config;
	$res=[];

	$siteId  =$demosphere_config['site_id'];
	$siteName=$demosphere_config['site_name'];

	$base_path   =preg_replace('@^https?://[^/]*@','',$base_url).'/';
	require_once 'demosphere-emails.php';
	$emailDomain=demosphere_emails_hosted_domain();
	
	$locale=dlib_get_locale_name();

	$res['std_base_url']=
		['type'=>'url',
		 ':section'=>'misc-config',
		 ':level'=>1,
		 'required'=>true,
		 'title'=>ct('The url of your site'),
		 'description'=>ct('The url you would normally use for your site. This is set automatically during install. You do not need to change it unless you move your site to a new url.'),
		 ':example'=>'https://paris.demosphere.net',
		];
	$res['safe_base_url']=
		[':init'=>['hosted'=>'https://safe.'.$siteId.'.demosphere.net'], 
		 'type'=>'url',
		 ':section'=>'misc-config',
		 ':level'=>2,
		 'required'=>true,
		 'title'=>ct('Safe url'),
		 'description'=>ct('An alternate domain URL for your site. It is used for viewing untrusted html. This should be different than the main URL, for security reasons (XSS). This can just be an alias pointing to the same site, but with a different domain name (see, for example the Apache ServerAlias directive).'),
		 ':example'=>'https://safe.paris.demosphere.net',
		];

	$res['debug_rule']=
		[':init'=>['default'=>'off',],
		 'type'=>'textfield',
		 ':section'=>'misc-config',
		 ':level'=>1,
		 'title'=>'Debug rule',
		 'description'=>ct('When to display all PHP errors and warnings. Do not enable it on a public site! (it can compromise the security of your site). Possible values: off,on,uid=123,localhost-test'),
		];

	$res['log_slow_queries']=
		[':init'=>['default'=>false,],
		 'type'=>'float',
		 ':section'=>'misc-config',
		 ':level'=>2,
		 'title'=>'Log slow queries',
		 'description'=>ct('Write information about slow queries (http and sql) in files/private/slow.log file. This field is the min number of seconds (floating point) of the query to get logged (For example: .1 = 100ms).  Leave empty to disable logging. '),
		 ':example'=>'.1',
		];


	$res['site_name']=
		[':init'=>['default'=>false,],
		 'type'=>'textfield',
		 ':section'=>'misc-config',
		 ':level'=>0,
		 'title'=>ct('Site name'),
		 'validate'=>function($v){if(strlen($v)>24){return 'Too long' ;}},
		 'description'=>ct('This should be very short. It is used in many places inside texts and emails.'),
		 ':example'=>'Demosphere',
		];

	$res['big_site_name']=
		[':init'=>['default'=>'démosphère',],
		 'type'=>'textfield',
		 ':section'=>'misc-config',
		 ':level'=>0,
		 'title'=>ct('Big site name'),
		 'description'=>ct('The big name displayed under the logo. This should be *very* short (around 10 letters). It can be the same as site name.'),
		 ':example'=>'demosphere',
		];

	$res['hosted']=
		[':init'=>['default'=>false,],
		 'type'=>'checkbox',
		 ':section'=>'misc-config',
		 ':level'=>2,
		 'title'=>'hosted Demosphere server',
		 'description'=>ct('Is this site hosted on the main Demosphere server? This determines the default values for certain configuration options.'),
		];

	$res['primary_links']=
		[':init'=>['default'=>[['title'=>t('About us')        ,'url'=>$base_path.t('about-us')],
							   ['title'=>t('Publish an event'),'url'=>$base_path.'publish']],],
		 'type'=>'array_of_arrays',
		 'aofa-options'=>[['title',t('title')],
						  ['url',t('url')],
						 ],
		 ':section'=>'misc-config',
		 ':level'=>0,
		 'title'=>'Primary links / menu',
		 'description'=>ct('Big menu links shown on the top right of all pages (other than frontpage).'),
		];

	$res['secondary_links']=
		[':init'=>['default'=>
				   [
					   [
						   'url'=>'/event/add',
						   'title'=>t('Create event'),
						   'where'=>'not:none',
						   'id'=>false,
						   'class'=>false,
					   ],
					   [
						   'url'=>'/user/[userid]',
						   'title'=>t('My profile'),
						   'where'=>'loggedin',
						   'id'=>false,
						   'class'=>false,
					   ],
					   [
						   'url'=>'/logout',
						   'title'=>t('Logout'),
						   'where'=>'loggedin',
						   'id'=>false,
						   'class'=>false,
					   ],
					   [
						   'url'=>'/login',
						   'title'=>t('Login'),
						   'where'=>'frontpage,not:loggedin',
						   'id'=>false,
						   'class'=>'login-link',
					   ],
					   [
						   'url'=>'?switchMobile=mobile',
						   'title'=>t('mobile'),
						   'where'=>'frontpage',
						   'id'=>'back-to-mobile',
						   'class'=>false,
					   ]
				   ],
				  ],
		 'type'=>'array_of_arrays',
		 'aofa-options'=>[['title',ct('title')],
						  ['url'  ,ct('url'  )],
						  ['where',ct('where')],
						  ['id'   ,'id'       ],
						  ['class','class'    ],
						 ],
		 ':section'=>'misc-config',
		 ':level'=>0,
		 'title'=>'Secondary links / menu',
		 'description'=>ct('Small menu links shown on the top right of all pages. "where" defines when this link is displayed. It can be a role, "loggedin", "not:loggedin", "frontpage","not:frontpage". Possible roles are : @roles. The role "none" is a loggedin user that has no roles. Roles can be negated (example: "not:admin"). If any role (normal or negated) is specified, the link is only shown to loggedin users. Note: admins and moderators have a simplified page-top on many pages. These secondary_links are not shown on those pages.',['@roles'=>implode(', ',User::$role_['values'])]),
		];

	$res['extra_body_class']=
		[':init'=>['default'=>false,],
		 'type'=>'textfield',
		 ':section'=>'misc-config',
		 'validate-regexp'=>'@^[a-z0-9_ -]+$@i',
		 'validate-regexp-message'=>ent(ct('must be letters and/or numbers')),
		 ':level'=>1,
		 'title'=>ct('Extra body class'),
		 'description'=>ct('Class name that will be added to HTML &lt;body&gt; tag.'),
		];

	$res['no_wget_sites']=
		[':init'=>['default'=>[],],
		 'type'=>'textarea',
		 ':cfg_to_form_val'  =>function($v){return implode("\n",$v);},
		 ':cfg_from_form_val'=>function($v){return trim($v)==='' ? [] : preg_split('@(\r\n|\r|\n)@',trim($v));},
		 ':section'=>'misc-config',
		 ':level'=>1,
		 'title'=>ct('Sites that block wget'),
		 'description'=>ct('A list of URLs that block wget downloads. Demosphere will use curl instead. Enter one item per line.'),
		 ':example'=>'<br/>http://abc.com<br/>http://xyz.org',
		];

	$res['no_ssl_cert_sites']=
		[':init'=>['default'=>[],],
		 'type'=>'textarea',
		 ':cfg_to_form_val'  =>function($v){return implode("\n",$v);},
		 ':cfg_from_form_val'=>function($v){return trim($v)==='' ? [] : preg_split('@(\r\n|\r|\n)@',trim($v));},
		 ':section'=>'misc-config',
		 ':level'=>1,
		 'title'=>ct('Sites with invalid ssl certificates'),
		 'description'=>ct('A list of URLs of sites where we should ignore invlaid ssl certificates. Enter one item per line.'),
		 ':example'=>'<br/>https://abc.com<br/>https://xyz.org',
		];

	$res['public_form_open_publishing']=
		[':init'=>['default'=>false,],
		 'type'=>'checkbox',
		 ':section'=>'misc-config',
		 ':level'=>0,
		 'title'=>ct('Open publishing from public form '),
		 'description'=>ct('If you enable this, any anonymous user will be able to publish events.'),
		];

	$res['show_event_publish_donate_message']=
		[':init'=>['default'=>'never',],
		 'type'=>'select',
		 ':section'=>'misc-config',
		 'options'=>['never'=>ct('never'),
					 'once'=>ct('once'),
					 'debug'=>ct('debug'),],
		 ':level'=>0,
		 'title'=>ct('Show event publish donate message'),
		 'description'=>ct('If you choose "once" a popup message will suggest a donation when visitor tries to publish an event. The message will only be shown once to a visitor. If you choose "debug" the message is shown on every page load of the publish form (do NOT use this on a production site, as it will be extremely annoying). You can change the message by editing an article called "donate_message" !link here</a>.',['!link'=>'<a href="'.$base_url.'/backend/post">']),
		];

	$res['public_form_ask_price']=
		[':init'=>['default'=>false,],
		 'type'=>'checkbox',
		 ':section'=>'misc-config',
		 ':level'=>0,
		 'title'=>ct('Public form price field'),
		 'description'=>ct('If you enable this, a price field will be shown on the public form.'),
		];

	$res['public_form_self_edit_link']=
		[':init'=>['default'=>false,],
		 'type'=>'checkbox',
		 ':section'=>'misc-config',
		 ':level'=>0,
		 'title'=>ct('Public form self edit'),
		 'description'=>ct('If you enable this, a user that gives his email in the public form will get a self edit link that allows him to edit the event before and after it is published.'),
		];

	$res['enable_opinions']=
		[':init'=>['default'=>false,],
		 'type'=>'checkbox',
		 ':section'=>'misc-config',
		 ':level'=>0,
		 'title'=>ct('Enable opinions'),
		 'description'=>ct('Participative moderation. If you enable this, certain users will be able to give opinions and rate un-published events.'),
		];

	$res['opinions_pub_guidelines']=
		[':init'=>['default'=>
				   "1.0 Example ... The event must have political contents
example second line
example third line
---
2.1 Example ... The event must be open to anybody
---
2.2 Example ... The cost of the event should be less than 7€.
---
3.1 Example ... The event should be participatory
",],
		 'type'=>'textarea',
		 ':section'=>'misc-config',
		 ':level'=>0,
		 'title'=>ct('Publishing guidelines for opinions'),
		 'description'=>ct('Participative moderation. A list of guidelines that contributors can reference when they give an opinion. Guidelines are separated by a line that contains only "---".'),
		];

	$res['opinions_disagree_weight']=
		[':init'=>['default'=>13,],
		 'type'=>'float',
		 ':section'=>'misc-config',
		 ':level'=>1,
		 'title'=>ct('Opinions disagree weight'),
		 'description'=>ct('Participative moderation. How much weight will be given to the bad reputation that would come when a user rates an event at -1.5 and a moderator rates it at +1. Higher values will lead to lower users reputations.'),
		];

	$res['opinions_auto_publish']=
		[':init'=>['default'=>1.2,],
		 'type'=>'float',
		 ':section'=>'misc-config',
		 ':level'=>0,
		 'title'=>ct('Opinions auto publish threshold'),
		 'description'=>ct('Total reputation of users giving positive ratings to an event, needed to automatically publish it. A very good contributor has a reputation close to 1. So "1.2" means "at least one good contributor and one less reliable one". Leave it blank if you do not want auto-publishing.'),
		];

	$res['opinions_reputation_buildup']=
		[':init'=>['default'=>60,],
		 'type'=>'float',
		 ':section'=>'misc-config',
		 ':level'=>0,
		 'title'=>ct('Opinions reputation buildup'),
		 'description'=>ct('The total accumulated weight of opinions necessary for a user to have full reputation. This setting defines how fast users can initially build up reputation after they begin rating. For example, "60" means that once a user has given opinions (whether good or bad) with weight of 60, his reputation will be fully accounted for. Before that, his reputation is decreased proportionally.'),
		];

	$res['compress_css']=
		[':init'=>['default'=>false,],
		 'type'=>'checkbox',
		 ':section'=>'misc-config',
		 ':level'=>1,
		 'title'=>ct('Compress CSS'),
		 'description'=>ct('Performance. This will speed up your site by compressing CSS files of a page and storing them in files/css/compressed/[md5sum].css. If you enable this, dont forget to clear caches every time you change your CSS files. Production sites should always enable this. Disable it during development.'),
		];

	$res['page_cache_enable']=
		[':init'=>['default'=>false,],
		 'type'=>'checkbox',
		 ':section'=>'misc-config',
		 ':level'=>1,
		 'pre-submit'=>function($v){if(!$v){demosphere_page_cache_clear_all();}},
		 'title'=>ct('Page cache enable'),
		 'description'=>ct('Performance. This will significantly speed up your site by storing rendered page in the database. Cached pages are automatically removed every time you save an event, post, place ... Production sites should always enable this. Disable it during development.'),
		];

	$res['rss_time_formats']=
		[':init'=>['default'=>[t('(no date in the title)'),
							   '%A %e %B %Y',
							   '%A %e %B %Y %R',
							   '%d/%m %R',
							   '%d %b %R',
							   '%R',
							   '|full-date-time',],
				  ],
		 'type'=>'textarea',
		 ':cfg_to_form_val'  =>function($v){return implode("\n",$v);},
		 ':cfg_from_form_val'=>function($v){return trim($v)==='' ? [] : preg_split('@(\r\n|\r|\n)@',trim($v));},
		 ':section'=>'misc-config',
		 ':level'=>1,
		 'title'=>ct('Rss time formats'),
		 'description'=>ct('Time formats available as options in RSS feeds. You can use strftime strings (<a href="http://php.net/manual/en/function.strftime.php" target="_blank">PHP strftime doc</a>) or demos_format_date formats (start line with "|")'),
		 ':example'=>'<br/>http://abc.com<br/>http://xyz.org',
		];

	$res['rss_use_eventdate_instead_of_pubdate']=
		[':init'=>['default'=>false,],
		 'type'=>'checkbox',
		 ':section'=>'misc-config',
		 ':level'=>1,
		 'title'=>ct('Use event date instead of pubdate in RSS'),
		 'description'=>ct('Normally RSS feeds are meant to display articles according to the date they where published. RSS feeds are NOT meant for events. However, certain aggregators might understand future days or might, by chance, work ok. This option allows you to replace "published at" dates by the start time of the event in your main RSS feed.'),
		];

	$res['future_date_today_with_no_time']=
		[':init'=>['default'=>false,],
		 'type'=>'checkbox',
		 ':section'=>'misc-config',
		 ':level'=>1,
		 'title'=>ct('Highlight today is a future date'),
		 'description'=>ct('If we find a date that is today, with no time specified, should we consider it as a future date and highlight it? Without this, you might miss some last minute events. However, if you choose this you will be getting a lot highlighted dates from the "published at" dates, which can be quite a problem.'),
		];

	$res['future_date_offset']=
		[':init'=>['default'=>0,],
		 'type'=>'textfield',
		 ':duration_unit'=>'hours',
		 ':cfg_to_form_val'  =>'demosphere_config_form_duration_to_form',
		 ':cfg_from_form_val'=>'demosphere_config_form_duration_from_form',
		 ':section'=>'misc-config',
		 ':level'=>1,
		 'title'=>ct('Highlight future date offset'),
		 'description'=>ct('Number of hours from now that are considered as future. For example 4 means that a time that is up to 4 hours in the future is not considered as future. (you can use negative values).'),
		];

	$res['highlight_keywords']=
		[':init'=>['default'=>[],
				   'language_fr'=>['@(manif)@i',
								   '@(rassemblement)@i',
								   '@(projection)@i',
								   '@(d(é|e)bat)@i',
								   '@(Rendez[- ]?vous)@i',],
				  ],
		 'type'=>'textarea',
		 ':cfg_to_form_val'  =>function($v){return implode("\n",preg_replace('/@\(([\w ]+)\)@i/','$1',$v));},
		 ':cfg_from_form_val'=>function($v){
			 $v=trim($v)==='' ? [] : preg_split('@(\r\n|\r|\n)@',trim($v));
			 foreach($v as &$kw)
			 {
				 if(substr($kw,0,1)!=='@'){$kw='@('.preg_quote($kw,'@').')@i';}
			 }
			 return $v;
		 },
		 ':section'=>'misc-config',
		 ':level'=>1,
		 'title'=>ct('Words that should be highlighted'),
		 'description'=>ct('Each line can be a word or part of a word, or it can be a regular expressions. Regular expressions start with "@". Advanced usage: you  can optionally assign a class to keywords and then change its color in highlight.css'),
		 ':example'=>'<br/>'.
		 ct('Simple word: demonstration<br />'.
			'regular expression: @(demonstration)@i<br />'.
			'Simple word+class: demonstration|keyword_class|demosphere_keywordgroup1<br />demosphere_keywordgroup1 is just an example, choose the name you want then use the same name in highlight.css'),
		];

	$res['event_archive_delay']=
		[':init'=>['default'=>48*3600,],
		 'type'=>'textfield',
		 ':duration_unit'=>'hours',
		 ':cfg_to_form_val'  =>'demosphere_config_form_duration_to_form',
		 ':cfg_from_form_val'=>'demosphere_config_form_duration_from_form',
		 ':section'=>'misc-config',
		 ':level'=>1,
		 'title'=>ct('Search engine no index'),
		 'description'=>ct('Number of hours after an event. After this, the event will no longer be  indexed by search engines (Google). You probably do not want search engines (Google) to archive old events, since searches might show results for past events instead of future events.'),
		];

	$res['awstats_name']=
		[':init'=>['default'=>false,
				   'hosted'=>$siteId,
				  ],
		 'type'=>'textfield',
		 'validate-regexp'=>'@^[a-z0-9_-]+$@i',
		 'validate-regexp-message'=>ent(ct('must be letters and/or numbers')),
		 ':section'=>'misc-config',
		 ':level'=>1,
		 'title'=>ct('Awstats config name'),
		 'description'=>ct('Only set this if you are hosted on the Demosphere server.'),
		];

	$res['raw_apache_log']=
		[':init'=>['default'=>false,
				   'hosted'=>'/var/www/demosphere/'.$siteId.'/files/private/access.log'],
		 'type'=>'textfield',
		 // validation works but is too troublesome 'validate'=>function($v){return file_exists($v) ? true : ent(ct('file does not exists'));},
		 ':section'=>'misc-config',
		 ':level'=>1,
		 'title'=>ct('Raw Apache log'),
		 'description'=>ct('The file where Apache puts its log. You need this for statistics and dayranking.'),
		 ':example'=>'/var/www/xyz/access.log',
		];

	$res['dayrank_enable']=
		[':init'=>['default'=>true,],
		 'type'=>'checkbox',
		 ':section'=>'misc-config',
		 ':level'=>0,
		 'title'=>ct('Use day ranking'),
		 'description'=>ct('Enables / disables display of day-ranking. Day-ranking displays a small icon in front of the most viewed events for each day.'),
		];

	$res['dayrank_min_visits']=
		[':init'=>['default'=>20,],
		 'type'=>'int',
		 ':section'=>'misc-config',
		 ':level'=>0,
		 'required'=>true,
		 'title'=>ct('Day-rank min visits'),
		 'description'=>ct('Do not display day-rank unless the event has at least this number of visits.'),
		];

	$res['topics_required']=
		[':init'=>['default'=>'check-entered',],
		 'type'=>'select',
		 ':section'=>'misc-config',
		 'options'=>['not-required' =>ct('Not required'),
					 'check-entered'=>ct('Check if entered'),
					 'mandatory'    =>ct('Mandatory'),],
		 ':level'=>0,
		 'title'=>ct('Are topics required ?'),
		 'description'=>ct(' <dl>
<dt>Not required    </dt><dd>You do not need to enter a topic for an event. It will default to "others". No checks are done in the edit form.</dd>
<dt>Check if entered</dt><dd>It is possible to have no topic set for an event, but you must explicitly say so.</dd>
<dt>Mandatory       </dt><dd>You must enter a real topic for an event. There is no "others" choice or display.</dd>

</dl>'),
		];

	$res['term_vocabs']=
		[':init'=>['default'=>[]],
		 'type'=>'array_of_arrays',
		 'aofa-options'=>[[false,ct('id'),'int'],
						  ['name',ct('Vocab name')],
						  ['type',ct('Vocab type'),'select','values'=>['tag'=>t('tag')]],
						 ],
		 ':section'=>'misc-config',
		 ':level'=>0,
		 'title'=>ct('Term vocabularies'),
		 'description'=>ct('Terms can be used to add tags or other information to events. Terms are organized into different vocabs. Supported types: "tag": freely chosen tag, one or more tags per event (maybe other tag types in the future).'),
		 ':example'=>'<table>
<tr><td>1</td><td>organizer</td><td>tag</td></tr>
</table>',
		];

	$res['use_rejected_shown']=
		[':init'=>['default'=>false,],
		 'type'=>'checkbox',
		 ':section'=>'misc-config',
		 ':level'=>0,
		 'title'=>'Use rejected shown status',
		 'description'=>ct('This allows you to show a page displaying events that have been rejected. If you enable this you will have two different rejected statuses: "rejected shown" and "rejected hidden". The list of "rejected shown" events is available here: <a href="!url">!url</a> . You probably should display that link somewhere so that your visitors can find it.',['!url'=>$base_url.'/?selectModerationStatus=5']),
		];

	$res['rejected_shown_message']=
		[':init'=>['default'=>t('The !site_name team has decided not to publish these events on the main calendar. These events might not meet our publishing guidelines. Certain events might be considered interesting by our team, but too far from the main theme of our calendar to be published on the frontpage. Our team might also strongly disagree with the ideas expressed in certain other events.',['!site_name'=>$siteName]),],
		 'type'=>'textarea',
		 ':section'=>'misc-config',
		 ':level'=>0,
		 'title'=>ct('Rejected shown message'),
		 'description'=>ct('A message that will be shown above the page displaying "rejected shown" events.'),
		];

	$res['event_image_max_width_float_right']=
		[':init'=>['default'=>360,],
		 'type'=>'int',
		 ':section'=>'misc-config',
		 ':level'=>1,
		 'required'=>true,
		 'title'=>ct('Max width for right images'),
		 'description'=>ct('Max width (in pixels) for float right images (images that appear on the right column).'),
		];

	$res['event_image_max_width_body']=
		[':init'=>['default'=>576,],
		 'type'=>'int',
		 ':section'=>'misc-config',
		 ':level'=>1,
		 'required'=>true,
		 'title'=>ct('Max width for images in body'),
		 'description'=>ct('Max width (in pixels) for body images (images that appear inside the main text).'),
		];

	$res['widget_extra_message']=
		[':init'=>['default'=>false,],
		 'type'=>'textfield',
		 ':section'=>'misc-config',
		 ':level'=>0,
		 'title'=>ct('Widget extra message'),
		 'description'=>ct('A message that will be displayed by default at the bottom of user widgets. Widgets are calendars that users can display on their own web sites. The user can override or erase this message.'),
		 ':example'=>'Powered by Demosphere',
		];

	$res['hidden_event_links']=
		[':init'=>['default'=>false,],
		 'type'=>'checkbox',
		 ':section'=>'misc-config',
		 ':level'=>0,
		 'title'=>ct('Hidden links on event display'),
		 'description'=>ct('If you select this, invisible links will be added to event display pages. If someone copy-pastes an event, those links will show up in the copy. This technique is also used by wikipedia.'),
		];
	$res['share_links']=
		[':init'=>['default'=>[['network'=>'facebook'],['network'=>'twitter']]],
		 'type'=>'array_of_arrays',
		 'aofa-options'=>[['network',ct('network')],
						 ],
		 ':section'=>'misc-config',
		 ':level'=>0,
		 'title'=>ct('Social networking share-links'),
		 'description'=>ct('A list of social networking sites you want to link to. For now only "facebook" and "twitter" are supported.'),
		 ':example'=>'<table>
<tr><td>twitter</td></tr>
<tr><td>facebook</td></tr>
</table>',
		];

	$res['show_title_in_event']=
		[':init'=>['default'=>'automatic',],
		 'type'=>'select',
		 'options'=>['never'=>ct('never'),
					 'automatic'=>ct('automatic'),
					 'always'=>ct('always'),],
		 ':section'=>'misc-config',
		 ':level'=>0,
		 'title'=>ct('Show title on event pages'),
		 'description'=>ct('The title field you enter for an event is meant for the front page. It is often too long and not pretty to display on event pages. The best solution is to manually enter a title at the beginning of the text of each event, but that requires more work. <ul><li>If you select "never", the front page title will never be displayed on the event page. You must add a title in the text manually.</li><li> If you select "automatic", the system will look for a title at the beginning of your event text. If no title is found, the frontpage title will be added.</li><li>If you select "always", the frontpage title will always be displayed on event pages.</li><ul> '),
		];

	$res['admin_menu']=
		[':init'=>['default'=>['home'   =>['path'=>''               ,'title'=>t('home'      )],
							   'panel'  =>['path'=>'/control-panel' ,'title'=>t('panel'     )],
							   'pending'=>['path'=>'/pending-events','title'=>t('pending'   )],
							   'mail'   =>['path'=>'/mail-import'   ,'title'=>t('mail'      )],
							   'feed'   =>['path'=>'/feed-import'   ,'title'=>t('feed'      )],
							   'watch'  =>['path'=>'/page-watch'    ,'title'=>t('page watch')],
							   ],
				   'hosted' =>['home'   =>['path'=>''               ,'title'=>t('home'      )],
							   'panel'  =>['path'=>'/control-panel' ,'title'=>t('panel'     )],
							   'pending'=>['path'=>'/pending-events','title'=>t('pending'   )],
							   'mail'   =>['path'=>'/mail-import'   ,'title'=>t('mail'      )],
							   'feed'   =>['path'=>'/feed-import'   ,'title'=>t('feed'      )],
							   'watch'  =>['path'=>'/page-watch'    ,'title'=>t('page watch')],
							   'stats'  =>['path'=>'/cgi-bin/awstats.pl?config='.$siteId,'title'=>t('web-stats' )],
							   ],
				   ],
		 'type'=>'array_of_arrays',
		 'aofa-options'=>[[false  ,ct('name' )],
						  ['path' ,ct('path' )],
						  ['title',ct('title')],
						  ],
		 ':section'=>'misc-config',
		 ':level'=>0,
		 'title'=>ct('Main admin menu'),
		 'description'=>ct('The main menu on top of the site. "path" can either be an absolute path ("/example") or a full url ("https://example.org/xyz").'),
		 ];

	$res['external_event_sources']=
		[':init'=>['default'=>[],],
		 'type'=>'array_of_arrays',
		 'aofa-options'=>[['id',ct('id'),'int'],
						  ['extId',ct('external id')],
						  ['name',ct('name')],
						  ['type',ct('type')],
						  ['url',ct('url')],
						  ['event_status',ct('stat.'),'int'],
						  ['event_moderation_status',ct('mod.'),'int'],
						  ['event_needs_attention',ct('att.'),'int'],
						 ],
		 ':section'=>'misc-config',
		 ':level'=>2,
		 'title'=>ct('External event sources'),
		 'description'=>ct('A list of sources where we can automatically fetch external events. Ids should be incremented. Never re-use an old id for a new source. Ids 0 and 1 are reserved. (0=not external, 1=manually entered). Type: "demosphere" (only type supported for now). Type=demosphere is used for importing events from another demosphere site. Stat.,mod.,att. are the default values for fields (status, moderationStatus, needsAttention) in imported events.'),
		 ':example'=>'<table id="frontpage-infoboxes-example">
<tr><td>2</td><td>Paris</td><td>demosphere</td><td>https://paris.demosphere.net</td><td>1</td><td>4</td><td>0</td></tr>
</table>',
		];


	$res['comments_site_settings']=
		[':init'=>['default'=>'enabled',],
		 'type'=>'select',
		 'options'=>array_combine(Comment::$settings,Comment::$settings),
		 ':section'=>'misc-config',
		 ':level'=>0,
		 'title'=>ct('Comments site-wide settings'),
		 'description'=>ct('Default settings for comments on all pages. You can also individually change settings on each page.'),
		];

	$res['carpool_enable_default']=
		[':init'=>['default'=>true,],
		 'type'=>'checkbox',
		 ':section'=>'misc-config',
		 ':level'=>0,
		 'title'=>ct('Share a ride'),
		 'description'=>ct('If you choose this, share a ride will be on by default. Otherwise it will be off by default. In both cases, you can still enable/disable share a ride for each event.'),
		];

	$res['event_url_name']=
		[':init'=>['default'=>'event',
				   'language_fr'=>'rv',],
		 'type'=>'textfield',
		 ':section'=>'misc-config',
		 ':level'=>0,
		 'title'=>ct('"Event" name in URL'),
		 'pre-submit'=>function($v){file_put_contents('files/private/event-url-name',$v);},
		 'description'=>ct('Event pages have a URL like this one: https://example.org/xxxx/1234 '),
		 ':example'=>''.ent('"event" or "ev" ... ').'',
		];

	$res['place_url_name']=
		[':init'=>['default'=>'place',
				   'language_fr'=>'lieu',],
		 'type'=>'textfield',
		 ':section'=>'misc-config',
		 ':level'=>0,
		 'title'=>ct('"Place" name in URL'),
		 'description'=>ct('Place pages have a URL like this one: https://example.org/xxxx/1234 '),
		 ':example'=>''.ent('"place" or "location" ... ').'',
		];

	$res['places_url_name']=
		[':init'=>['default'=>'places-list',
				   'language_fr'=>'lieux',],
		 'type'=>'textfield',
		 ':section'=>'misc-config',
		 ':level'=>0,
		 'title'=>ct('"Places" name in URL'),
		 'description'=>ct('The places has a URL like this: https://example.org/yyyy '),
		 ':example'=>''.ent('"places-list" or "locations" ... ').'',
		];

	$res['websockets_enabled']=
		[':init'=>['default'=>false,'hosted'=>true],
		 'type'=>'checkbox',
		 ':section'=>'misc-config',
		 ':level'=>2,
		 'title'=>ct('Use websockets'),
		 'description'=>ct('For this to work you have to install /usr/local/bin/demoswebsockets-server.js configure it, run it as a server, proxy it through Apache... Don\'t enable this unless your really know what you are doing.'),
		];

	$res['use_proxy_for_sphinx_search']=
		[':init'=>['default'=>false,'hosted'=>true],
		 'type'=>'checkbox',
		 ':section'=>'misc-config',
		 ':level'=>2,
		 'title'=>ct('Use proxy for Sphinx search'),
		 'description'=>ct('On a multi-site setup several sites have to share the same Sphinx server. Sphinx does not provide authentification. We implemented an authentificating proxy. Only enable this sites hosted on the Demosphere server.'),
		];

	// ********** backend-form
	
	$res['common_cities']=
		[':init'=>['default'=>[t('(undefined location)'),
							   t('example1'),
							   t('example2'),],
				  ],
		 'type'=>'textarea',
		 ':cfg_to_form_val'  =>'demosphere_config_form_word_list_to_form',
		 ':cfg_from_form_val'=>'demosphere_config_form_word_list_from_form',
		 ':section'=>'backend-form',
		 ':level'=>0,
		 'title'=>ct('Common cities'),
		 'description'=>ct('A comma separated list of common cities or districts. This will appear in a pull-down menu on the backend form. This is NOT a list of all possible cities/districts. There is no such list. New cities/districts are created automatically if you enter one that does not exist yet.'),
		 ':example'=>'(Lieu non défini),Paris 20e,Paris 19e,Paris 18e,Paris 17e,Paris 16e,Paris 15e,Paris 14e,Paris 13e,Paris 12e,Paris 11e,Paris 10e,Paris 9e,Paris 8e,Paris 7e,Paris 6e,Paris 5e,Paris 4e,Paris 3e,Paris 2e,Paris 1e,Saint-Denis',
		];

	$res['incomplete_messages']=
		[
		 ':init'=>['default'=>
				   [['name'=>t('recurring'                        ),'message'=>t('This is a recurring event, we have not yet confirmed this specific date.')],
					['name'=>t('This event needs to be confirmed.'),'message'=>t('This event needs to be confirmed.')],
					['name'=>t('Time needs to be specified.'      ),'message'=>t('Time needs to be specified.')],
					['name'=>t('Place needs to be specified.'     ),'message'=>t('Place needs to be specified.')],
					['name'=>t('no event yet'                     ),'message'=>t('We do not yet know if a demonstration is planned.')],
					]],
		 'type'=>'array_of_arrays',
		 'aofa-options'=>[['name'   ,ct('name'   )],
						  ['message',ct('message')],
						  ],
		 ':section'=>'backend-form',
		 ':level'=>0,
		 'title'=>ct('Incomplete messages'),
		 'description'=>ct('Predefined messages for incomplete events.'),
		 ];

	$res['moderation_messages']=
		[':init'=>['default'=>[t('Duplicate'),t('Wait %date'),t('Wait for answer'),t('Expensive'),t('Not political')],],
		 'type'=>'textarea',
		 ':cfg_to_form_val'  =>function($v){return implode("\n",$v);},
		 ':cfg_from_form_val'=>function($v){return trim($v)==='' ? [] : preg_split('@(\r\n|\r|\n)@',trim($v));},
		 ':section'=>'backend-form',
		 ':level'=>0,
		 'title'=>ct('Moderation messages'),
		 'description'=>ct('A list of common messages for moderators. Moderators can type any message they want, this list is just a speedup. Enter one message per line.'),
		];

	$res['require_keyword_in_event_title']=
		[':init'=>['default'=>true,],
		 'type'=>'checkbox',
		 ':section'=>'backend-form',
		 ':level'=>0,
		 'title'=>ct('Require keyword in event title'),
		 'description'=>ct('If true, moderators are required to enter underscores around a word in the title of an event. The surrounded word is displayed in different color on the frontpage. This helps quick reading, especially when there are a lot of events.'),
		];

	$res['suggest_place_bad_words']=
		[':init'=>['default'=>explode(',','the,of,to,and,a,in,is,it,you,that,he,was,for,on,are,with,as,I,his,they,be,at,one,have,this,from,or,had,by,hot,but,some,what,there,we,can,out,other,were,all,http,www,org'),
				   'language_fr'=>explode(',','le,les,la,de,métro,paris,des,du,ou,à,au,et,en,un,ces,par,aux,dans,devant,avec,sur,si,se,ce,http,www,org')],
		 'type'=>'textarea',
		 ':cfg_to_form_val'  =>'demosphere_config_form_word_list_to_form',
		 ':cfg_from_form_val'=>'demosphere_config_form_word_list_from_form',
		 ':section'=>'backend-form',
		 ':level'=>1,
		 'title'=>ct('Suggest place bad words'),
		 'description'=>ct('A comma separated list of words. When the event form suggests a place it searches for  similar words in the text. This option removes certain words that are too common and will mess up the suggestion.'),
		 ':example'=>''.ent('le,les,la,de,métro,paris,des,du,ou,à,au,et,en,un,ces,par,aux,dans,devant,avec,sur,si,se,ce,http,www,org').'',
		];

	$res['suggest_place_ugly_words']=
		[':init'=>['default'=>[]],
		 'type'=>'textarea',
		 ':cfg_to_form_val'  =>'demosphere_config_form_word_list_to_form',
		 ':cfg_from_form_val'=>'demosphere_config_form_word_list_from_form',
		 ':section'=>'backend-form',
		 ':level'=>1,
		 'title'=>ct('Suggest place ugly words'),
		 'description'=>ct('A comma separated list of words. When the event form suggests a place it searches for similar words in the text. This option diminishes the importance of certain words.'),
		 ':example'=>'porte,av,dans,saint,aux,direction,st,rue,pour,plus,lieu,pas',

		];

	$res['editor_custom_insert']=
		[':init'=>['default'=>[],],
		 'type'=>'array_of_arrays',
		 'aofa-options'=>[['name',ct('name')],
						  ['icon',ct('icon')],
						  ['html',ct('html')],
						 ],
		 ':section'=>'backend-form',
		 ':level'=>2,
		 'title'=>ct('Editor custom insert'),
		 'description'=>ct('Add custom buttons in editor to insert some HTML. Only use simple letters or numbers in "name". Icon files should be in demos "files/images" directory.'),
		 ':example'=>'<br/>'.
		 'name:example1;icon:ex1.png;html:&lt;b&gt;hi! &lt;/b&gt;<br/>'.
		 'name:example2;icon:ex2.png;html:&lt;b&gt;zzzz&lt;/b&gt;<br/>',
		];

	$res['post_on_other_site']=
		[':init'=>['default'=>[],],
		 'type'=>'array_of_arrays',
		 'aofa-options'=>[['url',ct('url')],
						  ['type',ct('type')],
						  ['user',ct('user')],
						  ['password',ct('password')],
						 ],
		 ':section'=>'backend-form',
		 ':level'=>2,
		 'title'=>ct('Post on other site'),
		 'description'=>ct('Adds an option in the backend form to post this event on another site. Before adding a site here, you should contact the other site and make sure they agree on this. For now the other site must be a demosphere site. In the future, we will support other sites (please contact us). Note: you generaly do not need to enter a username and password. If you do use a user/password, please choose a user that does not have priviledges on the other site. The password is stored in plaintext in the database... so it is not secure.'),
		 ':example'=>'<table >
<tr><th>url</th><th>type</th><th>user</th><th>password</th></tr>
<tr><td>https://xyz.demosphere.net</td><td>demosphere</td><td>(empty)</td><td>(empty)</td></tr>
<tr><td>https://zzz.demosphere.net</td><td>demosphere</td><td>myname</td><td>secretpassword</td></tr>
</table>',
		];

	// ********** frontpage

	$res['site_slogan']=
		[':init'=>['default'=>'FIXME site slogan'],
		 'type'=>'textfield',
		 ':section'=>'frontpage',
		 ':level'=>0,
		 'title'=>ct('Site slogan'),
		 'description'=>ct('A subtitle that will be displayed under the logo on the frontpage.'),
		 ':example'=>'agenda alternatif de la région parisienne',
		];

	$res['site_mission']=
		[':init'=>['default'=>false,],
		 'type'=>'textfield',
		 ':section'=>'frontpage',
		 ':level'=>0,
		 'title'=>ct('Site mission'),
		 'description'=>ct('A long text that will be displayed under the logo on the frontpage.'),
		 ':example'=>'Bla bla bla',
		];

	$res['frontpage_announcement']=
		[':init'=>['default'=>false,],
		 'type'=>'int',
		 ':section'=>'frontpage',
		 ':level'=>2,
		 'required'=>true,
		 'title'=>ct('Frontpage announcement'),
		 'description'=>ct('The number of the Post where the frontpage announcement is stored. Normally this is created when you install demosphere. You do not need to change it.'),
		];

	$res['frontpage_messages']=
		[':init'=>['default'=>false,],
		 'type'=>'int',
		 ':section'=>'frontpage',
		 ':level'=>2,
		 'required'=>true,
		 'title'=>ct('Frontpage messages'),
		 'description'=>ct('The number of the Post where the frontpage message is stored. Normally this is created when you install demosphere. You do not need to change it.'),
		];


	$res['front_page_infoboxes']=
		[':init'=>['default'=>[],],
		 'type'=>'array_of_arrays',
		 'aofa-options'=>[['pid',ct('Post id'),'int'],
						  ['class',ct('class')],
						  ['title-link',ct('title link')],
						 ],
		 ':section'=>'frontpage',
		 ':level'=>0,
		 'title'=>ct('Frontpage info-boxes'),
		 'description'=>ct('Info-boxes are displayed on the right column of the frontpage. <strong>nid</strong> is the "post number" which you can find in the url: for example if the url of a Post you created is https://example.org/post/456 then the pid is "456". To add an infobox, you must first create a Post for it (<a target="_blank" href="!url">click here</a>). The <strong>class</strong> is the css class you want to use for the Post. You can use several classes by separating them with a space. You must use either stdInfobox (normal text infobox) or fullInfobox. Notice that the titles of the default infoboxes are CSS images (for prettier fonts). <strong>title-link</strong> is a link that appears on the title.',['!url'=>$base_url.'/post/add']),
		 ':example'=>'<table id="frontpage-infoboxes-example">
<tr><td>14</td><td>demosphereMapLink fullInfobox</td><td></td></tr>
<tr><td>15</td><td>infoboxWithLogo stdInfobox</td><td>https://example.org/about-us</td></tr>
<tr><td>16</td><td>infoboxWithImageTitle1 stdInfobox</td><td></td></tr>
<tr><td>17</td><td>infoboxStayInformed fullInfobox</td><td></td></tr>
</table>',
		];

	$res['frontpage_regions']=
		[':init'=>['default'=>[],],
		 'type'=>'array_of_arrays',
		 'aofa-options'=>[[false,ct('id')],
						  ['cityId',ct('City id'),'int'],
						  ['name',ct('name'),],
						  ['lat',ct('latitude'),'float'],
						  ['lng',ct('longitude'),'float'],
						  ['distance',ct('distance'),'int'],
						  ['domain',ct('domain name'),],
						 ],
		 ':section'=>'frontpage',
		 ':level'=>0,
		 'title'=>ct('Frontpage regions'),
		 'description'=>ct('A list of geographical regions (cities, neighborhoods, map position + distance), that can be selected on the frontpage. The key is the name that will appear on the url. The value is an array describing the region. name: The name that will be displayed in the selector  (if using tid, "name" is optional); lat,lng,distance : describes the region ; tid: use a term (cityId) instead of lat,lng,distance ; domain: the front page url for this city (you must configure your DNS and add an apache ServerAlias)'),
		 ':example'=>'<table id="frontpage-regions-example"><tr><th>id</th><th>tid</th><th>nom</th><th>lat</th><th>lng</th><th>dist</th><th>domain</th></tr><tr><td class="type-text">paris</td><td class="type-int"></td><td class="type-text">Paris</td><td class="type-float">48.86031</td><td class="type-float">2.3397</td><td class="type-int">1</td><td></td></tr><tr><td class="type-text">le-blanc-mesnil</td><td class="type-int">66</td><td class="type-text"></td><td class="type-float"></td><td class="type-float"></td><td class="type-int"></td><td>https://bm.example.org</td></tr></table>',
		];


	$res['frontpage_regions_use_links']=
		[':init'=>['default'=>false,],
		 'type'=>'checkbox',
		 ':section'=>'frontpage',
		 ':level'=>0,
		 'title'=>ct('Frontpage regions use links'),
		 'description'=>ct('Whether you want to use link or a selector on the top of the frontpage to display regions.'),
		];

	$res['frontpage_regions_url_name']=
		[':init'=>['default'=>t('city'),],
		 'type'=>'textfield',
		 ':section'=>'frontpage',
		 ':level'=>0,
		 'title'=>ct('"Region" name in URL'),
		 'description'=>ct('If you choose to use frontpage region selectors or links, then the calendar for each region will have a URL like this one: https://example.org/city/paris . Some calendars might want another name than "city" in that URL.'),
		 ':example'=>''.ent('"city" or "district" or "region" ... ').'',
		];

	$res['front_page_tooltips']=
		[':init'=>['default'=>false,],
		 'type'=>'textfield',
		 ':section'=>'frontpage',
		 ':level'=>0,
		 'title'=>ct('Tooltips in frontpage titles'),
		 'description'=>ent(ct('Display tooltips when mouse hovers on front page events. Leave empty for no tooltips. The text you enter here will be displayed in the tooltip. You can use the special word <address> that will be replaced by the event\'s address')),
		 ':example'=>''.ent('<address>').'',
		];

	$res['frontpage_options']=
		[':init'=>['default'=>['select-topic'              =>['where'=>1],
							   'select-place-reference-id' =>['where'=>1],
							   'select-city-id'            =>['where'=>1],
							   'select-fp-region'          =>['where'=>1],
							   'most-visited'              =>['where'=>1],
							   'max-nb-events-per-day'     =>['where'=>1],
							   'select-recent-changes'     =>['where'=>1],
							   'order-by-last-big-changes' =>['where'=>1],
							   'near-lat-lng'              =>['where'=>1],
							   'map-shape'                 =>['where'=>2],
							   'select-vocab-term'         =>['where'=>2],
							   'select-vocab-term-or'      =>['where'=>2],
							   'limit'                     =>['where'=>2],
							  ]],
		 'type'=>'array_of_arrays',
		 'aofa-options'=>[[false,ct('name')],
						  ['where',ct('where'),'int'],
						 ],
		 ':section'=>'frontpage',
		 ':level'=>0,
		 'validate'=>function($v,$item)
			{
				$a=array_keys($item[':init']['default']);
				$b=array_keys($v);
				sort($a);
				sort($b);
				if($a!==$b){return 'you cannot add or remove options';}
			},
		 'title'=>ct('Frontpage options'),
		 'description'=>ct('Buttons on frontpage to help user find / select events. 0: do not show; 1: show directly; 2: show inside "more options" popup.'),
		];


	$res['front_page_limit_nb_events']=
		[':init'=>['default'=>300,],
		 'type'=>'int',
		 ':section'=>'frontpage',
		 ':level'=>0,
		 'required'=>true,
		 'title'=>ct('Max number of events on frontpage'),
		 'description'=>ent(ct('Limit the number of events displayed by default on the front page. Leave empty for no limit.')),
		];

	$res['event_title_max_width']=
		[':init'=>['default'=>553,],
		 'type'=>'int',
		 ':section'=>'frontpage',
		 ':level'=>2,
		 'required'=>true,
		 'title'=>ct('Max title width'),
		 'description'=>ct('Maximum width (pixels) for the title of an event. Do NOT change this.'),
		];

	$res['city_max_width']=
		[':init'=>['default'=>89,],
		 'type'=>'int',
		 ':section'=>'frontpage',
		 ':level'=>2,
		 'required'=>true,
		 'title'=>ct('Max city width'),
		 'description'=>ct('Maximum width (pixels) for the city of an event. Do NOT change this.'),
		];

	// ********** graphics

	$history=variable_get('color_history','',[]);
	$sel='';
	$colorPresets=demosphere_config_color_presets();
	foreach(array_merge($history,$colorPresets) as $name=>$colorPreset)
	{
		$sel.='<option value="'.ent($name).'">'.ent($name).'</option>';
	}
	$res['color_palette_fg']=
		[':init'=>['default'=>'#672178',],
		 'type'=>'color',
		 ':section'=>'graphics',
		 ':level'=>0,
		 'required'=>true,
		 'title'=>ct('Palette foreground color'),
		 'description'=>ent(ct('This is one of the 3 main colors from which many other colors on the site are computed.')),
		 'prefix'=>'<select id="color-presets"><option>(choose)</option>'.$sel.'</select>'.
		 '<script>demosphereColorPresets='.dlib_json_encode_esc(array_merge($history,$colorPresets)).'</script>',
		 ':cfg_postprocess'=>function(&$res)use($colorPresets)
			{
				// FIXME: move most of this into another function like  "demosphere_update_colors"
				global $demosphere_config;
				require_once 'demosphere-common.php';
				$colors=['color_palette_fg','color_palette_bg','color_palette_bg2'];
				// if colors have not changed, ignore
				if(array_intersect_key($res              ,array_flip($colors))===
				   array_intersect_key($demosphere_config,array_flip($colors))    )
				{
					demosphere_cache_clear();
					return;
				}

				require_once 'dlib/css-template.php';
				$hsl=css_template_to_hsl(css_template_parse_color($res['color_palette_bg']));
				$res['color_is_light_background']=$hsl[3]>85;

				// identify color preset for body class
				$extraBodyClass=val($res,'extra_body_class',$demosphere_config['extra_body_class']);
				$extraBodyClass=preg_replace('@\s*color-preset[^ ]*@','',$extraBodyClass);
				foreach($colorPresets as $presetName=>$colorPreset)
				{
					if(array_intersect_key($res        ,array_flip($colors))===
					   array_intersect_key($colorPreset,array_flip($colors)))
					{
						$extraBodyClass.=' color-preset-'.$presetName;
					}
				}
				$res['extra_body_class']=$extraBodyClass;

				demosphere_cache_clear_slow();
				dlib_message_add('Images (sprites) successfully rebuilt after color change.');

				// Fix favicon if it is std (one flat color) favicon
				$image = new Imagick("files/images/favicon.ico");
				if(count($image->getImageHistogram())===1)
				{
					$image->floodfillPaintImage($res['color_palette_fg'], 1,  $image->getImagePixelColor(5, 5), 5,5, false);
					$image->writeImage('files/images/favicon.ico'); 
				}

				// *** add to color history
				$old=array_intersect_key($demosphere_config,array_flip($colors));
				// dont add to color history if in predefined
				foreach($colorPresets as $colorPreset)
				{
					if($old===array_intersect_key($colorPreset,array_flip($colors))){return;}
				}
				$history=variable_get('color_history','',[]);
				array_unshift($history,$old);
				$history=array_slice($history,0,10);
				variable_set('color_history','',$history);
			},
		];

	$res['color_palette_bg']=
		[':init'=>['default'=>'#eeeeee',],
		 'type'=>'color',
		 ':section'=>'graphics',
		 ':level'=>0,
		 'required'=>true,
		 'title'=>ct('Palette background color'),
		 'description'=>ent(ct('The background of the frontpage and the top of most pages. This is one of the 3 main colors from which many other colors on the site are computed.')),
		];

	$res['color_palette_bg2']=
		[':init'=>['default'=>'#efe1f5',],
		 'type'=>'color',
		 ':section'=>'graphics',
		 ':level'=>0,
		 'required'=>true,
		 'title'=>ct('Palette background color 2'),
		 'description'=>ent(ct('The background of the frontpage and the top of most pages. This is one of the 3 main colors from which many other colors on the site are computed.')),
		];

	$res['color_palette_flashy']=
		[':init'=>['default'=>'#aadd66',],
		 'type'=>'color',
		 ':section'=>'graphics',
		 ':level'=>0,
		 'required'=>true,
		 'title'=>ct('Flashy color'),
		 'description'=>ent(ct('A color used for big buttons on frontpage, to make them visible.')),
		];

	$res['color_palette_text']=
		[':init'=>['default'=>false,],
		 'type'=>'color',
		 ':section'=>'graphics',
		 ':level'=>2,
		 'title'=>ct('Palette text color'),
		 'description'=>ent(ct('A color used for some texts and titles. This is slightly darker than foreground. If you leave it empty, it will be automatically computed from fg.')),
		];

	$res['sprites_enable']=
		[':init'=>['default'=>true,],
		 'type'=>'checkbox',
		 ':section'=>'graphics',
		 ':level'=>2,
		 'title'=>ct('enable sprites'),
		 'description'=>ent(ct('Disable this only for debugging. Warning: disabling sprites is not very well tested.')),
		];

	$res['color_is_light_background']=
		[':init'=>['default'=>true,],
		 'type'=>'checkbox',
		 ':section'=>'graphics',
		 ':level'=>2,
		 'title'=>ct('Background color is light'),
		 'description'=>ent(ct('Whether the background used is very light (almost white). This is automatically computed. Do not change it.')),
		];

	// ********** maps

	$res['map_center_latitude']=
		[':init'=>['default'=>false,'language_fr'=>46.75984],
		 'type'=>'float',
		 ':section'=>'maps',
		 ':level'=>0,
		 'title'=>ct('Interactive map center latitude'),
		 'description'=>ct('The geographic coordinates of the center of the interactive map. Typically this is the center of the city or region your calendar covers.'),
		 ':example'=>'48.856696',
		];

	$res['map_center_longitude']=
		[':init'=>['default'=>false,'language_fr'=>1.738281],
		 'type'=>'float',
		 ':section'=>'maps',
		 ':level'=>0,
		 'title'=>ct('Interactive map center longitude'),
		 'description'=>ct('The geographic coordinates of the center of the interactive map. Typically this is the center of the city or region your calendar covers.'),
		 ':example'=>'2.350817',
		];

	$res['map_zoom']=
		[':init'=>['default'=>1,'language_fr'=>6,],
		 'type'=>'int',
		 ':section'=>'maps',
		 ':level'=>0,
		 'required'=>true,
		 'title'=>ct('Interactive map zoom'),
		 'description'=>ct('The zoom level of the interactive map. You can find this by looking at the "link" in a Google-map. In the URL you will see "z=11" like in this example: https://maps.google.fr/?ie=UTF8&ll=48.89,2.43&z=11'),
		 ':example'=>'11',
		];

	$res['map_show_option_list']=
		[':init'=>['default'=>false,],
		 'type'=>'checkbox',
		 ':section'=>'maps',
		 ':level'=>0,
		 'title'=>ct('Interactive map show options'),
		 'description'=>ct('Do you want to show the options list by default ? If disabled, the user can still click to open the list.'),
		];

	$res['map_location_shortcuts']=
		[':init'=>['default'=>[],],
		 'type'=>'array_of_arrays',
		 'aofa-options'=>[['name',ct('name')],
						  ['lat',ct('latitude'),'float'],
						  ['lng',ct('longitude'),'float'],
						  ['zoom',ct('zoom'),'int'],
						 ],
		 ':section'=>'maps',
		 ':level'=>0,
		 'title'=>ct('Interactive map shortcuts'),
		 'description'=>ct('A selection box on the interactive map will appear allowing users to directly move the map to one of these predefined locations.'),
		];

	$res['gmap_key']=
		[':init'=>['default'=>false,'hosted'=>'AIzaSyDzoGkADv4F1mzPZnvhXwKKYrquK8V3mns',],
		 'type'=>'textfield',
		 ':section'=>'maps',
		 ':level'=>0,
		 'title'=>ct('Google map API Key'),
		 'description'=>ct('To display the interactive map, Google <a href="https://code.google.com/apis/maps/">requires</a> that you <a href="https://code.google.com/apis/maps/signup.html">register</a>.'),
		 ':example'=>'ABQIAAAAUC3ZNZmXmgZLkJxOgnCjCRTjPulzorcZf_c_ZCUPPUVJ5oWIrhRt9Gqqjb5s80ieMNCbX22EuCybbw',
		];

	$res['mapbox_access_token']=
		[':init'=>['default'=>false,'hosted'=>'pk.eyJ1IjoiZGVtb3NwaGVyZSIsImEiOiJjamtoNzl6aDkwcm96M3BreGZreDVlNG01In0.1Wqt0NzH4tmUICE1Whcgrw',],
		 'type'=>'textfield',
		 ':section'=>'maps',
		 ':level'=>0,
		 'title'=>ct('Mapbox access token'),
		 'description'=>ct('Access to <a href="https://www.mapbox.com">Mapbox</a>, a commercial OSM provider with a free plan.'),
		 ':example'=>'pk.eyJsdf1IfgjdfgoiDFGDFGNdFGfdgVlNG01In0.1Wqt0NzH4tmUICE1dfgdfg',
		];

	$res['country_map_url']=
		[':init'=>['default'=>'https://www.google.com/maps',
				   'language_fr'=>'https://www.google.fr/maps',
				   'language_es'=>'https://www.google.es/maps',],
		 'type'=>'url',
		 ':section'=>'maps',
		 ':level'=>0,
		 'title'=>ct('Backend form Google map URL'),
		 'description'=>ct('Url for the Google map of your country. This is used in the backend form and a few other places. This is not used for the interactive map with the events.'),
		 ':example'=>'https://www.google.fr/maps or https://www.google.com/maps',
		];

	$res['extra_map_links']=
		[':init'=>['default'=>[],
				  ],
		 'type'=>'array_of_arrays',
		 'aofa-options'=>[['url',ct('url')],
						  ['name',ct('name')],
						 ],
		 ':section'=>'maps',
		 ':level'=>0,
		 'title'=>ct('Backend form links'),
		 'description'=>ct('Links that will appear in the backend form to help you find the exact address and geographic position of an event. For example, this can be links to other maps or yellow page services.'),
		 ':example'=>' http://fr.mappy.com ; mappy',
		];

	$res['event_map_link']=
		[':init'=>['default'    =>'https://www.google.com/maps/place/%lat,%lng/@%lat,%lng,%zoomz',
				   'language_fr'=>'https://www.google.fr/maps/place/%lat,%lng/@%lat,%lng,%zoomz',
				   'language_es'=>'https://www.google.es/maps/place/%lat,%lng/@%lat,%lng,%zoomz',],
		 'type'=>'url',
		 ':section'=>'maps',
		 ':level'=>0,
		 'title'=>ct('Link, when user clicks on event map'),
		 'description'=>ct('Just copy one of the examples on the right. Form more details see the next option (Url for downloading map images...)'),
		 ':example'=>'<br/>'.
		 '<strong>Google map:</strong>'       .ent('https://www.google.com/maps/place/%lat,%lng/@%lat,%lng,%zoomz').'<br/>'.
		 '<strong>Google map France:</strong>'.ent('https://www.google.fr/maps/place/%lat,%lng/@%lat,%lng,%zoomz').'<br/>'.
		 '<strong>Open street maps:</strong>' .ent('http://www.openstreetmap.org/?mlat=%lat&mlon=%lng&zoom=%zoom').'<br/>'
		];

	$res['event_map_static_url']=
		[':init'=>['default'=>'https://maps.googleapis.com/maps/api/staticmap?center=%lat,%lng&zoom=%zoom&size=%widthx%height&maptype=roadmap&markers=color:red|label:|%lat,%lng&sensor=false&key=%gmap_key&language=%language&region=%region',
				  ],
		 'type'=>'url',
		 ':section'=>'maps',
		 ':level'=>0,
		 'title'=>ct('Url for downloading map images for events'),
		 'description'=>ct('The image displayed on each event is downloaded from a map provider (<a href="https://code.google.com/apis/maps/documentation/staticmaps/">google maps</a>, <a href="http://wiki.openstreetmap.org/wiki/Static_map_images">open street maps</a> <a href="http://ojw.dev.openstreetmap.org/StaticMap/?mode=API">[details]</a>, etc.). For a standaard map, just copy one of the urls from an examples on the right. Note: when you change this url, existing map images will remain, only new map locations will use your new settings. If you want to update all existing maps, you should erase all the files starting by dmap_ in the files/maps/ directory and clear your cache. The map services (google map, openstreetmap...) each provide a different "Static Map API", with different options. Each API just defines how to write a URL that will download a map image at a specific latitude and longitude. Available replacements: %width,%height,%zoom,%lat,%lng,%gmap_key'),
		 ':example'=>'<br/>'.
		 '<strong>Google map:</strong>'.ent('https://maps.googleapis.com/maps/api/staticmap?center=%lat,%lng&zoom=%zoom&size=%widthx%height&maptype=roadmap&markers=color:red|label:|%lat,%lng&sensor=false&key=%gmap_key&language=%language&region=%region').'<br/>'.
		 '<strong>Open street maps:</strong>'.ent('http://staticmap.openstreetmap.de/staticmap.php?center=%lat,%lng&zoom=%zoom&size=%widthx%height&maptype=mapnik').'<br/>'.
		 '<strong>OSM / mapquest (needs key):</strong>'.ent('https://www.mapquestapi.com/staticmap/v5/map?key=(FIXME:key)&size=370,195&zoom=%zoom&center=%lat,%lng').'<br/>'
		];

	$res['event_map_add_marker']=
		[':init'=>['default'=>false,],
		 'type'=>'checkbox',
		 ':section'=>'maps',
		 ':level'=>0,
		 'title'=>ct('Add marker to event map image?'),
		 'description'=>ct('Whether or not to add a red pointer at the center of the map. Certain map services do this directly.'),
		];

	$res['event_map_crop']=
		[':init'=>['default'=>25,],
		 'type'=>'int',
		 ':section'=>'maps',
		 ':level'=>0,
		 'required'=>true,
		 'title'=>ct('Event map crop'),
		 'description'=>ct('When dowloading certain maps, you might want to get a larger image, and then crop away the borders. This can be used, for example, to get of logos. This field specifies the number of pixels on each side of the map. Set to 0 for no crop.'),
		];

	$res['event_map_width']=
		[':init'=>['default'=>320,],
		 'type'=>'int',
		 ':section'=>'maps',
		 ':level'=>1,
		 'required'=>true,
		 'title'=>ct('Width of small map image'),
		 'description'=>ct('The size of the map that appears under the address in the display of an event.'),
		];
	$res['event_map_height']=
		[':init'=>['default'=>145,],
		 'type'=>'int',
		 ':section'=>'maps',
		 ':level'=>1,
		 'required'=>true,
		 'title'=>ct('Height of small map image'),
		 'description'=>ct('The size of the map that appears under the address in the display of an event.'),
		];

	// ********** date time

	$res['locale']=
		[':init'=>['default'=>'fr_FR.utf8',
				   'language_fr'=>'fr_FR.utf8',
				   'language_es'=>'es_ES.utf8',
				  ],
		 'type'=>'textfield',
		 ':section'=>'date-time',
		 ':level'=>0,
		 'required'=>true,
		 'validate'=>function($v){return setlocale(LC_ALL,$v)!==false;},
		 'title'=>ct('Locale setting'),
		 'description'=>ct('Language setting used for several things, notably for date and time display and parsing.'),
		 ':example'=>'en_US.utf8,fr_FR.utf8',
		];
	$res['timezone']=
		[':init'=>['default'=>'Europe/Paris',
				   'language_fr'=>'Europe/Paris',],
		 'type'=>'textfield',
		 ':section'=>'date-time',
		 ':level'=>0,
		 'required'=>true,
		 'title'=>ct('Time Zone'),
		 'description'=>ct('Choose the timezone form the following list:').
		 ' <a href="http://www.php.net/manual/en/timezones.php" target="_blank">'.ct('list of timezones').'</a>',
		 ':example'=>'Europe/Paris or America/New_York',
		];

	// ********** pages

	$res['built_in_pages_sync_ref']=
		[':init'=>['default'=>false,
				   'language_fr'=>'https://paris.demosphere.net',
				   'language_es'=>'https://elotrovalladolid.es',
				   'language_gl'=>'https://corunha.demosphere.net',
				   'language_el'=>'https://www.kinimatorama.net',
				   'language_pt'=>'https://saopaulo.demosphere.net',
				   'language_ca'=>'https://valencia.demosphere.net',
				   'language_de'=>'https://wuerzburg.demosphere.net',
				   'language_en'=>'https://brum.demosphere.net',
				  ],
		 'type'=>'url',
		 ':section'=>'pages',
		 ':level'=>0,
		 'required'=>false,
		 'title'=>ct('Page sync reference site'),
		 'description'=>ct('Pages that are synced will be automatically updated from this site. Leave empty if you are a reference site or to completely disable any sync.'),
		];

	$res['contact_page_sync']=
		[':init'=>['default'=>false,],
		 'type'=>'checkbox',
		 ':section'=>'pages',
		 ':level'=>0,
		 'title'=>ct('Contact page sync'),
		 'description'=>ct('If you select this, the page will be updated automatically from the reference site.'),
		];
	$res['about_us_page_sync']=
		[':init'=>['default'=>false,],
		 'type'=>'checkbox',
		 ':section'=>'pages',
		 ':level'=>0,
		 'title'=>ct('"About us" page sync'),
		 'description'=>ct('If you select this, the page will be updated automatically from the reference site.'),
		];

	$res['publishing_guidelines_page_sync']=
		[':init'=>['default'=>false,],
		 'type'=>'checkbox',
		 ':section'=>'pages',
		 ':level'=>0,
		 'title'=>ct('Publishing guidelines page sync'),
		 'description'=>ct('If you select this, the page will be updated automatically from the reference site.'),
		];

	$res['comment_guidelines_page_sync']=
		[':init'=>['default'=>false,],
		 'type'=>'checkbox',
		 ':section'=>'pages',
		 ':level'=>0,
		 'title'=>ct('Comment guidelines page sync'),
		 'description'=>ct('If you select this, the page will be updated automatically from the reference site.'),
		];

	$res['map_help_page_sync']=
		[':init'=>['default'=>true,],
		 'type'=>'checkbox',
		 ':section'=>'pages',
		 ':level'=>0,
		 'title'=>ct('Interactive map help page sync'),
		 'description'=>ct('If you select this, the page will be updated automatically from the reference site.'),
		];

	$res['user_cal_help_page_sync']=
		[':init'=>['default'=>true,],
		 'type'=>'checkbox',
		 ':section'=>'pages',
		 ':level'=>0,
		 'title'=>ct('User calendar help page sync'),
		 'description'=>ct('If you select this, the page will be updated automatically from the reference site.'),
		];

	$res['control_panel_data_page_sync']=
		[':init'=>['default'=>true,],
		 'type'=>'checkbox',
		 ':section'=>'pages',
		 ':level'=>0,
		 'title'=>ct('Control panel data sync'),
		 'description'=>ct('If you select this, the page will be updated automatically from the reference site.'),
		];

	$res['help_data_page_sync']=
		[':init'=>['default'=>true,],
		 'type'=>'checkbox',
		 ':section'=>'pages',
		 ':level'=>0,
		 'title'=>ct('Help data sync'),
		 'description'=>ct('If you select this, the page will be updated automatically from the reference site.'),
		];

	$res['spread_the_word_page_sync']=
		[':init'=>['default'=>false,],
		 'type'=>'checkbox',
		 ':section'=>'pages',
		 ':level'=>0,
		 'title'=>ct('Spread the word page sync'),
		 'description'=>ct('If you select this, the page will be updated automatically from the reference site.'),
		];

	$res['event_sources_page_sync']=
		[':init'=>['default'=>false,],
		 'type'=>'checkbox',
		 ':section'=>'pages',
		 ':level'=>0,
		 'title'=>ct('Event sources page sync'),
		 'description'=>ct('If you select this, the page will be updated automatically from the reference site.'),
		];

	$res['help_event_emails_page_sync']=
		[':init'=>['default'=>true,],
		 'type'=>'checkbox',
		 ':section'=>'pages',
		 ':level'=>0,
		 'title'=>ct('Help on email subscription page sync'),
		 'description'=>ct('If you select this, the page will be updated automatically from the reference site.'),
		];

	$res['help_feeds_page_sync']=
		[':init'=>['default'=>true,],
		 'type'=>'checkbox',
		 ':section'=>'pages',
		 ':level'=>0,
		 'title'=>ct('Help on using feeds page sync'),
		 'description'=>ct('If you select this, the page will be updated automatically from the reference site.'),
		];

	$res['help_ical_page_sync']=
		[':init'=>['default'=>true,],
		 'type'=>'checkbox',
		 ':section'=>'pages',
		 ':level'=>0,
		 'title'=>ct('Help on using ical page sync'),
		 'description'=>ct('If you select this, the page will be updated automatically from the reference site.'),
		];

	$res['help_on_your_website_page_sync']=
		[':init'=>['default'=>true,],
		 'type'=>'checkbox',
		 ':section'=>'pages',
		 ':level'=>0,
		 'title'=>ct('Help on publishing a calendar on a users own site page sync'),
		 'description'=>ct('If you select this, the page will be updated automatically from the reference site.'),
		];

	$res['donate_message_page_sync']=
		[':init'=>['default'=>true,],
		 'type'=>'checkbox',
		 ':section'=>'pages',
		 ':level'=>0,
		 'title'=>ct('A donate message that will be displayed when visitors want to publish an event.'),
		 'description'=>ct('If you select this, the page will be updated automatically from the reference site. To enable or disable this feature use the !link show_event_publish_donate_message config option</a>.',['!link'=>'<a href="'.$base_url.'/configure/misc-config#show_event_publish_donate_message">']),
		];

	// ********** emails

	$res['admin_email']=
		[':init'=>['default'=>false,
				   'hosted'=>$emailDomain===false ? '' : t('admin@').$emailDomain],
		 'type'=>'email',
		 ':section'=>'emails',
		 ':level'=>0,
		 'title'=>ct('Admin email'),
		 'description'=>ct('The email address where technical internal warning or errors messages will be sent. This is mandatory.'),
		];

	$res['contact_email']=
		[':init'=>['default'=>false,
				   'hosted'=>$emailDomain===false ? '' : t('contact@').$emailDomain],
		 'type'=>'email',
		 'required'=>true,
		 ':section'=>'emails',
		 ':level'=>0,
		 'title'=>ct('Contact email'),
		 'description'=>ct('The main email that the public can use to contact your site. This email displayed in several places on the site. It is also used as a "From" address when Demosphere sends certain emails. If you are on a hosted site that is configured to receive email on our server, then this is also an alias (see panel > Admin:email aliases and boxes).'),
		 ':cfg_postsave'=>'demosphere_config_moderator_emails_postsave',
		];

	$res['email_subscription_from']=
		[':init'=>['default'=>false,
				   'hosted'=>$emailDomain===false ? '' : 'NOREPLY@'.$emailDomain],
		 'type'=>'email',
		 ':section'=>'emails',
		 ':level'=>0,
		 'title'=>ct('Event email From address'),
		 'description'=>ct('The "From" address for email sent by the event subscription system.'),
		];

	$res['email_subscription_disable_message']=
		[':init'=>['default'=>false,],
		 'type'=>'textarea',
		 ':section'=>'emails',
		 ':level'=>0,
		 'title'=>ct('Subscription disable message'),
		 'description'=>ct('If this field is not empty, then emails will not be sent to subscribers. This message will be displayed to users that visit the email subscription configuration page. For example, this can be used to avoid sending lots of emails during summer holidays.'),
		 ':example'=>'Emails subscriptions have been suspended from July 15 to Aug. 20.',

		];

	$res['email_senders']=
		[':init'=>[
				'default'=>[['uid'=>0,
							 'name'=>t('The !site_name team',['!site_name'=>$siteName]),
							 'email'=>$emailDomain===false ? 
							 $demosphere_config['contact_email'] : // if no domain, contact_email is set during install
							 t('contact@').$emailDomain]],],
		 'type'=>'array_of_arrays',
		 'aofa-options'=>[['uid',ct('user id'),'int'],
						  ['name',ct('name')],
						  ['email',ct('email')],
						 ],
		 ':section'=>'emails',
		 ':level'=>0,
		 'title'=>ct('Email senders'),
		 'description'=>ct('This determines the "From" email address (and the name in the signature) that will be used to send emails when from the backend event form. You should always at least define one sender with uid "0", that will be the default sender. You can add other senders for specific moderators that want to have other names/signatures.'),
		];

	$res['emails']=
		[':init'=>['default'=>[
					['name'=>t('event is being published (can edit)'),
					 'subject'=>t('%sitename:%short_start_time- %title'),
					 'body'=>t("Hello,\n\nYour event on %start_time has been published on %sitename.\n%event_url\nIts title is : %title\nPlease contact us if you find any mistakes in this announcement.\n\nYou can also edit this event yourself by following this link:\n%self-edit-link\n\n\nSincerely,\n\n%first_name\n\n(Ref.%event_id)")], 
					['name'=>t('event is being published'),
					 'subject'=>t('%sitename:%short_start_time- %title'),
					 'body'=>t("Hello,\n\nYour event on %start_time has been published on %sitename.\n%event_url\nIts title is : %title\nPlease contact us if you find any mistakes in this announcement.\n\nIf you are not an organizer for this event, sorry for this mistake. If you happen to know the contact of the organizers, please send it to us.\n\n\nSincerely,\n\n%first_name\n\n(Ref.%event_id)")], 
					['name'=>t('more information needed'),
					 'subject'=>t('question for %sitename:%short_start_time- %title'),
					 'body'=>t("Hello,\n\nTo publish on %sitename the event at %start_time whose title is :\n   %title\nwe need the following information:\n\nIf you are not an organizer for this event, sorry for this mistake. If you happen to know the contact of the organizers, please send it to us.\n\n\nSincerely,\n\n%first_name\n\n(Ref.%event_id)")], 
					['name'=>t('published but incomplete'),
					 'subject'=>t('incomplete:%sitename:%short_start_time- %title'),
					 'body'=>t("Hello,\n\nWe have published on %sitename the event at %start_time whose title is :\n   %title\nHowever, it is incomplete. We need the following information:\n\nIf you are not an organizer for this event, sorry for this mistake. If you happen to know the contact of the organizers, please send it to us.\n\n\nSincerely,\n\n%first_name\n\n(Ref.%event_id)")], 
					['name'=>t('published but incomplete (can edit)'),
					 'subject'=>t('incomplete:%sitename:%short_start_time- %title'),
					 'body'=>t("Hello,\n\nWe have published on %sitename the event at %start_time whose title is :\n   %title\nHowever, it is incomplete. We need the following information:\n\nYou can either contact us or edit this event yourself by following this link:\n%self-edit-link\n\n\nSincerely,\n\n%first_name\n\n(Ref.%event_id)")], 
					['name'=>t('event rejected'),
					 'subject'=>t('%sitename:%short_start_time- %title'),
					 'body'=>t("Hello,\n\nThanks for suggesting this event for %sitename.\nUnfortunately, this event does not seem to fit with our publishing guidelines. Please contact us if you want more explanation or if you think this is a mistake.\n\nYou can find more information here:\n%publishing_guidelines_url\n\n\nSincerely,\n\n%first_name\n\n(Ref.%event_id)")], 
							  ],],
		 'type'=>'array_of_arrays',
		 'aofa-options'=>[['name',ct('name')],
						  ['subject',ct('subject')],
						  ['body',ct('body'),'textarea'],
						 ],
		 ':cfg_from_form_val'=>'demosphere_config_form_submit_emails_from_form',
		 ':section'=>'emails',
		 ':level'=>0,
		 'title'=>ct('Predefined emails'),
		 'description'=>ct('A list of emails that you can send from the backend form.'),
		];

	$res['auto_repetition_confirm_email_subject']=
		[':init'=>['default'=>t('%site_name: confirm : %title'),],
		 'type'=>'textfield',
		 ':section'=>'emails',
		 ':level'=>0,
		 'title'=>ct('Reptition confirm subject'),
		 'description'=>ct('The subject of the email sent to the organizer of a recurrent event so that he can confirm future recurrences.'),
		 ];

	$res['auto_repetition_confirm_email_body']=
		[':init'=>['default'=>t("Hello,\n\n%sitename would like you to confirm or update a recurring event.\nPlease follow this link:\n%self-edit-link\n\nThe following dates need confirmation:\n%event-list\n\nSincerely,\n\n%site_name\n\n(Ref.%event_id)")],
		 'type'=>'textarea',
		 ':section'=>'emails',
		 ':level'=>0,
		 'title'=>ct('Reptition confirm body'),
		 'description'=>ct('The body of the email sent to the organizer of a recurrent event so that he can confirm future recurrences.'),
		 ];

	// ********** text-matching

	$res['event_text_matching_keep_old']=
		[':init'=>['default'=>30*24*3600,],
		 'type'=>'textfield',
		 ':duration_unit'=>'days',
		 ':cfg_to_form_val'  =>'demosphere_config_form_duration_to_form',
		 ':cfg_from_form_val'=>'demosphere_config_form_duration_from_form',
		 ':section'=>'text-matching',
		 ':level'=>2,
		 'title'=>ct('Event text-matching expire'),
		 'description'=>ct('How many days (after the end of the event) should this event be kept in the text matching index. DB table can be come large if you have many events and this is too big.'),
		];
	$res['text_matching_bad_words']=
		[':init'=>['default'=>explode(',','the,and,you,that,was,for,are,with,his,they,one,have,this,from,had,hot,but,some,what,there,can,out,other,were,all,your,when,use,word,how,said,each,she,which,their,time,will,way,about,many,then,them,would,write,like,these,her,long,www'),
				   'language_fr'=>explode(',','leur,tous,ces,ont,cette,son,contre,metro,org,vous,www,rue,debat,plus,pas,sont,sans,http,aux,source,que,nous,qui,avec,par,sur,dans,est,une,pour,les,des,mais,ete,elle,il,faire,ses,meme,fait,etre,ils'),
				  ],
		 'type'=>'textarea',
		 ':cfg_to_form_val'  =>'demosphere_config_form_word_list_to_form',
		 ':cfg_from_form_val'=>'demosphere_config_form_word_list_from_form',
		 ':section'=>'text-matching',
		 ':level'=>1,
		 'title'=>ct('Bad Words'),
		 'description'=>ct('Bad words for text matching. (very common words). You can use suggest bad words link on the text-matching control panel, to find some of these.'),
		 ':example'=>'leur,tous,ces,ont,cette,son,contre,metro,org,vous,www,rue,debat,plus,pas,sont,sans,http,aux,source,que,nous,qui,avec,par,sur,dans,est,une,pour,les,des,mais,ete,elle,il,faire,ses,meme,fait,etre,ils',
		];


	// ********** feed-import

	$res['feed_import_default_articles_per_page']=
		[':init'=>['default'=>20,],
		 'type'=>'int',
		 ':section'=>'feed-import',
		 ':level'=>0,
		 'required'=>true,
		 'title'=>ct('Articles per page'),
		 'description'=>ct('The default number of articles to display on each page.'),
		];

	$res['feed_import_delete_old_articles']=
		[':init'=>['default'=>150*24*3600,],
		 'type'=>'textfield',
		 ':duration_unit'=>'days',
		 ':cfg_to_form_val'  =>'demosphere_config_form_duration_to_form',
		 ':cfg_from_form_val'=>'demosphere_config_form_duration_from_form',
		 ':section'=>'feed-import',
		 ':level'=>2,
		 'title'=>ct('Delete old articles delay'),
		 'description'=>ct('Number of days after which old articles can be deleted (not finished).'),
		];
	// ********** mail-import
	$res['mail_import_cache_time']=
		[':init'=>['default'=>7*24*3600,],
		 'type'=>'textfield',
		 ':duration_unit'=>'hours',
		 ':cfg_to_form_val'  =>'demosphere_config_form_duration_to_form',
		 ':cfg_from_form_val'=>'demosphere_config_form_duration_from_form',
		 ':section'=>'mail-import',
		 ':level'=>2,
		 'title'=>ct('Cache time'),
		 'description'=>ct('How many hours should cache information be kept.'),
		];

	$res['mail_import_imap_server']=
		[':init'=>['default'=>false,
				   'hosted'=>'{localhost:143/imap/notls}',
				  ],
		 'type'=>'textfield',
		 ':section'=>'mail-import',
		 ':level'=>2,
		 'title'=>ct('IMAP server description'),
		 'description'=>ct('The syntax is <a target="_blank" href="http://php.net/manual/en/function.imap-open.php">described here</a>. Do not use this unless you really know what you are doing. Mail-import *will* mess up your mailbox.'),
		];
	$res['mail_import_imap_inbox']=
		[':init'=>['default'=>false,
				   'hosted'=>'inbox',
				  ],
		 'type'=>'textfield',
		 ':section'=>'mail-import',
		 ':level'=>2,
		 'title'=>ct('IMAP default mailbox name'),
		 'description'=>ct('Typically "inbox". DO NOT CHANGE on hosted sites.'),
		];
	$res['mail_import_imap_username']=
		[':init'=>['default'=>false,
				   'hosted'=>$siteId.'-mailbox@'.$emailDomain],
		 'type'=>'textfield',
		 'attributes'=>['autocomplete'=>'off'],
		 ':section'=>'mail-import',
		 ':level'=>2,
		 'title'=>ct('IMAP username'),
		 'description'=>ct('The username of your IMAP mail account. DO NOT CHANGE on hosted sites.'),
		];
	$res['mail_import_imap_password']=
		[':init'=>['default'=>false,],
		 'type'=>'password',
		 'attributes'=>['autocomplete'=>'new-password'],
		 ':section'=>'mail-import',
		 ':level'=>2,
		 'title'=>ct('IMAP password'),
		 'description'=>ct('The password of your IMAP mail account. DO NOT CHANGE on hosted sites.'),
		];

	// ********** page-watch
	// (no config)

	return $res;
}

?>