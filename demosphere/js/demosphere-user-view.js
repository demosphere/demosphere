// namespace
(function() {
window.demosphere_user_view = window.demosphere_user_view || {};
var ns=window.demosphere_user_view;// shortcut

$(document).ready(function()
{
	// smore: add "..." button that allows user to show/hide lines that have class "smore-line"
	$('.smore-line').parent().each(function()
	{
		var parent=$(this);
		var first=parent.find('.smore-line').first();
		if(first.length===0){return;}
		var container=parent;
		if(container.is('tbody')){container=container.parent();}
		container.addClass('has-smore');
		var last=first.prev();
		var wrap=$('<span class="smore-wrap"><button class="smore-button smore-hidden"/></span>');
		var button=wrap.find('.smore-button');
		// Insert wrap inside container. If we insert at end of body, it will not move when container moves.
		container.before(wrap);
		// Position button by using position of last line.
		var pos=last.offset();
		pos.top+=last.height();
		if(parent.is('ul')){pos.left-=parseInt(parent.css('padding-left'));}
		button.offset(pos);
		// button needs to remember to which smore-line's it is attached (needed when clicked)
		button[0].demosData=this;
	});

	$('body').on('mousedown','.smore-button',function(e)
	{
		if(e.which!=1){return;}
		var parent=$(this.demosData);
		parent.find('.smore-line').toggle();
		var shown=parent.find('.smore-line').first().is(':visible');
		$(this).toggleClass('smore-shown' , shown);
		$(this).toggleClass('smore-hidden',!shown);
	});

	$('#user-data-toggle').mousedown(function(e)
									 {
										 if(e.which!=1){return;}
										 e.preventDefault();
										 $('#user-data').toggle();
										 if($('#user-data').is(':visible'))
										 {
											 $('#main').css('min-height',$('#user-data').height()+$('#user-data').offset().top);
										 }
									 });

});

// end namespace wrapper
}());
