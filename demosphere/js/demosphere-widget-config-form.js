
// Setup all of this pages controls.
// Controls are initialized in the order they appear on the page.
$(document).ready(function()
{
	window.example_last_update=false;

	// auto select text when we focus inside the "embed this" text field
	$('#edit-embed,#edit-embed-advanced').click(function()
    {
		this.select();
		$('#edit-embed-wrapper').removeClass("changed");
	});
	$('#embed-advanced-button').click(function(){$('#edit-embed-advanced-wrapper').toggle();});

	// make the example widget resizeable (jquery-ui) using a small handle on the bottom right
	$("#widget-example").resizable(
	{
		alsoResize: '#widget-example-content',
		start: function(e, ui) 
		{
			// avoid moving everything when we resize 
			$('#widget-example-wrapper').css('minHeight',$('#widget-example-wrapper').height()+'px');
		},
		stop: function(e, ui) 
		{
			$('#edit-width' ).val(ui.size.width);
			$('#edit-height').val(ui.size.height);
			//  this shouldn't be necesary but some times things get out of sync
			$('#widget-example'        ).css('height',$('#edit-height').val()+'px');
			$('#widget-example-content').css('height',(parseInt($('#edit-height').val())-15)+'px');
			$('#widget-example-wrapper').css('minHeight','');			
			example_update_check();
		}
	});

	// **************** appearance controls *********************

	// update example widget's size when user types in width and height text-fields
	var isInit=true;
	$('#edit-width,#edit-height').keyup(function(e)
    {
		window.setTimeout(function()
        {
			var height=$('#edit-height').val();
			var width =$('#edit-width' ).val();
		
			if(width=='')
			{
				$('#widget-example'        ).css('width' ,'');
				$('#widget-example-content').css('width' ,'');
			}
			else
			{
				if(parseInt(width)<10){return;}
				$('#widget-example'        ).css('width' ,width+"px");
				$('#widget-example-content').css('width' ,width+"px");
			}

			if(parseInt(height)<20){return;}
			$('#widget-example'        ).css('height' ,height+"px");
			$('#widget-example-content').css('height' ,(parseInt(height)-15)+"px");

			isInit=false;
		},isInit ? 0 :1500);
	}).triggerHandler('keyup');// set initial example widget size from values in textfields

	// select box for preset input values (for day-header-format and time-format)
	$('.input-suggestion').change(function()
    {
		$(this).parents('div').eq(0).find('input[type="text"]').val($(this).val());
		example_update_check();
	});
	// ie8 bug : use click instead of change
	$('#edit-show-scrollbar').click(function()
    {
		$('#widget-example-content').css('overflowY',
										 $('#edit-show-scrollbar').prop('checked') ? 
										 'auto' : 'hidden');
	}).triggerHandler('click');

	$('.form-item-color input[type="checkbox"]').change(function(){example_update_check();});

	// **************** event rules controls  *********************
 
	demosphere_event_list_form_map_shape();
	demosphere_event_list_form_near_lat_lng({update_callback:example_update_check});

	// **************** add/remove individual events controls  *********************

	// when this fieldset opens, async load user calendar for selecting individual events
	$('#edit-individual-events>legend').mousedown(function()
    {
		if(!$('.individual-events').hasClass('finished-loading'))
		{
			$('.individual-events').addClass('finished-loading');
			load_select_events();
		}
	});

	$("#add-event-city").autocomplete(
		{
			delay: 100,
			source: base_url+"/city/autocomplete"
		});

	$("#add-event button").click(add_external_event);

	// ie8 bug: use click instead of change
	$("#edit-usercal-policy input").click(function()
    {
		$('#select-events').removeClass('policy-0');
		if($(".usercal-policy-0 input").prop('checked'))
		{$('#select-events').addClass('policy-0');}
	}).triggerHandler('click');

	// ****************** reset button
	$('#reset').attr('title',$('#reset-wrapper .description').text());
	$('#reset').click(function(e)
    {
		if(!confirm($('#reset-wrapper .description').text()))
		{
			e.preventDefault();
		}
	});

	// ****************** post set-up **************************** 
	
	// changes in most inputs in this form will require updating the example widget
	add_change_handler_to_all_form_items('#demosphere_widget_config_form',
										 example_update_check,
										 '#edit-embed,'+
										 '#add-event-start-time,'+
										 '#add-event-url,'+
										 '#add-event-title,'+
										 '#add-event-city');
	$('#distance-plus,#distance-minus').click(example_update_check);

	// needed for jscolor picker
	$('.color').change(example_update_check);

	example_update_check();
});

// When values in most fields change we have to update the example widget.
// This functions adds a delay to avoid too frequent ajax requests.
function example_update_check(phase)
{
	if(phase!==true)
	{
		// a small wait is necessary, otherwise values have not been set in form fields yet
		window.setTimeout(function(){example_update_check(true);},1);
	}
	else
	{
		merge_calls('example_update',1000,true,example_update);
	}
}

// When values in most fields change we have to update the example widget
function example_update()
{
	// *** determine options by reading values in each field
	var options=
		{
			'usercal-policy': $('#edit-usercal-policy input[value=1]:checked').length==1 ? 1:0
		};
	if(window.widgetUid!==false){options.save=1;}
	for(var i=0;i<widgetOptions.length;i++)
	{
		var name=widgetOptions[i]['name'];
		if(typeof options[name]!=='undefined'){continue;}// this is a custom option
		var el=$('#edit-'+name);
		if(el.length===0){continue;}// some form items don't exist in user cal form 
		var val;
		if(el.is('.color-empty .form-color')){val='';}
		else
		if(el.is('input[type=checkbox]'))
		{
			val=$('#edit-'+name+":checked").length==1;
		}
		else{val=el.val();}
		options[name]=val;
	}
	//console.log(options);

	// CSRF protection using form tokens
	options.form_token=$('input[name=dlib-form-token-demosphere_widget_config_form],'+
						 'input[name=dlib-form-token-demosphere_user_calendar_config_form]').val();

	// *** ajax request to fetch new version of example calendar
	$.post(base_url+'/widget-config-ajax'+
		   (window.widgetUid!==false ? '?uid='+window.widgetUid : ''), 
		   options,
		  function(data)
		  {
			  $.each(widgetOptions,function(i,v)
              {
				 // console.log(v['name']);
				  $('#edit-'+v['name']).removeClass('error');				  
			  });
			  if(data.error!==false)
			  {
				  //console.log(data.error);
				  $.each(data.error, function(i,v)
				  {
					  $('#edit-'+v).addClass('error');
				  });
				  return;
			  }
			  $('#widget-example-content').html(data.html);
			  $('#demosphere-widget-css').remove();
			  // bug in ie8: 
			  //var style=$('<style type="text/css" id="demosphere-widget-css"></style>');
			  // $('head').append(style.text(data.css));

			  $('head').append('<style type="text/css" id="demosphere-widget-css">'+data.css+'</style>');

			  if($('#edit-embed').val()!=data.embed)
			  {
				  if($('#edit-embed').val()!=''){$('#edit-embed-wrapper').addClass("changed");}
				  $('#edit-embed').val(data.embed);
			  }
			  if($('#edit-embed-advanced').val()!=data.embedAdvanced)
			  {
				  $('#edit-embed-advanced').val(data.embedAdvanced);
			  }
			  
			  //$('#edit-embed-wrapper .description').html(data.embed);
			  // *** check if we need to reload the select events calendar
			  if(typeof window.selectEventsLastOptions!=='undefined' &&
				 window.selectEventsLastOptions!=select_event_options_checksum())
			  {
				  load_select_events();
			  }
		  },'json');	
}

// This "checksum" is used to determine if any values have changed in fields
// that would require a reload of the select-event calendar.
function select_event_options_checksum()
{
	var res= 	''+
		($('#edit-near-lat-lng-use-map'                   ).prop('checked') ? 'y':'n')+';'+
		($('#edit-near-lat-lng-distance'              ).val()                    )+';'+
		($('#edit-near-lat-lng-latitude-and-longitude').val()                    )+';'+
		'';
	$('.select-vocab').each(function(){res+=$(this).val()+';';});
	return res;
}

// Ajax request to load an event calendar with select boxes for selecting individual events.
function load_select_events()
{
	$.post(base_url+'/widget-select-events-calendar?uid='+window.widgetUid, 
		   {},
		   function(data) 
		   {
			   // checksum so that later we can check if this calendar needs reloading
			   window.selectEventsLastOptions=select_event_options_checksum();
			   $('#select-events').html(data);
			   // This adds checkboxes to events so that they can be selected/deselected
			   user_calendar_edit_setup(window.widgetUid,function(){example_update_check();});

			   // open event links in new tab/window
			   $('#select-events td.c2 a').attr('target','_blank');
			   // remove city links
			   $('#select-events td.c3').each(function(){$(this).text($(this).text());});
		   });
}


// Handler for button to add external (user-defined) events.
function add_external_event(e)
{
	if(e!==null){e.preventDefault();}

	// **** check if each input textfield is valid
	
	// dates are checked on server

	if(!$('#add-event-time' ).val().match(
			/^ *([0-9]{1,2}:[0-9]{2})? *$/))
	{
		alert(t('invalid time format: use hh:mm'));
		return;
	}

	if(!$('#add-event-url' ).val().match(
			/^ *https?:\/\//))
	{
		alert(t('invalid url: example: http://example.com/my-event'));
		return;
	}

	if(!$('#add-event-title' ).val().length>0)
	{
		alert('empty title not allowed');
		return;
	}

	var values=[$('#add-event-date' ).val(),
				$('#add-event-time' ).val(),
				$('#add-event-city'	).val(),
				$('#add-event-url'	).val(),
				$('#add-event-title').val()
			   ];

	// ajax request to create the new event
	$.post(base_url+'/user-calendar-external-event?uid='+window.widgetUid,
		   {
			   form_token: $('input[name=dlib-form-token-demosphere_widget_config_form],'+
							'input[name=dlib-form-token-demosphere_user_calendar_config_form]').first().val(),
			   action: 'add',
			   date  : values[0],
			   time  : values[1],
			   city	 : values[2],
			   url	 : values[3],
			   title : values[4]
		   },
		   function(data)
		   {
			   if(data!='add ok'){alert(data);return;}
			   // add a line telling user that his event has been added
			   var res=$('<div></div>');
			   $('#add-event input').val('');
			   $('#add-event-result').prepend(res);
			   res.text(t('Successfully added event:')+' ');
			   var spans=$('#add-event span');
			   for(var i=0;i<5;i++)
			   {
				   res.append($('<strong></strong>').text(spans.eq(i).text()));
				   res.append($('<span></span>').text(values[i]));
			   }
			   // update both example and select calendars
			   example_update_check();
			   load_select_events();
		   });
}
