// namespace
(function() {
window.demosphere_panel = window.demosphere_panel || {};
var ns=window.demosphere_panel;// shortcut


$(document).ready(function()
{
	blocks_init();
	$('.log .button').mousedown(function (e)
	{
		var log=$(this).parent();
		e.preventDefault();e.stopPropagation();
		$('.log').not(log).removeClass('open');
		log.toggleClass('open');
		log.find('.contents').css('max-width',$(window).width()-log.offset().left);
		log.find('.contents').scrollTop(log.find('.event-log').height());
	});
});


function blocks_init()
{
	restore_state();

 	$("#closed-blocks-bg").append($(".panel-block.closed"));
 	$(".panel-block-title").append('<span class="buttons"><a class="prev" href="javascript:void(0)">&lt;</a><a class="next" href="javascript:void(0)">&gt;</a><a class="close" href="javascript:void(0)">X</a></span>');
 	$(".panel-block-title").click(function()
    {
		//console.log("open");
		var block=$(this).parent();
		if(!block.hasClass("closed")){return;}
		block.removeClass("closed");
 		$("#panel-blocks").append(block);
		recompute_block_heights();	
		save_state();
	});
	$(".close").click(function(event)
    {
		event.stopPropagation();
		var block=$(this).parent().parent().parent();
		if(block.hasClass("closed")){return;}
		block.addClass("closed");
		$("#closed-blocks-bg").append($(".panel-block.closed"));
		recompute_block_heights();	
		save_state();
	});
	$(".prev").click(function(event)
    {
		event.stopPropagation();
		var block=$(this).parent().parent().parent();
		block.prev().before(block);
		recompute_block_heights();	
		save_state();
	});
	$(".next").click(function(event)
    {
		event.stopPropagation();
		var block=$(this).parent().parent().parent();
		block.next().after(block);
		recompute_block_heights();	
		save_state();
	});

	recompute_block_heights();
}

// Make each block as high as the highest block on the same line.
// (this is only to make the layout prettier)
function recompute_block_heights()
{
	var maxHeights=[];
	var refs=[];
 	$(".panel-block").css("height",'');
 	$(".panel-block").each(function()
	{
		var p="p"+$(this).position().top;
		maxHeights[p]=Math.max(maxHeights[p] ? maxHeights[p] : 0,$(this).height());
		refs[$(this).attr("id")]=p;
	});
 	$(".panel-block").each(function()
 	{
		$(this).css("height",maxHeights[refs[$(this).attr("id")]]);
	});
}

function restore_state()
{
 	for(var i=0;i<blocks.length;i++)
 	{
		var blockInfo=blocks[i];
 		var block=$('#block-'+blockInfo.nb);
		if(blockInfo.closed)
		{
			block.addClass("closed");
 			$("#closed-blocks-bg").append(block);
		}
		else
		{
			block.removeClass("closed");
 			$("#panel-blocks").append(block);
		}
	}
}

function save_state()
{
	var data='[';
 	$(".panel-block").each(function()
 	{
		var nb=$(this).attr("id").substr('#block-'.length-1);
		data+='{'+
		 	'"nb":'+nb+","+
		 	'"closed":'+($(this).hasClass("closed") ? "true" : "false" )+
			'},';
	});
	data=data.substr(0,data.length-1);
	data+=']';
	//console.log(data);
						   
	$.post(base_url+'/control-panel/ajax-save',
		   {data:data},
		   function(r)
		   {
			   if(r!=='demosphere_panel_ajax_save ok'){alert('save failed');}
		   });
}

// ******************  block contents **************************************


// Exports:

// end namespace wrapper
}());
