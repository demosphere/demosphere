// namespace
(function() {

window.demosphere_map = window.demosphere_map || {};
var ns=window.demosphere_map;// shortcut

// This starts up everything.
// Note: this starts before dom ready
// Warning: two async events:
// 1. dom page load finish (DOM ready)
// 2. demosphere events ajax 
function demosphere_map_init() 
{
	debug_log("get_demosevents_and_start_gmap");
	window.dMap=
		{
			// List of Demosphere events downloaded through ajax 
			demosevents: undefined,
			domIsReady: undefined,
			// The main google.maps.Map object
			googleMap: undefined,
			// The top box containing the slider
			dateSelBox: createDateSelBox(),
			// Currently selected topic
			highlightTopic: false,
			// Whether the eventmarkers are currently being drawn. This is apporximate.
			isDrawing: false,
			// Last time EventMarker.mdraw(false) was called
			lastDraw: 0,
			// Size of the markerpoint. FIXME: this should be computed.
			markerPointWidth: 35,
			markerPointHeight: 17, 
			// List of all EventMarkers (events shown on map using google.maps.OverlayView)
			eventMarkers: [],
			// White popup box, shown when user clicks on an event
			infoWindow: false,
			// The div shown inside the infoWindow
			infoWindowDiv: false,
			// Demosphere Event id for event currently displayed in infoWindow  
			// (only used for to avoid re-opening of same event)
			infoWindowEventId: false,
			createInfoWindow: function()
			{
				this.infoWindowDiv=document.createElement("div");
				this.infoWindowDiv.id="infoWindowDiv";
				this.infoWindowDiv.innerHTML="EventMarker unset";
				this.infoWindow=new google.maps.InfoWindow({content: window.dMap.infoWindowDiv});
			},
			closeInfoWindow: function()
			{
				this.infoWindowEventId=false;
				if(window.dMap.infoWindow===false){return;}
				var imap = window.dMap.infoWindow.getMap();
				if(imap !== null && typeof imap !== "undefined")
				{
					window.dMap.infoWindow.close();
				}
			}
		};


	// async event : DOM ready
	$(document).ready(dom_ready_callback);

	// async event : events:
	// fetch events using ajax 
	var now=new Date();
	var today=new Date(now.getFullYear(),now.getMonth(),now.getDate());
	var nbDays=Math.max(7,initialSliderNbDays*4);
	if(nbDays<7){nbDays=7;}
	get_demosevents(today.getTime(),
					today.getTime()+1000*3600*24*nbDays,
				   	when_dom_and_demosevents_both_ready);
}


function dom_ready_callback()
{
	debug_log("dom_ready_callback");
	window.dMap.domIsReady=true;

	if($("body").hasClass('IE6')){$('#badBrowser').show();}

	$("#bookmarkMapDialog").dialog({ autoOpen: false, width: 300,height: 300,
									 open: function(event, ui) {$("#bookmarkMapDialog").css('width',260);  }});
	//ie bugs: $("#bookmarkMapDialog").css('display','inherit');
	$("#bookmarkMapButton").click(function(){$("#bookmarkMapDialog").dialog('open');});

	$("#bookmarkMapDialog").mouseover(demosphere_map_set_link);
	$("#bookmarkMapLink").mouseover(demosphere_map_set_link);
	$("#bookmarkMapLink").mousedown(demosphere_map_set_link);

	$("#optionTitle").mousedown(function(){$("#optionList").toggle();});
	// set the default options, using the values in the html
	EventMarker.options=[
		{id:'overlap'   ,title:t('overlap'),value:0},
		{id:'fullTitle' ,title:t('title'     ),value:0},
		{id:'strongWord',title:t('keywords' ),value:0},
//		{id:'dbgId'		,title:'debug'	   ,value:0},
		{id:'day'		,title:t('day'	    ),value:0},
		{id:'time'		,title:t('time'	    ),value:0}
	];
	var urlArgs=parse_url_get_args(window.location.href);
	var optionList=document.getElementById('optionList');
	optionList.innerHTML="";

	for(var i=0;i<EventMarker.options.length;i++)
	{
		var o=EventMarker.options[i];
		EventMarker.options[o.id]=o;
		if(urlArgs.hasOwnProperty(o.id))
		{
			o.value=parseInt(urlArgs[o.id]);
		}
		
		var li=document.createElement("li");
		li.id='option_'+o.id;
		li.className="option_"+o.value;
		li.innerHTML="<h4>"+o.title+"</h4><p class='value-line'><span></span><span></span> auto</p>";
		$(li).click((function(idx){return function(){demosphere_map_set_preference(idx);};})(i));
		optionList.appendChild(li);		
	}

	debug_log(EventMarker.options);

	// location list
	$('#locationShortcuts select').change(
		function()
		{
			var map=window.dMap.googleMap;
			var sel=JSON.parse(this.value);
			map.setCenter(new google.maps.LatLng(sel.lat,sel.lng),sel.zoom);
		});

	googlemap_setup();

	document.getElementById('topicSelect').addEventListener('click',function(e){e.preventDefault();});
	document.getElementById('topicSelect').addEventListener('mousedown',highlight_term);
}

function googlemap_setup()
{
	debug_log("googlemap_setup");

	var sizes=compute_map_sizes();
	$("#wrap").css("height",sizes.windowHeight+"px");
	$("body" ).css("height",sizes.windowHeight+"px");
	debug_log(sizes);

	// *** Create google map instance
	var urlArgs=parse_url_get_args(window.location.href);

	var center=new google.maps.LatLng(demosphere_config.map_center_latitude,
						   demosphere_config.map_center_longitude);

	if(urlArgs.hasOwnProperty('lat') && 
	   urlArgs['lat'].match(/^[0-9.-]+$/)!==null &&
	   urlArgs['lng'].match(/^[0-9.-]+$/)!==null    )
	{
		center=new google.maps.LatLng(urlArgs['lat'],urlArgs['lng']);
	}

	var zoom=demosphere_config.map_zoom;
	if(urlArgs.hasOwnProperty('zoom')){zoom=parseInt(urlArgs['zoom']);}

	debug_log("pos:",center);
	debug_log("zoom:",zoom);

	$('#map').width (sizes.mapWidth );
	$('#map').height(sizes.mapHeight);

    var map = new google.maps.Map(document.getElementById("map"),
                                   {
									   center: center,
									   zoom: zoom,
									   mapTypeId: google.maps.MapTypeId.ROADMAP,
									   disableDefaultUI: true,
									   zoomControl: true,
									   gestureHandling: 'greedy'
								   });

	window.dMap.googleMap=map;

	window.dMap.createInfoWindow();

	EventMarker.initPrototype();

	// If user ressizes browser window, change map size.
	$(window).resize(function(){window.setTimeout(resize_window,500);});

	google.maps.event.addListener(map, 'zoom_changed',
								  function()
								  {
									  window.dMap.isDrawing=false; // Force recompute of options
									  $('.shifting').removeClass('shifting');
									  // Reset overlap information to show real pos during animated zoom, and force call to checkOverlaps after that.
									  for(var i=0,l=window.dMap.eventMarkers.length;i<l;i++)
									  {
										  var mi=window.dMap.eventMarkers[i];
										  mi.moved=false;
										  mi.fixedPos=false;
										  mi.lastPos=false;
									  }
									  window.dMap.closeInfoWindow();
								  });
	google.maps.event.addListener(map, 'dragstart',
								  function()
								  {
									  $('.shifting').removeClass('shifting');
								  });
	when_dom_and_demosevents_both_ready();
}

function when_dom_and_demosevents_both_ready()
{
	debug_log("when_dom_and_demosevents_both_ready (entry)");
	if(window.dMap.domIsReady ==undefined){debug_log("when_dom_and_demosevents_both_ready: dom not ready yet" );return;}
	if(window.dMap.demosevents==undefined){debug_log("when_dom_and_demosevents_both_ready: no demosevents yet");return;}

	debug_log("when_dom_and_demosevents_both_ready (inside)");

	//console.log(GLanguage);
	//alert("language:"+GLanguage.getLanguageCode());

	// initialize dateSelBox
	window.dMap.dateSelBox.init();
	// add markers to map
	recreate_eventmarkers_greyout();	
}

// Fetch all events from selectStartTime to endTime.
// Automatically checks if we already have those events.
function get_demosevents(selectStartTime,endTime,callback)
{
	debug_log("get_demosevents:"+selectStartTime+" - "+endTime);

	// fon't fetch if we already have time range
	if(window.dMap.demosevents!=undefined && 
	   window.dMap.demosevents.selectStartTime<=selectStartTime &&
	   window.dMap.demosevents.endTime  >=endTime  )
	{
		if(callback!=undefined){callback();}		
		return false;
	}
	
	$.getJSON(base_url+"/event-list-json",
			  {
				  selectStartTime:Math.floor(selectStartTime/1000),
				  endTime  :Math.floor(endTime/1000),
				  place__latitude: true,
				  place__longitude: true,
				  place__zoom: true,
				  topics: true,
				  url: true,
				  random  :Math.random()
			  },
			  function(d,s)
			  {get_demosevents_finished(s,{selectStartTime:selectStartTime,
										   endTime:  endTime,
										   events:d},callback);}
			 );

	return true;
}
  
function get_demosevents_finished(status,de,callback) 
{
	debug_log("get_demosevents_finished status:"+status);
	window.dMap.demosevents=de.events;

	// compute a few extra things in events
	for (var i = 0,l=window.dMap.demosevents.events.length; i < l ; i++) 
	{
		var e=window.dMap.demosevents.events[i];
		e.jsStartTime=e.startTime*1000;
		e.jsDate=new Date(e.jsStartTime);
		e.shortHour =format_date('short-time',e.jsDate); 
		e.hour      =format_date('time'     ,e.jsDate); 
		e.day       =format_date('short-day',e.jsDate); //("%a. %e");
		e.dayMonth  =format_date('day-month',e.jsDate);  //("%d/%m");
		e.fullDate	=format_date('full-date',e.jsDate);  //("%A %d %B %Y ");
		if(e.htmlTitle.match(/<strong>[^<]*<\/strong>/))
		{
			e.strongWord=e.htmlTitle.match(/<strong>[^<]*<\/strong>/g).join(" - ");
			e.strongWord=e.strongWord.replace(/<.?strong>/g,"");
		}
		else
		{
			e.strongWord='---';
		}
		e.topics=e.topics==='' ? [] : e.topics.split(',');
		e.topics=e.topics.map(parseInt);
		var min=false,idx;
		for(var j=0;j<e.topics.length;j++)
		{
			idx=demosphereTopics.indexOf(e.topics[j]);
			if(min===false || (idx!==-1 && idx<min)){min=idx;}
		}
		e.firstTopic=min===false ? false : demosphereTopics[min];
		//console.log(e);
	}

	if(callback!=undefined){callback();}
}	

function recreate_eventmarkers_greyout()
{
	$("body").addClass('busy');
	window.setTimeout(
		function()
		{
			recreate_eventmarkers();
		},50);
}

function recreate_eventmarkers()
{
	debug_log("recreate_eventmarkers");
	// grey out bar 
	$("body").addClass('busy');

	//debug_log(window.dMap.events);
	var map=window.dMap.googleMap;

	// Clear Overlays and remove all existing EventMarkers:
	while(window.dMap.eventMarkers[0]){window.dMap.eventMarkers.pop().setMap(null);}

	// if infoWindow is open, close it
	window.dMap.closeInfoWindow();

	// Now add displayable events (whose time is ok) to map.
	// Do it in 2 seperate loops: we need to know how many 
	// events will be displayed, before we start adding them.
	window.dMap.demosevents.displayed=[];
	for (var i = 0,l=window.dMap.demosevents.events.length; i < l ; i++) 
	{
		//debug_log("considering event "+i);
		//if(i!=13 && i!=11  && i!=8 && i!=16)continue;
		var e=window.dMap.demosevents.events[i];
		if(e.place__latitude==0 || e.place__longitude==0){continue;}
		if(e.jsStartTime<window.dMap.dateSelBox.sliderStart ||
		   e.jsStartTime>window.dMap.dateSelBox.sliderEnd     ){continue;}

		if(window.dMap.highlightTopic)
		{
			var found=false;
			if(window.dMap.highlightTopic==='others'){found=e.topics.length===0;}
			else
			{
				for(var k=0,kl=e.topics.length;k<kl;k++)
				{if(e.topics[k]==window.dMap.highlightTopic){found=true;break;}}
			}
			if(!found){continue;}
		}
		window.dMap.demosevents.displayed.push(e);
	}
	$('#dynamicInfo').text(window.dMap.demosevents.displayed.length+" "+
						   t("displayed events"));
	for (var i = 0,l=window.dMap.demosevents.displayed.length; i < l ; i++) 
	{
		//debug_log("*** adding event "+i);
		var e=window.dMap.demosevents.displayed[i];
		var point = new google.maps.LatLng(e.place__latitude,e.place__longitude);
 		var emarker=new EventMarker(point,e);
 		emarker.setMap(map);
	}
	//dbgsleep(2000);

	// don't un-greyout bar here. Un-greyout is done after
	// checkOverlaps which is delayed.
}

function highlight_term(event)
{
	if(event.which!==1){return;}
	event.preventDefault();
	var topicButton=false;
	if(event.target.nodeName==="A"   ){topicButton=event.target.parentNode;}
	if(event.target.nodeName==="SPAN"){topicButton=event.target;}
	var term=topicButton===false ? false : topicButton.id.replace(/^sel/,'');
	if(topicButton===false || topicButton.classList.contains('highlighted')){term=false;}

	if(term==='cl'){term=false;}
	window.dMap.highlightTopic=term;
	$("#topicSelect span").removeClass("highlighted");
	if(term===false)
	{
		$("#topicSelect span").removeClass("unhighlighted");
	}
	else
	{
		$("#topicSelect span").addClass("unhighlighted");
		$("#sel"+term).addClass("highlighted");
	}
	recreate_eventmarkers_greyout();
	return false;
}

function resize_window()
{
	var sizes=compute_map_sizes();
	$("#wrap").css("height",sizes.windowHeight+"px");
	$("body" ).css("height",sizes.windowHeight+"px");
	$("#map" ).css("width" ,sizes.mapWidth    +"px");
	$("#map" ).css("height",sizes.mapHeight   +"px");
	google.maps.event.trigger(window.dMap.googleMap, 'resize');
}

function compute_map_sizes()
{
	var y0=$("#map").offset().top;
	var windowHeight=0;

	debug_log("typeof window.innerHeight:"+typeof window.innerHeight);
	if(typeof window.innerHeight!='undefined'){windowHeight=window.innerHeight;}
	else
	{
		// ie 6+ strict
		if(typeof document.documentElement.clientHeight!=='undefined' && document.documentElement.clientHeight!=0){windowHeight=document.documentElement.clientHeight;}
		else
		{
			windowHeight=document.body.clientHeight;
		}
	}
	debug_log("windowHeight="+windowHeight);
	windowHeight=Math.min(700,windowHeight);
	windowHeight=Math.max(100+y0+2,windowHeight);
	//console.log("windowHeight="+windowHeight,windowHeight-y0-2);
	var h=windowHeight-y0-2;// 2=border
	return {mapHeight:h,
			mapWidth:850-1/*1=border*/,
			windowHeight:windowHeight
		   };
}
function demosphere_map_set_preference(num)
{
	debug_log('demosphere_map_set_preference:'+num);
	debug_log(num);
	var o=EventMarker.options[num];
	var v=parseInt($("#option_" +o.id).attr('class').substr(7,1));
	v=(1+v)%3;
	o.value=v;
	debug_log(o.value);

	$("#option_" +o.id).attr('class','option_'+v);

	// refresh map
	recreate_eventmarkers_greyout();
}
function demosphere_map_set_link()
{
	debug_log('demosphere_map_set_link');
	var url=window.location.href;
	if(url.indexOf('#')!=-1){url=url.substr(0,url.indexOf('#'));}
	url+="#";
// 	{id:'fullTitle' ,title:'titre'    ,value:false,auto:true},
// 	{id:'strongWord',title:'mots clés',value:false,auto:true},
// 	{id:'dbgId'     ,title:'debug'    ,value:false,auto:true},
// 	{id:'day'       ,title:'jour'     ,value:false,auto:true},
// 	{id:'time'      ,title:'heure'    ,value:false,auto:true},

	for(var i=0;i<EventMarker.options.length;i++)
	{
		var o=EventMarker.options[i];
		if(o.value==0){continue;}
		url+=o.id+"="+o.value+"__";
	}	
	var dstart=window.dMap.dateSelBox.sliderStart-window.dMap.dateSelBox.start;
	if(dstart>1000*3600*1)
	{
		url+="dstart="+Math.floor(dstart/1000)+"__";
	}

	var dlength=window.dMap.dateSelBox.sliderEnd-window.dMap.dateSelBox.sliderStart;
	if(Math.abs(dlength-1000*3600*24)>1000*3600*1)
	{
		url+="dlength="+Math.floor(dlength/1000)+"__";
	}
	// map position
	var zoom  =window.dMap.googleMap.getZoom();
	var center=window.dMap.googleMap.getCenter();
	url+="zoom="+zoom+"__";
	url+="lat="+center.lat()+"__";
	url+="lng="+center.lng()+"__";
	

	debug_log("url: "+url);
	$("#bookmarkMapLink").attr('href',url);
}

function createDateSelBox()
{
	var a= 
{
	box: undefined,
	slider: undefined,
	tickmarks: undefined,
	start:undefined,
	end:  undefined,
	sliderStart:undefined,
	sliderEnd  :undefined,
	leftHandleWidth: 8,
	rightHandleWidth: 8,

	init: function()
	{
		debug_log("dateSelBox::init");
		this.box      =document.getElementById("dateSelBox");
		this.slider   =document.getElementById("dateSlider");
		this.tickmarks=document.getElementById("tickmarks");
		
		var urlArgs=parse_url_get_args(window.location.href);

		var now=new Date();
		var today=new Date(now.getFullYear(),now.getMonth(),now.getDate());

		var nbDays=Math.max(7,initialSliderNbDays*4);
		this.start=today.getTime();
		this.end  =this.start+1000*3600*24*nbDays;
		/* needed so that left handle (8px) can move left of current day */
		this.start-=(8/840)*(this.end-this.start);

		// setup slider position
		var s=today.getTime();
		if(urlArgs.hasOwnProperty('dstart'))
		{
			s+=1000*parseInt(urlArgs.dstart);
		}
		var e=s+1000*3600*24*initialSliderNbDays -1000;
		if(urlArgs.hasOwnProperty('dlength'))
		{
			e=s+1000*parseInt(urlArgs.dlength);
		}
		this.moveSliderTo(s,e);

		this.drawTicks();
		// setup slider using jqueryui resizable and draggable with options
		debug_log("dateSelBox::init jqueryui");
		$("#dateSlider").resizable(
			{ containment: 'parent', 
			  start: function (){$(this).addClass('moving');},
			  stop: function (e,u){$(this).removeClass('moving');
								   window.dMap.dateSelBox.slideEvent(e,u);},
			  handles: 'e, w'});
		$("#dateSlider").draggable(
			{ containment: 'parent', 
			  start: function (){$(this).addClass('moving');;},
			  stop: function (e,u){$(this).removeClass('moving');
								   window.dMap.dateSelBox.slideEvent(e,u);},
			  axis: 'x'});


		debug_log("dateSelBox::init jqueryui over");

		var t=this;
		$("#dateSelBox").mousedown(
			function(e)
			{
				if(e.which!=1){return;}// only left button
				// Ignore clicks on slider or its handles
				if(e.target && ($(e.target).hasClass('ui-draggable') || $(e.target).hasClass('ui-resizable-handle'))){return;}
				//debug_log(e.target,window.zozo);
				//debug_log("#dateSelBox:mousedown"+this.nodeName);
				e.preventDefault();

				var sx=$("#dateSlider").offset().left;
				var w =$("#dateSlider").width();
				var x =e.clientX;
				if(x<sx || x>sx+w){t.clickMoveToDay(e);}
			});

		$("#dateTextStart").keydown(function(e){if(e.keyCode==13)window.dMap.dateSelBox.dateTextChange();});
		$("#dateTextEnd"  ).keydown(function(e){if(e.keyCode==13)window.dMap.dateSelBox.dateTextChange();});
		$("#dateTextSubmit").click(function(e){window.dMap.dateSelBox.dateTextChange();});

		$(".dateRangeButton").click(function(e){window.dMap.dateSelBox.dateRangeButton(this.id);});

	},

	moveSliderTo: function(start,end)
	{
		this.sliderStart=start;
		this.sliderEnd  =end;
		this.slider.style.left =(this.dateToPos(start)-
								 this.leftHandleWidth)+"px";
		this.slider.style.width=(this.dateToPos(end  )-
								 this.dateToPos(start))+"px";
		//debug_log("moveSliderTo start:"+this.sliderStart+
		// 		  " end:"+this.sliderEnd+
		// 		  " style left:"+this.slider.style.left+
		// 		  " width:"+this.slider.style.width);
		this.dateTextUpdate();
	},

	clickMoveToDay: function(event)
	{
		var day=day_at(this.posToDate(event.clientX-
									  $("#dateSelBox").offset().left));
		var start=day;
		// check if past end
		if(day+this.sliderEnd-this.sliderStart > this.end)
		{
			start=this.end-(this.sliderEnd-this.sliderStart);
		}
		this.moveSliderTo(start,start+this.sliderEnd-this.sliderStart);
		this.timeHasChanged();
	},

	drawTicks: function()
	{
		this.tickmarks.innerHTML='';
		var now=new Date().getTime();
//		var s=new Date(this.start);
// 		var first=(new Date(s.getFullYear(),s.getMonth(),
// 							s.getDate(),s.getHours())).getTime();
		var showMonthInLabel=(Math.abs(this.start-now)>1000*3600*24*5 ||
							  Math.abs(this.end  -now)>1000*3600*24*8 );
		var showDayTicks =Math.abs(this.end-this.start)<1000*3600*24*31;
		var showWeekTicks=Math.abs(this.end-this.start)<1000*3600*24*31*7;
		var first=day_at(this.start);
 		var lastLabel=undefined;
		for(var tick=first;tick<=this.end;tick=closest_day(tick+1000*3600*24))
		{
			//debug_log(tick,this.end,new Date(tick),new Date(this.end));
			var d=new Date(tick);
			var pos=this.dateToPos(tick,true);
			//debug_log("tick at:"+tick+" pos="+pos);
			var classes="";
			if(d.getHours()==0)
			{
				if(d.getDay() ==1){classes="monday";}
				if(d.getDate()==1){classes="month";}
				if(!showDayTicks  && classes==''      ){continue;}
				if(!showWeekTicks && classes=='monday'){continue;}
				var title=format_date(showMonthInLabel ? "short-day-month" : "short-day",d);
				if(lastLabel!=undefined && (pos-lastLabel)<7*8){title=undefined;}
				else{lastLabel=pos;}
				this.drawTick(pos,title,classes);
			}
		}
		$("#dateSelBox,#tickmarks li,#tickmarks").on('mousedown',function(e){e.preventDefault();});
	},
	drawTick: function(pos,title,extraClassnames)
	{
		var tm=document.createElement("li");
		tm.className="tickmark";
		tm.style.left=pos+"px";
		//if(width!=undefined){tm.style.width=width;}
		if(extraClassnames!=undefined){tm.className+=" "+extraClassnames;}
		this.tickmarks.appendChild(tm);
		if(title!=undefined)
		{
			var label=document.createElement("span");
			label.className="label";
			label.innerHTML=title;
			tm.appendChild(label);
		}


	},

	slideEvent: function(event,ui)
	{
		var x=parseInt(this.slider.style.left)+this.leftHandleWidth;
		var w=parseInt(this.slider.style.width);
		this.sliderStart=this.posToDate(x  );
		this.sliderEnd  =this.posToDate(x+w);

		//debug_log("slideEvent   start:"+this.sliderStart+
		//          " end:"+this.sliderEnd+
		// 		    " style left:"+this.slider.style.left+
		// 		    " width:"+this.slider.style.width);

		var move=false;
		var snap =1000*60*60*2;// 2 hour
		var snapx=10;// 20 px
		var d=0;
		// snap start to day
		d=closest_day(this.sliderStart);
		if(Math.abs(this.sliderStart-d)<snap){this.sliderStart=d+1000;move=true;}
		var xd=this.dateToPos(d);
		var xs=this.dateToPos(this.sliderStart);
		if(Math.abs(xs-xd)<snapx)			 {this.sliderStart=d+1000;move=true;}
		
		// snap end to day
		d=closest_day(this.sliderEnd  );
		if(Math.abs(this.sliderEnd	-d)<snap){this.sliderEnd  =d-1000;move=true;}
		var xd=this.dateToPos(d);
		var xs=this.dateToPos(this.sliderEnd);
		if(Math.abs(xs-xd)<snapx)			 {this.sliderEnd  =d-1000;move=true;}

		if(move){this.moveSliderTo(this.sliderStart,this.sliderEnd);}

		this.dateTextUpdate();
		this.timeHasChanged();
	},
	dateRangeButton: function(id)
	{
		var start=this.start;
		var end  =this.end  ;
		if(id=='dateSelBoxLeftTop') {start-=1000*3600*24*4;}
		if(id=='dateSelBoxLeftBot') {start+=1000*3600*24*4;}
		if(id=='dateSelBoxRightTop'){end  -=1000*3600*24*4;}
		if(id=='dateSelBoxRightBot'){end  +=1000*3600*24*4;}
		var minSize=1000*3600*24*4;
		if((end-start)<minSize)
		{
			if(start!=this.start){end  =start+minSize;}
			else                 {start=end  -minSize;}
		}
		this.resetRange(start,end);
	},
	dateTextUpdate: function()
	{
		document.getElementById("dateTextStart").value=
			format_date('short-full-date-time',this.sliderStart);
		document.getElementById("dateTextEnd"  ).value=
			format_date('short-full-date-time',this.sliderEnd);
	},

	dateTextChange: function()
	{
		debug_log("dateTextChange");
		var start=parse_date($("#dateTextStart").val()).getTime();
		var end  =parse_date($("#dateTextEnd"  ).val()).getTime();

		if(start<this.start || end>this.end)
		{
			this.resetRange(Math.min(start,this.start),
							Math.max(end  ,this.end  ) );
		}

		if(Math.abs(end-this.sliderEnd)<(1000*60*5))
		{
			this.moveSliderTo(start,start+this.sliderEnd-this.sliderStart);
		}
		else
		{
			this.moveSliderTo(start,end);
		}
		this.timeHasChanged();
	},

	resetRange: function(start,end)
	{
		start=day_at(start);
		end  =day_at(end+1000*3600*24);
		if(this.start==start && this.end==end){return;}
		this.start=start;
		this.end  =end;
		this.drawTicks();
		var sStart=this.sliderStart;
		var sEnd  =this.sliderEnd;
		// check if slider still in range. if not: move it.
		if(sStart>this.end  -1000*3600*6)
		{sEnd  =this.end;  sStart=sEnd  -(this.sliderEnd-this.sliderStart);}
		if(sEnd  <this.start+1000*3600*6)
		{sStart=this.start;sEnd  =sStart+(this.sliderEnd-this.sliderStart);}
		var moved=sStart!=this.sliderStart || sEnd!=this.sliderEnd;
		this.moveSliderTo(sStart,sEnd);
		get_demosevents(this.start,this.end,
						moved ? recreate_eventmarkers_greyout : undefined);
	},
	timeHasChanged: function()
	{
		$("body").addClass('busy');
		get_demosevents(this.sliderStart,this.sliderEnd,
						recreate_eventmarkers_greyout);
	},

	posToDate: function(pos)
	{
		var w=this.box.clientWidth;
		return Math.round(this.start+((this.end-this.start)*pos)/w);
	},

	dateToPos: function(date,round)
	{
		var w=this.box.clientWidth;
		var pos=((date-this.start)*w)/(this.end-this.start);
		return round==undefined ?  pos : Math.round(pos);
	}

};
	return a;
}
function closest_day(time)
{
	var d0=day_at(time);
	var d1=day_at(time+1000*3600*24);
	return time-d0 < d1-time ? d0 : d1;
}
function day_at(time)
{
	var t=new Date(time);
	return (new Date(t.getFullYear(),t.getMonth(),t.getDate())).getTime();
}

function parse_date(str)
{
	//debug_log("parse_date:"+str);
	var matches=
		str.match(/([0-9]+)\/([0-9]+)(\/([0-9]+))? *([0-9]+)?[h:]?([0-9][0-9])?/);
	//debug_log(matches)
	var now=new Date();

	var m=2,d=1;
	// FIXME: middle endian for USA not rest of english lang (GB...)
	if($('html').attr('lang')==="en"){m=1;d=2;}

	var year =matches[3]==undefined ? now.getFullYear() : parseInt(matches[4]);
	var month=parseInt(matches[m].replace(/^0*/,''))-1;
	var day  =parseInt(matches[d].replace(/^0*/,''));
	var hour =matches[5]==undefined ? undefined : parseInt(matches[5].replace(/^0/,''));
	var min  =matches[6]==undefined ? undefined : parseInt(matches[6].replace(/^0/,''));

	var date;
	if(hour==undefined){date=new Date(year,month,day);}
	else
	if(min ==undefined){date=new Date(year,month,day,hour);}
	else               {date=new Date(year,month,day,hour,min);}
	return date;
}

// EventMarker constructor
// A EventMarker represents an event on the map.
function EventMarker(position,event) 
{
	//console.log('EventMarker constructor',position,event);
	this.position = position;
	this.event = event;
	this.moved=false;
	this.fixedPos=false;
	this.lastPos =false;

	this.dbgId=window.dMap.eventMarkers.length;
	window.dMap.eventMarkers.push(this);

}

EventMarker.initPrototype=function ()
{
EventMarker.prototype = new google.maps.OverlayView();

EventMarker.prototype.onAdd=function ()
{
	//console.log('onAdd',this);
	// Create the DIV representing our rectangle
	this.div = document.createElement("div");
	this.div.className="event";

	var self=this;
	this.div.onmousedown=function (e){self.clicked(e);};

	// add this child at the right z-index pane
	this.getPanes().overlayMouseTarget.appendChild(this.div);
};


EventMarker.prototype.clicked=function (e0)
{
	var e=e0;

	// IE7,IE8
	if(e0==null)
	{
		e=event;
		if(e.button!=1){return;}
	}
	else
	// other browsers
	{
		if(e.which!=1){return;}
		//console.log('clicked',this);
		e.preventDefault();
		e.stopPropagation();
	}

	// Close if it is already opened for this event
	var imap = window.dMap.infoWindow.getMap();
	if(imap !== null && typeof imap !== "undefined" && 
	   this.event.id==window.dMap.infoWindowEventId)
	{
		window.dMap.closeInfoWindow();
		return;
	}

	window.dMap.infoWindowEventId=this.event.id;

	// Create html for infowindow
	var html="";
	window.dMap.infoWindowDiv.className="t"+this.event.topics.join(" t");
	//html+=this.dbgId+" :: ";
	html+='<span id="iwDateTime">'+this.event.fullDate+" - ";
	html+=this.event.hour+'</span>';
	html+='<p id="iwTitle">'+this.event.htmlTitle+'</p>';
	html+='<a href="'+this.event.url+'" target="_blank">'+
		t("more information")+'</a>';
	window.dMap.infoWindowDiv.innerHTML=html;

	// set position and open infowindow
	var ppos=new google.maps.Point(parseInt($(this.div).css('left'))+this.div.clientWidth/2 ,
								   parseInt($(this.div).css('top' ))+this.div.clientHeight/2 );
	var pos=this.getProjection().fromDivPixelToLatLng(ppos);
	window.dMap.infoWindow.setPosition(pos);
	window.dMap.infoWindow.open(this.map);
}


// Redraw the rectangle based on the current projection and zoom level
EventMarker.prototype.draw =function() {this.mdraw(false);}
EventMarker.prototype.mdraw=function(isDrawAfterOverlap)
{
	//console.log('draw',this);

	var zoom=this.map.getZoom();
	var useMarkerPoint=(zoom>=13) && !this.moved;

	var html="";
	html+='<div class="eventmarker '+(this.event.firstTopic!==false ? 't'+this.event.firstTopic : '')+'">';
	
	if(!isDrawAfterOverlap)
	{
		window.dMap.lastDraw=Date.now();
		if(window.dMap.isDrawing==false)
		{
			window.dMap.isDrawing=true;
			EventMarker.computeCurrentOptions();
			window.setTimeout(EventMarker.redrawingFinished,100);
		}
	}
	var o=EventMarker.options;

	// build marker text content
	var text="";
	var selBox=window.dMap.dateSelBox;
	//if(o['dbgId'	 ].current){text+=this.dbgId+" :: ";}
	if(o['day'		 ].current){text+=((selBox.end-selBox.start)<1000*3600*24*15 ?  this.event.day:this.event.dayMonth)+" ";}
	if(o['time'		 ].current){text+=this.event.shortHour+" - ";}
	if(o['strongWord'].current){text+=this.event.strongWord;}
	if(o['fullTitle' ].current){text+=this.event.htmlTitle;}
	text=text.replace(/[ -]*$/,'');
	html+=text;
	if(useMarkerPoint)
	{
		html+='<span><i></i><i></i></span>';
	}
	html+="</div>";
	this.div.innerHTML=html;
	
	var w=this.div.clientWidth;
	var h=this.div.clientHeight;

	var pixelPos;

	if(this.fixedPos===false){pixelPos=this.pixelPosition(useMarkerPoint);}
	else
	{
		// fixedPos is computed for a lastPos, but we may haved panned since that.
		var basePos=this.getProjection().fromLatLngToDivPixel(this.position);
		pixelPos={x:this.fixedPos.x+basePos.x-this.lastPos.x,
				  y:this.fixedPos.y+basePos.y-this.lastPos.y};
	}

	if(o['overlap'].current)
	{
		pixelPos.x+=Math.floor((Math.random()-.5)*w/8);
		pixelPos.y+=Math.floor((Math.random()-.5)*h/4);
	}

	// Now position our DIV 
	this.div.style.left=pixelPos.x+"px";
	this.div.style.top =pixelPos.y+"px";

	// Lower markers should apear on top, to avoid markerpoint
	// on top of neigboring marker.
	this.div.style.zIndex=Math.round(pixelPos.y);

	//debug_log("redrawing");
}

EventMarker.prototype.pixelPosition=function(useMarkerPoint)
{
	var w=this.div.clientWidth;
	var h=this.div.clientHeight;
	var pos=this.getProjection().fromLatLngToDivPixel(this.position);
	pos.x-=w/2;
	pos.y-=h/2;
	if(useMarkerPoint)
	{
		pos.y-=window.dMap.markerPointHeight;
	}
	return pos;
}


// Remove the main DIV from the map pane
EventMarker.prototype.onRemove = function() 
{
	//console.log('onRemove');
	this.div.parentNode.removeChild(this.div);
	this.div=null;
}

EventMarker.computeCurrentOptions=function()
{
	// **** setup options
	var zoom=window.dMap.googleMap.getZoom();
	var numEvents=window.dMap.demosevents.displayed.length;
	var o=EventMarker.options;
	var selBox=window.dMap.dateSelBox;
	var overlap   =o['overlap'   ].value ? o['overlap'   ].value==1 : false;
	if(o['overlap'   ].value==0)//auto
	{
		if(zoom>=17){overlap=numEvents>350;}
		if(zoom==16){overlap=numEvents>250;}
		if(zoom==15){overlap=numEvents>200;}
		if(zoom==14){overlap=numEvents>150;}
		if(zoom<=13){overlap=numEvents>100;}
	}
	var fullTitle =o['fullTitle' ].value ? o['fullTitle' ].value==1 : false;
	var strongWord=o['strongWord'].value ? o['strongWord'].value==1 : 
		(zoom>=(numEvents > 50 ? 15 : 14 ));
	var dbgId=false;// =o['dbgId'     ].value ? o['strongWord'].value==1 : false;
	var day       =o['day'       ].value ? o['day'       ].value==1 : 
	day_at(selBox.sliderStart)!=day_at(selBox.sliderEnd);
	var time      =o['time'      ].value ? o['time'      ].value==1 : !day;

	o['overlap'  ].current=overlap;
	o['fullTitle'].current=fullTitle;
	o['strongWord'].current=strongWord;
	//o['dbgId'].current=dbgId;
	o['day'].current=day;
	o['time'].current=time;
}
EventMarker.redrawingFinished=function()
{
	// Wait a bit, to make sure drawing is really finished. During animated zoom, this avoids jerks.
	if((Date.now()-window.dMap.lastDraw)<200)
	{
		window.setTimeout(EventMarker.redrawingFinished,100);
		return;
	}

	window.dMap.isDrawing=false;
	if(EventMarker.options['overlap'].current==false)
	{
		EventMarker.checkOverlaps();
	}
	$("body").removeClass('busy');
}

// Event  markers are rectangular  areas.  This moves the  event markers
// around so  that none of the rectangles overlap.
EventMarker.checkOverlaps=function()
{
	debug_log("checkOverlaps");

	// Avoid checkOverlaps if not necessary. This avoids a few unnecessary computations during panning (and maybe spurious movement).
	// We don't have a reliable way to know if there has been a significant geometry change.
	// So we just check if pixel positions have changed.
	var needsRebuild=false;
	if(!window.dMap.eventMarkers.length){return;}
	var m0=window.dMap.eventMarkers[0];
	if(m0.lastPos===false){needsRebuild=true;}
	else
	{
		var lastPos0=m0.lastPos;
		var pos0=m0.getProjection().fromLatLngToDivPixel(m0.position);
		for(var i=0,l=window.dMap.eventMarkers.length;i<l;i++)
		{
			var mi=window.dMap.eventMarkers[i];
			var pos=mi.getProjection().fromLatLngToDivPixel(mi.position);
			if(Math.max(Math.abs( (pos.x-pos0.x) - (mi.lastPos.x-lastPos0.x)),
						Math.abs( (pos.y-pos0.y) - (mi.lastPos.y-lastPos0.y)))>2)
			{
				needsRebuild=true;
				break;
			}
		}
	}
	if(!needsRebuild){return;}

	// Compute lastPos. Needed both by previous checks and for computing translated fixedPos in draw()
	for(var i=0,l=window.dMap.eventMarkers.length;i<l;i++)
	{
		var mi=window.dMap.eventMarkers[i];
		mi.lastPos=mi.getProjection().fromLatLngToDivPixel(mi.position);
	}
	
	var zoom=window.dMap.googleMap.getZoom();
	// compute corner positions
	for(var i=0,l=window.dMap.eventMarkers.length;i<l;i++)
	{
		var mi=window.dMap.eventMarkers[i];
		mi.fixedPos=mi.pixelPosition(zoom>=13);
		mi.corner={x:mi.fixedPos.x+mi.div.clientWidth,
				   y:mi.fixedPos.y+mi.div.clientHeight};
		mi.moved=false;
		mi.isFull=false;// when no faces are empty, just ignore (speedup)
	}

	// An  array which keeps track  of which faces of  each object are
	// already "occupied".
	// This is only used to speed up.
	var allPossiblePos=[];
	for(var i=0,l=window.dMap.eventMarkers.length;i<l;i++)
	{
		for(let p=0;p<4;p++)
		{
			allPossiblePos.push({empty: true,index:i,face:p});
		}
	}

	// Find a non overlapping position for each marker.
	// Alogrithm: place a marker mi by finding a position that does not
	// overlap any previously placed marker (mj, j<i).
	for(var i=1,l=window.dMap.eventMarkers.length;i<l;i++)
	{
		var mi=window.dMap.eventMarkers[i];
		//if(mi.dbgId==31){debug_log("**************** mi:"+mi.dbgId);}
		var ix0=mi.fixedPos.x;
		var iy0=mi.fixedPos.y;
		var ix1=mi.corner.x;
		var iy1=mi.corner.y;
		var iw=ix1-ix0;
		var ih=iy1-iy0;
		var previousMarkers=window.dMap.eventMarkers.slice(0,i);
		var pos0={x:ix0,y:iy0};
		
		if(!mi.overlapsList(previousMarkers)){continue;}
		//if(mi.dbgId==31){debug_log("building candidates");}
		mi.moved=true;

		// Compute all possible candidate positions for mi.
		var candidatePositions=[];
		for(var j=0;j<i;j++)
		{
			var mj=previousMarkers[j];
			if(mj.isFull){continue;}
			var jx0=mj.fixedPos.x;
			var jy0=mj.fixedPos.y;
			var jx1=mj.corner.x;
			var jy1=mj.corner.y;

			// Candidate positions are on  all 4 sides of previousy 
			// placed markers.
			var positions=[
				{x:jx0      , y:jy0-ih-1},//top
				{x:jx1+1    , y:jy0     },//right
				{x:jx0-iw-1 , y:jy0     },//left
				{x:jx0      , y:jy1+1   },//bottom
			];
			for(let p=0;p<4;p++)
			{
				if(allPossiblePos[j*4+p].empty==false){continue;}
				var d=dist2(positions[p],pos0);
				candidatePositions.push({dist2:d,
										 pos:positions[p],
										 index:j,
										 face:p});
			}
		}

		// sort candidate positions acording to distance
		candidatePositions.sort(function (a,b)
							{
								if(a.dist2 < b.dist2){return	 -1;}
								if(a.dist2 > b.dist2){return	  1;}
								return 0;							 
							});
		
		// use the first (closest) non overlapping candidate
		var found=false;
		for(var j=0,lj=candidatePositions.length;j<lj;j++)
		{
			//debug_log(candidatePositions[j]);
			var c=candidatePositions[j];
			mi.fixedPos.x=c.pos.x;
			mi.fixedPos.y=c.pos.y;
			mi.corner.x=mi.fixedPos.x+iw;
			mi.corner.y=mi.fixedPos.y+ih;
			if(!mi.overlapsList(previousMarkers))
			{
				found=true;
				occupy_face(allPossiblePos,c.index,  c.face);
				occupy_face(allPossiblePos,      i,3-c.face);
				break;
			}
		}
		if(found==false){alert("EventMarker:bug:no candidate position found!");
						 continue;}

	}

	// Redraw all markers
	for(var i=0,l=window.dMap.eventMarkers.length;i<l;i++)
	{
		window.dMap.eventMarkers[i].div.classList.add('shifting');
		window.dMap.eventMarkers[i].mdraw(true);
	}

	// change slider color to say "we're over"
	debug_log("finished checkOverlaps");
}

function occupy_face(allPossiblePos,index,face)
{
	allPossiblePos[index*4+face ].empty=false;
	// if none empty, set the whole marker to "full"
	for(var f=0;f<4;f++){if(allPossiblePos[index*4+f].empty)return;}
	window.dMap.eventMarkers[index].isFull=true;
}

// Square distance function.
function dist2(a,b)
{
	var dx=a.x-b.x;
	var dy=a.y-b.y;
	return dx*dx+dy*dy;
}

// Does this marker overlap wih any marker in the list.
EventMarker.prototype.overlapsList= function(list)
{
	for(var i=0,l=list.length;i<l;i++)
	{
		if(this.overlaps(list[i])){return true;}
	}
	return false;
}

// Does this marker (rectangle) overlap with another marker.
EventMarker.prototype.overlaps= function(other)
{
	return !(this.fixedPos.x>other.corner.x ||
			 this.fixedPos.y>other.corner.y ||
			 this.corner.x<other.fixedPos.x ||
			 this.corner.y<other.fixedPos.y   );
}
}

function dbgsleep(ms)
{
	var now=Date.now();
	while(Date.now()<(now+ms)){;}
}

function parse_url_get_args(url)
{
	if(url.indexOf('#')==-1){return {};}
	var t=url.substr(url.indexOf('#')+1);
	var parts=t.split('__');
	var res={};
	for(var i=0,l=parts.length;i<l;i++)
	{
		var nameVal=parts[i].split('=');
		res[decodeURIComponent(nameVal[0])]=decodeURIComponent(nameVal[1]);
	}
	//debug_log('has:',res.hasOwnProperty('c'),res['c']);
	return res;
}

window.mydbglog="";
function debug_log()
{
	return;
	//alert("debug_log:1:"+(typeof console));
	//alert("debug_log:2:"+(typeof console.log));
	if((typeof console    ) =="undefined" ||
	   (typeof console.log)!=="function"    )
	{
		//alert("log");
		window.mydbglog+="log: "+arguments[0]+"<br/>\n";
		//document.getElementById("tmpJsDebug").innerHTML=window.mydbglog;
		//window.setTimeout(function (){document.getElementById("tmpJsDebug").innerHTML=window.mydbglog;},1000);
		return;
	}
	//console.log(arguments[0]);
	console.log.apply(console.log,arguments);
}


// *************************************
// *************************************

function format_date(description,date)
{
	var lang;
	if(typeof lang==='undefined'){lang=$('html').attr('lang');}
	switch(lang)
	{
	case 'fr': return format_date_fr(description,date);
	case 'el': return format_date_el(description,date);
	case 'de': return format_date_de(description,date);
	case 'es': return format_date_es(description,date);
	default: return format_date_fr(description,date);
	}
}
function format_date_el(description,date)
{
	//debug_log(typeof date);
	if(typeof date=='number'){date=new Date(date);}
	var shortWeekDays=demosphere_map_locale_data.shortWeekDays;
	var longWeekDays =demosphere_map_locale_data.longWeekDays
	var months       =demosphere_map_locale_data.months;
	var weekDay=date.getDay();
	var day    =date.getDate();
	var year   =date.getFullYear();
	var hours  =date.getHours();
	var min    =date.getMinutes();
	var month  =date.getMonth()+1;
	var month0=month < 10 ? "0"+month : month;
	var day0  =day   < 10 ? "0"+day   : day;
	var min0  =min   < 10 ? "0"+min   : min;
	var hours0=hours < 10 ? "0"+hours : hours;
	
	switch(description)
	{
	case 'time':
		if(hours==3 && min==33){return 'χωρίς ώρα';}
		return hours0+"h"+min0;
	case 'short-time':
		if(hours==3 && min==33){return 'όχι ώρα';}
		return hours0+"h"+min0;
	case 'short-day':
		return shortWeekDays[weekDay]+" "+day;
	case 'short-day-month':
		return shortWeekDays[weekDay]+" "+day+"/"+month0;
	case 'day-month':
		return day0+"/"+month0;
	case 'full-date':
		return longWeekDays[weekDay]+" "+day0+" "+months[month-1]+" "+year;
	case 'short-full-date-time':
		return day0+"/"+month0+"/"+year+" "+hours0+":"+min0;
	default: return 'FIXME format_date';
	}
}
function format_date_en(description,date)
{
	//debug_log(typeof date);
	if(typeof date=='number'){date=new Date(date);}
	var shortWeekDays=demosphere_map_locale_data.shortWeekDays;
	var longWeekDays =demosphere_map_locale_data.longWeekDays
	var months       =demosphere_map_locale_data.months;
	var weekDay=date.getDay();
	var day    =date.getDate();
	var year   =date.getFullYear();
	var hours  =date.getHours();
	var min    =date.getMinutes();
	var month  =date.getMonth()+1;
	var month0=month < 10 ? "0"+month : month;
	var day0  =day   < 10 ? "0"+day   : day;
	var min0  =min   < 10 ? "0"+min   : min;
	var hours0=hours < 10 ? "0"+hours : hours;
	
	switch(description)
	{
	case 'time':
		if(hours==3 && min==33){return 'no time';}
		return hours0+":"+min0;
	case 'short-time':
		if(hours==3 && min==33){return 'no t.';}
		return hours0+":"+min0;
	case 'short-day':
		return shortWeekDays[weekDay]+" "+day;
	case 'short-day-month':
		return shortWeekDays[weekDay]+" "+day+"/"+month0;
	case 'day-month':
		return day0+"/"+month0;
	case 'full-date':
		return longWeekDays[weekDay]+" "+day0+" "+months[month-1]+" "+year;
	case 'short-full-date-time':
		return month0+"/"+day0+"/"+year+" "+hours0+":"+min0;
	default: return 'FIXME format_date';
	}
}

function format_date_fr(description,date)
{
	//debug_log(typeof date);
	if(typeof date=='number'){date=new Date(date);}
	var shortWeekDays=demosphere_map_locale_data.shortWeekDays;
	var longWeekDays =demosphere_map_locale_data.longWeekDays
	var months       =demosphere_map_locale_data.months;
	var weekDay=date.getDay();
	var day    =date.getDate();
	var year   =date.getFullYear();
	var hours  =date.getHours();
	var min    =date.getMinutes();
	var month  =date.getMonth()+1;
	var month0=month < 10 ? "0"+month : month;
	var day0  =day   < 10 ? "0"+day   : day;
	var min0  =min   < 10 ? "0"+min   : min;
	var hours0=hours < 10 ? "0"+hours : hours;
	
	switch(description)
	{
	case 'time':
		if(hours==3 && min==33){return 'sans heure';}
		return hours0+"h"+min0;
	case 'short-time':
		if(hours==3 && min==33){return 'sans h.';}
		return hours0+"h"+min0;
	case 'short-day':
		return shortWeekDays[weekDay]+" "+day;
	case 'short-day-month':
		return shortWeekDays[weekDay]+" "+day+"/"+month0;
	case 'day-month':
		return day0+"/"+month0;
	case 'full-date':
		return longWeekDays[weekDay]+" "+day0+" "+months[month-1]+" "+year;
	case 'short-full-date-time':
		return day0+"/"+month0+"/"+year+" "+hours0+"h"+min0;
	default: return 'FIXME format_date';
	}
}


function format_date_es(description,date)
{
	//debug_log(typeof date);
	if(typeof date=='number'){date=new Date(date);}
	var shortWeekDays=demosphere_map_locale_data.shortWeekDays;
	var longWeekDays =demosphere_map_locale_data.longWeekDays
	var months       =demosphere_map_locale_data.months;
	var weekDay=date.getDay();
	var day    =date.getDate();
	var year   =date.getFullYear();
	var hours  =date.getHours();
	var min    =date.getMinutes();
	var month  =date.getMonth()+1;
	var month0=month < 10 ? "0"+month : month;
	var day0  =day   < 10 ? "0"+day   : day;
	var min0  =min   < 10 ? "0"+min   : min;
	var hours0=hours < 10 ? "0"+hours : hours;
	
	switch(description)
	{
	case 'time':
		if(hours==3 && min==33){return 'sans heure';}
		return hours0+"h"+min0;
	case 'short-time':
		if(hours==3 && min==33){return 'sans h.';}
		return hours0+"h"+min0;
	case 'short-day':
		return shortWeekDays[weekDay]+" "+day;
	case 'short-day-month':
		return shortWeekDays[weekDay]+" "+day+"/"+month0;
	case 'day-month':
		return day0+"/"+month0;
	case 'full-date':
		return longWeekDays[weekDay]+" "+day0+" "+months[month-1]+" "+year;
	case 'short-full-date-time':
		return day0+"/"+month0+"/"+year+" "+hours0+"h"+min0;
	default: return 'FIXME format_date';
	}
}

function format_date_de(description,date)
{
	//debug_log(typeof date);
	if(typeof date=='number'){date=new Date(date);}
	var shortWeekDays=demosphere_map_locale_data.shortWeekDays;
	var longWeekDays =demosphere_map_locale_data.longWeekDays
	var months       =demosphere_map_locale_data.months;
	var weekDay=date.getDay();
	var day    =date.getDate();
	var year   =date.getFullYear();
	var hours  =date.getHours();
	var min    =date.getMinutes();
	var month  =date.getMonth()+1;
	var month0=month < 10 ? "0"+month : month;
	var day0  =day   < 10 ? "0"+day   : day;
	var min0  =min   < 10 ? "0"+min   : min;
	var hours0=hours < 10 ? "0"+hours : hours;
	
	switch(description)
	{
	case 'time':
		if(hours==3 && min==33){return 'keine zeit';}
		return hours0+":"+min0;
	case 'short-time':
		if(hours==3 && min==33){return 'keine z.';}
		return hours0+":"+min0;
	case 'short-day':
		return shortWeekDays[weekDay]+", "+day;
	case 'short-day-month':
		return shortWeekDays[weekDay]+", "+day+"."+month0;
	case 'day-month':
		return day0+"."+month0;
	case 'full-date':
		return longWeekDays[weekDay]+" "+day0+" "+months[month-1]+" "+year;
	case 'short-full-date-time':
		return day0+"."+month0+"."+year+" "+hours0+":"+min0;
	default: return 'FIXME format_date';
	}
}

// *************************************
// *************************************


// Exports:
window.demosphere_map.demosphere_map_init=demosphere_map_init;
// Global exports:
window.highlight_term=highlight_term;

// end namespace wrapper
}());

demosphere_map.demosphere_map_init();
