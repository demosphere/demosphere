
// namespace
(function() {
window.demosphere_event_multiple_edit = window.demosphere_event_multiple_edit || {};
var ns=window.demosphere_event_multiple_edit;// shortcut
var MEditable=meditable.MEditable;


// This is the main entry point to the JS for multiple edit interface.
$(document).ready(function()
{
	// This forces reload (on firefox) when tab is closed and then restored
	// Without this, tab restore can cause huge damage (mess up all select values!!!)
	window.onbeforeunload=function(){document.body.innerHTML='';};

	$('#changed-places').on('click','button',function()
							{
								var id=$(this).parents('tr').attr('data-id');
								$.post(base_url+'/'+demosphere_config.place_url_name+'/'+id+'/in-charge-changed-ok',
									   function()
									   {
										   $('#changed-places tr[data-id="'+id+'"]').addClass('ok');
									   });
							});

	MEditable.defaultSettings.postUrl=base_url+'/event-multiple-edit-ajax';
	MEditable.defaultSettings.updateData=
		{
			'dlib-form-token-demosphere_event_multiple_edit':$("input[name='dlib-form-token-demosphere_event_multiple_edit']").val()
		};

	// ***** first-future : mark first date in the future
	mark_first_future();

	// ***** repetition reference (only used in repetition list)
	$('.repetition-ref input').click(function(e)
    {
		var row=$(this).parents('tr').first();
		$('.repetition-ref input').not(this).prop('checked',false);
		MEditable.ajax_update({name:'repetition_ref',repetition_ref: true,eid:row.attr('data-eid')},this);
	});

	// ***** date fields (not time)
	add_popup_event_to_highlighted_dates();

	// ***** hover popup for moderation message and place
	var message=$('<div id="message-popup">-</div>');
	var hoverCurrent=false;
	$('body').append(message);
	var address=$('<div id="address-popup">-</div>');
	$('body').append(address);
	$('table.multiple-edit').on('mouseover','.moderation-message,.place',function()
    {
		var jthis=$(this);
		if(hoverCurrent==this || jthis.find('input,textarea').length ||
		  jthis.find('.empty').length || jthis.parents('.auto-uncreated').length!==0){return;}
		var popup=jthis.hasClass('place') ? address : message;
		popup.html(jthis.hasClass('place') ? jthis.html() : meditable.text_to_html(jthis.attr('data-val')));
		var offset=jthis.offset();
		popup.css('left',0); // needed to get correct width
		popup.show();
		popup.css('left',(Math.min(offset.left,$(window).width()-popup.outerWidth())-2)+'px');
		popup.css('top' ,(offset.top -2)+'px');
		hoverCurrent=this;
	});
	$('table.multiple-edit').on('mouseout','.moderation-message,.place',function()
    {
		message.hide();
		address.hide();
		hoverCurrent=false;
	});

	MEditable.defaultSettings.endEditingOverHook=function(meditable)
	{
		var row=meditable.editEl.parents('tr').first();
		flash(row);
	};

	// dont create lots of meditables on page load.
	// Just create an meditable when user actualy clicks on a .delayed-meditable
	$('body').on('mousedown','.delayed-meditable',function(e)
    {
		if(e.which!=1 || e.ctrlKey || e.metaKey || e.shiftKey){return;}
		var editEl=$(this);
		var row=editEl.parents('tr').first();
		editEl.removeClass('delayed-meditable');

		// *****
		// ***** moderation-status
		if(editEl.hasClass('moderation-status'))
		{
			var selectOptions={};
			var isRepetitionsPage=row.has('.repetition-ref').length!=0;
			$.each(demosphere_mstatus,function(n,status)
			{
				if(status=='rejected-shown'&& !demosphere_config['use_rejected_shown']   ){return;}
				selectOptions[n]=demosphere_mstatus_labels[n].title;
			}),
			editEl.meditable(
			{
				name: 'moderationStatus',
				inputType : 'select',
				selectOptions: selectOptions,
				updateData: {eid:row.attr('data-eid')},
				updateHook: function(meditable,response)
				{
					var value=editEl.find('select').val();
					// reset the actual value in case there was an error
					if(typeof response.resetModerationStatus!= 'undefined')
					{
						value=response.resetModerationStatus;
						editEl.attr('data-val',value);
						editEl.attr('data-selected',value);
						editEl.find('select').val(value);
					}
					// update status classes in tr
					row.attr("class",row.attr("class").replace(/\bstatus-[^ ]*\b/g,' '));
					row.addClass('status-'+value);
					row.addClass('status-'+demosphere_mstatus[value]);
					if(response.showOnFrontpage==0){row.removeClass('public');row.addClass   ('private');}
					else                   {row.addClass   ('public');row.removeClass('private');}
					// changes in mode status can change needs attention
					if(typeof response.needsAttention !== 'undefined')
					{
						row.attr("class",row.attr("class").replace(/\bnattention-?[^ ]*\b/g,' '));
						row.addClass('nattention-'+response.needsAttention);
						row.addClass('nattention-level-'+(response.needsAttention==0?0:
														 1+Math.floor(response.needsAttention/100)));
						row.find('div.needs-attention').text(demosphere_nattention[response.needsAttention]);
						row.find('div.needs-attention').attr('data-selected',response.needsAttention);
					}
				}
			});
		}
		else
		// *****
		// ***** needs-attention
		if(editEl.hasClass('needs-attention'))
		{
			var selectOptions=jQuery.extend({}, demosphere_nattention);
			editEl.meditable(
			{
				name: 'needsAttention',
				inputType : 'select',
				selectOptions: selectOptions,
				updateData: {eid:row.attr('data-eid')},
				inputSetup  : function(meditable,input)
				{
					input.find('option').each(function()
					{
						var v=$(this).attr('value');
						var l=(v==0 ? 0 : 1+Math.floor(v/100));
						if(l>0){$(this).addClass('nattention-level-'+l);}
					});
				},
				updateHook: function(meditable,response)
				{
					var val=editEl.find('select').val();
					// update nattention classes in tr
					row.attr("class",row.attr("class").replace(/\bnattention-?[^ ]*\b/g,' '));
					row.addClass('nattention-'+val);
					row.addClass('nattention-level-'+(val==0?0:1+Math.floor(val/100)));
				}
			});
		}
		else
		// *****
		// ***** date fields (day + month)
		if(editEl.hasClass('date'))
		{
			editEl.meditable(
			{
				name: 'date',
				updateData:
				{
					eid:row.attr('data-eid'),
					date_field: editEl.attr('class').match(/month/) ?  "month" : "day"
				},
				updateHook:function(meditable,response)
				{
					row.find('.weekday').not(editEl).text(response.weekday);
					row.find('.day'    ).not(editEl).text(response.day    );
					row.find('.month'  ).not(editEl).text(response.month  );
					row.find('.date'   ).not(editEl).attr('data-val',response.value);
					row.find('td.demosphere_timestamp').attr('data-demosphere-timestamp',
															 response.ts);
				}
			});
		}
		else
		// *****
		// ***** time
		if(editEl.hasClass('time'))
		{
			editEl.meditable(
			{
				name: 'time',
				updateData: {eid:row.attr('data-eid')}
			});
		}
		else
		// *****
		// ***** moderation-message
		if(editEl.hasClass('moderation-message'))
		{
			editEl.meditable(
			{
				name: 'moderationMessage',
				inputType : 'textarea',
				updateData: {eid:row.attr('data-eid')},
				inputSetup: function(meditable)
				{
					var input=meditable.input;
					var text=input.val();
					input.css('height',(Math.max(2,text.split("\n").length)*1.5)+'em');
				}
			});
		}
		else
		// *****
		// ***** title
		if(editEl.hasClass('title'))
		{
			editEl.meditable(
   			{
				name: 'title',
				updateData: {eid:row.attr('data-eid')},
				inputSetup  : function(meditable,input)
				{
					var w=row.find('.message-and-title-wrap').width()-
						  row.find('.moderation-message').width();
					w=Math.min(w,585);// title has max width
					input.parent().width(w);
					input.width(w-12);

					// title length warning message
					input.keyup(function(e)
					{
						var titleSize=html_text_width(input.val().replace(/_/g,''),{'font-weight':'bold'});
						var city=row.find('.place').contents().filter(function(){return this.nodeType == 3;});
						city=city.length ? city[0].nodeValue : '';
						var citySize=html_text_width(city);
						if(citySize>demosphere_config.city_max_width){citySize=demosphere_config.city_max_width;}
						// 15 extra margin ; 5 avoid conflicts with php validation
						var maxTot=demosphere_config.event_title_max_width+demosphere_config.city_max_width-15-5;
						var ok=titleSize+citySize<=maxTot;

						var msg=$('#title-length');
						if(msg.length==0)
						{
							msg=$('<span id="title-length"/>').text(t('titleTooLong'));
							input.after(msg);
						}
						msg.toggleClass("show-msg",!ok);
					});
				}
			});
		}
		else
		// *****
		// ***** place
		if(editEl.hasClass('place'))
		{
			var editingOver=false;
			editEl.meditable(
			{
				name: 'placeId',
				inputSetup: function(m,input)
				{
					editingOver=false;
					input.val('');
					input.autocomplete(
					{
						delay: 100,
						source: base_url+'/place-search-autocomplete?no-new-place',
						position: { my : "right top", at: "right bottom" },
						// don't change value in input field when navigating through suggestions with arrow keys
						focus: function(event, ui) {debug_log('focus:');event.preventDefault();},
						// User actually selects an existing place: set place number in input
						select: function(event, ui)
						{
							input.val(ui.item.value);
							m.endEditing();
						}
					});
				},
				updateData: {eid:row.attr('data-eid')},
				// Don't post ajax update until editing is over (user has selected suggestion).
				// This avoids sending letters (instead of place id) as they are typed.
				preUpdateHook: function(m){return editingOver;},
				endEditingHook: function(e,m)
				{
					// If user mouse click on suggestion, then do not end now: value is not yet copied into input.
					// endEditing() will be called from autocomplete select hook.
					if(e && $(e.target).parents('ul.ui-autocomplete').length!=0){return false;}

					// special case: user starts typing and then aborts (clicks elsewhere): 
					if(!/^[0-9]+$/.test(m.input.val())){m.input.val('');}

					editingOver=true;
					return true;
				}
			});
		}
		else
		// *****
		// ***** topics
		if(editEl.hasClass('topics'))
		{
			editEl.meditable(
			{
				name: 'topics',
				inputType : 'select',
				selectOptions: demosphere_topics,
				multipleSelect: true,
				updateData: {eid:row.attr('data-eid')},
				// open the select to the left, to avoid edge of screen
				inputSetup  : function(meditable,input)
				{
					if(input.offset().left+input.outerWidth()>$(window).width()){input.css('right',0);}
				}
			});
		}

		editEl.trigger(e);
	});

	// We need to know when events have changed on the server, so that we can update our display.
	// We can either use websockets or fallback to polling (slower).
	if(demosphere_config.websockets_enabled && typeof WebSocket!=='undefined')
	{
		websockets_setup();
	}
});


//! Make a websockets connection to ws server (/usr/local/bin/demosphere-websockets-server.js)
//! We will receive messages each time an event changes on the server.
//! After that, we still nee to retrieve the actual changes with ajax_get_event_changes()
function websockets_setup()
{
	var webSocket = new WebSocket(base_url.replace(/^http/,'ws')+"/websocket/");
	webSocket.onmessage = function (event) 
	{
		//console.log("received:",event.data);
		ajax_get_event_changes();
	};
    // Special case, should not happen normally. (can happen if demoswebsocket server is restarted, or crashes).
	// Try to reconnect in 15 seconds.
	webSocket.onclose = function()
	{
		//console.log('webSocket connection closed. Restarting in 15s');
        window.setTimeout(function(){websockets_setup();}, 15000);
    };
}


//! Asks server to send any events that have changed
function ajax_get_event_changes()
{
	//console.log('ajax_get_event_changes: ok to process event change notification');
	var eventsLastChanged={};
	$('tr[data-eid]').each(function()
	{
		var eid=this.getAttribute('data-eid');
		var changed=this.getAttribute('data-change-number');
		// there can be multiple rows of same event: use min last changed timestamp
		if(typeof eventsLastChanged[eid]==='undefined'){eventsLastChanged[eid]=changed;}
		else{eventsLastChanged[eid]=Math.min(eventsLastChanged[eid],changed);}
	});
	$.post(base_url+"/event-multiple-edit-ajax-get-changes",
		  {
			  events: JSON.stringify(eventsLastChanged),
			  isRepetition:$('#edit-add-daily-repetitions-wrapper').length==1
		  },
		  function(response)
		  {
			  $.each(response,function(eid,eventInfo)
			  {
				  $('tr[data-eid="'+eid+'"]').each(function()
				  {
					  var row=$(this);
					  // Don't change a row that has changed or that is being edited
					  if(eventInfo.changeNumber>row.attr('data-change-number') &&
						 row.find('.meditable.editing,.meditable.endEditing').length===0)
					  {
						  //console.log('replacing!!!',eventInfo.changed,row.attr('data-change-number'));
						  var newRow=$(eventInfo.html);
						  row.replaceWith(newRow);
						  flash(newRow);
						  mark_first_future();
					  }
				  });
			  });
			  $('table.pending-nattention .needs-attention.small-needs-attention').removeClass('small-needs-attention');
			  add_popup_event_to_highlighted_dates();
		  });
}


// ***** first-future : mark first date in the future
function mark_first_future()
{
	$('.multiple-edit tr.first-future').removeClass('first-future');
	$('.multiple-edit').each(function()
	{ 
		var now=Date.now();
		var prev=null;
		$(this).find('tr').each(function()
		{
			var date=$(this).find('td.demosphere_timestamp').attr('data-demosphere-timestamp');
			var ts=Date.parse(date.replace(' ','T'));
			if(prev!==(ts>now))
			{
				if(prev!==null){$(this).addClass('first-future');}
				prev=ts>now;
			}
		});
	});
}

//! use css3 transitions to temporarilly flash a fading yellow background color on an element
function flash(el)
{
	//console.log('flash',el);
	var initialColor=el.css('background-color');
	el.removeClass('transition');
	el.css('background-color','yellow');
	window.setTimeout(function()
	{
		el.addClass('transition');
		window.setTimeout(function()
		{
			el.css('background-color',initialColor);
			window.setTimeout(function()
			{
				el.removeClass('transition');
				el.css('background-color','');
			},800);
 		},1);
 	},1);
}

function debug_log()
{
 	//console.log.apply(null,arguments);
}

// Exports:
//window.demosphere_event_multiple_edit.xyz=xyz;

// end namespace wrapper
}());
