$(document).ready(function()
{
	var table=$('.dbobject-ui-list');
	table.addClass('no-popup');

	// Add visual queue for draging on last col
	table.find('tr td:last-child,tr th:last-child').text('');
	table.find('tr td:last-child')
		.html($('<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'));

	// Make table rows draggable (jquery ui sortable)
	table.find('tbody').sortable(
    {
		handle:'td:not(:nth-child(-n+3))',// avoid dragging on first 3 action links
		helper: fixHelperModified ,
		stop: function( event, ui )
		{
			var order=[];
			table.find('tr td:nth-child(4)').each(function(){order.push(parseInt($.trim($(this).text())));});
			$.post(base_url+'/topics-list-order',
				   {order:order,
				   'dlib-form-token-topic-list':$('input[name=dlib-form-token-topic-list]').val()}
				  );
		}
	}).disableSelection();
	
});


var fixHelperModified = function(e, tr) {
    var $originals = tr.children();
    var $helper = tr.clone();
    $helper.children().each(function(index)
    {
      $(this).width($originals.eq(index).width());
    });
    return $helper;
};
