// namespace
(function() {
window.demosphere_place_form = window.demosphere_place_form || {};
var ns=window.demosphere_place_form;// shortcut

ns.placeChangeOnlyHereOldPid=false;

$(document).ready(function()
{
	var isEvent=$('#Event').length!==0;

	if(!isEvent)
	{
		googlemap_refresh();
		googlemap_setup();
	}

	// ********* Choose which place we are editing
	// ********* The following actions are only used if this is an event

	if(isEvent)
	{
		$("#edit-place-change-type input").click(place_change_type_cb);
		$(".place-erase").click(function(e){e.preventDefault();erase_place();});
		update_change_type_options();

		$("#place-suggest").click(place_suggest);

		// Use async query to suggest address from text when address field is empty.
		// This used to be done during page generation, but it can be quite long.
		// Doing this in async speeds up the initial page display time and avoids extra php code.
		if($("#edit-address").val()==''){window.setTimeout(function()
		{
			demosphere_htmledit.wait_for_editor('edit-body',function(){place_suggest(null);});
		},500);}
	}

	// ***

	$("#edit-city").autocomplete(
		{
			delay: 100,
			select: function(){window.setTimeout(function(){check_city_size();});},
			source: base_url+'/city/autocomplete'
		});
	$("#edit-city-wrapper input").keyup(function(){merge_calls('city',500,true,check_city_size);});

	$("#edit-city-wrapper input").keypress(function(e){if(e.which===13){e.preventDefault();}});

	// *** common city (city/district) selector
	$("#citySelect").change(function()
    {
		/* simple $(this).val() fails on ie */
		$("#edit-city-wrapper input").val($(this).find('option:selected').text());
		check_city_size();
	});
	
	// ******** map edit ******

	if(isEvent)
	{
		// open/close the map area
		$('#map-button').click(function(e)
	    {
			$('#place').toggleClass("open-map");
			if($('#place').hasClass("open-map")){googlemap_refresh();}
			$('#map-open-icon').text($('#place').hasClass("open-map") ? '-':'+');
			googlemap_setup();
		});
	}

	// if user hits enter key in map coord/zoom then update map
	$('#map-coords input[type="text"]').keypress(function(e)
    {
		if (e.keyCode=='13'){e.preventDefault();googlemap_update_position_from_form();}
	});

	// manage the "enabled/disabled" text in the map button
	if($('#edit-enable-map').prop('checked')){$('#place').addClass('map-is-enabled');}
	$('#edit-enable-map').click(function(e)
    {
		if($(this).prop('readonly')){return;}
		$('#place').removeClass('map-is-enabled');
		if($('#edit-enable-map').prop('checked'))
		{
			$('#place').addClass('map-is-enabled');
			if($('#edit-zoom').val()==='0')
			{
				$('#edit-zoom').val('15');
				googlemap_update_position_from_form();
			}
		}
	});

	$('#map-open-in-gmap').mousedown(function()
    {
		$(this).attr("href",demosphere_config.country_map_url+'?q='+
					 $('#edit-latitude').val()+','+$('#edit-longitude').val());
	});

	// hide too long city warning when user enters short city
	$('#edit-city-short').keyup(function(){$('#city-too-long-warning').toggle($(this).val()==='');});

	// Show short-city 
	if($('#edit-city-short').val()!==''){$('#edit-city-short-wrapper').css('display','inline');}
	else{check_city_size();}

	if(isEvent)
	{
		// very special case: back/forward browser button
		if($('#variants-selector').length && $('#variants-selector').attr('data-ref-pid')!==$('#edit-referenceId').val())
		{
			$('#variants-selector').remove(); // FIXME: we should ajax request a variants selector
		}
		// Place suggest select handlers
		$('body').on('change','#place-suggestions,#variants-selector',place_selector_change);
	}

	if(!isEvent)
	{
		$("#edit-inChargeId").autocomplete(
			{
				delay: 100,
				source: base_url+'/user/autocomplete'
			});
	}
});

function check_city_size()
{
    var size=html_text_width($("#edit-city-wrapper input").val());
    if(size>demosphere_config.city_max_width)
    { 
		$.get(base_url+'/city/short',
			  {name:$("#edit-city-wrapper input").val()},
			  function(ret)
			  {
				  $('#edit-city-short').val(ret===false ? '' : ret);
				  $('#edit-city-short-wrapper').css('display','inline');
				  $('#city-too-long-warning').toggle(ret===false);
				  if(typeof window.demosphere_event_edit_form!=='undefined')
				  {
					  window.demosphere_event_edit_form.check_title_size();
				  }
			  });
    }
    else
    { 
		$('#edit-city-short').val('');
		$('#city-too-long-warning').hide();
		$('#edit-city-short-wrapper').hide();
		if(typeof window.demosphere_event_edit_form!=='undefined')
		{
			window.demosphere_event_edit_form.check_title_size();
		}
    }
}

// *************************************************
// ****** Choose which place we are editing
// ****** not single place (suggest, change type, ...)
// *************************************************


// Async request to guess
// - dates and times that might be inside the text of this article
// - existing places that might be inside the text of this article
// e===null, means that this is being called from initial page load to guess places.
function place_suggest(e)
{
	$("#place-suggestions-wrapper").show(); // for ie we need to hide it first
	if(e){e.preventDefault();}
	$("#place-suggestions-wrapper").html('please wait');
	if(e){$("#parsed-dates-times-selectors").html('please wait');}
	$("#place-suggestions-wrapper").addClass('waiting');
	if(e){$("#parsed-dates-times-selectors").addClass('waiting');}
	
	var content='';
	if($('#edit-body').length)
	{
		content=e? tinyMCE.get('edit-body').getContent() :
			$('#edit-body').val();
	}

	// cosmetic: flash source of suggestion (place text or body) so that user feels where it comes from
	if($('#edit-address').val().length)
	{
		$('#edit-address').css('background-color',$('#place').is('.readonly') ? '#bdb' : '#dfe');
		window.setTimeout(function(){$('#edit-address').css('background-color','');},300);
	}
	else
	{
		window.setTimeout(function()
		{
			if(typeof tinyMCE==="undefined" || tinyMCE.get('edit-body')===null){return;}
			$(tinyMCE.get('edit-body').getBody()).addClass('search-flash');
			window.setTimeout(function(){$(tinyMCE.get('edit-body').getBody()).removeClass('search-flash');},300);
		},50);
	}

	$.post(base_url+"/suggest-date-time-place", 
		   {
			   body:    content,
			   address: $('#edit-address').val()
		   },
		   function(response)
		   {
			   //console.log(response);
			   $("#place-suggestions-wrapper").removeClass('waiting');
			   $("#parsed-dates-times-selectors"  ).removeClass('waiting');
			   $("#place-suggestions-wrapper").html(response.placeSelector);
			   $("#parsed-dates-times-selectors"  ).html(response.dateSelector+
														 response.timeSelector);
		   },"json");	
}

//! Fill up all place related fields when a place is selected either in place-suggestions or variants-selector
function place_selector_change()
{
	var isVariants=$(this).is('#variants-selector');
	var val=$(this).val();
	// first option "(choose place)" to blank out all form items
	if(val==='0' && isVariants){return;}
	if(val==='0'){erase_place();return;}

	if(isVariants && val==='search')
	{
		variants_search();
		return;
	}

	var place=JSON.parse(val);
	set_place(place);
	if($('body').hasClass('eventAdmin') && !isVariants){load_variants_selector(place.referenceId);}
}

//! Fill up place fields, given a place object
function set_place(place)
{
	var mapEnabled=place.zoom!==0 && place.zoom!=='';

	$('#edit-placeId'    ).val(place.id);
	$('#edit-referenceId').val(place.referenceId);
	$("#edit-city"		 ).val(place.city);
	$('#edit-address'	 ).val(place.address);
	$('#edit-latitude'	 ).val(place.latitude);
	$('#edit-longitude'	 ).val(place.longitude);
	$('#edit-zoom'		 ).val(place.zoom);

	$('#edit-enable-map' ).prop('checked',mapEnabled);

	$('#place').toggleClass('map-is-enabled',mapEnabled);
	if(window.gmap && window.gmap.map){googlemap_update_position_from_form();}
	$('#edit-place-change-type input').prop('checked',false);
	$('.place-change-type-none input').prop('checked',true);
	ns.placeChangeOnlyHereOldPid=false;

	$('#edit-place-in-charge-email').val(place.hasOwnProperty('inChargeEmail') ? place.inChargeEmail : '');
	// FIXME: this is wrong in special case (user selects place currently used only by current event)
	$('#edit-place-is-used-elsewhere').prop('checked',true);
	check_city_size();
	update_change_type_options();
	demosphere_event_edit_form.rebuild_content_info();// email_suggestion_build_list() for  inChargeEmail 
}

function load_variants_selector(refPid)
{
	$("#variants-selector-wrapper").load(
		base_url+"/variants-selector?refPid="+refPid, 
		function(ret)
		{
			// necessary for ie<=8 display
			$("#variants-selector-wrapper").toggle($("#variants-selector-wrapper select").length>0);
		}
	);
}

//! Small search text field displayed when user select "[search]" in variants menu
function variants_search()
{
	$('#variants-selector').val(0);
	var input=$('<input id="variants-search" type="search">').attr('placeholder',t('variants-search'));
	input.autocomplete(
		{
			source: base_url+'/'+demosphere_config.place_url_name+'/'+parseInt($('#edit-referenceId').val())+'/search-variants',
			select: function(event, ui)
			{
				$.get(base_url+'/'+demosphere_config.place_url_name+'/'+parseInt(ui.item.value)+'/json',
					  function(place)
					  {
						  set_place(place);
						  input.remove();
					  });
			},
			position: { my: "left top", at: "left bottom", collision: "fit none" }
		});
	input.blur(function(){input.remove();});
	$('body').append(input);
	input.offset($('#variants-selector').offset());
	input.focus();
}

// Grey-out all address elements. 
function disable_city_and_address_forms()
{
	$("#place").addClass("readonly");
	$("#place input"       ).prop("readonly",true);
	$("#place textarea"    ).prop("readonly",true);
	$("#edit-place-suggest").prop("readonly",false);
	$("#edit-address").removeClass("error");
	$("#edit-place-is-readonly").prop('checked',true);
	$("#citySelect").prop("disabled",true);
	$(".place-change-type input").prop("readonly",false);
}

// Un-Grey-out all address elements. 
function enable_city_and_address_forms()
{
	$("#place").removeClass("readonly");
	$("#place input").prop("readonly",false);
	$("#place textarea").prop("readonly",false);
	$("#citySelect").prop("disabled",false);
	$("#edit-place-is-readonly").prop('checked',false);
}

// Empty all address elements. 
function erase_place()
{
	$('#place-suggestions'       ).prop("selectedIndex",0);
	$('#variants-selector'       ).prop("selectedIndex",0);
	$("#edit-city"               ).val('');
	$("#edit-city-short"         ).val('');
	$('#edit-address'		     ).val('');
	$('#edit-latitude'			 ).val('');
	$('#edit-longitude'			 ).val('');
	$('#edit-zoom'				 ).val('');
	$('#edit-enable-map'         ).prop('checked',false);
	$('#place').removeClass('map-is-enabled');
	$('#edit-placeId'           ).val(0);
	$('#edit-referenceId'        ).val(0);
	$('#edit-place-is-used-elsewhere').prop('checked',false);
	$('#edit-place-change-type input').prop('checked',false);
	$('.place-change-type-none input' ).prop('checked',true);
	$('#edit-place-in-charge-email').val('');
	ns.placeChangeOnlyHereOldPid=false;
	check_city_size();
	update_change_type_options();
}

// user clicks on one of the radio buttons that manage the change type of an existing place
function place_change_type_cb(e)
{
	//console.log('place_change_type_cb',$(e.target).val());
	switch($(e.target).val())
	{
	case "change-only-here" : 
		// change only here creates new place (unless the place is not used elsewhere)
		if($('#edit-placeId').val()!='0' && 
		   $('#edit-place-is-used-elsewhere').is(':checked'))
		{
			ns.placeChangeOnlyHereOldPid=$('#edit-placeId').val();
			$('#edit-placeId').val(0);
		}
		break;
	case "change-everywhere": 
		// normal case: do nothing.
		// special case: the user has previuosly clicked "change-only-here", 
		// we need to restore old Pid
		if($('#edit-placeId').val()=='0' && ns.placeChangeOnlyHereOldPid!==false)
		{
			$('#edit-placeId').val(ns.placeChangeOnlyHereOldPid);			
		}
		break;
	}
	update_change_type_options();
}
function update_change_type_options()
{
	//console.log('update_change_type_options');
	var pid=parseInt($('#edit-placeId').val());
	var ref=parseInt($('#edit-referenceId').val());
	var isEditable=
		pid==0 || 
		!$('#edit-place-is-used-elsewhere').is(':checked') ||
		$('.place-change-type-change-everywhere input').prop('checked');

	// update place-label
	var showLabel='#place-label';
	showLabel+=pid!=0   ? '-p':'-x';
	showLabel+=(ref!=0 && ref!=pid) ? '-r':'-x';

	$('.place-link').attr('href',$('.place-link').attr('href').replace(/[0-9]*$/,pid));
	$('.place-link').text(pid);
	$('.place-ref-link').attr('href',$('.place-ref-link').attr('href').replace(/[0-9]*$/,ref));
	$('.place-ref-link').text(ref);

	$('#place-label span').hide();
	$(showLabel).show();
	
	if(isEditable){enable_city_and_address_forms();}
	else {disable_city_and_address_forms();}
	
	$('#edit-place-change-type-wrapper').toggle(
		$('#edit-place-is-used-elsewhere').is(':checked') ||
		$('.place-change-type-change-only-here input' ).prop('checked') ||
		$('.place-change-type-change-everywhere input').prop('checked') 
	);
}


// *************************************************
//   Interactive Google map code
// *************************************************

function googlemap_async_load_api(callback)
{
    googlemap_async_load_api.callback=callback;
    window.gmap={map:false,geocoder:false,marker:false,apiIsInit:false,initWaitCount:0};
	var url=demosphere_google_map_api_url+'&async=2&callback=demosphere_place_form.googlemap_async_load_api_cb';
    $('head').append('<script src="'+url+'" type="text/javascript"></script>');
}
function googlemap_async_load_api_cb()
{
	window.gmap.apiIsInit=true;
	googlemap_async_load_api.callback();
}

// Full setup of the google map. It loads API if it is not yet loaded, 
function googlemap_setup()
{
	if(!window.gmap){googlemap_async_load_api(googlemap_setup);return;}

	if(!window.gmap.geocoder){window.gmap.geocoder = new google.maps.Geocoder();}

	// only setup googlemap once
	if(window.gmap.map!==false){return;}

	// ******** Center and zoom

	// default center and zoom
	var center=new google.maps.LatLng(demosphere_config.map_center_latitude,
									  demosphere_config.map_center_longitude);
	var zoom=demosphere_config.map_zoom;
	// look into the form elements for actual center and zoom
	if($("#edit-latitude").val()!='' && $("#edit-latitude").val()!='0')
	{
		center=new  google.maps.LatLng(parseFloat($("#edit-latitude" ).val()),
										parseFloat($("#edit-longitude").val()) );
		zoom=parseInt($("#edit-zoom").val());
	}

	// ************ Create google map
    var map =new google.maps.Map(document.getElementById("map"),
                                 {
									 zoom: zoom,
									 center: center,
									 mapTypeId: google.maps.MapTypeId.ROADMAP,
									 gestureHandling: 'greedy'
								 });
	window.gmap.map=map;

	// ********** create marker (the red point)
	var marker = new  google.maps.Marker({position: center,draggable: true});
	window.gmap.marker=marker;
	marker.setMap(map);  

	// ********** setup event listeners
	// Dont use zoom_changed: it calls callback when viewport is not really setup... use bounds_changed
	//google.maps.event.addListener(map,"zoom_changed",      function(){googlemap_update_position(true);});
	google.maps.event.addListener(map   ,"bounds_changed",    function(){googlemap_update_position(true);});
	google.maps.event.addListener(map   ,"idle",              function(){googlemap_update_position(true);});
	google.maps.event.addListener(map   ,"projection_changed",function(){googlemap_update_position();});
	google.maps.event.addListener(marker,"dragend",           function(){googlemap_update_position(true);});

	// ************* the red rectangle that delimits the map image that will be shown 

	// Create an overlay, this is only used to get a MapCanvasProjection that is needed
	// for pixel to lat/lng coordinate conversion (!) :-(
	MyOverlay.prototype = new google.maps.OverlayView();
	MyOverlay.prototype.onAdd = function() { };
	MyOverlay.prototype.onRemove = function() { };
	MyOverlay.prototype.draw = function() { };
	function MyOverlay(map) { this.setMap(map); }
	var overlay = new MyOverlay(map);
	window.gmap.overlay=overlay;

	var rectangle = new google.maps.Polygon({
		paths: [center,center,center,center],
		strokeColor: "#FF0000",
		fillOpacity: 0,
		strokeOpacity: 1
		//strokeWeight: 2,
		//fillColor: "#FF0000",
	});
	rectangle.setMap(map);
	window.gmap.rectangle=rectangle;

	// ***** search input field and button
	$('#map-search button').click(function(e)
    {
		e.preventDefault();
		googlemap_geocode();
	});
	$('#map-search input').keypress(function(e)
    {
		if (e.keyCode=='13'){e.preventDefault();googlemap_geocode();}
	});

	// used to detect changes in position and zoom 
	// (avoid redrawing rectangle too often)
	window.gmap.lastRect=false;

}

// Showing/hidding the map causses it to go strange, this will redraw it.
function googlemap_refresh()
{
	if(!(window.gmap && window.gmap.map)){return;}
	google.maps.event.trigger(window.gmap.map, "resize");
	window.gmap.map.setCenter(window.gmap.marker.getPosition());
}

// Get lat/lng coordinates from the text address that is in the search field.
// This will set the values in the form fields (lat,lng)
// It will also set the position in the map if the map exists.
function googlemap_geocode(zoom,callback)
{
	window.gmap.geocoder.geocode( 
		{ address: $('#map-search input').val()}, 
		function(results, status) 
		{
			if(status==google.maps.GeocoderStatus.OK) 
			{
				var pos=results[0].geometry.location;
				if(window.gmap.map)
				{
					$('#map-search-result-text p').text(results[0].formatted_address);
					$('#map-search-result-text').show();
					window.gmap.map.setCenter(pos);
					window.gmap.map.setZoom(typeof zoom==='undefined' ? 15 : zoom);
					window.gmap.marker.setPosition(pos);
					googlemap_update_position(true);
					$('#edit-enable-map').prop('checked',true);
					$('#place').addClass('map-is-enabled');
				}
				else
				{
					$('#edit-enable-map').prop('checked',true);
					$('#place').addClass('map-is-enabled');
					$('#edit-latitude' ).val(pos.lat());
					$('#edit-longitude').val(pos.lng());
				}
				if(typeof callback !=='undefined'){callback(results);}
			} 
			else 
			{
				alert("Geocode was not successful for the following reason: " + status);
			}
		});
}

// Read values from the form and set map location acordingly.
function googlemap_update_position_from_form()
{
	var newZoom;
	var newCenter;
	if($('#edit-zoom').val()=='' || $('#edit-zoom').val()==0)
	{
		newCenter=new google.maps.LatLng(demosphere_config.map_center_latitude,
										 demosphere_config.map_center_longitude);
		newZoom=demosphere_config.map_zoom;
	}
	else
	{
		newCenter=new google.maps.LatLng($('#edit-latitude' ).val(),
										 $('#edit-longitude' ).val());
		newZoom=parseInt($('#edit-zoom').val());
	}

	window.gmap.marker.setPosition(newCenter);
	window.gmap.map.setZoom(newZoom);
	window.gmap.map.setCenter(newCenter);	
	googlemap_update_position();
}

// This is called when something changes in the map.
function googlemap_update_position(setPos)
{
	//console.log('googlemap_update_position',setPos,window.gmap.map.getZoom());
	var map=window.gmap.map;
	var pos=window.gmap.marker.getPosition();
	var zoom=window.gmap.map.getZoom();

	// if this is an unset map and user changes pos, then auto-enable
	if(setPos && $('#edit-latitude' ).val()==='')
	{
		$('#edit-enable-map').prop('checked',true);
		$('#place').addClass('map-is-enabled');
	}

	// update values in lat,lng,zoom form 
	if(setPos && !$('#edit-latitude' ).prop('readonly'))
	{
		$('#edit-latitude' ).val(pos.lat());
		$('#edit-longitude').val(pos.lng());
		$('#edit-zoom'     ).val(zoom);
	}


	var proj=window.gmap.overlay.getProjection();
	if(proj)
	{
		var pt=proj.fromLatLngToDivPixel(pos);
		var dw=demosphere_config.event_map_width/2;
		var dh=demosphere_config.event_map_height/2;
		var p1=proj.fromDivPixelToLatLng(new google.maps.Point(pt.x-dw,pt.y-dh));
		var p2=proj.fromDivPixelToLatLng(new google.maps.Point(pt.x+dw,pt.y+dh));
		var newRect=[
				proj.fromDivPixelToLatLng(new google.maps.Point(pt.x-dw,pt.y-dh)),
				proj.fromDivPixelToLatLng(new google.maps.Point(pt.x+dw,pt.y-dh)),
				proj.fromDivPixelToLatLng(new google.maps.Point(pt.x+dw,pt.y+dh)),
				proj.fromDivPixelToLatLng(new google.maps.Point(pt.x-dw,pt.y+dh))
			];
	   	// avoid redrawing rectangle too often
		var lastRect=window.gmap.lastRect;
		if(lastRect===false ||
		   newRect[0].lat()!==lastRect[0].lat() || 
		   newRect[0].lng()!==lastRect[0].lng() || 
		   newRect[1].lat()!==lastRect[1].lat() || 
		   newRect[1].lng()!==lastRect[1].lng() || 
		   newRect[2].lat()!==lastRect[2].lat() || 
		   newRect[2].lng()!==lastRect[2].lng() || 
		   newRect[3].lat()!==lastRect[3].lat() || 
		   newRect[3].lng()!==lastRect[3].lng())
		{
			window.gmap.rectangle.setPath(newRect);
			window.gmap.lastRect=newRect;
		}
	}
 	else
 	{
		// Projetction not ready yet. Try again later.
 		if(window.gmap.initWaitCount++<10)
 		{window.setTimeout(function(){googlemap_update_position();},500);}
 	}
}


// Exports:
window.demosphere_place_form.place_suggest=place_suggest;
window.demosphere_place_form.googlemap_async_load_api=googlemap_async_load_api;
window.demosphere_place_form.googlemap_geocode=googlemap_geocode;
window.demosphere_place_form.googlemap_async_load_api_cb=googlemap_async_load_api_cb;
window.demosphere_place_form.erase_place=erase_place;
window.demosphere_place_form.disable_city_and_address_forms=disable_city_and_address_forms;

// end namespace wrapper
}());
