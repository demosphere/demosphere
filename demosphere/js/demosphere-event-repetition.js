$(document).ready(function()
{
	// allow scroll with very large popups
	//$("#main").css("overflow","visible");

	// *************************************************************
	// ****** only on repetitions form
	// *************************************************************


	// Click on [+] of un-created line to create an event (using ajax).
	$('table.multiple-edit').on('click','.create',function()
	{
		var defaults=meditable.MEditable.defaultSettings;
		var tr=$(this).parents('tr');
		var repGroupId=$('#edit-repetition-group').val();
		var data=defaults.updateData;
		data.ts=tr.attr('data-ts');
		$.post(base_url+'/repetition-group/'+repGroupId+'/create-auto',
			   data,
			   function(response)
			   {
				   tr.after($(response));
				   tr.remove();
			   });
	});

	$('#repetition-suggest-dates li').click(function()
    {
		var t=$('#edit-add-date-repetitions');
		t.val($.trim($.trim(t.val())+"\n"+$(this).text()));
	});

	// Repetition Manual / Auto tabs
	$('#repetitions-tabs-line>h2').mousedown(function(e)
	{
		e.preventDefault();
		$('#repetitions-tabs-line>h2').removeClass('active');
		$(this).addClass('active');
		var i=$('#repetitions-tabs-line>h2').index(this);
		$('#repetitions-tabs-bodies>div').removeClass('active');
		$('#repetitions-tabs-bodies>div').eq(i).addClass('active');
	});

	var isInit=true;
	// Change rule type (daily,weekly,...) should show ruleData form items that are specific to that rule type.
	$('#edit-ruleType').change(function()
	{
		var val=$(this).val();
		var tval=ruleTypes[val];
		$('.form-item.ruleData').hide();
		//console.log(val,tval,show[tval]);
		$.each(ruleFields[tval],function(i,v)
			   {
				   $('#edit-ruleData-'+v+'-wrapper').show();
			   });
		if(!isInit)
		{
			$('#edit-minConfirmPrior').val(minConfirmPriors[tval]);
			$('#edit-maxConfirmPrior').val(maxConfirmPriors[tval]);
		}
	}).change();
	isInit=false;

	// change text (cosmetic)
	$('#edit-ruleData-dayOfWeek').change(function()
	{
		$('#auto-body .weekday').text($(this).find('option:selected').text());
	}).change();

	// *************************************************************
	// ****** only on repetitions-copy form
	// *************************************************************

	// open close popups
	var popupZindexCt=5;
	$("div.html-diff").parent().mousedown(function(e)
	{
		e.stopPropagation();
		e.preventDefault();
		var popup=$(this).find("div.html-diff");
		popup.toggle();
		if(popup.css("display")=="block")
		{
			popup.css("z-index",popupZindexCt++);
			var oparent=popup.offsetParent();
			//console.log(oparent,oparent.position(),oparent.width());
			if(popup.position().left+popup.width()>oparent.width()-30)
			{
			    popup.css("right","0px");
			}
		}
	});
	
	$(".copyform td input:checked").parents("td").addClass("copy-selected");
	$(".copyform td input[type=checkbox]").click(function()
	{
		//console.log(this,$(this).prop("checked"));
		var cont=$(this).parents("td");
		if($(this).prop("checked")){cont.addClass   ("copy-selected");}
		else             {cont.removeClass("copy-selected");}
	});

	$('.togglecol').mousedown(function()
	{
	   var field=this.className.match(/toggle-([^ ]+)/)[1];
	   var checkboxes=$('.future .col-'+field+'>input[type="checkbox"]');
	   $(this).toggleClass('toggleon');
	   if($(this).hasClass('toggleon'))
	   {
		   checkboxes.prop('checked',true).parent().addClass('copy-selected');
	   }
	   else
	   {
		   checkboxes.prop('checked',false).parent().removeClass('copy-selected');
	   }
	});
});
