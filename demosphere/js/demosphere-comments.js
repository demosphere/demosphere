$(document).ready(function()
{
	// note need to check if called twice
	if($('#edit-body').hasClass('mce-setup-ok')){return;}
	$('#edit-body').addClass('mce-setup-ok');
	//console.log('setup editor',$('#edit-body').length);
	//console.log(demosphere_tiny_mce_locale);
	add_script(demosphere_tiny_mce_js_path,false,function()
    {
		var tinymceConfig=
			{
				mode : "exact",
				elements : "edit-body",
				plugins: 'link,code,image',
				language : demosphere_tiny_mce_locale,
				menubar: false,
				toolbar: "bold italic | bullist numlist | formatselect | undo redo | link unlink | image hr",
				block_formats: "Paragraph=p;Header 2=h2;Header 3=h3;Header 4=h4",
				relative_urls : false,
				height: "250",
				oninit: function(){$('#comment-form' )[0].scrollIntoView();}
			};
		if(!$('body').hasClass('mobile'))
		{
			tinymceConfig.width="710";
		}
		else
		{
			// mobile browsers zoom editable if font-size is < 16px
			tinymceConfig.content_style='p,h4{font-size: 16px;}';
		}
		tinyMCE.init(tinymceConfig);
	});
});
