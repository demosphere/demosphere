// Setup all of this pages controls.
// Controls are initialized in the order they appear on the page.
$(document).ready(function()
{
	// changes in most inputs in this form will require updating the example widget
	add_change_handler_to_all_form_items('#demosphere_user_calendar_config_form',
										 example_update_check,
										 '#edit-embed,'+
										 '#add-event-start-time,'+
										 '#add-event-url,'+
										 '#add-event-title,'+
										 '#add-event-city,'+
										 '#edit-title,'+
										 '#edit-ispublic'
										);
	$('#edit-title').keyup(usercal_config_update);
	$('#edit-ispublic').click(usercal_config_update);
	if(window.location.href.match(/#select$/))
	{
		window.setTimeout(function()
						  {
							  $("fieldset.individual-events>legend a").click().mousedown();
							  window.setTimeout(function()
							  {				
								  $("html").scrollTop($('a[name="select"]').offset().top);
							  },500);
						  },1);
	}
});
function usercal_config_update(e)
{
	$.post(base_url+'/user-calendar-config-ajax?uid='+window.widgetUid, 
		   {
			   form_token:   $('input[name=dlib-form-token-demosphere_user_calendar_config_form]').val(),
			   title:   $('#edit-title'   ).val(),
			   ispublic:$('#edit-ispublic').prop('checked')
		   },
		  function(data)
		  {
			  $('.error').removeClass('error');
			  $.each(data,function(name,message)
              {
				  $('#edit-'+name+'-wrapper').addClass('error');
			  });
		  },'json');
}