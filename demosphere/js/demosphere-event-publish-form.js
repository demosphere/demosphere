// namespace
(function() {
window.demosphere_event_publish_form = window.demosphere_event_publish_form || {};
var ns=window.demosphere_event_publish_form;// shortcut
var MEditable=meditable.MEditable;

$(document).ready(function()
{
	placeholder_for_old_browsers();

	// optional donate message
	if($('#donate-message').length!==0 && 
	   (document.cookie.match(/publish_donate/)===null || demosphere_config.show_event_publish_donate_message==='debug'))
	{
		show_donate_message();
	}

	// When this event is in "automatic" title mode, dont display the automatic title to avoid confusion
	if(demosphere_config.show_title_in_event==='automatic')
	{
		$('#eventTitle').remove();
	}

	// *********** defaults settings for meditable
	// These settings are also used by other callse to MEditable.ajax_update()
	MEditable.defaultSettings.postUrl=base_url+'/event-publish-form-ajax';
	MEditable.defaultSettings.updateData=
		{
			'dlib-form-token-demosphere_event_publish_form':$("input[name='dlib-form-token-demosphere_event_publish_form']").val(),
			tempId            :   $("input[name='tempId']").val()
		};
	MEditable.defaultSettings.changeNumberElement=$('[data-change-number]');

	// *********** check change number (detect back button / stale page)
	$.get(base_url+'/event-publish-form-get-change-number',
	      {tempId: $("input[name='tempId']").val(),rnd:Math.random()},
		  function(n)
		  {
			  if(n===false){throw new Error("strange:get-change-number event not found?");}
			  if(n!==parseInt($('[data-change-number]').attr('data-change-number')))
			  {
				  var m=window.location.href.match(/&reload=([0-9]+)/);
				  var reload=m===null ? 1 : parseInt(m[1])+1; // avoid reload loop if something is bugging
				  if(reload>10){throw new Error("too many reloads");}
				  window.location.href=window.location.href.replace(/&reload=[0-9]+/,'')+'&reload='+reload;
			  }
		  });

	// *********** misc

	if($('#dateContents .date').attr('data-val')=='')
	{
		$('#dateContents .date').text('');
		$('#frontpage h3').html('&nbsp;');
	}
	if($('#dateContents .time').attr('data-val')=='')
	{
		$('#dateContents .time').text('');
		$('#frontpage tr .c1,.mobile #frontpage .c1').text('-');
	}

	$('#event a').attr("target","_blank");
	$('#event a,#frontpage a').removeAttr("target").attr('href','javascript:void(0)');
	$('#commentLink').removeAttr('onclick');
	$('#demosShareLink').removeAttr('onmousedown');

	// *********** steps (big round numbers on the left)
	var stepCt=1;
	var stepFields=$('<div id="step-fields" class="step"/>');
	stepFields.text(stepCt++);
	$('#event').append(stepFields);
	stepFields.on('mouseenter',function(e){$('#event .meditable,#event .editwrap').addClass('highlight');});
	stepFields.on('mouseleave',function(e){$('#event .meditable,#event .editwrap').removeClass('highlight');});

	if(demosphere_config.show_title_in_event!=='always')
	{
		var stepTitle=$('<div id="step-title" class="step"/>');
		stepTitle.text(stepCt++);
		$('#frontpage').append(stepTitle);
		stepTitle.on('mouseenter',function(e){$('#frontpage .meditable').addClass('highlight');});
		stepTitle.on('mouseleave',function(e){$('#frontpage .meditable').removeClass('highlight');});
	}

	var stepInfo=$('<div id="step-info" class="step"/>');
	stepInfo.text(stepCt++);
	$('#information').append(stepInfo);

	stepInfo.on('mouseenter',function(e){$('#information').addClass('highlightAll');});
	stepInfo.on('mouseleave',function(e){$('#information').removeClass('highlightAll');});


	// *********** text-field / textarea inputs
	setup_information_fields();

	// *********** topics
	setup_topics_editable();

	// *********** date
	$('#dateContents .date').meditable(
    {
		name: 'date',
		placeHolder: t('phDate'),
		inputSetup: function(meditable)
		{
			var input=meditable.input;
			if(demosphere_is_mobile){input.attr('readonly',true);}
			input.datepicker(
			{
				onSelect: function()
				{
					debug_log('datepicker.onSelect');
					meditable.update();
					meditable.endEditing();
				},
				buttonImage: '/demosphere/css/images/datepicker-button.png',
				buttonImageOnly: true,
				//showOn: "button",
				showOn: "focus",
				duration: 0,
				gotoCurrent: true,
				minDate: '+0',
				maxDate: '+1y',
				hideIfNoPrevNext: true,
				// This format is required by server-side parsing, even if it is not shown to user
				dateFormat : demosphere_date_format
			});
		},
		updateHook: function(meditable,data)
		{
			$('#frontpage h3').text(data.text).addClass('updated');
			check_for_event_duplicate();
		},
		endEditingHook: function(e,meditable)
		{
			var over=!e || $(e.target).parents('#ui-datepicker-div').length==0;
			debug_log(over,meditable.input);
			if(over){meditable.input.datepicker('hide');}
			return over;
		}
	});

	// *********** time
	// This is a bit confusing / ugly: 
	// The meditable is a simple <input type="text"/>.
	// But immediately on creation, a <select> is created to show time suggestions.
	// However, browsers don't allow us to open the <select> using JS :-( 
	// Therefore, the <select> is replaced by a jQuery UI selectmenu().
	// The original <input type="text"/> is shown if the user selects "manual".
	$('#dateContents .time').meditable(
	{
		name: 'time',
		placeHolder: ' '+t('phTime'),
		inputSetup: function(meditable)
		{
			var input=meditable.input;
			var timeSuggest=$('.time-suggestions-model').clone();
			timeSuggest.removeClass('time-suggestions-model');
			timeSuggest.attr('id','time-suggestions');
			input.prop('placeholder',t('phTimeManual'));
			input.after(timeSuggest);
			input.hide();
			timeSuggest.val($('#dateContents .time').attr('data-val'));
			timeSuggest.show();
			timeSuggest.selectmenu(
				{
					position: {collision: "flip"},
					select: function(){timeSuggest.change();},
					change: function(){timeSuggest.change();},
					close:  function(){if(input.css('display')==='none'){meditable.endEditing();}}
				});
			timeSuggest.selectmenu("open");

			timeSuggest.on('change',function()
		    {
				if($(this).val()==='manual')
				{
					input.show();
					if(timeSuggest.selectmenu("instance")!== undefined){timeSuggest.selectmenu("destroy");}
					$(this).hide();
					return;
				}
				input.val($(this).val());
				meditable.update();
				meditable.endEditing();
				// Chrome bug: chrome misses the mouseleave and keeps :hover, this forces redraw
				$('.time').hide();window.setTimeout(function(){$('.time').show();},1);
			});
		},
		endEditingHook: function(event,meditable)
		{
			return !event || $(event.target).parents('#time-suggestions-menu').length==0;
		},
		updateHook: function(meditable,data)
		{
			$('#frontpage tr .c1,.mobile #frontpage .c1').text(data.value=='03:33' ? '-' : data.value);
			check_for_event_duplicate();
		}
	});

	// *********** place
	if(demosphere_place_is_empty){place_erase(false);setup_place_search();}
	else                         {setup_place_edit();}

	//$('#vocab'+demosphere_config.topic_vocabulary_id).addClass("meditable");

	// *********** inline title (when show_title_in_event=='always')
	var deTitle=$.trim($('#eventTitle').html());
	$('#eventTitle').attr('data-val',meditable.html_to_text(deTitle.replace(/<\/?strong>/g,'_')));
	$('#eventTitle').meditable(
    {
		name: 'title',
		placeHolder: t('phTitle')
	});

	// *********** body
	$('#htmlView').meditable(
    {
		name: 'body',
		placeHolder: t('phBody'),
		inputType : 'textarea',
		inputSetup: function(meditable)
		{
			debug_log('body: inputSetup');
			var input=meditable.input;
			input.attr('id','edit-body');

			// Adapt height to current height. The height is then slowly increased in tinymce_init()
			demosphereTinyMceConfig.height=$('#htmlView').height();
			tinyMCE.init(demosphereTinyMceConfig);
			var close=$('<div id="body-close" title="'+t('bodyClose')+'">✓</div>');
			close.on('mousedown',function(e){e.stopPropagation();e.preventDefault();meditable.endEditing();close.remove();});
			$('#htmlView').append(close);
		},
		getValue: function(meditable)
		{
			debug_log('body: getValue');
			if(tinyMCE.get('edit-body')===undefined || tinyMCE.get('edit-body')===null || !meditable.hasOwnProperty('editor')){return false;}
			var ebody=$(meditable.editor.getBody());
			var title=ebody.find('#editorTitlePlaceholder');
			if(title.length && title.text()==t('phPageTitle')){title.remove();}
			var para =ebody.find('#editorDescPlaceholder');
			if(para.length && para.text()==t('phDescription')){para.remove();}
			// Use getContent() so that tinymce can cleanup its internal invisible html.
			return meditable.editor.getContent();
		},
		endEditingHook: function(e,meditable)
		{
			if(!e || $(e.target).parents('#htmlView,.mce-container,.ui-dialog,.htmleditor-area-right').length==0)
			{
				// special case: endEditing but tinyMce not yet init
				if(!meditable.hasOwnProperty('editor')){return true;}
				var canFinish=meditable.editor.docconvert.can_finish();
				if(canFinish!==true)
				{
					if(!confirm(canFinish+"\n\n"+t("editorCloseConfirm"))){return false;}
				}
				// Set fixed height before closing, for animation
				$('#htmlView').height($('#htmlView').height());
				return true;
			}
			return false;
		},
		endEditingPreRemoveHook: function(meditable)
		{
			tinyMCE.get('edit-body').remove();
		},
		endEditingOverHook: function(meditable)
		{
			$('#htmlView #eventTitle').remove();
			refresh_floatright_positions();
			// Animate htmlView height so that closing editor is not too "jumpy"
			var h=0;
			$('.textPart').each(function(){h+=10+$(this).height();});
			$('#htmlView').addClass('isClosing');
			$('#htmlView').animate({height:h}, 700, function() {$('#htmlView').css('height',"");$('#htmlView').removeClass('isClosing');});
		}
	});

	// *********** frontpage title
	var title=$.trim($('#frontpage .c2 a').html());
	$('#frontpage .c2').html('');
	$('#frontpage .c2').append($('<div/>').html(title));	
	$('#frontpage .c2 div').attr('data-val',meditable.html_to_text(title.replace(/<\/?strong>/g,'_')));
	$('#frontpage .c2 div').meditable(
    {
		name: 'title',
		inputSetup: function(meditable)
		{
			meditable.input.prop('maxlength','107');
		},
		preUpdateHook: function(meditable,updateData)
		{
			updateData.cityForTitleLength=$('.place .city').text();
			return true;
		},
		placeHolder: t('phFrontpageTitle')
	});	
	$('#frontpage tr td.c3').text($('#frontpage tr td.c3').text());

	// *********** buttons
	var buttons=$('#buttons-line');
	var win=$(window);
	var information=$('#information');
	var firstTime=true;
	// toggle fixed position when buttons-line scrolls out of view
	$(window).on('resize.buttonsLine scroll.buttonsLine',function()
    {
		// Sometimes these buttons are not shown (ex: re-edit already open published event)
		if($('#cancel').length===0){return;}
		var bottom=-20/* ??? */+win.height()+win.scrollTop()-
			(information.offset().top+information.height()+buttons.height());
		var isFloat=bottom<0;
		var wasFloat=buttons[0].className==="float";
		if(firstTime || wasFloat!==isFloat)
		{
			firstTime=false;
			buttons.toggleClass('float',isFloat);
			if(isFloat)
			{
				buttons.css('right',(win.width()-($('#wrap').offset().left+$('#wrap').outerWidth()))+'px');
				// resize buttons-line so that it doesnt take too much place
				buttons.width(20+$('#cancel').offset().left+$('#cancel').outerWidth()-$('#ok').offset().left);
			}
			else
			{
				buttons.css('width','auto');
			}
		}
	});
	$(window).resize();

	// ok button
	$('#ok').on('click',function(e)
	{
		e.stopPropagation();e.preventDefault();
		wait_for_condition(
			function()
			{
				return !$('#htmlView').hasClass('isClosing') && 
					   $('.editing').length===0 && 
					   $('.endEditing').length===0 && 
					   merge_async_calls(undefined,'all-is-idle');
			})
			.done(function()
			{
				var ok=true;
				if(demosphere_event_publish_form_is_direct_publish &&
				   $('#topics .topic').first().attr('data-selected')===undefined)
				                                            {ok=false;set_error($('#topics'));}
				if($('#place-search').length>0             ){ok=false;set_error($('#place-search'));}
				if($('.date'		).attr('data-val')===''){ok=false;set_error($('.date'));}
				if($('.time'		).attr('data-val')===''){ok=false;set_error($('.time'));}
				if($('#htmlView').attr('data-val')===''){ok=false;set_error($('#htmlView'));}
				if($('#frontpage .c2 div').attr('data-val')===''){ok=false;set_error($('#frontpage .c2 div'));}
				if(demosphere_config.show_title_in_event==='always' &&
				   $('#eventTitle').attr('data-val')===''){ok=false;set_error($('#eventTitle'));}

				if(ok)
				{
					window.onbeforeunload=null;
					$('#publish-form').append('<input type="hidden" name="ok" value="1"/>');
					$('#publish-form').submit();
				}
				else
				{
					// scroll to show first error element
					var errorElTop=$('.error').first().offset().top-30;
					var offset = errorElTop - $(window).scrollTop();
					if(offset <0 || offset>window.innerHeight){$('html,body').scrollTop(errorElTop);}
				}
			});
	});

	// cancel button
	$('#cancel').on('click',function(e)
	{
		e.stopPropagation();e.preventDefault();
		wait_for_condition(function(){return $('.editing').length===0;}).
			done(function()
				 {
					 window.onbeforeunload=null;
					 $('#publish-form').append('<input type="hidden" name="cancel" value="1"/>');
					 $('#publish-form').submit();
				 });
	});

	// note: firefox v4 and later do not show a message 
	// (they just show a built in message which is ok).
	var initialChangeNumber=$("[data-change-number]").attr('data-change-number');
	window.onbeforeunload = function(e0) 
	{
		if($("[data-change-number]").attr('data-change-number')==initialChangeNumber){return undefined;}
		var message=t('leavePageMessage');
		var e = e0 || window.event;
		// For IE and Firefox prior to version 4
		if(e){e.returnValue=message;}
		// For Safari
		return message;
	};

	if(window.demosphere_event_publish_form_hide_buttons)
	{
		window.onbeforeunload=function()
		{
			// Just in case something went wrong: first close all other open meditables
			$('.meditable.editing').each(function(){$(this).data('meditable').endEditing();});
		};
		$('#ok,#cancel').remove();
		$(window).unbind('.buttonsLine');
	}

	// *********** 

	setup_hover();
	refresh_floatright_positions();

	// FIXME: do we really need tabbed focus browsing ?
	//var idx=1;
	//$('.meditable').each(function(){$(this).attr('tabindex',idx++);});
	
});

// This is called from demosphere-htmledit.js : from ed.on('init')
function tinymce_init(ed)
{
	debug_log('tinymce_init');
	ed.focus(true);

	ed.demosphereHtmleditorAreaRight=$('<div class="htmleditor-area-right"></div>');
	$('#htmlView').append(ed.demosphereHtmleditorAreaRight);

	$('#htmlView.meditable').data('meditable').editor=ed;

	$('.htmleditor-area-right').css('top',($('.place').height()-30)+'px');

	// We need to wait for htmleditor-area-right rendering to get its position & height
	window.setTimeout(function()
	{
		// FIXME: what is 458 ? A minimum reasonable height ?
		var h1=Math.max(458,$('.htmleditor-area-right').offset().top+$('.htmleditor-area-right').outerHeight()-$('#htmlView').offset().top);
		$('#htmlView>.tox').animate({height:h1},700);
	},0);


	// Add example title and text when body is empty 
	var ebody=$(ed.getBody());
	if(demosphere_config.show_title_in_event!=='always')
	{
		window.setTimeout(function()
		{
			var added=false;
			// add place holder title, auto-remove it on click
			if(!$.trim(ebody.html()).match(/^\s*(<h[23]|<p>\s*<strong>Attention)/))
			{
				var titlePH=$('<h2 id="editorTitlePlaceholder"/>').text(t('phPageTitle'));
				titlePH.one('mousedown',function(e)
							{
								if(titlePH.text()===t('phPageTitle'))
								{
									titlePH.html('&nbsp;');
									// firefox loses caret 
									if($.browser.mozilla)
									{
										ed.selection.select(titlePH[0].childNodes[0], true);
										ed.selection.collapse(false);
										tinyMCE.execCommand("mceStartTyping");
										e.stopPropagation();
										e.preventDefault();
									}
								}});
				ebody.prepend(titlePH);
				added=true;
			}
			// add place holder paragraph, auto-remove it on click
			if($.trim(ebody.find('p').not('.demosphere-sources').text())==='')
			{
				var textPH=$('<p id="editorDescPlaceholder"/>' ).text(t('phDescription'));
				textPH.one('mousedown',function(e)
						   {if(textPH.text()===t('phDescription'))
							{
								textPH.html('&nbsp;');
								// firefox loses caret 
								if($.browser.mozilla)
								{
									ed.selection.select(textPH[0].childNodes[0], true);
									ed.selection.collapse(false);
									tinyMCE.execCommand("mceStartTyping");
									e.stopPropagation();
									e.preventDefault();
								}
							}});
				if(ebody.find('p.demosphere-sources').length==0){ebody.append(textPH);}
				else
				{ebody.find('p.demosphere-sources').before(textPH);}
				
				added=true;
			}
			// remove empty paragraph
 			if(added)
			{
				var first=ebody.children('p').first();
				if(first.children().first().attr("data-mce-bogus")!==undefined){first.children().first().remove();}
				if(first.html()===''){first.remove();}
			}
		},1);
	}
}

      
// Add / flash error class for bad elements when user hits the "ok" button.
function set_error(el)
{
	if(el.hasClass('error'))
	{
		window.setTimeout(function()
		{
			el.removeClass('error');
			window.setTimeout(function()
			{
				el.addClass('error');
				window.setTimeout(function()
				{
					el.removeClass('error');
					window.setTimeout(function()
					{
						el.addClass('error');
					},300);		
				},300);		
			},300);		
		},1);		
	}
	else{el.addClass('error');}
	el.one('mousedown',function(){el.removeClass('error');});
}

// *** recompute size and position floatRight objects and textParts
// Floatright objects are position:absolute; Their "top" position
// depends (among others) on the height of the place block.
function refresh_floatright_positions()
{
	// Make sure #htmlView is big enough to contain a large place.
	$('#htmlView').css('min-height',Math.max(210,$('.place-inner').height()-$('#htmlView').position().top)+'px');
	
	if($('#rightObject0').length==0){$('.textPart').css('min-height',1);return;}

	// Compute "top" (and min-height) inside each textPart (text is divided into parts by <hr>)
	$('.textPart').each(function(n)
	{
		debug_log(n,this);
		var top=0;
		// Position of floatright objects in first text part 
		// is pushed down by the place block.
		if(n===0)
		{
			top=20+$('.place-inner').height()-$('#htmlView').position().top;
		}
		// Compute position of floatright objects in this textpart
		$(this).find('.floatRight').each(function(nf)
		{
			$(this).css('top',top+'px');
			top+=$(this).height()+20;
		});
		// textpart must be big enough to contain floatRight objects
		$(this).css('min-height',top);
	});
}

// User should see which meditable elements he can edit.
// When user hovers on any meditable, other meditables are also emphasized (walking ants border).
// This is done by adding/removing the meditable-hover class to event
// This has to be reset every time a meditable new element is created.
function setup_hover()
{
	$('.meditable, .editwrap').unbind('mouseenter.meditable-hover');
	$('.meditable, .editwrap').unbind('mouseleave.meditable-hover');
	$('.meditable, .editwrap').on('mouseenter.meditable-hover',function(e){$('body').addClass(	 'meditable-hover');});
	$('.meditable, .editwrap').on('mouseleave.meditable-hover',function(e){$('body').removeClass('meditable-hover');});
}

// Setup handlers (ajax) for input and textarea in (information fields)
function setup_information_fields()
{
	$('#information input,#information textarea,#information select').each(function()
    {
		var input=$(this);
		var last={val:false};

		input.keypress(function(e)
		{
			if(e.keyCode == '13' && !input.is("textarea")){e.stopPropagation();e.preventDefault();}
		});

		// save as you type (change and blur are needed when brower autocomplete is clicked)
		input.on('keyup change blur',function(e)
        {
			var val=input.val();
			var label=input.attr('name');
			if(input.hasClass('hasPlaceholder')){val='';}
			if(last.val===val){return;}
			last.val=val;
			var updateData={name: input.attr('name')};
			updateData[input.attr('name')]=val;
			merge_async_calls(label,'call',200,true,function()
			{
				MEditable.ajax_update(updateData,input)
					.always(function(response)
					{
						merge_async_calls(label,'end');
					});
			});
		});
	});

	$('.publish-help').hover(
		function()
		{
			var offset=$('.publish-help').offset();
			offset.top+=$('.publish-help').height();
			// fit popup inside window (this is a problem only for mobile)
			offset.left=Math.max(0,offset.left-300);
			if($('.publish-help-popup').width()>$(window).width()-12){$('.publish-help-popup').width($(window).width()-12);}
			offset.left=Math.min(offset.left,$(window).width()-$('.publish-help-popup').width()-12);
			$('.publish-help-popup').show();
			$('.publish-help-popup').offset(offset);
		},
		function(){$('.publish-help-popup').hide();});
}

// *************** topics
// Topics use "select" meditables. The meditable ajax is not used.
// The whole topics span is recreated from html sent by server each time a topic is selected.
function setup_topics_editable()
{
	var topicsList=$('.topicsList');
	
	$('#topics.no-topics').show();
	// replace "a" links by spans
	topicsList.find('a').replaceWith(function(){return $('<span class="topic"/>').append($(this).contents()).
											   attr('data-selected','t'+$(this).attr('data-topic'));});
	if(topicsList.find('.topic').length!=0){topicsList.html($.trim(topicsList.html()));}
	if(topicsList.find('.meditable-placeholder').length==0)
	{
		if(topicsList.find('.topic').length!=0){topicsList.append(',');}
		topicsList.append(' <span class="topic"/>');
	}

	var selectedTopics=$.map(topicsList.find('.topic'),function(x){return $(x).attr('data-selected');});

	if($('#frontpage tr,ul.day-events>li').length)
	{
		$('#frontpage tr,ul.day-events>li')[0].className='';
		$('#frontpage tr,ul.day-events>li').addClass(selectedTopics.join(' '));
	}

	topicsList.find('.topic').meditable(
	{
		name: 'topic',
		placeHolder: t('phTopic'),
		inputType : 'selectmenu',
		selectOptions: $.extend(demosphereTopics,{tchoose:t('topicDelete')}),
		postUrl: false,// don't use standard meditable ajax
		inputSetup: function(meditable,input)
		{
			// Hide topics that are already selected. They need to be present to avoid problems.
			// Also add class for topic colors & icons.
			input.find('option').each(function()
			{
				var option=$(this);
				var menuItem=false;
				input.selectmenu('instance').menu.find('.ui-menu-item-wrapper').each(function()
				{
					if($(this).text()===option.text()){menuItem=$(this).parent();}
				});
				if(menuItem===false){console.error('menu item not found');return;}
				menuItem.addClass(option.attr('value'));
				if(selectedTopics.indexOf(option.attr('value'))!==-1)
				{
					menuItem.addClass('topic-already-selected');
				}
			});
		},
		// Instead, use custom ajax call when meditable is closed
		endEditingOverHook: function(meditable)
		{
			MEditable.ajax_update(
				{
					name:'topics',
					// send full list of topics
					topics: $.map(topicsList.find('.topic'),function(x){return $(x).attr('data-selected');})
				},
				meditable.editEl)
				.done(function(response)
				{
					// Very special case: user has started editing another meditable 
					topicsList.find('.meditable.editing').each(function(){$(this).data('meditable').endEditing();});
					// completely recreate span with all topics
					topicsList.html(response.html);
					setup_topics_editable();
				});
			return true;
		}
	});
	// Add meditable hovering (needed for newly recreated topics span)
	setup_hover();
}

// Place can be in one of the following states
// - Search : setup_place_search    
// - Edit	: setup_place_edit
//	 a) Existing place (unmodified) : on any change switch to b) 
//		Variant used only here		: city is not modifiable
//	 b) Empty place	 (unmodified)	: on any change switch to d)
//		New place					: city is modifiable
function setup_place_edit()
{
	//console.log('setup_place_edit');
	$('.place-links,.address-text,.place .city,.place .mapimage').show();
	$('.place .city').removeAttr("href");
	$('.place .address-text a').removeAttr("href");
	$('.place .address-text').meditable(
    {
		name: "address",
		placeHolder: t('phAddress'),
		inputType : 'textarea',
		inputSetup: function(e,meditable){refresh_floatright_positions();},
		endEditingOverHook: function(meditable){refresh_floatright_positions();}
	});

	// City is editable
	if($('.place').hasClass('new-place'))
	{
		$('.place .city').meditable(
			{
				name: 'city',
				placeHolder: t('phCity'),
				inputSetup: function(meditable)
				{
					var input=meditable.input;

					// use jquery ui autocomplete, instead of drupal autocomplete
					input.autocomplete(
					{
						delay: 100,
						position: { my: "left top", at: "left bottom", collision: "fit none" },
						select: function(event, ui)
						{
							input.val(ui.item.value);
							meditable.update();
							meditable.endEditing();
						},
						source: function(request,response)
						{
							$.get(base_url+'/city/autocomplete',
								  {term:request.term},
								  function(data)
								  {
									  var reformat=[];
									  $.each(data,function(k,v){reformat.push(v);});
									  response(reformat);
								  },'json');
						}
					});
				},
				endEditingHook: function(e,meditable)
				{
					return !e || $(e.target).parents('ul.ui-autocomplete').length==0;
				},
				endEditingOverHook: function(meditable)
				{
					$('#frontpage tr .c3,.mobile #frontpage .c3').text($('.city').text());
					if($('.city').width()>120){$('#place-erase').css('right',($('.city').width()+20)+'px');}
				}
			});
	}

	setup_hover();

	$('#place-erase').remove();
	$('.place .placeLabel').after($('<button type="button" id="place-erase"/>').text(t('placeErase')));
	if($('.city').width()>120){$('#place-erase').css('right',($('.city').width()+20)+'px');}
	$('#place-erase').mousedown(function(e)
	{
		if(e.which!=1){return;} 
		e.stopPropagation();e.preventDefault();
		place_erase();
		setup_place_search();
		refresh_floatright_positions();
	});
}

function place_erase(doAjaxUpdate)
{
	if($('.meditable.editing').length){$('.meditable.editing').data('meditable').endEditing();}

	$('#htmlView').css('min-height','');
	$('#place-search').remove();
	// Recreate DOM (deletes old elements)
	$('.place').replaceWith($('.place').clone().wrap('<div>').parent().html());
	$('.place .address-text').text('');
	$('.place .city').text('');
	$('.place .mapimage').html('');
	$('.place .place-links').html('');
	$('.place').addClass('new-place');
	$('#place-erase').remove();
	$('#frontpage tr .c3,.mobile #frontpage .c3').text($('.city').text());
	refresh_floatright_positions();

	if(doAjaxUpdate!==undefined && doAjaxUpdate===false){return true;}
	// ** send ajax notification of erased place
	MEditable.ajax_update(
		{
		    name:'place_erase',
		    place_erase: true,
		},$('.place'));
	return true;
}

function setup_place_search()
{
	$('.place-links,.address-text,.place .city,.place .mapimage').hide();
	$('#place-search').remove();
	var search=$('<label id="place-search"/>');
	var input=$('<input id="place-search-input" class="has-placeholder editwrap" type="text" autocomplete="off"/>');
	input.attr('placeholder',t('phPlaceSearch'));
	search.append(input);
	search.append($('<img id="search-icon" title="search">').attr('src',base_url+'/demosphere/css/images/search.png'));
	search.append($('<div id="place-search-error"></div>').text(t('placeError')));
	$('.place-inner').append(search);
	placeholder_for_old_browsers();

	// Create the search / suggestions popup using jquery-ui autocomplete.
	var isSelecting={val:false};
	input.autocomplete(
	{
		delay: 100,
		position: { my: "left top", at: "left bottom", collision: "fit none" },
		source: base_url+'/place-search-autocomplete',
		create: function(event, ui) {
			debug_log('create:');
			$('.ui-autocomplete').addClass('place-search-menu');},
		// don't change value in input field when navigating throug sggestions with arrow keys
		focus: function(event, ui) {debug_log('focus:');event.preventDefault();},
		//open:   function(event, ui) {debug_log('opent:' );},
		//close:  function(event, ui) {debug_log('close:' );},
		//change: function(event, ui) {debug_log('change:');},

		search: function(event,ui)
		{
			// IE refires a search after selecting
			if(isSelecting.val===true){return false;}
			return undefined;
		},
		// User actually selects an existing place.
		// Retrieve the full display for that place using an ajax query
		select: function(event, ui) 
		{
			debug_log('select:',ui.item);
			isSelecting.val=true;
			$('#place-search-input,#search-icon').hide();
			$('#search-icon').after('<div class="meditable-waiting"></div>');
			if(ui.item.value==0)
			{
				place_erase();
				setup_place_edit();
				refresh_floatright_positions();
				return;
			}
			MEditable.ajax_update({name: 'placeId',placeId: ui.item.value},$('.place'))
				.done(function(response)
				{
					$('body').append($('<div id="tmpplace" style="display:none"></div>').html(response.html));
					$('#eventMain .place').replaceWith($('#tmpplace').find('.place'));
					check_for_event_duplicate();
					$('#tmpplace').remove();
					// disable links (clicking links will mess up form)
					$('.place a').attr('href','javascript:void(0)');
					$('#htmlView').css('min-height',$('.place-inner').height()-20);
					$('.place').addClass('place-variant');
					$('#frontpage tr .c3,.mobile #frontpage .c3').text($('.city').text());
					setup_place_edit();
					setup_hover();
					refresh_floatright_positions();
				});
		}
	});

	// Detail: don't submit form on return key.
	input.keypress(function(e)
	{
		if(e.keyCode == '13'){e.stopPropagation();e.preventDefault();}
	});

	// Detail: if user refocuses on search, open search popup
	input.focus(function(e)
	{
		if(demosphere_is_mobile)
		{
			$(window).scrollTop(input.offset().top);
		}
		search.removeClass('error');
		if($.trim($(this).val())!=='' && !$('.ui-autocomplete').is(':visible'))
		{
			$(this).autocomplete('search');
		}
	});

	// Problem: user might not understand this is a search box.
	// Show validation error if user leaves search box without selecting place.
	input.blur(function(e)
	{
		if($.trim($(this).val())===''){return;}
		window.setTimeout(function()
		{
			if(isSelecting.val===true){return;}
			search.addClass('error');
		},200);
	});

}

//! Check using ajax for other events with same place and date/time.
function check_for_event_duplicate()
{
	//console.log('check_for_event_duplicate');

	// Get PlaceId
	var placeId=$('.place').attr('data-place-id');
	if(placeId==1){return;}

	// Date time
	var date=$('#dateContents .date').attr('data-val');
	var time=$('#dateContents .time').attr('data-val');
	if(date===''){return;}

	$.get(base_url+'/event-publish-form-check-duplicate',
		  {placeId: placeId,
		   date: date,
		   time: time,
		   tempId:$("input[name='tempId']").val()
		  },
		  function(response)
		  {
			  if(!response.hasDuplicate){return;}
			  $('#duplicate-message').remove();
			  var dialogCfg=
				  {
					  width: 640,
					  modal: true,
					  title: response.title
				  };
			  var msgDiv=$('<div id="duplicate-message">');
			  msgDiv.html(response.message);
			  msgDiv.dialog(dialogCfg);
		  });
}

function show_donate_message()
{
	var dialogCfg=
		{
			width: 640,
			modal: true,
			title: $("#donate-message" ).attr('data-title')
		};
	dialogCfg['buttons']={};
	dialogCfg['buttons'][t("doNotDonate")]= function() 
	{
		$( this ).dialog( "close" );
	};
	
	dialogCfg['buttons'][t("donateNow")]= function() 
	{
		var lang=demosphere_config.locale.replace(/_.*$/g,'');
		var donate_url='https://demosphere.net/en/content/donate';
		if(lang==='fr'){donate_url='https://demosphere.net/fr/content/soutien-financier';}
		if(lang==='es'){donate_url='https://demosphere.net/es/content/dona-aqu%C3%AD';}
		window.location=donate_url;
		$( this ).dialog( "close" );
	};
	$( "#donate-message" ).dialog(dialogCfg);

	var date = new Date(Date.now()+(1000*365*24*3600));// next year
	var cookie='publish_donate=1; expires='+date.toUTCString()+';';
	document.cookie =cookie;
}

// This adds placeholder support to browsers that wouldn't otherwise support it. 
// Mostly copied from http://www.cssnewbie.com/example/placeholder-support/
function placeholder_for_old_browsers()
{
	if(!placeholder_for_old_browsers.hasOwnProperty('isSupported'))
	{
		var test = document.createElement('input');
		placeholder_for_old_browsers.isSupported= 'placeholder' in test;
	}

	if(placeholder_for_old_browsers.isSupported){return;}

	// save currently focused element 
	var active = document.activeElement;
	// list 
	var inputs=$('input[type="text"],input[type="url"],input[type="email"],textarea');

	// When user focuses into input, erase placeholder
	inputs.focus(function () 
	{
		if($(this).attr('placeholder')!='' && 
		   $(this).val()==$(this).attr('placeholder'))
		{
			$(this).val('').removeClass('hasPlaceholder');
		}
	});
	// When user blurs (un-focuses) out of input, add placeholder (if needed)
	inputs.blur(function() 
	{
		if($(this).attr('placeholder')!='' && 
		   ($(this).val()=='' || $(this).val()==$(this).attr('placeholder')))
		{
			$(this).val($(this).attr('placeholder')).addClass('hasPlaceholder');
		}
	});
	// Look for empty inputs that currently need a placeholder
	inputs.blur();
	// restore currently focused element 
	$(active).focus();
	// submit handler: remove placeholders
	$('form:eq(0)').submit(function () 
	{
		inputs.filter('.hasPlaceholder').val('');
	});
}

function debug_log()
{
 	//console.log.apply(null,arguments);
}

// Exports:
window.demosphere_event_publish_form.tinymce_init=tinymce_init;

// end namespace wrapper
}());
