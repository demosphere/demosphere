
// ************ adds an onclick handler to demosphere_timestamp class
// when they are clicked, a popup shows events near that date
// using an async (ajax) request
function add_popup_event_to_highlighted_dates()
{
	if(typeof document.getElementsByClassName=='undefined'){alert("unsupported browser : add_popup_event_to_highlighted_dates");return;}
	var els=document.getElementsByClassName("demosphere_timestamp");
	for(var i=0,li=els.length;i<li;i++)
	{
		els[i].addEventListener("mousedown",popup_events_near_date, true);
		els[i].addEventListener("click",
			function(e){e.preventDefault();e.stopPropagation();}, true);
	}
}

function popup_events_near_date(event,text,eid)
{
	event.preventDefault();
	event.stopPropagation();
	var el=event.currentTarget;// the highlighted element
	// close and delete popup if we re-click on the higlighted date
	if(el.popupDiv!==undefined)
	{
		el.popupDiv.style.display="none" ;
		el.popupDiv.parentNode.removeChild(el.popupDiv);
		el.popupDiv=undefined;
		return ;
	}
	// find the date / time for this highlight (either content or title)
	var date=el.innerHTML;
	if(text!==undefined) {date=text;}
	else
	if(el.hasAttribute('data-demosphere-timestamp'))
	{date=el.getAttribute('data-demosphere-timestamp');}
	else
	if(el.title.length>0){date=el.title;}

	// create the popup
	var popup=document.createElement("div");
	el.popupDiv=popup;
	popup.className="eventsNearDatePopup";
	popup.style.position="absolute";
	popup.style.left=Math.max(-(3+9),Math.min(window.innerWidth-820-(3+9)*2-10,
											  event.clientX))+"px";
	popup.style.top =(event.pageY+8)+"px";
	popup.innerHTML='<h1 class="eventsNearDatePopupTitlebar">X</h1>...';
	document.body.appendChild(popup);
	popup.popupSource=el;
	// click to close popup even if it hasn't finished loading yet
	var title=popup.getElementsByClassName("eventsNearDatePopupTitlebar")[0];
	title.addEventListener("mousedown",function(pevent)
		{
			if(pevent.button!=0){return true;}
			el.popupDiv.style.display="none" ;
			el.popupDiv.parentNode.removeChild(el.popupDiv);
			el.popupDiv=undefined;
			pevent.stopPropagation();
			pevent.preventDefault();
			return false;
		}, true);

	// ajax request
	var request = new XMLHttpRequest();
	if (request.readyState != 4 && 
		request.readyState != 0    ) {dump(" failed?\n");return;}

	if(eid===undefined){eid=-1;}
	var burl=false;
	if(typeof demosphere_config!=='undefined')
	{
		if(typeof demosphere_config.std_base_url!=='undefined'){burl=demosphere_config.std_base_url;}
		else
		if(typeof demosphere_config.base_url    !=='undefined'){burl=demosphere_config.base_url;}
	}
	if(burl===false){burl=base_url;}
	// fix http/https
	var isHttps=/^https:/.test(window.location.href);
	burl=burl.replace(new RegExp('^http'+(isHttps ? '' : 's')+':'),
					              'http'+(isHttps ? 's' : '')+':');
	var url=burl+"/ajax-events-near-date";

	var getArgSep='?';
	if(url.match(/[?]/)){getArgSep='&';}

	url+=getArgSep+"date="+encodeURIComponent(date);
	url+="&id="+eid;
	url+="&source=mail";

	// we want loggged in version when displayed from safe domain (mail import, feed import items)
	if(window.location.href.match(/sdtoken=[0-9a-f-]+/)!==null)
	{
		// CORS, from safe domain
		request.withCredentials = true;
		url+='&srcUrl='+encodeURIComponent(window.location.href);
	}

	//	alert("url:"+url);
	request.open("GET", url, true);
	request.onreadystatechange = receive_response;
	request.send(null);
	//dump("sendRequest over\n");
	function receive_response()
	{
		//dump("response1\n");
		//dump("state1:"+request.readyState+"\n");
		//console.log("state1:"+request.readyState+"\n");
		if (request.readyState != 4) {return;}
		//alert('response:'+request.responseText);
		
		if(request.responseText!="")
		{
			popup.innerHTML=request.responseText;
			// add a listener to close & destroy popup
			var title=popup.getElementsByClassName("eventsNearDatePopupTitlebar")[0];
			title.addEventListener("mousedown",function(pevent)
								   {
									   if(pevent.button!=0){return true;}
									   el.popupDiv.style.display="none" ;
									   el.popupDiv.parentNode.removeChild(el.popupDiv);
									   el.popupDiv=undefined;
									   pevent.stopPropagation();
									   pevent.preventDefault();
									   return false;
								   }, true);
		}
		else
		{
			alert('events near date: bad response:"'+request.responseText+'"'+' date:'+date+' url:'+url);
		}
	}
}
