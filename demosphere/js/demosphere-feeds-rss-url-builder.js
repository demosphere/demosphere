// this js file is used the rss feed builder only
$(document).ready(function()
{
	window.mapcircle=false;

	//$("#edit-time-format"           ).change(rss_change);
	//$("#edit-use-event-date-not-pub").change(rss_change);
	//$("#edit-use-event-date-not-pub").click (rss_change);
	//$("#edit-title-city"            ).change(rss_change);
	//$("#edit-title-city"            ).click (rss_change);
	//$("#edit-limit"	                ).keyup (rss_change);
	//$(".select-vocab"               ).change(rss_change);

	demosphere_event_list_form_near_lat_lng({update_callback:rss_change});
	demosphere_event_list_form_map_shape   ();
	demosphere_event_list_form_more_options();

	add_change_handler_to_all_form_items('#rss-url-builder-form',rss_change);

	rss_change();
});

function lat_lng_dist_on_change()
{
	if($("#lat_lng_dist_on").prop("checked")){$("#lat_lng_dist_div").show();}
	else                                     {$("#lat_lng_dist_div").hide();}

	if(!window.mapcircle)
	{
		window.mapcircle=new Mapcircle({center_changed:rss_change});
	}

	rss_change();
}

function rss_change()
{
	var def=rss_builder_defaults;
	var el;
	var urlParts={};

	var options=demosphere_event_list_form_option_values();

	// reparse "limit" as it has its own default value
	delete options.limit;
	el=$('#edit-limit');
	if(el.length && el.val()!=def['limit']){options.limit=el.val();}

	el=$('#edit-time-format');
	if(el.length && el.val()!=def['time-format']){options["time_format"]=el.val();}

	el=$('#edit-use-event-date-not-pub');
	if(el.length && (el.prop('checked') ? 1:0)!=def['use-event-date-not-pub'])
	{options["use_event_date_not_pub"]=el.prop('checked') ? 1:0;}

	el=$('#edit-title-city');
	if(el.length && (el.prop('checked') ? 1 :0)!=def['title-city'])
	{options["title_city"]=el.prop('checked') ? 1:0;}

	// ****** build url from  options.
	var url=base_url+"/events."+
		(feed_builder_type==='rss' ? "xml" : "ics")+"?";
	// FIXME: use encodeURIComponent, but commas are encoded.
	// Maybe we should not use commas (google does)...
	$.each(options,function(name,val){url+=name+"="+val+"&";});
	url=url.replace(/[&?]$/,'');

	// show rss-url
	$("#rss-url").attr("href",url);
	$("#rss-url").html(url);

	var title_city            =avalue(options['title_city'        ],
									  def    ["title-city"]);
	var use_event_date_not_pub=avalue(options['use_event_date_not_pub'],
									  def    ["use-event-date-not-pub"]);

	// example title
	$("#example-title-time").html(options["time_format"]==0 ? '' : 
								  $('#edit-time-format option:selected').html()+' - ');
	$("#example-title-city"      ).css('display',title_city              ? 'inline' : 'none');
	$("#example-pubdate"         ).css('display',!use_event_date_not_pub ? 'inline' : 'none');
	$("#example-pubdate-startime").css('display', use_event_date_not_pub ? 'inline' : 'none');	
}

function avalue(a,def)
{
	if(typeof a==="undefined"){return typeof def==="undefined" ? false : def;}
	return a;
}
