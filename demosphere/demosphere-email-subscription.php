<?php
/**
 * Form builder for user event email form
 *
 * Provides options so that a user can subscribe to emails containing events.
 * User can choose when he receives email and how many events per email...
 * User can choose topics, cities, position on a map...
 *
 * @see demosphere_email_subscription_validate()
 * @see demosphere_email_subscription_submit()
 * @ingroup forms
 */
function demosphere_email_subscription_form()
{
	global $demosphere_config,$user,$base_url,$currentPage;

	$currentPage->title=t('Email subscription options');

	// No user logged in : redirect to login form
	if($user->id==0)
	{
		dlib_redirect($base_url.'/login?destination=email-subscription-form');
	}

	$subsUser=false;

	// uid: admin changes config of other user of this site
	if(isset($_GET['uid']))
	{
		if(!$user->checkRoles('admin')){dlib_permission_denied_403();}
		$subsUser=User::fetch($_GET['uid']);
	}

	// Most common case: a user edits his own subscription
	if($subsUser===false){$subsUser=$user;}

	require_once 'demosphere-event.php';
	require_once 'demosphere-event-list-form.php';
	$currentPage->addJsConfig('demosphere',['map_center_latitude','map_center_longitude','map_zoom','mapbox_access_token']);
	$currentPage->addJs('lib/jquery.js');
	$currentPage->addJs("demosphere/js/demosphere-event-list-form.js");
	$currentPage->addJs("demosphere/js/demosphere-email-subscription.js");
	$currentPage->addCssTpl('demosphere/css/demosphere-event-list-form.tpl.css');
	$currentPage->addCssTpl("demosphere/css/demosphere-email-subscription.tpl.css");
	$lang=str_replace('locale_language_','',t('locale_language_en'));
	$currentPage->addJs("demosphere/js/demosphere-email-subscription.js");
	$currentPage->addCss('lib/leaflet/leaflet.css');
	$currentPage->addJs('lib/leaflet/leaflet.js');
	
	$values=val($subsUser->data,'demosphere_email_subscription',[]);

	// add default values
	$values=array_merge(demosphere_email_subscription_default_values(),$values);
	
	$formOpts=[];

	$form['title'] = ['html'=>'<h1 class="page-title">'.t('Receive emails with events').'</h1>'];

	if($demosphere_config['email_subscription_disable_message']!==false)
	{
		dlib_message_add($demosphere_config['email_subscription_disable_message'],'warning');
	}

	$form['active'] = 
		['type' => 'checkbox',
		 'title' => t('Yes, I want to receive events by email'),
		 'description' => t("Don't forget to hit the \"Save\" button after any changes."),
		 'default-value'=>$values['active'],
		];

	$form['subsUser'] = 
		['type' => 'data',
		 'data'=>$subsUser,
		];

	$form['submit-0'] = 
		[
			'type' => 'submit',
			'value' => t('Save'),
		];
	$form['submit-and-test-0'] = 
		[
			'type' => 'submit',
			'value' => t('Save and send test email'),
		];

	$form['options1'] = 
		['type' => 'fieldset',
		 'title' => t('Choose when emails are sent to you and how many events you want'),
		 'collapsible' => true,
		 'collapsed' => true,
		 'prefix'=>'<div id="options" class="">',
		];

	$howOften=[1 => t('Every day')];
	foreach([2,3,4,5,6,7,10,15] as $n){$howOften[$n]=t('Every !n days',['!n'=>$n]);}

	$form['how-often-days'] = 
		['type' => 'select',
		 'title' => t('How often '),
		 'options' => $howOften,
		 'description' => t('How often do you wnat to receive emails.'),
		 'default-value'=>$values['how-often-days'],
		];
	
	$times=[];
	foreach($values['email-send-times'] as $t)
	{$times[]=sprintf('%02d:%02d',$t[0],$t[1]);}
	$times=implode(',',$times);
	$form['email-send-times'] = 
		['type' => 'textfield',
		 'title' => t('Time when emails are sent'),
		 'required' => false,
		 'description' => t('At what time should the email be sent? Use 24 hour time format like this "hh:mm". If you want several emails to be sent in one day, you can use several times separated by a comma. For example: "22:30" or "8:00" or "8:00, 20:00"'),
		 'default-value'=>$times,
		];

	$form['start-time'] = 
		['type' => 'select',
		 'title' => t('Events start'),
		 'options' => ['d'=>t('The begining of the day on which the email is sent'),
					   't'  =>t('The moment the email is sent'),
					   'd+1'=>t('The day after the email is sent'),
					  ],
		 'description' => t('When do events displayed in the email begin?'),
		 'default-value'=>$values['start-time'],
		];
	$howManyOpts=['n'=>t('All days until next email'),
				  '1d'=>t('1 day'),];
	foreach([2,3,4,5,7,10,30] as $d)
	{$howManyOpts[$d.'d']=t('!days days',['!days'=>$d]);}
	$form['how-many-events'] = 
		['type' => 'select',
		 'title' => t('How many events'),
		 'options' => $howManyOpts,
		 'description' => t('How many events should be displayed in the email?'),
		 'default-value'=>$values['how-many-events'],
		];

	demosphere_event_list_form_item('select-recent-changes',$values,$form);
	// value=1 : hack meaning "all changes since last email"
	$form['select-recent-changes']['options']=
		dlib_array_insert_assoc($form['select-recent-changes']['options'],0,1,t('new events since last email'),true);
	$form['select-recent-changes']['description'].=t('Note: if you select this, the "How many events" option will be ignored.');

	$form['urgent-events'] = 
		['type' => 'checkbox',
		 'title' => t('Urgent events'),
		 'description' => t('Receive an extra email, manually sent by a moderator, when an important event is published less than 48h beforehand.'),
		 'default-value'=>$values['urgent-events'],
		];

	$form['options1-end']=['type'=>'fieldset-end'];

	$form['options2'] = 
		['type' => 'fieldset',
		 'title' => t('Choose which events you want'),
		 'collapsible'=> true,
		 'collapsed'  => true,
		];

	demosphere_event_list_form_item('select-topic'             ,$values,$form);
	demosphere_event_list_form_item('select-city-id'           ,$values,$form);
	demosphere_event_list_form_item('near-lat-lng'             ,$values,$form);
	demosphere_event_list_form_item('select-fp-region'         ,$values,$form);
	demosphere_event_list_form_item('select-place-reference-id',$values,$form);
	demosphere_event_list_form_item('more-options-fs'          ,$values,$form);
	demosphere_event_list_form_item('map-shape'                ,$values,$form);
	demosphere_event_list_form_item('max-nb-events-per-day'    ,$values,$form);
	demosphere_event_list_form_item('most-visited'             ,$values,$form);
	demosphere_event_list_form_item('select-vocab-term'        ,$values,$form);
	demosphere_event_list_form_item('select-vocab-term-or'     ,$values,$form);
	$form['more-options-fs-end']=['type'=>'fieldset-end'];
	$form['options2-end'] = ['type' => 'fieldset-end'];

	$form['options3'] = ['type' => 'fieldset',
						 'title' => t('Choose email subject'),
						 'collapsible' => true,
						 'collapsed' => true,
						];

	$form['email-subject'] = 
		['type' => 'textfield',
		 'title' => t('Email subject'),
		 'required' => false,// why???
		 'description' => t('The subject of the email that will be sent. "%number" will be replaced by the number of events in the email.'),
		 'default-value'=>$values['email-subject'],
		];

	$form['options3-end']=['type'=>'fieldset-end'];
	
	$form['change-my-email-address'] = ['html' => '<p id="change-my-email-address">'.
										'<a target="_blank" href="'.$base_url.'/user/'.intval($subsUser->id).'/edit">'.t('Change my e-mail address').'</a></p>'];

	$form['submit'] = 
		[
			'type' => 'submit',
			'value' => t('Save'),
		];
	$form['submit-and-test'] = 
		[
			'type' => 'submit',
			'value' => t('Save and send test email'),
		];

	$form['reset-defaults'] = 
		[
			'type' => 'submit',
			'value' => t('Reset to default values'),
		];


	require_once 'demosphere-date-time.php';
	$form['last-sent-time-display'] = 
		['html'=>'<p id="last-time-sent">'.t('Last time an email was sent : !date (actualy: !date1)',
											 ['!date'=>
											  $values['last-sent-time'] ? 
											  demos_format_date('full-date-time',$values['last-sent-time']) : 
											  t('never'),
											  '!date1'=>
											  $values['last-sent-time-actual'] ? 
											  demos_format_date('full-date-time',$values['last-sent-time-actual']) : 
											  t('never')]).'</p>'];

	$formOpts['validate']='demosphere_email_subscription_form_validate';
	$formOpts['submit'  ]='demosphere_email_subscription_form_submit';
	require_once 'dlib/form.php';
	return form_process($form,$formOpts);
}
function demosphere_email_subscription_default_values()
{
	global $base_url,$demosphere_config;
	$res=['active'               =>false,
		  'how-often-days'       =>1,
		  'email-send-times'     =>[[2,0]],
		  'start-time'           =>'d',
		  'how-many-events'      =>'n',
		  'select-recent-changes'=>false,
		  'urgent-events'        =>true,
		  'near-lat-lng-use-map' =>false,
		  'near-lat-lng-distance'=>false,
		  'near-lat-lng-latitude-and-longitude'=>false,
		  'map-shape'            =>false,
		  'max-nb-events-per-day'=>false,
		  'most-visited'         =>false,
		  'select-vocab-term'    =>false,
		  'select-vocab-term-or' =>false,
		  'email-subject'        =>t('@sitename calendar',
									 ['@sitename'=>$demosphere_config['site_name']]).' - '.t('%number events'),
		  'last-sent-time'       =>false,
		  'last-sent-time-actual'=>false,
		  'select-topic'         =>false,
		  'select-city-id'       =>false,
		  'select-fp-region'     =>false,
		  'select-place-reference-id'=>false,
		  'bounce-unsubscribed'  =>false,
		 ];

	//var_dump($res);fatal('pp');
	return $res;
}

function demosphere_email_subscription_form_validate(&$form)
{
	global $demosphere_config;
	$values=dlib_array_column($form,'value');

	$subsUser=$form['subsUser']['data'];

	// validate email send time
	$times=explode(',',$values['email-send-times']);
	foreach($times as $time)
	{
		if(!preg_match('@^([0-9]?[0-9]):([0-9][0-9])$@',trim($time),$matches) ||
		   intval($matches[1])>23 || intval($matches[2])>59)
		{
			$form['email-send-times']['error']=t('Bad time format: use comma separated hh:mm');
		}
	}
	// several times needs every day
	$howOften=intval($values['how-often-days']);
	if(count($times)>1 && $howOften!==1)
	{
		$form['how-often-days']['error']=t('You have chosen several times, which means that you want emails "every day". Please select "every day".');
	}

	// validate standard options
	require_once 'demosphere-event-list.php';
	require_once 'demosphere-event-list-form.php';
	demosphere_event_list_form_item_validate('near-lat-lng-distance'              ,$values);
	demosphere_event_list_form_item_validate('near-lat-lng-latitude-and-longitude',$values);
	demosphere_event_list_form_item_validate('map-shape'                          ,$values);
	demosphere_event_list_form_item_validate('max-nb-events-per-day'              ,$values);
	demosphere_event_list_form_item_validate('most-visited'                       ,$values);
	demosphere_event_list_form_item_validate('select-vocab-term'                  ,$values);
	demosphere_event_list_form_item_validate('select-vocab-term-or'               ,$values);
	demosphere_event_list_form_item_validate('select-topic'                       ,$values);
	demosphere_event_list_form_item_validate('select-city-id'                     ,$values);
	demosphere_event_list_form_item_validate('select-fp-region'                   ,$values);
	demosphere_event_list_form_item_validate('select-place-reference-id'          ,$values);
	demosphere_event_list_form_item_validate('select-recent-changes'              ,$values);
}

function demosphere_email_subscription_form_submit($form,$formOpts,$activeButton)
{
	require_once 'demosphere-event.php';
	global $user,$base_url,$demosphere_config;
	$values=dlib_array_column($form,'value');

	$subsUser=$form['subsUser']['data'];

	if($activeButton=='reset-defaults')
	{
        unset($subsUser->data['demosphere_email_subscription']);
        $subsUser->save();
		dlib_redirect($base_url.'/email-subscription-form'.
					  ($subsUser->id!=$user->id ? '?uid='.intval($subsUser->id) : ''));
	}

	$options=val($subsUser->data,'demosphere_email_subscription',demosphere_email_subscription_default_values());
    
	//var_dump($values);
	$options['active']=$values['active']!=0;
	$options['how-often-days']=intval($values['how-often-days']);

	$options['email-send-times']=[];	
	$times=explode(',',$values['email-send-times']);
	foreach($times as $time)
	{
		preg_match('@^([0-9]?[0-9]):([0-9][0-9])$@',trim($time),$matches);
		$options['email-send-times'][]=[intval(preg_replace('@^0*@','',$matches[1])),
										intval(preg_replace('@^0*@','',$matches[2])),
									   ];
	}
	sort($options['email-send-times']);

	$options['start-time'               ]=$values['start-time'];
	$options['how-many-events'          ]=$values['how-many-events'];
	$options['select-topic'             ]=$values['select-topic'];
	$options['select-city-id'           ]=$values['select-city-id'];
	$options['select-fp-region'         ]=$values['select-fp-region'];
	$options['select-place-reference-id']=$values['select-place-reference-id'];
	$options['urgent-events'            ]=$values['urgent-events'];
	$options['select-recent-changes'    ]=$values['select-recent-changes'];
	$options['near-lat-lng-use-map'     ]=$values['near-lat-lng-use-map']!=0;
	$options['near-lat-lng-distance'    ]=$values['near-lat-lng-distance'];
	$options['near-lat-lng-latitude-and-longitude']=$values['near-lat-lng-latitude-and-longitude'];
	$options['map-shape'                ]=$values['map-shape'];
	$options['max-nb-events-per-day'    ]=$values['max-nb-events-per-day'];
	$options['most-visited'             ]=$values['most-visited'];
	$options['select-vocab-term'        ]=$values['select-vocab-term'];
	$options['select-vocab-term-or'     ]=$values['select-vocab-term-or'];
	$options['email-subject'            ]=$values['email-subject'];

    $subsUser->data['demosphere_email_subscription']=$options;
    $subsUser->save();

	// send a test email
	if($activeButton=='submit-and-test' || 
	   $activeButton=='submit-and-test-0' )
	{
		if($user->email==='')
		{
			dlib_message_add(t("We cannot send you a test email yet. You first have to validate your email address by clicking on the link in the email that was previously sent to you."),'error');
		}
		else
		{
			$from=$demosphere_config['email_subscription_from'];
			demosphere_email_subscription_send($from,$user->email,$subsUser->id,$options,time(),true);
			dlib_message_add(t("test email sent to %user",['%user'=>$user->email]));
		}
	}
}

function demosphere_email_subscription_cron($test=false)
{
    demosphere_email_subscription_send_all($test);
    demosphere_email_subscription_bounces();
}
function demosphere_email_subscription_send_all($test=false)
{
	global $demosphere_config;

	$stats=variable_get('email_subscription_sent_stats','',[]);
	$stats=array_filter($stats,function($t){return $t>time()-24*3600;});
	variable_set('email_subscription_sent_stats','',$stats);
	variable_set('email_subscription_sent_count','',count($stats));
	
	if($demosphere_config['email_subscription_disable_message']!==false ||
	   $demosphere_config['is_backup_site'])
	{
		return;
	}

	$uids=db_one_col('SELECT id FROM User');

	foreach($uids as $uid)
	{
		$now=time();

		if($test!==false){$values=$test['values'];$now=$test['now'];}
		else
		{
			$euser=User::fetch($uid);
			$values=$euser->data;
			if(!isset($values['demosphere_email_subscription'])){continue;}
			$values=$values['demosphere_email_subscription'];
		}
		
		if(!val($values,'active')){continue;}
		if($euser->email===''    ){continue;}

		// We found a user that wants email. Now check if it is time to send it.
		// For that, we figure out  next time mail should be sent based
		// on last time it was sent.
		//echo 'name:'.ent($euser->name).'<br/>';
		//echo 'last :'.date('r',$values['last-sent-time']).'<br/>';
		//echo 'lasta:'.date('r',$values['last-sent-time-actual']).'<br/>';
		//echo 'lasta:'.$values['how-often-days'].'<br/>';
		//var_dump($values['email-send-times']);
		$last=$values['last-sent-time'];
		$howOften=$values['how-often-days'];
		$times=$values['email-send-times'];
		$ntimes=count($times);
		// $last time should be the same as in config, unless
		// the config has changed. We should check that.
		$timeindex=array_search([date("H",$last),date("i",$last)],$times);
		if($timeindex===false)
		{
			$timeindex=0;
			$last=mktime($times[0][0], $times[0][1], 0,
						 date("n",$last),date("j",$last),date("Y",$last));
		}
		if($ntimes==1)
		{
			$next=strtotime('+'.intval($howOften).' days',$last);
		}
		else
		{
			$next=mktime($times[($timeindex+1)%$ntimes][0],
						 $times[($timeindex+1)%$ntimes][1],0,
						 date("n",$last),date("j",$last),date("Y",$last));
			if($timeindex>=$ntimes-1){$next=strtotime('+1 day',$next);}
		}
		//echo 'next :'.date('r',$next).'<br/>';
		if($test!==false){return $next;}
		if($now<($next-1800)){continue;}// too soon
		$willSendAt=mktime(date("H",$next),date("i",$next),0,
						   date("n",$now),date("j",$now),date("Y",$now));
		$to=$euser->email;
		$from=$demosphere_config['email_subscription_from'];

		//echo 'often:'.$howOften.'<br/>';
		//echo 'last:'.date('r',$values['last-sent-time']).'<br/>';
		//echo 'will:'.date('r',$willSendAt).'<br/>';
		$ok=demosphere_email_subscription_send($from,$to,$uid,$values,$willSendAt);
		if(!$ok){continue;}
		// set the last sent date and save it to db
		$values['last-sent-time-actual']=$now;
		$values['last-sent-time']=$willSendAt;

		$euser->data['demosphere_email_subscription']=$values;
		$euser->save();
	}
}

function demosphere_email_subscription_send($from,$to,$uid,$values,$willSendAt,$sendEvenIfEmtpy=false)
{
	global $demosphere_config,$base_url;
	$selectStartTime=false;
	switch($values['start-time'])
	{
	case 'd'  : $selectStartTime=strtotime('03:00',$willSendAt);break;
	case 't'  : $selectStartTime=$willSendAt;break;
	case 'd+1': $selectStartTime=strtotime('+1 day 03:00',$willSendAt);break;
	default : fatal('invalid start-time:'.$values['start-time']);
	}

	$days=$values['how-many-events']==='n' ?
		intval($values['how-often-days']) :
		intval($values['how-many-events']);
	$endTime=strtotime('+'.(1+$days).' days 03:00',$willSendAt);

	// hack: 'select-recent-changes'==1 means "all changes since last mail".
	if($values['select-recent-changes']==1)
	{
		$last=$values['last-sent-time-actual'];
		if(!$last){$last=time()-3600*24*7;}
		$values['select-recent-changes']=time()-$last;
	}
	// select recent changes implies no end time
	if($values['select-recent-changes']){$endTime=false;}

	require_once 'demosphere-event-list.php';
	require_once 'demosphere-event-list-form.php';
	$calOptions=[];
	demosphere_event_list_form_item_parse('nearLatLng'            ,$values,$calOptions,false);
	demosphere_event_list_form_item_parse('selectTopic'		      ,$values,$calOptions,false);
	demosphere_event_list_form_item_parse('selectCityId'	      ,$values,$calOptions,false);
	demosphere_event_list_form_item_parse('selectFpRegion'	      ,$values,$calOptions,false);
	demosphere_event_list_form_item_parse('selectPlaceReferenceId',$values,$calOptions,false);
	demosphere_event_list_form_item_parse('mapShape'		      ,$values,$calOptions,false);
	demosphere_event_list_form_item_parse('maxNbEventsPerDay'     ,$values,$calOptions,false);
	demosphere_event_list_form_item_parse('mostVisited'		      ,$values,$calOptions,false);
	demosphere_event_list_form_item_parse('selectVocabTerm'	      ,$values,$calOptions,false);
	demosphere_event_list_form_item_parse('selectVocabTermOr'     ,$values,$calOptions,false);
	demosphere_event_list_form_item_parse('selectRecentChanges'   ,$values,$calOptions,false);

	$calOptions['selectStartTime']=$selectStartTime;
	$calOptions['endTime'  ]=$endTime;
	$calOptions['fullUrls' ]=true;
	$calOptions['lastBigChange' ]=true;
	
	// hash for quick unsubscribe link
	$auth=substr(hash('sha256',$uid.':unsubscribe:'.$demosphere_config['secret']),0,15);

	// ***** fetch all events and render them using a template

	$events=demosphere_event_list($calOptions);

	if(count($events)===0 && !$sendEvenIfEmtpy){return false;}

	// use demosphere_calendar_render to build a pre-rendered version
	demosphere_calendar_render($events,$calOptions,false,true);

	$html=template_render('templates/demosphere-email-subscription.tpl.php',
						  compact('events','values','auth','uid'),
							 []);
	//echo $html;die('oo');

	// **** compute text version of whole page from the html version

	$text=$html;
	// add stars and extra newlines before html to text transform
	$text=str_replace('<h3>',"<h3>******* ",$text);
	$text=str_replace('</h3>',' *******<br/></h3>',$text);
	$text=str_replace('</tr>',"<br/></tr>",$text);
	$text=Html2Text::convert($text);
	// move links to next line
	$text=preg_replace('@\[([^\]]*)\]\((http[^)]*)\)([^\n]*)@','$1$3'."\n".'$2',$text);

	//echo '<pre>'.ent($text).'</pre>';die('oo');

	
	// ******* email subject
	$subject=$values['email-subject'];
	if($subject===t('Demosphere calendar')) // update old default value
	{
		$subject=t('@sitename calendar',
				   ['@sitename'=>$demosphere_config['site_name']]).
			' - '.t('%number events');
	}
	$subject=str_replace('%number',count($events),$subject);

	// ******* Create the PHPMailer email
	require_once 'lib/class.phpmailer.php';
	$mail=new PHPMailer();
	$mail->CharSet="utf-8";
	if(isset($values['reply-to']) && $values['reply-to']!=='')
	{
		$mail->AddReplyTo($values['reply-to']);
	}
	$mail->SetFrom($from);
	$mail->AddAddress($to);
	$mail->Subject=$subject;
	$mail->MsgHTML($html);
	$mail->AltBody = $text;
	$mail->AddCustomHeader('Precedence','bulk');
	$mail->AddCustomHeader('List-Unsubscribe','<'.$base_url.'/unsubscribe/'.$uid.'?auth='.$auth.'>');

	// ******* Actually send email

	//echo "demosphere_email_subscription_send: sending mail to: ".ent($to)." subject:".ent($mail->Subject)."<br/>\n";
	//echo $html;die("zzz");
	//echo '<pre>'.ent($html);die("zzz");

	// never send emails from test site
	if($demosphere_config['debug_rule']=='localhost-test'){return true;}
	if(!$mail->Send()) 
	{
		dlib_message_add(t("Error while sending to @to.",['@to'=>$to])."<br/>".
						   ent($mail->ErrorInfo),'error');
		return false;
	}

	$stats=variable_get('email_subscription_sent_stats','',[]);
	$stats[]=time();
	$stats=array_filter($stats,function($t){return $t>time()-24*3600;});
	variable_set('email_subscription_sent_stats','',$stats);
	variable_set('email_subscription_sent_count','',count($stats));

	return true;
}

function demosphere_email_subscription_unsubscribe($uid)
{
	global $demosphere_config;
	$auth=substr(hash('sha256',$uid.':unsubscribe:'.$demosphere_config['secret']),0,15);
	if($auth!==val($_GET,'auth')){dlib_permission_denied_403('invalid auth for unsubscribe',false);}

	$euser=User::fetch($uid);
	$form=[];
	$form['message']=['html'=>'<h2>'.t('Unsubscribe').'</h2>'.
					  '<p>'.t('Are you sure you want to unsubscribe @email ?',['@email'=>$euser->email]).'</p>',
					 ];

	if(!isset($euser->data['demosphere_email_subscription']) ||
	   !val($euser->data['demosphere_email_subscription'],'active'))
	{
		return '<p>'.t('You are already unsubscribed.').'</p>';
	}

	$form['ok'] = 
		[
			'type' => 'submit',
			'value' => t('Confirm'),
			'redirect'=>'email-subscription-form',
			'submit'=>function()use($euser)
			{
				$euser->data['demosphere_email_subscription']['active']=false;
				$euser->save();
				dlib_message_add(t('You have been successfully unsubscribed.'));
			}
		];
	$form['cancel'] = 
		[
			'type' => 'submit',
			'value' => t('Cancel'),
			'redirect'=>'email-subscription-form'
		];

	require_once 'dlib/form.php';
	return form_process($form);
}

//! Returns an array with list of users that are subscribed.
//! This is pretty slow
function demosphere_email_subscription_subscribers()
{
	global $demosphere_config;
	$qr=db_query("SELECT id,login,data FROM User WHERE ".
				 "data LIKE '%%demosphere_email_subscription%%' ORDER BY created DESC");

	$res=[];
	while ($euser = db_fetch_object($qr))
	{
		$values=unserialize($euser->data,['allowed_classes'=>false]);
		if(!isset($values['demosphere_email_subscription'])){continue;}
		$values=$values['demosphere_email_subscription'];
		if(!val($values,'active')){continue;}
		$res[$euser->id]=['id'   =>$euser->id,
						  'login'=>$euser->login,
						  'prefs'=>$values];
	}
	return $res;
}

// **************  Quick (one click) email subscription ****************

/**
 * Builds Drupal form array for the quick email subscription.
 *
 * This form is created with the dtoken: "demosphere_dtoken_quick_email_subscribe"
 */
function demosphere_email_subscription_quick_form()
{
	global $user,$base_url;

	demosphere_backup_site_assert();

	$formOpts=[];

	if($user->id!=0)
	{
		$items['message']=['html'=>'<em>'.
						   t('You are already logged into your account, please configure your email <a href="!config">subscription options here</a>.',
							 ['!config'=>$base_url.'/email-subscription-form']).'</em>',
						  ];
		return [$items,$formOpts];
	}

	$items['email']=
		['type' => 'email',
		 'title' => t('Your email address'),
		 'required' => true,
		 'size' => 20,
		 'check-dns'=>true,
		 'validate'=>'demosphere_email_subscription_quick_form_validate_email',
		];
	$items['spamcheck']=['type'=>'spamcheck'];
	$items['submit']=
		['type' => 'submit',
		 'value'=>t('Subscribe!'),
		 'validate'=>'demosphere_email_subscription_quick_form_validate',
		 'submit'=>'demosphere_email_subscription_quick_form_submit',
		];
	return [$items,$formOpts];
}

function demosphere_email_subscription_quick_form_validate_email($email)
{
	global $base_url;
	global $user;

	if($user->id!=0)
	{
		dlib_message_add(t('You are already logged into your account, please configure your email <a href="!config">subscription options here</a>.',
							 ['!config'=>$base_url.'/email-subscription-form']), 'error');
		dlib_redirect($base_url.'/email-subscription-form');
	}

	// **** Check if not already used
	if(User::searchByEmail($email)!==false)
	{
		return t('This email address is already used. Please <a href="!loginurl">login here</a>, or go <a href="!losturl">here if you have lost</a> your login or password.',
				 ['!loginurl'=>$base_url.'/user',
				  '!losturl' =>$base_url.'/user/forgot-password',
				 ]);
	}
	return true;
}

function demosphere_email_subscription_quick_form_validate($items)
{
	require_once 'dlib/mail-and-mime.php';
	$ok=true;

	$ip=$_SERVER['REMOTE_ADDR'];

	if(dlib_throttle_count('email-subscription-quick-form::'.$ip,24*3600)>1)
	{
		dlib_message_add(t('You should not use this form to subscribe other people.'),'error');
		$ok=false;
	}

	if(dlib_check_ip_in_stopforumspam($ip)===true ){$ok=false;}
	if(dlib_check_dnsbl              ($ip)!==false){$ok=false;}

	if(!$ok)
	{
		dlib_message_add(t('Your IP address is not allowed to use this form. Please contact the admin.'),'error');
	}

	return $ok;
}

/**
 * Creates a user from an email submited in the quick subscribe form.
 *
 * The username and password are random. The new user is automatically logged-in
 * and redirected to the event email options form.
 *
 * @see demosphere_email_subscription_form()
 */
function demosphere_email_subscription_quick_form_submit($items)
{
	global $base_url,$demosphere_config;
	$email=$items['email']['value'];

	dlib_throttle_add('email-subscription-quick-form::'.$_SERVER['REMOTE_ADDR']);
	
	// don't use email address as username, as it might appear later in comments...

	$name='euser-'.mt_rand(10000,99999);
	// find unique name
	while(db_result("SELECT COUNT(*) FROM User WHERE login='%s'",$name)!=0)
	{
		$name=preg_replace('@-[0-9]+$@','-'.mt_rand(10000,99999),$name);
	}

    // Create user
	$eUser=new User();
	$eUser->login=$name;
	$eUser->email='';
	$eUser->setHashedPassword(dlib_random_string(20));
	// activate email send (set user email options from defaults)
	$options=demosphere_email_subscription_default_values();
	$options['active']=true;
	$eUser->data['demosphere_email_subscription']=$options;
	$eUser->save();

	// now actually log in the user
	require_once 'demosphere-user.php';
	demosphere_user_login($eUser);

	demosphere_user_email_verify_send($eUser,$email);

	dlib_redirect($base_url.'/email-subscription-form');
}

//! Send a single event to all users that are subscribed and that have selected "urgent events".
//! Called from event edit form submit by moderator.
function demosphere_email_subscription_send_urgent_event($event)
{
	global $demosphere_config,$base_url;
	require_once 'lib/class.phpmailer.php';
	require_once 'demosphere/demosphere-event-map.php';

	$defaults=demosphere_email_subscription_default_values();
	$nbSent=0;
	foreach(db_one_col('SELECT id FROM User') as $uid)
	{
		$euser=User::fetch($uid);
		$values=$euser->data;
		if(!isset($values['demosphere_email_subscription']        )){continue;}
		$values=$values['demosphere_email_subscription'];
		if(!val($values,'active')){continue;}
		if(!$values['urgent-events']){continue;}

		// **** We found a user that wants urgent event email. 
		// Now check if it meets his selection criteria
		require_once 'demosphere-event-list.php';
		require_once 'demosphere-event-list-form.php';
		$calOptions=[];
		demosphere_event_list_form_item_parse('nearLatLng'       ,$values,$calOptions,false);
		demosphere_event_list_form_item_parse('selectTopic'		 ,$values,$calOptions,false);
		demosphere_event_list_form_item_parse('selectCityId'	 ,$values,$calOptions,false);
		demosphere_event_list_form_item_parse('mapShape'		 ,$values,$calOptions,false);
		demosphere_event_list_form_item_parse('maxNbEventsPerDay',$values,$calOptions,false);
		demosphere_event_list_form_item_parse('mostVisited'		 ,$values,$calOptions,false);
		demosphere_event_list_form_item_parse('selectVocabTerm'	 ,$values,$calOptions,false);
		demosphere_event_list_form_item_parse('selectVocabTermOr',$values,$calOptions,false);

		$calOptions['default_noOtherFields']=true;
		$calOptions['id']=true;
		$events=demosphere_event_list($calOptions);
		$eventsIds=dlib_object_column($events,'id');
		if(array_search($event->id,$eventsIds)===false){continue;}
		if($euser->email===''                         ){continue;}

		// **** This event meets user's selection. We can now send the email.

		$subject=t('!sitename: urgent : !date : !title',
				   ['!sitename'=>$demosphere_config['site_name'],
					'!date'=>demos_format_date('short-date-time',$event->startTime),
					'!title'=>$event->title,
				   ]);
		$html=template_render('templates/demosphere-event-urgent-email.tpl.php',
							  [compact('base_url','event'),
							   'site_name'=>$demosphere_config['site_name'],
							   'place'=>$event->usePlace(),
							   'city'=>$event->usePlace()->getCityName(),
							   'map_url'=>demosphere_get_map_url($event->usePlace(),$event),
							  ]);

		$mail=new PHPMailer();
		$mail->CharSet="utf-8";
		$mail->SetFrom($demosphere_config['email_subscription_from']);
		$mail->AddAddress($euser->email);
		$mail->Subject=$subject;
		$mail->MsgHTML($html);
		$mail->AltBody = Html2Text::convert($html);

		// never send emails from test site
		if($demosphere_config['debug_rule']=='localhost-test'){continue;}

		if(!$mail->Send()) 
		{
			dlib_message_add(t("Error while sending urgent event to @to.",['@to'=>$to])."<br/>".
							 ent($mail->ErrorInfo),'error');
			continue;
		}
		$nbSent++;
	}
	dlib_message_add(t('Successfully sent urgent event to !nbSent users',['!nbSent'=>$nbSent]));
	$event->logAdd('send-urgent-event to '.$nbSent.' users');
}

//! Only meant to be called from update-db when new values are added to form
function demosphere_email_subscription_update_defaults()
{
	$defaults=demosphere_email_subscription_default_values();
	$nbSent=0;
	foreach(db_one_col('SELECT id FROM User') as $uid)
	{
		$euser=User::fetch($uid);
		$values=$euser->data;
		if(!isset($values['demosphere_email_subscription']        )){continue;}
		$values=$values['demosphere_email_subscription'];
		$new=array_diff(array_keys($defaults),array_keys($values  ));
		$old=array_diff(array_keys($values  ),array_keys($defaults));
		if(!count($new) && !count($old)){continue;}
		foreach($new as $k){$values[$k]=$defaults[$k];}
		foreach($old as $k){unset($values[$k]);}
		$euser->data['demosphere_email_subscription']=$values;
		$euser->save();
	}
}


function demosphere_email_subscription_foreach($fct)
{
	$defaults=demosphere_email_subscription_default_values();
	$nbSent=0;
	foreach(db_one_col('SELECT id FROM User') as $uid)
	{
		$euser=User::fetch($uid);
		$values=$euser->data;
		if(!isset($values['demosphere_email_subscription']        )){continue;}
		$values=$values['demosphere_email_subscription'];
		if($fct($values,$euser))
		{
			$euser->data['demosphere_email_subscription']=$values;
			$euser->save();
		}
	}
}

//function demosphere_email_subscription_tmp()
//{
// 	demosphere_email_subscription_foreach(
// 	function(&$values,$euser)
// 	{
// 		echo $euser->id;var_dump($values['select-recent-changes']);
// 	});
//}

function demosphere_email_subscription_bounces()
{
	global $demosphere_config;
	if(!$demosphere_config['hosted']){return;}
	$bounces=demosphere_emails_bounces_parse();
    $now=time();
    demosphere_email_subscription_foreach(function(&$values,$euser) use($bounces,$now)
    {
        if(!val($values,'active')){return;}
        if($euser->email===''    ){return;}
        $timestamps=val($bounces,$euser->email);
        if($timestamps===false){return;}
        // Count number of bounces (since last auto-unsubscribe)
        $ct15=0;
        $ct90=0;
        foreach($timestamps as $ts)
        {
            if($ts<val($values,'bounce-unsubscribed',0)){continue;}
            if($ts>$now-3600*24*90){$ct90++;}
            if($ts>$now-3600*24*15){$ct15++;}
        }
        if($ct90>15 && $ct15>2)
        {
            $values['active']=false;
            $values['bounce-unsubscribed']=$now;
            echo "Unsubscribing ".ent($euser->email)." : too many bounces\n";
            return true;
        }
    });
}


?>