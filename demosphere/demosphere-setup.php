<?php

function demosphere_start()
{
	//require_once 'demosphere-all.php';demosphere_all(); // uncomment for profiling
	//xdebug_start_code_coverage();
	//xdebug_start_trace('/tmp/xdebug-trace');

	demosphere_setup_part1();

	if(demosphere_page_cache_serve_page(dlib_current_url())){return;}

	demosphere_setup_part2();

	demosphere_exec_request(val($_GET,'q','/'));

	//var_dump(xdebug_get_code_coverage());
	//xdebug_stop_trace();
}

function demosphere_setup_part1()
{
	global $base_url;

	// ******** includes
	ini_set("include_path",ini_get("include_path").':'.dirname(__FILE__));

	require_once 'dlib/tools.php';
	require_once 'dlib/dlib-debug.php';
	require_once 'dlib/translation.php';
	require_once 'demosphere-page-cache.php';

	// ******** setup dlib (mb encoding, libxml errors)
	dlib_setup();	

	// ******** $base_url
	$base_url=demosphere_setup_base_url();

	// ******** mobile
	demosphere_setup_mobile();

	// ******** class autoloader
	spl_autoload_register('demosphere_class_autoloader');

	// ******** site-specific config file (notably database connection info)
	require_once 'site-config.php';
	$siteConfig=site_config();

	// ******** database 
	global $db_config;
	$db_config=$siteConfig['database'];
	require_once 'dlib/database.php';
	db_setup();

	// ******** session
	demosphere_setup_session();
}

function demosphere_setup_part2()
{
	global $base_url,$currentPage,$user,$demosphere_config,$custom_config;

	// ******** setup global $user
	demosphere_setup_init_global_user();

	// ******** setup global $demosphere_config
	demosphere_setup_demosphere_config();

	if($base_url===false){$base_url=$demosphere_config['std_base_url'];}

	// ******** debuging
	demosphere_setup_debug($demosphere_config['debug_rule']);

	// ******** Time locale & timezone
	if(setlocale(LC_ALL, $demosphere_config['locale'])===false)
	{fatal("failed setting locale to :".$demosphere_config['locale']);}
	// Otherwise "," is used for decimal points, which breaks lots of internal stuff.
	setlocale(LC_NUMERIC, 'C');
	// Note: date_default_timezone_set is very slow (??!!) : (4% exec time!!)
	// date_default_timezone_set($demosphere_config['timezone']);
	// This is much faster, but afterwards its strftime that becomes slow.
	// PHP probably sets up stuff on first call of one of these fcts.
	ini_set('date.timezone',$demosphere_config['timezone']);

	// ******** feed-import, mail-import, etc.
	demosphere_setup_components_config();

	// ******** backup site
	demosphere_setup_backup_site();

	// ******** template system
	global $template_config;
	$template_config=['dir'=>'files/private/templates',
					  'filename_cleanup_regexp'=>'@^(/var/www/demosphere/|/var/www/|/usr/local/demosphere/master/|/usr/local/demosphere/paris/|/home/[^/]*/|.*/demosphere|.*/sites/all/themes)@',
					  'path'=>['custom','.',dirname(__FILE__)],
					  'variables_hook'=>false,
					  'common_vars'=>['base_url'=>$base_url,
									  'user'=>$user,
									  'demosphere_config'=>$demosphere_config,
									 ]];

	// ******** dlib comments
	demosphere_setup_comments();

	// ******** dbobject_ui (backend web interface for models)
	demosphere_setup_dbobject_ui();

	// ******** dbtable_ui (backend web interface for db tables)
	demosphere_setup_dbtable_ui();

	// ******** $currentPage
	$currentPage=new HtmlPageData();
	$currentPage->robots=['noindex','nofollow'];// defaults
	$currentPage->bodyClasses[]='role-'.str_replace(' ','-',$user->getRole());
	// tmp 1/2018: experimental: report any js errors
	$currentPage->addJs('window.onerror=function(msg,url,line,col,error){if(typeof demosphere_errct==="undefined"){demosphere_errct=0;}if(demosphere_errct++>=4){return;}document.createElement("img").src="/js-error-report?msg="+encodeURIComponent(msg)+"&url="+encodeURIComponent(url)+"&line="+line+"&col="+col+"&error="+encodeURIComponent(error)+(typeof error==="object" && error!==null &&  typeof error.stack!=="undefined" ? "&stack="+encodeURIComponent(error.stack) : "");};','inline');

	// ******** css templates and sprites
	demosphere_setup_css_template();
	demosphere_setup_sprites();

	// ******** custom php extensions for some sites
	$custom_config=[];
	@include 'custom/custom.php';
	if(function_exists('custom_setup')){custom_setup();}
}

// ************************************************************
// Setup functions, in order of execution
// ************************************************************

function demosphere_setup_base_url()
{
	global $dlib_config;
	if(val($dlib_config,'is_commandline')){return false;}
	return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']== "on" ? "https://" : "http://").
			$_SERVER['HTTP_HOST'].
			rtrim(dirname($_SERVER['SCRIPT_NAME']), '\/'); 
}

function demosphere_setup_mobile()
{
	global $base_url,$dlib_config;
	if(val($dlib_config,'is_commandline')){return;}
	$host=dlib_host_url($base_url);

	// no redirect and mobile handling on safe domains
	if(strpos($base_url,'http://safe.' )===0 ||
	   strpos($base_url,'https://safe.')===0    ){return;}

	$urlIsMobile=demosphere_is_mobile();
	$detectedIsMobile=demosphere_detect_mobile();
	$url=$_SERVER['REQUEST_URI'];

	//  if user clicks on switch button, then store prefererence in cookie on both domains
	if(isset($_GET['switchMobile']))
	{
		// Switching is done in two steps (because cookies are domain dependent):
		// 1) a) set cookie and b) redirect to the right domain (mobile or plain), keeping switchMobile get arg
		// 2) a) set cookie and b) redirect to url without switchMobile

		$destIsMobile=$_GET['switchMobile']==='mobile';


		// step 1) a) set cookie on current domain
		setcookie('mobile-preference',($destIsMobile ? '1' : '0').($detectedIsMobile ? 'm':'s'),time()+3600*24*30*2,'/');

		// step 1) b) redirect to correct domain
		if($destIsMobile!=$urlIsMobile)
		{
			if($destIsMobile)
			{
				dlib_redirect(preg_replace('@^(https?://)(www\.)?@','$1mobile.',$host).$url);
			}
			else
			{
				dlib_redirect(preg_replace('@^(https?://)mobile\.@','$1',$host).$url);
			}
		}

		// step 2) a) set cookie on dest domain
		setcookie('mobile-preference',($destIsMobile ? '1' : '0').($detectedIsMobile ? 'm':'s'),time()+3600*24*30*2,'/');

		// step 2) b) redirect to url without switchMobile
		$url0=$url;
		$url=preg_replace('@\?switchMobile=[a-z]+&?@','?',$url);
		$url=preg_replace('@&switchMobile=[a-z]+@','',$url);
		$url=preg_replace('@[?]$@','',$url);
		if($url===$url0){fatal('Invalid switchMobile url');}
		dlib_redirect($host.$url);
	}

	// Redirect to detected type for users that have never clicked on switch button
	if(!isset($_COOKIE['mobile-preference']))
	{
		if($detectedIsMobile && !$urlIsMobile)
		{
			dlib_redirect(preg_replace('@^(https?://)(www\.)?@','$1mobile.',$host).$url);
		}

		if(!$detectedIsMobile && $urlIsMobile)
		{
			dlib_redirect(preg_replace('@^(https?://)mobile\.@','$1',$host).$url);
		}
	}
	else
	{
		// Redirect to prefered type for users that have already clicked on switch button

		if($_COOKIE['mobile-preference']==1 && !$urlIsMobile)
		{
			dlib_redirect(preg_replace('@^(https?://)(www\.)?@','$1mobile.',$host).$url);
		}

		if($_COOKIE['mobile-preference']==0 && $urlIsMobile)
		{
			dlib_redirect(preg_replace('@^(https?://)mobile\.@','$1',$host).$url);
		}
	}
}

//! returns whether we are on a site that has "mobile." in its url.
function demosphere_is_mobile($url=false)
{
	global $base_url;
	if($url===false){$url=$base_url;}
	return strpos($url,'http://mobile.' )===0 ||
		strpos($url,'https://mobile.')===0;
}

//! User agent sniffing to tell if this is a mobile device.
function demosphere_detect_mobile($useragent=false)
{
	// Update this regexp regularly from  http://detectmobilebrowsers.com/
	// http://detectmobilebrowsers.com/download/php (last update: 8/7/2015)
	// FIXME: that site is dead since august 1 2014 !
	if($useragent===false)
	{
		if(!isset($_SERVER['HTTP_USER_AGENT'])){return false;}
		$useragent=$_SERVER['HTTP_USER_AGENT'];
	}

	return preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4));
}

//! sets-up php ini values and uses dlib database sessions (dbsessions).
function demosphere_setup_session()
{
	require_once 'dlib/dbsessions.php';
	// http://php.net/manual/en/session.configuration.php
	// Use session cookies, not transparent sessions that puts the session id in
	// the query string.
	ini_set('session.use_cookies', '1');
	ini_set('session.use_only_cookies', '1');
	ini_set('session.use_trans_sid', '0');
	// Don't send HTTP headers using PHP's session handler.
	ini_set('session.cache_limiter', 'none');
	// Use httponly session cookies.
	ini_set('session.cookie_httponly', '1');

	// Cookie can only be used via https 
	ini_set('session.cookie_secure', '1');

	// Long lasting session
	ini_set('session.cookie_lifetime', 2000000);
	ini_set('session.gc_maxlifetime', 200000);

	session_name('DEMSESSID');
	dbsessions_setup();
	session_start();
}

function demosphere_setup_init_global_user()
{
	global $user;
	// ***** load current user
	$user=null;
	if(isset($_SESSION['uid']))
	{
		$user=User::fetch($_SESSION['uid'],false);
		// update lastAccess time for this user
		if($user!==null && $user->lastAccess<time()-60*3)
		{
			$user->lastAccess=time();
			$user->lastIp=$_SERVER['REMOTE_ADDR'];
			$user->save();
		}
	}
	if($user===null)
	{
		$user=new User();
		$user->login='anonymous';
	}
}

/**
 * Read the default config and overide with custom config.
 *
 * Config goes to global variable $demosphere_config  .
 */
function demosphere_setup_demosphere_config()
{
	global $demosphere_config,$base_url,$dlib_config;

	require_once 'dlib/variable.php';
	// security whitelist : these classes are used by event-repetition-copy
	$dlib_config['variable_unserialize_allowed_classes']=['Event','Place','City'];

	// read demosphere_config from DB using dlib variable system
	$demosphere_config=variable_get('demosphere_config');

	// auto set std_base_url if not set
	if(strlen($base_url) && (!isset($demosphere_config['std_base_url']) || $demosphere_config['std_base_url']===false))
	{
		$demosphere_config['std_base_url']=$base_url;
		variable_set('demosphere_config','',$demosphere_config);
	}

	if(!isset($demosphere_config['secret']) || strlen($demosphere_config['secret'])<5 )
	{
		$demosphere_config['secret']=bin2hex(openssl_random_pseudo_bytes(30));
		variable_set('demosphere_config','',$demosphere_config);
	}

	// non-public readable directory where all writable files and directories will be created
	$demosphere_config['tmp_dir' ]='files/private/tmp';

	$siteConfig=site_config();
	$demosphere_config['site_id']=$siteConfig['site_id']; 

	if(!isset($demosphere_config['is_backup_site'])){$demosphere_config['is_backup_site']=$siteConfig['is_backup_site'];} 

	// css override path
	$demosphere_config['css_override_path']=['files/css','custom',];
	// image override path
	$demosphere_config['image_override_path']=['files/images','custom',];


	$demosphere_config['file_browser']=
		[
		 'css'=>[
				 'dir'=>'files/css',
				 'filter'=>'@^[a-zA-Z0-9_.-]+\.css$@',
				 'accept'=>'text/css',
				 'editable'=>'@\.css$@',
				 'form_alter'=>'demosphere_file_browser_css',
				 ],
		 'images'=>[
				 'dir'=>'files/images',
				 'filter'=>'@[a-zA-Z0-9_.-]+\.(png|svg|gif|jpg|jpeg|ico)$@',
				 'accept'=>'image/*',
				 'editable'=>false,
				 'form_alter'=>'demosphere_file_browser_images',
				 ],
		   ];

	// These are values that do not appear in config form, but are set in a custom module
	//$demosphere_config['remove-drupal-javascript']=true;
}

//! Setup 'mail-import','feed-import', 'page-watch','text-matching','html-selector','dlib_config', ... 
function demosphere_setup_components_config()
{
	global $demosphere_config,$base_url;

	foreach(['mail-import','feed-import', 'page-watch','text-matching','html-selector'] as $dcomp)
	{
		demosphere_setup_dcomp_config($dcomp);
	}

	global $feed_import_config;
	$feed_import_config['log_file']='files/private/feed-import.log';
	$feed_import_config['tmp_dir' ]='files/private/tmp';

	global $mail_import_config;
	$mail_import_config['tmp_dir']='files/private/tmp';
	$mail_import_config['cache_dir']='files/private/mail-import-files';
	$mail_import_config['mbox_abs_path']=$_SERVER['DOCUMENT_ROOT'].'/'.'files/private/mail-import-files';
	//var_dump($mail_import_config);

	global $text_matching_config;
	$text_matching_config['std_base_url']=$demosphere_config['std_base_url'];
	$text_matching_config['doctypes']=['event'      =>'demosphere_event_text_matching_hook',
									   'feed_import'=>'feed_import_text_matching_hook',
									   'mail_import'=>'mail_import_text_matching_hook',
									   'tmpdoc'     =>'demosphere_tmpdoc_text_matching_hook',];
	$text_matching_config['includes']=['event'      => 'demosphere/demosphere-event.php',
									   'feed_import'=> 'feed-import/feed-import.php',
									   'mail_import'=> 'mail-import/mail-import.php',
									   'tmpdoc'     => 'demosphere/demosphere-event-import.php',];
	global $page_watch_config;
	$page_watch_config['log_file']='files/private/page-watch.log';

	global $dcomponent_config;
	$dcomponent_config['dir_url']=$base_url.'/demosphere/dcomponent';
	$dcomponent_config['safe_base_url']=$demosphere_config['safe_base_url'];

	global $docconvert_config;
	$docconvert_config['dir_url']=$base_url.'/demosphere/docconvert';
	$docconvert_config['log']='files/private/docconvert.log';
	$docconvert_config['tmp_dir']=$demosphere_config['tmp_dir'];

	global $dlib_config;
	$dlib_config['log'      ]='files/private/demosphere-general.log';
	$dlib_config['error_log']=getcwd().'/files/private/error.log'; // full path because errors can happen when env is messed up (after end)
	$dlib_config['no_wget_sites']=$demosphere_config['no_wget_sites'];
	$dlib_config['no_ssl_cert_sites']=$demosphere_config['no_ssl_cert_sites'];
	$dlib_config['secret']=$demosphere_config['secret'];
	$dlib_config['css_file_alter'         ]=function($css      ){require_once 'demosphere-common.php';return demosphere_css_overrides_list($css);};
	$dlib_config['html_page_script_render']=function($src,$desc){require_once 'demosphere-common.php';return demosphere_js_script_render($src,$desc);};
	$dlib_config['html_page_always_cache_checksum']=true;
	$dlib_config['translation_files']=["dlib/translations/dlib.pot","demosphere/translations/demosphere.pot"];
	$dlib_config['translation_exec_path']='demosphere_exec_request';
	$dlib_config['backtrace_file']='demosphere_backtrace_file';
	$dlib_config['permission_denied_403_override']=
		function($message=false,$tryLogin=true){require_once 'demosphere-common.php';return demosphere_permission_denied_403($message,$tryLogin);};
	$dlib_config['tmp_dir']=$demosphere_config['tmp_dir'];
	$dlib_config['data_url_image_dir']='files/images/data-url';
	$dlib_config['format_date']=function($d,$t1,$t2){require_once 'demosphere-date-time.php'; return demos_format_date($d,$t1,$t2);};

	global $db_config;
	$db_config['log_slow_queries']=$demosphere_config['log_slow_queries'];

	global $html_selector_config;
	$html_selector_config['tmp_dir']=$demosphere_config['tmp_dir'];
	//var_dump($demosphere_config);
}

/** Setup configuration for each "semi-independent" component
 * ('mail-import' 'feed-import'  'page-watch' 'text-matching').
 *
 * Note, we can't call functions in these components, or else we would need to 
 * include/require files which would include/require other files... and lead 
 * to a performance problem.
 */
function demosphere_setup_dcomp_config($dcomp)
{
	// ********* setup environement config
	global $base_url,$demosphere_config,$db_config;
	$configVarName=str_replace('-','_',$dcomp).'_config';
	global $$configVarName;
	$config=&$$configVarName;

	// read the config from the demosphere config
	// FIXME: this is a big performance hit on each page !
	$default_config=[];
	$dcompu=str_replace('-','_',$dcomp).'_';
	$dcompul=strlen($dcompu);
	$keys=preg_grep('@^'.$dcompu.'@',array_keys($demosphere_config));
	foreach($keys as $k)
	{
		$default_config[substr($k,$dcompul)]=$demosphere_config[$k];
	}

	// check for alt config (this is used in unit tests)
	if(!isset($$configVarName)){$$configVarName=$default_config;}

	// path and url
	$config['dir_url'  ]=$demosphere_config['std_base_url'].'/demosphere/'.$dcomp;
}

//! Changes config on a backup site
//! On a backup site, users should not be allowed to post anything (comments, new events, etc.)
function demosphere_setup_backup_site()
{
	global $demosphere_config;
	$siteConfig=site_config();
	$isBackupSite=$siteConfig['is_backup_site'];
	// check for first run in backup site
	if($isBackupSite && !$demosphere_config['is_backup_site'])
	{
		$demosphere_config['is_backup_site']=true;
		variable_set('demosphere_config','',$demosphere_config);
	}
	if(!$isBackupSite && $demosphere_config['is_backup_site'])
	{
		$demosphere_config['is_backup_site']=false;
		variable_set('demosphere_config','',$demosphere_config);
	}
}

//! Fails with error if this site is a backup site. 
//! Use this to block write operations (submit event, comment, ...) that are not permitted on read-only backup site.
function demosphere_backup_site_assert()
{
	global $demosphere_config;
	if(val($demosphere_config,'is_backup_site'))
	{
		fatal(t('The main web site for @sitename is not working. We are fixing it. You are seeing a backup site. We hope the main web site will be back soon. Sorry for the inconvenience.',['@sitename'=>$demosphere_config['site_name']]));
	}
}

function demosphere_setup_debug($rule)
{
	global $demosphere_config,$user,$dlib_config;

	$dlib_config['debug']=
		$rule==='on' ||
		$rule==='localhost-test' ||
		$rule==='uid='.$user->id;

	if($dlib_config['debug'])
	{
		ini_set('display_errors',true);
		if(!$dlib_config['is_commandline']){ini_set('html_errors',true);}
		error_reporting(E_ALL);
		// We want pretty printed errors on live server (without xdebug)
		set_error_handler('dlib_debug_error_handler');
		set_exception_handler('dlib_debug_exception_handler');
	}
}

//! Setup demosphere customizations for dlib/comments
function demosphere_setup_comments()
{
	global $comments_config,$base_url,$user,$demosphere_config;
	$comments_config=
		['page_types'=>['Event'=>[],'Post'=>[]],
		 'user'=>['id'=>$user->id,
				  'isAdmin'     =>$user->checkRoles('admin','moderator'),
				  'isSuperAdmin'=>$user->checkRoles('admin'),
				  'nameFct'=>function(int $uid): string
			      {
					  $out=ent(db_result_check('SELECT login FROM User WHERE id=%d',$uid));
					  return $out;
				  },
				  'url'=>function(int $uid): string
			      {
					  global $base_url;
					  return $base_url.'/user/'.intval($uid);
				  },
				  'canView'=>function(string $pageType,int $pageId): bool
			      {
					  global $user,$comments_config;
					  if(!isset($comments_config['page_types'][$pageType])){fatal('invalid pageType');}
					  $o=$pageType::fetch($pageId,false);
					  if($o===null){return false;}
					  if($pageType==='Event'){return $o->access('view');}
					  else{return $user->checkRoles('admin','moderator') || $o->status;}
				  },
				 ],
		 'link'=>function(Comment $comment): string
			{
				global $demosphere_config,$base_url;
				return $comment->pageType=='Event' ? 
				$base_url.'/'.$demosphere_config['event_url_name'].'/'.intval($comment->pageId).'#comment-'.intval($comment->id) : 
				$base_url.'/post/'.                                    intval($comment->pageId).'#comment-'.intval($comment->id);
			},
		 'guidelines_url'=>function(){return Post::builtInUrl('comment_guidelines');},
		 'form_alter'=>function($comment,&$items,&$options)
		 {require_once 'demosphere-misc.php';demosphere_comment_form_alter($comment,$items,$options);},
		 'body_render'=>function($html)
		 {require_once 'demosphere-misc.php';return demosphere_comment_body_render($html);},
		 'body_post_render'=>function($html)
		 {require_once 'demosphere-misc.php';return demosphere_comment_body_post_render($html);},
		 'manager_ip'  =>function($ip)
		 {require_once 'demosphere-misc.php';return demosphere_comment_manager_ip($ip);},
		 'manager_item'=>function($comment)
		 {require_once 'demosphere-misc.php';return demosphere_comment_manager_item($comment);},
		 'site_settings'=>$demosphere_config['comments_site_settings'] ?? 'disabled', // FIXME TMP: remove ?? ... after 9/2019 upgrade
		 'throttle_1h' => 4,  // FIXMEc: should be in $demosphere_config
		 'throttle_48h'=>10,  // FIXMEc: should be in $demosphere_config
		 'save_hook'  =>function($comment){require_once 'demosphere-misc.php';demosphere_comment_save_hook($comment);},
		 'delete_hook'=>function($comment){require_once 'demosphere-misc.php';demosphere_comment_delete_hook($comment);},
		 'too_long_max_lines'=>15,
		 'line_width'=> demosphere_is_mobile() ? 500 : 715,
		 'notifications_vapid_subject'=> 'mailto:webpush@demosphere.net',
		];
	if($demosphere_config['is_backup_site'] && 
	   $comments_config['site_settings']!=='readonly' &&
	   $comments_config['site_settings']!=='disabled'    ){$comments_config['site_settings']='readonly';}
}

//! Customize built-in user interface for viewing/editing DBObject derived classes used in Demosphere
function demosphere_setup_dbobject_ui()
{
	global $dbobject_ui_config,$demosphere_config,$user,$base_url;
	$dbobject_ui_config=
		['classes'=>
		 [
			 'City' =>['setup-file'=>'demosphere-city.php'  ,'setup'=>'demosphere_city_class_info'  ],
			 'Event'=>['setup-file'=>'demosphere-event.php' ,'setup'=>'demosphere_event_class_info' ],
			 'Place'=>['setup-file'=>'demosphere-place.php' ,'setup'=>'demosphere_place_class_info' ],
			 'Post' =>['setup-file'=>'demosphere-post.php'  ,'setup'=>'demosphere_post_class_info'  ],
			 'Topic'=>['setup-file'=>'demosphere-topics.php','setup'=>'demosphere_topics_class_info'],
			 'Term' =>['setup-file'=>'demosphere-terms.php' ,'setup'=>'demosphere_terms_class_info' ],
			 'User' =>['setup-file'=>'demosphere-user.php'  ,'setup'=>'demosphere_user_class_info'  ],
			 'Carpool'=>[],
			 'Article'=>[],
			 'FakeFeed'=>['setup-file'=>'feed-import/feed-import-view.php','setup'=>'feed_import_view_fake_feed_class_info'],
			 'Feed'    =>['setup-file'=>'feed-import/feed-import-view.php','setup'=>'feed_import_view_feed_class_info'     ],
			 'MailRule'  =>['main_url'=>$base_url.'/mail-import/mail-rule',
							'setup-file'=>'mail-import/mail-import-view.php','setup'=>'mail_import_mail_rule_class_info'],
			 'Message'=>[],
			 'Page'=>
			 ['main_url'=>$base_url.'/page-watch/page',
			  'setup-file'=>'page-watch/page-watch-view.php','setup'=>'page_watch_page_class_info'],
			 'TMDocument'=>[],
			 'Comment'=>['new'=>['pageType'=>['type'=>'textfield'],
								 'pageId'  =>['type'=>'int'	  ],
								]],
			 'IpWhois'=>[],
			 'Opinion'=>[],
			 'RepetitionGroup'=>[],
			 'Ban' =>['setup-file'=>'dlib/comments/comments.php'  ,'setup'=>'comments_ban_class_info'  ],
		 ]];
}

//! Customize built-in user interface for viewing/editing database tables used in Demosphere
function demosphere_setup_dbtable_ui()
{
	global $dbtable_ui_config,$demosphere_config,$user,$base_url;
	$dbtable_ui_config=
		['tables'=>
				[
				 'daily_visits'             =>['schema'=>['event'   =>['type'=>'foreign_key','class'=>'Event'],
														  'day'     =>['type'=>'timestamp'],]],
				 'dbsessions'				=>['schema'=>['uid'     =>['type'=>'foreign_key','class'=>'User' ],
														  'last_access'=>['type'=>'timestamp'],]],
				 'event_revisions'			=>['schema'=>['event_id'=>['type'=>'foreign_key','class'=>'Event'],
														  'changed_by_id'=>['type'=>'foreign_key','class'=>'User'],
														  'changed' =>['type'=>'timestamp']],],
				 'log'						=>['schema'=>['user_id' =>['type'=>'foreign_key','class'=>'User' ],
														  'timestamp'=>['type'=>'timestamp'],]],
				 'migrate_nodes'			=>['schema'=>['id'      =>['type'=>'foreign_key','class'=>'Event']]],
				 'page_cache'				=>[],
				 'page_revision'			=>['schema'=>['pageId'  =>['type'=>'foreign_key','class'=>'Page' ],
														  'timestamp'=>['type'=>'timestamp'],]],
				 'path_alias'				=>[],
				 'place_search_index'		=>['schema'=>['pid'     =>['type'=>'foreign_key','class'=>'Place'],]],
				 'self_edit'				=>['schema'=>['event'   =>['type'=>'foreign_key','class'=>'Event'],
														  'created' =>['type'=>'timestamp'],]],
				 'tm_keyphrase_doc'		    =>['schema'=>['document_id'=>['type'=>'foreign_key','class'=>'TMDocument'],]],
				 'translations'			    =>[],
				 'user_calendar_selection'  =>['schema'=>['event'   =>['type'=>'foreign_key','class'=>'Event'],
														  'user'    =>['type'=>'foreign_key','class'=>'User' ]],],
				 'variables'				=>[],
				 ]
		 ];
}

function demosphere_setup_css_template()
{
	global $css_template_config,$currentPage,$demosphere_config;
	// ******** css template system and sprites
	$css_template_config=[
						  'dest_dir'=>'files/css/templates',
						  'svg_to_png_dir'=>'files/images/svg-to-png',
						  'variables'          =>function()    {require_once 'demosphere-misc.php';  return demosphere_css_template_variables();},
						  'colormap'     =>function($type,$svg){require_once 'demosphere-misc.php';  return demosphere_svg_colormap($type,$svg);},
						  'filename_hook'      =>function($f)  {require_once 'demosphere-common.php';return demosphere_cacheable($f);},
						  'image_override_hook'=>function($f)  {require_once 'demosphere-common.php';return demosphere_image_override($f);},
						  ];
	// add body class to tell css that this is a light (almost white) background
	if($demosphere_config['color_is_light_background']){$currentPage->bodyClasses[]='light-background';}
}


function demosphere_setup_sprites()
{
	global $sprite_config,$demosphere_config;
	$sprite_config=
		[
		 'enable'=>$demosphere_config['sprites_enable'],
		 'dest_dir'=>'files/images/sprites',
		 'svg_to_png'=>true,
		 'src_dir'=>'demosphere/css/images',
		 'sprites'=>
		 [
		  // Frontpage only (not mobile)
		  'frontpage.png'=>['images'=>['fp-logo.svg'      =>['colormap'=>true],
									   'fp-email.svg'     =>['colormap'=>true],
									   'fp-search.svg'    =>['colormap'=>true],
									   'fp-dayrank1.svg'  =>[],
									   'fp-dayrank2.svg'  =>[],
									   'fp-map.png'=>[],
									   'fp-white-logo.svg'=>[],
									   'fp-rss-logo.svg'  =>[],
									   'fp-new.svg'       =>[],
									   'city.svg'         =>['resize'=>'18x18','level_colors'=>'#ffffff,transparent','png_fname'=>'city.png'],
									   'map-point.svg#2'  =>['resize'=>'18x18','level_colors'=>'#ffffff,transparent','png_fname'=>'map-point-white.png'],
									   'dayrank.svg'      =>[],
									   'region.svg'       =>[],
									   'max-per-day.svg'  =>['resize'=>'18x18','level_colors'=>'#ffffff,transparent','png_fname'=>'max-per-day.png'],
									   'time.svg'         =>['resize'=>'18x18','level_colors'=>'#ffffff,transparent','png_fname'=>'time.png'],
									   'sort-recent.svg'  =>['resize'=>'18x18','level_colors'=>'#ffffff,transparent','png_fname'=>'sort-recent.png'],
									   'gps.svg'          =>['resize'=>'18x18','level_colors'=>'#ffffff,transparent','png_fname'=>'gps.png'],
									   'link.svg'         =>['resize'=>'18x18','colormap'=>true                     ,'png_fname'=>'link.png'],
									   ]],
		  // On all event pages (not mobile)
		  'page.png'=>     ['images'=>['page-logo.svg'        =>['colormap'=>true],
									   'actionbox-comment.svg'=>[],
									   'carpool.svg'          =>['level_colors'=>'#808080,transparent',
																 'resize'=>'18x16',
																 'png_fname'=>'actionbox-carpool.png'],
									   'carpool.svg#2'        =>['resize'=>'18x16',
																 'png_fname'=>'actionbox-carpool-dark.png'],
									   'actionbox-email.svg'  =>[],
									   'actionbox-ical.svg'   =>[],
									   'actionbox-share.svg'  =>[],
									   'page-dayrank1.svg'    =>[],
									   'page-dayrank2.svg'    =>[],
									   'page-incomplete.svg'  =>[],
									   'stars-empty.svg'      =>[],
									   'stars-full.svg'       =>[],
									   'pdf-24.png'           =>[],
									   ]],
		  // Mobile frontpage and event pages (all commonly used images in mobile should be here)
		  'mobile.png'=>     ['images'=>['menu32.svg'           =>[],
										 'map-point.svg'    =>['level_colors'=>'#ff766C,transparent','png_fname'=>'map-point-red.png'],
										 'fp-search.svg'    =>['colormap'=>true],
										 'fp-dayrank1.svg'  =>[],
										 'fp-dayrank2.svg'  =>[],
										 'actionbox-comment.svg'=>[],
										 'carpool.svg'          =>['level_colors'=>'#808080,transparent',
																   'resize'=>'18x16',
																   'png_fname'=>'actionbox-carpool.png'],
										 'carpool.svg#2'        =>['resize'=>'18x16',
																   'png_fname'=>'actionbox-carpool-dark.png'],
										 'actionbox-email.svg'  =>[],
										 'actionbox-ical.svg'   =>[],
										 'actionbox-share.svg'  =>[],
										 'page-dayrank1.svg'    =>[],
										 'page-dayrank2.svg'    =>[],
										 'page-incomplete.svg'  =>[],
										 'pdf-24.png'           =>[],
										]],

		  // Rarely used
		  'extra.png'=>    ['images'=>['person.svg'            =>[],
									   'internet.svg'          =>[],
									   'comment.svg'           =>[],
									   'mail.svg'              =>[],
									   'event.svg'             =>[],
									   'personal-calendar.svg' =>[],
									   'opinion.svg'           =>[],
									  ]],
		  'event-admin.png'=>
		                   ['images'=>[
									   'refresh.svg'          =>['level_colors'=>'#707070,transparent',
																'png_fname'=>'refresh-grey.png'],
									   'attach.svg'           =>['level_colors'=>'#707070,transparent',
																'png_fname'=>'attach-grey.png'],
									   'attachment.svg'       =>['level_colors'=>'#707070,transparent',
																'png_fname'=>'attachment-grey.png'],
									   'office.svg'           =>['resize'=>'24x24',
																'png_fname'=>'office-small.png'],
									   'image-icon.svg'       =>['level_colors'=>'#707070,transparent',
																'png_fname'=>'image-icon.png'],
									   'reply.svg'            =>['level_colors'=>'#ffffff,transparent',
																'png_fname'=>'reply-white.png'],
									   'close.svg'            =>['level_colors'=>'#707070,transparent',
																'png_fname'=>'close-grey.png'],
									   'trash.svg'            =>['level_colors'=>'#707070,transparent',
																'png_fname'=>'trash-grey.png'],
									   'trash.svg#2'          =>['level_colors'=>'#d14836,transparent',
																'png_fname'=>'trash-red.png'],
									   'lines-3.svg'          =>['level_colors'=>'#707070,transparent',
																'png_fname'=>'lines-3-grey.png'],
									   'lines-5.svg'          =>['level_colors'=>'#707070,transparent',
																'png_fname'=>'lines-5-grey.png'],
									   'star.svg'             =>['level_colors'=>'#707070,transparent',
																'png_fname'=>'star.png'],
									   'star.svg#2'           =>['level_colors'=>'#d14836,transparent',
																'png_fname'=>'star-red.png'],
									   'star-border.svg'      =>['level_colors'=>'#707070,transparent',
																'png_fname'=>'star-border.png'],
									   'star-border.svg#2'    =>['level_colors'=>'#d14836,transparent',
																'png_fname'=>'star-border-red.png'],
									   'sort.svg'             =>['level_colors'=>'#707070,transparent',
																'png_fname'=>'sort-grey.png'],
									   'corner.svg'           =>['level_colors'=>'#f6f0f0,transparent',
																'png_fname'=>'corner-rose.png'],
									   'corner.svg#2'         =>['level_colors'=>'#f6f0f0,transparent','flop'=>true,
																'png_fname'=>'corner-rose-flop.png'],
									   'external-link.svg'    =>['level_colors'=>'#707070,transparent',
																'png_fname'=>'external-link-grey.png'],
									   'download.svg'         =>['level_colors'=>'#707070,transparent',
																'png_fname'=>'download.png'],
									  ]],

		  'htmledit.png'=> ['src_dir'=>'demosphere/htmledit/images',
							'images'=>['button-bighr.png'          =>[],
									   'button-smallhr.png'        =>[],
									   'button-downcase.svg'       =>[],
									   'button-upcase.svg'         =>[],
									   'button-tidy.png'           =>[],
									   'button-linecleanup.svg'    =>[],
									   'button-linewrapcleanup.svg'=>[],
									   'button-listcleanup.svg'    =>[],
									   'button-imageright.svg'     =>[],
									   'button-paragraphright.svg' =>[],
									   'button-dtoken.png'         =>[],
									   'rightleft.png'             =>[],
									   'delete.png'                =>[],
									   'edit.png'                  =>[],
									   'left-arrow.svg'            =>[],
									   'right-arrow.svg'           =>[],
									   'short-down-arrow.svg'      =>[],
									   ]]
		  ],
		 'setup'=>function(){require_once 'demosphere-misc.php';demosphere_sprite_setup_extra();},
	];
}

// ************************************************************
// 
// ************************************************************

//! The main function where all (non-cached) http requests are processed.
function demosphere_exec_request($requestPath)
{
	global $base_url,$demosphere_config;
	require_once 'dlib/template.php';

	// Debug slow queries
	if($demosphere_config['log_slow_queries']!==false){$slowQueryStart=microtime(true);}

	$fullUrl=dlib_current_url();
	$now=time();

	// **** check for path alias and redirect if necessary

	require_once 'demosphere-path-alias.php';
	$srcPath=demosphere_path_alias_src($requestPath);
	// If current path is a src path (ex: post/123), redirect to alias instead (ex: contact-page)
	$goodPath=demosphere_path_alias_dest($srcPath);
	if($goodPath!==$requestPath){dlib_redirect($base_url.'/'.$goodPath);}

	// **** build list of all paths

	require_once 'demosphere-paths.php';
	list($paths,$options)=demosphere_paths($srcPath);

	// **** 
	$options['custom_output']=true;
	// Special case: Pages with output=false directly echo their own content. 
	// We need to send headers before calling them. 
	$options['before_function_hook']=function($desc) use($srcPath,$now)
		{
			if(val($desc,'output',true)===false){demosphere_uncached_page_headers($now);}
			demosphere_setup_page_body_class($desc);
		};

	// **** Execute function that generates output for this path

	require_once 'dlib/dlib-paths.php';
	list($out,$headers,$pathDesc)=dlib_paths_exec($srcPath,$paths,$options);
	if($demosphere_config['log_slow_queries']!==false){demosphere_exec_request_log_slow_queries($requestPath,$slowQueryStart,microtime(true));}
	if(val($pathDesc,'output',true)===false){return;}

	// **** post process: save page to cache if necessary, and send http headers
	
	if(demosphere_page_cache_is_cacheable($srcPath,$pathDesc))
	{
		demosphere_page_cache_save($fullUrl,$out,$headers,$now);
		demosphere_page_cache_headers($now);
	}
	else
	{
		demosphere_uncached_page_headers($now);
	}
	foreach($headers as $name=>$val){header($name.': '.$val);}
	// FIXME consider X-Frame-Options: SAMEORIGIN  for security

	// **** In most (uncached) cases, display is actually done here !! 
	echo $out;
}

//! Log slow http and sql queries to files/private/slow.log
//! This is controlled by $demosphere_config['log_slow_queries'] 
function demosphere_exec_request_log_slow_queries($requestPath,$start,$end)
{
	global $demosphere_config,$dbSlowQueries;
	$duration=$end-$start;
	if($duration<$demosphere_config['log_slow_queries'] ||
	   strpos($requestPath,'cron')!==false ||
	   strpos($requestPath,'feed-import')!==false ||
	   strpos($requestPath,'async-get-url-types')!==false ||
	   strpos($requestPath,'feed-import')!==false ||
	   strpos($requestPath,'mail-import')!==false   )
	{
		return;
	}

	// Log the http query
	$totSql=array_sum(dlib_array_column($dbSlowQueries,'duration'));
	file_put_contents('files/private/slow.log',
					  $_SERVER['REMOTE_ADDR'].' '.
					  date('r').' '.
					  round(1000*$duration,1).'ms '.
					  count($dbSlowQueries).' '.
					  round(1000*$totSql,1).' '.
					  round(100*$totSql/$duration).'% '.
					  $requestPath.
					  "\n",FILE_APPEND);
	// Also log slow SQL queries
	foreach($dbSlowQueries as $q)
	{
		if($q['duration']*1000<2){continue;}// ignore queries < 2ms
		file_put_contents('files/private/slow.log',
						  ' sqlq: '.round(1000*$q['duration'],1).' '.$q['query']
						  ."\n",FILE_APPEND);
	}
}

function demosphere_uncached_page_headers($now)
{
	global $user,$demosphere_config,$base_url;
	// default headers for non cached pages 
	// Note: on normal PHP pages, these headers are automatically added by PHP when session_start is called.
	// (See  php: session_cache_limiter())
	// However, we need custom headers for cached pages. 
	// So we have set session.cache_limiter to none, and add the headers manually.
	header('Expires: Thu, 19 Nov 1981 08:52:00 GMT');
	header('Cache-Control: no-cache, must-revalidate, post-check=0, pre-check=0');
	// These two are necessary for complex reasons (see long explanation in Drupal7: drupal_page_header())
	// (Firefox bug(?) when behind proxy server)
	header('Last-Modified: '.gmdate(DATE_RFC1123, $now));
	header('ETag: "'.$now.'"');	

	// We do this in PHP instead of apache conf to avoid bloating every request with uneeded headers.
	if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=="on")
	{
		header("Strict-Transport-Security: max-age=63072000; includeSubdomains;");
	}

	// Privacy : hide referrer for admins and moderators and for safe domain
	if($user->checkRoles('admin','moderator') ||
	   $base_url===$demosphere_config['safe_base_url'])
	{
		// This is not vital and blocking here would be annoying while debugging 
		//if(headers_sent()){fatal('Headers already sent, so we can\'t add Content-Security-Policy. Aborting.');}
		// This is deprecated on Firefox, but "Referrer-Policy:" is not yet supported on other browsers.
		// header('Content-Security-Policy: referrer no-referrer');
		// New version (supported Firefox 50+, Chrome 56+)
		header('Referrer-Policy: no-referrer');

	}
}

//! Adds a bodyClass computed from the current path.
//! This makes it simpler to write CSS selectors that are specific to a given page.
function demosphere_setup_page_body_class($path)
{
	global $currentPage;
	$label=false;
	if(!isset($currentPage)){return;}
	if(isset($path['output'])){return;}

	// Determine label from function name, path, or path regexp (in that order)
	if(is_string($path['function'])){$label=$path['function'];}
	else
	if(substr($path['path'],0,1)!=='@'){$label=$path['path'];}
	else
	if(preg_match('#^@\^(.*)\$@$#',$path['path'],$m))
	{
		$label=$m[1];
		$label=str_replace(['([0-9]+)','(\d+)'],['n','n'],$label);
	}
	if($label===false){return;}
	$label=preg_replace('@[^a-z0-9_-]@i','_',$label);
	$label=preg_replace('@^[_0-9-]+@','',$label);
	$label='path_'.$label;
	$currentPage->bodyClasses[]=$label;
}

function demosphere_class_autoloader($class) 
{
	static $classes=[
		'City'=>'demosphere',
		'Event'=>'demosphere',
		'Place'=>'demosphere',
		'Post'=>'demosphere',
		'Topic'=>'demosphere',
		'Term'=>'demosphere',
		'User'=>'demosphere',
		'Carpool'=>'demosphere',
		'Opinion'=>'demosphere',
		'RepetitionGroup'=>'demosphere',
		'Article'=>'demosphere/feed-import',
		'FakeFeed'=>'demosphere/feed-import',
		'Feed'=>'demosphere/feed-import',
		'MailRule'=>'demosphere/mail-import',
		'Message'=>'demosphere/mail-import',
		'MimeMessage'=>'demosphere/mail-import',
		'Page'=>'demosphere/page-watch',
		'TMDocument'=>'demosphere/text-matching',
		'DBObject'=>'dlib',
		'Comment'=>'dlib/comments',
		'IpWhois'=>'dlib/comments',
		'Ban'=>'dlib/comments',
		'HtmlPageData'=>'dlib',
		'Pager'=>'dlib',
		'Html2Text'=>'lib',
					];
	
	if(!isset($classes[$class])){return;}
    require_once $classes[$class].'/'.$class.'.php';
}

//! Customize dlib_debug_backtrace()
function demosphere_backtrace_file($file,$line,$outputType,$onlyUrl=false)
{
	$pwd=getcwd();
	switch($outputType)
	{
	case 'html':
		$url='http://localhost/debug.php?file='.urlencode($file).'&line='.urlencode($line);
		if($onlyUrl){return $url;}
		$shortFile=$file;
		if(strpos($file,$pwd.'/')===0){$shortFile='::'.substr($file,strlen($pwd)+1);}
		return '<a href="'.ent($url).'">'.ent($shortFile).'</a>';
	case 'term':
		return 'http://e/'.preg_replace('@^/home/[^/]+@','~',$file);
	case 'text':
		return $file;
	}
}

?>