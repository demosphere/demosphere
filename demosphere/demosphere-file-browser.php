<?php
// ************************************************************
// Image and css override manager
// ************************************************************

//! A file browser: displays files in a directory and allows user to upload, edit and delete files.
//! A file browser is identified by a $cfgName (example: "css" our "images"). 
//! It is configured in $demosphere_config['file_browser'][$cfgName].
//! Main config items : 
//! - filter : a regexp of files of interest
//! - editable : a regexp (or false) of files that can be edited online (text files, such as CSS).
function demosphere_file_browser($cfgName)
{
	global $base_url,$currentPage,$demosphere_config;
	if(!isset($demosphere_config['file_browser'][$cfgName])){dlib_not_found_404();}
	extract($demosphere_config['file_browser'][$cfgName]);

	$url=$base_url.'/browse/'.$cfgName;
	$ct=0;
	$list='<table id="file-list">'."\n";
	$scanRes=scandir($dir);
	foreach($scanRes as $fname)
	{
		if(!preg_match($filter,$fname)){continue;}
		$fileUrl=$base_url.'/'.$dir.'/'.$fname;
		$list.='<tr>';
		$list.='<td class="edit">';
		if($editable!==false && preg_match($editable,$fname))
		{$list.='<a href="'.$url.'/edit?file='.urlencode($fname).'"><img alt="edit" src="'.$base_url.'/dlib/edit.png"/></a></td>';}
		$list.='</td>';
		$list.='<td class="delete"><a href="'.$url.'/delete?file='.urlencode($fname).'"><img alt="del" src="'.$base_url.'/dlib/delete.png"/></a></td>';
		$list.='<td class="fname" ><a href="'.ent($fileUrl).'">'.ent($fname).'</a></td>';
		$list.='<td class="preview">';
		if(preg_match('@\.(png|gif|svg|jpg|jpeg|ico)$@',$fname))
		{$list.='<a href="'.ent($fileUrl).'"><img src="'.ent($fileUrl).'?'.filemtime($dir.'/'.$fname).'"/></a>';}
		$list.='</td>';
		$list.='</tr>'."\n";
		$ct++;
	}
	$list.='</table>';
	if($ct===0){$list='<p>'.t('[ Empty: no files ]').'</p>';}

	$items=[];

	$items['title']=['html'=>'<h2>'.ent('Files:').'</h2>'];
	$items['list']=['html'=>$list];
	$items['upload']=
		[
		 'type'=>'file',
		 'title'=>t('Upload file'),
		 'attributes'=>['accept'=>$accept,'onchange'=>'document.getElementById("edit-ok").click();'],
		 'validate'=>function()use($filter)
			 {
				 $fname=$_FILES['upload']['name'];
				 if(!preg_match($filter,$fname)){return ent(t('Invalid file name: ').$fname);}
			 },
		 'pre-submit'=>function()use($filter,$dir)
			 {
				 move_uploaded_file($_FILES['upload']['tmp_name'],$dir.'/'.basename($_FILES['upload']['name']));
			 },
		 ];

	if($editable!==false)
	{
		$items['add']=
			[
			 'type'=>'textfield',
			 'title'=>t('Add new empty file'),
			 'validate'=>function($v)use($filter,$editable)
				 {
					 if(!preg_match($filter,$v) || !preg_match($editable,$v))
					 {
						 return ent(t('Invalid file name: ').$v);
					 }
				 },
			 'pre-submit'=>function($v)use($filter,$dir)
				 {
					 touch($dir.'/'.$v);
				 },
			 ];
	}

	$items['ok']=
		[
		 'type'=>'submit',
		 'value'=>t('Submit'),
		 ];

	if(isset($form_alter)){$form_alter($items);}
	
	$currentPage->addCssTpl('demosphere/css/demosphere-file-browser.tpl.css');
	require_once 'dlib/form.php';
	return form_process($items,['id'=>'file-browser','attributes'=>['class'=>['browse-'.$cfgName]]]);
}

function demosphere_file_browser_delete($cfgName)
{
	global $base_url,$currentPage,$demosphere_config;
	if(!isset($demosphere_config['file_browser'][$cfgName])){dlib_not_found_404();}
	extract($demosphere_config['file_browser'][$cfgName]);

	$file=$_GET['file'];
	if(!preg_match($filter,$file)){dlib_bad_request_400('invalid filename');}
	if(db_result("SELECT COUNT(*) FROM Topic WHERE icon='%s'",$dir.'/'.$file)>0)
	{
		return '<p>'.t('This image cannot be deleted, it is used as an icon for a topic.').'</p>';
	}

	$items['confirm']=['html'=>'<p>'.ent(t('Are you sure you want to delete this file: ').$file).'</p>'];

	$items['ok']=
		[
		 'type'=>'submit',
		 'value'=>t('Ok'),
		 'submit'=>function()use($dir,$file)
			 {
				 if(unlink($dir.'/'.$file)){dlib_message_add('File successfully deleted.');}
				 else{dlib_message_add('File delete failed','error');}
			 }
		 ];

	$items['cancel']=
		[
		 'type'=>'submit',
		 'value'=>t('Cancel'),
		 ];

	require_once 'dlib/form.php';
	return form_process($items,['redirect'=>'browse/'.$cfgName]);
}


function demosphere_file_browser_edit($cfgName)
{
	global $base_url,$currentPage,$demosphere_config;
	if(!isset($demosphere_config['file_browser'][$cfgName])){dlib_not_found_404();}
	extract($demosphere_config['file_browser'][$cfgName]);

	$file=$_GET['file'];
	if($editable===false || !preg_match($editable,$file) || !preg_match($filter,$file)){dlib_bad_request_400('invalid filename');}

	$items['confirm']=['html'=>'<p>'.ent(t('Editing file: ').$file).'</p>'];

	$items['contents']=['type'=>'textarea',
					   'default-value'=>file_get_contents($dir.'/'.$file),
					   ];

	$items['save']=
		[
		 'type'=>'submit',
		 'value'=>t('Save'),
		 'submit'=>function($items)use($dir,$file)
			 {
				 if(file_put_contents($dir.'/'.$file,$items['contents']['value'])!==false)
				 {dlib_message_add('File successfully updated.');}
				 else{dlib_message_add('File update failed','error');}
			 }
		 ];

	$items['cancel']=
		[
		 'type'=>'submit',
		 'value'=>t('Cancel'),
		 ];

	require_once 'dlib/form.php';
	return form_process($items,['redirect'=>'browse/'.$cfgName]);
}

//! Customization of file browser form for CSS files
function demosphere_file_browser_css(&$items)
{
	global $base_url;
	$items['title']['html']='<h2>'.t('CSS files to override').'</h2>'.
		'<p>'.t('You can add, upload and edit CSS files. Use the same name as an exisiting CSS file (example: demosphere-frontpage.css, demosphere-page.css, demosphere-common.css...). Both the standard Demosphere file and your file will be used. You should !link disable CSS compression</a> while working here.',['!link'=>'<a href="'.$base_url.'/configure/misc-config#compress_css">']).'</p>';
}

//! Customization of file browser form for image files
function demosphere_file_browser_images(&$items)
{
	global $base_url;
	$items['title']['html']='<h2>'.t('Images to override').'</h2>'.
		'<p class="description">'.t('You can upload image files (svg, png,...). Use the same name as an exisiting image (example: fp-logo.svg, fp-logo.png, page-logo.svg, page-logo.png ... see list below). In many cases you will need to !link clear all caches</a> after adding an image. This will rebuild sprite images ... and can be long (30 seconds). You might also need to refresh/reload the page on your browser.',['!link'=>'<a href="'.$base_url.'/cache-clear-slow">']).'</p>';
	$out='';
	$out.='<hr/>';
	$out.='<p class="description">'.t('Examples of images:<br/>Note: this list is just a hint, some images are missing, might not actually be used, or might not be overrideable.').'</p>'; 
	$out.='<p class="description">';
	$imgDirs=['demosphere/css/images','demosphere/htmledit/images','demosphere/dcomponent'];
	foreach($imgDirs as $imgDir)
	{
		$scanRes=scandir($imgDir);
		foreach($scanRes as $fname)
		{
			if(!preg_match('@\.(svg|png|gif|jpg|jpeg)$@',$fname)){continue;}
			$out.='<a href="'.ent($base_url.'/'.$imgDir.'/'.$fname).'">'.ent($fname).'</a>, ';
		}
	}
	$out.='</p>';
	$out.='<p class="description">'.t('Examples use: 1) download fp-logo.svg by clicking on its link in the previous list and saving the image 2) edit it with inkscape. 3) upload it here 4) clear all caches 5) view new logo on front page').'</p>'; 
	
	$items['file-list']=['html'=>$out];
}

?>