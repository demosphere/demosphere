<?php

function demosphere_place_form_access($cuser,$placeId)
{
	if($cuser->checkRoles('admin','moderator')){return true;}
	if($cuser->id==0){return false;}
	return db_result('SELECT COUNT(*) FROM Place WHERE id=%d AND inChargeId=%d',$placeId,$cuser->id)>0;
}

//! Form for editing a Place 
//! (not inside the event form).
function demosphere_place_form($place)
{
	global $demosphere_config,$base_url,$currentPage,$user;
	require_once 'demosphere-place.php';

	$place=Place::fetch($place,false);
	if($place===null){dlib_not_found_404();}
	if($place->id===1){dlib_bad_request_400("You should not edit place id=1, it is reserved as an empty place");}
	demosphere_place_tabs($place,'edit');
	$currentPage->title=t('Edit: ').$place->nameOrTitle();

	if($user->id===0){fatal('demosphere_place_form: user id=0. This should not happen.');}

	$isReference=$place->id===$place->referenceId;

	// If this place is being edited by a non admin that is in charge of this place
	$isInCharge=!$user->checkRoles('admin','moderator');
	if($isInCharge && $user->id!=$place->inChargeId){fatal('demosphere_place_form: strange isInCharge.');}

	list($form,$formOpts,$objForm)=demosphere_place_form_common($place);
	$form0=$form;

	if(!$isReference)
	{
		$form=[
			      'is-variant'=>['html'=>'<div id="is-variant">'.ent(t("This place is only a variant of place:")).
				                           '<a href="'.ent($place->useReference()->url()).'">'.ent($place->referenceId).'</a></div>']
			  ]+$form;
	}

	$form['city']['required']=true;

	if($isReference || $place->title!='')
	{
		$form['title']=$objForm['title'];
		$form['title']['title']=t('Title');
		$form['title']['description']=t('A title (name) for this place. If you leave this empty, the first line of the address is used.');
		if(!$isReference)
		{
			$form['title']['description'].=
				'<div class="error">'.ent(t('This place has a title but is only a variant. You should erase the title!')).'</div>';
		}
		$form['title']['validate']=function($v)
			{
				require_once 'dlib/html-tools.php';
				if(dlib_html_estimate_display_width($v)>511){return t('the title is too long');}
			};
		$form['title']['pre-submit']=function(&$v){$v=Place::cleanupTitle($v);};
	}

	if($isReference || $place->description!='')
	{
		require_once 'htmledit/demosphere-htmledit.php';
		$form['description']=$objForm['description'];
		$form['description']['title']=t('Description');
		if(!$isReference)
		{
			$form['description']['field-prefix']=
				'<div class="error">'.ent(t('This place has a description but is only a variant. You should erase the description!')).'</div>';
		}
		$form['description']['suffix']=demosphere_htmledit_js_config('edit-description');
		require_once 'dlib/filter-xss.php';
		$form['description']['default-value']=filter_xss_admin($form['description']['default-value'] ?? '');
		$form['description']['pre-submit']=function(&$v)
			{
				if($v!=='')
				{
					require_once 'htmledit/demosphere-htmledit.php';
					$v=demosphere_htmledit_submit_cleanup($v);
				}
			};
	}

	unset($form['referenceId']);

	if($user->checkRoles('admin','moderator'))
	{
		if($place->referenceId==$place->id)
		{
			$form['inChargeId']=$objForm['inChargeId'];
			$form['inChargeId']['type']='textfield';
			$form['inChargeId']['title']=t('Person in charge');
			$form['inChargeId']['description']=t('An ordinary user (not a moderator) that will be able to edit this place\'s title, description and edit all of it\'s events.');
			$form['inChargeId']['default-value']=$place->inChargeId==0 ? '' : User::fetch($place->inChargeId)->login;
			$form['inChargeId']['validate'  ]=function($v){return $v!='' && User::fetchByLogin($v,false)===null ? t('Invalid user name') : true;};
			$form['inChargeId']['pre-submit']=function(&$v){$v=$v=='' ? 0 : User::fetchByLogin($v)->id;};
		}

		if($place->inChargeChanged && $place->inChargeId)
		{
			require_once 'demosphere-date-time.php';
			$form['inChargeChangedReset']=
				['type'=>'checkbox',
				 'title'=>t('changes are ok'),
				 'description'=>t('This place was edited by the user in charge of it on @date. You should check this box once you have made sure those edits are ok.',
								  ['@date'=>demos_format_date('full-date-time',$place->inChargeChanged)]),
				];
		}

		$form['more-options'] = 
			['type' => 'fieldset',
			 'title' => t('more options'),
			 'collapsible' => true,
			 'collapsed' => true,
			];

		$form['referenceId']=$form0['referenceId'];
		$form['referenceId']['description']=t('If this place is *not* a variant of another place, then referenceId should be equals to place id. If this place *is* a variant of another place, then referenceId is the id of that place.');
		$form['referenceId']['validate']=function($v)use($place)
			{
				$r=Place::fetch($v,false);
				return $r!==null && ($r->referenceId===$r->id || $v==$place->id);
			};

		$form['hideFromAltSuggestion']=
			['type' => 'checkbox',
			 'title' => t('Hide from variants suggestion'),
			 'description'=>t('If you choose this, this place can still be a variant of another place, but it will not show up in the suggestions list.'),
			 'default-value'=>$place->id==0 ? false: $place->hideFromAltSuggestion,
			];

		$form['more-options-end'] = ['type' => 'fieldset-end',];
	}

	unset($form['place-fields-end']);
	$form['place-fields-end']=$form0['place-fields-end'];

	$form['save']=$objForm['save'];
	$form['save']['pre-dbobject-save']=function($place,$items)use($user,$isInCharge)
		{
			$values=dlib_array_column($items,'value');
			if($values['inChargeChangedReset'] ?? false){$place->inChargeChanged=0;}
			$place->setCity(City::findOrCreate($values['city']));
			if($user->checkRoles('admin','moderator') && $values['city-short']!=='')
			{
				$place->useCity()->shortName=$values['city-short'];
			}
			if($isInCharge){$place->inChargeChanged=time();}
			// If inCharge user changes, then reset last changed time to 0 
			if($user->checkRoles('admin','moderator') && $place->inChargeChanged!=0)
			{
				if($place->inChargeId==0 || $place->useInCharge()->login!=$items['inChargeId']['default-value']){$place->inChargeChanged=0;}
			}
		};
	$form['save']['redirect']=$place->url();


	unset($form['place-end']);
	$form['place-end'       ]=$form0['place-end'       ];

	return form_process($form,$formOpts);
}

//! Form for editing the place fields inside an Event.
//! $place is the initial place. The user can then select another place, erase it, change it ... 
function demosphere_place_form_event($place,$eventId,$isSelfEdit)
{
	global $base_url,$currentPage,$user,$demosphere_config;

	require_once 'demosphere-common.php';

	$isInCharge=!$user->checkRoles('admin','moderator') && $user->id!=0 && $place->useReference()->inChargeId==$user->id;

	// replace empty place (id=1) with new place
	if($place->id===1){$place=new Place();}

	$placeLink   ='<a class="place-link"     target="_blank" href="'.$base_url.'/'.$demosphere_config['place_url_name'].'/'.'"></a>';
	$placeRefLink='<a class="place-ref-link" target="_blank" href="'.$base_url.'/'.$demosphere_config['place_url_name'].'/'.'"></a>';

	$isUsedElsewhere=$place->isUsedElsewhere($eventId);
	$isReference=$place->id===$place->referenceId;

	list($form,$formOpts)=demosphere_place_form_common($place);

	$variantsSuggest='';

	$formTop=[];

	$formTop['place-start']= ['html'=>'<div id="place" class="'.($isInCharge && !$isSelfEdit ? 'only-variants' : '').'">'];
	unset($form['place-start']);

	$formTop['place-head']= 
		['html'=>
		 '<div id="place-head">'.
		 '<div id="place-head-first-line">'.
		 '<h3 id="place-head-label">'.t('Place:').'</h3>'.
		 '<button type="button" id="place-suggest">'.
		 '<img src="'.$base_url.'/'.demosphere_cacheable('demosphere/css/images/search2.png').'" alt=""/>'.
		 t('suggest').'</button>'.
		 '<div id="place-suggestions-wrapper"></div>'];

	$formTop['variants-selector']=['html'=>'<div id="variants-selector-wrapper"></div>'];

	$formTop['place-head-first-line-end' ]=['html'=>'</div>'];

	$placeChangeTypes=['none'=>'',
					   'change-only-here'=>t('small changes (variant)'),
					   'change-everywhere'=>t('change everywhere'),
					  ];
	if(!$user->checkRoles('admin','moderator'))
	{
		unset($placeChangeTypes['change-everywhere']);
		$placeChangeTypes['change-only-here']=t('small changes');
	}

	$formTop['place_change_type'] =
		[
			'title' => t("How to edit place"), 
			'type' => 'radios',
			'options' => $placeChangeTypes,
			'default-value' => ($isUsedElsewhere ||
								// direct edit of reference place
								($place->id!=0 && $isReference)
								? 'none' : 'change-only-here'),
			'attributes'=>['class'=>['place-change-type']],
			'field-suffix'=>
			'<button type="button" class="place-erase">'.
			'<img src="'.$base_url.'/'.demosphere_cacheable('demosphere/css/images/delete.png').'" alt=""/>'.
			t('erase').'</button>',
		];
	// This is hidden. It is changed by js when user clicks on a change place_change_type
	$formTop['place_is_readonly'] =
		['title'=>'is readonly',
		 'type'=>'checkbox',
		 'default-value'=>$isUsedElsewhere,
		];
	// This is hidden. It is only changed when user selects a different place (or erases).
	// We need in a form item (and not in js var) for state consistency (browser back/forward sequence)
	$formTop['place_is_used_elsewhere'] =
		['title'=>'is_used_elsewhere',
		 'type'=>'checkbox',
		 'default-value'=>$isUsedElsewhere,
		];
	// Same as place_is_used_elsewhere
	$formTop['place_in_charge_email'] =
		['title'=>'in_charge_email',
		 'type'=>'hidden',
		 'default-value'=>$place->inChargeId ? $place->useInCharge()->email : '',
		];

	$formTop['place-head-end']=['html'=>'</div><!-- end place-head -->'];

	$formTop['place-fields']=$form['place-fields'];
	unset($form['place-fields']);

	$formTop['place-erase-and-labels']=['html'=>
		 '<input type="image" class="place-erase" src="'.$base_url.'/demosphere/css/images/delete.png" alt="new place"/>'.
		 '<span id="place-label">'.
		 '<span id="place-label-x-x">'.t('New place').'</span>'.
		 '<span id="place-label-p-r">'.t('Place !pid (variant of !ref)',['!pid'=>$placeLink,'!ref'=>$placeRefLink]).'</span>'.
		 '<span id="place-label-x-r">'.t('New place (variant of !ref)', ['!ref'=>$placeRefLink]).'</span>'.
		 '<span id="place-label-p-x">'.t('Place !pid',['!pid'=>$placeLink]).'</span>'.
		 '</span>',
		];

	$formTop['event_id'] = 
		['type'=>'data',
		 'data'=>$eventId,];

	$formTop['placeId'] =
		['title' => 'place-id', 
		 'type' =>  'int',
		 'required' => false,
		 'default-value' => $place->id,
		];

	$mapButton= 
		['html'=>
		 '<div id="map-button-wrapper">'.
		 '<div id="map-button">'.t('Map').' '.
		 '<span id="map-ok">✓</span>'.
		 '<span id="map-notok">x</span>'.''.
		 '</div>'.
		 '</div>'
		];

	$form=dlib_array_insert_assoc($form,'map-fields-start','map-button',$mapButton);

	// rebuild form array in good order
	$form=array_merge($formTop,$form);


	$form['referenceId']['validate']=function($v,$item)use($isInCharge,$isSelfEdit)
		{
			if($isInCharge && !$isSelfEdit)
			{
				if($v!=$item['default-value'])
				{
					$message='Bug: attempt to change place referenceId during inCharge place edit.';
					dlib_log_error($message);
					return $message;
				}
 			}
		};

	// Special (unimportant) case : add alt suggestions for places that have alternatives.
	// (normally suggestions and alt suggestions are built by client through ajax)
	// Using this callback is necessary, since actual values (ex: place) are not known during form build (on validation error).
	$formOpts=['pre-render'=>function(&$items,$formOpts,$isValidationErr)use($eventId)
		{
			global $currentPage,$user;
			$placeId=$isValidationErr ? val($items['placeId'],'value'        ) : 
			                            val($items['placeId'],'default-value');
			$place=Place::fetch($placeId,false);

			if($place!==null && $place->id!==0)
			{
				$nbAlt=db_result('SELECT COUNT(*) FROM Place WHERE referenceId=%d AND id!=%d',$place->referenceId,$place->id);
				if($nbAlt>0)
				{
					require_once 'demosphere-place-search.php';
					$variantsSuggest=demosphere_place_search_variants_selector($place->referenceId);
					$items['variants-selector']=['html'=>'<div id="variants-selector-wrapper">'.$variantsSuggest.'</div>'];	
				}
			}
		}];

	return [$form,$formOpts];
}

//! Returns an incomplete form with fields that are common to demosphere_place_form() and demosphere_place_form_event().
function demosphere_place_form_common($place)
{
	global $demosphere_config,$currentPage,$user;

	// *** js + css
	$currentPage->addJs('lib/jquery.js');
	$currentPage->addJs('lib/jquery-browser.js');
	$currentPage->addJs('lib/jquery-ui-1.12.1.custom/jquery-ui.js');
	$currentPage->addCss('lib/jquery-ui-1.12.1.custom/jquery-ui.min.css');
	$currentPage->addCssTpl('demosphere/css/demosphere-place-form.tpl.css');
	$currentPage->addJs('dlib/merge-calls.js');
	$currentPage->addJs('demosphere/js/demosphere-place-form.js');
	$currentPage->addJs('demosphere/js/demosphere-misc.js');
	require_once 'demosphere/demosphere-event-map.php';
	$currentPage->addJsVar('demosphere_google_map_api_url',demosphere_google_map_api_url());
	$currentPage->addJsConfig('demosphere',
							  ['event_map_width','event_map_height','country_map_url','city_max_width',
							   'map_center_latitude','map_center_longitude','map_zoom','place_url_name']);
	$currentPage->addJsTranslations(
		[
			'variants-search'=>t('search'),
		]);


	// Automatic form created from $place object
	require_once 'dlib/form.php';
	list($objForm,$formOpts)=form_dbobject($place);

	$form=[];
	$form['place-start']= ['html'=>'<div id="place">'];

	$form['place-fields']=
		['html'=>
		 '<div id="place-fields">'.
		 '<div id="place-non-map-fields">'];

	// Pulldown menu with common cities. 
	$tmp='<select id="citySelect">';
	$tmp.='<option>---</option>';
	$tmp.='<option>';
	$tmp.=implode('</option><option>',$demosphere_config['common_cities']);
	$tmp.='</option>';
	$tmp.='</select>';
	$tmp.='<span id="city-too-long-warning">'.t('City too long !').'</span>';

	$form['referenceId']=$objForm['referenceId'];
	$form['referenceId']['title'] = 'reference-id';
	$form['referenceId']['required'] = false;

	$form['city']=
		['type'          => 'textfield',
		 'title'         => t('City'),
		 'field-prefix'  => '<span id="city-field-prefix">',
		 'field-suffix'  => $tmp.'</span>',
		 'default-value' => $place->getCityName(true),
		 'attributes'=>['maxlength'     => 1024,],
		];

	
	$short=db_result_check('SELECT shortName FROM City WHERE id=%d',$place->cityId);
	$form['city-short']=['type'=>'textfield',
						 'attributes'=>['size'=>14,'maxlength'=>14],
						 'default-value'=>$short===false ? '' : $short,
						 'title'=>t('short name')];

	$extraLinks='<ul id="map-links">';

	foreach($demosphere_config['extra_map_links'] as $map)
	{
		$extraLinks.='<li><a target="_blank" href="'.$map['url'].'">'.$map['name'].'</a></li>';
	}
	$extraLinks.='</ul>';

	$form['address']=$objForm['address']; 
	$form['address']['type'  ]= 'textarea'; 
	$form['address']['title' ]= t("Address"); 
	$form['address']['attributes'  ]=['rows'=>5,'cols'=>50];
	$form['address']['prefix']=$extraLinks;
	$form['address']['pre-submit']=function(&$v){$v=Place::cleanupAddress($v);};

	$form['place-non-map-fields-end']=['html'=>'</div><!-- place-non-map-fields end -->'];

	$form['map-fields-start'] = ['html'=> '<div id="map-fields">',];
	$form['map-coords-start']=['html'=>'<div id="map-coords">'];

	$form['latitude']=$objForm['latitude']; 
	$form['latitude']['title' ]= t("latitude"); 
	$form['latitude']['attributes']=['size'=> 12,
									 'maxlength'=> 500];
	$form['latitude']['required' ]= false;

	$form['longitude']=$objForm['longitude'];
	$form['longitude']['title' ]= t("longitude"); 
	$form['longitude']['attributes']=['size'=> 12,
									  'maxlength'=> 500];
	$form['longitude']['required' ]= false;

	$form['zoom']=$objForm['zoom'];
	$form['zoom']['title' ]= t("zoom"); 
	$form['zoom']['attributes' ]= ['size'=> 5];
	$form['zoom']['required'  ]= false;
	$form['zoom']['pre-submit']= function(&$v,$u1,$u2,$items)
		{
			// disable map is stored as zoom=0
			if(!$items['enable-map']['value']){$v=0;} 
		};
	
	$form['enable-map'] =
		[
			'title' => t("enable map"), 
			'size' => 5,
			'maxlength' => 500,
			'type' => 'checkbox',
			'default-value' => $place->id!=0 && is_numeric($place->zoom) && $place->zoom>0,
			'suffix'=>'<a id="map-open-in-gmap" target="_blank"  href="#">'.t('open in google map').'</a>',
		];			 
	$form['map-coords-end']=['html'=>'</div><!-- end map-coords -->'];

	$form['map-image-info']=
		['html'=>'<div id="map-block">'.
		 '<div id="map-help">'.t('<p>Enter an address in the search box and press the search button. The address must be in the following format (notice the comma): 123 example avenue, Example-city</p>').'</div>'.
		 '<div id="map-search-result-text" style="display:none"><h4>'.t('search result').
		 ':</h4><p></p></div>'.
		 '<div id="map-search"><input type="text" /><button type="button">'.t('search').'</button></div>'.
		 '<div id="map">map</div></div>',
		];

	$form['map-fields-end'  ]=['html'=>'</div><!-- end map-fields -->',];
	$form['place-fields-end']=['html'=>'</div><!-- end place-fields -->',];
	$form['place-end'       ]=['html'=>'</div><!-- end place -->',];

	return [$form,$formOpts,$objForm];
}


function demosphere_place_form_event_validate(&$form)
{
	global $user;
	$values=dlib_array_column($form,'value');

	$eventId=$form['event_id']['data'];

	$originalId=$form['placeId']['default-value'];
	$placeId=intval($values['placeId']);

	$place=Place::fetch($placeId,false);

	$isUsedElsewhere=$place!==null  && $place->isUsedElsewhere($eventId);

	// Sanity check: 'change-only-here' implies new place (id=0)
	if($placeId!=0 && 
	   $values['place_change_type']=='change-only-here' &&
	   $isUsedElsewhere)
	{
		$form['place_change_type']['error']='Bug! placeId not 0 for new change-only-here.';
		dlib_log_error($form['place_change_type']['error']);
	}

	// Non admin users are only allowed to change existing places if they are not used elsewhere
	if(!$user->checkRoles('admin','moderator') && 
	   !$values['place_is_readonly'] &&
	   $isUsedElsewhere)
	{
		$form['place_change_type']['error']='Bug! placeId not 0 and place not readonly';
		dlib_log_error($form['place_change_type']['error']);
	}
}

function demosphere_place_form_event_submit(&$form)
{
	global $demosphere_config,$user;
	$values=dlib_array_column($form,'value');

	$eventId=$form['event_id']['data'];

	$placeId=intval($values['placeId']);
	if($placeId==0){$place=new Place();}
	else           {$place=Place::fetch($placeId);}
	
	$changeType=$values['place_change_type'];
	$readOnly  =$values['place_is_readonly'];

	// *** If the readonly option is checked, just return the existing place, with no changes.
	if($readOnly){return $place;}

	$ref=Place::fetch($values['referenceId'],false);

	// local function that fills Place fields from values array
	$copyFields=function($values,&$place)
		{
			unset($values['title']);
			$place->dboCopyFromStringArray($values);
			$place->setCity(City::findOrCreate($values['city']));
		};

	// normal/standard op : copy form fields to current place 
	$copyFields($values,$place);

	// special case: avoid creating new empty places 
	if($place->isEmpty()){return Place::fetch(1);}

	// Set city-short.
	if($user->checkRoles('admin','moderator') && $values['city-short']!=='')
	{
		$place->useCity()->shortName=$values['city-short'];
		$place->useCity()->save();
	}

	// special case : user didn't really change anything:  do nothing 
	if($changeType==='change-only-here' && $ref!==null && $place->equals($ref))
	{
		return $ref;
	}

	// 'change-only-here' and new place : add reference to reference place
	if($placeId===0 && $changeType==='change-only-here' && $ref!==null)
	{
		$place->referenceId=$ref->referenceId;
	}

	// normal/standard op : save the edited place 
	$place->save();

	// 'change-everywhere': Also change reference of current place and save.
	if($changeType==='change-everywhere' && $ref!==null)
	{
		$copyFields($values,$ref);
		$ref->save();
		// $place and $ref are now identical, they can be merged
		// (all events using $place will now use $ref)
		if($place->id!=$ref->id)
		{
			$ref->replace($place);
			$place->delete();
			$place=$ref;
		}
	}

	return $place;
}

//! Ajax call on multiple edit / pending events page
function demosphere_place_in_charge_changed_ok($placeId)
{
	$place=Place::fetch($placeId,false);
	if($place===null){dlib_not_found_404();}
	$place->inChargeChanged=0;
	$place->save();
	return 'ok';
}

?>