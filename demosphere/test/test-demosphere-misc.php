<?php

require_once 'test-demosphere.php';

function test_demosphere_misc()
{
	global $dlib_config,$base_url,$demosphere_config;
	$base_path=preg_replace('@^https?://[^/]*@','',$base_url).'/';
	$base_root=dlib_host_url($base_url);

	require_once 'demosphere-misc.php';

	test_equals(demosphere_misc_text_urls('<p>abc www.xyz.st/abc/def. zzz</p>'),
				'<p>abc <a href="http://www.xyz.st/abc/def">www.xyz.st/abc/def</a>. zzz</p>');

	test_equals(demosphere_misc_text_urls('<p>abc www.xyz.st zzz</p>'),
				'<p>abc <a href="http://www.xyz.st">www.xyz.st</a> zzz</p>');
	test_equals(demosphere_misc_text_urls('<p>http://abc.de/fgh i</p>'),
				'<p><a href="http://abc.de/fgh">http://abc.de/fgh</a> i</p>');
	test_equals(demosphere_misc_text_urls('<p>http://abc.de/fgéh i</p>'),
				'<p><a href="http://abc.de/fgéh">http://abc.de/fgéh</a> i</p>');
	test_equals(demosphere_misc_text_urls('<p>http://abc.de/fg. zzz</p>'),
				'<p><a href="http://abc.de/fg">http://abc.de/fg</a>. zzz</p>');
	test_equals(demosphere_misc_text_urls('<p>http://abc.de/fg?x=y&amp;z=t xyz</p>'),
				'<p><a href="http://abc.de/fg?x=y&amp;z=t">http://abc.de/fg?x=y&amp;z=t</a> xyz</p>');
	test_equals(demosphere_misc_text_urls('<p>http://abc.de xyz</p>'),
				'<p><a href="http://abc.de">http://abc.de</a> xyz</p>');
	test_equals(demosphere_misc_text_urls('<p>http://abc.de</p>'),
				'<p><a href="http://abc.de">http://abc.de</a></p>');


}
?>