<?php

require_once 'test-demosphere.php';

/*
function display_event_ratings()
{
	global $demosphere_config;
	$eventIds=db_one_col('SELECT Event.id FROM Event WHERE EXISTS (SELECT * FROM Opinion WHERE Opinion.eventId=Event.id)');
	$userReputations=demosphere_opinion_all_users_reputation();
	foreach($eventIds as $eventId)
	{
		$event=Event::fetch($eventId);
		$avgRatingData=demosphere_opinion_event_rating_average($event->id,$userReputations);
		$userRatings=db_one_col("SELECT userId,rating FROM Opinion WHERE eventId=%d AND isEnabled=1 ORDER BY rating ASC",$event->id);
		if(count($userRatings)===0 || min($userRatings)>=0){continue;}
		if($event->status==0){continue;}
		$opposition=0;
		$oppositionRep=0;
		foreach($userRatings as $userId=>$rating)
		{
			if($rating>=1){continue;}
			$oppositionRep+=$userReputations[$userId];
			$opposition+=$userReputations[$userId]*(1-$rating);
		}


		echo "https://paris.local/rv/$eventId : ".sprintf("%1.2f",$opposition)." :  ";
		echo sprintf("%1.2f",$avgRatingData['average'])." :  ";
		foreach($userRatings as $userId=>$rating)
		{
			echo "$rating(".round($userReputations[$userId],2)."), ";
		}
		echo "\n";
		//$totRep=0;
		//foreach($userIds as $uid){$totRep+=$userReputations[$uid];}
		//vd($avgRatingData['average']>0	&& $totRep>=$demosphere_config['opinions_auto_publish']);
	}
}
*/

// Sum[all](gi*ri)/Sum[all](gi) > Sum[ri<=1](gi*(1-ri))

function test_demosphere_opinion()
{
	// ********************************
	// Create users and events from data in a CSV file
	// Outputs another CSV file describing the eveolution of reputations.
	// This is a "destructive" test. Only run it on a dev site.

	$results=[];
	require_once 'dlib/parse-file.php';
	$testList=dlib_read_csv('demosphere/test/opinions.csv',",");
	$users=[];

	// cleanup previous
	$ids=db_one_col("SELECT id FROM Event WHERE title LIKE '%%opinion test event%%'");
	foreach($ids as $id){Event::fetch($id)->delete();}
	$ids=db_one_col("SELECT id FROM User  WHERE login IN ('M1','M2','U1','U2','U3')");
	foreach($ids as $id){User::fetch($id)->delete();}

	foreach($testList[0] as $colName=>$colVal)
	{
		if(preg_match('@^([UM])[0-9]+$@',$colName,$m))
		{
			$users[$colName]=new User();
			$users[$colName]->login=$colName;
			$users[$colName]->role=$m[1]==='U' ? 0 : 2;
			$users[$colName]->contributorLevel=$m[1]==='U' ? 1 : 0;
			$users[$colName]->save();
		}
	}

	foreach($testList[0] as $colName=>$colVal)
	{
		$resultLine[$colName]=$colName;
		if(isset($users[$colName]))
		{
			$resultLine[$colName.'-rep'   ]='';
			$resultLine[$colName.'-totrep']='';
		}
	}		
	$results[]=$resultLine;

	$date=time()-3600-24*20;

	foreach($testList as $nb=>$eventDesc)
	{
		$event=new Event(['title'=>'opinion test event '.$nb,
						  'body'=>'opinion test event '.$nb,
						  'startTime'=>time()+3600*24,
						  'placeId'=>420]);

		if($eventDesc['status']==0){$event->moderationStatus=6;}
		else                       {$event->moderationStatus=4;$event->status=1;}
		$event->save();
		foreach($eventDesc as $colName=>$colVal)
		{
			if(!isset($users[$colName])){continue;}
			if(trim($colVal)===''){continue;}
			$opinion=new Opinion();
			$opinion->eventId=$event->id;
			$opinion->userId =$users[$colName]->id;
			$opinion->rating =$colVal;
			$opinion->time=$date;
			$opinion->save();
			$date+=3600*10;
		}

		// ** now get results and store in $results

		// copy src event line
		foreach($eventDesc as $colName=>$colVal)
		{
			$resultLine[$colName]=$colVal;
			if(isset($users[$colName]))
			{
				$rep=db_result_check('SELECT reputation FROM Opinion WHERE userId=%d AND eventId=%d',$users[$colName]->id,$event->id);
				if($rep===false){$rep='';}
				else            {$rep=round($rep,2);}
				$resultLine[$colName.'-rep'   ]=$rep;
				$resultLine[$colName.'-totrep']=round((float)demosphere_opinion_user_reputation($users[$colName]),2);
			}
		}		
		$results[]=$resultLine;

	}
	$handle=fopen('/tmp/opinion-ratings.csv','w');
	foreach($results as $resultLine)
	{
		fputcsv($handle,$resultLine, ";");
	}
	fclose($handle);
}

// This is a "destructive" test. Only run it on a dev site.
function test_demosphere_opinion_toulouse($specialUserLogin=false)
{
    db_query("DELETE FROM Event  WHERE title LIKE 'TTEvent%%'");
    db_query("DELETE Opinion FROM Opinion,User WHERE User.id=Opinion.userId AND User.login LIKE 'TTUser_%%'");
    db_query("DELETE FROM User WHERE login LIKE 'TTUser_%%'");

    // dlib @toulouse php-eval '$o=Opinion::fetchList("WHERE 1");echo json_encode($o);' > ~/trash/toulouse-opinions.json
    // dlib @toulouse php-eval 'echo json_encode(db_arrays_keyed("SELECT id,login,role,contributorLevel FROM User WHERE EXISTS (SELECT * FROM Opinion WHERE userId=User.id)"));' > ~/trash/toulouse-users.json
    // dlib @toulouse php-eval 'echo json_encode(db_arrays_keyed("SELECT id,title,status,moderationStatus,startTime FROM Event WHERE EXISTS (SELECT * FROM Opinion WHERE Opinion.eventId=Event.id) "));' > ~/trash/toulouse-events.json

    $import=json_decode(file_get_contents('files/private/toulouse-opinions.json'));

    $users=(array)json_decode(file_get_contents('files/private/toulouse-users.json'));
    //vd($users);
    $users=array_combine(array_keys($users),array_values($users));

    $events=(array)json_decode(file_get_contents('files/private/toulouse-events.json'));
    $events=array_combine(array_keys($events),array_values($events));
    vd(array_keys($events));

//    $yesMan=new User();
//    $yesMan->login='TTUser_YesMan';
//    $yesMan->role=0;
//    $yesMan->contributorLevel=2;
//    $yesMan->setHashedPassword('ttt');
//    $yesMan->save();


    foreach($import as $iOpinion)
    {
        if(!isset($users[$iOpinion->userId])){echo "no matching user\n";vd($iOpinion->userId);die("\n");}
        if(!isset($events[$iOpinion->eventId])){echo "no matching event\n";die("\n");}

        // *** Create user if it is not already created
        $iUser=$users[$iOpinion->userId];
        if(preg_match('@^contrib@',$iUser->login)){echo "skiping user ".$iUser->login."\n";continue;}
        if(!isset($iUser->user))
        {
            $lUser=new User();
            $lUser->login='TTUser_'.$iUser->login;
            $lUser->role=0;
            $lUser->role=($lUser->login===$specialUserLogin ? 0 : $iUser->role);
            $lUser->contributorLevel=$iUser->contributorLevel;
            $lUser->setHashedPassword('ttt');
            $lUser->save();
			$iUser->user=$lUser;
        }
        $lUser=$iUser->user;

        // *** Create event if it is not already created
        $iEvent=$events[$iOpinion->eventId];
        if(!isset($iEvent->event))
        {
            $lEvent=new Event(['title'=>'TTEvent: '.$iEvent->title,
            'body'=>'opinion test event <a href="https://toulouse.demosphere.net/rv/'.$iEvent->id.'">'.$iEvent->id.'</a>',
						  'startTime'=>$iEvent->startTime,
						  'status'=>$iEvent->status,
						  'moderationStatus'=>$iEvent->moderationStatus,
						  'placeId'=>420]);
            $lEvent->save();
			$iEvent->event=$lEvent;

            // Automatic yes by $yesMan
            //$yesOpinion=new Opinion();
            //$yesOpinion->eventId=$lEvent->id;
            //$yesOpinion->userId =$yesMan->id;
            //$yesOpinion->time=$lEvent->startTime;
            //$yesOpinion->text='yes!';
            //$yesOpinion->rating =2;
            //$yesOpinion->save();
        }
        $lEvent=$iEvent->event;

        // *** Create Opinion
        $lOpinion=new Opinion();
        $lOpinion->eventId=$lEvent->id;
        $lOpinion->userId =$lUser->id;
        $lOpinion->time=$iOpinion->time;
        $lOpinion->text=$iOpinion->text;
        $lOpinion->rating =$iOpinion->rating;
        $lOpinion->pubGuideline=$iOpinion->pubGuideline;
        $lOpinion->modComment=$iOpinion->modComment;
        $lOpinion->modReputation=$iOpinion->modReputation;
        $lOpinion->isEnabled=$iOpinion->isEnabled;
        $lOpinion->save();
        echo "imported opinion ".$lOpinion->id." : rating: ".$lOpinion->rating."\n";
    }

}