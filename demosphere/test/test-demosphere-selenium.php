<?php

function demosphere_selenium_testing()
{
	global $demosphere_config,$base_url;
	if(strpos($base_url,'demoselenium')===false ||
	   exec('hostname')!=='gloups' ||
	   $demosphere_config['site_id']!=='demoselenium'){fatal('demosphere_selenium_testing failed');}
	switch($_GET['action'])
	{
	case 'php-exec': demosphere_selenium_php_exec();break;
	default: fatal('demosphere_selenium_php_exec bad action');
	}
}

function demosphere_selenium_php_exec()
{
	require_once 'demosphere-place.php';
	require_once 'demosphere-place-search.php';

	global $demosphere_config,$base_url,$user,$currentPage;
	$auth=$_POST['auth'];
	if(hash("SHA256",$auth)!=='115820c66cbe95eb7dc65e479a4d4b6977fc10e2a1d0c0614bdc10448cfb10aa'){fatal('bad auth');}
	$phpCode=$_POST['php'];
	file_put_contents('/tmp/selenium-php-exec.log',date('r')."\n".'PHP CODE: '.$phpCode."\n",FILE_APPEND);
	ob_start();
	$ok=eval($phpCode);
	if($ok===false){fatal("demosphere_selenium_php_exec: parse error in eval");}
	file_put_contents('/tmp/selenium-php-exec.log','OUTPUT: '.mb_substr(ob_get_contents(),0,1000)."\n",FILE_APPEND);
	ob_end_flush();
}

?>