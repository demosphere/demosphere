<?php

//! Main entry point for unit tests run from cron.
function test_demosphere($emailOnFail=false)
{
	global $test_fail_count;
	global $test_ok_count,$demosphere_config,$dlib_config;
	$test_fail_count=0;
	$test_ok_count=0;

	error_reporting(E_STRICT | E_ALL);

	require_once 'dlib/testing.php';

	// *************** 
	// *************** non destructive tests (dont change app state: safe to use on live site)
	// *************** 

	// test dlib (short)
	require_once 'dlib/test/test-dlib.php';
	test_dlib(true);

	require_once 'test-demosphere-apache-config.php';
	test_demosphere_apache_config();

	// ****** html-selector
	require_once 'html-selector/html-selector.php';
	require_once 'html-selector/test-html-selector.php';
	//html_selector_web_test();

	require_once 'test-demosphere-htmledit.php';
	test_demosphere_htmledit();

	require_once 'test-demosphere-htmlview.php';
	test_demosphere_htmlview();

	require_once 'test-demosphere-misc.php';
	test_demosphere_misc();

	// ****** feed-import
	require_once 'feed-import/test-feed-import.php';
	test_feed_import();


	// ****** mail-import
	require_once 'mail-import/test/test-mail-import.php';
	test_mail_import();
	// require_once 'mail-import/test/test-mime-message.php';
	// test_mime_message();



	// ****** 
	// test_demosphere_email_subscription_cron();
	// ****** 
	test_demosphere_event_list_form_parse();
	// ****** 
	//test_demosphere_copy_part();

	// ****** docconvert
	//require_once 'docconvert/docconvert.php';
	//require_once 'docconvert/test-docconvert.php';
	//docconvert_web_test();

	// ****** page-watch
	//require_once 'page-watch/page-watch.php';
	//require_once 'page-watch/test/test-page-watch.php';
	//page_watch_web_test();

	test_demosphere_make_images_local();

	// ****** text-matching
	//require_once 'text-matching/test-text-matching.php';
	//test_text_matching();

	// ****** mapshape
	require_once 'demosphere-event-map.php';
	test_demosphere_mapshape();

	test_demosphere_event_import();

	require_once 'demosphere/html-selector/test/test-html-selector.php';
	test_html_selector();

	// re-test dlib (long test)
	test_dlib(true);

	echo "TOTAL: failed:$test_fail_count  OK:$test_ok_count\n";
	if($emailOnFail && $test_fail_count>0)
	{
		$headers = "From: www-data@localhost\r\n" .
			'X-Mailer: PHP/' . phpversion()."\r\n".
			"MIME-Version: 1.0\r\n".
			"Content-Type: text/plain; charset=utf-8\r\n".
			"Content-Transfer-Encoding: 8bit\r\n";
		mail('root@localhost', 'demosphere_test() failed',
			 "TOTAL: failed:$test_fail_count  OK:$test_ok_count\n".
			 "check logs for more info : \n".
			 '~/demosphere/log/demosphere-daily-unit-test.log',
			 $headers);
	}
// FIXME touch('files/private/demosphere-unit-tests-finished-ok');
}

function ns($s)
{
	return preg_replace('@[[:space:]]+@',' ',$s);
}


function test_demosphere_make_images_local()
{
	require_once 'dlib/html-tools.php';

	$src='<img src="http://www.google.com/intl/en_ALL/images/logo.gif"/>';
	$dir='/tmp';
	require_once 'demosphere-misc.php';
	$res=demosphere_make_images_local($src,false,'/tmp');
	echo ent($res)."\n";
	preg_match('@src="([^"]*)"@',$res,$matches);
	$newUrl=$matches[1];
	echo $newUrl."\n";
	global $base_url;
	$checkBase=substr($newUrl,0,strlen($base_url));
	$newFname =substr($newUrl,  strlen($base_url)+1);
	echo '$base_url:'.$base_url."\n";
	echo '$checkBase:'.$checkBase."\n";
	echo '$newfname:'.$newFname."\n";
	test_assert(file_exists($newFname));
	test_equals($checkBase,$base_url);	
	return true;
}


function test_demosphere_email_subscription_cron()
{
	require_once 'demosphere-email-subscription.php';
	$t0=strtotime('Oct 15 2009 10:00');
	$test1=['now'=>$t0+60*20,
			'values'=>
			[
				'active'=>true,
				'last-sent-time'=>strtotime('-1 day',$t0),
				'how-often-days'=>1,
				'email-send-times'=>[[10,0]],
			]];
	$next=demosphere_email_subscription_cron($test1);
	echo 'last:'.date('r',$test1['values']['last-sent-time']).'<br/>';
	echo 'next:'.date('r',$next).'<br/>';
	test_equals($next,$t0);

	$t0=strtotime('Oct 15 2009 10:00');
	$test1=['now'=>$t0+60*20,
			'values'=>
			[
				'active'=>true,
				'last-sent-time'=>strtotime('7:00',$t0),
				'how-often-days'=>1,
				'email-send-times'=>[[7,0],[10,0],],
			]];
	$next=demosphere_email_subscription_cron($test1);
	echo 'last:'.date('r',$test1['values']['last-sent-time']).'<br/>';
	echo 'next:'.date('r',$next).'<br/>';
	test_equals($next,$t0);

	$t0=strtotime('Oct 15 2009 10:00');
	$test1=['now'=>$t0+60*20,
			'values'=>
			[
				'active'=>true,
				'last-sent-time'=>$t0,
				'how-often-days'=>1,
				'email-send-times'=>[[7,0],[10,0],],
			]];
	$next=demosphere_email_subscription_cron($test1);
	echo 'last:'.date('r',$test1['values']['last-sent-time']).'<br/>';
	echo 'next:'.date('r',$next).'<br/>';
	test_equals($next,strtotime('Oct 16 2009 7:00'));


	$t0=strtotime('Oct 15 2009 10:00');
	$test1=['now'=>$t0+60*20,
			'values'=>
			[
				'active'=>true,
				'last-sent-time'=>$t0,
				'how-often-days'=>3,
				'email-send-times'=>[[10,0],],
			]];
	$next=demosphere_email_subscription_cron($test1);
	echo 'last:'.date('r',$test1['values']['last-sent-time']).'<br/>';
	echo 'next:'.date('r',$next).'<br/>';
	test_equals($next,strtotime('Oct 18 2009 10:00'));

	//echo demos_format_date('full-date-time',$next);
}


function test_demosphere_mapshape()
{
	$shape=['isPublic'=>true,
			'type'=>'polygon',
			'coordinates'=>[[0,0],
							[1,0],
							[.5,1],],
		   ];
 // coords are inverted between shapecoords and point_is_inside 
 //                X                                 
 //                                              
 //                                              
 //            C  B                               
 //             X     X                            
 //                                              	
 //                                              	
 //                 A                               	
	test_equals(demosphere_mapshape_point_is_inside($shape,-1 ,.6),false);
	test_equals(demosphere_mapshape_point_is_inside($shape,.25,.25),true);
	test_equals(demosphere_mapshape_point_is_inside($shape,.25,-.1),false);

	$kml='<?xml version="1.0" encoding="UTF-8"?><kml xmlns="http://earth.google.com/kml/2.2"><Document><name><![CDATA[Val-de-Marne (94)]]></name><Style id="gitesdegaule.fr"><LineStyle><color>cc2d3939</color><width>3</width></LineStyle><PolyStyle><color>804d4def</color></PolyStyle></Style><Placemark id="val-de-marne"><name><![CDATA[Val-de-Marne (94)]]></name><styleUrl>#gitesdegaule.fr</styleUrl><Polygon><outerBoundaryIs><LinearRing><tessellate>1</tessellate><coordinates>2.54428931,48.69844832,250 2.54019066,48.69899471,250 2.53105501,48.69978299000001,250 2.52788074,48.70437229,250 2.52911008,48.70630284000001,250 2.52610355,48.70861771,250 2.51927146,48.71270017,250 2.51796647,48.71591104000001,250 2.51545792,48.72907476000001,250 2.51004044,48.73466363000001,250 2.5054763,48.73470613000001,250 2.48453452,48.72907128,250 2.48010208,48.72791719000001,250 2.47535958,48.72759922,250 2.4729683,48.72756602,250 2.46800777,48.72696038000002,250 2.4503339,48.72150459,250 2.45093177,48.71535594,250 2.44655918,48.71591643000001,250 2.44245406,48.72116128,250 2.43981295,48.72516342000002,250 2.43511028,48.72408878000001,250 2.4305174,48.72352621,250 2.41424739,48.71781226,250 2.414713750000001,48.72084015,250 2.41099897,48.72604769,250 2.3865645,48.72009296000001,250 2.3706858,48.72016949,250 2.37041739,48.72530194000001,250 2.37041902,48.72786362,250 2.37035302,48.73089181,250 2.36982039,48.74605608,250 2.36487166,48.74568971000001,250 2.35451295,48.73864398000001,250 2.35042308,48.74050508000001,250 2.34073502,48.74090124,250 2.33073067,48.74810068,250 2.32799839,48.75013184,250 2.32045508,48.7489985,250 2.31190308,48.75172939,250 2.31146636,48.75801202,250 2.3165063,48.76684832,250 2.31990552,48.77071373,250 2.32562625,48.78182361,250 2.32240148,48.78691043,250 2.31853816,48.78802439,250 2.32250711,48.79814531,250 2.3238395,48.80151591,250 2.3249001,48.80421237000001,250 2.31893986,48.80992784000001,250 2.323157,48.80969473,250 2.32888526,48.81356899000001,250 2.33201494,48.81693957,250 2.34405939,48.81595084,250 2.35262179,48.81831382,250 2.35557294,48.81585975000001,250 2.36002311,48.81560733000001,250 2.364419180000001,48.81608275,250 2.36880232,48.81755476000001,250 2.38989247,48.82591574,250 2.39397789,48.82756759,250 2.40322279,48.82916262,250 2.41991748,48.82392139,250 2.45662548,48.81699666,250 2.46117416,48.81826827,250 2.46616269,48.82666647000001,250 2.464972980000001,48.82998419000001,250 2.46960336,48.83607274000001,250 2.46729649,48.83912207000001,250 2.46465964,48.84153359,250 2.44755263,48.84489403000001,250 2.44282885,48.84554541,250 2.43704672,48.84116448,250 2.42744957,48.8416305,250 2.42294304,48.84271233,250 2.4164766,48.8346999,250 2.41586843,48.84663577,250 2.41634901,48.84924183,250 2.41895022,48.84932995,250 2.44228422,48.85230359000001,250 2.44708928,48.85114976,250 2.45427597,48.85542095,250 2.476829910000001,48.86031278,250 2.48151861,48.86140366,250 2.49637493,48.85990182,250 2.49969883,48.85567323,250 2.50392736,48.85723031,250 2.510721,48.85270081000001,250 2.51365409,48.85016203,250 2.51563296,48.85138138,250 2.518252270000001,48.84877106,250 2.53423565,48.84406319000001,250 2.53895366,48.83875262000001,250 2.56776946,48.82466161,250 2.56847606,48.81827895,250 2.56998607,48.81508535,250 2.59225409,48.80743604,250 2.596479130000001,48.80606974,250 2.59303672,48.80029799,250 2.59152439,48.79736130000001,250 2.59656726,48.79647049,250 2.59879629,48.79335579,250 2.59671281,48.79020558000001,250 2.58865818,48.78140566,250 2.58711586,48.77461395,250 2.6064328,48.77330448,250 2.61479866,48.76111532000001,250 2.59753008,48.76056925000001,250 2.59953337,48.75075062000001,250 2.58774372,48.74407171000001,250 2.59408766,48.73523075000001,250 2.594655,48.73180496,250 2.580387650000001,48.72298144000001,250 2.57094974,48.71172975000001,250 2.56811241,48.70897493,250 2.56878368,48.70722086000001,250 2.5715744,48.70265909,250 2.57432354,48.70099794000001,250 2.57632341,48.69618593,250 2.57224579,48.69496263,250 2.57163509,48.69201557,250 2.56689586,48.69182708000001,250 2.549071670000001,48.68909172,250 2.54831139,48.69243685,250 2.54428931,48.69844832,250</coordinates></LinearRing></outerBoundaryIs></Polygon></Placemark></Document></kml>';

	$coords=demosphere_mapshape_parse_kml_polygon($kml);
	$shape=['type'=>'polygon',
			'coordinates'=>$coords,
		   ];
	test_equals(demosphere_mapshape_point_is_inside($shape,48.856696,2.350817),false);
	test_equals(demosphere_mapshape_point_is_inside($shape,48.8643756,2.4469474),false);
	test_equals(demosphere_mapshape_point_is_inside($shape,48.786622,2.3803428),true);
	test_equals(demosphere_mapshape_point_is_inside($shape,48.8085589,2.4294379),true);
 	test_equals(demosphere_mapshape_point_is_inside($shape,48.7332096,2.4644568),true);
	test_equals(demosphere_mapshape_point_is_inside($shape,48.6284836,2.4280646),false);
	test_equals(demosphere_mapshape_point_is_inside($shape,48.7218861,2.7755072),false);
	test_equals(demosphere_mapshape_point_is_inside($shape,48.7037633,2.165766),false);
	test_equals(demosphere_mapshape_point_is_inside($shape,45.735062,-3.4317682),false);


}

function test_demosphere_event_list_form_parse()
{
	require_once 'demosphere-event-list.php';
	require_once 'demosphere-event-list-form.php';

	$calOptions=[];
	$options=['near-lat-lng-use-map'=>false,
			  'near-lat-lng-distance'=>'1.23',
			  'near-lat-lng-latitude-and-longitude'=>'4.56,-7.89',];
	demosphere_event_list_form_item_parse('nearLatLng' ,$options,$calOptions);
	test_equals($calOptions,['nearLatLng'=>false]);

	$calOptions=[];
	$options['near-lat-lng-use-map']=true;
	demosphere_event_list_form_item_parse('nearLatLng' ,$options,$calOptions);
	test_equals($calOptions,
				['nearLatLng'=>['lat'=>4.56,'lng'=>-7.89,'distance'=>1.23]]);

	$calOptions=[];
	$options=['near-lat-lng-use-map'=>false,
			  'near-lat-lng-distance'=>'xyz1.23',
			  'near-lat-lng-latitude-and-longitude'=>'4.56,-7.89',];
	demosphere_event_list_form_item_parse('nearLatLng' ,$options,$calOptions);
	test_equals($calOptions,['nearLatLng'=>false]);

	list($ok,$value,$message)=
		demosphere_event_list_form_item_validate('near-lat-lng-distance','xyz1.23','simple');
	test_equals($ok,false);
	test_assert(strlen($message)>0);

	list($ok,$value,$message)=
		demosphere_event_list_form_item_validate('near-lat-lng-distance','1.23','simple');
	test_equals($ok,true);
	test_equals($value,1.23);
	test_equals($message,false);


	require_once 'demosphere-event-map.php';
	$shape=['isPublic'=>true,
			'type'=>'polygon',
			'coordinates'=>[[0,0],
							[1,0],
							[.5,1],],
		   ];
	demosphere_mapshape_save('testshape',$shape);
	
	$calOptions=[];
	$options=['map-shape'=>'testshape'];
	$ok=demosphere_event_list_form_item_parse('mapShape' ,$options,$calOptions,false);
	test_equals($calOptions,['mapShape'=>'testshape']);
	test_equals($ok,true);

	$calOptions=[];
	$options=['map-shape'=>'BADtestshape'];
	$ok=demosphere_event_list_form_item_parse('mapShape' ,$options,$calOptions,false);
	test_equals($calOptions,[]);
	test_equals($ok,false);

	list($ok,$value,$message)=
		demosphere_event_list_form_item_validate('map-shape','testshape','simple');
	test_equals($ok,true);
	test_equals($value,'testshape');
	test_equals($message,false);

	list($ok,$value,$message)=
		demosphere_event_list_form_item_validate('map-shape','BADtestshape','simple');
	test_equals($ok,false);
	test_assert(strlen($message)>0);

	list($ok,$value,$message)=
		demosphere_event_list_form_item_validate('map-shape','','simple');
	test_assert($ok===true && $value===false);

	list($ok,$value,$message)=
		demosphere_event_list_form_item_validate('map-shape',' ','simple');
	test_assert($ok===true && $value===false);

	list($ok,$value,$message)=
		demosphere_event_list_form_item_validate('limit','','simple');
	test_assert($ok===true && $value===false);

	list($ok,$value,$message)=
		demosphere_event_list_form_item_validate('limit','123','simple');
	test_assert($ok===true && $value===123);

	$calOptions=[];
	$ok=demosphere_event_list_form_item_parse('limit' ,['limit'=>''],$calOptions,false);
	test_equals($ok,true);
	test_equals($calOptions,['limit'=>false]);

	$calOptions=[];
	$ok=demosphere_event_list_form_item_parse('limit' ,['limit'=>false],$calOptions,false);
	test_equals($ok,true);
	test_equals($calOptions,['limit'=>false]);

	$calOptions=[];
	$ok=demosphere_event_list_form_item_parse('limit' ,['limit'=>'123'],$calOptions,false);
	test_equals($ok,true);
	test_equals($calOptions,['limit'=>123]);

	$calOptions=[];
	$ok=demosphere_event_list_form_item_parse('limit' ,['limit'=>'xyz'],$calOptions,false);
	test_equals($ok,false);

	//demosphere_event_list_parse_getpost_option('limit','123'

}


//! FIXME: very old (Drupal version!)
function test_demosphere_copy_part()
{
	require_once 'demosphere-misc.php';

	// create two nodes: node1 is ref
	$node1=new Event();
	$node1->save();
	$node2=new Event();
	$node2->save();
	$index=vxariable_get('demosphere_copy_part_index',[]);
	test_assert(!isset($index[$node1->nid]));
	test_assert(!isset($index[$node2->nid]));
	node_load(false,null,true);// empty node_load cache

	$node1Text='<p>part1</p><hr /><p>part2 demosphere_dtoken_copy_part('.$node1->nid.')</p>';
	$node1->body=$node1Text;
	node_save($node1);
	$index=vxariable_get('demosphere_copy_part_index',[]);
	test_assert(!isset($index[$node1->nid]));
	test_assert(!isset($index[$node2->nid]));
	node_load(false,null,true);

	$node2->body='<p>partA</p><hr /><p>partB demosphere_dtoken_copy_part('.$node1->nid.')</p>';
	node_save($node2);
	$index=vxariable_get('demosphere_copy_part_index',[]);
	test_assert( isset($index[$node1->nid]));
	test_assert(!isset($index[$node2->nid]));
	//var_dump($node1->nid);
	//var_dump($node2->nid);
	//var_dump($index);
	test_equals($index[$node1->nid],[intval($node2->nid)]);
	
	// now re-load node2, which should should have changed partB to part2
	node_load(false,null,true);
	$node2=node_load($node2->nid);
	test_contains($node2->body,'partA');
	test_not_contains($node2->body,'partB');
	test_contains($node2->body,'part2');

	// now re-load node1, make sure nothing changed
	node_load(false,null,true);
	$node1=node_load($node1->nid);
	test_equals($node1->body,$node1Text);
	//echo "node1 body:";var_dump($node1->body);
	//echo "node1 text:";var_dump($node1Text);
	
	// change and save node1
	$node1->body=str_replace('part2','partX',$node1->body);
	node_save($node1);
	node_load(false,null,true);

	// now re-load node2, which should should have changed part2 to partX
	$node2=node_load($node2->nid);
	test_not_contains($node2->body,'part2');
	test_contains($node2->body,'partX');

	// *** Now check start/end dtokens
	$node1Text='<p>abc demosphere_dtoken_copy_part('.$node1->nid.',start)def '.
		'demosphere_dtoken_copy_part('.$node1->nid.',end)</p>';
	$node1->body=$node1Text;
	node_save($node1);
	node_load(false,null,true);
	$node2=node_load($node2->nid);
	test_contains($node2->body,'def');
	// copying from start/end block to hr part, should re-add token:
	test_contains($node2->body,'demosphere_dtoken_copy_part');

	// now try a des start/end block
	$node2->body='rst demosphere_dtoken_copy_part('.$node1->nid.',start)ZZZ demosphere_dtoken_copy_part('.$node1->nid.',end) uvw';
	node_load(false,null,true);
	node_save($node2);
	// check if update ok
	$node2=node_load($node2->nid,null,true);
	test_not_contains($node2->body,'ZZZ');
	test_contains($node2->body,'def');
	test_equals(substr_count($node2->body,'demosphere_dtoken_copy_part'),2);


	node_load(false,null,true);
	$node1Text='<p>YYY demosphere_dtoken_copy_part('.$node1->nid.')QQQ</p>';
	$node1->body=$node1Text;
	node_save($node1);
	$node2=node_load($node2->nid,null,true);
	test_not_contains($node2->body,'def');
	test_contains($node2->body,'YYY');
	test_contains($node2->body,'QQQ');
	test_equals(substr_count($node2->body,'demosphere_dtoken_copy_part'),2);

	// delete node1 and check if index has been removed
	$n1nid=$node1->nid;
	node_delete($node1->nid,true);
	$index=vxariable_get('demosphere_copy_part_index',[]);
	test_assert(!isset($index[$n1nid]));
	node_delete($node2->nid);
	// erase drupal messages to avoid confusing future tests
	$_SESSION['dlib_messages'] = [];
}

function test_demosphere_event_import()
{
	require_once 'demosphere-event-import.php';
	test_equals(demosphere_event_import_baseurl('http://a.b/c.d'),'http://a.b');
	test_equals(demosphere_event_import_baseurl('http://a.b/c.d/'),'http://a.b/c.d');
	test_equals(demosphere_event_import_baseurl('http://a.b'),'http://a.b');
	test_equals(demosphere_event_import_baseurl('http://a.b/c/d/e/f/g'),'http://a.b/c/d/e/f');
	test_equals(demosphere_event_import_baseurl('http://a.b/c/d/e/f/' ),'http://a.b/c/d/e/f');
}

?>