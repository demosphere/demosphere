<?php

//! Displays Opinions below an Event, adding a form where the User/contributor can edit his opinion.
function demosphere_opinions_event($event)
{
	global $base_url,$demosphere_config,$user,$currentPage;

	// not really necessary, but it's safer to double check
	if(!$event->access('view')){dlib_permission_denied_403('demosphere_opinions_event: view not permitted');}
	if(!$demosphere_config['enable_opinions']){fatal('opinions not enabled');}

	$isEventAdmin=$user->checkRoles('admin','moderator');
	if(!$isEventAdmin && $user->contributorLevel==0){return '';}

	// check for existing opinion for this user on this event
	$id=db_result_check('SELECT id FROM Opinion WHERE userId=%d AND eventId=%d',$user->id,$event->id);
	if($id===false)
	{
		$opinion=new Opinion();
		$opinion->userId=$user->id;
		$opinion->eventId=$event->id;
	}
	else {$opinion=Opinion::fetch($id);}

	// Level 1 contributors only see their own opinion
	if($user->contributorLevel>1 || $isEventAdmin)
	{
		$opinions=Opinion::fetchList('WHERE eventId=%d ORDER BY time DESC',$event->id);
	}
	else
	{
		$opinions=Opinion::fetchList('WHERE eventId=%d AND userId=%d',$event->id,$user->id);
	}

	$currentPage->addCssTpl('demosphere/css/demosphere-opinion.tpl.css');

	$ratingLabels=[
				   //  "9": hack to avoid confusion with 0 :-( 
				   '9choose'  =>t('(choose)'),
				   '9dontknow'=>t("I don't know"),
				   +5=>'+5 : '.t("This event fits perfectly in the guidelines. Rejection would be a big problem for me."),
				   +4=>'+4 : '.t("This event fits perfectly in the guidelines. I strongly support its publication."),
				   +3=>'+3 : '.t("This event fits in the guidelines."),
				   +2=>'+2 : '.t("This event fits in the guidelines, with minor reservations."),
				   +1=>'+1 : '.t("This event has significant issues with the publication guidelines, but should still be published."),
				   +0=>' 0 : '.t("This event has significant issues with the publication guidelines. I'm split."),
				   -1=>'-1 : '.t("This event has enough issues with the guidelines that it shouldn't be published."),
				   -2=>'-2 : '.t("This event shouldn't be published. There is a significant issue with the guidelines."),
				   -3=>'-3 : '.t("This event shouldn't be published. An important point in the guidelines is not respected."),
				   -4=>'-4 : '.t("This event contradicts the guidelines in very important ways."),
				   -5=>'-5 : '.t("This event contradicts the core values of this site. Its publication would be a big problem for me."),
				   ];

	require_once 'dlib/form.php';
	// Disable all opinions single button form.
	$disAllForm=false;
	if($isEventAdmin)
	{
		$ditems=['dis-all'=>['type'=>'submit',
							 'value'=>'x',
							 'attributes'=>['title'=>t('Disable all existing opinions on this event. Use this if moderator\'s decision is unrelated to opinions (for example: event is canceled)')],
							 'submit'=>function()use($event)
								 {
									 db_query('UPDATE Opinion SET isEnabled=0, reputation=0, weight=0 WHERE eventId=%d',$event->id);
									 demosphere_opinions_update_all_reputations_for_event($event->id);
									 dlib_message_add(t('Disabled all existing opinions on this event.'));
								 }]];
		$disAllForm=form_process($ditems,['id'=>'dis-all-form']);
	}

	$form=false;
	// Only show form for events that are open to opinions
	if($event->canGiveOpinion())
	{
		list($objForm,$formOpts)=form_dbobject($opinion);

		$items=[];
		$items['userId' ]=['type'=>'data','value'=>$user->id];
		$items['eventId']=['type'=>'data','value'=>$event->id];
		$items['isEnabled']=$objForm['isEnabled'];
		$items['isEnabled']['type']='data';
		$items['isEnabled']['value']=true;

		$items['time']=['type'=>'data','value'=>time()];

		require_once 'demosphere-date-time.php';			
		$items['title']=['html'=>'<div class="opinion-information">'.ent(t('My opinion')).
						 ' - '.($opinion->id ? ent(t('saved on ')).' '.ent(demos_format_date('shorter-date-time',$opinion->time)) : ent(t('not saved'))).
						 '</div>'];

		$items['rating']=$objForm['rating'];
		$items['rating']['title']=t('Rating');
		$items['rating']['type']='select';
		$items['rating']['options']=$ratingLabels;
		if(!$opinion->id){$items['rating']['default-value']='9choose';} // new opinion forces user to choose
		$items['rating']['attributes']['onchange']=
			'var isVisible=parseInt(this.options[this.selectedIndex].getAttribute(\'value\'))<=1;
			 document.getElementById(\'edit-pubGuideline-wrapper\').setAttribute(\'style\',isVisible ? \'display:block\' : \'display:none\');';
		$items['rating']['validate']=function($v){if($v==='9choose'){return t('This field is required');}};
									 
		$items['text']=$objForm['text'];
		$items['text']['type']='textarea';
		$items['text']['title']=t('Comment');
		$items['text']['attributes']['rows']=3;
		$items['text']['required']=function($v,$name,$items){return abs($items['rating']['value'])>4;};

		$items['pubGuideline']=$objForm['pubGuideline'];
		$items['pubGuideline']['title']=t('Guidelines');
		$items['pubGuideline']['type']='textarea';
		$items['pubGuideline']['pre-submit']=function(&$v,$u1,$u2,$items){if($items['rating']['value']>0){$v='';}};
		$items['pubGuideline']['required'  ]=function($v,$name,&$items	 )
			{
				if($v==='' && $items['rating']['value']<0)
				{$items[$name]['error']=t('When you give a negative rating, you must select an item in publishing guidelines.');}
				return $items['rating']['value']<0;
			};

		$items['save']	=$objForm['save'];
		$items['save']['submit']=function($items)use($opinion)
			{
				global $user;
				if($items['rating']['value']==='9dontknow')
				{
					$opinion->isEnabled=false;
					$opinion->rating=0;
					$opinion->save();
				}
				dlib_message_add(t('Your opinion was saved successfully.'));

				demosphere_opinion_auto_publish($opinion->useEvent(),$user);
			};
		$items['save']['redirect']=$event->url().'#ignore'; // redirect to top of page, so that user sees message

		// Add HTML & JS that depends on actual values. 
		// Using this callback is necessary, since values are not known during form build (on validation error).
		$formOpts['pre-render']=function(&$items,$formOpts,$isValidationErr)
			{
				global $demosphere_config;
				$rating		 =$isValidationErr ? $items['rating'	  ]['value'] : $items['rating'		]['default-value'];
				$pubGuideline=$isValidationErr ? $items['pubGuideline']['value'] : $items['pubGuideline']['default-value'];

				$items['pubGuideline']['wrapper-attributes']['style']=$rating<0 ? 'display:block' : 'display:none';

				$pubGuidelines=array_map('trim',preg_split("@[\n\r]+---[\n\r]+@",$demosphere_config['opinions_pub_guidelines']));
				$pubGuidelinesSel='<select id="pubGuidelinesSel" '.
				'onchange="document.getElementById(\'edit-pubGuideline\').textContent=this.options[this.selectedIndex].getAttribute(\'value\')">';
				$pubGuidelinesSel.='<option value="">'.t('--- Choose guidelines point --').'</option>';
				foreach($pubGuidelines as $pg)
				{
					$pubGuidelinesSel.='<option value="'.ent($pg).'" '.
						((dlib_cleanup_plain_text(trim($pg))==
						  dlib_cleanup_plain_text(trim($pubGuideline))) ? ' selected' :'' ).'>'.
						ent(preg_replace("@\n.*$@s"," (...)",$pg)).
						'</option>';
				}
				$pubGuidelinesSel.='</select>';
				$items['pubGuideline']['field-prefix']=$pubGuidelinesSel;
			};

		$form=form_process($items,$formOpts);
	}

	// *** Forms so that moderators can add comments & reputation on each opinion
	$disForms=[];
	$modForms=[];
	if($isEventAdmin)
	{
		foreach($opinions as $id=>$op)
		{
			$modForm=[];
			$modForm['modData-'		 .$id]=['type'=>'data','data'=>$op];
			$modForm['modReputation-'.$id]=['type'=>'float',
											'title'=>t('Reputation adj.:'),
											'default-value'=>$op->modReputation,
											'required'=>true,
											];
			$modForm['modComment-'	 .$id]=['type'=>'textarea',
											'title'=>t('Mod. comment:'),
											'default-value'=>$op->modComment,
											];
			$modForm['modSave-'		 .$id]=['type'=>'submit',
											'value'=>t('Save'),
											'submit'=>function($items)
											{
												$t=dlib_first($items);
												$op=$t['data'];
												$op->modComment	  =$items['modComment-'	  .$op->id]['value'];
												$op->modReputation=$items['modReputation-'.$op->id]['value'];
												$op->save();
											}
									   ];
			$modForms[$id]=form_process($modForm,['id'=>'opinion-mod-'.$id]);

			$disForm=['dis-'.$op->id=>['type'=>'submit',
									   'data'=>$op,
									   'value'=>$op->isEnabled ? 'x' : '✓',
									   'attributes'=>['title'=>$op->isEnabled ? t('Disable this opinion.') : t('Enable this opinion.')],
									   'submit'=>function($items,$name)
										{
											$op=$items[$name]['data'];
											$op->isEnabled=!$op->isEnabled;
											$op->save();
											dlib_message_add($op->isEnabled ? t('Opinion enabled.') : t('Opinion disabled.'));
										}]];
			$disForms[$id]=form_process($disForm,['id'=>'opinion-dis-'.$id,'attributes'=>['class'=>['opinion-dis']],]);
		}
	}

	$eventRatingData=demosphere_opinion_event_rating_average($event->id);
	$agreement=0;
	demosphere_opinion_auto_publish_check($event,$agreement);
	$eventRatingData['agreement']=$agreement;

	require_once 'demosphere-date-time.php';			
	return template_render('templates/demosphere-opinions-event.tpl.php',
						   [compact('event','disAllForm','opinions','opinion','form','ratingLabels','isEventAdmin','eventRatingData','modForms','disForms'),
						   ]);

}

//! Returns true if this event has enough reputable users giving positive ratings and not too much opposition to be automatically published .
function demosphere_opinion_auto_publish_check($event,&$agreement=null)
{
	global $demosphere_config;
	if(!$demosphere_config['enable_opinions'] || 
	   !$demosphere_config['opinions_auto_publish']){return false;}

	$userReputations=demosphere_opinion_all_users_reputation();
	$avgRatingData=demosphere_opinion_event_rating_average($event->id,$userReputations);
	$userRatings=db_one_col("SELECT userId,rating FROM Opinion WHERE eventId=%d AND isEnabled=1",$event->id);
	$totYesRep=0;
	$opposition=0;
	foreach($userRatings as $uid=>$rating)
	{
		if($rating>=1){$totYesRep+=$userReputations[$uid];}
		else
		{
			$opposition+=$userReputations[$uid]*(1-$rating);
		}
	}

	$agreement=$totYesRep/$demosphere_config['opinions_auto_publish'];
	$agreement-=1.2*$opposition;
	return $avgRatingData['average']>0	&& $agreement>=1;
}


//! Automatically publish or unpublish an event based on its opinions.
//! This is called after opinion form is saved.
function demosphere_opinion_auto_publish($event,$euser)
{
	global $demosphere_config;
	if(!$demosphere_config['opinions_auto_publish']){return;}

	$isRecentAutoPublish=$event->isOpinionAutoPublished() && $event->extraData['opinions-auto-publish']>time()-3600*24;

	$canAutoPublish=demosphere_opinion_auto_publish_check($event);

	// If user can autopublish this event, redirect him to form where he can publish it.
	if($event->status==0 && $canAutoPublish)
	{
		$event->status=1;
		$event->setModerationStatus('published');
		$event->setNeedsAttention('non-event-admin');
		$event->logAdd('opinions-auto-publish:publish');
		$event->extraData['opinions-auto-publish']=time();
		$event->save();
		dlib_message_add(t('There are enough positive ratings to publish this event. It has been automatically published.'));
	}
	else
	// Automatically un-publish this event (less common case, for example when somebody adds a bad rating after auto-publish)
	if($isRecentAutoPublish && !$canAutoPublish)
	{
		$event->status=0;
		$event->setModerationStatus('waiting');
		$event->setNeedsAttention('publication-request');
		$event->logAdd('opinions_auto_publish:unpublish');
		unset($event->extraData['opinions-auto-publish']);
		$event->save();
		dlib_message_add(t('This event no longer has enough positive ratings to be published. It has been automatically un-published.'));
	}
}

//! Display a list of all opinions that a User ever has made.
function demosphere_opinions_of_user($cuid)
{
	global $currentPage,$user,$demosphere_config;
	$currentPage->addCssTpl('demosphere/css/demosphere-opinion.tpl.css');
	$currentPage->addJs('lib/Chart.bundle.min.js');
	$cuser=User::fetch($cuid);

	$isEventAdmin=$user->checkRoles('admin','moderator');

	if(!$isEventAdmin)
	{
		if($user->id!=$cuser->id ||
		   $cuser->contributorLevel==0){dlib_permission_denied_403();}
	}

	$currentPage->title=t('Opinions of !user',['!user'=>$cuser->login]);

	$repChart=
		[
			'type'=>'line',
			'data'=>
			[
				'labels'=>[],
				'datasets'=>
				[
					0=>[
						'fill'	   =>false,
						'pointBackgroundColor'=>"rgba(255,0,0,0)",
						'pointBorderColor'	  =>"rgba(255,0,0,0)",
						'pointStrokeColor'	  =>"rgba(255,0,0,0)",
						'data'=>[]
					   ],
					1=>[
						'backgroundColor'	  =>"rgba(165,0,0,.5)",
						'pointBorderColor'	  =>"rgba(165,0,0,1)",
						'pointBackgroundColor'=>"rgba(165,0,0,1)",
						'pointStrokeColor'	  =>"rgba(165,0,0,1)",
						'pointRadius'=>1,
						'pointHitRadius'=>5,
						'data'=>[]
					   ],
				],
			],
			'options'=> [
				'responsive'=> false,
				'animation'=> false,
				'legend'=> false,
				'scales'=> [
					'xAxes'=> [
						[
							'ticks'=>['maxTicksLimit'=>10] ,
						]
							  ]],

						],
		];

	$userRepHistory=demosphere_opinion_user_reputation_history_cache($cuser);
	$weights=db_one_col('SELECT time,weight FROM Opinion WHERE isEnabled=1 AND userId=%d LIMIT 200',$cuser->id);
	$totWeight=0;
	$lastYear=strtotime('last year');
	foreach($userRepHistory as $time=>$userRep)
	{
		$totWeight+=val($weights,$time,0);
		$repChart['data']['datasets'][0]['data'][]=min(1,$totWeight/$demosphere_config['opinions_reputation_buildup']);
		$repChart['data']['datasets'][1]['data'][]=$userRep;
		$repChart['data']['labels'][]=strftime('%e/%m'.($time<$lastYear ? '/%y' : ''),$time);
	}

	$currentPage->addJsVar('reputationChartData',$repChart);

	$userReputation=demosphere_opinion_user_reputation($cuser);

	$ratings=db_one_col('SELECT rating FROM Opinion WHERE isEnabled=1 AND userId=%d',$cuser->id);
	$histogram=demosphere_histogram($ratings,70,range(-5, +5));

	$pager=new Pager(['itemName'=>t('opinions')]);
	$opinions=Opinion::fetchList('SELECT SQL_CALC_FOUND_ROWS * FROM Opinion WHERE userId=%d ORDER BY time ASC'.$pager->sql(),$cuser->id);
	$pager->foundRows();

	// reverse time order 
	uasort($opinions,function($a,$b)
		   {
			   if ($a->time == $b->time) {return 0;}
			   return ($a->time > $b->time) ? -1 : 1;
		   });


	$nbOpinions=db_result('SELECT COUNT(*) FROM Opinion WHERE userId=%d',$cuser->id);


	$reputationData=[];
	foreach($opinions as $op)
	{
		$data=$op->calcReputation(true);
		$reputationData[$op->id]=$data;
	}
	$isSingleUser=true;
	require_once 'demosphere-date-time.php';
	return template_render('templates/demosphere-opinions-of-user.tpl.php',
						   [compact('isEventAdmin','nbOpinions','opinions','pager','cuser','reputationData','isSingleUser',
									'userReputation','histogram'),
						   ]);
}

//! User reputation history is a bit slow to compute, so we cache results and only compute the last month.
//! The cache is refreshed monthly by cron in demosphere_opinion_user_reputation_history_cache_update_all(true)
function demosphere_opinion_user_reputation_history_cache($cuser)
{
	global $demosphere_config;

	$recent=time()-3600*24*30;
	$userReps=variable_get($cuser->id,'opinion_user_reputation_history_cache',[]);
	$userReps=array_filter($userReps,function($v)use($recent){return $v<$recent;},ARRAY_FILTER_USE_KEY);
	$recent=min($recent,count($userReps) ? max(array_keys($userReps)) : 0);
	$recentTimes=db_one_col('SELECT time FROM Opinion WHERE userId=%d AND time>%d ORDER BY time ASC',$cuser->id,$recent);
	foreach($recentTimes as $time)
	{
		$userReps[$time]=demosphere_opinion_user_reputation($cuser,$time+1);
	}
	variable_set($cuser->id,'opinion_user_reputation_history_cache',$userReps);
	return $userReps;
}

//! Called from cron
function demosphere_opinion_user_reputation_history_cache_update_all($onlyMonthly=false)
{
	if($onlyMonthly && variable_get('last-update','opinion_user_reputation_history_cache')>time()-3600*24*30){return;}

	$uids=db_one_col('SELECT id FROM User WHERE EXISTS (SELECT * FROM Opinion WHERE Opinion.userId=User.id)');
	foreach($uids as $uid)
	{
		$cuser=User::fetch($uid);
		variable_delete($cuser->id,'opinion_user_reputation_history_cache');
		demosphere_opinion_user_reputation_history_cache($cuser);
	}
	variable_set('last-update','opinion_user_reputation_history_cache',time());
}


//! UNUSED: computes which users are most active
//! We wanted to use this as a factor for reputation, but it does not seem to be reliablr or usefull.
function demosphere_opinion_relative_activity($start,$end=false)
{
	$res=[];
	$interval=7*24*3600;
	$now=time();
	for($ts=$start;$ts<($end===false ? $now : $end);$ts+=$interval)
	{
		$usersActivity=db_one_col("SELECT userId,COUNT(*) AS ct FROM Opinion INNER JOIN User on User.id=Opinion.userId WHERE ".
								  "time>%d AND time<%d AND role NOT IN (%d,%d) GROUP BY userId ORDER BY ct DESC",
								  $ts-3600*24*30,$ts,User::roleEnum('admin'),User::roleEnum('moderator'));
		$rel=[];
		$top=max(1,floor(count($usersActivity)/4));
		$topAvg=0;
		$i=0;
		foreach($usersActivity as $userId=>$ct)
		{
			// average of top 25% users serves as reference for others.
			if(($i++)<$top){$rel[$userId]=1;$topAvg+=$ct/$top;}
			else
			{
				$rel[$userId]=$ct/$topAvg;
			}
		}
		$res[$ts]=$rel;
	}
	return $res;
}

//! Displays a list or opinions for moderators, sorted by date
function demosphere_opinions()
{
	global $currentPage,$user;
	$currentPage->addCssTpl('demosphere/css/demosphere-opinion.tpl.css');

	$currentPage->title=t('Opinions');

	$pager=new Pager(['itemName'=>t('opinions'),'defaultItemsPerPage'=>60]);
	$opinions=Opinion::fetchList('SELECT SQL_CALC_FOUND_ROWS * FROM Opinion ORDER BY time DESC '.$pager->sql());
	$pager->foundRows();
	$reputationData=[];

	foreach($opinions as $op)
	{
		$data=$op->calcReputation(true);
		$reputationData[$op->id]=$data;
	}

	$ratings=db_one_col('SELECT rating FROM Opinion WHERE isEnabled=1');
	$histogram=demosphere_histogram($ratings,70,range(-5, +5));
	$isSingleUser=false;
	$isEventAdmin=$user->checkRoles('admin','moderator');

	require_once 'demosphere-date-time.php';			
	return '<h1 class="page-title">'.t('All opinions').'</h1>'.
		$histogram.
		template_render('templates/demosphere-opinions-list.tpl.php',
						[compact('opinions','reputationData','pager','isSingleUser','isEventAdmin'),
						]);	
}


function demosphere_opinion_user_stars($cuser)
{
	global $currentPage,$user;
	if($user->id!==$cuser->id && !$user->checkRoles('admin','moderator')){fatal('demosphere_opinion_user_stars: bug');}
	$currentPage->addCssTpl('demosphere/css/demosphere-opinion.tpl.css');
	$rep=demosphere_opinion_user_reputation($cuser);
	return '<span class="stars-empty" title="'.round($rep*100).'"><span class="stars-full" style="width:'.round(($rep)*55).'px"></span></span>';
}

//! Computes the reputation of a user.
//! The reputation of a user is based on the weighted average reputation of all of his opinions.
//! Admins and moderators allways have a reputation of 1.
//! User reputations are limited to values between 0 and 1.
function demosphere_opinion_user_reputation($cuser,$before=false)
{
	global $demosphere_config;
	if($cuser->checkRoles('admin','moderator')){return 1;}
	list($totRep,$totWeight)=array_values(db_array('SELECT SUM(reputation*weight),SUM(weight) FROM Opinion WHERE userId=%d AND time<%d AND isEnabled=1',
											   $cuser->id,$before===false ? time() : $before));
	if($totWeight==0){return 0;}
	//return $totRep/$totWeight;

	$res=$totRep/$totWeight;
	$res=demosphere_opinion_user_reputation_correction_fct($res);
	return ($res)*min(1,$totWeight/$demosphere_config['opinions_reputation_buildup']);
}

//! A simple hack to make reputation more clear-cut (closer to 0 and 1)
function demosphere_opinion_user_reputation_correction_fct($r)
{
	require_once 'Opinion.php';
	// First stop [.17,0] brings to zero someone that would always say yes (rating=2) (check YesMan simulation)
	// The next stops are a sigmoid that make reputation more clear-cut 
	return linear_by_parts_fct($r,[[0,0],[.17,0],[.5,.4],[.6,.8],[1,1]]);
}

//! Returns reputation for all contributors,  mods and admins.
//! This is usefull for performance when you need to compute lots of reputations. Otherwise use demosphere_opinion_user_reputation()
function demosphere_opinion_all_users_reputation()
{
	global $demosphere_config;
	$userReps=db_arrays_keyed('SELECT userId,SUM(reputation*weight) AS totRep,SUM(weight) AS totWeight FROM Opinion WHERE isEnabled=1 GROUP BY userId');
	$mods=db_one_col('SELECT id FROM User WHERE role IN (1,2)');// 1=>admin 2=>moderator 
	$res=[];
	foreach($userReps as $uid=>$userRep)
	{
		extract($userRep);
		if($totWeight==0){$res[$uid]=0;continue;}
		// A user's reputation is basically just the average reputation of all options
		$reputation=$totRep/$totWeight;
		$reputation=demosphere_opinion_user_reputation_correction_fct($reputation);
		// Decrease reputation for new-commers
		$reputation*=min(1,$totWeight/$demosphere_config['opinions_reputation_buildup']);
		$res[$uid]=$reputation;
	}
	foreach($mods as $mod){$res[$mod]=1;}
	return $res;
}


//! Called from Event::save()
function demosphere_opinions_event_save($event)
{
	demosphere_opinions_update_all_reputations_for_event($event);
}

//! Move opinions from old, duplicate event, to new one
//! Called from event edit form when a duplicate is detected
function demosphere_opinions_duplicate($oldEventId,$newEventId)
{
	$newUids=db_one_col('SELECT userId FROM Opinion WHERE eventId=%d',$newEventId);
	$oldOpinions=Opinion::fetchList('WHERE eventId=%d',$oldEventId);
	foreach($oldOpinions as $opinion)
	{
		if(array_search($opinion->userId,$newUids)!==false)
		{
			$opinion->isEnabled=false;
		}
		else
		{
			$opinion->eventId=$newEventId;
			$opinion->text.="\n".t('[Opinion moved from duplicate event]');
		}
		$opinion->save();
	}
	demosphere_opinions_update_all_reputations_for_event($oldEventId);
	demosphere_opinions_update_all_reputations_for_event($newEventId);
}


//! Updates the "reputation" field in each Opinion of a given event.
//! Called from demosphere_opinions_event_save()
function demosphere_opinions_update_all_reputations_for_event($eventId)
{
	//**** Update reputation for all users that have opinions on $event.
	$opinions=Opinion::fetchList('WHERE eventId=%d AND isEnabled=1',$eventId);
	foreach($opinions as $op)
	{
		$oldRep	  =$op->reputation;
		$oldWeight=$op->weight;
		list($newRep,$newWeight)=$op->calcReputation();
		if(abs($newRep	 -$oldRep	)<.001 && 
		   abs($newWeight-$oldWeight)<.001	  ){continue;}
		//! cannot use $op->save() : it would be recursive.
		db_query('UPDATE Opinion SET reputation=%f,weight=%f WHERE id=%d',$newRep,$newWeight,$op->id);
	}
}

//! Returns the weighted average rating for an event (and some other info).
//! Ratings from each user are weighed by the user's reputation.
//! $userReputations is only for perfomance ($userReputations=demosphere_opinion_all_users_reputation())
function demosphere_opinion_event_rating_average($eventId,$userReputations=false)
{
	$display='( ';
	$ratings=db_one_col('SELECT userId,rating FROM Opinion WHERE eventId=%d AND isEnabled=1 ORDER BY time DESC',$eventId);
	$total=0;
	$totalWeight=0;
	foreach($ratings as $userId=>$rating)
	{
		if($userReputations===false)
		{
			$userReputation=demosphere_opinion_user_reputation(User::fetch($userId));
		}
		else
		{
			$userReputation=$userReputations[$userId];
		}
		$total+=$userReputation*$rating;
		$totalWeight+=$userReputation;
		$display.=round($userReputation*100).' x '.$rating.' + ';
	}
	$average=$totalWeight>0 ? $total/$totalWeight : 0;
	$display=preg_replace('@ \+ $@','',$display);
	$display.=' ) / '.round(100*$totalWeight);

	return ['average'=>$average,
			'nbRatings'=>count($ratings),
			'totalWeight'=>$totalWeight,
			'display'=>$display,
			];
}

//! only for discrete values
function demosphere_histogram($data,$height,$values=[])
{
	$hist=array_fill_keys($values,0);
	foreach($data as $v){$hist[$v]=val($hist,$v,0)+1;}
	ksort($hist);
	$max=max($hist);
	if($max===0){return '';}
	$out='';
	$out.='<table class="histogram">';
	$out.='<tr class="count">';
	foreach($hist as $v=>$count)
	{
		$out.='<td><div class="bar" title="'.$count.'" style="height:'.round($height*$count/$max).'px;"></div></td>';
	}
	$out.='</tr>';
	$out.='<tr class="labels">';
	foreach($hist as $v=>$count)
	{
		$out.='<td>'.ent($v).'</td>';
	}
	$out.='</tr>';
	$out.='</table>';
	return $out;
}

function demosphere_opinion_cron()
{
	demosphere_opinion_email_all();
	demosphere_opinion_user_reputation_history_cache_update_all(true);
}

function demosphere_opinion_email_all()
{
	$cusers=User::fetchList('WHERE contributorLevel!=0');
	foreach($cusers as $cuser)
	{
		demosphere_opinion_email($cuser);
	}
}

function demosphere_opinion_email($cuser)
{
	global $demosphere_config,$base_url;

	// *** Check if we can send email
	$opinionOptions=val($cuser->data,'opinion-options',[]);
	$now=time();
	$howOften=val($opinionOptions,'email-how-often',0);
	$lastSent=val($opinionOptions,'email-last-sent',0);
	$lastOpinion=(int)db_result('SELECT MAX(time) FROM Opinion WHERE userId=%d',$cuser->id);
	if($howOften===0){return;}
	if($now<$lastSent+$howOften*3600*24){return;}
	// Don't send anymore emails after two emails without any opinions.
	if($lastOpinion!==0 && $now>$lastOpinion+$howOften*3600*24*2.5){return;}
	if($cuser->email===''){return;}
	
	// *** Build email body
	$events=Event::fetchList('WHERE startTime>%d AND '.
							 'moderationStatus=%d AND '.
							 'needsAttention IN (%d,%d) AND '.
							 'NOT EXISTS (SELECT * FROM Opinion WHERE Opinion.userId=%d AND Opinion.eventId=Event.id)',
							 $now,
							 Event::moderationStatusEnum('waiting' ),
							 Event::needsAttentionEnum('publication-request'),
							 Event::needsAttentionEnum('publication-request-waiting'),
							 $cuser->id);
	if(count($events)===0){return;}
	$emailBody='';
	$emailBody='<p>'.ent(t('Please give your opinion on the following events:')).'</p>';
	$emailBody.='<ul>';
	require_once 'demosphere-date-time.php';
	foreach($events as $event)
	{
		$emailBody.='<li>';
		$emailBody.=ent(demos_format_date('full-date-time',$event->startTime)).':<br/>';
		$emailBody.='<a href="'.ent($event->url()).'">';
		$emailBody.=$event->safeHtmlTitle();
		$emailBody.='</a>';
		$emailBody.='</li>';
	}
	$emailBody.='</ul>';
	$emailBody.='<p><a href="'.$base_url.'/user/'.$cuser->id.'/opinion-options">'.t('Change your options or unsubscribe').'</a></p>';

	// *** Actually send email
	require_once 'lib/class.phpmailer.php';
	$mail=new PHPMailer(false);
	$mail->CharSet="utf-8";
	$mail->SetFrom($demosphere_config['contact_email']);
	$mail->AddAddress($cuser->email);
	$mail->Subject=t('!site_name: !nb events are ready for your opinion',['!site_name'=>$demosphere_config['site_name'],
																		  '!nb'=>count($events)]);
	$mail->MsgHTML($emailBody);
	$mail->AltBody = Html2Text::convert($emailBody);
	if($mail->Send())
	{
		$cuser->data['opinion-options']['email-last-sent']=$now;
		$cuser->save();
	}
}


function demosphere_opinion_options($uid)
{
	global $user;
	$isEventAdmin=$user->checkRoles('admin','moderator');
	if(!$isEventAdmin)
	{
		if(((int)$uid)!==$user->id){dlib_permission_denied_403();}
		if($user->contributorLevel==0){dlib_permission_denied_403();}
	}
	$cuser=$uid===$user->id ? $user : User::fetch($uid);

	$opinionOptions=val($cuser->data,'opinion-options',[]);

	$items=[];
	$items['top']=['html'=>'<h1 class="page-title">'.t('Opinion options').'</h1>'];
	$items['email-how-often']=['type'=>'select',
							   'title'=>t('Email me when events need my opinion'),
							   'options'=>[0=>t('No'),
										   1=>t('Max once per day'),
										   2=>t('Max once every 2 days'),
										   5=>t('Max once every 5 days'),
										   7=>t('Max once a week'),],
							   'default-value'=>val($opinionOptions,'email-how-often',0),
							   'description'=>t('The emails will automatically stop if you do not give any opinions.'),
							   ];
	$items['save']=['type'=>'submit',
					'value'=>t('Save'),
					'submit'=>function($items) use ($cuser)
					{
						$cuser->data['opinion-options']['email-how-often']=(int)$items['email-how-often']['value'];
						$cuser->save();
					}];
	require_once 'dlib/form.php';
	return form_process($items);
}


//! Display reputation value, with colors and up/down arrow
function demosphere_opinion_display_reputation($op,$repData)
{
	$isMod=$op->useUser()->checkRoles('admin','moderator');
	$modProblem=$isMod && $repData['publishingDecisionMadeByMods'] && $op->rating!==0 && (($op->rating>0)!==($op->useEvent()->status>0));
	return '<span class="opinion-reputation '.
		($modProblem  ? 'mod-problem' : '').' '. 
		($repData['weight']>0  ? 'rep-'.$repData['reputation'] : '').' '. 
		($repData['weight']>=5 || $modProblem ? 'big-weight' : '').'">'.
		($isMod ? ($modProblem ? '!' : '') : round($repData['weight'],1)).
		'</span>';
}


//! Mathematical function defined by a set of straight lines connecting points.
//! At least 2 points are needed. X coord of points in $parts must be strictly ascending.
//! Example: $parts=[[0,0],[1,1],[3,-1]] (x=0 y=0, x=1 y=1, x=3 y=-1)
function linear_by_parts_fct($x,$parts)
{
	for($i=1;$i<count($parts);$i++)
	{
		if($x<$parts[$i][0] || $i==count($parts)-1)
		{
			return $parts[$i  ][1]*($x-$parts[$i-1][0])/($parts[$i	][0]-$parts[$i-1][0])+
				   $parts[$i-1][1]*($x-$parts[$i  ][0])/($parts[$i-1][0]-$parts[$i	][0]);
		}
	}
}


?>