<?php

//! Interface that allows user to select part of a PDF page or image and perform PDF text select or OCR on that selection.
function docconvert_pdf_select()
{
	global $base_url,$currentPage,$docconvert_config,$dlib_config;

	$currentPage->addCssTpl('demosphere/docconvert/docconvert-pdf-select.tpl.css');
	$currentPage->addCss('lib/jquery-ui-1.12.1.custom/jquery-ui.min.css');
	$currentPage->addJs('lib/jquery.js');
	$currentPage->addJs('lib/jquery-ui-1.12.1.custom/jquery-ui.js');
	$currentPage->addJs('demosphere/docconvert/docconvert-pdf-select.js');


	$url=$_GET['url'] ?? false;
	if($url===false || !preg_match('@^https?://@',$url)){dlib_bad_request_400('invalid url');}
	$isPdf=($_GET['type'] ?? false) !== 'image';

	$id='pdf-select-'.substr(hash('SHA256',$url.':'.$dlib_config['secret']),0,32);
	$dirname=$docconvert_config['tmp_dir'].'/'.$id;
	$download=$dirname.'/download';
	$src=$dirname.'/src'.($isPdf ? '.pdf' : '.png');
	if(!file_exists($dirname) || !file_exists($src))
	{
		if(!file_exists($dirname)){mkdir($dirname);}
		require_once 'dlib/download-tools.php';
		$ret=dlib_curl_download($url,$download);
		if($ret===false || !file_exists($download))
		{
			rmdir($dirname);
			dlib_bad_request_400('document download failed');
		}
		if($isPdf){rename($download,$src);}
		else
		{
			exec('convert '.escapeshellarg($download).' '.escapeshellarg($src),$unused,$ret);
			if($ret || !file_exists($src)){fatal('src convert failed');}
		}
	}

	$out='';
	$out='<div id="select-pdf-outer">';
	if($isPdf)
	{
		require_once 'dlib/filter-xss.php';
		exec('pdfinfo '.escapeshellarg($src),$output,$ret);
		if($ret){fatal('pdfinfo failed');}
		$out.='<div id="document-info">';
		$out.=''.ent(t('PDF file information:')).' <a href="'.ent(u($url)).'">[PDF]</a> <span id="pdf-info-open">[+]</span>';
		$out.='<pre id="pdf-info">'.ent(implode($output,"\n")).'</pre>';
		$out.='</div>';
	}

	// Text output box
	$out.='<div id="text-output-box"><h3>'.ent(t('Extracted text:')).'</h3>';
	$out.='<div><textarea id="text-output" spellcheck="true"></textarea></div>';
	$out.='<input type="hidden" id="pdf-id" name="pdf-id" value="'.ent($id).'"/>';
	$out.='<input type="button" id="ocr"    name="ocr"    value="OCR !"/>';
	$out.=' Ocr-program:<select id="ocrProg" name="ocrProg">';
	$out.='<option selected="selected" >tesseract</option>';
	$out.='<option                     >cuneiform</option>';
	$out.='</select>';
	$out.='</div>';

	if($isPdf)
	{
		if(!file_exists($dirname.'/page-0.png'))
		{
			exec('convert -density 72 '.escapeshellarg($src).' '.escapeshellarg($dirname.'/page.png'),
				 $output,$ret);
			if(file_exists($dirname.'/page.png')){rename($dirname.'/page.png',$dirname.'/page-0.png');}
		}

		$pages=[];
		for($i=0;$i<20;$i++)
		{
			$page='page-'.$i.'.png';
			if(file_exists($dirname.'/'.$page))
			{
				$info=getimagesize($dirname.'/'.$page);
				$pages[]=['width'=>$info[0],
						  'height'=>$info[1],
						  'name'=>$id.'/'.$page];
			}
		}

		$out.='<p class="help">'.
			ent(t('Move the red rectangle to fit the text you want to extract.')).'<br/>'.
			ent(t('Try OCR button if text is incorrect.')).
			'</p>';
	}
	else
	{
		$out.='<p class="help">'.
			ent(t('Move the red rectangle to fit the text you want to extract.')).'<br/>'.
			ent(t('Then press on the OCR button.')).
			'</p>';
		$info=getimagesize($src);
		$pages=[['width'=>$info[0],
				 'height'=>$info[1],
				 'name'  =>$id.'/src.png']];
	}
	
	$out.='<div id="pdf-pages">';
	foreach($pages as $n=>$page)
	{
		if(count($pages)>1){$out.='<span class="pdf-page-num">'.t('Page !n',['!n'=>($n+1).'/'.count($pages)]).'</span>';}
		$out.='<div class="pdf-page-wrap"><img class="pdf-page" '.
			'src="'   .$base_url.'/docconvert/pdf-select-image?image='.urlencode($page['name']).'" '.
			'data-orig-width= "'.intval($page['width' ]).'" '.
			'data-orig-height="'.intval($page['height']).'" /></div>';
	}
	$out.='</div>';
	$out.='</div>';

	return $out;
}

//! Serve PDF page images or src image that are in private tmp directory.
function docconvert_pdf_select_image()
{
	global $docconvert_config;
	$image=$_GET['image'] ?? '';
	if(!preg_match('@^pdf-select-[a-f0-9]+/(src|page-[0-9]+)[.]png$@',$image)){dlib_bad_request_400('invalid image');}
	$file=$docconvert_config['tmp_dir'].'/'.$image;
	if(!file_exists($file)){dlib_not_found_404();}
	header('Content-Type: image/png');
	readfile($file);
	exit;
}

//! Ajax call for text inside selection box.
//! Can be a request for pdf text select (pdf only) or OCR (pdf or image).
function docconvert_pdf_select_selection()
{
	global $docconvert_config;
	$id=$_GET['id'] ?? '';
	if(!preg_match('@^pdf-select-[a-z0-9]+$@',$id)){dlib_bad_request_400('invalid id');}
	$dirname=$docconvert_config['tmp_dir'].'/'.$id;
	if(!file_exists($dirname)){dlib_not_found_404();}
	$src=$dirname.'/src.pdf';
	$isPdf=file_exists($src);
	if(!$isPdf){$src=$dirname.'/src.png';}
	$page_nb=intval($_GET['page_nb']);
	$x      =intval($_GET['x']);
	$y      =intval($_GET['y']);
	$width  =intval($_GET['width']);
	$height =intval($_GET['height']);
	$isOcr  =$_GET['ocr']==='true';
	$ocrProg=$_GET['ocrProg'];

	$lang=dlib_get_locale_name();

	if($ocrProg!=='tesseract' && $ocrProg!=='cuneiform'){dlib_bad_request_400('bad ocrProg');}

	if(!$isPdf && !$isOcr){dlib_bad_request_400('not pdf or ocr');}

	if(!$isOcr)
	{
		exec('pdftotext -f '.intval($page_nb+1).' -l '.intval($page_nb+1).
			 ' -x '.intval($x).' -W '.intval($width).' -y '.intval($y).' -H '.intval($height).' '.
			 escapeshellarg($src).' '.
			 escapeshellarg($dirname.'/selected.txt'));
	}
	else
	{
		if($isPdf)
		{
			// convert image portion
			$ocrResolution=600;
			dlib_logexec('convert -flatten -crop '.
						 round($width *$ocrResolution/72).'x'.
						 round($height*$ocrResolution/72).'+'.
						 round($x     *$ocrResolution/72).'+'.
						 round($y     *$ocrResolution/72).' '.
						 '-density 600 '.
						 ' +repage '.escapeshellarg($src.'['.intval($page_nb).']').' '.
						 escapeshellarg($dirname.'/ocr-src.png'),
						 $docconvert_config['log']);
		}
		else
		{
			dlib_logexec('convert -flatten -crop '.
						 round($width ).'x'.
						 round($height).'+'.
						 round($x	  ).'+'.
						 round($y	  ).' '.
						 ' +repage '.escapeshellarg($src).' '.
						 escapeshellarg($dirname.'/ocr-src.png'),
						 $docconvert_config['log']);
		}
		// do ocr
		if($ocrProg==='tesseract')
		{
			$tesseractLangs=['el'=>'ell','en'=>'eng','es'=>'spa','fr'=>'fra','gl'=>'glg','pt'=>'por','eu'=>'eus','de'=>'deu',];
			$ocrLang=$tesseractLangs[$lang] ?? false;
			if($ocrLang===false){$ocrLang='eng';dlib_log_error("docconvert_pdf_select: unknown tesseract language :".$lang);}
			dlib_logexec('tesseract '.$dirname.'/ocr-src.png '.$dirname.'/selected -l '.$ocrLang.'',
						 $docconvert_config['log']);
		}
		else
		{
			$cuneiformLangs=['bg'=>'bul','cs'=>'cze','da'=>'dan','nl'=>'dut','en'=>'eng','et'=>'est','fr'=>'fra','de'=>'ger','hr'=>'hrv',
							 'hu'=>'hun','it'=>'ita','lv'=>'lav','lt'=>'lit','pl'=>'pol','pt'=>'por','ro'=>'rum','ru'=>'rus','sl'=>'slv',
							 'es'=>'spa','sr'=>'srp','sv'=>'swe','tr'=>'tur','uk'=>'ukr',];
			$ocrLang=$cuneiformLangs[$lang] ?? false;
			if($ocrLang===false){$ocrLang='eng';dlib_log_error("docconvert_pdf_select: unknown cuneiform language :".$lang);}
			dlib_logexec('cuneiform -l '.escapeshellarg($ocrLang).' '.
						 '-o '.escapeshellarg($dirname.'/selected.txt').' '.
						 escapeshellarg($dirname.'/ocr-src.png'),
						 $docconvert_config['log']);
		}
	}
	if(file_exists($dirname.'/selected.txt'))
	{
		$text=file_get_contents($dirname.'/selected.txt');
		unlink($dirname.'/selected.txt');
	}
	else{$text=$isOcr ? 'OCR failed' : 'select failed';}
	return ['text'=>$text,'debug'=>[$isOcr]];
}

?>