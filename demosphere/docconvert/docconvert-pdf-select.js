// namespace
(function() {
window.docconvert_pdf_select = window.docconvert_pdf_select || {};
var ns=window.docconvert_pdf_select;// shortcut

$(document).ready(function()
{
    $('#pdf-info-open').mousedown(function()
	{
		$('#pdf-info').slideToggle();
	});

	var rectangle=$('<div id="rectangle"></div>');
	$('.pdf-page-wrap').first().addClass('active');
 	$('.pdf-page-wrap .pdf-page').one('load',function()
    {
 		// We need to manualy reset wrapper size 
 		// when image long to load. Otherwize it is 0 width.
		// For large images, screen size might be affected by CSS max-width.
 		var img=$(this);
 		img.parent().width( img.width());
 		img.parent().height(img.height());
		if($('.pdf-page-wrap').index(img.parent())===0)
		{
 			$('.pdf-page-wrap').first().append(rectangle);
 			make_resizable_and_draggable();
		}
 	});
	$('.pdf-page-wrap .pdf-page').each(function(){if(this.complete){$(this).trigger('load');}});

	if($('#text-output-box').offset().left>700){$('#text-output-box').css('left',700);}

	$('.pdf-page').mousedown(function(e)
	{
		if(e.which!==1){return;}
		e.preventDefault();

		// Click on inactive page, makes it active
		var wrap=$(this).parents('.pdf-page-wrap').first();
		if(!wrap.hasClass('active'))
		{
			$(rectangle).resizable( "destroy" );
			$(rectangle).draggable( "destroy" );
			$('.pdf-page-wrap.active').removeClass('active');
			wrap.addClass('active');
			wrap.append(rectangle);
			make_resizable_and_draggable();
		}

		// This is a click on image, not on rectangle : Center rectangle on mouse
		var x=e.pageX;
		var y=e.pageY;
		var w=$('#rectangle').width();
		var h=$('#rectangle').height();
		var ro=$('#rectangle').offset();
		var space=20;
		// leave a margin around the rectangle where click does not move it.
		if(x<ro.left-space || x>ro.left+w+space ||
		   y<ro.top -space || y>ro.top+h +space   )
		{
			x-=w/2;
			y-=h/2;
			x=Math.max(x,$(this).offset().left);
			y=Math.max(y,$(this).offset().top);
			x=Math.min(x,$(this).offset().left+$(this).width() -w);
			y=Math.min(y,$(this).offset().top +$(this).height()-h);
			$('#rectangle').offset({left: x,top: y});
		}

		update_text();
	});

	$('#ocr').click(function(e)
	{
		update_text(true);
	});

});

function make_resizable_and_draggable()
{
	$(rectangle).resizable(
	{ 
		containment: "parent",
		handles: "all",
		minWidth: 35,
		minHeight: 35,
		stop: function(e,ui){update_text();}
	});
	$('#rectangle').draggable(
	{
		containment: "parent",
		stop: function(e,ui){update_text();}
	});
}


function update_text(ocr)
{
	var rectangle=$('#rectangle');
	var wrap=rectangle.parents('.pdf-page-wrap')[0];
	var page_nb=$(wrap).prevAll('.pdf-page-wrap').length;
	var useOcr=(typeof ocr!=='undefined' && ocr);
	if(useOcr){$('#text-output-box').append('<span id="wait">please wait...</span>');}
	var img=$(wrap).find('img.pdf-page');
	var resizeFactor=img.attr('data-orig-width')/img.width(); // max-width can change width
	$.get(base_url+'/docconvert/pdf-select-selection',
		   {
			   id:$('#pdf-id').val(),
			   page_nb: page_nb,
			   x:      Math.round(resizeFactor*rectangle.position().left),
			   y:      Math.round(resizeFactor*rectangle.position().top),
			   width:  Math.round(resizeFactor*rectangle.width()),
			   height: Math.round(resizeFactor*rectangle.height()),
			   ocr: useOcr,
			   ocrProg: $('#ocrProg').val()
		   },
		  function(data)
		  {
			  //console.log(data);
			  $('#text-output').val(data.text);
			  $('#wait').remove();
		  },'json');
}

// end namespace wrapper
}());
