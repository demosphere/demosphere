// namespace
(function() {
window.docconvert = window.docconvert || {};
var ns=window.docconvert;// shortcut

//! ***********************************************
//! ****  Docconvert : document (pdf, office, ...) management 
//! ***********************************************

//! Docconvert adds document management functionallity to demosphere-htmledit
//! It recognizes documents (pdf, office, ...) and displays a menu on the right that provides actions on them.
//! There can be several TinyMce editors that use docconvert on the same pafe (ex: hdiff), so all functionally that is specific to an editor is 
//! is bound to the "editor" variable inside the docconvert_init(editor) function.

//! This should be called when DOM is finished loading.
function docconvert_init(editor)
{
	docconvert_log('docconvert_init');

	// Common shared variables for this editor. 
	var edocument=$(editor.getBody().ownerDocument);// dom document (inside frame)
	var ebody=$(editor.getBody());
	var areaRight=editor.demosphereHtmleditorAreaRight;

	// exports
	editor.docconvert={};
	editor.docconvert.rebuild_content_info=docconvert_rebuild_content_info;
	editor.docconvert.can_finish=docconvert_can_finish;
	editor.docconvert.unselect_doc=docconvert_unselect_doc;
	editor.docconvert.upload_success=docconvert_upload_success;
	editor.docconvert.upload_failed=docconvert_upload_failed;
	editor.docconvert.currentDoc=false;

	docconvert_setup();

	function docconvert_setup()
	{
		// Create the main docconvert menu
		areaRight.append(
			'<div class="docconvert-info">'+
				'<h2>'+
				'<a href="javascript:void(0);" class="docconvert-cycle prev" title="'+t('prev')+'">&lt;</a>'+
				'<a href="javascript:void(0);" class="docconvert-cycle next" title="'+t('next')+'">&gt;</a>'+
				'<span>'+t('documents')+'</span>'+
				'<a href="javascript:void(0);" class="docconvert-refresh" title="'+t('refresh')+'"></a></h2>'+
				'<p class="docconvert-upload"></p>'+
				'<div class="docconvert-current-doc-menu">'+
				'</div>'+
				'</div>');

		areaRight.find('.docconvert-cycle'        ).click(docconvert_cycle);
		areaRight.find('.docconvert-refresh'		).click(docconvert_refresh);

		docconvert_upload_file_setup();

		// add stylesheet to text inside editor frame
		edocument.find("head").append('<link rel="stylesheet" type="text/css" '+
										'href="'+base_url+'/'+docconvert.editor_css+'">');

		// find all document links
		
		// FIXME : most of this should go to demosphere-htmledit.js
		// (problem: performance concerns, this is called often)
		editor.on('change',function(ed, l)
		{
			// remove dangling empty composite docs after cut/delete
			ebody.find('.composite-doc').each(function(){if($.trim($(this).text())==='' && $(this).find('img').length==0){$(this).remove();}});
			if(!$.browser.msie){ebody.find('a:empty,h2:empty,h3:empty,h4:empty').remove();}
			// remove dangling composite-doc-container after a cut 
			ebody.find('.composite-doc-container').not(':has(.composite-doc)').remove();
			// remove (completely) empty paragraphs
			if(!$.browser.msie){ebody.find('p:empty').remove();}
			// remove stray image handles
			ebody.find('.image-handles').each(function()
											  {
												  if($(this).prev('img').length===0){$(this).remove();}
											  });

			if(ebody.find('hr').not('.hr-with-handle').length!==0){editor.demosphere_htmledit.add_hr_handles();}

			editor.demosphere_htmledit.add_empty_paragraphs();

			if(editor.docconvert.currentDoc!==false && 
			   $(editor.docconvert.currentDoc).parents('body').length==0)
			{
				docconvert_unselect_doc();
			}
		});

		// Click on docconvert link shows document in right-col document pane
		edocument.on('mousedown','.docconvert-doc',function(e)
		{
			if(e && e.which!=1){return;}
			docconvert_select_doc($(this));
		});

		// ***** Event handlers for document handles 

		edocument.on('mousedown','.composite-doc-handle-edit',function(e)
		{
			if(e.which!=1){return;}
			e.preventDefault();e.stopPropagation();
			var doc=$(this).parents('.composite-doc-is-setup').first();
			if(doc.length==0){return;}
			if(doc.hasClass('mceNonEditable'))
			{doc.removeClass("mceNonEditable").removeAttr('data-mce-contenteditable');}
			else
			{doc.addClass(   "mceNonEditable").attr(      'data-mce-contenteditable','false');}
		});

		edocument.on('mousedown','.composite-doc-handle-down',function(e)
		{
			if(e.which!=1){return;}
			e.preventDefault();e.stopPropagation();
			editor.demosphere_htmledit.move_down($(this).parents('.composite-doc-container'));
		});

		edocument.on('mousedown','.composite-doc-handle-resize',function(e)
		{
			if(e.which!=1){return;}
			e.preventDefault();e.stopPropagation();
			var doc=$(this).parents('.composite-doc-is-setup').first();
			if(doc.length==0){return;}
			doc.toggleClass("floatRight");
		});

		edocument.on('mousedown','.composite-doc-handle-close',function(e)
		{
			if(e.which!=1){return;}
			e.preventDefault();e.stopPropagation();
			var doc=$(this).parents('.composite-doc-is-setup').first();
			if(doc.length==0){return;}
			doc.remove();
			if(editor.docconvert.currentDoc!==false && 
			   $(editor.docconvert.currentDoc).parents('body').length==0)
			{
				docconvert_unselect_doc();
			}
		});
	}

	//! Rebuilds info about docconvert docs (types).
	//! Called when user clicks the refresh button in the top part of the menu.
	//! Actual work is done in docconvert_find_docconvert_docs()
	function docconvert_refresh(e)
	{
		
		ebody.find('.docconvert-doc'        ).removeClass('docconvert-doc');
		ebody.find('.docconvert-type-pdf'   ).removeClass('docconvert-type-pdf');
		ebody.find('.docconvert-type-office').removeClass('docconvert-type-office');
		ebody.find('.docconvert-type-image' ).removeClass('docconvert-type-image');
		ebody.find('span' ).removeAttr('data');
		docconvert_find_docconvert_docs(e,true);
	}

	//! Next and prev button callback.
	function docconvert_cycle(e)
	{
		e.preventDefault();
		e.stopPropagation();
		var isNext=$(this).hasClass("next");
		
		var links=[];
		var old=0;
		ebody.find('.docconvert-doc').each(function()
										   {
											   if(this===editor.docconvert.currentDoc){old=links.length;} 
											   links.push(this);
										   });
		if(links.length===0){return;}
		var link=links[(old+(isNext ? 1 : -1)+links.length)%links.length];
		editor.docconvert.currentDoc=link;
		// scroll editor to doc (or better, to its button)
		var button=$(link).children('.docconvert-doc-button');
		scroll_editor_to(button.length ? button[0] : link);
		docconvert_select_doc(link);
	}

	function docconvert_rebuild_content_info()
	{
		docconvert_log('docconvert_rebuild_content_info');
		

		// Horrible hack to fix copy paste of float right document.
		// Detect the pasted document and reinsert it properly in a float right paragraph.
		ebody.find('.pasted-composite-doc').each(function()
												 {
													 var doc=$(this);
													 if($.browser.msie)
													 {
														 var html=doc.clone().wrap('<div>').parent().html();
													 }
													 if(doc.parents('.composite-doc-container').length===0)
													 {
														 var html=doc.clone().wrap('<div>').parent().html();
														 var tmp=$('<a>');
														 doc.replaceWith(tmp);
														 var split=demosphere_htmledit.split_after(tmp[0],ebody[0]);
														 $(split).after($('<p class="floatRight">').html(html));
														 tmp.remove();
													 }
												 });
		ebody.find('.pasted-composite-doc').removeClass('pasted-composite-doc');

		// Detect "composite-doc"s (document link with pdf-images, usually floatright)
		ebody.find('p>a:has(.pdfPage)').addClass('composite-doc').parent().addClass('doc-type-pdf');
		// Hack so that tinymce does not open imagetools image editing toolbar for pdfPage images.
		ebody.find('.pdfPage').attr("data-mce-object",true);
		// Detect composite-docs parents and make them non-editable
		ebody.find('.composite-doc').parent()
			.addClass('composite-doc-container')
			.addClass('mceNonEditable')
			.attr('data-mce-contenteditable','false');
		ebody.find('.composite-doc-container').not('.composite-doc-is-setup').
			each(function(){docconvert_composite_doc_setup($(this));});

		editor.demosphere_htmledit.add_empty_paragraphs();

		docconvert_find_docconvert_docs();
	}

	//! Adds handles and event handlers to all composite documents.
	//! Composite documents are non-editable paragraphs with a link and other stuff inside (currently only pdf doc link with page previews).
	//! FIXME: we should use bubbling for event handlers.
	function docconvert_composite_doc_setup(doc)
	{
		
		doc.addClass('composite-doc-is-setup');

		// handles not supported on ie<9
		//if($.browser.msie && parseInt($.browser.version, 10)<9){return;}

		doc.find('.composite-doc-handles').remove();


		doc.append('<span class="composite-doc-handles mceNonEditable">'+
				   '<span class="composite-doc-handle composite-doc-handle-edit"  ></span>'+
				   '<span class="composite-doc-handle composite-doc-handle-down"  ></span>'+
				   '<span class="composite-doc-handle composite-doc-handle-resize"></span>'+
				   '<span class="composite-doc-handle composite-doc-handle-close" ></span>'+
				   '</span>');

		// avoid mozilla resizing handles 
		doc.find('.composite-doc-handles')[0].contentEditable=false;

		// Event handlers use bubling and are declared in docconvert_init
	}


	//! Finds all "a" links and text links (plain urls in text) and adds a 
	//! docconvert button to them. The type of each link is determined server side
	//! through an async request.
	function docconvert_find_docconvert_docs(e,nocache,callback)
	{
		docconvert_log("docconvert_find_docconvert_docs");
		if(e){e.preventDefault();e.stopPropagation();}
		
		$('.docconvert-info').addClass('searching-for-docs');
		
		// remove buttons and reset current-link class
		ebody.find('.docconvert-current').removeClass("docconvert-current");
		ebody.find('.docconvert-doc-button').remove();

		// find plain text urls
		docconvert_find_plain_text_urls();

		// all "a" links are candidate links
		ebody.find('a').addClass("docconvert-doc");

		// find all urls we haven't checked for yet, so that we can determine their type
		var noTypeLinks=[];
		var urls=[];
		ebody.find('.docconvert-doc').each(function()
										   {
											   var type=this.className.match(/docconvert-type-([a-z0-9-]*)/);
											   if(type!==null){return;}
											   noTypeLinks.push(this);
											   var url=this.tagName==="A" ? this.href.toString() : this.getAttribute("data");
											   //docconvert_log("dbga:",this.tagName);
											   //docconvert_log("url:",url);
											   urls.push(docconvert_full_url(url));
										   });

		// reset current doc
		if(editor.docconvert.currentDoc)
		{$(editor.docconvert.currentDoc).addClass("docconvert-current");}

		// Async request to fetch document types for all urls of unknown type
		if(urls.length)
		{
			$.post(base_url+"/docconvert/async-get-url-types"+
				   (typeof nocache!=="undefined" && nocache===true ? "&nocache" : ""), 
				   {"urls[]": urls},
				   function(response)
				   {
					   if(!response){alert("async-get-url-types failed");}
					   for(var i=0;i<noTypeLinks.length;i++)
					   {
						   var link=$(noTypeLinks[i]);
						   var type=response.types[i];
						   docconvert_log('found type:',type);
						   if(type===false){link.removeClass('docconvert-doc');}
						   else            {link.addClass("docconvert-type-"+type);}
					   }
					   docconvert_after_find_docconvert_docs();
					   if(typeof callback === 'function'){callback();}
				   },"json");
		}
		else
		{
			docconvert_after_find_docconvert_docs();
			if(typeof callback === 'function'){callback();}
		}
	}

	//! Second part of docconvert_find_docconvert_docs(), called after it has finished changing things.
	function docconvert_after_find_docconvert_docs()
	{
		docconvert_log("docconvert_after_find_docconvert_docs");
		
		// add hidden buttons to each doc link
		ebody.find('.docconvert-doc-button').remove();
		ebody.find('.docconvert-doc').append('<span class="docconvert-doc-button"></span>');
		// Tinymce 4.1.9 bug: selecting line and hitting del deletes following mceNonEditable line, not selected line.
		// Add white space after link with doccconvert button
		ebody.find('a>.docconvert-doc-button').each(function()
													{
														var a=$(this).parent();
														if(a.next().is('br') && a.parent().next().is('.composite-doc-container'))
														{
															a.after($('<span class="spacer-for-del-bug"></span>'));
														}
													});
		$('.docconvert-info').removeClass('searching-for-docs');
		$('.docconvert-info').toggleClass("noDocs",
										  ebody.find('.docconvert-doc').length==0);
	}

	//! Converts a link (ex: b/xyz.pdf ) in this page (ex: http://example.org/a) to a full url (ex: http://example.org/a/b/xyz.pdf).
	function docconvert_full_url(url)
	{
		//docconvert_log("docconvert_full_url:",url);
		if(url.match(/^https?:\/\//)){return url;}
		var page=window.location.toString();
		var root=page.replace(new RegExp("^(https?://[^/]*).*$"),'$1');
		docconvert_log("page:",page);
		docconvert_log("root:",root);
		if(url.match(/^\//)){return root+""+url;}
		return page+"/"+url;
	}

	//! Callback when a document is selected. Shows buttons (menu items) for this document.
	//! Warning: the actual execution of this function can happen in the future (waits for async-get-url-types to finish)
	function docconvert_select_doc(selDoc)
	{
		//console.log('docconvert_select_doc');

		// wait for async-get-url-types to finish
		if($('.docconvert-info').hasClass('searching-for-docs'))
		{
			window.setTimeout(function()
							  {
								  //console.log('wait for docs');
								  docconvert_select_doc(selDoc);
							  },200);
			return;
		}
		
		var menu=areaRight.find('.docconvert-current-doc-menu');

		// If this is a click on a doc button, fetch the actual doc.
		selDoc=$(selDoc).closest('.docconvert-doc')[0];
		if(typeof selDoc=="undefined"){return;} // strange rare problem

		// quick animation so that user notices a change if same type of docs
		if(editor.docconvert.currentDoc!==selDoc)
		{
			menu.css("opacity",.2);
			menu.animate({'opacity':1},300);
		}

		// reset the current doc class 
		ebody.find('.docconvert-doc').removeClass("docconvert-current");
		$(selDoc).addClass('docconvert-current');
		editor.docconvert.currentDoc=selDoc;

		// get the type of this doc ("pdf","image","office")
		var type=selDoc.className ?  selDoc.className.match(/docconvert-type-([a-z0-9-]*)/) : null;
		docconvert_log(type);
		docconvert_log(selDoc);
		if(type===null){alert("Bug: no type found for this doc. This should not happen");return;}
		type=type[1];

		// Create menu
		menu.html('');
		var url=selDoc.tagName==="A" ? selDoc.href.toString() : selDoc.getAttribute("data");
		var fname=url.replace(/^.*\//,'');
		menu.append($('<h3>').text(type+' : '+fname));

		// Create the menu lines for this doc
		var lines=$('<ul>');
		lines.append($('<li class="direct-link">').
					 append($('<a href="javascript:void(0);" target="_blank"/>').
							text(t("view_document")+' ('+type+')')));
		if($.inArray(type,["pdf","office"])!==-1)
		{
			lines.append($('<li>').
						 append($('<span data-docconvert-cmd="preview-pages">').text(t("page_icons"))).
						 append($('<span data-docconvert-cmd="preview-pages-1">').text("(1)")).
						 append($('<span data-docconvert-cmd="preview-pages-2">').text("(2)"))
						);
		}
		if($.inArray(type,["pdf","office"])!==-1)
		{
			lines.append($('<li data-docconvert-cmd="extract-contents" >').
						 text(t("extract_contents")));
		}
		if($.inArray(type,["pdf"])!==-1 && $('body').hasClass('eventAdmin'))
		{
			lines.append($('<li class="pdf-select-link">').
						 append($('<a target="_blank" href="javascript:void(0);">').
								text(t("pdf_select"))));
		}
		if($.inArray(type,["pdf"         ])!==-1 && $('body').hasClass('eventAdmin'))
		{
			lines.append($('<li data-docconvert-cmd="extract-pdf-images" >').
						 text(t("extract_pdf_images")));
		}
		if($.inArray(type,["image"       ])!==-1)
		{
			lines.append($('<li data-docconvert-cmd="insert-image" >').
						 text(t("insert_image")));
		}

		lines.find('[data-docconvert-cmd]').addClass("docconvert-command");
		menu.append(lines);

		menu.find(".docconvert-command").click(
			function(e){docconvert_command(this,e,selDoc,
										   $(this).attr('data-docconvert-cmd'));});

		menu.find(".direct-link a").attr("href",url);
		menu.find(".pdf-select-link a").attr("href",base_url+"/docconvert/pdf-select?url="+encodeURIComponent(url));
	}

	function docconvert_unselect_doc()
	{
		areaRight.find('.docconvert-current-doc-menu').html('');
		ebody.find('.docconvert-doc').removeClass("docconvert-current");
		editor.docconvert.currentDoc=false;
	}

	function docconvert_command(target,e,selDoc,command,removeSelDocOnSuccess,successCallback)
	{	
		if(e){e.stopPropagation();e.preventDefault();}
		docconvert_log(command);
		var url=selDoc.tagName==="A" ? selDoc.href.toString() : selDoc.getAttribute("data");

		// insert image command does not need async command: do it now and finish
		if(command==="insert-image")
		{
			var node=demosphere_htmledit.split_after(selDoc,ebody[0]);
			$(node.nextSibling).before('<p><img id="tmp-insert-image" src=""/></p>');
			ebody.find("#tmp-insert-image").attr("src",url).removeAttr("id");
			editor.demosphere_htmledit.rebuild_content_info();
			return;
		}

		// change the active link's appearence and avoid re-click by user
		//var li=e.currentTarget.parentNode; IE: no currentTarget
		var li=target.parentNode;
		if($(li).hasClass("waiting"))
		{
			alert(t("wait_for_cmd"));
			return;
		}
		$(li).addClass("waiting");

		// get the type of this doc ("pdf","image","office")
		var type=selDoc.className.match(/docconvert-type-([a-z0-9-]*)/)[1];
		// create the destination html element
		var insert=$('<p class="docconvert-waiting-placeholder" '+
					 'style="padding-left: 40px;background-image: url(\''+base_url+'/demosphere/docconvert/waiting.gif\');"'+
					 '>'+t('loading')+'</p>');
		var hr=null;

		// Determine where we are going to insert the results,
		// and insert placeholder
		switch(command)
		{
			case "preview-pages":
			case "preview-pages-1":
			case "preview-pages-2":
			// if doc is in list insert after list
			if($(selDoc).parents('body>ul,body>ol').length)
			{
				$(selDoc).parents('body>ul,body>ol').after(insert);
			}
			else
			{
				var after=docconvert_text_after_node(selDoc);
				// for small paragraphs just insert after doclink paragraph
				if(after.length<50)
				{
					$(selDoc).parents('body>*').after(insert);
				}
				else
				{
					// for long paragraphs insert directly after doc link
					var node=demosphere_htmledit.split_after(selDoc,ebody[0]);
					ebody[0].insertBefore(insert[0],node.nextSibling);
				}
			}
			break;
			case "extract-contents":
			case "extract-pdf-images":
			// insert the result of these commands before next HR
			var node;
			for(node=selDoc;node.parentNode && node.parentNode!==ebody[0];
				node=node.parentNode);
			for(;node.nextSibling && node.nextSibling.tagName!=="HR";
				node=node.nextSibling);
			hr=document.createElement("HR");
			ebody[0].insertBefore(hr,node.nextSibling);
			ebody[0].insertBefore(insert[0] ,hr.nextSibling);
			break;
		}
		scroll_editor_to(insert[0]);
		
		// execute async convert command on server
		$.post(base_url+"/docconvert/async-command", 
			   {url: docconvert_full_url(url),
				command: command,
				type: type},
			   function(response)
			   {
				   $(li).removeClass("waiting");
				   insert.css('background-image','none');
				   if(!response){alert("async command failed");}

				   // if error, display alert and quit
				   if(response.error)
				   {
					   alert(response.error);
					   insert.remove();
					   if(hr!==null){$(hr).remove();}
					   return;
				   }

				   if(removeSelDocOnSuccess){$(selDoc).remove();}
				   insert.replaceWith(response.html);
				   // reparse in case new docs have been inserted
				   docconvert_find_docconvert_docs();
				   if(successCallback){successCallback();}
				   docconvert_rebuild_content_info();
			   },"json");	
	}

	// Upload file using a custom button, and submitting the form in a hidden frame.
	// This was very difficult to achieve on ie, 1.5 days of tedious trial and error.
	// Error "SCRIPT5: Access is denied." kept appearing in unpredictable places.
	// This form contains an iframe that will be used as a target for submission
	// that way the form can be submitted without refreshing this page.
	// Note: jquery.form.js (.ajaxForm) plugin does this automatically, but it doesn't work on ie.
	function docconvert_upload_file_setup()
	{
		docconvert_log('docconvert_upload_file_setup');
		
		// cleanup in case this is a re-init
		$('#docconvert-upload-form').remove();

		// This form cannot be inserted into right area, as this will introduce a nested form.
		var form=$('<form id="docconvert-upload-form" method="POST" enctype="multipart/form-data" target="docconvert-upload-target">'+
				   '<input id="docconvert-upload-file" type="file" name="docconvertupload" value="upload" />'+
				   '<input type="hidden" name="docconvert-editor-id" value="'+editor.id+'"/>'+
				   '<input type="hidden" name="dlib-form-token-docconvert_upload_form"/>'+
				   '<iframe id="docconvert-upload-target" name="docconvert-upload-target" style="width:0;height:0;border:0px solid red;"></iframe>'+
				   '</form>');
 		form.attr('action',base_url+'/docconvert/submit-upload');
 		form.find('input[name="dlib-form-token-docconvert_upload_form"]').val(docconvert.upload_form_token);
		// Avoid frame cross domain problems on IE (not sure if really needed)
 		//form.find('#docconvert-upload-target').attr('src',docconvert_config.script_url);
		// add form at end of body
		$('body').append(form);
		// add button and text to right area
		$('.docconvert-upload').append(
			'<div id="docconvert-upload-button-wrap">'+
				'<div class="docconvert-upload-button">upload file</div>'+
				'</div>');
		$('.docconvert-upload').append($('<div/>').html(t('upload-add')));
		$('.docconvert-upload-button').text(t('upload-button')+($.browser.msie ? " : ":""));

		// IE requires wants us to click on the real file input.
		// So we temporarily move it into right area, beside the button.
		if($.browser.msie)
		{
			$('#docconvert-upload-button-wrap').prepend($('#docconvert-upload-file'));
		}

		// Open file input dialog when user clicks un custom upload button.
		if(!$.browser.msie && typeof window.selenium_testing_docconvert==="undefined")
		{
			$('.docconvert-upload-button').click(function(e){$('#docconvert-upload-file').click();});
		}

		// Automatically submit form when file is chosen
		$('#docconvert-upload-file').on('change',function(e){docconvert_upload_file_start_upload();});

	}

	//! This is called when the user has chosen a file (file input: on change).
	function docconvert_upload_file_start_upload()
	{
		docconvert_log('docconvert_upload_file_start_upload');
		
		var fileInput=$('#docconvert-upload-file');

		// Check if file is not too large (on compatible browsers)
		if(typeof window.FileReader === "function" || typeof window.FileReader === "object") 
		{
			if(fileInput[0].files && fileInput[0].files[0])	 
			{
				// FIXME: there is probably a better way to detect errors???
				try{var s=fileInput[0].files[0].size;}
				catch(ex)
				{
					alert("invalid file");
					return;
				}
				if(fileInput[0].files[0].size>docconvert.upload_maxsize)
				{
					alert(t('file_too_large')+' : '+
						  (Math.round(100.0*fileInput[0].files[0].size/(1024*1024))/100)+"M"+' > '+
						  (Math.round(100.0*docconvert.upload_maxsize /(1024*1024))/100)+"M")
					return;
				}
			}
			else
			{
				alert('invalid file');
				return;
			}
		}

		// On IE we have temporarily moved the file input into right area. 
		// We need to move it back into form, before submit.
		if($.browser.msie){$('#docconvert-upload-form').prepend(fileInput);}

		// ***** Setup place where the new document will be inserted.

		// remove any old docconvert-uploading-p from previous uploads
		ebody.find("#docconvert-uploading-p").removeAttr("id");

		// This is the paragraph where the document will be inserted
		var insert=$('<p id="docconvert-uploading-p"></p>');

		// Get current caret position
		var caret=editor.selection!==null ? editor.selection.getNode() : null;

		// If caret position ok
		if(caret!==null && typeof caret.tagName!='undefined' && caret.tagName.toLowerCase()!=='body')
		{
			pos=$(caret);
			// Move up the DOM tree to a direct descendant of body
			pos=pos.closest('body>*');
			// If it is a "Source:" line, move up one line
			if(pos.text().match(new RegExp("^\\s*"+escapeRegExp(t('source'))+"\\s*:"))){pos=pos.prev();}
			// If everything is ok, insert after pos
			if(pos.length){pos.after(insert);}
		}
		else
		{
			// Caret position failed (for example, when user has not clicked in editor yet: caret is not in document)
			// Insert document at end of first part (hr), before Source:
			var pos=ebody.find('hr').first();
			pos=pos.length!==0 ? pos.prev() : ebody.children().last();
			if(pos.length)
			{
				if(pos.text().match(new RegExp("^\\s*"+escapeRegExp(t('source'))+"\\s*:"))){pos.before(insert);}
				else                                                                       {pos.after (insert);}
			}
		}

		// Catchall: if something failed, insert at end of body
		if(ebody.find('#docconvert-uploading-p').length===0){ebody.append(insert);}

		scroll_editor_to(ebody.find('#docconvert-uploading-p')[0]);

		// Add wait message to placeholder
		ebody.find('#docconvert-uploading-p').append($('<span id="docconvert-uploading-label"></span>').text(t('uploading_')));

		// actually submit the form
		$('#docconvert-upload-form').submit();
	}

	//! This function is called by js on the page (inside a hidden frame) sent from the server.
	function docconvert_upload_success(url)
	{
		docconvert_log('docconvert_upload_success:',url);
		

		// replace placeholder with actual link to document
		var p=ebody.find('#docconvert-uploading-p');
		var link=$('<a/>').attr("href",url).attr("data-mce-href",url).text(t('uploaded_document'));
		p.removeAttr("id");
		p.find('#docconvert-uploading-label').replaceWith(link);

		// Delete old upload form and recreate it. 
		// This is only necessary on webkit, otherwise, 
		// "change" event will not be re-fired if same file is chosen again.
		$('#docconvert-upload-form').remove();
		$('.docconvert-upload').html('');
		docconvert_upload_file_setup();

		// automatically extract image or pdf-page previews
		docconvert_find_docconvert_docs(undefined,undefined,function()
										{
											if(link.hasClass("docconvert-type-image"))
											{
												link.replaceWith($('<img/>').attr('src',link.attr('href')));
												editor.demosphere_htmledit.add_empty_paragraphs();
												window.setTimeout(function(){editor.demosphere_htmledit.rebuild_content_info();},500);
											}
											else
												if(link.hasClass("docconvert-type-pdf"))
											{
												docconvert_command(false,undefined,link[0],'preview-pages',true,
																   function()
																   {
																	   if($.trim(p.text())==''){p.remove();}
																   });
											}		
										});
	}

	//! Called by js generated on server for hidden iframe.
	function docconvert_upload_failed()
	{
		
		var p=ebody.find('#docconvert-uploading-p');
		p.removeAttr("id");
		p.find('#docconvert-uploading-label').remove();

		// Delete old upload form and recreate it. 
		// This is only necessary on webkit, otherwise, 
		// "change" event will not be re-fired if same file is chosen again.
		$('#docconvert-upload-form').remove();
		$('.docconvert-upload').html('');
		docconvert_upload_file_setup();

		alert(t('upload_failed'));
	}


	//! Tell other script if this editor can be closed. 
	//! For example: in public form, warn user if he wants to close the editor while an upload is pending.
	function docconvert_can_finish()
	{
		
		if(ebody.find('#docconvert-uploading-p').length)
		{
			return t('still_uploading');
		}

		if(ebody.find('.docconvert-waiting-placeholder').length)
		{
			return t('docconvert_not_finished');
		}

		return true;
	}

	//! Adds a docconvert-doc span around all plain text url's inside the editor.
	function docconvert_find_plain_text_urls()
	{
		var re=new RegExp('(https?://\\S*)');
		var textNodes=docconvert_find_plain_text_urls_nodes(ebody[0]);
		for(var i=0,l=textNodes.length;i<l;i++)
		{
			var text=textNodes[i];
			var parent=text.parentNode;
			var re2=new RegExp("(http://[^\s]*)","");re2=re;
			
			var parts=typeof text.textContent!=="undefined" ?
				text.textContent.split(re2) :
				//ie <=8
				$(text).text();
			//docconvert_log(parts,text.textContent);
			var prev=false;
			for(var j=0;j<parts.length;j+=2)
			{
				if(prev!==false)
				{
					text=document.createTextNode("");
					parent.insertBefore(text,prev.nextSibling);
				}
				text.textContent=parts[j];
				prev=text;
				if((j+1)<parts.length)
				{
					var span=document.createElement("SPAN");
					span.textContent=parts[j+1];
					span.setAttribute("data",parts[j+1]);
					span.className="docconvert-doc";
					parent.insertBefore(span,text.nextSibling);
					prev=span;
				}
			}
		}
	}

	//! Recursively builds a list of nodes containing plain text urls.
	function docconvert_find_plain_text_urls_nodes(node)
	{
		//docconvert_log("find_text:",node,node.nodeType);
		var allText= typeof node.innerText!=='undefined' ? node.innerText /* ie */ : node.textContent;

 		if(typeof allText==='undefined' && node.nodeType===3)
		{
			allText=node.data;// ie: innerText not valid for textnodes
		}

		var textHasUrl=
			(allText.indexOf('http://' )!=-1 ||
			 allText.indexOf('https://')!=-1 );
		
		if(!textHasUrl){return false;}
		if(node.nodeType===3)// text node
		{
			if(textHasUrl){return [node];}
			return false;
		}

		var children=node.childNodes;
		var res=[];
		for(var i=0,l=children.length;i<l;i++)
		{
			if(children[i].tagName==="A"){continue;} // ignore links
			if(children[i].tagName==="SPAN" && 
			   children[i].hasAttribute("data")){continue;} // ignore text links
				var found=docconvert_find_plain_text_urls_nodes(children[i]);
			if(found!==false){res=res.concat(found);}
		}
		return res;
	}


	//! Scroll editor so that the argument node is visible.
	function scroll_editor_to(node)
	{
		var iframe=$(editor.getContainer()).find('iframe')[0];
		// scroll so that the current doc is visble
		var pos=$(node).offset().top;
		var h=$(iframe).height();
		var scroll=ebody.scrollTop()!=0 ? 
			ebody.scrollTop() :
			ebody.parent().scrollTop();
		if(pos>scroll+(h-40))
		{
			ebody.scrollTop(pos-h+40);
			ebody.parent().scrollTop(pos-h+40);
		}
		else
		if(pos<scroll)
		{
			ebody.scrollTop(pos);
			ebody.parent().scrollTop(pos);
		}
	}
}

//! Translation function, fetches strings translated in php in docconvert_config.
//function t(s)
//{
// 	if(docconvert_config.text[s]===undefined)
// 	{alert('untranslated:"'+s+'", please report bug.');return s;}
// 	return docconvert_config.text[s];
//}


function docconvert_text_after_node(node)
{
	var res=null;
	for(var i=0;i<node.parentNode.childNodes.length;i++)
	{
		var n=node.parentNode.childNodes[i];
		if(res!==null)
		{
			if(n.nodeType===3 /* Node.TEXT_NODE (ie prob)*/)
			{res+=n.nodeValue;}
			else
			{res+=$(n).text();}
		}
		if(n===node){res="";}
	}
	if(res===null){res="";}
	return res;
}

function docconvert_log()
{
	//try{console.log.apply(this,arguments);}
	//catch(e)
	//{
	// 	$.each(arguments,function(){console.log(this);});
	//}
}

// http://stackoverflow.com/questions/3446170/escape-string-for-use-in-javascript-regex/6969486#6969486
function escapeRegExp(str) 
{
	return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}

// Exports:
window.docconvert.docconvert_init=docconvert_init;

// end namespace wrapper
}());
