<?php

error_reporting(E_STRICT | E_ALL);

// launch command line test if called from command line
if(!isset($_SERVER['SERVER_NAME']) && isset($argv) &&
   basename($argv[0])===basename(__FILE__))
 {text_matching_command_line_test();return;}

function text_matching_command_line_test()
{
	setlocale(LC_TIME, "fr_FR.utf8");
    ini_set("include_path", ini_get("include_path").PATH_SEPARATOR."/var/www/common/demosphere");
	require_once 'dlib/tools.php';
 	global $db_params,$text_matching_config;

 	$db_params=['host'=>'localhost','user'=>'demosphere','password'=>'FIXME','database'=>'test'];

	require_once 'dlib/tools.php';
	require_once 'text-matching.php';
	require_once 'dlib/testing.php';

	// FIXME setup config
	$text_matching_config=
		[
			'bad_words'=>	['tt','tt2'],
		]; 

	db_setup();
	test_text_matching_setup_cleandb();

	//test_text_matching_badsentences();
	// test_word_hash_randonmness();
	test_text_matching_index_demosphere_events();
// 	test_text_matching_match_events();
}

function test_text_matching_badsentences()
{
	$bad=new BadSentence('aaaa bbbb cccc');
	$bad->save();
	$bad=BadSentence::fetch(1);
	$wordList=$bad->wordList;
	test_equals(count($wordList),3);
	test_equals($wordList[0],'aaaa');
	$bad1=new BadSentence('dddd eeee ffff');
	$bad1->save();
	$bad2=new BadSentence('aaaa xxxx yyyy');
	$bad2->save();
	list($sentences,$firstWords)=BadSentence::buildBadSentencesRef();
	test_equals(count($sentences),3);
	test_equals(count($firstWords),2);

	$wordList=TMDocument::splitIntoWordList('zz1 zz2 zz3 zz4','html');
	test_equals(count($wordList),4);

	$wordList=TMDocument::splitIntoWordList('zz1 aaaa bbbb cccc zz2 zz3 zz4','html');
	test_equals(count($wordList),4);

	$wordList=TMDocument::splitIntoWordList('zz1 aaaa x bbbb cccc zz2 zz3 zz4','html');
	test_equals(count($wordList),4);

	$wordList=TMDocument::splitIntoWordList('zz1 aaaa zz2 zz3 zz4','html');
	test_equals(count($wordList),5);

	$bad2=new BadSentence('aaaa xxxx yyyy zzzz');
	$bad2->save();

	$wordList=TMDocument::splitIntoWordList('zz1 aaaa xxxx yyyy zzzz zz2 zz3 zz4','html');
	test_equals(count($wordList),4);

	$wordList=TMDocument::splitIntoWordList('zz1 aaaa xxxx yyyy uuuu zz2 zz3 zz4','html');
	test_equals(count($wordList),5);


	$wordList=TMDocument::splitIntoWordList('zz1 b aaaa xxxx b yyyy uuuu zz2 b zz3 b zz4','html',$extraInfo);
	test_equals(count($wordList),5);
	test_equals($extraInfo['words'],[0 =>  'zz1',
									 6 =>  'uuuu',
									 7 =>  'zz2',
									 9 =>  'zz3',
									 11 => 'zz4']);
	
	//p	rint_r($sentences);
	//print_r($firstWords);

}


function test_word_hash_randonmness()
{
	$nids=db_one_col("SELECT node.nid FROM demosphere.demosevent,demosphere.node WHERE node.nid=demosevent.nid AND node.status!=0 AND node.nid<11000 ORDER BY node.nid DESC LIMIT 500");	
	$allWords=[];
	foreach($nids as $nid)
	{
		$text=db_result('SELECT body FROM demosphere.node_revisions WHERE nid=%d',$nid);
		$words=TMDocument::splitIntoWordList($text,'html');
		$allWords=array_merge($allWords,array_values($words));
		$allWords=array_unique($allWords);
	}
	//print_r($allWords);
	echo 'nbwords:'.count($allWords)."\n";
	$kpFrequency=7;
	$n=0;
	$buckets=[];
	for($i=0;$i<$kpFrequency;$i++){$buckets[$i]=0;}
	foreach($allWords as $word)
	{
		$m=md5($word,true);
		//$m=md5($word."".strlen($word),true);
		$wordHash=ord($m); 
		//$wordHash=ord($m[1]); 
		//$wordHash=ord($word[0]);
		//$wordHash=ord($word[0])+strlen($word);
		$mod=($wordHash)%$kpFrequency;
		//printf("%15s : %3d %3d\n",$word,$wordHash,$mod);
		$buckets[$mod]++;
		if($mod==0){$n++;}
	}
	echo "total:$n:".(count($allWords)/$n)."\n";
	foreach($buckets as $i => $val){echo "bucket $i:".sprintf("%+3.5f",(count($allWords)/($val))-$kpFrequency)."\n";}
}
function test_text_matching()
{
// 	global $db_params;
// 	$db_params=array('host'=>'localhost','user'=>'demosphere','password'=>'FIXME','database'=>'test');
// 	db_setup();
	test_text_matching_basic();
	//test_insert();
}

function test_text_matching_basic()
{
	test_text_matching_setup_cleandb();

	// add a few bad sentences to make it more realistic
	$bad=new BadSentence('aaaa bbbb cccc');
	$bad->save();
	$bad=new BadSentence('xxx yyy zzz uuu aaaa');
	$bad->save();

	db_query("INSERT INTO test_tm_testdoc (id,contents) VALUES (%d,'%s')",
			   123,'axx bxx cxx dxx exx fxx gxx hxx ixx jxx kxx lxx mxx nxx oxx pxx qxx rxx sxx txx uxx vxx');
 	$doc0=new TMDocument('testdoc',123);
 	$doc0->addToIndex();

	// check for index
	$tid=db_result('SELECT tid FROM tm_keyphrase_doc,TMDocument WHERE tm_keyphrase_doc.document_id=TMDocument.id LIMIT 1');
	test_equals($tid,'123');


	// check for matching docs
	$matches=TMDocument::findKPMatchingDocuments('ayy byy cyy dyy eyy fyy gyy hyy iyy jyy kyy lyy myy nyy oyy pyy qyy ryy syy tyy uyy vyy','html');
	test_assert(count($matches)==0);

	$matches=TMDocument::findKPMatchingDocuments('axx bxx cxx dxx exx fxx gxx hxx ixx jxx kxx lxx mxx nxx oxx pxx qxx rxx sxx txx uxx vxx','html');
	test_assert(count($matches)>=1);

	// add new doc
	db_query("INSERT INTO test_tm_testdoc (id,contents) VALUES (%d,'%s')",
			   124,'ayy byy cyy dyy eyy fyy gyy hyy iyy jyy kyy lyy myy nyy oyy pyy qyy ryy syy tyy uyy vyy');
 	$doc1=new TMDocument('testdoc',124);
 	$doc1->addToIndex();

	$matches=TMDocument::findKPMatchingDocuments('ayy byy cyy dyy eyy fyy gyy hyy iyy jyy kyy lyy myy nyy oyy pyy qyy ryy syy tyy uyy vyy','html');
	test_assert(count($matches)>=1);

	$docs=array_keys($matches);
	$foundId=$docs[0];
	$doc1bis=TMDocument::fetch($foundId);
	test_assert($doc1bis->tid==124);

	// check deletion
	$doc1bis->delete();
	$nbdocs=db_result('SELECT COUNT(id) FROM TMDocument WHERE id=%d',
						$foundId);
	test_equals($nbdocs,'0');

	$matches=TMDocument::findKPMatchingDocuments('ayy byy cyy dyy eyy fyy gyy hyy iyy jyy kyy lyy myy nyy oyy pyy qyy ryy syy tyy uyy vyy','html');
	test_equals(count($matches),0);


}

function test_text_matching_setup_cleandb()
{
	// use different tables for testing
	global $db_connection;
	fatal('tablename_map no longer supported');
	$db_config["tablename_map"]['tm_keyphrase_doc' ]='test_tm_keyphrase_doc';
	$db_config["tablename_map"]['TMDocument'       ]='test_TMDocument';
	$db_config["tablename_map"]['BadSentence'      ]='test_BadSentence';

	$result = mysqli_query($db_connection,"SET NAMES 'utf8mb4';") or fatal('Query failed: ' . mysqli_error($db_connection));
	text_matching_install();

	db_query("DROP TABLE IF EXISTS test_tm_testdoc");
	db_query("CREATE TABLE test_tm_testdoc (".
			   "`id` int(11) NOT NULL,".
			   "`contents` TEXT NOT NULL default '',".
			   "PRIMARY KEY  (`id`)".
			   ") DEFAULT CHARSET=utf8mb4;");	
}



//function test_text_matching_simple_data()
//{
// 	db_query("INSERT INTO test_tm_testdoc (id,contents) VALUES (%d,'%s')",
// 			   0,'ceci est un texte tres simple blabla blibli');
// 
// 	db_query("INSERT INTO test_tm_testdoc (id,contents) VALUES (%d,'%s')",
// 			   1,'un autre texte pas trop complique non plus');
// 
// 	db_query("INSERT INTO test_tm_testdoc (id,contents) VALUES (%d,'%s')",
// 			   2,'du simple blabla blibli qui sert a tester');
//}

// speed test
function test_text_matching_index_demosphere_events()
{
	// add a few bad sentences to make it more realistic
	$bad=new BadSentence('aaaa bbbb cccc');
	$bad->save();
	$bad=new BadSentence('xxx yyy zzz uuu aaaa');
	$bad->save();

	$nids=db_one_col("SELECT node.nid FROM demosphere.demosevent,demosphere.node WHERE node.nid=demosevent.nid AND node.status!=0 AND node.nid<11000 ORDER BY node.nid DESC LIMIT 2000");	
	echo "indexing ".count($nids)." events\n";
	$start = microtime(true);
	foreach($nids as $nid)
	{
		$doc=new TMDocument('testevent',$nid);
		$doc->addToIndex();
	}
	$end = microtime(true);
	echo ($end-$start)." s - ".(count($nids)/($end-$start))."event/s\n";
}
function test_text_matching_match_demosevents()
{
	$ids=db_one_col("SELECT id FROM TMDocument ORDER BY id DESC LIMIT 2000");		
	$start = microtime(true);
	foreach($ids as $id)
	{
		$doc=TMDocument::fetch($id);
		$doc->findKPSimilarDocuments();
	}
	$end = microtime(true);
	echo "match : ".($end-$start)." s - ".(count($ids)/($end-$start))."docs/s\n";
}

function testdemosphere_event_text_matching_hook($op,$arg1=false)
{
	//echo "testdoc_text_matching_hook op:$op\n";
	switch($op)
	{
	case 'allTids': fatal('allTids uninmplemented for testdoc');return;
	case 'getContents':
		return [db_result('SELECT body FROM demosphere.node_revisions WHERE nid=%d',$arg1),'html'];
	case 'getExpirationDate':
		return false;
	}
}


function testdoc_text_matching_hook($op,$arg1=false)
{
	//echo "testdoc_text_matching_hook op:$op\n";
	switch($op)
	{
	case 'allTids': fatal('allTids uninmplemented for testdoc');return;
	case 'getContents':
		return [db_result('SELECT contents FROM test_tm_testdoc WHERE id=%d',$arg1),'html'];
	case 'getExpirationDate':
		return false;
	}
}


?>
