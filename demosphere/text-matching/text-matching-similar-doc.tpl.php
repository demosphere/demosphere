<? // This is a simplified version of dcomponent/dcitem.tpl.php ?>
<? // We can re-use dcomponent/dcomponent-common.tpl.css  and js resize buttons ?>
<article id="$type-$typeId"  class="$type tm-similar-doc dcitem <?: $classes ?? '' ?>">
<h2 class="title-line">
	<span class="idNumber">$typeId</span>
	<a class="title-link <?: strlen($title) > 100 ? 'long-title' :'' ?>"
		href="<?: u($titleUrl) ?>">
		$title
	</a>
	<?= @$titleRight ?>
</h2>
<div class="main-line">
	<ul class="info-left">
		<?= @$infoLeft ?>
	</ul>
	<div class="dc-middle">
		<div class="info-top line-0">
			<?= @$infoTopLine ?>
		</div>
		<div class="frame-resize-buttons">
			<a href="#" title="_(bigger viewing area)">+</a><a href="#" title="_(smaller viewing area)">-</a>
		</div>
		<div class="tm-similar-doc-content content-frame">
			<?= filter_xss_admin($content) ?> 
		</iframe>
	</div>
</div>
</article>
