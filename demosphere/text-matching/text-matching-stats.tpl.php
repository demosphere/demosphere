<h1 class="page-title">Stats</h1>
<ul>
	<li>total num of keyphrase/doc pairs:$totKp</li>
	<li>total num of distinct keyphrases :$ditinctKP (<?: round(100*$ditinctKP/$totKp,2) ?>%)</li>
	<li>total num of documents:$totDocs</li>
	<li>average/stdev num keyphrases per doc: $avgKpPerDoc/$devKpPerDoc</li>
</ul>
<h3>Keys per document</h3>
	<p>min:$min max:$max</p>
<table id="stats" style="text-align: right">
	<? foreach($statsTable as $tableLine){?>
		<? extract($tableLine); ?>
		<tr>
			<td>$bin</td>
			<td><?: round($binStart,1) ?></td>
			<td><?: round($binEnd,1) ?></td>
			<td><?: $ct? round($tot/$ct,1) : '-' ?></td>
			<td><?: round(100*$cumul/$totCumul,1) ?>%</td>
			<td>$ct</td>
			<td class="stats-bar"><div style="width:$l~px">&nbsp;</div></td>
	<?}?>
</table>
