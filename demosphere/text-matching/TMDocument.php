<?php
/** \addtogroup text_matching 
 * @{ */

/*
 compare 1 doc to around 10.000 others (around 100-800 words/doc)
 compare time must be less than 1/3 second
 
 full= 4 million words 

 => look for full sentences


 ****** alogrithm:
 ** indexing:
 extract keyphrases  from refdoc.
 add keyphrases to indexes
   
 ** searching:
 STEP 1: coarse matching
 extract keyphrases  from doc.
 look for keyphrases in indexes

 GOALS : 
 (G1) few false positives: STEP 1 should return few documents 
 (G2) few false negatives: STEP 1 must return all matching documents

 KEYPHRASE HEURISTICS:

 1 - Detect sentence begining and choose n words. Make n big for G1, n small for G2.
 Problem: reliably identify sentence beginings.
 Problem: docs with few sentences (large sentences)
 => probably unreliable

 2 - Choose keyphrase start words using arbitrary criterion (ex: first letter ascii value is in a given range).
 advantage: can modulate number of keyphrases

 parameters: number of keyphrases, keyphrase length (n)
 Nb keyphrases:
 (NK+) : many keyphrases: good for G2, but biger index size (not big problem?)
 (NK-) : few keyphrases : G2! specially for short documents
 remark: NK probably doesn't impact G1 too much, n does. (unsure)

 try many:
 feeling: $kpFrequency=8 $kpLength=8




 Evaluation procedure:
 - evaluate keyphrase extraction heuristics, by automatic tests on large realworld DB of documents. 
 Problem: large DB needs to be manually labeled. 



 STEP 2: finer matching
 evaluate overlap between doc and refdoc


 Indexes:

 keyphrase ID -> doc ID


 keyphrase ID  -> keyphrase

 (what representation for keyphrase? : big string ?)

 keyphrase MD5 -> keyphrase ID [only usefull if keyphrase rep is not indexable by mysql]


 Other data structures:
 Doc:
 Id, type, 
*/


/**
 * A document (mail, feed article, or event) that is indexed for text matching.
 *
 *
 */
class TMDocument extends DBObject
{
	public $id=0;
	static $id_           =['type'=>'autoincrement'];
	// Types : event, feed_import, mail_import, tmpdoc
	public $type;
	static $type_         =[];
	public $tid;
	static $tid_          =['type'=>'int'];

	static $ignoredKeyphrasesCache=false;

	//! information for database load / save in DBObject.
	static $dboStatic=false;

	function __construct($type,$tid)
	{
		$this->type=$type;
		$this->tid=$tid;
	}

	static function getByTid($type,$tid)
	{
		$id=db_result_check("SELECT id FROM TMDocument WHERE tid=%d AND type='%s'",$tid,$type);
		if($id===false){return null;}
		return TMDocument::fetch($id);
	}

	function delete()
	{
		$this->removeFromIndex();
		parent::delete();
	}

	static function reindexTid($type,$tid)
	{
		require_once 'text-matching.php';
		$isExpired=text_matching_call_hook($type,'isExpired',$tid);
		$doc=TMDocument::getByTid($type,$tid);

		if($isExpired)
		{
			if($doc!==null){$doc->delete();}
			return;
		}

		if($doc===null){$doc=new TMDocument($type,$tid); }
		$doc->updateIndex();
	}

	public function updateIndex()
	{
		if($this->id){$this->removeFromIndex();}
		$this->addToIndex();
	}
	
	//! call daily from a cron job
	static function cleanupExpired()
	{
		$ids=db_one_col("SELECT id FROM TMDocument");
		foreach($ids as $id)
		{
			$doc=TMDocument::fetch($id);
			$isExpired=text_matching_call_hook($doc->type,'isExpired',$doc->tid);
			if($isExpired===true){$doc->delete();}
		}
	}

	//! indexes this document in keyphrase index
	protected function addToIndex()
	{
		// put this doc into DB if it's brand new (we need valid id)
		if($this->id==0){$this->save();}
		// ******* make keyphrases
		$keyphrases=self::makeKeyphrases($this->getWordList());
		if(count($keyphrases)===0)
		{
			//echo "addToIndex: no keyphrases!\n";
			return;
		}
		// ******* store keyphrase hashes in DB
		$s=','.intval($this->id).'),';
		$query='INSERT INTO tm_keyphrase_doc (keyphrase,document_id) VALUES';
		foreach($keyphrases as $hash)
		{
			$query.='('.$hash.''.$s;
		}
		$query=substr($query,0,-1);
		//echo $query."\n";
		db_query($query);
	}

	protected function removeFromIndex()
	{
		db_query('DELETE FROM tm_keyphrase_doc WHERE document_id=%d',$this->id);
	}

	function getStoredKeyphrases()
	{
		return db_one_col('SELECT keyphrase FROM tm_keyphrase_doc '.
							'WHERE document_id=%d',$this->id);
	}

	//! create word list from content of this document
	//! FIXME should be protected, but problem with DBObject::__call()
	public function getWordList(&$extraInfo=false)
	{
		require_once 'text-matching.php';
		list($content,$contentType)=
			text_matching_call_hook($this->type,'getContents',$this->tid);
		return self::splitIntoWordList($content,$contentType,$extraInfo);
 	}

	public static function makeKeyphrases($words,$posInfo=false,$checkIgnored=true)
	{
		$ignore=self::getIgnoredKeyphrases();
		$kpLength=8;
		$kpFrequency=6;
		//echo '<pre>';
		//print_r($words);
		// FIXME $words=simplify_words($words);
		// build raw keyphrases from wordlist
		$keyphrases=[];
		$wordsCt=count($words);
		foreach($words as $k=>$word)
		{
			$m=md5($word,true);
			$wordHash=ord($m); 
			//printf("%15s : %3d %3d\n",$word,$wordHash,($wordHash)%$kpFrequency);
			if(($wordHash)%$kpFrequency){/*echo "not chosen\n";*/continue;}
			//echo "word OK****\n";
			if($k+$kpLength>$wordsCt){/*echo "too close to end\n";*/continue;}// ignore kp past end of text
			$kp=implode('|',array_slice($words,$k,$kpLength,true));
			$hash=self::keyphraseHash($kp);
			if($checkIgnored && isset($ignore[$hash])){continue;}
			if(!$posInfo)
			{
				$keyphrases[]=$hash;
			}
			else
			{
				$keyphrases[$kp]=['hash'=>$hash,'wordnum'=>$k,'length'=>$kpLength];
			}
		}
		if(!$posInfo){return array_unique($keyphrases);}
		return $keyphrases;
	}

	static function splitIntoWordList($content,$contentType,&$extraInfo=false)
	{
		global $text_matching_config;
		static $badWords=false;
		if($badWords===false){$badWords=array_flip($text_matching_config['bad_words']);}

		$text=dlib_simplify_text($content,$contentType==='html');// note 12/2017: $contentType is always 'html'
		$words0=explode(' ',$text);
		if($extraInfo!==false)
		{
			$extraInfo=[];
			$extraInfo['allWords']=$words0;
			$extraInfo['words']=[];
			$extraInfo['map0']=[];
		}
		// first pass: remove bad words
		$words=[];
		$ct=0;
		foreach($words0 as $pos=>$word)
		{
			if((strlen($word)<=2 && !ctype_digit($word)) || 
			   isset($badWords[$word])){continue;}
			$words[]=$word;
			if($extraInfo!==false)
			{
				$extraInfo['words'][$pos]=$word;
				$extraInfo['map0'][$ct++]=$pos;
			}
		}

		//print_r($words);
		return $words;		
	}

	function findKPSimilarDocuments($ignore=false,$ignore2=false)
	{
		$docs=db_one_col(
'SELECT 
  kp2.document_id,
  COUNT(*)
FROM 
  tm_keyphrase_doc AS kp1, 
  tm_keyphrase_doc AS kp2 
WHERE 
  kp1.document_id =%d AND 
  kp1.keyphrase=kp2.keyphrase AND
  kp2.document_id!=%d
GROUP BY kp2.document_id',$this->id,$this->id);
		return $docs;
	}

	static function findKPMatchingDocuments($content,$contentType,
											$ignore=false,$ignore2=false)
	{
		if($ignore2!==false)
		{$ignore=TMDocument::getByTid($ignore,$ignore2)->id;}
		$keyphrases=
			self::makeKeyphrases(self::splitIntoWordList($content,
														 $contentType));
		//print_r($keyphrases);
		$documents=[];
		foreach($keyphrases as $hash)
		{
			$docs=db_one_col("SELECT document_id FROM tm_keyphrase_doc ".
							   "WHERE keyphrase=".$hash.
							   ($ignore!==false ? ' AND document_id!='.
								intval($ignore) : ''));

			foreach($docs as $docId)
			{
				$documents[$docId][]=$hash;
			}
		}
		return $documents;
	}

	static function removeKeyphrases($wordList)
	{
		global $dbConnection;
		$keyphrases=self::makeKeyphrases($wordList);
		//print_r($keyphrases);
		foreach($keyphrases as $hash)
		{
			$res=db_query("DELETE FROM tm_keyphrase_doc WHERE keyphrase=".$hash);
			$rows=mysqli_affected_rows($dbConnection);
			//echo "affected rows:$rows<br/>";
		}
	}
	
	//! Returns a positive 64bit integer representing the first 8 bytes of a hash.
	static function keyphraseHash($kp)
	{
		$rawHash=hash('md5',$kp,true);
		$res=0;
		for($i=0;$i<7;$i++){$res+=ord($rawHash[$i])<<($i*8);}
		$res+=(ord($rawHash[7]) & 127)<<(7*8);

		return $res;
	}

	//! Returns html to display a list of links to documents similar to this document.
	//! This list is displayed, for example, on Messages, Articles, and event edit forms.
	//! $extra is used by event-edit-form to add pasted/imported documents even if they are not really similar.
	function displaySimilarDocInfo($doOutput=true,$timestamps=false,$extra=[],$useLinkTargetBlank=false)
	{
		global $base_url;
		require_once 'text-matching/text-matching.php';
		$out='';
		$similarDocCounts=$this->findKPSimilarDocuments();
		
		$linkTarget=$useLinkTargetBlank ? ' target="_blank" ' : '';

		// Add extra to $similarDocCounts
		foreach($extra as $docId){if(!isset($similarDocCounts[$docId])){$similarDocCounts[$docId]=0;}}
		$extra=array_flip($extra);

		if(count($similarDocCounts)==0){return;}

		$nbRefKp=db_result('SELECT COUNT(*) FROM tm_keyphrase_doc WHERE document_id=%d',$this->id);
		
		// sort: show best matches first
		arsort($similarDocCounts);
		$out.='<span class="tmInfo overflow-popup">';
		$out.='<a class="similar-docs-link" '.$linkTarget.
			'href="'.$base_url.'/text-matching/tmdocument/'.$this->id.'/similar" '.
			'title="'.ent(t('documents that contain similar text')).'">'.
			ent(t('similar docs.')).'</a> : ';
		$ct=0;
		$similarDocs=TMDocument::fetchList('WHERE id IN('.implode(',',array_keys($similarDocCounts)).')');
		foreach($similarDocCounts as $similarDocId=>$nbCommonKp)
		{
			if($ct++>40 && !isset($extra[$similarDocId]))
			{
				if(strrpos($out,' ... ')!==strlen($out)-5){$out.=' ... ';}
				continue;
			}
			$similarDoc=$similarDocs[$similarDocId];
			// get extra info (class,title,url) for document
			$matchInfo=text_matching_call_hook($similarDoc->type,'matchInfo',$similarDoc->tid,$timestamps);
			$accuracy=$nbRefKp ?  round(100*$nbCommonKp/$nbRefKp) : 0;
			$out.=
				'<span class="tmMatch tm_'.$similarDoc->type.' '.
				              (isset($extra[$similarDocId]) ? 'extra ' : '').
				              val($matchInfo,'class','').'" '.
				      'title="'.ent($similarDoc->type.'#'.$similarDoc->tid).': '.
				               $accuracy.'%('.$nbCommonKp.'/'.$nbRefKp.') : '.
				               htmlspecialchars(val($matchInfo,'title','no title')).'" '.
				'>';
			$out.='<a class="accuracy" '.$linkTarget.' href="'.htmlspecialchars(val($matchInfo,'url','badurl')).'">'.$accuracy.'%</a>';
			$out.='<a class="diff" '.    $linkTarget.' href="'.$base_url.'/text-matching/hdiff?tmDoc1='.$this->id.'&amp;tmDoc2='.$similarDoc->id.'">¤</a>';
			$out.='</span> ';
		}
		$out.='</span>';
		if($doOutput){echo $out;}else{return $out;}
	}
	
	//! This can be usefull to add new bad words to 'bad_words' config
	//! Note: you should reindex everything if you change that config...
	//! FIXME: this needs to be done automatically!!!
	static function suggestBadWords()
	{
		//$maxDocs=1000;
		$ids=db_one_col("SELECT id FROM TMDocument");
		$count=[];
		foreach($ids as $id)
		{
			$doc=TMDocument::fetch($id);
			$list=$doc->getWordList();
			foreach($list as $word){$count[$word]=val($count,$word,0)+1;}
		}
		arsort($count,SORT_NUMERIC);
		return $count;
	}
	
    //! Returns a list of keyphrases that should be ignored in matches.
	//! see text_matching_update_ignored_keyphrases()
	//! This is cached for performance.
	public static function getIgnoredKeyphrases()
	{
		if(self::$ignoredKeyphrasesCache===false)
		{
			self::$ignoredKeyphrasesCache=variable_get('ignoredKeyphrases','',[]);
		}
		return self::$ignoredKeyphrasesCache;
	}

	static function addIgnoredKeyphrases(array $newHashes)
	{
		if(count($newHashes)===0){return;}
		$newHashes=array_map('intval',$newHashes);
		self::getIgnoredKeyphrases();
		$index=array_flip($newHashes);
		self::$ignoredKeyphrasesCache=self::$ignoredKeyphrasesCache+$index;
		variable_set('ignoredKeyphrases','',self::$ignoredKeyphrasesCache);
		db_query('DELETE FROM tm_keyphrase_doc WHERE keyphrase IN('.implode(',',$newHashes).')');
	}

	//! Returns a wordlist that generates the $keyphrase
	//! This is very slow and should only be used for display & debug.
	//! It will only work for keyphrases that are still present in existing documents.
	static function keyphraseToWordList($keyphrase)
	{
		$docIds=db_one_col('SELECT document_id FROM tm_keyphrase_doc INNER JOIN TMDocument on tm_keyphrase_doc.document_id=TMDocument.id '.
						   'WHERE keyphrase=%d ORDER BY document_id DESC',$keyphrase);
		foreach($docIds as $docId)
		{
			$tmdoc=TMDocument::fetch($docId,false);
			if($tmdoc===null){continue;}
			$tmdocKeyphrases=TMDocument::makeKeyphrases($tmdoc->getWordList(),true);
			foreach($tmdocKeyphrases as $words=>$tmdocKeyphrase)
			{
				if($tmdocKeyphrase['hash']==$keyphrase){return $words;}
			}
		}
		return false;
	}


	static function createTable()
	{
		db_query("DROP TABLE IF EXISTS TMDocument");
		db_query("CREATE TABLE TMDocument (".
				 "`id` int(11) NOT NULL auto_increment,".
				 "`type` varchar(50) NOT NULL,".
				 "`tid`  int(11) NOT NULL,".
				 "PRIMARY KEY  (`id`),".
				 "KEY `type` (`type`),".
				 "KEY `tid` (`tid`)".
				 ") DEFAULT CHARSET=utf8mb4;");	
	}

}


/** @} */

?>