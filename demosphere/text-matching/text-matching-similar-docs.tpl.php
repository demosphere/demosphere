<div id="tm-similar-docs">
<h3>_(Reference doc:)</h3>
<? text_matching_call_hook($refDoc->type,'displayInList',$refDoc->tid);?>

<h3><?: t('!nb similar docs:',['!nb'=>count($similarDocs)])?></h3>
<p id="similar-doc-info"><?= $refDoc->displaySimilarDocInfo(true,$refTimestamps) ?></p>

<?foreach($similarDocs as $doc){?>
	<div class="tmSimilar"><?: round(100*$nbMatches[$doc->id]/$nbRefKp) ?>%  
		<a href="$base_url/text-matching/hdiff?tmDoc1=$refDoc->id&tmDoc2=$doc->id">¤ _(view diff)</a>
	</div>
	<? text_matching_call_hook($doc->type,'displayInList',$doc->tid,$refDoc);?>
<?}?>
</div>