<div id="keyphrases">
	<h3>_(Matching keyphrases:)</h3>
	<ol>
		<? foreach($commonKeyphrases as $kp){?>
			<li>$kp (<?: TMDocument::keyphraseHash($kp)?>)</li>
		<?}?>
	</ol>
	<p>
		tot keyphrases ref: <?: count($keyphrases1) ?> : 
			<?: count($keyphrases1)==0 ? '-' : round(100*count($commonKeyphrases)/count($keyphrases1),1)?>%<br/>
		tot keyphrases oth: <?: count($keyphrases2) ?> : 
			<?: count($keyphrases2)==0 ? '-' : round(100*count($commonKeyphrases)/count($keyphrases2),1)?>%<br/>
	</p>

	<table>
		<tr>
			<? foreach($keyphrasesWithContext as $out){?>
				<td><?= $out?></td>
			<?}?>
		</tr>
	</table>
</div>
