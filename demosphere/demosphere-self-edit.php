<?php

/**
 * @file
 * self edit (non auth user can edit event using link received by email)
 */

/**
 * Creates an entry in self_edit table that will allow a non-auth user to edit a event.
 *
 * SelfEdit items have the following fields:
 * - 'password': randomly generated password that is sent as a GET arg in url
 * - 'event': the id of the event that can be edited with the password
 * - 'repetition_group': the id of the repetition group that can be edited with the password
 * - 'email': the email of the user associated to this self edit auth.
 * - 'created': when this selfedit auth was created.
 */
function demosphere_self_edit_create($email,$eventId,$groupId=false)
{
	if(intval($eventId)===0 && intval($groupId)===0){fatal('self_edit_create: eventId and groupId both 0');}

	// check if a self edit already exists for this event|group+email
	foreach(demosphere_self_edit_get_list($eventId,$groupId) as $selfEdit)
	{
		if($selfEdit['email']===$email){return $selfEdit;}
	}

	// create a new self edit
	$selfEdit=
		['password'=>dlib_random_string(15),
		 'event'=>intval($eventId),
		 'repetition_group'=>intval($groupId),
		 'email'=>$email,
		 'created'=>time(),
		];
	
	if(demosphere_self_edit_get($selfEdit['password'])!==null){dlib_permission_denied_403('self edit password problem.');}
	db_query("INSERT INTO self_edit (password,event,repetition_group,email,created) VALUES ('%s',%d,%d,'%s',%d)",
			 $selfEdit['password'],$selfEdit['event'],$selfEdit['repetition_group'],$selfEdit['email'],$selfEdit['created']);
	return $selfEdit;
}

//! Deletes the SelfEdit defined by password in $_POST['selfEdit'] (ajax)
function demosphere_self_edit_ajax_update()
{
	dlib_check_form_token('Event');
	$selfEdit=demosphere_self_edit_get($_POST['password']);
	if($selfEdit===null){return null;}

	// ev:      grp: 
	//  0     f  => hide
	//  id    f  => 
	//  f     f  => hide
	//  0     0  => delete
	//  id    0  => 
	//  f     0  => hide
	//  0     id =>      
	//  id    id => 
	//  f     id => 

	$prevGroup=$selfEdit['repetition_group'];

	$eventOk=val($_POST,'event-ok');
	$groupOk=val($_POST,'group-ok');
	$eventId=$_POST['event-id'];
	$groupId=db_result('SELECT repetitionGroupId FROM Event WHERE id=%d',$eventId);


	if($eventOk!==false){$selfEdit['event'           ]=$eventOk ? $eventId : 0;}
	if($groupOk!==false){$selfEdit['repetition_group']=$groupOk ? $groupId : 0;}

	// Delete if no group and no event
	if($selfEdit['event']==0            && 
	   $selfEdit['repetition_group']==0 )
	{
		db_query("DELETE FROM self_edit WHERE password='%s'",$selfEdit['password']);
		return null;
	}
	else
	{
		db_query("UPDATE self_edit SET event=%d,repetition_group=%d WHERE password='%s'",
				 $selfEdit['event'],
				 $selfEdit['repetition_group'],
				 $selfEdit['password']);
		return $selfEdit;
	}
}

//! Returns true if a user can self-edit an event.
//! Also checks if the event is in a repetition group that the user can self-edit.
//! Optionally returns self-edit password.
function demosphere_self_edit_check_event($eventId,$cuser=false,$retPass=false)
{
	global $user;
	if($cuser===false){$cuser=$user;}
	$selfEdits=[];
	if($cuser->id==$user->id)
	{
		demosphere_self_edit_from_url();
		$selfEdits=val($_SESSION,'selfEdit',[]);
	}
	$selfEdits=array_merge($selfEdits,demosphere_self_edit_list_for_email($cuser->email));
	foreach($selfEdits as $password)
	{
		$selfEdit=demosphere_self_edit_get($password);
		if($selfEdit['event']==$eventId){return $retPass ? $password : true;}
		if($selfEdit['repetition_group']!=0 &&
		   $selfEdit['repetition_group']==db_result('SELECT repetitionGroupId FROM Event WHERE id=%d',$eventId))
		{return $retPass ? $password : true;}
	}
	return false;
}

//! Returns true if current user can self-edit repetition group.
//! Optionally returns self-edit password.
function demosphere_self_edit_check_group($group,$retPass=false)
{
	global $user;
	demosphere_self_edit_from_url();
	$selfEdits=val($_SESSION,'selfEdit',[]);
	$selfEdits=array_merge($selfEdits,demosphere_self_edit_list_for_email($user->email));
	foreach($selfEdits as $password)
	{
		$selfEdit=demosphere_self_edit_get($password);
		if($selfEdit['repetition_group']!=0 && $selfEdit['repetition_group']==$group){return $retPass ? $password : true;}
	}
	return false;
}

//! Checks if url has valid $_GET password and adds it to $_SESSION. 
//! Does not return anything.
function demosphere_self_edit_from_url()
{
	global $user;
	$password=val($_GET,'selfEdit');
	if($password===false){return;}
	$selfEdit=demosphere_self_edit_get($password);
	if($selfEdit===null){return;}

	// Very special case, for (slight) convenience : logged-in user using a selfEdit that has a different email. Duplicate the selfEdit.
	if($user->id && !$user->checkRoles('admin','moderator') && $selfEdit['email']!==$user->email)
	{
		// demosphere_self_edit_create() checks for duplicates, so this is ok
		$selfEdit=demosphere_self_edit_create($user->email,$selfEdit['event'],$selfEdit['repetition_group']);
		$password=$selfEdit['password'];
	}

	// ***
	if(array_search($password,$_SESSION['selfEdit'] ?? [])===false)
	{
		$_SESSION['selfEdit'][]=$password;
	}
}


//! Returns SelfEdit for this password.
function demosphere_self_edit_get($password)
{
	return db_array("SELECT * FROM self_edit WHERE password='%s'",$password);
}

//! Returns all SelfEdit arrays for this event (event id).
function demosphere_self_edit_get_list($eventId=false,$groupId=false)
{
	return db_arrays('SELECT * FROM self_edit WHERE event=%d OR repetition_group=%d',
					 $eventId>0 ? $eventId : -1,
					 $groupId>0 ? $groupId : -1);
}

//! Returns list of passwords of selfedits that match an email
function demosphere_self_edit_list_for_email($email)
{
	if($email!='')
	{
		return db_one_col("SELECT password FROM self_edit WHERE email='%s'",$email);
	}
	return [];
}

//! Deletes all self edit items that have expired.
//! This is only used to keep the table small. Actual checks are done in Event::canSelfEdit()
//! FIXME: This does not cleanup old self edit's that have repetition_group
function demosphere_self_edit_cleanup_old()
{
	$eventExpiresDelay=3600*24*7;// 7 days after event is over
	db_query('DELETE self_edit FROM self_edit,Event WHERE self_edit.event=Event.id AND self_edit.repetition_group=0 AND Event.startTime<%d',
			 time()-$eventExpiresDelay);
}

function demosphere_self_edit_install()
{
	db_query("DROP TABLE IF EXISTS self_edit");
	db_query("CREATE TABLE self_edit (
  `password` varchar(100) NOT NULL,
  `event` int(11) NOT NULL,
  `repetition_group` int(11) NOT NULL,
  `email` varchar(250) NOT NULL,
  `created` int(11) NOT NULL,
  PRIMARY KEY  (`password`),
  KEY `password` (`password`),
  KEY `event` (`event`),
  KEY `repetition_group` (`repetition_group`),
  KEY `email` (`email`)
) DEFAULT CHARSET=utf8mb4");

}

?>