<?php

function demosphere_htmledit_js_config($id,$returnHtml=true,$addJsCssFiles=true)
{
	global $demosphere_config,$base_url,$currentPage,$user;

	if($addJsCssFiles){demosphere_htmledit_add_js_css();}

	// ******** button title translations
	$buttonTitles=['bighr'	        =>t('big horizontal line'),
				   'smallhr'	    =>t('small horizontal line'),
				   'downcase'	    =>t('down case'),
				   'upcase'	    =>t('up case'),
				   'tidy'		    =>t('cleanup'),
				   'linecleanup'   =>t('line cleanup'),
				   'linewrapcleanup'=>t('line wrap cleanup'),
				   'listcleanup'   =>t('list cleanup'),
				   'imageright'    =>t('image right'),
				   'paragraphright'=>t('paragraph right'),
				   'dtoken'		=>t('tokens'),];

	// build list of css files (including overrides) and add md5 '?' get arg to avoid cache problems
	require_once 'demosphere-common.php';
	require_once 'dlib/css-template.php';
	$contentCss=demosphere_css_overrides_list(css_template('demosphere/htmledit/demosphere-htmledit-contents.tpl.css'));
	foreach(array_keys($contentCss) as $k)
	{
		$contentCss[$k]=$base_url.'/'.demosphere_cacheable($contentCss[$k]);
	}

	$tinyMceLocale=demosphere_htmledit_tinymce_locale();

	$tinymceConfig=[
		'mode'             =>  "exact",
		'elements'         =>  $id,
		'plugins'          => "link,code,image,paste,demosphere,noneditable,searchreplace,lists".
		// Only admin or mod use image editing tools
		($user->checkRoles('admin','moderator') ? ',imagetools' : ''),
		'language'         =>  $tinyMceLocale,
		'entity_encoding'  =>  "raw",
		'resize' => true,
		'menubar'=>false,
		'toolbar'=> 'bold italic | bullist numlist outdent indent | undo redo | link unlink | '.
		'image code removeformat | formatselect | searchreplace | bighr smallhr | downcase upcase | '.
		'tidy linecleanup linewrapcleanup listcleanup | imageright paragraphright | dtoken'.
		(count($demosphere_config['editor_custom_insert'])==0 ? '':
		 ' | '.implode(' ',preg_replace('@^@','custominsert_',dlib_array_column($demosphere_config['editor_custom_insert'],'name')))),
		'demosphere_button_titles'=> $buttonTitles,
		'block_formats' => "Paragraph=p;Header 2=h2;Header 3=h3;Header 4=h4",
		'contextmenu' => false, // disable right click menu 
		'demosphere_area_right'=> 'sidebar',
		'document_base_url'=> $base_url.'/',
		'relative_urls'    =>  false,
		'gecko_spellcheck' =>  true,
		// FIXME: remove styles does no seem to work in tinymce4. So we remove styles manually in paste postprocess hook
		// wait for doc to update, and think about removing the following lines
		'paste_remove_styles'=> true,
		'paste_remove_styles_if_webkit'=> true,
		'paste_auto_cleanup_on_paste'=>  true,
		// link plugin: no target blank
		'target_list' => false,
		'content_css'      => implode('","',$contentCss),
		'width'=>'auto', // otherwise tinymce adds inline width: 100% which breaks position of htmleditor-area-right
				   ];

	//if(demosphere_is_mobile()){$tinymceConfig['mobile']=[ 'theme'=> 'mobile' ];}

	//$tinymceConfig=array(
	//'mode'             =>  "exact",
	//'elements'         =>  $id,
	//'plugins'          => "paste,demosphere,noneditable,searchreplace",
	// 					 );

	if(!$returnHtml){return $tinymceConfig;}

	$tinymceConfig=dlib_json_encode_esc($tinymceConfig);
	return '<script type="text/javascript">demosphere_htmledit_init=false;wait_for_script(function(){demosphere_htmledit_init=tinymce.init('.$tinymceConfig.');$("#'.$id.'").addClass("htmleditor-textarea");})</script>';
}

function demosphere_htmledit_tinymce_locale()
{
	// Find best locale
	$availableTinymceLocales=['de','de_AT','el','es','es_MX','eu','fr_FR','gl','pt_PT','pt_BR','ca'];
	$locale=setlocale(LC_TIME,"0");
	$matchingLocale=false;
	$match=array_search($locale,$availableTinymceLocales);
	if($match!==false){$matchingLocale=$locale;}
	else
	{
		$locale=preg_replace('@[._].*$@','',$locale);
		$match=array_search($locale,preg_replace('@[._].*$@','',$availableTinymceLocales));
		if($match!==false){$matchingLocale=$availableTinymceLocales[$match];}
	}
	return $matchingLocale;
}

function demosphere_htmledit_add_js_css($useDocConvert=true)
{
	global $currentPage;
	// ****** Add tiny mce and htmledit js+css
	$currentPage->addJs ('lib/jquery.js');
	$currentPage->addJs ('lib/jquery-browser.js');
	$currentPage->addCssTpl('demosphere/htmledit/demosphere-htmledit.tpl.css');
	$currentPage->addJsTranslations(['resized_image'=>
									 t('This image has been resized. Double-click on it to return to original size.'),
									 'img_ocr'=>t('Extract text (OCR)'),
									]
									);
	// translation not used here ... it is used in css template.
	$unused=t('ctrl+shift');

	$currentPage->addJsConfig('demosphere',['editor_custom_insert','locale']);

	//$currentPage->addJs ('lib/tinymce-dev/js/tinymce/tinymce.js','file',['cache-checksum'=>false]);
	$currentPage->addJs ('lib/tinymce/js/tinymce/tinymce.min.js','file',['cache-checksum'=>false]);
	$currentPage->addJs ('demosphere/htmledit/demosphere-htmledit.js' );

	if($useDocConvert)
	{
		// ****** Add docconvert 
		require_once 'docconvert/docconvert.php';
		docconvert_js_config();
		$currentPage->addJs ('demosphere/docconvert/docconvert.js' );
		$currentPage->addCssTpl('demosphere/docconvert/docconvert.tpl.css');
	}
}

//! Helper to setup dlib/hdiff using htmledit tinymce features
function demosphere_htmledit_hdiff()
{
	global $currentPage;
	$tinymceConfig=demosphere_htmledit_js_config('TMP',false,false);
	unset($tinymceConfig['elements']);
	$currentPage->addJsVar('demosphereTinyMceConfig',$tinymceConfig);

    demosphere_htmledit_add_js_css(false);

	$currentPage->addJs('demosphere/htmledit/demosphere-htmledit-hdiff.js');

	$token=dlib_add_form_token('hdiff');
	return $token;
}

// ************************************************************
// ********** Ajax interactions
// ************************************************************

function demosphere_htmledit_ajax_tidy()
{
	return demosphere_htmledit_cleanup_html($_POST['html'],
											val($_POST,'isArticle') ?
											['extra-attributes'=>['id'],
											 'class-whitelist'=>false]:
											[]);
}

function demosphere_htmledit_ajax_linecleanup()
{
	require_once 'dlib/html-tools.php';
	$html=$_POST['html'];
	$html=demosphere_htmledit_cleanup_html($html);
	$doc=dlib_dom_from_html($html,$xpath);

	foreach($xpath->query("//br") as $br)
	{
		if(dlib_dom_has_class($br->parentNode,'composite-doc')){continue;}
		dlib_dom_split($br);
		// special case: resplit again, in case parent was not a block (<p>aaa<strong><br />bbb</strong>ccc</p>)
		if(array_search($br->parentNode->nodeName,['p','h2','h3','h4','li','ul','ol'])===false)
		{
			dlib_dom_split($br->parentNode);
		}
		$br->parentNode->removeChild($br);
	}

	$html=dlib_dom_to_html($doc);
	$html=demosphere_htmledit_cleanup_html($html);
	return $html;
}

function demosphere_htmledit_ajax_linewrapcleanup()
{
	require_once 'dlib/html-tools.php';
	$html=demosphere_htmledit_ajax_linecleanup();
	$doc=dlib_dom_from_html($html,$xpath);

	$bullets=demosphere_htmledit_list_bullets();

	foreach($xpath->query("/p|/h2|/h3|/h4") as $curr)
	{
		//dlib_dom_print($curr);
		//echo $curr->textContent."\n";

		if(dlib_dom_has_class($curr,'composite-doc')){continue;}
		// check if this a real break or not
		for($next=$curr->nextSibling;$next && $next->nodeName==='#text';$next=$next->nextSibling);
		
		if($next===null || array_search($next->nodeName,['p','h2','h3','h4'])===false){continue;}
		
		$currText=trim(preg_replace('@\s+@',' ',$curr->textContent));
		$nextText=trim(preg_replace('@\s+@',' ',$next->textContent));
		//echo "currText: $currText\n";
		//echo "nextText: $nextText\n";

		if(preg_match('@[.?:!»";]$@s',$currText) || // prev finishes by punctuation
		   mb_strlen($currText)<10 ||             // prev is too short
		   ctype_upper(mb_substr($nextText,0,1)) ||   // next starts by a capital letter
		   array_search(mb_substr($nextText,0,1),$bullets)!==false  // next starts by a bullet
		   ){continue;}

		// hyphenation at end of line
		if(preg_match('@[-−―‐-]\s*$@u' ,$curr->textContent,$m1) &&
		   preg_match('@^\s*([^\s]+)@',$next->textContent,$m2) )
		{
			$curr->textContent=mb_substr($curr->textContent,0,-mb_strlen($m1[0])).$m2[1];
			$next->textContent=mb_substr($next->textContent,mb_strlen($m2[0]));
		}

		dlib_dom_merge_before($curr,$next);
	}

	$html=dlib_dom_to_html($doc);
	$html=demosphere_htmledit_cleanup_html($html);
	//echo $html;
	return $html;
}

/**
 * create a ul list from a set of paragraphs, remove all the junk
 * (spaces, hyphens... ) at the begining of each lines.
*/
function demosphere_htmledit_ajax_listcleanup()
{
	require_once 'dlib/html-tools.php';
	$src=$_POST['html'];
	$src=preg_replace('@[\x0d]@',"\n",$src);
	$src=dlib_html_cleanup_tidy_xsl($src);
	$res=$src;
	$lines=preg_split("@<p[^>]*>@",$res);
	$head=array_shift($lines);
	$res=$head.'<ul>';
	foreach($lines as $lnb=>$line)
	{
		$rline=$line;
		$rline=str_replace(demosphere_htmledit_list_bullets(),'-',$rline);
		$rline=preg_replace('@^[ \n]*(<strong>)?([ \n*-]|&nbsp;)*(<img[^>]*>)?([ \n*-]|&nbsp;)*@',
							'$1',$rline);
		$rline=str_replace('</p>','</li>',$rline);
		$res.='<li>'.$rline;
	}
	$res.='</ul>';
	require_once 'dlib/html-tools.php';
	$res=dlib_html_cleanup_tidy_xsl($res);
	return $res;
}

function demosphere_htmledit_list_bullets()
{
	return ['-','•','●','Ø','·','►','▶','−','♦','―','','','⇨','→','♦','','▪️'];
}

// ************************************************************
// ********** html cleanup
// ************************************************************

//! Returns clean html for events and posts and other html from editor (tinymce).
//! This function should be fast enough to be used in ajax tidy.
//! (no image processing...)
//! Actions done: 
//! - removes empty lines
//! - fixes html structure (tidy)
//! - uses xsl to enforce: whitelist of html tags, whitelist html attributes
//! - whitelist of classes
//! - merge "Source: ..." lines
//! - ...
//! This cleanup function does NOT do :
//! - download and resize images
//! - fix links
//! - do XSS filtering
//! For those actions (but not XSS) @see demosphere_htmledit_submit_cleanup()
function demosphere_htmledit_cleanup_html($html,$options=[])
{
	require_once 'dlib/html-tools.php';
	$html=dlib_html_data_url_remove($html);

	$options['extra-attributes'][]='data-float-right-text-height';

	$html=dlib_html_cleanup_tidy_xsl($html,$options);

	$doc=dlib_dom_from_html($html,$xpath);
	$xpath2 = new DOMXPath($doc);

	demosphere_htmledit_fix_urls_in_text($doc);

	// Fix tracking img src
	require_once 'demosphere-common.php';
	foreach($xpath->query("//img/@src") as $attr)
	{
		$attr->value=ent(demosphere_url_remove_tracking($attr->value));
	}

	// floatRight images should only be in paragraphs (not h2...h4)
	foreach($xpath->query("//img[contains(@class,'floatRight')]") as $img)
	{
		$imgOrA=$img;
		if($img->parentNode->nodeName=='a'){$imgOrA=$img->parentNode;}
		if($imgOrA->parentNode->nodeName!=='p' && $imgOrA->parentNode->parentNode->nodeName=='#document')
		{
			$h2=$imgOrA->parentNode;
			$newParagraph=$doc->createElement('p');
			$newParagraph->appendChild($imgOrA);
			$h2->parentNode->insertBefore($newParagraph,$h2->nextSibling);
		}
	}

	// cleanup empty space before and after a float right image 
	foreach($xpath->query("//img[contains(@class,'floatRight')]") as $img)
	{
		$imgOrA=$img;
		if($img->parentNode->nodeName=='a'){$imgOrA=$img->parentNode;}
		$p=$imgOrA->parentNode;
		if($imgOrA->parentNode->parentNode==null){continue;}// very strange
		if($imgOrA->parentNode->parentNode->nodeName!=='#document'){continue;}// strange
		if(trim($p->nodeValue)==='' && 
		   $xpath2->query(".//img",$p)->length===1 /* only one image in p */)
		{
			$newParagraph=$doc->createElement('p');
			$newParagraph->appendChild($imgOrA);
			$p->parentNode->replaceChild($newParagraph,$p);
		}
	}

	// remove floatRight class from paragraphs that do not contain a pdfPage
	foreach($xpath->query("//p[contains(@class,'floatRight') and not(contains(@class,'floatRightText'))]") as $p)
	{
		if($xpath2->query(".//*[contains(@class,'pdfPage')]",$p)->length==0)
		{
			dlib_dom_remove_class($p,'floatRight');
		}
	}

	// merge succesive <ul>
	do
	{
		$changed=false;
		foreach($xpath->query("//ul") as $ul)
		{
			$next=$ul->nextSibling;
			if($next===null || $next->nodeName!=='ul'){continue;}
			dlib_dom_merge($next,$ul);
			$changed=true;
			break;
		}
	}while($changed);


	// Remove heading and trailing <br/>'s inside li :  <li><br/>xxxx<br/></li>
	foreach($xpath->query("//li/br") as $br)
	{
		$isWhiteSpace=true;
		for($n=$br->previousSibling;$n!==null;$n=$n->previousSibling)
		{
			if($n->nodeName!=='#text'){$isWhiteSpace=false;break;}
			if(!preg_match('@^\s*$@',$n->textContent)){$isWhiteSpace=false;break;}
		}
		if($isWhiteSpace){$br->parentNode->removeChild($br);continue;}
	}
	$brs=[];
	foreach($xpath->query("//li/br") as $br){$brs[]=$br;}
	foreach(array_reverse($brs) as $br)
	{
		$isWhiteSpace=true;
		for($n=$br->nextSibling;$n!==null;$n=$n->nextSibling)
		{
			if($n->nodeName!=='#text'){$isWhiteSpace=false;break;}
			if(!preg_match('@^\s*$@',$n->textContent)){$isWhiteSpace=false;break;}
		}
		if($isWhiteSpace){$br->parentNode->removeChild($br);}
	}

	// remove all classes that are not in whitelist
	$whitelist=['floatRight','floatRightText','subsection','pdfPage','composite-doc','composite-doc-container','highres-checked','highres-check-timeout','doc-type-pdf'];
	if(!isset($options['class-whitelist'])){$options['class-whitelist']=$whitelist;}
	demosphere_htmledit_class_whitelist($doc,$whitelist);

	demosphere_htmledit_merge_sources_dom($doc);

	//dlib_dom_print($doc);
	$html=dlib_dom_to_html($doc);

	// tinymce bug: trailing space (<p >) badly breaks editor contents
	$html=preg_replace('@\s+>@','>',$html);

	// Keep consistent html formating after all of the processing done in this function
	// This avoids spurious html changes. 
	// Performance impact is low (12%) which much better than recalling full dlib_html_cleanup_tidy_xsl() ... for a typical doc: 1.8ms => 3.2ms)
	$html=dlib_tidy_repair_string($html,dlib_html_cleanup_tidy_xsl_config(),"utf8");

	return $html;
}

//! Turn text URLs into <a> links and cleanup misleading <a> links on text urls.
function demosphere_htmledit_fix_urls_in_text($doc)
{
	global $demosphere_config;
	require_once 'demosphere-common.php';
	$xpath=new DOMXPath($doc);

	// Transforms a valid URL into a prettier, shorter, displayable version (that may not be valid).
	$makeDisplayUrl=function($url,$isWWW)
		{
 			if($isWWW){$url=preg_replace('@^https?://(www[0-9]{0,3}\.)@','$1',$url);}
			$url=urldecode($url);
			$url=dlib_cleanup_plain_text($url);
			$url=dlib_truncate($url,45);
			return $url;
		};

	// Regex for urls in text. Special case: Wikipedia uses parentheses in urls.
	$httpRegex='\bhttps?://'.
		'(?:'.
		'[^[:space:]<>"\'\[\]{}/]*wikipedia.org/[^[:space:]<>"\']*[^[:space:]<>"\'.…,]'.
		'|'.
		'[^[:space:]<>"\'\[\]{}()]*'.                            '[^[:space:]<>"\'.…,()]'.
		')';
	$wwwRegex='www[0-9]{0,3}\.(?:[a-z0-9-]+\.){1,2}[a-z]{2,5}(?:/[^[:space:]<>"\']*[^[:space:]<>"\'.…,])?';
	// https://www.regular-expressions.info/email.html
	$emailRegex='[A-Z0-9._%+-]{1,64}\@[A-Z0-9.-]{1,125}\.[A-Z]{2,63}';

	// Merge adjacent text nodes.
	$doc->normalizeDocument();

	// Remove mailto: links that are on a text email
    // <a href="mailto:aaa@bbb.ccc">aaa@bbb.ccc</a> => aaa@bbb.ccc
	foreach($xpath->query("//a[starts-with(@href,'mailto:')]") as $link)
	{
		if(preg_match('/'.$emailRegex.'/i',$link->textContent)){dlib_dom_withdraw($link);}
	}

	// Merge adjacent text nodes.
	$doc->normalizeDocument();

	// Correct misleading text links : 
    // <a href="http://abc.com">bla http://xyz.org bla</a> => <a href="http://abc.com">bla http://abc.com bla</a>
	foreach($xpath->query("//a//text()") as $textNode)
	{
		for($linkEl=$textNode->parentNode;$linkEl!==null && $linkEl->nodeName!=='a';$linkEl=$linkEl->parentNode);
		if($linkEl===null){continue;}// should not happen

		$text=$textNode->wholeText;
		$href=$linkEl->getAttribute('href');

		// Fix relative or incomplete href's
		if(strpos($href,'//')===0          ){$href=parse_url($demosphere_config['std_base_url'],PHP_URL_SCHEME).':'.$href;}
		else
		if(strpos($href,'/' )===0          ){$href=$demosphere_config['std_base_url'].$href;}
		else
		// maybe mailto: or something else, not handled here
		if(!preg_match('@^https?://@',$href)){continue;}

		// sometimes a link is displayed as urldecoded (so we also need to compare to the decoded form)
		$decodedHref=urldecode($href);
		$decodedHref=dlib_cleanup_plain_text($decodedHref,false,false);

		// Search for url (anything begining with https:// or www) and check if we can match with href URL
		$newText='';
		for($pos=0;$pos<strlen($text);)
		{
			if(!preg_match('@(?:https?://|www[0-9]{0,3}\.[a-z0-9])@',$text,$m,PREG_OFFSET_CAPTURE,$pos))
			{
				$newText.=substr($text,$pos);
				break;
			}
			$oldPos=$pos;
			$pos=$m[0][1];
			$newText.=substr($text,$oldPos,$pos-$oldPos);
			$rest=substr($text,$pos);
			$isWWW=strpos($rest,'www')===0;
			$addScheme=$isWWW ? parse_url($href,PHP_URL_SCHEME).'://'  : '';

			$cleanHref=demosphere_url_remove_tracking($href);
			$displayUrl=$makeDisplayUrl($cleanHref,$isWWW);

			// Try to find the fullest text URL possible, beginning at $pos
			// Since the text URL might be a "pretty" displayed URL (urldecoded, shortened) it might not perfectly match $href
			$sRest=$addScheme.$rest;
			$foundFull=false;
			if(strpos($sRest,$displayUrl )===0){$mbLength=mb_strlen($displayUrl );$foundFull=true;}
			else
			if(strpos($sRest,$decodedHref)===0){$mbLength=mb_strlen($decodedHref);$foundFull=true;}
			else
			if(strpos($sRest,$href       )===0){$mbLength=mb_strlen($href       );$foundFull=true;}
			else
			{
				// Find common prefix to $sRest,$href,$decodedHref (this is slow)
				$min=min(mb_strlen($sRest),max(mb_strlen($href),mb_strlen($decodedHref)));
				$sRestA      =preg_split('//u', $sRest      , null, PREG_SPLIT_NO_EMPTY);
				$hrefA       =preg_split('//u', $href       , null, PREG_SPLIT_NO_EMPTY);
				$decodedHrefA=preg_split('//u', $decodedHref, null, PREG_SPLIT_NO_EMPTY);
				$mbLength=0;
				for($i=0;$i<$min;$i++)
				{
					if($sRestA[$i]!==      @$hrefA[$i] &&
					   $sRestA[$i]!==@$decodedHref[$i]   ){break;}
				}
				$mbLength=$i;
				// very special case: http://aaa.bbb truncated to http://aaa... (ambigous ellipsis "...")
				if(mb_substr($sRest,$mbLength-1,3)==='...'){$mbLength--;}
			}
			$found=mb_substr($sRest,0,$mbLength);
			
			// Check if regexp match finds longer url
			if(!$foundFull && preg_match('@(?:'.$httpRegex.'|'.$wwwRegex.')@iu',$addScheme.$rest,$reMatches))
			{
				$lRe=mb_strlen($reMatches[0]);
				if($lRe>$mbLength){$mbLength=$lRe;}
			}
			$length=strlen(mb_substr($rest,0,$mbLength-mb_strlen($addScheme)));

			// This doesn't handle special cases where there are multiple text urls inside this link.
			$linkEl->setAttribute('href',$cleanHref);

			$newText.=$displayUrl;

			if($length<=0){fatal('strange');}
			$pos+=$length;

			$newRest=substr($text,$pos);
			if(!$foundFull && preg_match('@^(\.\.\.(?!\.)|…)@',$newRest,$m)){$pos+=strlen($m[0]);}
			// Add extra space after url in newText if the following text is not punctuation or space
			if($newRest!=='' && !preg_match('@^[[:space:]<>"\'\[\]{}(),.]@',$newRest)){$newText.=' ';}
		}

        $textNode->nodeValue=$newText;
	}

	$doc->normalizeDocument();

	// Remove https:// links that are on a text email
    // <a href="https://example.org">aaa@bbb.ccc</a> => aaa@bbb.ccc
	foreach($xpath->query("//a[starts-with(@href,'http://') or starts-with(@href,'https://') or starts-with(@href,'/')]") as $link)
	{
		if(preg_match('/'.$emailRegex.'/i',$link->textContent)){dlib_dom_withdraw($link);}
	}

	// Merge adjacent text nodes. Can be created by dlib_dom_withdraw()
	$doc->normalizeDocument();

	// Remove all sorts of tracking and proxying from links.
	// Note: this is already done for <a> containing a text url. Doing it twice is OK.
	foreach($xpath->query("//a/@href") as $attr)
	{
		$attr->value=ent(demosphere_url_remove_tracking($attr->value));
	}

	// Manage text URL's not already in an <a>
	foreach($xpath->query("//text()[not(ancestor::a) and (contains(.,'http') or contains(.,'www'))]") as $textNode)
	{
		for($linkEl=$textNode->parentNode;$linkEl!==null && $linkEl->nodeName!=='a';$linkEl=$linkEl->parentNode);
		if($linkEl!==null){continue;} // should not happen

		$text=$textNode->wholeText;

		$regex='(?:'.
			'(?P<httpUrl>'.$httpRegex.')|'.
			'(?P<wwwUrl>(?<!://)'.$wwwRegex.')'.
			')';
			
		if(!preg_match_all('@'.$regex.'@iu',$text,$matches, PREG_OFFSET_CAPTURE)){continue;}

		$cTextNode=$textNode;
		$ctext='';
		$prevPos=0;
 		foreach($matches[0] as $nMatch=>$m)
 		{
			$url=$m[0];
			$pos=$m[1];

			$type=false;
			foreach(['httpUrl','wwwUrl'] as $t)
			{
			 	if($matches[$t][$nMatch]!=='' && $matches[$t][$nMatch][1]!==-1){$type=$t;}
			}

			$prevText=substr($text,$prevPos,$pos-$prevPos);
			// very special case: ignore text url in demosphere_dtoken
			if(preg_match('@demosphere_dtoken[a-z_0-9]*\(\s*$@',$prevText)){continue;}

			// url is followed by '…' => probably a previously shortened and unlinked url. Don't mess things up any further.
			if(mb_substr($text,$pos+mb_strlen($url),1)==='…' || 
			   mb_substr($text,$pos+mb_strlen($url),3)==='...' ){continue;}
			$ctext.=$prevText;
			$prevPos=$pos;
			$cTextNode->nodeValue=$ctext;
			
			$href=$url;
			if($type==='wwwUrl'){$href='https://'.$href;}
			$href=demosphere_url_remove_tracking($href);
			$displayUrl=$makeDisplayUrl($href,$type==='wwwUrl');

			$a=$doc->createElement('a');
			$a->setAttribute('href',$href);
			$a->textContent=$displayUrl;
			$nTextNode=$doc->createTextNode('');
			$ctext='';
			$cTextNode->parentNode->insertBefore($nTextNode,$cTextNode->nextSibling);
			$cTextNode->parentNode->insertBefore($a        ,$cTextNode->nextSibling);
			$cTextNode=$nTextNode;

			$prevPos+=strlen($url);
 		}
		$ctext.=substr($text,$prevPos);
		$cTextNode->nodeValue=$ctext;
	}
}


function demosphere_htmledit_class_whitelist($domDoc,$whitelist)
{
	$xpath=new DOMXPath($domDoc);
	foreach($xpath->query("//@class") as $attr)
	{
		if(trim($attr->value)===''){$attr->ownerElement->removeAttribute('class');continue;}
		$classes=explode(" ",$attr->value);
		foreach(array_keys($classes) as $k)
		{
			if(($whitelist!==false && array_search($classes[$k],$whitelist)===false) ||
			   ($whitelist===false && preg_match('@[^a-z0-9_-]@i',$classes[$k])))
			{unset($classes[$k]);}
		}
		$classes=implode(" ",$classes);
		$attr->value=ent($classes);
		if(trim($classes)===''){$attr->ownerElement->removeAttribute('class');}
	}
}


//! Merge all "Source: " lines into a single source paragraph (uses DOM not regexp)
function demosphere_htmledit_merge_sources_dom($doc)
{
	$source=preg_quote(strip_tags(t('_source_')),'@');
	$xpath = new DOMXPath($doc);
	foreach($xpath->query("/hr|/p[contains(@class,'demosphere-source')]|/p[last()]") as $s)
	{
		// found existing source or hr, now merge previous containers until no sources found
		//echo "found\n";dlib_dom_print($s);
		$sourceNode=$s->nodeName==='p' ? $s : null;
		for($n=$s;$n!==null;$n=$n->previousSibling)
		{
			//echo "considering:\n";dlib_dom_print($n);
			if($n->nodeName=='#text'){if(trim($n->nodeValue)==''){continue;}else{break;}}
			if($sourceNode===null && $n->nodeName==='hr'){continue;}
			if($n->nodeName!=='p'){break;}
			if(!preg_match('@^\s*'.$source.'\s*:@',$n->nodeValue)){break;}
			// found source line
			if($sourceNode!==null && $n!==$sourceNode)
			{
				$n->appendChild($doc->createElement('br'));
				dlib_dom_merge($sourceNode,$n);
			}
			$sourceNode=$n;
			dlib_dom_add_class($sourceNode,'demosphere-sources');
		}
	}

	// Remove empty lines inside Source paragraph
	foreach($xpath->query("/p[contains(@class,'demosphere-source')]/br") as $br)
	{
		if($br->nextSibling!==null &&
		   ($br->nextSibling->nodeName=='br' ||
			($br->nextSibling->nodeName=='#text' && trim($br->nextSibling->nodeValue)==='' && 
			 $br->nextSibling->nextSibling!==null && $br->nextSibling->nextSibling->nodeName=='br')))
		{
			$br->parentNode->removeChild($br);
		}
	}
}


// ************************************************************
// ********** on submit processing
// ************************************************************

//! Heavyweight cleanup when html edited in html editor is submited.
//! This 
//! - cleans up html: @see demosphere_htmledit_cleanup_html()
//! - fixes private links
//! - fixes relative/full links and paths
//! - fixes images (download, resize...)
function demosphere_htmledit_submit_cleanup($html,$options=[])
{
	global $base_url,$demosphere_config;
	require_once 'dlib/html-tools.php';
	$html=dlib_html_data_url_remove($html);

	//remove docconvert buttons (before cleanup_html that removes docconvert-link-button class)
	$html=preg_replace('@<span[^>]*class="[^">]*docconvert-link-button.*</span>@Us','',$html);

	// cleanup broken html
	$html=demosphere_htmledit_cleanup_html($html,$options);

	$doc=dlib_dom_from_html($html,$xpath);

	// remove copy pasted obfuscated mailto's
	foreach($xpath->query("//@href[.='mailto:---']") as $attr)
	{
		dlib_dom_withdraw($attr->ownerElement);
	}

	// fix relative image-src/a-href urls broken by htmleditor
	// remove copy pasted obfuscated mailto's
	foreach($xpath->query("//a/@href|//img/@src[starts-with(.,'..')]") as $attr)
	{
		$attr->value=ent(preg_replace('@^(\.\./)+@',$base_url.'/',$attr->value));
	}

	// convert relative link urls to other nodes to full urls
	foreach($xpath->query("//a/@href") as $attr)
	{
		$attr->value=ent(preg_replace('@^([0-9]+)@',
								  $base_url.'/'.$demosphere_config['event_url_name'].'/$1',
									  $attr->value));
	}

	// convert local full urls to absolute paths
	$base_root=dlib_host_url($base_url);
	foreach($xpath->query("//a/@href|//img/@src[starts-with(.,'..')]") as $attr)
	{
		$attr->value=ent(preg_replace('@^'.preg_quote($base_root,'@').'/@','/',$attr->value));
	}

	// fix links: convert private links to public
	foreach($xpath->query("//a/@href") as $href)
	{
		if(strlen($href->value)===0){dlib_dom_withdraw($href->ownerElement);continue;}
		$href->value=ent(demosphere_htmledit_submit_fix_private_link($href->value,$options));
	}

	// fix text links: convert private text links to public
	foreach($xpath->query("//text()") as $text)
	{
		if(strpos($text->wholeText,$base_url)===false){continue;}
		$ntext=preg_replace_callback('@'.preg_quote($base_url,'@').'[^[:space:]"\':]*@',
									function($m)use($options){return demosphere_htmledit_submit_fix_text_links_re_cb($m,$options);},
									$text->wholeText);
		$text->nodeValue=$ntext;
	}

	// fix images: compute width & height + make local
	foreach($xpath->query("//img") as $img)
	{
		demosphere_htmledit_submit_fix_image($img);
	}

	$html=dlib_dom_to_html($doc);

	return $html;
}

function demosphere_htmledit_submit_fix_text_links_re_cb($matches0,$options)
{
	global $base_url;
	$match=$matches0[0];
	$url0=$match;
	$p=explode(" ",$url0);// regexp might have matched extra nbsp after url
	$url=array_shift($p);
	$fixed=demosphere_htmledit_submit_fix_private_link($url,$options);
	if($fixed===''){return '[invalid link]';}
	if($fixed!==$url)
	{
		$base_root=dlib_host_url($base_url);
		$match=ent($base_root.$fixed).
			(count($p)>0 ? ent(" ".implode(" ",$p)) : '');
	}
	return $match;
}

//! If the given url points to a private document, the document is made public.
//! The new document will be in the files/docs directory.
//! The path of the new document is returned.
//! If the source url is not a private doc, this function just returns an unmodified $url.
//! Examples of private docs: mail attachment, docconvert doc
//! Only image, office or pdf documents are published. 
//! Links to other document types will removed (this function returns an empty string).
function demosphere_htmledit_submit_fix_private_link($url,$options)
{
	global $demosphere_config,$user,$base_url;
	$base_path=preg_replace('@^https?://[^/]*@','',$base_url).'/';

	require_once 'demosphere-misc.php';
	$private=demosphere_private_file($url);

	if($private['type']===false){return $url;}

	// Only event admins can access private files.
	// Docconvert is ok, since user cannot guess hash (doc id). (actually, mail import too, since it uses safe domain tokens)
	if(!$user->checkRoles('admin','moderator') && $private['type']!=='docconvert'){return '';}

	// First download it to private tmp dir
	$downloadFname=tempnam($demosphere_config['tmp_dir'],"fix-link-");
	$info=demosphere_download_file($url,$downloadFname);
   
	$failMsg=t("Failed to load link to private document : @url",['@url'=>$url]);
	if($info===false)
	{
		dlib_message_add($failMsg,'warning');
		return '';
	}

	// ** determine filename, mime-type and filename extension
	// As of 11/2018 we use checksum based filenames.
	require_once 'dlib/mail-and-mime.php';
	$mime=dlib_finfo($downloadFname);

	require_once 'dlib/mail-and-mime.php';
	$simpleType=dlib_mime_to_simple_type($mime);

	if($simpleType==='image')
	{
		// This is an image. Always use convert (for security and right type).
		static $goodTypes=['image/png'=>'png',
						   'image/jpg'=>'jpg',
						   'image/gif'=>'gif'];
		$extension=val($goodTypes,$mime,'png');
		$tmp2=$demosphere_config['tmp_dir'].'/'.dlib_random_string(20).'.'.$extension;
		$ret=dlib_logexec("convert ".escapeshellarg($downloadFname)." ".escapeshellarg($tmp2));
		unlink($downloadFname);
		if($ret!=0)
		{
			unlink($tmp2);
			dlib_message_add($failMsg.' (2)','warning');
			return '';
		}
		$fname=demosphere_htmledit_move_file_to_docs($tmp2,$extension);
		return $base_path.$fname;
	}

	// This is a pdf or an office document. 
	if($simpleType==='office' || $simpleType==='pdf')
	{
		$extension=dlib_mime_to_extension($mime);
		// Putting a document with an uncontrolled extension in a public dir is a security problem (ex: .php, .html ...)
		// FIXME: this list might be a bit restrictive
		$extensionWhiteList=['pdf','doc','docx','rtf','xls','xlsx','ppt','pptx','odt','ods','odp',];
		if(array_search(strtolower($extension),$extensionWhiteList,true)!==false)
		{
			$fname=demosphere_htmledit_move_file_to_docs($downloadFname,$extension);
			return $base_path.$fname;
		}
	}
	// Nothing found... just return an empty link (tidy phase, will remove the link later on)
	dlib_message_add(t("Removing link to document with unknown type (%type).",['%type'=>$mime]),'warning');
	unlink($downloadFname);
	return '';
}

//! Move a file to the files/docs/ directory and give it a short hash based filename.
//! Returns the filename.
//! The hash based approach avoids duplicate files (which previously occupied 25% of space).
function demosphere_htmledit_move_file_to_docs($src,$extension)
{
	$fname='files/docs/'.substr(hash_file('sha256',$src),0,15).'.'.$extension;
	// Extremely unlikely (unless hash collision attack)
	if(file_exists($fname) && hash_file('sha256',$src)!==hash_file('sha256',$fname))
	{
		$fname='files/docs/'.hash_file('sha256',$src).'.'.$extension;
	}
	chmod($src,0644);
	rename($src,$fname);
	return $fname;
}

function demosphere_htmledit_submit_fix_image($imgNode)
{
	global $base_url,$demosphere_config;
	$base_path=preg_replace('@^https?://[^/]*@','',$base_url).'/';
	$base_root=dlib_host_url($base_url);

	// get url, src and info attributes
	$url  =$imgNode->getAttribute('src');
	$class=$imgNode->getAttribute('class');

	// **** make images local and public
	require_once 'demosphere-misc.php';
	$private=demosphere_private_file($url);
	$isLocal=strpos($url,'/')===0  || strpos($url,$base_url)===0;

	// not a local image : make it local
	if(!$isLocal || $private['type']!==false)
	{
		require_once 'dlib/html-tools.php';
		//dlib_message_add(t('not a public local image, downloading:').ent($url));
		$url=demosphere_make_image_url_local($url);
		$imgNode->setAttribute('src',$url);
	}

	// change local url's to absolute path
	if(strpos($url,$base_root)===0)
	{
		$url=substr($url,strlen($base_root));
		$imgNode->setAttribute('src',$url);
	}

	if(strpos($url,$base_path)!==0)
	{
		dlib_message_add("local image url does not start with base_path:".$url,'error');
		return;
	}
	$fname=substr($url,strlen($base_path));
	$fname=str_replace('/document-conversion/','/docs/',$fname); // old backward compatibility

	if(!file_exists($fname))
	{
		dlib_message_add(t('image size : could not find image file:').ent($fname),'error');
		return;
	}

	// ******* Check if this image is inside a link to a higher resolution version of itself
	demosphere_htmledit_img_in_link_to_highres($imgNode);
	$url=$imgNode->getAttribute('src');
	$fname=substr($url,strlen($base_path));

	// ******* resize images and set width/height attributes

	// get original values of width and height attributes
	$foundAttrSize[0]=intval($imgNode->getAttribute('width'));
	$foundAttrSize[1]=intval($imgNode->getAttribute('height'));
	//echo '$foundAttrSize: ';var_dump($foundAttrSize);

	// get the real size of the image (the size as defined inside the image file)
	$origFileSize=demosphere_get_image_size($fname);
	//echo '$origFileSize: ';var_dump($origFileSize);
	
	$newSize=$foundAttrSize;

	// check for unset width/height attributes
	if($newSize[0]===0){$newSize[0]=$origFileSize[0];}
	if($newSize[1]===0){$newSize[1]=$origFileSize[1];}
	//echo '$newSize(check unset): ';var_dump($newSize);

	// resize FloatRight images: they aren't supposed to be larger than $maxRight
	$maxRight=$demosphere_config['event_image_max_width_float_right'];
	if(strpos($class,'floatRight')!==false && $newSize[0]>$maxRight){$newSize[0]=$maxRight;}
	//echo '$newSize(FloatRight): ';var_dump($newSize);

	// all images must be < $maxBody pixels
	$maxBody =$demosphere_config['event_image_max_width_body'];
	if($newSize[0]>$maxBody){$newSize[0]=$maxBody;}
	//echo '$newSize($maxBody): ';var_dump($newSize);

	// only resize images that need resizing
	if($newSize!=$origFileSize)
	{
		// impose known dir for resized image (example: problem if image is in theme)
		$resizedDir=dirname($fname);
		if($resizedDir!=='files/import-images' &&
		   $resizedDir!=='files/docs')
		{$resizedDir='files/import-images';}

		// source filename (for better image quality, use original, non resized file, if available)
		$srcFname=preg_replace('@(resized[0-9]*-)+([^/]*)$@','$2',$fname);
		if(!file_exists($srcFname)){$srcFname=$fname;}

		// destination filename (make sure it doesn't exist yet)
		$resizedFname=$resizedDir.'/resized-'.basename($srcFname);
		if(file_exists($resizedFname))
		{
			for($i=1;;$i++)
			{
				$resizedFname=$resizedDir.'/resized'.$i.'-'.basename($srcFname);
				if(!file_exists($resizedFname)){break;}
			}
		}
		// Resize image using "convert"
		//dlib_message_add('debug: orig:'.$fname.' src:'.$srcFname.' resized:'.$resizedFname);
		$cmd="convert ".escapeshellarg($srcFname).
			' -geometry '.intval($newSize[0]).'x'.intval($newSize[1]).' '.
			escapeshellarg($resizedFname);
		$ret=dlib_logexec($cmd);
		$imgNode->setAttribute('src',$base_path.$resizedFname);
		$fname=$resizedFname;
		// re-read the new image size 
		$newSize=demosphere_get_image_size($fname,false);
		//echo '$newSize(resized!): ';var_dump($newSize);
		//dlib_message_add(t('resized image to').': '.$newSize[0].'x'.$newSize[1]);
	}

	// change html width and height attributes if necessary
	if($newSize!=$foundAttrSize)
	{
		$imgNode->setAttribute('width' ,$newSize[0]);
		$imgNode->setAttribute('height',$newSize[1]);
	}
}

//! Checks if this image is contained in a link to a higher resolution version of itself.
//! If it is, this image's src is changed to the highres version and the parent link is removed.
//! A lot of this code duplicates demosphere_make_image_url_local(), but we need a few extra options that won't merge well into that function.
function demosphere_htmledit_img_in_link_to_highres($imgNode)
{
	global $demosphere_config,$base_url;
	require_once 'dlib/mail-and-mime.php';
	$base_path=preg_replace('@^https?://[^/]*@','',$base_url).'/';

	$goodTypes=['image/png'=>'png',
				'image/jpg'=>'jpg',
				'image/jpeg'=>'jpg',
				'image/gif'=>'gif'];

	// ** Basic checks
	if(dlib_dom_has_class($imgNode,'highres-checked')){return;}
	if($imgNode->parentNode===null){return;}
	$a=$imgNode->parentNode;
	if($a->nodeName!=='a'){return;}
	$linkUrl=$a->getAttribute('href');
	if(!preg_match('@^https?://@',$linkUrl)){return;}

	$fname=substr($imgNode->getAttribute('src'),strlen($base_path));
	$newSrcImgPrefix='files/import-images/'.md5($linkUrl);

	// Add class that avoids re-calling this function on each submit.
	dlib_dom_add_class($imgNode,'highres-checked');

	// ** Speedup: check if this url matches an existing, downloaded image file.
	$preexistingLinkImg=false;
	foreach(array_unique($goodTypes) as $suffix)
	{
		if(file_exists($newSrcImgPrefix.'.'.$suffix))
		{
			$preexistingLinkImg=$newSrcImgPrefix.'.'.$suffix;
			break;
		}
	}

	// ** If we don't already have it, download link image 
	if($preexistingLinkImg===false)
	{
		//  Short query to get remote content type
		// FIXME: if demosphere_get_content_type() times out we don't know about it (just consider it a fail)
		$mime=demosphere_get_content_type($linkUrl,['curl'=>[CURLOPT_TIMEOUT=>3]]);
		if(dlib_mime_to_simple_type($mime)!=='image'){return;}

		// now actually download image (use timeout to avoid huge image delay)
		$tmpLinkImg=tempnam($demosphere_config['tmp_dir'],"highres-img-compare-img-");
		$isTimeout=false;
		$info=demosphere_download_file($linkUrl,$tmpLinkImg,['timeout'=>3,
														  'error_callback'=>function($errno)use(&$isTimeout){$isTimeout=$errno===CURLE_OPERATION_TIMEDOUT;}]);
		if($isTimeout){dlib_dom_add_class($imgNode,'highres-check-timeout');}
		if($info===false){return;}
	}

	// ** Check if the link image and the src image are very similar
	$compare=demosphere_highres_img_check($fname,$preexistingLinkImg!==false ? $preexistingLinkImg : $tmpLinkImg);
	if($compare<1)
	{
		if($preexistingLinkImg===false){unlink($tmpLinkImg);}
		return;
	}

	// ** If highres image is new, create a converted copy in the std image directory
	if($preexistingLinkImg===false)
	{
		$tmpLinkImgMime=dlib_finfo($tmpLinkImg);
		$suffix=val($goodTypes,$tmpLinkImgMime,'png');
		$newSrcImg=$newSrcImgPrefix.'.'.$suffix;
		$ret=dlib_logexec("convert ".escapeshellarg($tmpLinkImg)." ".escapeshellarg($newSrcImg));
		unlink($tmpLinkImg);
	}
	else
	{
		$newSrcImg=$preexistingLinkImg;
		$ret=0;
	}

	// double check
	if(!file_exists($newSrcImg)){$ret=1;}

	// ** If everything is ok, replace srcImg src and remove parent <a> link
	if($ret==0)
	{
		//dlib_message_add(t('Replacing lowres image by highres found in link'));
		$url=$base_path.$newSrcImg;
		$imgNode->setAttribute('src',$url);
		$imgNode->removeAttribute('width');
		$imgNode->removeAttribute('height');
		dlib_dom_withdraw($a);
	}
}

//! Returns the size of an image in an array with 2 values.
//! Caches results to avoid recomputing image size too often.
function demosphere_get_image_size($fname,$useCache=true)
{
	static $cache=false;
	if($cache===false)
	{
		$cache=variable_get('demosphere-image-size-cache','',[]);
	}
	if($useCache && isset($cache[$fname])){return $cache[$fname];}
	$tmp=getimagesize($fname);
	$size=[$tmp[0],$tmp[1]];
	$cache[$fname]=$size;
	if(count($cache)>100){$cache=array_slice($cache,50,null,true);}
	variable_set('demosphere-image-size-cache','',$cache);
	return $size;
}

?>