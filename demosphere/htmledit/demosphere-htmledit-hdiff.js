// Add demosphere-htmledit capabilities to dlib/diff-html
window.hdiff = window.hdiff || {};
hdiff.tinymce_config_hook=function(tinymceConfig,mainDiv,col)
{
	// copy all htmledit configs to actual config
	$.each(demosphereTinyMceConfig,function(n,v)
		   {
			   if(!tinymceConfig.hasOwnProperty(n)){tinymceConfig[n]=v;}
		   });
	delete tinymceConfig.content_css;
	tinymceConfig.toolbar=tinymceConfig.toolbar.replace(/undo redo/,"");
	tinymceConfig.toolbar+=" | "+demosphereTinyMceConfig.toolbar;
	tinymceConfig.toolbar=tinymceConfig.toolbar.replace(/outdent indent /,"");
	tinymceConfig.toolbar=tinymceConfig.toolbar.replace(/code removeformat /,"");
	// Remove buttons depending on window width.
	// Note: this doesn't work with CSS @media, because tinymce pre-computes width of toolbar
	var w=$(window).width();
	//console.log(w);
	if(w<1700){tinymceConfig.toolbar=tinymceConfig.toolbar.replace(/ *\| */g," ");}
	if(w<1700){tinymceConfig.toolbar=tinymceConfig.toolbar.replace(/imageright/,"");}
	if(w<1700){tinymceConfig.toolbar=tinymceConfig.toolbar.replace(/paragraphright/,"");}
	if(w<1700){tinymceConfig.toolbar=tinymceConfig.toolbar.replace(/searchreplace/,"");}
	if(w<1550){tinymceConfig.toolbar=tinymceConfig.toolbar.replace(/dtoken/,"");}
	if(w<1450){tinymceConfig.toolbar=tinymceConfig.toolbar.replace(/image/,"");}
	if(w<1400){tinymceConfig.toolbar=tinymceConfig.toolbar.replace(/numlist/,"");}
	if(w<1350){tinymceConfig.toolbar=tinymceConfig.toolbar.replace(/undo/,"");}
	if(w<1250){tinymceConfig.toolbar=tinymceConfig.toolbar.replace(/redo/,"");}
	if(w<1200){tinymceConfig.toolbar=tinymceConfig.toolbar.replace(/listcleanup/,"");}
	if(w<1150){tinymceConfig.toolbar=tinymceConfig.toolbar.replace(/linewrapcleanup/,"");}
	if(w<1050){tinymceConfig.toolbar=tinymceConfig.toolbar.replace(/upcase/,"");}
	if(w<1000){tinymceConfig.toolbar=tinymceConfig.toolbar.replace(/downcase/,"");}
	if(w <950){tinymceConfig.toolbar=tinymceConfig.toolbar.replace(/smallhr/,"");}
	//console.log(tinymceConfig.toolbar);

};

//! User just hit the save button. We need to add a form token.
hdiff.save_hook=function(postData,mainDiv,colNum)
{
	postData['dlib-form-token-hdiff']=$('input[name="dlib-form-token-hdiff"]').val();
};

//! Some part of the event has been saved. Server is sending the response (ok===true/false).
hdiff.save_response_hook=function(response,mainDiv,colNum)
{
	if(response.ok)
	{
		// Only update "changed" when it is supported (ie. Events, not Posts)
		if(typeof response.changeNumber!=='undefined')
		{
			var savedEventId=response.id;
			// There are several hdiff's for each event (title, body, ...)
			// We have to set the event changed number in the save url for all of them.
			$('.event-hdiff-part.event-id-'+savedEventId).each(function()
			{
				var col=$(this);
				var url=col.attr('data-save');
				if(url.indexOf('/event-hdiff-save/'+savedEventId+'?')!==-1)
				{
					col.attr('data-save',url.replace(/&changeNumber=[0-9]+/,'&changeNumber='+response.changeNumber));
				}
			});
		}
	}
};
