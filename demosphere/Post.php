<?php

class Post extends DBObject
{
	public $id=0;
	static $id_        =['type'=>'autoincrement'];
	//! Named pages that are created at install and can optionally be synced with a reference site.
	public $builtIn;
	static $builtIn_ =[];	
	public $title;
	static $title_     =[];
	public $body;
	static $body_      =['type'=>'html'];
	public $authorId=0;
	static $authorId_    =['type'=>'foreign_key','class'=>'User'];
	public $status=0;
	static $status_    =['type'=>'enum','values'=>[0=>'not-published',1=>'published']];
	public $created;
	static $created_   =['type'=>'timestamp'];
	public $changed;
	static $changed_=['type'=>'timestamp'];

 	static $dboStatic=false;

	function __construct($title,$authorId,$body)
	{
		$this->title=$title;
		$this->body=$body;
		$this->created=time();
		$this->changed=$this->created;		
	}
	function save()
	{
		$this->changed=time();
		parent::save();
		require_once 'demosphere-misc.php';
		demosphere_log('Post','save',$this->id);
		demosphere_page_cache_clear_all();
		if($this->builtIn==='help_data')
		{
			require_once 'demosphere-help.php';
			demosphere_help_parse_data($this->body);
		}	
	}
	function delete()
	{
		require_once 'demosphere-misc.php';
		demosphere_log('Post','delete',$this->id);
		db_query("DELETE FROM path_alias WHERE src='post/%d'",$this->id);

		foreach(Comment::fetchList("WHERE pageType='Post' AND pageId=%d",$this->id) as $c)
		{
			$c->delete();
		}

		parent::delete();
	}

	//! Returns the id of a built-in page (Post) by name.
	static function builtInPid($page){return intval(db_result("SELECT id FROM Post WHERE builtIn='%s'",$page));}
	//! Returns the url of a built-in page by name.
	static function builtInUrl($page,$full=true)
	{
		global $base_url;
		$path=demosphere_path_alias_dest('post/'.Post::builtInPid($page));
		return ($full ? $base_url.'/' : '').$path;
	}
	static function allBuiltIn(){return db_one_col("SELECT builtIn,id FROM Post WHERE builtIn!=''");}
	static function createTable()
	{
		db_query("DROP TABLE IF EXISTS Post");
		db_query("CREATE TABLE Post (
  `id` int(11) NOT NULL auto_increment,
  `builtIn` varchar(250)  NOT NULL,
  `title` varchar(250)  NOT NULL,
  `body` longtext NOT NULL,
  `authorId` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created` int(11) NOT NULL,
  `changed` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `builtIn` (`builtIn`),
  KEY `authorId` (`authorId`),
  KEY `status` (`status`),
  KEY `created` (`created`),
  KEY `changed` (`changed`)
) DEFAULT CHARSET=utf8mb4");

	}

}
?>