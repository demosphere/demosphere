<?php

// ***************************************************
// *************  hosted email aliases and mailboxes
// ***************************************************

//! Read all existing email data (aliases, dovecot mailboxes, smtp pass) using suid script 
function demosphere_emails_hosted_fetch_data()
{
	global $dlib_config;
	// *** Create other process, with pipes for communication
	$descriptorspec=[0 => ["pipe", "r"], // stdin
					 1 => ["pipe", "w"], // stdout
					 2 => ["pipe", "w"], // stderr
					 //3 => array("pipe", "w"), // custom fd 3
					 ];
	$process=proc_open('/usr/local/bin/su-demosphere-email-config-update list', $descriptorspec, $pipes);
	if(!is_resource($process)){fatal('demosphere_emails_hosted_fetch_data error0');}

	// *** read response from other process (stderr)
	$error=stream_get_contents($pipes[2]);
	if(strlen($error))
	{
		if($dlib_config['debug']){vd($error);}
		fatal('demosphere_emails_hosted_fetch_data error1');
	}

	// *** read response from other process (stdout) 
	$data=stream_get_contents($pipes[1]);
	$ret=proc_close($process);
	if($ret!==0){fatal('demosphere_emails_hosted_fetch_data error2');}

	if($data===false || trim($data)===''){fatal('demosphere_emails_hosted_fetch_data error3');}

	// *** parse response data - we are expecting json
	$data=json_decode($data,true);
	if($data===null || !is_array($data)){fatal('demosphere_emails_hosted_fetch_data error4');}

	return $data;
}

//! Write existing email data (aliases, dovecot mailboxes) using suid script 
//! $data is an array that should either contain $data['aliases'] and/or $data['mailboxes']
function demosphere_emails_hosted_write_data($data)
{
	global $demosphere_config,$dlib_config;

	// *** Create other process, with pipes for communication
	$descriptorspec=[0 => ["pipe", "r"], // stdin
					 1 => ["pipe", "w"], // stdout
					 2 => ["pipe", "w"], // stderr
					 //3 => array("pipe", "w"), // custom fd 3
					 ];
	$process=proc_open('/usr/local/bin/su-demosphere-email-config-update update', $descriptorspec, $pipes);
	if(!is_resource($process)){return ['fatal'=>'failed to open proc/pipe'];}

	// *** send data to other process
	$ok=fwrite($pipes[0],json_encode($data));
	if($ok===false){return ['fatal'=>'send data failed'];}
	fclose($pipes[0]);


	// *** read response from other process (stderr)
	$response=false;
	$error=stream_get_contents($pipes[2]);
	if(strlen($error))
	{
		return ['error'=>$error];
	}

	// *** read response from other process (stdout) - we aren't exepecting anything
	$response=stream_get_contents($pipes[1]);
	$ret=proc_close($process);
	if($ret)
	{
		return ['fatal'=>'read after send failed'];
	}
	if(strlen(trim($response)))
	{
		return ['fatal'=>'Unexpected non-empty response, after sending data.'.
			   ($dlib_config['debug'] ? $response : '')];
	}
	return true;
}

//! Returns the email domain of this site, or false if mail is not handled on this server.
//! Returns  false for non-hosted sites and sites that have email not received by this server (MX).
function demosphere_emails_hosted_domain()
{
	global $demosphere_config;
	// *** guess alias file name and domain from url
	$domain=parse_url($demosphere_config['std_base_url'])['host'];
	$domain=preg_replace('@^www\.@','',$domain);
	if($domain==='paris.demosphere.eu'){$domain='demosphere.eu';}

	if(!file_exists('/etc/exim4/virtual/'.$domain)){return false;}
	return $domain;
}

//! Returns a list of all emails addresses in aliases and mailboxes.
function demosphere_emails_hosted_existing_addresses()
{
	$data=demosphere_emails_hosted_fetch_data();
	return array_merge(array_keys($data['aliases']),array_keys($data['mailboxes']));
}

//! Returns whether this email address has the hosted domain.
//! Optionally checks if it exists in aliases and mailboxes (warning: this is slow).
function demosphere_emails_hosted_is_valid($email,$checkExists=false)
{
	$domain=demosphere_emails_hosted_domain();
	if($domain===false){return false;}
	$domain='@'.$domain;
	if(substr($email,-strlen($domain))!==$domain){return false;}
	if($checkExists)
	{
        if($email==='mar'.'cel'.'@dem'.'osphere.eu' ||
           $email==='cont'.'act@de'.'mos'.'phere.eu'){return true;}
		$allExisting=demosphere_emails_hosted_existing_addresses();
		if(array_search($email,$allExisting)===false){return false;}
	}
	return true;
}

//! email alias and mailbox manager
//! (only for sites hosted on the main demosphere server)
//! This is only a front end. All work (validation, submition) is done by an external
//! script that runs as suid (user "email-config-update"), so that it can change config files.
function demosphere_emails_hosted_form()
{
	global $base_url,$demosphere_config,$currentPage;
	$currentPage->title = t('Email aliases manager');
	$currentPage->addCssTpl('demosphere/css/demosphere-emails.tpl.css');
	$currentPage->addJs('lib/jquery.js');
	$domain=demosphere_emails_hosted_domain();

	if($domain===false){fatal('Your site has not been configured with an email server. Please contact the system administrator.');}

	$out='';

	// normal pages (use header/footer)
	require_once 'dlib/array-of-arrays.php';
	$currentPage->addJs (array_of_arrays_js_translations(),'inline');
	$currentPage->addJs ('dlib/array-of-arrays.js');
	$currentPage->addCss('dlib/array-of-arrays.css');
	$currentPage->addJs ('demosphere/js/demosphere-emails.js');

	$data=demosphere_emails_hosted_fetch_data();
	
	$aliases=   false;
	$mailboxes=	false;
	if(isset($_POST['aliases'  ]))
	{
		dlib_check_form_token('email-aliases');
		$aliases=   json_decode($_POST['aliases'  ],true);
		$mailboxes=	json_decode($_POST['mailboxes'],true);

		foreach($aliases as &$alias)
		{
			// aliases dests are returned by aofa as string, format of $data (used here and in remote prog) expects array
			$alias['dests']=explode(',',$alias['dests']);
			// new aliases in form have empty type, set them to 'ordinary'
			if($alias['type']===''){$alias['type']='ordinary';}
		}
		unset($alias);

		$ok=demosphere_emails_hosted_write_data(['aliases'=>$aliases,'mailboxes'=>$mailboxes]);
		if($ok===true)
		{
			// before saving, check for change in contact_email (but only save it later, after validation)
			$oldContact=val(array_flip(dlib_array_column($data['aliases'],'type')),'contact');
			$newContact=val(array_flip(dlib_array_column($aliases        ,'type')),'contact');
			if($oldContact===$demosphere_config['contact_email'] &&
			   $newContact!==$oldContact)
			{
				$demosphere_config['contact_email']=$newContact;
				variable_set('demosphere_config','',$demosphere_config);
				dlib_message_add(t('Contact email changed. Also updating in Demosphere config.'));
			}

			$aliases=false;
			$mailboxes=false;
			// Update aliases before reload. 
			demosphere_emails_received_update_aliases();
		}
		else
		{
			if(isset($ok['error']))
			{
				$errorTranslations=
					['Invalid email address :'=>t('Invalid email address :'),
					 'No destinations for alias :'=>t('No destinations for alias :'),
					 'This destination address does not exist. Did not find an alias or a mailbox called:'=>
					 t('This destination address does not exist. Did not find an alias or a mailbox called:'),
					 'Invalid alias destination email address :'=>t('Invalid alias destination email address :'),
					 'Invalid mailbox email address :'=>t('Invalid mailbox email address :'),
					 'The password is too short for :'=>t('The password is too short for :'),
					 'Found circular alias :'=>t('Found circular alias :'),
					 'ERROR: '=>t('ERROR: '),
					];
				dlib_message_add(ent(str_replace(array_keys($errorTranslations),$errorTranslations,$ok['error'])),'error');
			}
			if(isset($ok['fatal'])){fatal($ok['fatal']);}
		}
	}

	// ******* load (or reload) existing email data (aliases, dovecot mailboxes, smtp pass)
	if($aliases===false)
	{
		$data=demosphere_emails_hosted_fetch_data();
		$aliases  =$data['aliases'];
		$mailboxes=$data['mailboxes'];
	}	

	$out.='<form method="post">';
	$out.=dlib_add_form_token('email-aliases');

	$out.='<h1 class="page-title">'.t('Alias and mailbox manager for domain: ').' <strong>'.ent($domain).'</strong></h1>';
	$out.='<p>'.t('You can create and manage "email addresses" here. To send email from any of these addresses (aliases or mailboxes) you must configure your usual email client (thunderbird, outlook, evolution, gmail...). There are <strong>two different ways</strong> of configuring your email client:</p><ul><li>Send mail <strong>from an alias : </strong> Create a new "identity" for your existing email address. For example if you usually use a mail called "roger.xyz@gmail.com", and you want to send email as "roger@@domain", you will need to: 1) add an alias below "roger@@domain" with a destination "roger.xyz@gmail.com" and then 2) go to your gmail account and add an "identity" called roger@@domain. Gmail will send an email to confirm the new identity. If you use Thunderbird, just click on "View account settings" and then "Manage identities".</li><li>Send mail <strong>from a mailbox : </strong> create a new mail "account". This is the usual way of configuring your email. You will need to 1) create a new mailbox in the mailbox manager here. 2) configure your email client with the new POP/IMAP account.</li></ul><p>',
				  ['@domain'=>$domain,'!url'=>$base_url.'/configure/emails']).'</p>';

	$out.='<div id="aliases-section">';
	$out.='<h2>'.t('Alias manager').'</h2>';
	$out.='<p>'.t('Example: an <strong>alias</strong> called "roger@@domain" with <strong>destinations</strong> "tom@riseup.net,bob@@domain,zzz@gmail.com" means that a mail sent to roger@@domain will actually be sent to all three addresses : tom@riseup.net,bob@@domain,zzz@gmail.com.</p><p>You can define as many aliases as you want. For each alias, you can define one or more destination emails. Destination emails are separated by a comma. You can put anything you want in the comments field. It can help you remember what this alias is used for.</p><p>Note: Demosphere sends emails to addresses that might be defined as aliases here. You can configure <a href="!url">that here</a>.',['@domain'=>$domain,'!url'=>$base_url.'/configure/emails']).'</p>';


	$columns=[[false,'alias',],
			  ['dests',t('Recipients'),],
			  ['comments',t('Comments'),],
			  ['type','type',],
			 ];
	$out.='<textarea name="aliases" class="array_of_arrays" id="aliases" data-aofa="'.ent(json_encode($columns)).'">'.
			ent(json_encode($aliases)).
			'</textarea>';
	$out.='<p><input type="submit" value="'.t('Save').'"></p>';
	$out.='</div>';

	$out.='<div id="mailboxes-section">';
	$out.='<h2>'.t('Mailbox manager').'</h2>';
	$columns=[[false,'mailbox',],
			  ['password',t('Password'),],
			  ['type','type',],
			 ];
	$out.='<p>'.t('You can create POP/IMAP mailboxes here.</p><ul><li>The <strong>mailbox</strong> is an address at your domain: example@@domain</li><li>The <strong>password</strong> must be at least 6 characters long.</li></ul><p>You can configure your mail client software (thunderbird, outlook, evolution, gmail...) to use the mailbox created here. When configuring your email client, here is some information you might be asked. Many email clients will automatically find some of this information, so you will probably not need to enter everything.<br/>You can also use these emails addresses on our webmail: <a href="https://webmail.demosphere.net">https://webmail.demosphere.net</a></p><ul><li><strong>Server type : </strong>POP or IMAP</li><li><strong>Port : </strong>For POP: 110 or for IMAP: 143 </li><li><strong>Server Name : </strong>@domain</li><li><strong>User Name : </strong>example@@domain</li><li><strong>Connection security : </strong>TLS</li><li><strong>Authentification method : </strong>Normal password</li><li><strong>Outgoing server (SMTP) : </strong>For normal home users: use the default server for your setup or access provider; For mobile users, or for webmail users (gmail), you can use: <br />server: demosphere.net  ; port:25 (or 26 if 25 is blocked); TLS)<br />username: @smtp_user  <br />password: @smtp_pass</li></ul><p>'
				  ,['@domain'=>$domain,'!url'=>$base_url.'/configure/emails',
					'@smtp_user'=>$demosphere_config['site_id'],
					'@smtp_pass'=>$data!==false ? $data['smtp_pass'] : '?' ]).
		'</p>';
	$out.='<textarea name="mailboxes" class="array_of_arrays" id="mailboxes" data-aofa="'.ent(json_encode($columns)).'">'.
			ent(json_encode($mailboxes)).
			'</textarea>';
	$out.='<p><input type="submit" value="'.t('Save').'"></p>';

	$out.='</form>';
	$out.='</div>';


	return $out;
}




// *************************************************************
// ********* Received emails: notifications and alias subscriptions - moderators, contributors...
// *************************************************************

//! List of email notification types.
function demosphere_emails_notification_tags($tag=false)
{
	$tags=[
		   'repetition-disabled'=>
		   [  
			'name'=>t('Repetition disabled'),
			'description'=>t('Email is sent when an automatic repetition was disabled because the organizer did not confirm any events after he was requested to do so twice.'),
			  ],
		   'comment'=>
		   [  
			'name'=>t('Comment'),
			'description'=>t('Email sent when a visitor adds a comment.'),
			  ],
		   'self-edit'=>
		   [  
			'name'=>t('Self edit'),
			'description'=>t('Email sent when a user edits an event using a self-edit link.'),
			  ],
		   'non-admin-event'=>
		   [  
			'name'=>t('Non admin edit'),
			'description'=>t('Email sent when a non admin or moderator edits an event, and this is not a self-edit.'),
			  ],
		   'archive-sent'=>
		   [  
			'name'=>t('Archive sent'),
			'description'=>t('Email copy sent when a moderator sends an email using the event edit form. This copy is meant for achival.'),
			  ],
		   'public-form'=>
		   [  
			'name'=>t('Public form'),
			'description'=>t('Email sent when a visitor requests the publishing of an event by using the public form.'),
			  ],
		   'register'=>
		   [  
			'name'=>t('Registration'),
			'description'=>t('Email sent when a user creates a new account.'),
			  ],
		   ];

	if($tag!==false){return $tags[$tag];}
	return $tags;
}

//! Returns a list of moderator email addresses that are subscribed to a tag.
function demosphere_emails_notification_addresses($tag)
{
	global $demosphere_config;
	$moderators=User::fetchList('WHERE role IN (%d,%d)',
								User::roleEnum('admin'),
								User::roleEnum('moderator'));
	$res=[];
	foreach($moderators as $uid=>$moderator)
	{
		$options=val($moderator->data,'emails-received-options',[]);
		if(val($options,'never'  )===false &&
		   val($options,$tag,true)===true  &&
		   $moderator->email!==''              ){$res[$uid]=$moderator->email;}
	}
	return $res;
}

//! Send an email to all moderators that are subscribed to a given tag
//! Accepts html or text. Optionally ($text===true) will convert html to text.
function demosphere_emails_notification_send($tag,$subject,$html=false,$text=false)
{
	global $base_url,$demosphere_config;	

	$recipients=demosphere_emails_notification_addresses($tag);
	foreach($recipients as $uid => $recipient)
	{
		$tagDesc=demosphere_emails_notification_tags($tag);

		require_once 'lib/class.phpmailer.php';
		$mail=new PHPMailer(false);
		$mail->CharSet="utf-8";
		$mail->SetFrom($demosphere_config['contact_email']);
		$mail->AddAddress($recipient);

		$unsubscribeUrl=$base_url.'/user/'.intval($uid).'/emails-received-options';

		// ** List-Id
		$mail->AddCustomHeader('List-Id',
							   '<'.str_replace('_','-',$tag).'.moderators.'.parse_url($demosphere_config['std_base_url'])['host'].'>');
		// ** unsubscribe header
		$mail->AddCustomHeader('List-Unsubscribe',
							   '<'.$unsubscribeUrl.'>');

		// *** subject
		$mail->Subject='['.mb_strtolower($tagDesc['name']).'] '.$subject;

		// *** footer
		$footerHtml='<p><br/></p><hr/><p>'.t('You can unsubscribe from these messages from the "emails" section of you profile:').
			'<br/><a href="'.ent($unsubscribeUrl).'">'.ent($unsubscribeUrl).'</a></p>';
		$footerText="\n\n".Html2Text::convert($footerHtml);

		// *** body
		if($html!==false)
		{
			$mail->MsgHTML($html.$footerHtml);
			$mail->AltBody = ($text===false ? str_replace("\n","\r\n",Html2Text::convert($html)) : $text).
				$footerText;
		}
		else
		{
			$mail->Body=$text.$footerText;
		}
		$mail->Send();
	}
}

//! Displays form to moderator where he can choose if he wants to receive emails from different sources.
function demosphere_emails_received_options($uid)
{
	global $base_url,$demosphere_config,$currentPage;
	$currentPage->title = t('Email options');
	$currentPage->addJs ('lib/jquery.js');
	$currentPage->addJs ('$(function(){'.
						 '$("#edit-never").change(function(){$("#email-wrap").toggleClass("disabled",$(this).is(":checked"));}).change();'.
						 '});','inline');
	$currentPage->addCssTpl('demosphere/css/demosphere-emails.tpl.css');
	$cuser=User::fetch($uid);
	$options=val($cuser->data,'emails-received-options',[]);
	$items=[];
	$items['title']=['html'=>'<h1 class="page-title">'.ent(t('Moderator email options for "@name"',['@name'=>$cuser->login])).'</h1>'];

	$items['never']=['type'=>'checkbox',
					 'title'=>t('Never'),
					 'description'=>t('Never receive any of the emails below or other future types of emails like those.'),
					 'default-value'=>val($options,'never'),
				  ];


	$items['wrap-start']=['html'=>'<div id="email-wrap">'];

	// ***** Emails in aliases (contact & moderators)
	require_once 'demosphere-misc.php';
	if(demosphere_emails_hosted_domain()!==false)
	{
		$aliases=demosphere_emails_hosted_fetch_data()['aliases'];
		$types=array_flip(dlib_array_column($aliases,'type'));

		//$items['contact']=['type'=>'checkbox',
		// 				   'title'=>t('Contact emails'),
		// 				   'description'=>t('Receive emails sent to !contact',['!contact'=>$demosphere_config['contact_email']]),
		// 				   'default-value'=>val($options,'contact',true),
		// 				   ];

		$items['moderators']=['type'=>'checkbox',
							  'title'=>t('Moderators emails'),
							  'description'=>t('Receive emails sent to !moderators',['!moderators'=>$types['moderators']]),
							  'default-value'=>val($options,'moderators',true),
							  ];
		if($cuser->contributorLevel>0)
		{
			$items['contributors']=['type'=>'checkbox',
									'title'=>t('Contributor emails'),
									'description'=>t('Receive emails sent to !contributors. This is an address to communicate with all other contributors. You will only receive these emails if you have given an opinion in the past month. Don\'t confuse this with the notifications you receive when an event needs your opinion.',['!contributors'=>$types['contributors']]),
									'default-value'=>val($options,'contributors',true),
									];
		}
	}

	// ***** Email notifications 
	$tagDescs=demosphere_emails_notification_tags();
	if(!$cuser->checkRoles('admin','moderator')){$tagDescs=[];}
	else
	{
		$items['notifications-start']=['html'=>'<fieldset id="notifications"><legend>'.t('Email notifications').'</legend>'];
		foreach($tagDescs as $tag=>$tagDesc)
		{
			$items[$tag]=['type'=>'checkbox',
						  'title'=>$tagDesc['name'],
						  'description'=>$tagDesc['description'],
						  'default-value'=>val($options,$tag,true),
						  'attributes'=>['class'=>["modtag"]],
						 ];
		}
		$items['notifications-end']=['html'=>'</fieldset>'];
	}
	$items['wrap-end']=['html'=>'</div>'];

	$items['save']=['type'=>'submit',
					'value'=>t('Save'),
					'redirect'=>$base_url.'/user/'.$uid,
					'submit'=>function($items) use($cuser,$tagDescs,$options)
			{
				foreach($tagDescs as $tag=>$tagDesc)
				{
					$options[$tag]=(bool)$items[$tag]['value'];
				}
				$options['never']=(bool)$items['never']['value'];
				//if(isset($items['contact'   ])){$options['contact'     ]=(bool)$items['contact'     ]['value'];}
				if(isset($items['moderators'  ])){$options['moderators'  ]=(bool)$items['moderators'  ]['value'];}
				if(isset($items['contributors'])){$options['contributors']=(bool)$items['contributors']['value'];}
				$cuser->data['emails-received-options']=$options;
				$cuser->save();
				demosphere_emails_received_update_aliases();
				dlib_message_add(t('Moderator email options updated succesfully.'));
			}
					];
	require_once 'dlib/form.php';
	return form_process($items,['id'=>'emails-received-options']);
}

//! Recreates list of recipients for "moderators", "contact" and "contributors" email aliases.
//! This should be called when roles are changed.
//! It is also called from cron, in case users where deleted, or updated.
function demosphere_emails_received_update_aliases()
{
	global $demosphere_config;
	require_once 'demosphere-misc.php';
	if(demosphere_emails_hosted_domain()===false){return;}

	$domain=demosphere_emails_hosted_domain();
	$aliases=demosphere_emails_hosted_fetch_data()['aliases'];
	$aliasesByType=array_flip(dlib_array_column($aliases,'type'));
	$users=User::fetchList('WHERE role IN (%d,%d) OR contributorLevel>0',
						   User::roleEnum('admin'),
						   User::roleEnum('moderator'));

	// Check for strange situation  (just in case) : only allow one alias for non-"ordinary" type
	foreach(array_keys($aliases) as $alias)
	{
		if($aliases[$alias]['type']!=='ordinary' && $alias!==$aliasesByType[$aliases[$alias]['type']])
		{
			$aliases[$alias]['type']='ordinary';
		}
	}

	foreach([/*'contact',*/'moderators','contributors'] as $type)
	{
		// **** Create lists of email addresses for contact, moderators and contributors
		$recipientList=[];
		//if($type==='contact'){$recipientList[]=$demosphere_config['site_id'].'-mailbox@demosphere.net';}

		foreach($users as $cuser)
		{
			if($type!=='contributors' && !$cuser->checkRoles('admin','moderator')){continue;}
            if($type==='contributors' && !$demosphere_config['enable_opinions']){continue;}
			// Skip non-verified emails
			if($cuser->email===''){continue;}
			// Skip non-existent hosted email addresses. 
			if( demosphere_emails_hosted_is_valid($cuser->email) && 
			   !demosphere_emails_hosted_is_valid($cuser->email,true)){continue;}
			// Do not include contributors that have not given an opinion for a while.
			if($type==='contributors' && !$cuser->checkRoles('admin','moderator'))
			{
				$lastOpinion=(int)db_result('SELECT MAX(time) FROM Opinion WHERE userId=%d',$cuser->id);
				if($lastOpinion<time()-3600*24*30){continue;}
			}

			$options=val($cuser->data,'emails-received-options',[]);
			if(val($options,'never'  )===false)
			{
				if(val($options,$type,true)===true){$recipientList[]=$cuser->email;}
			}
		}

		// determine $aliasAddress
		$aliasAddress=val($aliasesByType,$type);
		// special case: defaults when alias is not created yet
		if($aliasAddress===false)
		{
			//if($type==='contact'     ){$aliasAddress=$demosphere_config['contact_email'];;}
			if($type==='moderators'  ){$aliasAddress=t('moderators@'  ).$domain;}
			if($type==='contributors'){$aliasAddress=t('contributors@').$domain;}
		}
		$aliasExtraAddress=str_replace('@','-extra@',$aliasAddress);

		if(isset($aliases[$aliasExtraAddress])){$recipientList[]=$aliasExtraAddress;}

		// **** Save to aliases file	
		if(demosphere_emails_hosted_is_valid($aliasAddress))
		{
			if(count($recipientList)===0){unset($aliases[$aliasAddress]);}
			else
			{
				$typeComments=[//'contact'     =>t("Your site's contact address."),
							   'moderators'  =>t("An email address for your team's internal communications."),
							   'contributors'=>t("An email address for all contributors (opinions)."),
							   ];
				$aliases[$aliasAddress]['dests'   ]=$recipientList;
				$aliases[$aliasAddress]['type'    ]=$type;
				$aliases[$aliasAddress]['comments']=$typeComments[$type]." ".
					t('This alias is automatically updated with all users of your site that are moderators or admins. Users can unsubscribe from their profile.')."\n".
					t('If you need to add extra recipients, create an alias called !extraAddress',['!extraAddress'=>$aliasExtraAddress]);
			}
		}
	}

	$ok=demosphere_emails_hosted_write_data(['aliases'=>$aliases]);
	if($ok!==true)
	{
		dlib_message_add('Failed to update aliases: '.ent(val($ok,'error','').val($ok,'fatal','')),'error');
		return false;
	}
	return true;
}

// ***************************************************
// *************  email bounce management (see also: demosphere-stats.php and demosphere-email-subscription.php)
// ***************************************************


//! Parse email bounces. Only for hosted calendars.
//! This is a bit slow and consumes memory.
function demosphere_emails_bounces_parse($tsOnly=false)
{
	global $demosphere_config;
	if(!$demosphere_config['hosted']){fatal('demosphere_emails_bounces_parse: not on hosted demosphere site');}
	$bounces=explode("\n",file_get_contents('/var/local/demosphere/bounces.log'));
	$res=[];
	foreach($bounces as $line)
	{
		$parts=explode("#",$line);
		// FIXME: there are quite a few (20%) bounces that dont have X-Failed-Recipients :-(
		if(count($parts)!==3){continue;}
		// Only bounces for our site's email_subscription_from email
		if(!preg_match('@^To:\s+'.preg_quote($demosphere_config['email_subscription_from'],'@').'@i',$parts[1])){continue;}
		if(!preg_match('@^X-Failed-Recipients: (.*\@[^\s]+)@i',$parts[2],$m)){continue;}
		$email=$m[1];
		if(!preg_match('@^From [^\s]+\s(.*)$@i',$parts[0],$m)){continue;}
		$ts=strtotime($m[1]);
		$email=mb_substr($email,0,90);// avoid strange security cases
		if($tsOnly){$res[]=$ts;}
		else       {$res[$email][]=$ts;}
	}
	return $res;
}


//! Since this is a bit slow, it is called from cron and sets result in var.
//! It is displayed in panel.
function demosphere_emails_bounces_past_24h()
{
	global $demosphere_config;
	if(!$demosphere_config['hosted']){return false;}
	$tss=demosphere_emails_bounces_parse(true);
	$ct=0;
	$begin=time()-24*3600;
	foreach($tss as $ts){if($ts>$begin){$ct++;}}
	variable_set('email_bounces_past_24h','',$ct);
	return $ct;
}

?>