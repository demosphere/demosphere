<?php

// ****************************************************************
// **********  terms for free taging 
// ****************************************************************

//! Setup for class information for terms displayed/edited by dbobject-ui
function demosphere_terms_class_info(&$classInfo)
{
	global $currentPage,$demosphere_config;
	// *********** common
	$currentPage->addJs('lib/jquery.js');

	$classInfo['tabs']=function(&$tabs,$object)
		{
			$tabs['add']['text']=t('Add term');
		};

	$classInfo['new']=
		['vocab'=>['type'=>'select',
				   'title'=>t('Vocab'),
				   'options'=>dlib_array_column($demosphere_config['term_vocabs'],'name')
				  ]];
}


function demosphere_terms_autocomplete()
{
	$typed=$_GET['term'];
	$vocab=intval($_GET['vocab']);

	// Only suggest last term, when there are multiple comma-separated terms 
	$terms=explode(',',$typed);
	$term=array_pop($terms);
	$term=trim($term);
	if($term===''){return [];}
	
	$res=db_one_col("SELECT Term.id,Term.name FROM Term WHERE ".
					"Term.vocab=%d AND ".
					"LOWER(name) LIKE LOWER('%%%s%%') AND ".
					// Only show terms that are in published events
					"EXISTS (SELECT * FROM Event,Event_Term WHERE Event_Term.event=Event.id AND Event_Term.term=Term.id AND Event.status=1) ".
					// Show most popular terms first (helps avoid alternate spellings).
					'ORDER BY '.
					'(SELECT COUNT(*) FROM Event,Event_Term WHERE Event_Term.event=Event.id AND Event_Term.term=Term.id AND Event.status=1) DESC '.
					'LIMIT 100 ',
					$vocab,
					$term
					);

	if(count($terms)){$res=preg_replace('@^@',implode(', ',$terms).', ',$res);}
	
	// prefix keys with "t" to avoid reordering in js
	$res=array_combine(preg_replace('@^@','t',array_keys($res)),$res);

	return $res;
}

?>