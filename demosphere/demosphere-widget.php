<?php
/**
 * Returns an array of all options that can be configured by user.
 *
 * This array is also transmitted to javascript (json). 
 */
function demosphere_widget_options()
{
	global $demosphere_config;
	$res=[['name'=>'layout'           ,'type'=>'textfield'  ,'default'=>'day-lists'],
		  ['name'=>'width'            ,'type'=>'int'        ,'default'=>false],
		  ['name'=>'height'           ,'type'=>'int'        ,'default'=>300],
		  ['name'=>'show-scrollbar'   ,'type'=>'boolean'    ,'default'=>true],
		  ['name'=>'show-city'        ,'type'=>'boolean'    ,'default'=>true],
		  ['name'=>'city-color'       ,'type'=>'color'      ,'default'=>false],
		  ['name'=>'text-color'       ,'type'=>'color'      ,'default'=>false],
		  ['name'=>'text-size'        ,'type'=>'textfield'  ,'default'=>'13px'],
		  ['name'=>'day-header-format','type'=>'textfield'  ,'default'=>'%A, %e %B %Y'],
		  ['name'=>'day-header-color' ,'type'=>'color'      ,'default'=>false],
		  //array('name'=>'date-format'    ,'type'=>'select','default'=>5/*FIXME*/),
		  ['name'=>'time-format'      ,'type'=>'textfield'  ,'default'=>'%H:%M'],
		  ['name'=>'time-color'       ,'type'=>'color'      ,'default'=>false],
		  ['name'=>'extra-message'    ,'type'=>'textfield'  ,'default'=>val($demosphere_config,'widget_extra_message')],
		  ['name'=>'extra-message-size','type'=>'textfield' ,'default'=>'10px'],
		  ['name'=>'custom-css'       ,'type'=>'textfield'  ,'default'=>false],
		  ['name'=>'limit'            ,'type'=>'int'        ,'default'=>false],
		  ['name'=>'near-lat-lng-use-map' ,'type'=>'boolean','default'=>false],
		  ['name'=>'near-lat-lng-distance','type'=>'float'  ,'default'=>false],
		  ['name'=>'near-lat-lng-latitude-and-longitude','type'=>'textfield','default'=>false],
		  ['name'=>'map-shape'        ,'type'=>'textfield'  ,'default'=>false],
		  ['name'=>'most-visited'     ,'type'=>'int'        ,'default'=>false],
		  ['name'=>'max-nb-events-per-day','type'=>'int'    ,'default'=>false],
		  ['name'=>'usercal-policy'   ,'type'=>'radio'      ,'default'=>0],
		  //['name'=>'select-terms-expression','type'=>'textfield'    ,'default'=>false],
		  ['name'=>'select-vocab-term','type'=>'textfield'  ,'default'=>false],
		  ['name'=>'select-vocab-term-or','type'=>'textfield','default'=>false],
		  ['name'=>'select-topic'     ,'type'=>'textfield'  ,'default'=>false],
		  ['name'=>'select-city-id'   ,'type'=>'int'        ,'default'=>false],
		  ['name'=>'select-fp-region' ,'type'=>'textfield'  ,'default'=>false],
		  ['name'=>'select-place-reference-id','type'=>'int','default'=>false],
		 ];
	return $res;
}

/**
 * Form builder for widget config form
 *
 * Builds the form that allows users to choose set options for their widget (calendar).
 * Note that this form is never actually submited but instead uses ajax for updating.
 * This is called directly from a request.
 *
 * @ingroup forms
 */
function demosphere_widget_config_form($isUserCalendarConfigForm=false)
{
	global $demosphere_config,$user,$base_url,$currentPage;
	require_once 'demosphere-event-list-form.php';
	require_once 'demosphere-date-time.php';

	$currentPage->title=t('Widget options');

	// Whose calendar is this. Note that admins can edit other users widgets.
	// So it is possible that $user->id!=$widgetUid
	$widgetUid=isset($_GET['uid']) ? intval($_GET['uid']) : false;

	// if the widget is user-specific, we need to be logged in to configure it 
	// (so we redirect to login form)
	if($user->id==0 && $widgetUid!==false)
	{
		dlib_redirect($base_url.'/login?destination=widget-config?uid='.$widgetUid);
	}

	// Always use the user-specific config for logged-in users (redirect)
	if($user->id!=0 && $widgetUid===false)
	{
		dlib_redirect($base_url.'/widget-config?uid='.$user->id);
	}

	// only admins can edit somebody else's widget
	if($user->id!=0 && $widgetUid!==intval($user->id) && !$user->checkRoles('admin'))
	{
		dlib_permission_denied_403("wrong user: you are not allowed to edit another user's widget");
	}

	$wuser=false;
	if($widgetUid){$wuser=$widgetUid===intval($user->id) ? $user: User::fetch($widgetUid);}

	// reset values
	if($user->id!=0 && isset($_GET['reset']))
	{
		if(val($_GET,'form_token')!==dlib_get_form_token('demosphere_widget_config_form'))
		{dlib_permission_denied_403('invalid form token',false);}

		$wuser->data['demosphere_widget_config']=[];
		require_once 'demosphere-user-calendar.php';
		$wuser->data['calendar']=demosphere_user_calendar_defaults($wuser);
		$wuser->save();
		// hidden option: only used for unit testing: deletes all selections and private events
		if(isset($_GET['full']))
		{
			db_query("DELETE FROM user_calendar_selection WHERE user=%d",$wuser->id);
			// delete private events (uid,not shown on frontpage)
			db_query("DELETE FROM Event WHERE authorId=%d AND showOnFrontpage=0",$wuser->id);
		}
		dlib_redirect($base_url.'/'.($isUserCalendarConfigForm ? 'user-calendar-config' : 'widget-config').'?uid='.$widgetUid);
	}

	$values=demosphere_widget_option_values($widgetUid,$errors);
	//var_dump($values);
	if($errors!==false){dlib_message_add('bad values for:'.implode(',',$errors),'error');}	
	
	$currentPage->addJsConfig('demosphere',['map_center_latitude','map_center_longitude','map_zoom','mapbox_access_token']);
	$currentPage->addJs( 'lib/jquery.js');
	// send option array to js
	$currentPage->addJsVar('widgetOptions',demosphere_widget_options());
	// set widget user as global js variable
	$currentPage->addJsVar('widgetUid',($widgetUid===false ? false : $widgetUid));
	$currentPage->addJs( 'dlib/merge-calls.js');
	$currentPage->addJs( "demosphere/js/demosphere-widget-config-form.js");
	$currentPage->addJs( 'lib/jquery-ui-1.12.1.custom/jquery-ui.js');
	$currentPage->addCssTpl('demosphere/css/demosphere-event-list-form.tpl.css');
	$currentPage->addCssTpl("demosphere/css/demosphere-widget-config-form.tpl.css");
	$currentPage->addCssTpl("demosphere/css/demosphere-calendar.tpl.css");
	$currentPage->addCss('lib/jquery-ui-1.12.1.custom/jquery-ui.min.css');
	$currentPage->addJs("demosphere/js/demosphere-event-list-form.js");
	$currentPage->addJsTranslations(['invalid time format: use hh:mm'=>t('invalid time format: use hh:mm'),
									 'Successfully added event:'=>t('Successfully added event:'),
									 'invalid url: example: http://example.com/my-event'=>t('invalid url: example: http://example.com/my-event'),
									]);

	$currentPage->addCss('lib/leaflet/leaflet.css');
	$currentPage->addJs('lib/leaflet/leaflet.js');

	// main form title
	$form['title'] = 
		['html'=>'<h1 class="page-title">'.t("Show a calendar on your web site").
		 ($widgetUid && $widgetUid!=$user->id ? ' (user: '.ent($widgetUid).')':'').
		 '</h1>'.
		 (!$widgetUid ? '':
		  '<p>'.
		  '(<a target="_blank" href="'.$base_url.'/usercal/'.intval($widgetUid).'">'.
		  t('You can also view the calendar directly on @siteName.',
			['@siteName'=>$demosphere_config['site_name']]).'</a>)'.
		  '</p>')];
	
	if($user->id==0)
	{
		$form['head'] = ['html'=>'<div id="widget-head-wrapper" class="custom-field">'.
						 t('For more options and easier handling, you can <a href="!base_url/login">create an account</a> (<a href="!base_url/user?destination=widget-config">or log-in</a> if you already have an account).',['!base_url'=>$base_url]).
						 '<div class="description">'.
						 t('Being logged-in would allow you to add your own events and select individual events for your calendar.').
						 '</div></div>'];
	}

	$form['embed'] = ['type'=>'textarea',
					  'title'=>t('Copy this on your site'),
					  'resizable' =>false,
					  'description'=>t("To make this calendar appear on your site, you have to copy the following HTML code.").' '.($user->id!=0 ? t("When you change the configuration of your calendar, this HTML code might change. A red frame will tell you that you need to copy the new version of this code to replace the old version on your site.") : t("Since you are not logged-in, each time you change the configuration of your calendar, this HTML code will change. You will you need to copy the new version of this code to replace the old version on your site.")).' (<a id="embed-advanced-button" href="javascript:void(0);">'.t('advanced embedding options').'</a>)',
					 ];

	$form['embed-advanced'] = ['type'=>'textarea',
							   'title'=>t('Advanced embedding option'),
							   'resizable' =>false,
							   'description'=>t("By coping this second HTML you can get a more flexible layout (it uses javascript generated content instead of a fixed-height iframe) . It also allows you to directly configure the calendar in your site's own CSS. <strong>Warning:</strong> this HTML includes javascript from !sitename on your site. This type of embedding is used by many web services, but you should be aware of the <a target='_blank' href='http://en.wikipedia.org/wiki/Cross-site_scripting'>security concerns</a> involved. This means that you trust that !sitename has no malicious intent. <br /><br />For more advanced usage (PHP code, custom rendering) please contact us. The un-rendered events are available in JSON format at <a href='!jsonurl'>this url</a>",['!sitename'=>$demosphere_config['site_name'],'!jsonurl'=>$base_url.'/widget-json?uid='.intval($widgetUid)]),
							  ];

	$form['example'] = ['html'=>'<div id="widget-example-wrapper" class="custom-field">'.
						'<h4>'.t('Example display:').'</h4>'.
						'<div class="description">'.t('This is approximately how the calendar will look on your site. This example calendar will be immediately updated each time you change an option below. You can resize this calendar by moving the handle on the bottom-right corner.').'</div>'.
						'<div id="widget-example"><div id="widget-example-content"></div></div>'.
						'</div>'];


	$form['display']=
		[
			'type' => 'fieldset',
			'title' => t('Configure how your calendar will look'),
			'collapsible' => true,
			'collapsed' => true,
			'attributes' => ['class'=>['display']],
		];

	$form['width'] = 
		['type' => 'textfield',
		 'title' => t('Width'),
		 'description' => t('Width of the embedded calendar in pixels. You can also change this field by resizing the calendar. If you leave this field empty, the calendar will be as wide as the area that contains it.'),
		 'default-value'=>$values['width'],
		];
	$form['height'] = 
		['type' => 'textfield',
		 'title' => t('Height'),
		 'description' => t('Height of the embedded calendar in pixels. You can also change this field by resizing the calendar. Due to technical restrictions (iframes) you must fill this value. If you do not want a fixed height calendar, then you must use the "Advanced embedding option".'),
		 'default-value'=>$values['height'],
		];

	$form['layout'] = 
		['type' => 'select',
		 'title' => t('Layout'),
		 'options'=>demosphere_widget_layouts(),
		 //'description' => t('.'),
		 'default-value'=>$values['layout'],
		];

	$form['show-scrollbar'] = 
		['type' => 'checkbox',
		 'title' => t('Show scrollbar'),
		 'description' => t('Whether or not to show the vertical scrollbar if the events do not  fit in the calendar area.'),
		 'default-value'=>$values['show-scrollbar'],
		];

	$form['text-color'] = 
		['type' => 'color',
		 'title' => t('Text color'),
		 'default-value'=>$values['text-color'],
		];

	$form['text-size'] = 
		['type' => 'textfield',
		 'title' => t('Text size'),
		 'description' => t('The font-size expressed in px or % (for example 12px or 130%) '),
		 'default-value'=>$values['text-size'],
		];

	// buid a date format selector with demos_format_date and custom date formats
	$dateFormatSelect='<select class="input-suggestion">'.
		'<option value="">('.t('choose format').')</option>'.
		'<option value="">' .t('none'         ). '</option>';
	require_once 'demosphere-date-time.php';
	foreach(['short-day-month','week-and-day','week-day-month'] as $format)
	{
		$dateFormatSelect.='<option value="|'.$format.'">'.demos_format_date($format,time()).
			'</option>';
	}
	foreach(['%A %e %B %Y'] as $format)
	{
		$dateFormatSelect.='<option value="'.ent($format).'">'.strftime($format,time()).'</option>';
	}
	$dateFormatSelect.='</select>';


	$form['day-header-format'] = 
		['type' => 'textfield',
		 'title' => t('Day header format'),
		 'description' => t('How the name of each day should be displayed at the beginning of each day. Leave this blank if you do not want to display anything at the begining of each day. You can select predefined options on the right or use your own format string. (<a target="_blank" href="http://php.net/manual/en/function.strftime.php">syntax of the format string</a>)'),
		 'field-suffix'=>$dateFormatSelect,
		 'default-value'=>$values['day-header-format'],
		];

	$form['day-header-color'] = 
		['type' => 'color',
		 'title' => t('Day header color'),
		 'default-value'=>$values['day-header-color'],
		];

	// buid a date format selector with demos_format_date and custom date formats
	$dateFormatSelect='<select class="input-suggestion">'.
		'<option value="">('.t('choose format').')</option>'.
		'<option value="">' .t('none'         ). '</option>';
	foreach(['shorter-date-time','short-date-time','approx-short-date-time','full-date-time'] as $format)
	{
		$dateFormatSelect.='<option value="|'.$format.'">'.demos_format_date($format,time()).
			'</option>';
	}
	foreach(['%R','%l:%M %p'] as $format)
	{
		$dateFormatSelect.='<option value="'.ent($format).'">'.strftime($format,time()).'</option>';
	}
	$dateFormatSelect.='</select>';

	$form['time-format'] = 
		['type' => 'textfield',
		 'title' => t('Day/time format'),
		 'description' => t('The format of the day or time displayed at the beginning of each event line.'),
		 'field-suffix'=>$dateFormatSelect,
		 'default-value'=>$values['time-format'],
		];

	$form['time-color'] = 
		['type' => 'color',
		 'title' => t('Day/time color'),
		 'description' => t('The color of the day or time at the beginning of each event line.'),
		 'default-value'=>$values['time-color'],
		];

	$form['show-city'] = 
		['type' => 'checkbox',
		 'title' => t('Show city / district'),
		 'description' => t('Whether or not to show the city / district for each event.'),
		 'default-value'=>$values['show-city'],
		];

	$form['city-color'] = 
		['type' => 'color',
		 'title' => t('City / district color'),
		 'default-value'=>$values['city-color'],
		];

	$form['extra-message'] = 
		['type' => 'textfield',
		 'title' => t('Extra message'),
		 'description' => t('A message you want to add at the end of the event list.'),
		 'default-value'=>$values['extra-message'],
		];

	$form['extra-message-size'] = 
		['type' => 'textfield',
		 'title' => t('Extra message size'),
		 'description' => t('The font-size expressed in px or % (for example 12px or 130%) '),
		 'default-value'=>$values['extra-message-size'],
		];

	$form['custom-css'] = 
		['type' => 'textfield',
		 'title' => t('Custom CSS'),
		 'description' => t('A URL to a custom CSS file. This is an advanced option. To understand this feature you need to have some knowledge in web development. This CSS file is included inside the embedded iframe, so you can use it to change all aspects of the appearance of the calendar contents. Please note that for security reasons this custom CSS will be disabled for users that are logged into !sitename. This means you will have to log-out of !sitename to see what other users will see on your site.',['!sitename'=>$demosphere_config['site_name']]),
		 'default-value'=>$values['custom-css'],
		];

	$form['display-end']=['type'=>'fieldset-end'];


	//***************** which-events

	$form['which-events']=
		[
			'type' => 'fieldset',
			'title' => t('Rules for choosing which events will be displayed'),
			'collapsible' => true,
			'collapsed' => true,
			'attributes' => ['class'=>['which-events']],
		];

	// add standard options
	demosphere_event_list_form_item('select-topic'         ,$values,$form);
	demosphere_event_list_form_item('select-city-id'       ,$values,$form);
	demosphere_event_list_form_item('select-fp-region'     ,$values,$form);
	demosphere_event_list_form_item('select-place-reference-id',$values,$form);
	demosphere_event_list_form_item('limit'                ,$values,$form);
	demosphere_event_list_form_item('near-lat-lng'         ,$values,$form);
	demosphere_event_list_form_item('more-options-fs'      ,$values,$form);
	demosphere_event_list_form_item('map-shape'            ,$values,$form);
	demosphere_event_list_form_item('max-nb-events-per-day',$values,$form);
	demosphere_event_list_form_item('most-visited'		   ,$values,$form);
	demosphere_event_list_form_item('select-vocab-term'    ,$values,$form);
	demosphere_event_list_form_item('select-vocab-term-or' ,$values,$form);

	//$form['which-events']['more-options-fs']['select-terms-expression']=
	// 	array(
	// 		  'type' => 'textfield',
	// 		  'title' => t('Complex expression on terms'),
	// 		  'collapsible' => true,
	// 		  'collapsed' => true,
	// 		  'maxlength' => 2000,
	// 		  'attributes'=>array('class'=>array('which-events')),
	// 		  );
	$form['more-options-fs-end']=['type'=>'fieldset-end'];
	$form['which-events-end'   ]=['type'=>'fieldset-end'];

	//***************** individual events

	// individual event selection is only possible for loggedin users
	if($user->id!=0)
	{

		$form['individual-events']=
			[
				'type' => 'fieldset',
				'title' => t('Add or remove individual events'),
				'collapsible' => true,
				'collapsed' => true,
				'attributes'=>['class'=>['individual-events']],
			];
		$form['add-event'] = 
			[
				'html' => 
				'<div id="add-event-wrapper" class="form-item">'.
				'<h4>'.t('Add your own external event').'</h4>'.
				'<div id="add-event">'.
				'<span>'.t('Date:' ).'</span> <input type="text" id="add-event-date"  placeholder="'.t('Ex: ').
				demos_format_date('short-day-month-year',strtotime("2013-3-21")).'" /> '.
				'<span>'.t('Time:' ).'</span> <input type="text" id="add-event-time"	placeholder="'.t('Ex: 20:30').'" /> &nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;'.
				'<span>'.t('City:' ).'</span> <input type="text" id="add-event-city"	placeholder="'.t('Ex: New-York').'" /><br/>'.
				'<span>'.t('Url:'	 ).'</span> <input type="text" id="add-event-url"	placeholder="'.t('Ex: http://example.org/event1234').'" /><br/>'.
				'<span>'.t('Title:').'</span> <input type="text" id="add-event-title" placeholder="'.t('Ex: Conference on migrants rights').'" /><br/>'.
				'<button type="button">'.t('add').'</button></div>'.
				'<div id="add-event-result"></div>'.
				'<div class="description">'.
				t('You can add events that are not on !sitename but on another web site. You can remove an event you have added by clicking on the right check box beside the event on the calendar below.',
				  ['!sitename'=>$demosphere_config['site_name']]).'</div>'.
				'</div>',
			];

		if($user->checkRoles('admin','moderator','frontpage event creator','private event creator'))
		{
			$form['create-event'] = 
				[
					'html' => 
					'<div id="add-event-wrapper" class="form-item">'.
					'<h4>'.t('Create a private event on !sitename',
							 ['!sitename'=>$demosphere_config['site_name']]).'</h4>'.
					'<div class="description">'.
					t('<a target="_blank" href="!url">Click here</a> to create an event on !sitename. Your event will not be displayed on the frontpage of !sitename, but it will be displayed on your own calendar.',
					  ['!sitename'=>$demosphere_config['site_name'],
					   '!url'=>$base_url.'/'.$demosphere_config['event_url_name'].'/add?dontShowOnFrontpage']).'</div>'.
					'</div>',
				];
		}

		$form['usercal-policy'] = 
			[
				'type' => 'radios',
				'title' => t('Policy for events you have not individually selected or rejected'),
				'options' => [t('Display the event (if it matches rules)'), 
							  t('Never display an event that has not been explicitly selected.')],
				'description' => t('You can choose what should be done if you have not yet explicitly selected or rejected an event. The first option means that your calendar will be generated automatically (according to the rules you defined), but you can still choose to remove certain events individually. The second options means that you will have to manually select each event before it appears in your calendar.'),
				'default-value'=>$values['usercal-policy'],
			];

		$form['select-events'] = 
			[
				'html' => 
				'<div id="select-events-wrapper" class="form-item"><a name="select"></a>'.
				'<h4>'.t('Select individual events').'</h4>'.
				'<div class="description">'.
				t('Select individual events you want to show or hide. The light-green or black line on the far-left tells you wether en event is visible or not on your calendar. The check boxes allow you to select (dark-green) or reject (grey) an event. Events with a grey title are events that do not match the rules you defined.').'</div>'.
				'<div id="select-events"><div id="select-events-loading">'.t('loading...').'</div></div>'.
				'</div>',
			];


		$form['individual-events-end'   ]=['type'=>'fieldset-end'];
	}

	$form['reset'] = 
		[
			'html' => 
			'<div id="reset-wrapper" class="form-item">'.
			'<a id="reset" href="'.$base_url.'/'.($isUserCalendarConfigForm ? 'user-calendar-config' : 'widget-config').'?uid='.$widgetUid.'&amp;reset&amp;form_token='.
			dlib_get_form_token('demosphere_widget_config_form').'">'.t('Reset options').'</a>'.
			'<div class="description">'.
			t('Reseting will re-initilize all of your options to default values. All of your options for your widget or your personal calendar will be erased. This will not erase your account or any other data.').'</div>'.
			'</div>',
		];
	if($user->id==0){unset($form['reset']);}

	if($isUserCalendarConfigForm){return $form;}
	require_once 'dlib/form.php';
	$formOpts=['id'=>'demosphere_widget_config_form'];
	return form_process($form,$formOpts);
}
function demosphere_widget_layouts()
{
	return ['day-lists'      =>t('Date on a separate line. Followed by one event per line.'),
			'event-per-line' =>t('Simple, one event per line. Each line begins with date.'),
			//'small-calendar' =>t('Compact calendar with clickable links.'),
			];
}

/**
 * Iframe widget : fisplays an html page with a calendar.
 *
 * This is normally displayed inside an iframe on the user's web-site.
 * This is called directly from a request.
 */
function demosphere_widget_html()
{
	global $user;

	$widgetUid=isset($_GET['uid']) ? intval($_GET['uid']) : false;

	$values=demosphere_widget_option_values($widgetUid,$errors);
	if($errors!==false){dlib_bad_request_400('bad values for:'.implode(',',$errors));}
	$events=demosphere_widget_get_calendar_events($values,$widgetUid);
	$html=demosphere_widget_render_html_calendar($events,$values);
	$css=demosphere_widget_build_css($values);

	$out='';
	$out.='<!DOCTYPE html>'."\n";
	$out.='<html xmlns="http://www.w3.org/1999/xhtml"><head>';
	$out.='<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
	$out.='<title>Demosphere widget</title>';
	$out.='<meta name="robots" content="noindex"/>';
	if($values['custom-css']!==false && $user->id==0)
	{
			$out.='<link type="text/css" rel="stylesheet" href="'.ent($values['custom-css']).'" />';
	}
	$out.='<style type="text/css" id="demosphere-widget-css">';
	$out.=$css;
	$out.='</style>';
	$out.='</head><body>';//FIXME (fix what?)
	if($values['custom-css']!==false && $user->id!=0)
	{
		$out.=t('(custom CSS disabled)').'<br/>';
	}
	$out.=$html;
	$out.='</body></html>';//FIXME (fix what?)
	return $out;
}

/**
 * Javascript widget : displays a javascript file that will display a calendar.
 * This is called directly from a request.
 */
function demosphere_widget_js()
{
	global $user;

	$widgetUid=isset($_GET['uid']) ? intval($_GET['uid']) : false;

	$values=demosphere_widget_option_values($widgetUid,$errors);
	if($errors!==false)
	{
		$html='<p>Error:bad values for:'.implode(',',$errors).'</p>';
	}
	else
	{
		$events=demosphere_widget_get_calendar_events($values,$widgetUid);
		$html=demosphere_widget_render_html_calendar($events,$values);
	}
	//$css=demosphere_widget_build_css($values);

	$js=
		'(function()'."\n".
		'{ '."\n".
		'	var html='.json_encode($html).';'."\n".
		'	var allDivs = document.getElementsByTagName("div");'."\n".
		'	var div = allDivs[allDivs.length - 1];'."\n".
		'	div.innerHTML=html;'."\n".
		'})();'.
		'';
	return ['out'=>$js,
			'headers'=>['Content-Type'=>'application/javascript']];
}

/**
 * Json "widget" : displays a javascript file with event data.
 *
 * This can be used by users to custom render their own calendar (both on server and on client). 
 * This is called directly from a request.
 */
function demosphere_widget_json()
{
	$widgetUid=isset($_GET['uid']) ? intval($_GET['uid']) : false;
	$values=demosphere_widget_option_values($widgetUid,$errors);
	if($errors!==false){dlib_bad_request_400('ERROR: bad values for:'.implode(',',$errors));}
	$events=demosphere_widget_get_calendar_events($values,$widgetUid);

	foreach(array_keys($events) as $k)
	{
		// remove full event info
		unset($events[$k]->event);
	}

	return ['events'=>$events];
}

/**
 * Json/ajax communication between widget config form on the client and the server..
 *
 * This saves user's options (if logged in) and 
 * returns to the client an example calendar (and a few other things).
 * This is called directly from a request.
 */
function demosphere_widget_config_ajax()
{
	$error='';// FIXME
	$widgetUid=isset($_GET['uid']) ? intval($_GET['uid']) : false;
	$values=demosphere_widget_option_values($widgetUid,$errors);
	$events=demosphere_widget_get_calendar_events($values,$widgetUid);
	$html=demosphere_widget_render_html_calendar($events,$values);
	$css=demosphere_widget_build_css($values);
	$embed=demosphere_widget_build_embed($values,$widgetUid);
	$res=['html'=>$html,
		  'css'=>$css,
		  'error'=>$errors,
		  'embed'=>$embed['iframe'],
		  'embedAdvanced'=>$embed['js'],
		 ];
	//var_dump($res);die('ii');
	return $res;
}

// Determines option values for this widget, either by looking at GET/POST arguments,
// or by loading option values from user if uid is specified.
// If GET/POST values are used, parsing is done by demosphere_widget_parse_client_options.
function demosphere_widget_option_values($widgetUid,&$errors=false)
{
	global $base_url;
	// if this is a user-specific widget, load data from user (not from GET args)
	if($widgetUid)
	{
		$wuser=User::fetch($widgetUid,false);
		if($wuser===null){dlib_not_found_404("invalid user");}

		// special case: if requested
		// save GET args options to user data 
		if(val($_POST,'save')==='1')
		{
			// CSRF protection using form tokens
			if(val($_POST,'form_token')!==dlib_get_form_token('demosphere_widget_config_form') &&
			   val($_POST,'form_token')!==dlib_get_form_token('demosphere_user_calendar_config_form'))
			{dlib_permission_denied_403('invalid form token (w)',false);}

			global $user;
			if($user->id==0 || !($wuser->id===$user->id || $user->checkRoles('admin')) )
			{dlib_permission_denied_403();}
			$defaults=val($wuser->data,'demosphere_widget_config',[]);

			$values=demosphere_widget_parse_client_options($errors,$defaults);

			if($errors===false)
			{
				$wuser->data['demosphere_widget_config']=$values;
				$wuser->save();
				demosphere_page_cache_clear('%/widget-html?uid='.$wuser->id.'%');
				demosphere_page_cache_clear('%/widget-js?uid='.  $wuser->id.'%');
			}
		}
		else
		{
			$errors=false;
			// normal case: get options from user data
			$values=$wuser->data;
			if(!is_array($values)){fatal("invalid user");}
			$values=val($values,'demosphere_widget_config',[]);

			//$values=array();// FIXME: reset for testing

			// add default values for any missing options 
			// (for ex. if new options have been created)
			foreach(demosphere_widget_options() as $opt)
			{
				if(!isset($values[$opt['name']])){$values[$opt['name']]=$opt['default'];}
			}
		}
	}
	else
	{
		// this is an anonymous widget, get all options from url GET only
		$values=demosphere_widget_parse_client_options($errors);
	}
	//var_dump($values);
	return $values;
}

/**
 * Parse options passed through GET or POST and return an array with the values for each
 * option (missing options are filled with default values).
 *
 */
function demosphere_widget_parse_client_options(&$errors=false,$defaults=[])
{
	global $custom_config;
	require_once 'demosphere-event-list.php';
	require_once 'demosphere-event-list-form.php';

	if(isset($custom_config['widget_parse_client_options']))
	{$custom_config['widget_parse_client_options']();}

	$res=[];
	foreach(demosphere_widget_options() as $option)
	{
		$res[$option['name']]=val($_POST,$option['name'],
									 val($_GET,$option['name'],
											val($defaults,$option['name'],
												   $option['default'])));
	}

	// backward compatibility: deprecated names
	if(isset($_GET['use-map'      ])){$res['near-lat-lng-use-map' ]=$_GET['use-map'];}
	if(isset($_GET['map-distance' ])){$res['near-lat-lng-distance']=$_GET['map-distance'];}
	if(isset($_GET['map-latitude-and-longitude'])){$res['near-lat-lng-latitude-and-longitude']=$_GET['map-latitude-and-longitude'];}
	if(isset($_GET['max-nb-events'])){$res['limit'                ]=$_GET['max-nb-events'];}
	if(isset($_GET['select-terms-expression'])){$res['select-vocab-term-or']=$_GET['select-terms-expression'];}
	for($vid=0;$vid<8;$vid++)
	{
		if(isset($_GET['vocab-'.$vid]))
		{
			$topicNames=Topic::getAllNames();
			$tid=$_GET['vocab-'.$vid];
			$vocab=db_result_check('SELECT vocab FROM Term WHERE id=%d',$tid);
			if($vocab!==false          ){$res['select-vocab-term']=''.$tid;}
			else
			if(isset($topicNames[$tid])){$res['select-topic'     ]=''.$tid;}
			else                        {$res['select-city-id'   ]=''.$tid;}
		}
	}

	// support for standard options in public GET 
	$rawOpts=$_GET;
	unset($rawOpts['uid']);// avoid confusion between widget uid arg and std option (uid db field)
	$parseRes=demosphere_event_list_parse_getpost_options($rawOpts);
	foreach($parseRes['options'] as $name=>$val)
	{
		demosphere_event_list_form_item_values_from_parsed_value($name,$val,$res);
	}

	$errors=[];
	foreach(demosphere_widget_options() as $option)
	{
		extract($option);
		$val=&$res[$name];
		$ok=true;
		$emptyIsOk=true;
		$isEmpty=($val==='' || $val===false);

		switch($type)
		{
		case 'color':
			$ok=preg_match('@^(#[a-z0-9]{6}|)$@i',$val);
			// backwards compatibility with widgets that have a "ab12cd" color in url
			if(preg_match('@^([a-z0-9]{6}|)$@i',$val)){$ok=true;$val='#'.$val;}
			break;
		case 'int':
			$ok=ctype_digit($val) || is_int($val);/* is_int: for stdoptions, preparsed */
			$val=intval($val);
			break;
		case 'boolean':
			$val=($val!=='false' && $val!==false && $val!=='0' && $val!==0);
			break;
		case 'radio':
			$val=intval($val);
			break;
		}

		switch($name)
		{
		case 'height':
			$emptyIsOk=false;
			break;
		case 'layout':
			$layouts=demosphere_widget_layouts();
			$ok=isset($layouts[$val]);
			break;
		case "text-size":
		case "extra-message-size":
			$ok=preg_match('@^[0-9]+(px|%)$@',$val);
			break;
		case 'custom-css':
			$ok=filter_var($val,FILTER_VALIDATE_URL)!==false && preg_match('@^https?://@',$val);
			break;
		case "near-lat-lng-use-map":
		case "near-lat-lng-distance":
		case "near-lat-lng-latitude-and-longitude":
		case "map-shape":
		case "most-visited":
		case "max-nb-events-per-day":
		case "select-topic":
		case "select-city-id":
		case "select-fp-region":
		case "select-place-reference-id":
			if($val===false){$val='';}
		    $val=(string)$val;
			list($ok,$val)=demosphere_event_list_form_item_validate($name,$val,'simple');
		    break;
		}

		if($emptyIsOk && $isEmpty){$ok=true;$val=false;}
		if(!$ok){$errors[]=$name;$val=false;}
	}
	if(count($errors)===0){$errors=false;}
	return $res;
}

/**
 * Converts widget options into standard options as used in demosphere_event_list()
 * and defined in demosphere_event_list_options_defaults().
 */
function demosphere_widget_options_to_list_options($options,$widgetUid=false)
{
	global $demosphere_config;
	require_once 'demosphere-event-list.php';
	require_once 'demosphere-event-list-form.php';
	$res=[];
	$names=['nearLatLng','limit','selectTopic','selectCityId','selectFpRegion','selectPlaceReferenceId',
			'mapShape','mostVisited','maxNbEventsPerDay','selectVocabTerm','selectVocabTermOr'];
	foreach($names as $name)
	{
		if(demosphere_event_list_form_item_parse($name,$options,$res,false)===false){dlib_bad_request_400('Invalid value for option '.$name);}
	}
	$res['userCalPolicy']=val($options,'usercal-policy');
	$res['addPrivateEvents']=$widgetUid;
	return $res;
}

/**
 * Return a list of events that match the users options.
 */
function demosphere_widget_get_calendar_events($options,$widgetUid=false)
{
	global $custom_config;
	require_once 'demosphere-event-list.php';
	require_once 'demosphere-event-list-form.php';

	$calOptions=[];
	$calOptions=demosphere_widget_options_to_list_options($options,$widgetUid);

	$calOptions['fullUrls'    ]=true;
	$calOptions['userCalendar']=$widgetUid;
	$calOptions['userCalendarNoLoad']=true;

	// fetch all events
	$events=demosphere_event_list($calOptions);

	// use template_preprocess to build a easier to render
	demosphere_calendar_render($events,$calOptions,false,true);

	if(isset($custom_config['widget_events'])){$custom_config['widget_events']($events);}

	return $events;
}

/**
 * Render in html a list of events.
 *
 * Note: this could be transformed into a drupal template.
 */
function demosphere_widget_render_html_calendar($events,$options)
{
	global $custom_config;
	require_once 'demosphere-date-time.php';
	require_once 'dlib/filter-xss.php';

	// second check 
	$layouts=demosphere_widget_layouts();
	if(!isset($layouts[$options['layout']])){fatal('invalid widget layout');}

	$html=template_render('templates/demosphere-widget-'.$options['layout'].'.tpl.php',
						  [compact('events','options'),]
						  );
	if(isset($custom_config['widget_html'])){$custom_config['widget_html']($html);}
	return $html;
}

/**
 * Returns a CSS file that implements the user's display options.
 */
function demosphere_widget_build_css($options)
{
	$layouts=demosphere_widget_layouts();
	if(!isset($layouts[$options['layout']])){fatal('invalid widget layout');}

	require_once 'dlib/css-template.php';
	return css_template_compile(file_get_contents('demosphere/css/demosphere-widget-'.$options['layout'].'.tpl.css'),false,
								compact('options'));
}

/**
 * Returns the html code that the user can paste into his own site.
 *
 * This returns an array with both the iframe and js widgets.
 */
function demosphere_widget_build_embed($values,$widgetUid)
{
	global $base_url;
	$urlOpts='?';
	if($widgetUid)
	{
		$urlOpts.='uid='.intval($widgetUid);
	}
	else
	{
		// Note: this generates a url where all options are in "form" snytax
		// ("most-visited=...") and not in our standard option GET syntax ("mostVisited=...").
		// It might be better if standard options where in std opt syntax, but 
		// there is no rush, as the std opt syntax is already supported during parsing.
		foreach(demosphere_widget_options() as $opt)
		{
			$name=$opt['name'];
			$value=$values[$name];
			//var_dump(array($name,$value,$opt['default']));
			// shorter url and better maintainability : don't add opts with default vals
			if($value===$opt['default']){continue;}
			if($opt['type']==='boolean'){$value=$value?'true':'false';}
			$urlOpts.=$name.'='.urlencode($value).'&';
		}
		
	}
	$urlOpts=preg_replace('@[&?]$@','',$urlOpts);

	$res['iframe']='<iframe class="demosphere-calendar" src="'.ent($base_url.'/widget-html'.$urlOpts).'" '.
		'style="'.
		'width:' .($values['width' ]===false ? '100%;' : intval($values['width' ]).'px;').
		'height:'.($values['height']===false ? '100%;' : intval($values['height']).'px;').
		'" '.
		'frameborder="no" '.
		($values['show-scrollbar']===true ? '' : 'scrolling="no" ').
		'>'.
		'<h2 style="font-size:13px"><a href="'.$base_url.'">'.t('calendar').'</a></h2>'.
		'</iframe>';

	$res['js']='<div class="demosphere-calendar">'.
		'<div><h2 style="font-size:13px"><a href="'.$base_url.'">'.t('calendar').'</a></h2></div>'.
		'<script type="text/javascript" src="'.ent($base_url.'/widget-js'.$urlOpts).'"></script>'.
		'</div>';

	return $res;
}


/**
 * Generates an html calendar for selecting individual events in the config form.
 * 
 * This calendar displays all events, even those that don't match user's rules.
 * This way user can choose to display certain events that don't match his rules.
 * This calendar is filled into the config form through an ajax call.
 * This is called directly from a request.
 */
function demosphere_widget_select_events_calendar()
{
	global $demosphere_config,$user,$base_url;
	// setup widget user and check permissions
	if(!isset($_GET['uid'])){dlib_bad_request_400("uid not specified");}
	$widgetUid=intval($_GET['uid']);
	$wuser=User::fetch($widgetUid,false);
	if($wuser===null || $user->id==0 || 
	   !($wuser->id===$user->id || $user->checkRoles('admin')) )
	{dlib_permission_denied_403();}

	// **** find a list of all events that match automatic rules (terms, ...)
	// (but do not use individual event selection)
	$options=demosphere_widget_option_values($widgetUid,$errors);
	$autoCalOptions=demosphere_widget_options_to_list_options($options,$widgetUid);
	// dont use "userCalendar", (otherwise it will include selected)
	$autoCalOptions['addPrivateEvents']=$widgetUid;
	$autoCalOptions['userCalendar']=false;
	$autoCalOptions['id']=true;
	$autoCalOptions['default_noOtherFields']=true;
	$autoEvents=demosphere_event_list($autoCalOptions);
	// build a list of nids for each event in $autoEvents
	$autoEids=[];
	foreach($autoEvents as $e){if($e->id!==false){$autoEids[intval($e->id)]=true;}}

	// **** now fetch a complete list of ALL events (no criteria)
	$calOptions=[];
	$calOptions['addPrivateEvents']=$widgetUid;
	$calOptions['addUserCalInfo' ]=$widgetUid;
	$allEvents=demosphere_event_list($calOptions);
	//var_dump($allEvents);

	// ***** add extra class to each event to say if it is selected by rules
	// FIXME: this is not enough: there are many new rules!
	foreach($allEvents as $event)
	{
		if($event->id!==false &&
		   !isset($autoEids[intval($event->id)])){$event->render['addClasses']='rules-not-ok';}
	}

	require_once 'demosphere/demosphere-event-list.php';
	return demosphere_calendar_render($allEvents,$calOptions,false);
}

?>