<?php

//! Do a full text search for place with matching words.
//! There are two types of searches:
//! - "short":  search as you type, usually just a few words.
//! - "long": full text search : example: find places that might be referenced in a long text.
//! Both searches use the same db table (place_search_index), but compute scores differently.
function demosphere_place_search_long_search($text,$limit=10)
{
	$words=array_unique(explode(' ',trim(dlib_simplify_text($text))));
	if($words===['']){return [];}
	$wordsSql=implode(',',array_map(function($v){return "'".db_escape_string($v)."'";},$words));
	$query="
  SELECT
    pid,
    MAX(place_count),
    MAX(place_score)*SUM(word_score) AS mscore,
    MAX(place_score),
    GROUP_CONCAT(word) AS concat
  FROM place_search_index WHERE
    word IN (".$wordsSql.")
  GROUP BY pid
  ORDER BY mscore DESC
  LIMIT %d";

	// ********** Debug
	if(isset($_GET['debug']))
	{
		$time_start = microtime(true);
		$results=db_arrays($query,$limit);
		echo "time:".(microtime(true) - $time_start)."<br/>\n";
		foreach($results as $result)
		{
			echo "pid:".$result['pid']." ".
				"mscore:".round($result['mscore'],1)." ".
				"pscore:".round($result['place_score'],2)." ".
				"concat:".$result['concat']."<br/>";
			$words=explode(',',$result['concat']);
			$wordsQ=implode(',',array_map(function($v){return "'".db_escape_string($v)."'";},$words));

			$matches=db_arrays('SELECT * FROM place_search_index WHERE word IN ('.$wordsQ.') AND pid=%d ORDER BY word_score DESC',$result['pid']);
			foreach($matches as $match)
			{
				echo $match['word'].': '.round($match['word_score'],2).'&nbsp;&nbsp;&nbsp; ';
			}
			echo '<br/>';

			$place=Place::fetch($result['pid']);
			echo ent($place->address).'<br/><hr/>';

		}
	}
	return db_one_col($query,$limit);
}

//! Search for places that contain a few words.
//! Last word is possibly incomplete ("search as you type")
function demosphere_place_search_short_search($typed,$incomplete=true,$limit=10)
{
	$words=explode(' ',dlib_simplify_text($typed));
	// ignore space at end of search, but it means it is a full (not incomplete) word
	if(dlib_last($words)===''){array_pop($words);$incomplete=false;}
	if(count($words)===0){return [];}
	$completeWords=$words;
	$incompleteWord=$incomplete ? array_pop($completeWords) : false;

	// The nbmatch clause.
	// This is a very simple scoring approach: each matched word counts as 1.
	// The sql is a bit complex only because it needs to support incomplete words.
	$nbmatch='COUNT(*)';
	if($incomplete===true)
	{
		if(count($completeWords)==0){$nbmatch='1';}
		else
		{
			$completeWordsSql='('.
				implode(',',
						array_map(function($v){return "'".db_escape_string($v)."'";},$completeWords)).')';
			$nbmatch=
				// incomplete word counts as 1, event if multiple matches
				'(MAX(CASE WHEN word IN '.$completeWordsSql.' THEN 0 ELSE 1 END) +  '."\n".
				// normal (complete) words count as 1
				'     SUM(CASE WHEN word IN '.$completeWordsSql.' THEN 1 ELSE 0 END))';
		}
	}

	// The 'WHERE' clause for words
	$wordConditions='';
	foreach($completeWords as $word)
	{
		$wordConditions.="    word='".
			db_escape_string($word)."' OR\n";
	}
	if($incompleteWord!==false){$wordConditions.="    word LIKE '".db_escape_string($incompleteWord)."%%' OR\n";}
	$wordConditions=substr($wordConditions,0,-3);
	$wordConditions=trim($wordConditions);

	$query="
  SELECT
    pid,
    ".$nbmatch." AS nbmatch,
    MAX(place_score) as mscore,
    GROUP_CONCAT(word) as concat
  FROM place_search_index WHERE (
    ".$wordConditions.")
  GROUP BY pid
  ORDER BY nbmatch DESC, mscore DESC
  LIMIT %d";
	$results=db_arrays($query,$limit);

	// ********** Debug
	if(isset($_GET['debug']))
	{
		var_dump($query);
		foreach($results as $result)
		{
			echo "pid:".$result['pid']." ".
				"nbmatch:".$result['nbmatch']." ".
				"mscore:".$result['mscore']." ".
				"concat:".$result['concat']."<br/>";
			$place=Place::fetch($result['pid']);
			echo ent($place->address).'<br/><hr/>';
		}
	}
	return dlib_array_column($results,'pid');
}

//! Update db search index for a single place (called from $place->save())
function demosphere_place_search_index_update($place)
{
	// delete this place from search index
	db_query("DELETE FROM place_search_index WHERE pid=%d",$place->id);
	// only index reference places
	if($place->referenceId!==$place->id){return;}
	// only places in published events are indexed
	$nbEvents=db_result('SELECT COUNT(*) FROM Event WHERE status=1 AND showOnFrontpage=1 AND placeId=%d',$place->id);
	if($nbEvents==0){return;}

	$insertSql=demosphere_place_search_index_sql_rows($place);
	// Note: use REPLACE instead of insert, for rare concurency between 
	// this fct and demosphere_place_search_index_rebuild() called from cron
	if($insertSql!=='')
	{
		// Lock table to avoid concurency problems
		db_query("LOCK TABLES place_search_index WRITE");
		db_query("DELETE FROM place_search_index WHERE pid=%d",$place->id);
		db_query("REPLACE INTO place_search_index VALUES ".substr($insertSql,0,-1));
		db_query("UNLOCK TABLES");
	}
}

//! Recreate the db search index for a all places.
//! This is called from cron.
//! Note that it shouldn't be necessary in normal usage.
function demosphere_place_search_index_rebuild()
{
	// Get list of references for published events
	$placeIds=db_one_col('SELECT Place.referenceId FROM Event,Place WHERE '.
							'Event.status=1 AND Event.showOnFrontpage=1 AND '.
							'Place.id=Event.placeId GROUP BY Place.referenceId');

	// Regroup inserts into chunks of 1000 places, to avoid too many small inserts or a single huge insert.
	$insertSqlChunks=[];
	for($offset=0;$offset<count($placeIds);$offset+=1000)
	{
		$places=Place::fetchListFromIds(array_slice($placeIds,$offset,1000));

		// **** Compute rows that will be inserted into place_search_index
		$insertSql='';
		foreach($places as $place)
		{
			$insertSql.=demosphere_place_search_index_sql_rows($place);
		}
		$insertSqlChunks[]=$insertSql;
	}

	// Lock table to avoid concurency problems (also avoid wrong search results in concurrent processes)
	db_query("LOCK TABLES place_search_index WRITE");
	db_query("TRUNCATE place_search_index");
	foreach($insertSqlChunks as $insertSql)
	{
		if($insertSql!==''){db_query("INSERT INTO place_search_index VALUES ".substr($insertSql,0,-1));}
	}
	db_query("UNLOCK TABLES");

}

//! Returns a fragment of sql to update db search index for each word of a given place.
function demosphere_place_search_index_sql_rows($place)
{
	// Get text from city and address and split it into words
	$city=$place->getCityName();
	$address=$place->address;
	$text=$city.' '.$address;
	$words=explode(' ',trim(dlib_simplify_text($text)));
	if($words===['']){$words=[];}
	$words=array_unique($words);
	list($placeScore,$wordScores,$placeUsageCt)=
		demosphere_place_search_index_place_and_word_scores($place,$words);
	$res='';
	foreach($wordScores as $word=>$wordScore)
	{
		$res.='('.
			((int  )$place->id).",".
			"'".db_escape_string($word)."',".
			((float)$wordScore).",".
			((float)$placeScore).",".
			((float)$placeUsageCt).'),';
	}
	return $res;
}

//! Returns a score for a place and scores for a list of words
//! Word scores and place scores are only used for full text search (not for "search as you type").
//! This scoring system is heuristic and is not based on any theoretical framework.
function demosphere_place_search_index_place_and_word_scores($place,$words)
{
	static $wordFrequencies=false;
	if($wordFrequencies===false){$wordFrequencies=variable_get('word_freq');}

	// In how many events is this place's reference place used ?
	// This is used to determine the importance of the place.
	$placeUsageCt=db_result('SELECT COUNT(*) FROM Event '.
							'INNER JOIN Place ON Place.id=Event.placeId WHERE '.
							// only count published addresses
							'Event.status=1 AND '.
							'Event.showOnFrontpage=1 AND '.
							'Place.referenceId=%d',$place->referenceId);
	if($placeUsageCt==0){$placeUsageCt=1;} // FIXME: why?

	// This is a bit slow :-(
	$totalNbEvents=db_result('SELECT COUNT(*) FROM Event WHERE Event.status=1 AND Event.showOnFrontpage=1');

	// ************ compute placeScore
	// value=1  for ordinary places (not too freq, not too long)
	// computed from probabilty of each place and adjusted
	$placeScore=$placeUsageCt/$totalNbEvents;
	// uncommon places: $placeScore=1
	//   common places: $placeScore=2
	$placeScore=1+2*sqrt(min(1,$placeScore/0.02));
	// adjustment: less placeScore for very long addresses
	$nbPlaceWords=count($words);
	$placeScore/=max(1,$nbPlaceWords/15);

	// ************ compute word scores
	$wordScores=[];
	foreach($words as $word)
	{
		// compute wordScore: 0-1 value:
		// slightly adjusted min(1,$minwordProb/Prob(word))
		$len=strlen($word);
		// word probabilty is a mix of wordFrequency (langauge) and wordCount (places)
		// FIXME: these statistics are unreliable for small sites.
		$minWordFreq=.0005;
		$wordScore=isset($wordFrequencies[$word]) ? $wordFrequencies[$word] : $minWordFreq;
		$wordScore=max($minWordFreq,$wordScore);
		$wordScore=$minWordFreq/$wordScore;
		// score adjustment for word length
		if($len<4){$wordScore/=(5-$len);}
		if($len>5){$wordScore+=.1;}
		if($len>7){$wordScore+=.1;}
		if($len>10){$wordScore+=.2;}
		$wordScores[$word]=$wordScore;
	}
	return [$placeScore,$wordScores,$placeUsageCt];
}

function demosphere_place_search_index_install()
{
	db_query("DROP TABLE IF EXISTS place_search_index");
	db_query("CREATE TABLE place_search_index (
  `pid` int(11) NOT NULL,
  `word` varchar(191)  NOT NULL,
  `word_score` FLOAT NOT NULL,
  `place_score` FLOAT NOT NULL,
  `place_count` int NOT NULL,
  PRIMARY KEY  (`pid`,`word`),
  KEY `pid_index` (`pid`),
  KEY `word_index` (`word`),
  KEY `word_score` (`word_score`),
  KEY `place_score` (`place_score`),
  KEY `place_count` (`place_count`)
) DEFAULT CHARSET=utf8mb4;");
}

//! This is only used by admin to create statistics for a new language or site-specific custom statistics.
//! For  a  new  language,  the   result  file  should  be  copied  to
//! demosphere/translations/word-frequencies.[LANG].txt  .
//! Note that this file is only used  at install time.  The actually used values
//! are in demosphere_var:word_freq, and are updated by calling
//! this function:
//! http://example.org/maintenance/update-place-search-word-freq
//! For this function to work, meaningfull statistics must be available.
//! (about a million words?)
//! dlib @xyz php-eval 'variable_set("word_freq","",json_decode(file_get_contents("/var/www/demosphere/paris/demosphere/translations/word-frequencies.fr.txt"),true));'
function demosphere_place_search_word_freq()
{
	$minWordFreq=.0005;
	$fname='/tmp/word-frequencies.'.dlib_get_locale_name().'.txt';
    echo 'output file: '.ent($fname)."<br/>\n";

	// Count words from bodies of all (non repetition) events
	$qres=db_query('SELECT body FROM Event WHERE Event.status=1 AND Event.showOnFrontpage=1 AND repetitionGroupId=0;');
	$wordCount=[];
	$total=0;
	while ($row = db_fetch_array($qres))
	{
		$words=explode(' ',trim(dlib_simplify_text($row['body'],true)));
		if($words===['']){$words=[];}
		$total+=count($words);
		foreach($words as $word)
		{
			if(isset($wordCount[$word])){$wordCount[$word]+=1;}
			else                       {$wordCount[$word] =1;}
		}
	}
	foreach(array_keys($wordCount) as $word){$wordCount[$word]/=$total;}
	arsort($wordCount);
	$bodyWordCount=$wordCount;
	$bodyWordCountTotal=$total;

	// Counts how many times each word is used in places
	// FIXME: this gives unreliable statistics if there are not enough places.
	$placeIds=db_one_col('SELECT Place.referenceId FROM Event,Place WHERE '.
						  // only suggest published addresses
						  'Event.status=1 AND '.
						  'Event.showOnFrontpage=1 AND '.
						  'Place.id=Event.placeId GROUP BY Place.referenceId');
	$wordCount=[];
	$total=0;
	$places=Place::fetchListFromIds($placeIds);
	foreach($places as $place)
	{
		// Get text from city and address and plit it into words
		$city=$place->getCityName();
		$address=$place->address;
		$text=$city.' '.$address;
		$words=explode(' ',trim(dlib_simplify_text($text)));
		if($words===['']){$words=[];}
		$words=array_unique($words);
		$total+=count($words);
		foreach($words as $word)
		{
			if(isset($wordCount[$word])){$wordCount[$word]+=1;}
			else                        {$wordCount[$word] =1;}
		}
	}
	foreach(array_keys($wordCount) as $word){$wordCount[$word]/=$total;}
	arsort($wordCount);
	$placeWordCount=$wordCount;
	$placeWordCountTotal=$total;

	// ********** merge results from bodies and places

	echo 'Total nb words:'.ent($bodyWordCountTotal).' '.ent($placeWordCountTotal)."<br/>\n";
	echo 'Distinct words:'.count($bodyWordCount).' '.count($placeWordCount)."<br/>\n";
	//echo 'Freq:'.ent($bodyWordCount).' '.ent($placeWordCount)."<br/>\n"; ??

	$res=[];
	foreach($bodyWordCount  as $word=>$freq){if($freq<$minWordFreq){break;}$res[$word]=$freq;}

	foreach($placeWordCount as $word=>$freq)
	{
		if($freq<$minWordFreq*4) {break;}
		$freq/=2;// less credibility for incomplete stats
		$res[$word]=max($freq,val($res,$word,0));
	}

	arsort($res);
	//var_dump(count($res),$res);

	echo "Res:<br/>\n";
	$ct=0;
	foreach($res as $word=>$freq)
	{
		if($ct++>1000){break;}
		echo ent($word).' :	'.round($minWordFreq/$freq,3).' : '.ent($freq)."<br/>\n";
	}

	file_put_contents($fname,json_encode($res));

	variable_set('word_freq','',$res);
}

// ***************************************
// ***** UI interaction
// ***************************************

//! Ajax call to get place variants dropdown selector
function demosphere_place_search_variants_selector_ajax()
{
	echo demosphere_place_search_variants_selector(intval($_GET['refPid']));
}

//! Returns HTML of a <select> for place suggestions based on a search text.
function demosphere_place_search_suggestions($searchText,$isAnonymous=false)
{
	$maxResults=$isAnonymous ? 10 : 20;
	$placeCts=demosphere_place_search_long_search($searchText,$maxResults);
	if(count($placeCts)===0){return '';}
	return demosphere_place_search_selector_render($placeCts,['id'=>'place-suggestions'],t('choose a place'),false,$isAnonymous);
}

//! Returns HTML of a <select> containing variants of $refPid
//! (places that have referenceId=$refPid)
function demosphere_place_search_variants_selector($refPid)
{
	$variantCts=db_one_col('SELECT '.
						   'Place.id,'.
						   '(SELECT COUNT(*) FROM Event WHERE Event.placeId=Place.id AND status=1 AND showOnFrontpage=1) AS ct '.
						   'FROM Place WHERE '.
						   'Place.referenceId=%d AND '.
						   'Place.hideFromAltSuggestion=0 '.
						   'HAVING ct>0 '.
						   'ORDER BY id=referenceId DESC, ct DESC '.
						   'LIMIT 40 ',
						   $refPid);
	if(count($variantCts)<=1){return '';}
	return demosphere_place_search_selector_render($variantCts,
												   ['id'=>'variants-selector','data-ref-pid'=>$refPid],
												   t('variants'),count($variantCts)>10,false);
}

//! Renders an HTML <select> to choose place or place variants.
function demosphere_place_search_selector_render($placeCts,$attrs,$title,$addSearch=false,$isAnonymous=false)
{
	global $base_url,$user;

	$res='<select '.dlib_render_html_attributes($attrs).'>';
	$res.= '<option value="0">('.ent($title).')</option>';
	foreach($placeCts as $pid=>$count)
	{
		$place=Place::fetch($pid,false);
		if($place===null){continue;}

		$displayAddress=$place->address;
		$displayAddress=preg_replace('@[ \n]*\n@s',";  ",$displayAddress);
		if(mb_strlen($displayAddress)>90)
		{$displayAddress=mb_substr($displayAddress,0,90).'…';}
		$displayAddress=ent($displayAddress);
		$displayAddress=str_replace(';  ',";&nbsp;&nbsp;&nbsp;",$displayAddress);

		$info=($count!=0 ? intval($count) : '').
			  ($place->zoom!=0 ? '' : '-');

		for($i=strlen($info);$i<4;$i++){$info.="&nbsp;&nbsp;";}

		$placeFields=['id','referenceId','city','address','latitude','longitude','zoom',];
		$val=array_intersect_key((array)$place,array_flip($placeFields));
		$val['city']=$place->getCityName();
		if($user->checkRoles('admin','moderator') && $place->inChargeId)
		{
			$val['inChargeEmail']=$place->useInCharge()->email;
		}
		$res.= '<option value="'.ent(json_encode($val)).'">'.
			($isAnonymous ? '' : $info).
			$displayAddress.
			'</option>';
	}
	if($addSearch)
	{
		$res.='<option value="search">['.ent(t('search')).']</option>';
	}
	$res.='</select>';
	return $res;
}

//! Ajax response for autocomplete - search for variants (bottom of variants-selector menu).
function demosphere_place_search_variants($refPid)
{
	$searchTerm=$_GET['term'];
	$res=db_arrays("SELECT id AS value,address AS label FROM Place ".
				   "WHERE referenceId=%d ".
				   "AND EXISTS (SELECT * FROM Event WHERE placeId=Place.id AND status=1 AND showOnFrontpage=1) ".
				   "AND hideFromAltSuggestion=0 ".
				   "AND address LIKE '%%%s%%'",$refPid,$searchTerm);
	return $res;
}

//! Ajax for "Search as you type"
//! This is meant to be used with jquery ui autocomplete
function demosphere_place_search_autocomplete()
{
	$pids=demosphere_place_search_short_search($_GET['term'],true,15);
	$places=Place::fetchListFromIds($pids);
	$ct=0;
	$suggestions=[];
	if(!isset($_GET['no-new-place']))
	{
		$suggestions[]=['label'=>t('choose one of the following places, or click here to create a new place'),'value'=>'0'];
	}
	foreach($places as $place)
	{
		$displayAddress=$place->address;
		$displayAddress=preg_replace('@[ \n]*\n@s',";  ",$displayAddress);
		if(mb_strlen($displayAddress)>90)
		{$displayAddress=mb_substr($displayAddress,0,90).'(...)';}
		$suggestions[]=['label'=>$displayAddress,'value'=>$place->id];
	}
	if(count($pids)===0){$suggestions[]=['label'=>t('No places found'),'value'=>'0'];}

	return $suggestions;
}


?>