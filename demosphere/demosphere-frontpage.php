<?php 

/**
 * Renders frontpage using demosphere-frontpage.tpl.php
 * 
 * Which is called is called by the "/" url path.
 *
 * @see demosphere-calendar.tpl.php
 */
function demosphere_frontpage()
{
	global $user,$base_url,$demosphere_config,$currentPage;
	$isLoggedIn=$user->id!=0;
	$isEventAdmin=$user->checkRoles('admin','moderator');
	$std_base_url=$demosphere_config['std_base_url'];

	require_once 'demosphere-common.php';

	$currentPage->title=$demosphere_config['site_name'].' : '.$demosphere_config['site_slogan'];

	$isMobile=demosphere_is_mobile();
	$currentPage->bodyClasses[]=$isMobile ? 'mobile' : 'not-mobile';

	if(!$isMobile){$currentPage->bodyClasses[]='notFullWidth';}

	// ***************  Parse frontpage GET and url arguments
	require_once 'demosphere-event-list.php';
	$rawOpts=$_GET;

	// find selectFpRegion from current url
	foreach($demosphere_config['frontpage_regions'] as $id=>$region)
	{
		if(val($region,'domain')===$base_url){$rawOpts['selectFpRegion']=$id;}
	}
	if(isset($_GET['q']) &&
	   preg_match('@^'.$demosphere_config['frontpage_regions_url_name'].'/(.+)$@',$_GET['q'],$matches))
	{
		$rawOpts['selectFpRegion']=$matches[1];
	}
	if(isset($rawOpts['selectFpRegion']) && isset($rawOpts['selectCityId']))
	{
		unset($rawOpts['selectFpRegion']);
	}

	// Custom user calendars (url is like https://example.org/usercal/123)
	if(isset($_GET['q']) && preg_match('@^perso/@',$_GET['q']))
	{
		// redirect /perso/123 to /usercal/123 (renamed)
		dlib_redirect(preg_replace('@(https?://[^/]+)/perso/@','$1/usercal/',dlib_current_url()));
	}
	if(isset($_GET['q']) &&
	   preg_match('@^usercal/([0-9]+)$@',$_GET['q'],$matches)){$rawOpts['userCalendar']=$matches[1];}

	// rename certain GET args (shortcuts or deprecated option names)
	$rename=['start'=>'selectStartTime',
			 'end'=>'endTime',
			 'htopic'=>'highlightTopic',
			 'archive'=>'showArchiveLinks',
			];
	foreach($rename as $old=>$new)
	{
		if(isset($rawOpts[$old])){$rawOpts[$new]=$rawOpts[$old];unset($rawOpts[$old]);}
	}

	// ?a is a shortcut for selectStartTime=first day of month & showArchiveLinks
	if(isset($rawOpts['a']))
	{
		if(!ctype_digit($rawOpts['a'])){dlib_bad_request_400('Invalid a (archive) argument');}
		$rawOpts['selectStartTime']=''.strtotime('first day of this month 0:00:01',$rawOpts['a']);
		$rawOpts['endTime'  ]=''.strtotime('first day of next month 0:00:01',$rawOpts['a']);
		$rawOpts['showArchiveLinks']='';
		$rawOpts['limit']='10000';
		unset($rawOpts['a']);
	}

	// actually call the main parsing function
	$parseRes=demosphere_event_list_parse_getpost_options($rawOpts,
													['doNotParse'=>['highlightTopic']]);
	$rawOpts=$parseRes['notParsed'];
	$demosFPArgs=$parseRes['options'];

	$userCalendar=val($demosFPArgs,'userCalendar');
	if($userCalendar){require_once 'demosphere-user-calendar.php';}

	// add list of frontpage specific args, to avoid unkown option errors
	$demosFPArgs['ignoreOptions']=['showArchiveLinks'];

	// parse frontpage specific options
	// highlightTopic
	if(isset($rawOpts['highlightTopic']))
	{
		$demosFPArgs['highlightTopic'   ]=  
			$rawOpts['highlightTopic']==='n' || $rawOpts['highlightTopic']==='others' ? $rawOpts['highlightTopic'] : intval($rawOpts['highlightTopic']);
	}
	// showArchiveLinks
	if(isset($rawOpts['showArchiveLinks'])){$demosFPArgs['showArchiveLinks']=true;}
	// set selectStartTime at begining of month if archive mode with no selectStartTime
	if(isset($demosFPArgs['showArchiveLinks']) &&
	   !isset($demosFPArgs['selectStartTime']))
	{
		$demosFPArgs['selectStartTime']=strtotime('first day of this month 0:00:01');
	}

	// allow browsing unpublished/unshown on frontpage events for admins
	if($isEventAdmin && isset($demosFPArgs['selectModerationStatus']))
	{
		if(count(array_intersect([Event::moderationStatusEnum('waiting'),
								  Event::moderationStatusEnum('rejected'),],
								 $demosFPArgs['selectModerationStatus']))>0)
		{
			$demosFPArgs['selectShowOnFrontpage']=false;
			$demosFPArgs['selectStatus']=false;
		}
	}
	else
	// Contributors can browse certain unpublished events to give an Opinion
	if($demosphere_config['enable_opinions'] && $user->contributorLevel>0)
	{
		$demosFPArgs['addForContributor']=true;
	}

	// ************* css
	require_once 'dlib/sprite.php';

	$currentPage->addCssTpl('demosphere/css/demosphere-common.tpl.css');

	if($isMobile){$currentPage->addCssTpl('demosphere/css/demosphere-mobile-common.tpl.css');}

	$currentPage->addCssTpl('demosphere/css/'.($isMobile ? 
											   'demosphere-frontpage-mobile.tpl.css':
											   'demosphere-frontpage.tpl.css'));
	$currentPage->addCssTpl('demosphere/css/'.($isMobile ? 
											   'demosphere-calendar-mobile.tpl.css':
											   'demosphere-calendar.tpl.css'));

	$extraGetArgs=$_GET;
	unset($extraGetArgs['q']);
	if(count($extraGetArgs)===0  &&
	   ($base_url==$demosphere_config['std_base_url'] ||
		$base_url==preg_replace('@^(https?://)(www\.)?@','$1mobile.',$demosphere_config['std_base_url'])))
	{
		$currentPage->robots=[];
	}

	$siteName=$demosphere_config['site_name'];

	$title=$siteName.' : '.dlib_fast_html_to_text($demosphere_config['site_slogan']);

	// special title for user calendars
	$userCalBanner=false;
	if($userCalendar)
	{
		$userCalBanner=demosphere_user_calendar_sitebanner($userCalendar);
	}

	$calTopics=demosphere_frontpage_topic_selector($demosFPArgs,'frontpage'.($isMobile ? '-mobile' : ''));

	$options=demosphere_frontpage_options($demosFPArgs);

	// ******* build the main calendar display
	require_once 'demosphere-event-list.php';
	$calOpts=array_merge($demosFPArgs,
						 [
							 // display oriented args : how to display those events
							 'showAdminLinks' =>$isEventAdmin,
						 ]);
	if($userCalendar         ){$calOpts['extraGetArgs']['userCalendar']=$userCalendar;}
	$autoLimitNbEvents=isset($demosFPArgs['limit']) ? false : 
		$demosphere_config['front_page_limit_nb_events'];
	if($autoLimitNbEvents!==false){$calOpts['limit']=$autoLimitNbEvents;}

	$calendar='';
	if(val($demosFPArgs,'showArchiveLinks')!==false)
	{
		$calendar.=demosphere_calendar_archive_links($calOpts);
		$currentPage->addCssTpl('demosphere/css/demosphere-frontpage-archive.tpl.css');		
	}
	$disabledFilterMessages=['userCalendar'];
	if($autoLimitNbEvents || val($calOpts,'limit',0)>9000){$disabledFilterMessages[]='limit';}
	$calendar.=demosphere_calendar_event_filter_message($calOpts,$disabledFilterMessages);
	$events=demosphere_event_list($calOpts);
	$autoLimitMoreEventsUrl=false;
	if(count($events)===$autoLimitNbEvents){$autoLimitMoreEventsUrl=demosphere_frontpage_url($demosFPArgs,['limit'=>10000]);}
	$calendar.=demosphere_calendar_render($events,$calOpts,$isMobile);
	// cleanup calendar whitespaces for shorter html size
	$calendar=preg_replace('@\n[ \t]*@','',$calendar);
	$calendar=preg_replace('@[ \t]{2,}@',' ',$calendar);

	require_once 'demosphere-page.php';
	$secondary_links=demosphere_secondary_links(true);

	$eventAdminMenubar='';

	$currentPage->bodyClasses[]='frontpage';
	if($isEventAdmin)
	{
		$currentPage->addCssTpl('demosphere/css/demosphere-admin.tpl.css');
		$eventAdminMenubar=demosphere_event_admin_menubar();
		$currentPage->bodyClasses[]='eventAdmin';
		if($demosphere_config['extra_body_class']!==false)
		{
			$currentPage->bodyClasses[]=$demosphere_config['extra_body_class'];
		}
	}

	// *** build rss/ical feed links (with similar get args as this page)
	$rssLink =$base_url.'/events.xml';
	$icalLink=$base_url.'/events.ics';
	$feedArgs=array_diff_key($demosFPArgs,array_flip(['highlightTopic','showArchiveLinks','selectStartTime','endTime','limit']));
	$args=demosphere_frontpage_url($feedArgs,[],[],false);
	$base_path=preg_replace('@^https?://[^/]*@','',$base_url).'/';
	$args=substr($args,strlen($base_path));
	$rssLink .=$args;
	$icalLink.=$args;

	// *** javascript 
	$currentPage->addJsConfig('demosphere',
							  ['map_center_latitude','map_center_longitude','map_zoom','mapbox_access_token','locale','frontpage_regions_url_name']);

	$currentPage->addJs(demosphere_js_shorten(file_get_contents('demosphere/js/add-script.js')),'inline');

	if(!$isMobile)
	{
		// These are inserted in demosphere-frontpage.js's namespace
		$currentPage->addJsVar('demosphere_frontpage',
			[
				'jquery_js_path'                      =>demosphere_cacheable('lib/jquery.js'),
				'demosphere_frontpage_options_js_path'=>demosphere_cacheable('demosphere/js/demosphere-frontpage-options.js'),
				'demosphere_event_list_form_js_path'  =>demosphere_cacheable('demosphere/js/demosphere-event-list-form.js'),
				'topics'                              =>Topic::getAllNames(),
			]);

		$currentPage->addJsTranslations(['dr1'=>t('most visited event for this day'),
										 'dr2'=>t('second most visited event for this day'),]);

		// Last JS. May (or may not?) be better for performance.
		$currentPage->addJs('demosphere/js/demosphere-frontpage.js','file',['defer'=>true,'cache-checksum'=>true]);
	}
	else
	{
		$currentPage->addJs(file_get_contents('demosphere/js/demosphere-mobile-common.js'),'inline');
	}

	//$currentPage->addJs("var req=new XMLHttpRequest();req.open('GET','".$base_url."/size?".
	// 					"w='+document.documentElement.clientWidth+'&h='+document.documentElement.clientHeight+'".
	// 					"&sw='+screen.width+'&sh='+screen.height+'".
	// 					"&dpr='+window.devicePixelRatio+'".
	// 					"&m='+(window.location.href.indexOf('https://mobile')===0)+'".
	// 					"&u=".($isEventAdmin ? 'mod':'u').
	// 					"');req.send(null);",
	// 					'inline');

	$bodyJavascript=demosphere_browser_detect_css_javascript();
	foreach($currentPage->js as $desc)
	{
		if($desc['location']==='inline-body'){$bodyJavascript.='<script type="text/javascript">'.$desc['js'].'</script>'."\n";}
	}

	// actual start and end times of show events (including limit)
	$t0=val($calOpts,'selectStartTime',Event::today());
	$l =val($calOpts,'limit')===count($events) && count($events) ? dlib_last($events)->startTime : time()+3600*24*365*10;
	$t1=val($calOpts,'endTime',time()+3600*24*365*10);
	$t1=min($t1,$l);
	$calnavigation=demosphere_frontpage_calnavigation($demosFPArgs,$t0,$t1);
	$infoboxes='';
	foreach($demosphere_config['front_page_infoboxes'] as $desc){$infoboxes.=demosphere_frontpage_infobox($desc);}

	$annouce=demosphere_frontpage_announcement();
	$message=demosphere_frontpage_messages();
	$featured=demosphere_frontpage_featured_events();

	if($userCalendar || isset($rawOpts['showArchiveLinks'])){$message='';$annouce='';$featured='';}

	$cities=demosphere_frontpage_regions_selector($demosFPArgs);

	$metaDescription=t('Demosphere is a calendar for activist events near Paris');
	$metaKeywords   =t('demosphere,demonstration,activist,event,calendar');

	$loginLink= $isLoggedIn ? 
		'<a href="'.$base_url.'/logout" class="logoutlink"><span>'.ent(t('log out')).'</span></a>' :
		'<a href="'.$base_url.'/login"><span>'.ent(t('login')).'</span></a>' ;


	$lang=dlib_get_locale_name();


	// FIXMEx : custom_demos_preprocess_demosphere_frontpage
	//if(function_exists('custom_demos_preprocess_demosphere_frontpage'))
	//{
	// 	custom_demos_preprocess_demosphere_frontpage($variables);
	//}
	return template_render($isMobile ? 
						   'templates/demosphere-frontpage-mobile.tpl.php':
						   'templates/demosphere-frontpage.tpl.php',
						   [compact('lang','title','siteName','base_url','std_base_url','rssLink','icalLink','metaDescription','metaKeywords','eventAdminMenubar','bodyJavascript','userCalBanner','calTopics','options','annouce','message','featured','cities','calendar','calnavigation','infoboxes','loginLink','autoLimitMoreEventsUrl','demosFPArgs','secondary_links','userCalendar'),
							// Performance: inline all CSS on mobile frontpage, as suggested by Google PageSpeed Insights 
							$currentPage->variables($demosphere_config['compress_css'],$isMobile),
						   ]);
	
}


/**
 * Returns the url (actually abs path)  of the frontpage with all necessary GET arguments. 
 *
 * This function is usefull to propagate GET arguments for the many options 
 * supported by the frontpage. 
 *
 * $demosFPArgs is the main list of options. It can either be a short list,
 * or the full list that includes all default values. In either case, this function
 * only includes GET arguments whose value is different than default.
 *
 * $override: these values will be used instead of those in $demosFPArgs (with the same key)
 * $unsetArgs: list of names which will not show up in the url
 * $allowPathChange: ?? (only used in demos_preprocess_demosphere_frontpage()
 *              (Used for cities, where baseurl and path can be changed)
 */
function demosphere_frontpage_url($demosFPArgs,$override=[],$unsetArgs=[],$allowPathChange=true)
{
	require_once 'demosphere-event-list.php';
	global $demosphere_config,$base_url;
	// get default options, to see which options are changed
	static $defaults=false;
	if($defaults===false){$defaults=demosphere_event_list_options_defaults([]);}

	$args=array_merge($demosFPArgs,$override);
	$ct=0;
	// never put these in a url
	static $ignore0=['ignoreOptions'=>true,'showAdminLinks'=>true,'extraGetArgs'=>true,'selectStatus'=>true,'addForContributor'=>true];
	$ignore=$ignore0;

	$base_path=preg_replace('@^https?://[^/]*@','',$base_url).'/';
	$url=$base_path;

	// very special case: subdomains are used for cities, and we want to remove city.
	if($allowPathChange && $base_url!=$demosphere_config['std_base_url'] && 
	   array_search('city',$unsetArgs)!==false)
	{
		$url=$demosphere_config['std_base_url'].'/';
	}
	
	// special case for selectFpRegion (the region name can be directly in the path or domain name)
	if($allowPathChange && val($args,'selectFpRegion')!==false)
	{
		$regionConfig=$demosphere_config['frontpage_regions'][$args['selectFpRegion']];
		$ignore['selectFpRegion']=true;
		if(array_search('selectFpRegion',$unsetArgs)!==false){}
		else
		if($regionConfig['domain']!==false){$url=$regionConfig['domain'].'/';}
		else
		{
			$url=$base_path.''.$demosphere_config['frontpage_regions_url_name'].'/'.$args['selectFpRegion'];
		}
	}

	// special case for userCalendar (directly in the path name)
	if($allowPathChange && val($args,'userCalendar')!==false && array_search('userCalendar',$unsetArgs)===false)
	{
		$ignore['userCalendar']=true;
		$url=$base_path.'usercal/'.intval($args['userCalendar']);
	}

	// PHP 5.4 Notice: $args=array_diff_assoc($args,$defaults); (does string cast before comparing?!)
	$notDefault=[];
	foreach($args as $k=>$v){if(!isset($defaults[$k]) || $defaults[$k]!==$v){$notDefault[$k]=$v;}}
	$args=$notDefault;

	if(count($unsetArgs)){$args=array_diff_key($args,array_flip($unsetArgs));}
	$args=array_diff_key($args,$ignore);
	foreach($args as $name=>$val)
	{
		$displayVal=is_array($val) ? implode(',',$val) : $val;
		if($displayVal===false){$displayVal=0;}// FIXME: some args really need to be 'false'
		// my reading of rfc3986 is that ',' is allowed as a subdelimiter (urlencode encodes it...)
		$tag='KLJKLJMLKSDF76978S6D9F8S7DFSDKJHNLMJLK';
		$displayVal=str_replace(',',$tag,$displayVal);
		$displayVal=urlencode($displayVal);
		$displayVal=str_replace($tag,',',$displayVal);
		$url.=($ct?'&':'?').urlencode($name).'='.$displayVal;
		$ct++;
	}
	return $url;
}

// ********** topic selector
function demosphere_frontpage_topic_selector($demosFPArgs,$destination='frontpage')
{
	global $demosphere_config,$base_url,$user,$currentPage;

	$selectTopic   =val($demosFPArgs,'selectTopic');
	$highlightTopic=val($demosFPArgs,'highlightTopic');

	$allTopics=array_values(Topic::getAll('WHERE showOnFrontpage=1'));

	// fake "others" topic
	if($demosphere_config['topics_required']!=='mandatory')
	{
		$clear=new stdClass();
		$clear->id='others';
		$clear->name='';
		$clear->description=t('Events with none of the previous topics');
		$clear->color='';
		$clear->frontpageLineBreak=false;

		$allTopics[]=$clear;
	}

	$res='';

	$res.='<div id="topicSelect" class="">';

	$selectTermVal=false;
	$lineNb=0;
	$isNewLine=true;
	$nbTopics=count($allTopics);
	foreach($allTopics as $topicNb=>$topic)
	{
		if($isNewLine)
		{
			$lineHasColor=false;
			for($i=$topicNb;$i<$nbTopics;$i++)
			{
				if($allTopics[$i]->color!=''){$lineHasColor=true;break;}
				if($allTopics[$i]->frontpageLineBreak){break;}
			}
			$res.='<div class="line line'.$lineNb.' '.($lineHasColor ? 'lineHasColor' : 'lineNoColor').'">';
			$isNewLine=false;
		}

		$res.='<span id="sel'.$topic->id.'" class="'.($topic->color==='' ? 'nocol ' : 'col ').'" '.
			(strlen($topic->description)>0 ? 'title="'.$topic->description.'" ' :'').'>';
		$res.='<a href="'.ent(demosphere_frontpage_url($demosFPArgs,['highlightTopic'=>$topic->id])).'" rel="nofollow" >';
		$res.=ent($topic->name)."</a>";
		$res.="</span>\n";

		if($topic->frontpageLineBreak)
		{
			$res.='</div>';
			$isNewLine=true;
			$lineNb++;
		}
	}
	$res.='</div>';
	$res.='</div>';

	return $res;
}


/** Creates the selector so that user can view the calendar of a specific region. 
 *
 * Frontpage regions are configured in $demosphere_config['frontpage_regions']
 */
function demosphere_frontpage_regions_selector($demosFPArgs)
{
	global $demosphere_config,$base_url;
	$std_base_url=$demosphere_config['std_base_url'];
	$regions=$demosphere_config['frontpage_regions'];
	if($regions===false || count($regions)===0){return '';}

	$currentRegion=val($demosFPArgs,'selectFpRegion');

	$url=demosphere_frontpage_url($demosFPArgs);
	$noRegionArgs=$demosFPArgs;
	unset($noRegionArgs['selectFpRegion']);
	$noRegionUrl=demosphere_frontpage_url($noRegionArgs);

	$out='';
	$useLinks=$demosphere_config['frontpage_regions_use_links'];
	if($useLinks)
	{
		$out.='<div id="frontpage-regions-links">';
	}
	else
	{
		$out.='<select id="frontpage-regions" onchange="window.location=this.value;"'.
			'">'.
			'<option value="'.ent($noRegionUrl).'">'.t('--choose region--').'</option>';
	}
	foreach($regions as $urlName=>$region)
	{
		$specificRegionArgs=$demosFPArgs;
		$specificRegionArgs['selectFpRegion']=$urlName;
		$specificRegionUrl=demosphere_frontpage_url($specificRegionArgs);

		// if empty city name : use city name
		if(isset($region['cityId']) && $region['cityId']!==false)
		{
			$city=City::fetch($region['cityId'],false);
			if($city===null){dlib_message_add('Invalid city in frontpage regions','error');continue;}
			if(strlen(val($region,'name'))==0){$region['name']=$city->name;}
		}
		if($useLinks)
		{
			$out.='<a class="'.($currentRegion===$urlName ? 'active' : '' ).
				'" href="'.ent($specificRegionUrl).'">'.ent($region['name']).'</a> ';
		}
		else
		{
			$out.='<option value="'.ent($specificRegionUrl).'" '.
				($currentRegion===$urlName ? 'selected="selected" ' : '' ).'>'.
				ent($region['name']).'</option>';
		}
	}
	$out.=$useLinks ? '</div>' : '</select>';
	return $out;
}

function demosphere_frontpage_calnavigation($demosFPArgs,$eventsStart,$eventsEnd)
{
	global $user,$demosphere_config;
	$eventsStart=strtotime('today 0:00:01',$eventsStart);
	$eventsEnd  =strtotime('today 0:00:01',$eventsEnd  );

	$selectStartTime=val($demosFPArgs,'selectStartTime');
	$time=$selectStartTime===false ? time(): $selectStartTime;
	$m0=date('n',$time);
	$y0=date('Y',$time);
	list($m1,$y1)=demosphere_frontpage_nmonths_away(1,$m0,$y0);
	list($m2,$y2)=demosphere_frontpage_nmonths_away(2,$m0,$y0);
	list($m3,$y3)=demosphere_frontpage_nmonths_away(3,$m0,$y0);
	$bigMonths=
		demosphere_frontpage_month_cal_weeks($m0,$y0)>5 || 
		demosphere_frontpage_month_cal_weeks($m1,$y1)>5;

	$out='';
	$out.='<div id="calnavigation" class="fullInfobox'.
		($bigMonths? ' bigMonths':'').
		'">';	
	$out.='<h3 class="infobox-title">'.t('Navigation').'</h3>';
	$out.='<div id="calnav-inner">';
	$out.='<div id="calnav-tablewrap">';
	$out.=demosphere_frontpage_navigation_calendar($m0,$y0,'class="leftcal"',$bigMonths,
												   $selectStartTime===false,$eventsStart,$eventsEnd);
	$out.=demosphere_frontpage_navigation_calendar($m1,$y1,''               ,
												   $bigMonths,$selectStartTime===false,$eventsStart,$eventsEnd);
	$out.='</div>';
	$out.='<p id="archives-link">';
    $m2ts=mktime(1,0,0,$m2,1,$y2);
    $m3ts=mktime(1,0,0,$m3,1,$y3);
	$out.='<a href="'.($m2ts>$eventsEnd ? '?limit=1000' : '').'#m'.$m2.'">'.strftime('%B',$m2ts).'</a>,&nbsp;';
	$out.='<a href="'.($m3ts>$eventsEnd ? '?limit=1000' : '').'#m'.$m3.'">'.strftime('%B',$m3ts).'</a>';
	if($demosphere_config['site_id']!='kinimatorama')
	{
		$out.=', ';
		$out.='<a id="archive-link" href="'.
			ent(demosphere_frontpage_url($demosFPArgs,
										 ['selectStartTime'  =>strtotime('first day of this month 0:00:01',$selectStartTime===false ? time(): $selectStartTime),
										  'showArchiveLinks'=>true])).
			'"  rel="nofollow" >'.t('archives').'</a>';
	}
	$out.='</p></div></div>';
	return $out;
}

//! Renders a small calendar using an html table.
function demosphere_frontpage_navigation_calendar($month=false,$year=false,$class='',
												  $bigMonth,$greyOld=true,$eventsStart,$eventsEnd)
{
	global $user;
	$isLoggedIn=$user->id!=0;

	$today=mktime(1,0,0,date('n'),date('j'),date('Y'));

	//echo "******** $month:$year<br/>";
	$out='';
	$out.='<table '.$class.'>';
	$cm=date('n',mktime(1,0,0,$month,1,$year));
	$tm=date('n');
	$out.='<caption>'.
		($cm!=$tm ? '<a href="#m'.$cm.'">':'').
		strftime('%B',mktime(1,0,0,$month,1,$year)).
		($cm!=$tm ? '</a>':'').
		'</caption>';
	$out.='<colgroup span="5"></colgroup>';
	$out.='<colgroup span="2" class="we"></colgroup>';


	$firstWdOfMonth=date('N',mktime(1,0,0,$month,1,$year));
	$past=2;
	$nbWeeks=demosphere_frontpage_month_cal_weeks($month,$year);
	if($bigMonth){$nbWeeks=6;}
	for($i=0;$i<$nbWeeks*7;$i++)
	{
		list($curDay,$curMonth,$curYear)=demosphere_frontpage_ndays_away(1+$i-$firstWdOfMonth,
													1,$month,$year);
		$weekday=($i%7)+1;
		$curDate=mktime(1,0,0,$curMonth,$curDay,$curYear);

		if($i>60){fatal("argh");}

        if($weekday==1)
		{
			$oldweek=false;
			if($greyOld && $curDate<$today-7*24*3600){$out.='<tr class="o">';$oldweek=true;}
			else
			{$out.='<tr>';}
		}

		$class="";
		$isOldDay=$greyOld && $curDate<$today;
		if(!$oldweek && $isOldDay){$class.='o ';}
		if($weekday==6 || ($weekday==7 && !$isOldDay))
		{$class.='w'.$weekday.' ';}
		if($curMonth!=$month){$class.='x ';}
		if($curDate==$today){$class.=' now';}
		$out.='<td '.($class!='' ? 'class="'.$class.'"' : '').'>';
		$isShown=$curDate>$eventsStart && $curDate<$eventsEnd;
		$out.='<a href="'.($isShown ? '' : '?a='.$curDate).'#d'.$curDay.'-'.$curMonth.'">'.
			$curDay.'</a>';
		
		$out.='</td>';
        if($weekday==7){$out.="</tr>\n";}
	}
	$out.='</table>';
	return $out;
}

function demosphere_frontpage_nmonths_away($offset,$month=false,$year=false)
{
	$ts=strtotime($offset.' months',mktime(1,0,0, $month,1,$year));
	return [intval(date('n',$ts)),intval(date('Y',$ts))];
}

function demosphere_frontpage_ndays_away($offset,$refDay,$refMonth,$refYear)
{
	$ts=strtotime($offset.' days',mktime(1,0,0, $refMonth,$refDay,$refYear));
	return [intval(date('d',$ts)),intval(date('n',$ts)),intval(date('Y',$ts))];
}

function demosphere_frontpage_nbdays_in_month($m,$y)
{
	list($m,$y)=demosphere_frontpage_nmonths_away(1,$m,$y);
	return date('j',mktime(1,0,0,$m,1,$y)-5*3600);
}

//! How many weeks appear on the calendar of given month.
function demosphere_frontpage_month_cal_weeks($m,$y)
{
	$days=demosphere_frontpage_nbdays_in_month($m,$y);
	$firstWd=date('N',mktime(1,0,0,$m,1,$y));
	$lastWd =date('N',mktime(1,0,0,$m,$days,$y));
	$totdays=($firstWd-1)+$days+(7-$lastWd);
	return $totdays/7;
}

function demosphere_frontpage_infobox($desc)
{
	global $user,$base_url,$currentPage;
	$post=Post::fetch($desc['pid'],false);
	if($post===null){return 'undefined infobox!!';}
	require_once 'dlib/filter-xss.php';
	$body=filter_xss($post->body,['a','em','br','strong','cite','code','ul','ol','li','dl','dt','dd','h1','h2','h3','h4','h5','h6','img','p','hr','span','div']);
	if(strpos($body,'demosphere_tag_')!==false ||
	   strpos($body,'demosphere_dtoken_')!==false )
	{
		require_once 'demosphere-dtoken.php';
		$body=demosphere_dtokens($body);
	}

	if(strpos($desc['class'],'demosphereMapLink')!==false)
	{
		$topics=Topic::getAll();
		$fpMapTopics=demosphere_frontpage_infobox_maplink_topics($topics,$body);
		// Add eventmarker CSS only for requested topic colors
		$css='';
		foreach($fpMapTopics as $n=>$tid)
		{
			if($tid!==false){$css.=demosphere_frontpage_infobox_maplink_eventmarker_css($tid,$topics[$tid]->color);}
		}
		$currentPage->addCss($css,'inline');
		// Fix HTML for each marker
		$nth=0;
		$body=preg_replace_callback('#<p>([^<]*)(@[0-9]+)?</p>#',function($m)use(&$nth,$fpMapTopics)
						   {
							   return '<p class="eventmarker t'.$fpMapTopics[$nth++].'">'.$m[1].'<span><i></i><i></i></span></p>';
						   },$body);
	}
	$out='';
	if(strpos($desc['class'],'infoboxWithLogo')!==false){$out.='<div id="infoboxLogo'.$post->id.'" class="infoboxLogo"></div>';}
	$out.='<div id="infobox'.$post->id.'" class="'.(strlen($desc['class']) ? $desc['class'] : 'stdInfobox').'">'.
		'<h3 class="infobox-title'.
		(preg_match('@^\s*démosphère\s*\?\s*$@iu',$post->title) && strpos($desc['class'],'infoboxWithLogo')!==false ? ' isDemosphere' : '').
		'">'.($desc['title-link']!==false ? '<a href="'.$base_url.'/'.$desc['title-link'].'">' : '').
		'<span>'.ent($post->title).'</span>'.
		($desc['title-link']!==false ? '</a>' : '').
		'</h3>'.
		'<div class="ibContent">'.
		$body.'</div>'.
		'</div>';
	return $out;
}


//! Returns a list of topicIds for each "<p>time</p>" in frontpage demosphereMapLink infobox body
//! Some topicIds may be "false" meaning no distinct topic was found.
function demosphere_frontpage_infobox_maplink_topics($topics,$postBody)
{
	global $demosphere_config;

	$fpMapTopics=[];

	preg_match_all('#<p>\s*([^@<]*)(@([0-9]+))?\s*</p>#S',$postBody,$matches);
	foreach($matches[3] as $n=>$match)
	{
		$fpMapTopics[$n]=$match!=='' && isset($topics[$match]) ? intval($match) : false;
	}

	foreach($topics as $topic)
	{
		if($topic->color!=='' && array_search($topic->id,$fpMapTopics)===false)
		{
			$k=array_search(false,$fpMapTopics);
			if($k===false){break;}
			$fpMapTopics[$k]=$topic->id;
		}
	}

	return $fpMapTopics;
}

//! This could be in .tpl.css file but it would add too much CSS on frontpage of sites that use a lots of topics.
function demosphere_frontpage_infobox_maplink_eventmarker_css($tid,$color,$isMap=false)
{
	if($color===''){$color='#fff';}
	$res="
.eventmarker.t$tid{background-color: $color;}
.eventmarker.t$tid>span>i:nth-child(1)::before
{
	border-right: 1px solid black;
	border-top:   1px solid black;
	box-shadow: 1px -1px 0 $color,
                2px -2px 0 $color,
				3px -3px 0 $color,
				4px -4px 0 $color,
				5px -5px 0 $color;
}
.eventmarker.t$tid>span>i:nth-child(2)::before
{
	border-left: 1px solid black;
	border-top:  1px solid black;
	box-shadow: -1px -1px 0 $color,
                -2px -2px 0 $color,
				-3px -3px 0 $color,
				-4px -4px 0 $color,
				-5px -5px 0 $color;
}
";
    // use white text for dark colors
	$colorRGB=css_template_parse_color($color);
    // HSL lightness is not visually correct as it considers r, g and b of equal lightness. Luma is better.
	$luma=0.21*$colorRGB[1] + 0.72*$colorRGB[2] + 0.07*$colorRGB[3];
	if($luma<42)
	{
		$res.=".eventmarker.t$tid{ color: white;border: none;}\n";
		$res.="#wrap .eventmarker.t$tid>span>i::before{border: none;}\n";
		if($isMap){$res.="#map .eventmarker.t$tid:hover{ color: black;}\n";}
	}
	return $res;
}


function demosphere_frontpage_announcement()
{
	global $demosphere_config,$base_url,$user;
	$pid=intval($demosphere_config['frontpage_announcement']);
	$out='';
	if($pid===false){return $out;}
	$body=db_result('SELECT body FROM Post WHERE Post.id=%d',$pid);
	if(strlen(trim($body)))
	{
	 	$out.='<div id="announcement">';
		require_once 'demosphere-htmlview.php';
		$body=demosphere_htmlview_punctuation_nbsp($body);
		require_once 'dlib/filter-xss.php';
		$body=filter_xss($body,['a','br','em','strong','cite','code','ul','ol','li','dl','dt','dd','h1','h2','h3','h4','h5','h6','img','p','hr','span','div']);
		if(strpos($body,'demosphere_dtoken_')!==false)
		{
			require_once 'demosphere-dtoken.php';
			$body=demosphere_dtokens($body);
		}
		$out.=$body;
		if(strpos($body,'floatRight')!==false){$out.='<br style="clear: both"/>';}
		$out.='</div>';
		$out.='<!--[if (IE 7)|(IE 8)]><script type="text/javascript">ie_vml();</script><![endif]-->';
	}

	return $out;
}

function demosphere_frontpage_messages()
{
	global $demosphere_config,$base_url,$user;
	$pid=intval($demosphere_config['frontpage_messages']);
	$editLink='';
	$messages=db_result('SELECT body FROM Post WHERE Post.id=%d',$pid);
	$messages=trim(str_replace("\r",'',$messages));
	$messages=preg_split("@\s*<hr[^>]*>\s*@",$messages);
	foreach(array_keys($messages) as $k)
	{
		if(preg_match('@^\s*(<[^>]*>\s*)*#no_message@',$messages[$k])){$messages[$k]='';}
		if(preg_match('@^\s*(<[^>]*>\s*)*#@',$messages[$k])){unset($messages[$k]);}
	}
	$messages=array_values($messages);

	if(count($messages)===0){$message='';}
	else{$message=$messages[mt_rand(0,count($messages)-1)];}

	require_once 'demosphere-htmlview.php';
	$messages=demosphere_htmlview_punctuation_nbsp($messages);
	require_once 'dlib/filter-xss.php';
	$message=filter_xss($message,['a','br','em','strong','cite','code','ul','ol','li','dl','dt','dd','h1','h2','h3','h4','h5','h6','img','p','hr','span','div']);
	if(strpos($message,'demosphere_dtoken_')!==false)
	{
		require_once 'demosphere-dtoken.php';
		$message=demosphere_dtokens($message);
	}
	
	if($message==''){return $editLink;}
	return $editLink.'<div id="messages">'.$message.'</div>';
}

function demosphere_frontpage_featured_events($useEditButton=true)
{
	global $base_url,$user,$demosphere_config;
	require_once 'demosphere/demosphere-date-time.php';
	require_once 'demosphere/demosphere-common.php';
	$featured='';
	$events=Event::fetchList("FROM Event,variables WHERE variables.type='fp-featured' AND Event.id=variables.name AND Event.startTime>%d ".
							 "ORDER BY Event.startTime ASC",Event::today());
	if($user->checkRoles('admin','moderator') && $useEditButton)
	{
		$js="demosphere_event_set_featured_form_token='".dlib_get_form_token('demosphere_event_set_featured')."';".
			"add_script('".demosphere_cacheable('lib/jquery.js')."',".
			"true,function(){if(typeof frontpage_featured_events !=='undefined')frontpage_featured_events();});".
			"add_script('".demosphere_cacheable('demosphere/js/demosphere-frontpage-featured-events.js')."',".
			"true,function(){frontpage_featured_events();});".
			"demosphere_frontpage.add_css('".demosphere_merge_css(css_template('demosphere/css/demosphere-frontpage-featured-events.tpl.css'))."')";
		$featured.='<span id="edit-featured"><span class="edit-link" title="'.t('featured events').'" onclick="'.$js.'">'.
			t('[edit]').'</span></span>';
	}
	if(count($events))
	{
		require_once 'dlib/filter-xss.php';
		$featured.='<div id="featured-events" class="'.(count($events)===1 ? 'single':'').'">';
		$featured.='<ul>';
		foreach($events as $event)
		{
			$featured.='<li><h2>';
			$featured.='<a href="'.ent($event->url()).'">';
			$featured.='<span class="fdate">';
			$featured.=ent(demos_format_date('full-date-time-no-year',$event->startTime));
			$featured.=' : ';
			$featured.='</span>';
			$title=filter_xss($event->htmlTitle,['strong']);
			$sizeClass='';
			$size=mb_strlen(dlib_fast_html_to_text($title));
			if($size<41){$sizeClass='short';}
			if($size>70){$sizeClass='long';}
			if($size>85){$sizeClass='very-long';}
			$featured.='<span class="ftitle '.$sizeClass.'">'.$title.'</span>';
			$featured.='<span class="ft fcorner"><span></span></span>';
			$featured.='<span class="fr fcorner"><span></span></span>';
			$featured.='<span class="fb fcorner"><span></span></span>';
			$featured.='<span class="fl fcorner"><span></span></span>';
			$featured.='</a>';
			$featured.='<span class="ffeatured">'.t('Featured event').'</span>';
			$featured.='</h2></li>';
		}
		$featured.='</ul></div>';
	}
	return $featured;
}


// ****************************
// **** Option buttons and popups
// ****************************

//! Generate frontpage option buttons
//! These are created using the standard form items for event selection options : demosphere_event_list_form_item()
function demosphere_frontpage_options(array $demosFPArgs)
{
	global $demosphere_config,$currentPage;
	require_once 'demosphere-event-list-form.php';
	require_once 'dlib/form.php';

	$directOptions=array_keys(array_intersect(dlib_array_column($demosphere_config['frontpage_options'] ?? [],'where'),[1]));
	$form=[];
	$buttons=$directOptions;
	if(array_search('near-lat-lng',$buttons)===false){$buttons=array_merge($buttons,['near-lat-lng']);} // add to avoid js complexity
	foreach($directOptions as $name)
	{
		if($name==='near-lat-lng')
		{
			// nearLatLng is not a form item, just a button that opens an ajax popup
			$f=[];
			demosphere_event_list_form_near_lat_lng([],$f);
			$form['near-lat-lng']=['html'=>
								   '<div id="near-lat-lng-pwrap" '.(array_search('near-lat-lng',$directOptions)===false ? 'style="display: none"' : '').'>'.
							       '<span id="near-lat-lng-button" title="'.ent($f['near-lat-lng-use-map']['title']).'" class="options-button"></span>'.
							       '<div id="near-lat-lng-popup" class="options-popup">...</div>'.
								   '</div>'];
		}
		else
		{
			demosphere_event_list_form_item($name,[],$form);
		}
	}

	// more-options is not a form item, just a button that opens an ajax popup
	$hasMoreOptions=count(array_keys(array_intersect(dlib_array_column($demosphere_config['frontpage_options'] ?? [],'where'),[2])))!==0;
	$form['more-options']=['html'=>'<div id="more-options" '.(!$hasMoreOptions ? 'style="display:none"' : '').'>'.
						                 '<span id="more-options-button">+</span>'.
						                 '<div id="more-options-popup" class="options-popup">...</div>'.
						           '</div>'];

	// userCalendar: create a fake url with user cal options so that js can fill in option values
	if(isset($demosFPArgs['userCalendar']))
	{
		$calUser=User::fetch($demosFPArgs['userCalendar']);
		if(isset($calUser->data['demosphere_widget_config']))
		{
			require_once 'demosphere-widget.php';
			$widgetConfig=demosphere_widget_option_values($calUser->id);
			$opts=demosphere_widget_options_to_list_options($widgetConfig,$calUser->id);
			$opts["unused"]="unused";
			$userCalFakeUrl=demosphere_frontpage_url($opts);
			$currentPage->addJsVar('userCalFakeUrl',$userCalFakeUrl);
		}
	}

	// Custom form templates to simplify the HTML: add "options-button" class and simplify html.
	$formTemplates=form_item_templates();
	$formTemplates['0']='<div id="edit-$cname-wrapper" class="form-item form-item-$type options-button $wrapper_class" title="$title" $wrapper_attributes>'.
		'$template</div>';
	$formTemplates['1']['select'  ]=preg_replace('@^.*(<select.*</select>).*$@s','$1',$formTemplates['1']['select'  ]);
	$formTemplates['1']['checkbox']=preg_replace('@^.*(<input[^>]*>).*$@s'      ,'$1',$formTemplates['1']['checkbox']);

	// Render the form items
	$out='';
	foreach($form as $name=>$item)
	{
		$out.=form_render_item($name,$item,$formTemplates);
	}
	return $out;
}

//! Contents of ajax popup with near_lat_lng map in frontpage options
function demosphere_frontpage_options_near_lat_lng()
{
	return demosphere_frontpage_options_ajax_form(['near-lat-lng',]);
}

//! Contents of ajax popup with more options [+] on frontpage
function demosphere_frontpage_options_more_options()
{
	global $demosphere_config;
	$moreOptions=array_keys(array_intersect(dlib_array_column($demosphere_config['frontpage_options'] ?? [],'where'),[2]));
	return demosphere_frontpage_options_ajax_form($moreOptions);
}

//! Renders event list form items in $names 
function demosphere_frontpage_options_ajax_form($names)
{
	require_once 'demosphere-event-list-form.php';
	require_once 'dlib/form.php';
	require_once 'dlib/css-template.php';
	require_once 'demosphere-common.php';
	$out='';
	$out.='<style>'.ent(file_get_contents(demosphere_merge_css(css_template('demosphere/css/demosphere-event-list-form.tpl.css')))).
		            ent(file_get_contents(demosphere_merge_css(css_template('demosphere/css/demosphere-frontpage-options.tpl.css')))).
		            ent(file_get_contents('dlib/form.css')).
		'</style>';

	$form=[];
	foreach($names as $name)
	{
		demosphere_event_list_form_item($name,[],$form);
	}

	$formTemplates=form_item_templates();
	$formTemplates['0']=preg_replace('@\((<div[^>]*class="description".*?</div>)\)@s','<span class="options-help">?</span>$1',
									 $formTemplates['0']);

	foreach($form as $name=>$item)
	{
		$out.=form_render_item($name,$item,$formTemplates);
	}
	return $out;

}

//! Render the frontpage calendar, called via ajax when user changes options.
function demosphere_frontpage_options_calendar_update()
{
	// *** parse options 
	require_once 'demosphere-event-list.php';

	$rawOpts=$_GET;
	$parseRes=demosphere_event_list_parse_getpost_options($rawOpts);
	$rawOpts=$parseRes['notParsed'];
	$demosFPArgs=$parseRes['options'];

	// *** render calendar elements 
	$calendar='';
	if(isset($rawOpts['showArchiveLinks']))
	{$calendar.=demosphere_calendar_archive_links($demosFPArgs);}
	$calendar.=demosphere_calendar_event_filter_message($demosFPArgs);
	$events=demosphere_event_list($demosFPArgs);
	$calendar.=demosphere_calendar_render($events,$demosFPArgs,false);
	return $calendar;
}
