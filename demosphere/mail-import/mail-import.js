(function() {
window.mail_import = window.mail_import || {};
var ns=window.mail_import;// shortcut

$(document).ready(function()
{
	//********** importance dropdown menu
	// Menu is built with dropdown.js. Here, we only deal with item selection and ajax update.
	$('body').on('mousedown mouseup','.importance-menu li[data-importance],.importance-menu li[data-delete-rule]',function(e)
	{
		if(e.which!==1){return;}
		e.preventDefault();
		var message=$(this).parents('.message');
		var messageId=parseInt(message.attr('id').substr(7));
		var isAdd=$(this).is('li[data-importance]');
		var postData=
			{
				messageId: messageId,
				'dlib-form-token-mail_import_mail_rule_ajax':$('input[name=dlib-form-token-mail_import_mail_rule_ajax]').val(),
				'command': isAdd ? 'add' : 'delete'
			};
		if(isAdd)
		{
			postData['match-type'   ]=$(this).attr('data-match-type');
			postData['action-string']=$(this).attr('data-importance');
		}
		else
		{
			postData['rule-id'      ]=$(this).attr('data-delete-rule');
		}

		$(this).parents('.dropdown').removeClass('open');

		$.post(base_url+'/mail-import/mail-rule/ajax',
			   postData,
			   function(response)
			   {
				   if(typeof response!=='object'){alert('invalid response');return;}
				   if(response.error)
				   {
					   alert('Error : '+response.error);
					   return;
				   }
				   var infoLeftBottom=$('<div>'+response.infoLeftBottom+'</div>');
				   message.find('.infoPart.importance').remove();
				   message.find('.infoPart.cycleButtons').after(infoLeftBottom.find('.infoPart.importance'));
				   message.removeClass('importance-direct');
				   message.removeClass('importance-ordinary');
				   message.removeClass('importance-trash');
				   message.addClass('importance-'+response.newImportance);
				   message.toggleClass('auto-hidden',response.newImportance==='ordinary' && !message.hasClass('highlighted'));
			   }
			  ).error(function(){alert('error');});
	});
	
	//********** message rule manager

    $('#simple-rules-list li>input,#simple-rules-list li>select').focus(function()
	{
        $(this).parents("li").find("input[type=radio]").prop("checked","checked");
    });

});


// Exports:
//window.mail_import.xyz=xyz;

// end namespace wrapper
}());
