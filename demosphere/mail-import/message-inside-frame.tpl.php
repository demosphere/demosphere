<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="fr" xml:lang="fr" >
<head>
	<title>$message->subject</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<?= $head ?>
</head>
<body class="<?: substr_count($html,'<div class="demosphere-message-part' /*'"*/)<2 ? 'single-part ' :' ' ?>"
	data-demosphere-src="message-$message->id">
	<?=	$html ?>
</body>
</html>
