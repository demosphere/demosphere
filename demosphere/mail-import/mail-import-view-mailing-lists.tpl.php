<h1 id="mailing-lists-title" class="mail-import-top-title">_(Mailing lists) <span>(_(past year))</h1>
<table id="mailing-lists" class="gray-table">
		<tr class="">
			<th>_(tot)</th>
			<th>_(unread)</th>
			<th>_(list name)</th>
			<th>_(last)</th>
			<th>_(rules)</th>
			<th>_(subs-address)</th>
			<th>_(unsubscribe)</th>
		</tr>
	<?foreach($lists as $list){?>
		<tr class="">
			<td class="tot"         >$list['tot']</td>
			<td class="unread"      >$list['nbUnread']</td>
			<td class="list-id"     ><a href="$base_url/mail-import?display=list&listId=<?: urlencode($list['listId']) ?>">$list['name']</a></td>
			<td class="last-mail"   >
				<?if($list['recent']){?>
					<a href="<?: $list['recent']->url() ?>"><?: dcomponent_format_date('relative-short',$list['recent']->date) ?></a><?}?>
			</td>
			<td class="last-mail"   >
				<?: $list['importance']!==false ? t($list['importance']) : '' ?>
				<?: $list['source'    ]!==false ? t('source') : '' ?>
			</td>
			<td class="subs-address">$list['subsAddress']</td>
			<td class="unsubscribe" ><? foreach($list['unsubscribe']  as $url){?><a href="$url"><?: preg_replace('@:.*$@','',$url) ?></a>, <?}?></td>
		</tr>
	<?}?>
</ul>
