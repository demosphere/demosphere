<?php
/** \addtogroup mail_import 
 * @{ */
require_once 'dlib/template.php';

//! Prepares common features for all pages in mail-import.
//! CSS, JS, top menu html...
function mail_import_view($message=false,$isItemList=true,array $topRenderVars=[])
{
	global $mail_import_config,$base_url,$currentPage;
	require_once 'dcomponent/dcomponent-common.php';	
	$scriptUrl=$base_url.'/mail-import';

	$options=val($topRenderVars,'options');
	if($options===false)
	{
		$options=mail_import_view_message_list_options(['display'=>'other']);
	}

	// *** Compute the number of messages in each category displayed at the top of the page.

	$q="SELECT COUNT(*) FROM Message WHERE importance='%s' ";
	$nb['importance']['direct'  ]['unread'  ]=db_result($q." AND finishedReading=0"                       ,'direct');

	$nb['importance']['ordinary']['unread'  ]=db_result($q." AND finishedReading=0  AND isHighlighted!=0" ,'ordinary');

	if($options['listId']!==false)
	{
		$q="SELECT COUNT(*) FROM Message WHERE listId='%s' AND importance!='trash' ";
		$nb['list']['unread'  ]=db_result($q." AND finishedReading=0  AND isHighlighted!=0 AND (importance!='ordinary' OR isHighlighted=1)",$options['listId']);
	}

	dcomponent_page_css_and_js($isItemList);
	$currentPage->addCssTpl('demosphere/mail-import/mail-import.tpl.css');
	mail_import_view_title($message);

	// shortcut for frequent calls to mail_import_view_url(...)
	$url=function($override=[])use($options){return mail_import_view_url($options,$override);};

	// Top of page, with menus, that is common to all mail-import pages
	$currentPage->dcomponentMenubar=
		template_render('mail-import-top.tpl.php',[compact('scriptUrl','nb','options','url'),$topRenderVars]);

	$currentPage->addCssTpl('demosphere/mail-import/message-item.tpl.css');
	$currentPage->addJs    ('demosphere/mail-import/mail-import.js');

	$currentPage->addJs ('dlib/dropdown.js');
	$currentPage->addCss('dlib/dropdown.css');

	// Common variables used in templates
	return ['scriptUrl'=>$base_url.'/mail-import'];
}

//! Sets <title></title> by using current url and current $message 
function mail_import_view_title($message)
{
	global $currentPage;
	$title='mi-';
	if(preg_match('@mail-import(/(message)/\d+)?/([^/?&]+)@',$_GET['q'],$matches))
	{
		$title.=$matches[3];
	}
	if($message!==false){$title.=': '.$message->subject;}
	if($title==='mi-'){$title=t('mail');}
	$currentPage->title=$title;
}

//! This is the main display. It shows a list of emails according options.
function mail_import_view_message_list()
{
	global $mail_import_config,$dlib_config;
	global $currentPage;

	dcomponent_progress_log_start('messages');
	try{mail_import_imap_fetch_if_needed();}
	catch(Exception $e)
	{
		dlib_message_add("Fetching mail from imap failed.".($dlib_config['debug'] ? ' :: '.ent($e->getMessage()) : '' ),
						 'error');
	}
	dcomponent_progress_log_end();

	// Check for invalid messages (this should not happen)
	$badCt=(int)db_result('SELECT COUNT(*) FROM Message WHERE dateFetched=0');
	if($badCt>0){dlib_message_add(ent(t('Deleting !nb corrupted mails.',['!nb'=>$badCt])),'error');}
	db_query('DELETE FROM Message WHERE dateFetched=0');

	// *** GET options:
	$options=mail_import_view_message_list_options($_GET);

	Message::rehighlightAll();

	// ******** Build sql query

	$sql='';
	$sql.='WHERE dateFetched!=0 ';

	// *** restrict to display type 
	if($options['display']==='sent'      ){$sql.="AND isSent=1 ";}
	if($options['display']==='thread'    ){$sql.="AND threadNb=".   intval(          $options['threadNb'     ])."  ";}
	if($options['display']==='list'      ){$sql.="AND listId='".    db_escape_string($options['listId'])."' ";}
	if($options['display']==='importance'){$sql.="AND importance='".db_escape_string($options['importance'   ])."' ";}

	// *** restrict according to $options['select']
	if($options['select']==='unread'  ){$sql.="AND finishedReading=0 AND (importance!='ordinary' OR isHighlighted=1) AND importance!='trash' ";}
	if($options['select']==='finished'){$sql.="AND finishedReading!=0 AND importance!='trash' ";}
	if($options['select']==='hidden'  ){$sql.="AND finishedReading=0 AND importance='ordinary' AND isHighlighted=0 AND importance!='trash' ";}

	// *** order
	$sql.="ORDER BY ";
	if($options['order']==='urgent')
	{
		$sql.="importance='direct' DESC, ";
		$sql.="isHighlighted DESC, ";
		$sql.="firstFutureHLDate=0 ASC, ";
		$sql.="firstFutureHLDate ASC, ";
	}
	$sql.="dateFetched DESC, id DESC, ";
	$sql=substr($sql,0,-2);

	// actual query 
	$itemsPerPageChoices=$options['density']==='full' ? [5,10,20,40,80] : [5,10,20,40,100,200,500,1000];
	$defaultItemsPerPage=$options['density']==='full' ? 20 : 100;
	$pager=new Pager(['itemName'=>t('mails'),'alwaysShow'=>true,
					  'itemsPerPageChoices'=>$itemsPerPageChoices,
					  'defaultItemsPerPage'=>$defaultItemsPerPage]);
	$messages=Message::fetchList('SELECT SQL_CALC_FOUND_ROWS * FROM Message '.$sql.' '.$pager->sql());
	$pager->foundRows();

	// Thread display: determine a title for this thread
	$threadTitle=false;
	if($options['display']==='thread')
	{
		$bestSubjectHash=db_result('SELECT subjectHash FROM Message WHERE threadNb=%d GROUP BY subjectHash ORDER BY COUNT(*) DESC LIMIT 1',
								   $options['threadNb']);
		$threadTitle    =db_result("SELECT subject FROM Message WHERE threadNb=%d AND subjectHash='%s' ORDER BY dateFetched ASC LIMIT 1",
								   $options['threadNb'],$bestSubjectHash);
	}

	return template_render($options['density']==='full' ? 'message-list-full.tpl.php' : 'message-list-compact.tpl.php',
						   [mail_import_view(false,true,compact('pager','options','threadTitle')),
							compact('messages','pager','options')]);
}

//! Parses options from $get (typically $get=$_GET) and returns a safe, validated $options array.
function mail_import_view_message_list_options(array $get=[])
{
	$options=[];

	// Which type of display is requested ?
	// "other" is for pages that do not actually display a Message list (all mail-import pages need $options for top menu).
	$options['display']=mail_import_checkval(val($get,'display','importance'),['importance','thread','list','sent','other']);

	// Data for this display type
	$options['importance'     ]=mail_import_checkval(val($get,'importance','direct'),Message::$importances);
	$options['threadNb'       ]=isset($get['threadNb'     ]) ? intval($get['threadNb']) : false;
	$options['listId'         ]=isset($get['listId'       ]) ?        $get['listId'  ]  : false;

	// Remove data for the wrong display type
	if($options['display']!=='importance'){$options['importance']=false;}
	if($options['display']!=='thread'    ){$options['threadNb'  ]=false;}
	if($options['display']!=='list'      ){$options['listId'    ]=false;}

	// Optional: select which mails are displayed, for this display type
	$defaultSelect=['importance'=>'unread','thread'=>'all','list'=>'unread','sent'=>'all','other'=>'all'][$options['display']];
	$options['select']=mail_import_checkval(val($get,'select',$defaultSelect),['all','unread','finished','hidden',]);
	if($options['importance']==='trash'   ){$options['select']='all';}
	if($options['display'   ]==='thread'  ){$options['select']='all';}

	// order
	$defaultOrder=$options['select']==='unread' && $options['importance']!=='direct' ? 'urgent' : 'date';
	$options['order']=mail_import_checkval(val($get,'order',$defaultOrder),['urgent','date']);

	// density
	$defaultDensity=$options['select']==='unread' || $options['display']==='thread' ? 'full' : 'compact';
	$options['density']=mail_import_checkval(val($get,'density',$defaultDensity),['full','compact']);
		
	return $options;
}

//! Checks if a value is within a set of allowed values and returns value with correct type
//! Tries to find best match. Aborts if none found.
//! If it does not abort, it always returns one of the $allowedValues
//! FIXME: this is a copy of demosphere_search_checkval(), think about putting it into dlib
function mail_import_checkval($v,array $allowedValues)
{
	if(is_string($v) && ctype_digit($v)){$v=(int)$v;}
	// first try strict search
	$f=array_search($v,$allowedValues,true);
	if($f!==false){return $allowedValues[$f];}
	// then try fuzzy search (this can give unexpected results between strings an bools, but previous search should have caught valid string matches)
	$f=array_search($v,$allowedValues);
	if($f!==false){return $allowedValues[$f];}
	dlib_bad_request_400('invalid search option');
}

//! Returns an url that contains options set by override and replace values in $_GET with those in $options.
//! Only options that are set either in $_GET or in $override will be actually set in returned url.
//! This avoids longs url's with lots of default options.
//! Density and order are propagated on urls that use this function (currently: not the main categories).
//! This needs some thought and experience. It might be better to set per session / user pref / per display type...
//! But that might be confusing and user might lose the default values.
function mail_import_view_url(array $options,array $override=[])
{
	global $base_url;
	$url=$base_url.'/mail-import?';
	foreach($options as $option=>$value)
	{
		// Ignore options that are not in $_GET or in $override (assume we use default for them)
		if(!isset($_GET[$option]) && !isset($override[$option])){continue;}
		if(isset($override[$option])){$value=$override[$option];}
		$v=$value;
		if(is_bool($v)){$v=(int)$v;}
		$url.=$option.'='.urlencode($v).'&';
	}
	$url=preg_replace('@[?&]$@','',$url);
	return $url;
}

//! Displays a table with all mailing lists that are present in Messages.
function mail_import_view_mailing_lists()
{
	global $currentPage;	
	$currentPage->addCssTpl('dlib/table-tools.css');
	$timePeriod=strtotime('1 year ago');
	$lists=db_arrays("SELECT COUNT(*) AS tot, ".
					 "SUM((importance='direct' OR isHighlighted=1) AND finishedReading=0 AND importance!='trash') nbUnread, ".
					 "listId FROM Message WHERE ".
					 "listId!='' AND importance!='trash' AND dateFetched>%d GROUP BY listId ORDER BY tot DESC",$timePeriod);
	foreach($lists as &$list)
	{
		$list['name']=Message::mailingListName($list['listId']);
		// The most recent Message of this mailing-list
		$recentId=db_result("SELECT id FROM Message WHERE listId='%s' ORDER BY dateFetched DESC LIMIT 1",$list['listId']);
		$list['recent']=Message::fetch($recentId,false);
		$list['importance']=db_result_check("SELECT actionString FROM MailRule WHERE matchType=%d AND matchString='%s' AND actionType=%d ".
											"ORDER BY priority DESC LIMIT 1",
											MailRule::matchTypeEnum('list-id'),$list['listId'],MailRule::actionTypeEnum('importance'));
		$list['source']    =db_result_check("SELECT actionString FROM MailRule WHERE matchType=%d AND matchString='%s' AND actionType=%d ".
											"ORDER BY priority DESC LIMIT 1",
											MailRule::matchTypeEnum('list-id'),$list['listId'],MailRule::actionTypeEnum('source'));
		$list['recent']=Message::fetch($recentId,false);
		$list['subsAddress']='';
		$list['unsubscribe']=[];
		if($list['recent']!==null)
		{
			$list['subsAddress']=str_replace(',',', ',$list['recent']->getHeader('Envelope-to'     ));
			$unsubs=             $list['recent']->getHeader('List-Unsubscribe');
			foreach(explode(',',$unsubs) as $unsubsMethod)
			{
				require_once 'dlib/filter-xss.php';
				if(preg_match('@<(mailto:[^>]*)>@'  ,trim($unsubsMethod),$matches)){$list['unsubscribe'][]=filter_xss_check_url($matches[1]);}
				if(preg_match('@<(https?://[^>]*)>@',trim($unsubsMethod),$matches)){$list['unsubscribe'][]=filter_xss_check_url($matches[1]);}
			}
		}
	}
	unset($list);

	return template_render('mail-import-view-mailing-lists.tpl.php',
						   [mail_import_view(),compact('lists')]);
}

//! Display a form to compose and send an email.
//! Optionally reference (reply, reply-all or forward) an existing message.
//! The sent mail is also saved as a Message.
function mail_import_view_send_mail($refMessageId=false)
{
	global $user,$currentPage,$base_url,$demosphere_config;// FIXME
	$scriptUrl=$base_url.'/mail-import';
	mail_import_view();

	$currentPage->addCssTpl('demosphere/mail-import/mail-import-send-mail.tpl.css');

	if($refMessageId===false){$action='send';}
	else
	{
		$action=val($_GET,'action');
		if(array_search($action,['reply','reply-all','forward'],true)===false){dlib_bad_request_400('invalid action');}
	}
	
	require_once 'dlib/mail-and-mime.php';

	require_once 'demosphere-emails.php';
	if(demosphere_emails_hosted_domain()===false)
	{
		fatal('Sending mail is only possible from sites hosted on the Demosphere server.');
	}

	$currentPage->addJs('demosphere/mail-import/mail-import-send-mail.js');

	$from='';
	$to='';
	$cc='';
	$bcc='';
	$subject='';
	$body='';

	$refMessage=false;
	if($refMessageId!==false)
	{
		$refMessage=Message::fetch($refMessageId,false);
		if($refMessage===null){dlib_not_found_404();}
	}

	// **** from: build list of email addresses that this user can send from

	require_once 'mail-import.php';
	require_once 'demosphere-emails.php';
	$allowedFrom=[];
	if(demosphere_emails_hosted_is_valid($demosphere_config['contact_email'],true))
	{
		$allowedFrom[]=$demosphere_config['contact_email'];
	}
	if(demosphere_emails_hosted_is_valid($user->email,true))
	{
		$allowedFrom[]=$user->email;
	}
	foreach(mail_import_destination_addresses()  as $address)
	{
		// FIXME: quick hack, we need a better way of identifying this address as being the mailing list subscription address
		if(strpos($address,'list')!==false || strpos($address,'minfos')!==false)
		{
			$allowedFrom[]=$address;
		}
	}

	if(count($allowedFrom)===0)
	{
		return 'Sorry, there are no email addresses you can send from. You can only send mail from addresses that have the domain "'.
			ent(demosphere_emails_hosted_domain()).'".'.
			' Also, you can only send mail from 1) the sites contact address 2) your user address or 3) the mailing-list address.'.
			' Please contact the server admin for more information and help.';
	}

	// **** to,cc

	switch($action)
	{
	case 'send':
		$to='';
		$cc='';
		break;
	case 'reply':
		$to=$refMessage->from;
		if($refMessage->getHeader('Reply-To')!==''){$to=$refMessage->getHeader('Reply-To');}
		// special case, reply to one's own message 
		if($to===$allowedFrom[0]){$to=$refMessage->to;}
		$cc='';
		break;
	case 'reply-all':
		$to=$refMessage->from;
		$cc='';
		if($refMessage->to!=''              ){$cc.=$refMessage->to.',';}
		if($refMessage->getHeader('cc')!==''){$cc.=$refMessage->getHeader('cc').',';}
		$cc=preg_replace('@,+$@','',$cc);
		break;
	case 'forward':
		$to='';
		$cc='';
		break;
	}

	// **** subject
	switch($action)
	{
	case 'send': 
		$subject='';
        break;
	case 'reply':
	case 'reply-all':
		$subject='Re: '.preg_replace('@\bre: *@i','',$refMessage->subject);
		break;
	case 'forward': 
		$subject='Fwd: '.preg_replace('@\bfwd: *@i','',$refMessage->subject); 
        break;
	}


	// **** body

	if($refMessage!==false)
	{
		// build text-only version of $refMessage 
		$html=$refMessage->getFullHtml(false,true);
		$text=Html2Text::convert($html);
		$text=str_replace([" ","\r\n","\r"],[" ","\n","\n"],$text);
		$text=preg_replace('@[\n]{4,}@',"\n",$text);
		$text=trim($text);
		$text=mail_import_view_wordwrap($text);
		
		$body="\n\n\n\n";

		if($action==='forward')
		{
			$body.='-------- '.t('Forwarded mail').' --------'."\n";
			$linePrefix='';
			$body.=mail_import_view_wordwrap('Subject: '.$refMessage->subject)."\n";
			$body.=mail_import_view_wordwrap('Date: '.date('r',$refMessage->date))."\n";
			$body.=mail_import_view_wordwrap('To: '.$refMessage->to)."\n";
			$body.=mail_import_view_wordwrap('From: '.$refMessage->from)."\n";
		}
		else
		{

			$fromParts=dlib_email_address_parts($refMessage->from);
			$body.=t('On !date, !address wrote:',
						  ['!date'=>dcomponent_format_date('full-date-time',$refMessage->date),
						   '!address'=>val($fromParts,'clean-name',$refMessage->from)])."\n";
			$linePrefix='> ';
		}

		$body.=$linePrefix."\n";
		foreach(explode("\n",$text) as $line)
		{
			$body.=$linePrefix.$line."\n";
		}
		$body.=$linePrefix."\n";
		$body.="\n\n";

		if($action==='forward')
		{
			$body.='-------- '.t('End forwarded mail').' --------'."\n";
			$body.="\n\n";
		}
	}
	else
	{
		$body='';
	}

	// **** Build form

	$items=[];

	$items['ref-message']=['type'=>'data','data'=>$refMessage];

	switch($action)
	{
	case 'send':     $top=t('Compose new mail');break;
	case 'reply':    $top=t('Reply to <a href="@url">mail</a>'        ,['@url'=>$refMessage->url()]);break;
	case 'reply-all':$top=t('Reply to all for <a href="@url">mail</a>',['@url'=>$refMessage->url()]);break;
	case 'forward':  $top=t('Forward this <a href="@url">mail</a>'    ,['@url'=>$refMessage->url()]);break;
	}
	$items['top'    ]=['html'=>'<h1 class="mail-import-top-title">'.$top.'</h1>'];

	$items['from'   ]=['type'=>'select',
					   'title'=>t('From'),
					   'options'=>array_combine($allowedFrom,$allowedFrom),
					  ];

	$items['to'     ]=['type'=>'textfield',
					   'title'=>t('To'),
					   'default-value'=>$to,
					   'check-dns'=>true,
					   'allow-display-name'=>true,
					   'required'=>true,
					   'validate'=>'validate_email_list',
					   'attributes'=>['maxlength'=>2000],
					   'field-suffix'=>
					   '<span id="cc-bcc-buttons">'.
					   '    <input id="show-cc"  type="button" value="'.ent(t('Cc' )).'">'.
					   '    <input id="show-bcc" type="button" value="'.ent(t('Bcc')).'">'.
					   '</span>',
					  ];

	$items['cc'     ]=['type'=>'textfield',
					   'title'=>t('Cc'),
					   'default-value'=>$cc,
					   'check-dns'=>true,
					   'allow-display-name'=>true,
					   'attributes'=>['maxlength'=>2000],
					   'validate'=>'validate_email_list'];

	$items['bcc'     ]=['type'=>'textfield',
					   'title'=>t('Bcc'),
					   'default-value'=>$bcc,
					   'check-dns'=>true,
					   'allow-display-name'=>true,
					   'attributes'=>['maxlength'=>2000],
					   'validate'=>'validate_email_list'];

	$items['subject']=['type'=>'textfield',
					   'title'=>t('Subject'),
					   'attributes'=>['spellcheck'=>'true'],
					   'default-value'=>$subject];

	$items['body'   ]=['type'=>'textarea',
					   'title'=>t('Body'),
					   'attributes'=>['spellcheck'=>'true'],
					   'default-value'=>$body];

	// Create 10 attachments, only one is initially shown. More are shown when user clicks on "attach" button.
	for($i=0;$i<10;$i++)
	{
		$items['attachment-'.$i]=['type'=>'file',
								  'title'=>t('Attachment'),
								  'wrapper-attributes'=>['class'=>['attachment','detached']],
								  'field-suffix'=>'<button type="button" class="detach">x</button>',
								 ];
	}
	$items['bottom']=['html'=>'<button id="attach" type="button" title="'.ent(t('Add file attachment')).'"><span></span></button>'];

	$items['send'  ]=['type'=>'submit',
					   'value'=>t('Send'),
					  'validate'=>function($items)
		               {
						   // Check if user is trying to send too many emails
						   $ct=0;
						   foreach(['to','cc','bcc'] as $name)
						   {
							   $v=$items[$name]['value'];
							   if(trim($v)===''){continue;}
							   $ct+=substr_count($v,',')+1;
						   }
						   if(dlib_throttle_count('mail-import-send-mail-24h',24*3600)+$ct>40)
						   {
							   dlib_message_add(t('You cannot send more than 40 mails in 24h. Mass-mailing can cause problems for other users of this server.'),
												'error');
							   return false;
						   }
					   },
					  'submit'=>'mail_import_view_send_mail_submit',
					  ];

	require_once 'dlib/form.php';
	return form_process($items,['id'=>'send-mail']);
}

function mail_import_view_send_mail_submit(array $items)
{
	global $demosphere_config,$base_url;// FIXME
	$scriptUrl=$base_url.'/mail-import';
	$values=dlib_array_column($items,'value');

	require_once 'lib/class.phpmailer.php';
	$mail=new PHPMailer();
	$mail->CharSet="utf-8";
	$mail->SetFrom($values['from']);
	foreach(['to'=>'addAddress','cc'=>'addCC','bcc'=>'addBCC'] as $type=>$fct)
	{
		if(trim($values[$type])!=='')
		{
			foreach(explode(',',$values[$type]) as $address)
			{
				$parts=dlib_email_address_parts($address);
				$mail->$fct($parts['address'],val($parts,'clean-name',''));
				dlib_throttle_add('mail-import-send-mail-24h');
			}
		}
	}

	$mail->Subject=$values['subject'];

	$refMessage=$items['ref-message']['data'];
	if($refMessage!==false)
	{
		$refMessageId=$refMessage->getHeader('Message-Id');
		if($refMessageId!==false)
		{
			$mail->addCustomHeader('In-Reply-To', $refMessageId);
		}
	}

	$mail->Body   =$values['body'];

	// Attachments
	foreach($items as $name=>$item)
	{
		if(strpos($name,'attachment-')!==0){continue;}
		if($_FILES[$name]['error']===UPLOAD_ERR_NO_FILE){continue;}
		// form_process() has already checked for errors
		$finfo = finfo_open(FILEINFO_MIME_TYPE);
		$mimeType=finfo_file($finfo,$_FILES[$name]['tmp_name']);
		$mail->addAttachment($_FILES[$name]['tmp_name'],$_FILES[$name]['name'],'base64', $mimeType);
	}

	if(!$mail->send()) 
	{
		dlib_message_add(t("Error while sending mail."),'error');
		return;
	}

	dlib_message_add(t("Mail successfully sent"));

	// Save a copy of mail into Message so that it appears in message list.
	try
	{
		$message=Message::createFromRaw($mail->getSentMIMEMessage());
		$message->isSent=1;
		$message->save();
	}catch(Exception $e){dlib_message_add('Saving copy of sent mail failed.','error');}

	dlib_redirect($scriptUrl);
}

//! Validate a comma separated list of email addresses
//! Think about moving this info dlib/form.php
function validate_email_list($value,array $item)
{
	require_once 'dlib/form.php';
	if(trim($value)===''){return;}
	$errors=[];
	$emails=explode(',',$value);
	foreach($emails as $email)
	{
		if(trim($email)===''){$errors[]=t('Invalid empty address in list');continue;}
		$error=form_validate_email(trim($email),$item);
		if($error!==null){$errors[]=$error;}
	}
	if(count($errors)){return implode('<br/>',$errors);}
}

//! Copied from http://stackoverflow.com/questions/3825226/multi-byte-safe-wordwrap-function-for-utf-8
function mail_import_view_wordwrap($str, $width=75, $break="\n", $cut=false) 
{
	$lines=explode($break, $str);
	foreach ($lines as &$line) 
	{
		$line=rtrim($line);
		if(mb_strlen($line)<=$width){continue;}
		$words=explode(' ',$line);
		$line='';
		$actual='';
		foreach($words as $word) 
		{
			if(mb_strlen($actual.$word)<=$width){$actual.=$word.' ';}
			else 
			{
				if($actual!=''){$line.=rtrim($actual).$break;}
				$actual=$word;
				if($cut) 
				{
					while(mb_strlen($actual)>$width) 
					{
						$line.=mb_substr($actual,0,$width).$break;
						$actual=mb_substr($actual,$width);
					}
				}
				$actual.=' ';
			}
		}
		$line.=trim($actual);
	}
	return implode($break, $lines);
}


//! MailRule list, view & edit use dbobject-ui which is customized here. (FIXME: wil we actually use this ?)
function mail_import_mail_rule_class_info(array &$classInfo)
{
	global $base_url,$currentPage;
	// ******* list setup
	$classInfo['list_setup']=function(){mail_import_view();};
	$classInfo['list_orderBy']='priority';
	$classInfo['list_orderDir']='ASC';

	// ******* edit form setup
	$classInfo['edit_form_alter']=
		function(&$items,&$options,$mf)use(&$classInfo)// FIXME remove reference required by PHP < 5.3.6
		{
			global $base_url;
			mail_import_view(false,false);
			// fix redirect
			if(isset($_GET['messageId']))
			{
				$manageRulesUrl=$base_url.'/mail-import/message/'.intval($_GET['messageId']).'/manage-rules';
				$items['save'  ]['redirect']=$manageRulesUrl;
			}
		};

	// ******* view setup
	$classInfo['view_vars']=
		function($rule,$isDelete)
		{
			mail_import_view();
			return [];
		};
	$classInfo['delete_alter']=
		function(&$items,$mf)
		{
			global $base_url;
			// fix redirect
			if(isset($_GET['messageId']))
			{
				$manageRulesUrl=$base_url.'/mail-import/message/'.intval($_GET['messageId']).'/manage-rules';
				$items['delete']['redirect']=$manageRulesUrl;
				$items['cancel']['redirect']=$manageRulesUrl;
			}			
		};
}

//! Displays a full html page with an interface to manage mail rules by using values for the Message given by $messageId.
function mail_import_view_message_rule_manager($messageId)
{
	global $base_url,$currentPage;
	$message=Message::fetch($messageId,false);
	if($message===null){dlib_not_found_404();}
	$scriptUrl=$base_url.'/mail-import';

	$miv=mail_import_view($message,false);

	if(isset($_POST['simple-rule']))
	{
		dlib_check_form_token('Message::manageRules');
		$ruleDesc=explode('/',$_POST['simple-rule']);
		if(count($ruleDesc)<2){dlib_bad_request_400('invalid simple-rule');}
		$matchType =$ruleDesc[0];
		$actionType=$ruleDesc[1];
		$matchString  =trim($_POST['match-string-'.$matchType.'-'.$actionType]);
		$actionString =trim($_POST[                $matchType.'-'.$actionType]);
		$error=mail_import_view_create_mail_rule($matchType,$matchString,$actionType,$actionString,$message);
		if($error!==false){dlib_bad_request_400('invalid rule: '.$error);}

		dlib_message_add(t("Successfully added new rule."));

		if($actionType==MailRule::actionTypeEnum('importance'))
		{
			dlib_redirect($message->url());
		}
	}

	$rules=MailRule::findMatchingMailRuleIds($message);

	if(count($rules))
	{
		global $dbobject_ui_config;
		$ciOverride=['list_where'=>'WHERE id IN ('.implode(',',$rules).')','list_noSort'=>true,'list_noRotate'=>true,
					 'list_orderSql'=>'ORDER BY priority DESC, id DESC'];
		$mailRuleList=dbobject_ui_list('MailRule',$ciOverride);
		// dirty hack: change links so that edit & delete mail fitlers redirect to this page
		$mailRuleList=preg_replace('@(/mail-import/mail-rule/\d+/(edit|delete))"@','$1?messageId='.$message->id.'"',$mailRuleList);
	}
	else{$mailRuleList=t('[ No rules ]');}

	// Remove dbobject_ui tabs
	$currentPage->tabs=[];
	
	return template_render('message-rule-manager.tpl.php',
						   [$miv,
							compact('message','mailRuleList'),
						   ]);
}

//! Validates arguments and creates a MailRule for a $message.
//! Returns the rule on success, false otherwise.
//! Example : $matchTypeEnum='from' $actionTypeEnum='importance' $actionString='ordinary'
function mail_import_view_create_mail_rule($matchTypeVal,$matchString,$actionTypeVal,$actionString,Message $templateMessage)
{
	//vd($templateMessage,$matchTypeVal,$matchString,$actionTypeVal,$actionString,$_POST);die('pp');
	$matchType =MailRule::matchTypeEnum( $matchTypeVal);
	$actionType=MailRule::actionTypeEnum($actionTypeVal);
	if($matchType ===false){return 'invalid match type';}
	if($actionType===false){return 'invalid action type';}

	if($actionTypeVal==='importance')
	{
		if(array_search($actionString,Message::$importances,true)===false){return 'invalid actionString';}
	}
	else
	{
		require_once 'dlib/filter-xss.php';
		$actionString=filter_xss($actionString);
	}

	// If $matchString is not provided, that means we are creating a rule that should match $templateMessage
	if($matchString===false)
	{
		$matchString=MailRule::suggestMatchStringFromMessage($templateMessage,$matchType);
		// simple interface only proposes mailing list subject match (not any subject)
		if($matchTypeVal==='subject'){$matchString=$templateMessage->getMailingListSubject();}
	}

	// Check for duplicates
	$nbDuplicates=db_result("SELECT COUNT(*) FROM MailRule WHERE matchType=%d AND matchString='%s' AND actionType=%d",
							$matchType,$matchString,$actionType);
	if($nbDuplicates>0){return 'attempt to create a duplicate mail rule';}

	$rule=new MailRule(compact('matchType','matchString','actionType','actionString'));
	$rule->save();
	return false;
}

//! Handle ajax call to add or delete a MailRule. 
//! Returns JSON with part of the Message re-rendered, so that the interface can display the new "importance" dropdown menu.
function mail_import_mail_rule_ajax()
{
	global $base_url;
	dlib_check_form_token('mail_import_mail_rule_ajax');
	$message=Message::fetch($_POST['messageId'],false);
	if($message===null){dlib_not_found_404();}

	if($_POST['command']==='add')
	{
		$error=mail_import_view_create_mail_rule($_POST['match-type'],false,'importance',$_POST['action-string'],$message);
		if($error!==false){return ['error'=>'failed creating rule: '.$error];}
	}
	else
	if($_POST['command']==='delete')
	{
		$rule=MailRule::fetch($_POST['rule-id'],false);
		if($rule===null){return ['error'=>'cannot delete rule : does not exist'];}
		$rule->delete();
	}
	else{dlib_bad_request_400('invalid command');}
	
	// reload because $rule->save() causes refresh that changes $message
	$message=Message::fetch($message->id);

	// We need to re-render part of the message to get the new importance menu
	$parts=template_render_parts('message-item.tpl.php',['item'=>$message,
														 $message->viewData(),
														 'scriptUrl'=>$base_url.'/mail-import']);

	return [
		'error'=>false,
		'newImportance'=>$message->importance,
		'infoLeftBottom'=>$parts['infoLeftBottom']];
}

//! Page that displays a confirm button for finishing a list of messages.
function mail_import_view_finish($type,$threadNb=false)
{
	// SQL query and text message for each type
	$q="dateFetched!=0 AND (importance='direct' OR (importance='ordinary' AND isHighlighted=1)) AND finishedReading=0 ";
	$text='';
	switch($type)
	{
	case 'old':
		$finishOldDays=20;
		$q.="AND dateFetched<".intval(strtotime($finishOldDays.' days ago'));
		$text=t('Finish mail that are more than !nb days old.',['!nb'=>$finishOldDays]);
		break;
	case 'ordinary':
		$q.="AND firstFutureHLDate=0 AND importance='ordinary' ";
		$text=t('Finish ordinary mail with no future date.');
		break;
	case 'thread':
		$q.="AND threadNb=".intval($threadNb)." ";
		$text=t('Finish all mail in this thread.');
		break;
	}
	
	
	$pager=new Pager(['defaultItemsPerPage'=>100,'itemName'=>t('mails')]);
	$messages=Message::fetchList("SELECT SQL_CALC_FOUND_ROWS * ".
						"FROM Message WHERE ".
						$q.' '.
						"ORDER BY dateFetched DESC, id DESC".
						$pager->sql());
	$pager->foundRows();
	// We need a full list, since $messages is limited by pager
	$allIds=db_one_col('SELECT id FROM Message WHERE '.$q);

	if(count($messages)==0){mail_import_view();return '<p>'.t('No mail left.').'</p>';}

	$list=template_render('dlib/pager.tpl.php',['pager'=>$pager]);
	$list.=template_render('message-list-compact.tpl.php',
						  [mail_import_view(),
						   'messages'=>$messages,
						   'pager'=>$pager,
						  ]);
	require_once 'dlib/form.php';
	
	$items=[];
	$items['text']=['html'=>
					'<h1 class="mail-import-top-title">'.ent($text).'</h1>'.
					'<p>'.
					ent(t('Are you sure that you want to finish the following !nb mails?',['!nb'=>count($allIds)])).'<br/>'.
					ent(t('Finished mail is not deleted. It is still available in the "finished" menus and can still be searched.')).
					'</p>',
				   ];

	$items['confirm']=['type'=>'submit',
					   'value'=>t('confirm'),
					   'submit'=>function()use($allIds)
		               {
						   db_query('UPDATE Message SET finishedReading=%d WHERE id IN ('.implode(',',$allIds).')',time());
						   dlib_message_add(ent(t('Finished mails :')).implode(', ',$allIds));
						   dcomponent_search_proxy_call_catch('demosphere_search_dx_set_documents_status','log','mail',$allIds,0);
					   },
					   'redirect'=>'mail-import',
					  ];
	$items['list']=['html'=>$list];
	return form_process($items,['id'=>'finish-confirm-form']);
}

//! Displays technical details on a Message (all Message properties + parts + full headers)
function mail_import_view_message_details($messageId)
{
	require_once 'dlib/filter-xss.php';
	$message=Message::fetch($messageId);
	return template_render('message-details.tpl.php',
						   [mail_import_view($message),
							'message'=>$message,
						   ]);
}

//! Display single Message
function mail_import_view_message($messageId)
{
	$message=Message::fetch($messageId,false);
	if($message===null){dlib_not_found_404();}
	$commonVars=mail_import_view($message,true);
	$viewData=$message->viewData(true);
	return template_render('dcomponent/dcitem.tpl.php',
						   [$commonVars,
							$viewData,
							'item'=>$message,
							template_render_parts('message-item.tpl.php',
												  [$commonVars,
												   'item'=>$message,
												   $viewData,
												  ]),
						   ]);
}

//! This is the page rendered inside an iframe for each message (full view)
//! HTML is displayed in safe domain. So XSS is not possible. 
//! However, js img, links... can be used to confirm email addresses for spammers.
//! This is solved by Content-Security-Policy (see below)
function mail_import_view_message_inside_frame($messageId)
{
	global $mail_import_config,$base_url,$currentPage;
	if(!demosphere_safe_domain_check()){fatal('mail_import_view_message_inside_frame() can only be displayed in safe domain.');}

	$currentPage->addCssTpl('demosphere/mail-import/message-inside-frame.tpl.css');

	$message=Message::fetch($messageId);

	$htmlInfo=$message->getHighlighted(false);

	$disablePrivacyChecks=isset($_GET['disablePrivacyChecks']);

	$head=dcomponent_dcitem_inside_frame_js_and_css('mail-import',$disablePrivacyChecks ? false : 'allow-inline-css'); 

	// html attachments are full documents (with head, body...). $htmlInfo['content'] is the (cleaned-up) body
	// $htmlInfo['head'] contains some of their headers (ex: <style>), so that we can add them here.
	$head.=$htmlInfo['head'];

	// Do not need to tidy : each html part has already been tidied
	$html=$htmlInfo['content'];

	if(!$disablePrivacyChecks)
	{
		global $demosphere_config;
		// Replace external images by an image showing "blocked image" 
		// Note: this is only cosmetic. There are lots of other ways people can display images or do external requests.
		// The actual blocking is done by Content-Security-Policy in dcomponent_dcitem_inside_frame_js_and_css()
		$okUrl=preg_quote($demosphere_config['safe_base_url'].'/safe-domain/mail-import','@');
		$html=preg_replace('@(<img[^>]*)src=([\'"](?!'.$okUrl.')(https?|ftp)://[^\'"]*[ \'"])@',
						   '$1 title=$2 src="'.$mail_import_config['dir_url'].'/blocked-image.png" data-demosphere-other-site-image="yes" ',
						   $html);
	}

	echo template_render('message-inside-frame.tpl.php',
						 [compact('message','mail_import_config','head','html'),
						  'sdLogin'=>preg_replace('@-.*$@','',$_GET['sdtoken']),
						 ]);
}
/** @} */

?>