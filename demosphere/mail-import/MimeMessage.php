<?php
/** \addtogroup mail_import 
 * @{ */

//! A tree representing the parts of a Mime email message.
//! This is basically a wrapper around the tree built by imap_fetchstructure()
//!
//! It would be great to find a better Mime parser than the imap functions which are *very* quirky, old, low level, and poorly documented.
//! A full replacement imap library might be interesting too.
class MimeMessage
{
	// http://php.net/manual/en/function.imap-fetchstructure.php
	public static $encodings=[0=>'7BIT',1=>'8BIT',2=>'BINARY',3=>'BASE64',4=>'QUOTED-PRINTABLE',5=>'OTHER'];
	public static $types    =[0=>'text',1=>'multipart',2=>'message',
							  3=>'application',4=>'audio',5=>'image',
							  6=>'video',7=>'model',8=>'other',
							  // Invalid (but appear strangely?): see uw-imap-2007f~dfsg/src/c-client/mail.h
							  // http://php.net/manual/en/function.imap-fetchstructure.php
							  9 =>'unknown',10=>'unknown',11=>'unknown',12=>'unknown',13=>'unknown',14=>'unknown',15=>'unknown',
							 ];

	//! imap connection either passed are constrctor arg, or opened in constructor (only for raw mail)
	public $imap;
	//! Unique imap id for this imap connection (===1 for raw mail)
	public $uid;

	//! Root of a tree of parts. This is initially given by imap_fetchstructure. 
	//! This structure is fairly difficult to use. 
	//! We have added following fields to each part:
    //! 'mm_message'    => (internal use only)
    //! 'mm_label'      => Example: '1.2'        : Part label needed to reference a part in imap
    //! 'mm_name'       => Example: 'p-0-0-2' 	 : Our naming scheme (why not use mm_label ?)
    //! 'mm_type'       => Example: 'text/plain' : Mime type    
	//! We have also added subparts for RFC822 attachments ("mail in a mail")
	public $rootPart;

	//! Flat list of references to parts in $rootPart (built by us)
	//! Much simpler to use than the recursive structure in $rootPart
	//! This includes subparts for RFC822 attachments "mail in a mail"
	public $partsList;

	//! MimeMessages created from RFC822 attachments of this MimeMessage only
	public $subMessages=[];

	//! Only used for rawMessage 
	public $fnameForFakeImap=false;

	function __construct($rawMessage=false,$imap=false,$uid=false,$rfc822Depth=0)
	{
		require_once 'mail-import.php';

		// Open a fake imap connection to rawMessage (if requested)
		if($imap===false)
		{
			list($this->imap,$this->fnameForFakeImap)=mail_import_imap_open_raw($rawMessage);
			$this->uid=1;
		}
		else
		{
			$this->imap=$imap;
			$this->uid=$uid;
		}
		
		// Fetch the actual tree structure of parts using imap.
		// This structure is fairly difficult to use. 
		$this->rootPart=imap_fetchstructure($this->imap,$this->uid,FT_UID);
		mail_import_imap_errors($this->rootPart!==false);

		// Recursively add labels used by imap (Ex: "1.2")
		$this->addLabelToPart($this->rootPart);

		// Special case: recursively parse all RFC822 attachments ("email inside an email")
		if($rfc822Depth>10)
		{
			throw new Exception("MimeMessage: Failed decoding, recursion problem.");
		}
		$this->extractRfc822Attachments($this->rootPart,$rfc822Depth);

		// Normal case : recursively build a flat list of parts
		// This is only needed for $rfc822Depth===0 because buildPartsList will 
		// recursivelly descend into rfc822 attachments that were added by extractRfc822Attachments()
		if($rfc822Depth===0)
		{
			$this->partsList=[];
			$this->buildPartsList($this->rootPart,$this->partsList);
		}
	}

	//! Automatically called when refcount reaches 0
	function __destruct()
	{
		$this->cleanup();
	}

	//! Close imap and delete cricular references
	//! Close imap connections is only needed for MimeMessage that are created from rawData (which create a tmp fake imap connection).
	//! As of 2016/08 fake imap is only used for rfc822 attachments.
	function cleanup()
	{
		// Cleanup submessages
		foreach($this->subMessages as $subMessage){$subMessage->cleanup();}
		// Help garbage collection by removing circular ref
		if(is_array($this->partsList))
		{
			foreach($this->partsList as $name=>$part)
			{
				if(is_object($part)){$part->mm_message=null;}
			}
		}

		// Close imap and delete file
		if($this->imap===false){return;}
		if($this->fnameForFakeImap===false){return;}
		imap_close($this->imap);
		mail_import_imap_errors();
		if(file_exists($this->fnameForFakeImap)){unlink($this->fnameForFakeImap);}
		$this->imap=false;
		$this->fnameForFakeImap=false;
	}

	//! Returns a simplified version of $this->partList with only the important information in a easy to use format.
	//! This is used in Message::$parts
	public function simplifiedPartList()
	{
		$res=[];
		foreach($this->partsList as $name=>$part)
		{
			$children=[];
			foreach(oval($part,'parts',[]) as $subPart){$children[]=$subPart->mm_name;}

			// get attachment filename if it exists
			$fname=                    self::partParameter($part,'FILENAME');
			if(!strlen($fname)){$fname=self::partParameter($part,'NAME'    );}

			$res[$name]=
				['name'=>$part->mm_name,
				 'type'=>$part->mm_type,
				 // The size of the encoded part (a bit larger thatn the decoded size). 
				 // Value present and reliable for non multipart/ parts.
				 'size'=>oval($part,'bytes'),
				 'children'=>$children,
				];
			if(strlen($fname)){$res[$name]['filename']=$fname;}
		}
		return $res;
	}

	//! Returns the mime type of a part object (ex: entry in partsList)
	//! Ordinary mime types (text/plain, text/html, ...) are read from mail.
	//! Other mime types (images, attachments...) are determined by actually checking the parts contents with finfo()
	//! Example result : "text/html" or "application/pdf"
	function getPartMimeType(stdClass $part)
	{
		$mainType=self::$types[$part->type];
		$mimeType=$mainType."/".strtolower($part->subtype);

		$mimeOk=false;
		// Some types are directly accepted.
		// All other types are potentially wrong. 
		if($mainType=='multipart'            ||
		   $mainType=='message'              ||
		   $mimeType=='text/rfc822-headers'    ){$mimeOk=true;}

		// Sometimes big attachments (ex: pdf) are mislabeled as text/plain
		// So double-check any very large text (html with data: images can also be very large)
		$isTextorHtml=
			$mimeType=='text/plain'         ||
			$mimeType=='text/html'          ||
			$mimeType=='text/x-vcard'       ||
			$mimeType=='text/richtext'      ||
			$mimeType=='text/enriched'        ;

		if($isTextorHtml && (!isset($part->bytes) || $part->bytes<50000)){$mimeOk=true;}

		if(!$mimeOk)
		{
			// Determine mime type by looking at the actual attachment contents
			$contents=$this->getRawPartContents($part);
			$finfo=finfo_open(FILEINFO_MIME_TYPE);
			$finfoType=finfo_buffer($finfo,$contents);
			finfo_close($finfo);
			// Special case: strange bug :: finfo_buffer finds octet-stream and finfo_file finds correct (word) ???
			if($finfoType==='application/octet-stream')
			{
				global $mail_import_config;
				$tmp=tempnam($mail_import_config['tmp_dir'], "mime-message-attachment-type-check-");
				file_put_contents($tmp,$contents);
				require_once 'dlib/mail-and-mime.php';
				$finfoType=dlib_finfo($tmp);
				unlink($tmp);
			}
			// if finfo finds text, it is better to trust the original $mimeType. Otherwise use finfo result.
			if($finfoType!=='text/plain' || !$isTextorHtml){$mimeType=$finfoType;}
		}

		return $mimeType;
	}

	//! Charsets reported in mail are sometimes invalid. 
	static function fixPartCharset($charset)
	{
		// invalid charsets
		if($charset==='missing_parameter_value' || $charset==='charset' || $charset==='X-UNKNOWN'  || $charset===''){$charset=false;}
		// HACK: US-ASCII is actually sometimes iso-8859-1
		if($charset==='US-ASCII'){$charset='iso-8859-1';}
		// alias https://github.com/ashtuchkin/iconv-lite/issues/94
		if($charset==='unicode-1-1-utf-7'){$charset='utf-7';}
		// alias
		if($charset==='iso-latin1'){$charset='iso-8859-1';}
		if($charset==='CP-850'    ){$charset='iso-8859-1';}
		if($charset==='cp-850'    ){$charset='iso-8859-1';}
		// HACK: empty charset is actually sometimes iso-8859-1
		if($charset===false){$charset='iso-8859-1';}
		return $charset;
	}

	//! Each imap part can have parameters: like CHARSET or FILENAME
	//! $part->parameters  => extra options after Content-type (generally "CHARSET" or "NAME")
	//! $part->dparameters => extra options after Content-disposition (generally "FILENAME")
	static function partParameter(stdClass $part,$attrName,$checkParam=true,$checkDParam=true)
	{
		require_once 'mail-import.php';
		$attrName=strtolower($attrName);
		$check=[];
		if($checkParam ){$check[]='parameters' ;}
		if($checkDParam){$check[]='dparameters';}
		foreach($check as $param)
		{
			if(!isset($part->$param)){continue;}
			foreach($part->$param as $pair)
			{
				$attribute=strtolower($pair->attribute);
				if($attribute===$attrName)
				{
					require_once 'dlib/mail-and-mime.php';
					return dlib_mime_header_decode($pair->value);
				}
				//! Note: some mailers split long parameters (filenames) into several blocks
				if($attribute===$attrName.'*')
				{
					$pieces=explode("''",$pair->value);
					if(count($pieces)!==2){continue;}
					$fixed=urldecode($pieces[1]);
					$fixed=@iconv($pieces[0], "UTF-8//IGNORE", $fixed);
					if($fixed!==false){return $fixed;}
				}
			}
		}
		return false;
	}

	//! Returns the raw, unmodified contents of a message part (attachment)
	public function getRawPartContents(stdClass $part)
	{
		$res=imap_fetchbody($part->mm_message->imap,$part->mm_message->uid,$part->mm_label,FT_UID);
		mail_import_imap_errors($res!==false);
		$res=self::decodeBodyPartContents($part->encoding,$res);
		return $res;
	}

	//! Returns the contents of a mail part (attachment). 
	//! Text and html attachments are converted to UTF-8 and crossreferences are fixed to use our names (instead of imap labels).
	//! Non-text / non-html attachments are returned raw.
	public function getFixedPartContents($partName)
	{
		$part=val($this->partsList,$partName);
		if($part===false){return false;}
		$res=$this->getRawPartContents($part);

		// change html crossref ids (used for html images) to simpler partnames
		if($part->mm_type==='text/html')
		{
			foreach($this->partsList as $otherPart)
			{
				if(isset($otherPart->id) && $otherPart->id!='')
				{
					$oid=substr($otherPart->id,1,strlen($otherPart->id)-2);
					$res=str_replace('cid:'.$oid,
									 'mime_message_crossref:'.$otherPart->mm_name.':',
									 $res);
				}
			}
		}

		// convert charsets and entities to UTF-8 for text and html files
		$charset=self::partParameter($part,'CHARSET',true,false);
		$charset=self::fixPartCharset($charset);
		switch($part->mm_type)
		{
		case 'text/plain':
		case 'text/enriched': 
			// IGNORE should silence PHP warnings, but we still get some (very very rare). Check again. We add "@" ("@iconv")
			$res=@iconv($charset, "UTF-8//IGNORE", $res);
			$res=dlib_cleanup_plain_text($res);
			break;
		case 'text/html':
			require_once 'dlib/html-tools.php';
			$res=dlib_html_convert_page_to_utf8($res,'charset='.$charset);
			$res=dlib_html_entities_decode_text($res);
			break;
		}
		return $res;
	}

	//! RFC822 attachments are "email inside an email", so we have to parse them recursively.
	private function extractRfc822Attachments(stdClass &$part,$rfc822Depth)
	{
		// We found one: parse it
		if($part->type===TYPEMESSAGE && $part->subtype==='RFC822')
		{
			$this->extractSingleRfc822Attachment($part,$rfc822Depth);
			return;
		}

		// Recursivelly look for others in our sub-parts
		if(!isset($part->parts)){return;}
		foreach($part->parts as $subPart)
		{
			$this->extractRfc822Attachments($subPart,$rfc822Depth);
		}
	}

	//! Parse a single RFC822 attachment ("email inside an email")
	private function extractSingleRfc822Attachment(stdClass &$part,$rfc822Depth)
	{
		// Create a new MimeMessage object by using raw mail data attachment
		$raw=imap_fetchbody($this->imap,$this->uid,$part->mm_label,FT_UID );
		mail_import_imap_errors($raw!==false);
		$content="From "."MAILER-DAEMON"." ".date('D M d H:i:s Y')."\r\n".$raw;
		$subMessage=new MimeMessage($content,false,false,$rfc822Depth+1);

		// This part uses a different MimeMessage (different imap connection)
		$part->mm_mimemessage=$subMessage;
		$this->subMessages[]=$subMessage;
		// add the new mail's rootPart to the parts tree
		$part->parts=[$subMessage->rootPart];
	}

	//! Recursivelly adds mm_label to all parts described in the message structure.
	//! Label is needed to reference a part with imap.
	private function addLabelToPart(stdClass &$part,$level=0,$pos=1,$parentLabel="")
	{
		$part->mm_message=$this;
		if($level>0)
		{
			$part->mm_label=($parentLabel!="" ? $parentLabel."." : '').$pos;
		}
		else
		{
			$part->mm_label="";
			if(!isset($part->parts)){$part->mm_label="1";}
		}

		if(!isset($part->parts)){return;}

		$childPos=1;
		foreach($part->parts as $key=>$subPart)
		{
			$this->addLabelToPart($subPart,$level+1,$childPos,$part->mm_label);
			$childPos++;
		}
	}

	//! Recursively build a flat list of parts (attachments) associated to this message
	private function buildPartsList(stdClass &$part,&$res,$parent='',$pos=0)
	{
		$name=($parent!=='' ? $parent.'-' : 'p-').$pos;
		$res[$name]=&$part;
		$part->mm_name=$name;
		$part->mm_type=$this->getPartMimeType($part);

		if(!isset($part->parts)){return;}
		$childPos=0;
		foreach($part->parts as &$subPart)
		{
			$this->buildPartsList($subPart,$res,$name,$childPos);
			$childPos++;
		}	
	}

	public static function decodeBodyPartContents($encoding,$string)
	{
		require_once 'mail-import.php';
		$res=$string;
		switch(self::$encodings[$encoding] ?? 'UNKNOWN')
		{
		case '7BIT':// consider it is ok
		case '8BIT':
		case 'BINARY':
			break;
		case 'BASE64':
			$res=imap_base64($string);
			mail_import_imap_errors();
			break;
		case 'QUOTED-PRINTABLE':
			$res=quoted_printable_decode($string);
			break;
		case 'OTHER':
		default:
			dlib_message_add('MimeMessage::decodeBodyPartContents: decode error :'.ent($encoding),'warning');
			break;
		}
		return $res;
	}

	//! Returns a list of part labels that should be ignored
	//! because they are not the last of a list of multipart/alternatives.
	//! The algorithm (RFC) is quite simple: 
	//! ignore all but the last supported type in multipart alternatives.
	//! This is intended to be a public helper function. (that's why it'is static).
	//! @param an array of part descriptions such as the one returned by simplifiedPartList()
	//! @param an optional array of types that the client supports
	public static function multipartAltIgnore(array $parts,$supported=false)
	{
		$res=[];
		foreach($parts as $n=>$part)
		{
			$mimeType=$part['type'];
			if($mimeType!=='multipart/alternative'){continue;}
			//echo "part $n : $mimeType\n";
			$last=false;
			$found=false;
			foreach($part['children'] as $subPart)
			{
				$subType=$parts[$subPart]['type'];
				if($supported!==false && 
				   array_search($subType,$supported)===false){$found=$last;}
				$res[$subPart]=$subPart;
				$last=$subPart;
			}
			// don't ignore this one
			unset($res[$found===false ? $last : $found]);
		}
		return $res;
	}

}


/** @} */

?>