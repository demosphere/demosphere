<?php

error_reporting(E_STRICT | E_ALL);

// launch command line test if called from command line
if(!isset($_SERVER['SERVER_NAME']) && isset($argv) &&
   basename($argv[0])===basename(__FILE__))
 {mime_parser_command_line_test();return;}


require_once 'mail-import.php';
require_once 'dlib/testing.php';
require_once 'FIXME/local/src/mimeparser-2011-05-05/mime_parser.php';

function mime_parser_command_line_test()
{
	setlocale(LC_TIME, "fr_FR.utf8");
    ini_set("include_path", ini_get("include_path").PATH_SEPARATOR."..".PATH_SEPARATOR."../..");
	require_once 'dlib/tools.php';
	require_once 'dlib/testing.php';
	require_once 'FIXME/local/src/mimeparser-2011-05-05/mime_parser.php';
	require_once 'FIXME/local/src/mimeparser-2011-05-05/rfc822_addresses.php';

	mime_parser_test_basic('test');
}
function mime_parser_web_test()
{
	mime_parser_test_basic('demosphere/mail-import/test');
}

function mime_parser_test_basic($testDir)
{
	//if(file_exists($testDir.'/source-mail-ubknownmimetype-problem'))
	//{
	// 	$mm=new MimeMessage(file_get_contents($testDir.'/source-mail-ubknownmimetype-problem'));
	// 	$list=$mm->getPartList();
	// 	//var_dump($list);
	//}
	//fatal('iii');
 	global $db_params;
 	$db_params=['host'=>'localhost','user'=>'demosphere','database'=>'demosphere_non_drupal','password'=>'FIXME'];
	db_setup();
	global $db_connection;
	$result = mysqli_query($db_connection,"SET NAMES 'utf8mb4';") or fatal('Query failed: ' . mysqli_error($db_connection));

	$parser=new mime_parser_class;
	$scanRes=scandir($testDir.'/test-raw');
	foreach($scanRes as $nb=>$dirEntry)
	{
		//$dirEntry='rawmail-id42100';
		//$dirEntry='rawmail-id42263';
		//$dirEntry='rawmail-id42126';
		if(strpos($dirEntry,'rawmail-id')!==0){continue;}
		$id=intval(str_replace('rawmail-id','',$dirEntry));
		$dMessage=Message::fetch($id);

		$raw=file_get_contents($testDir.'/test-raw/'.$dirEntry);
		echo "\n=========================== $dirEntry (".$nb.'/'.count($scanRes).' : '.strlen($raw).")\n";
		if(strlen($raw)===0){echo "skipping empty mail\n";continue;}
		$raw=substr($raw,strpos($raw,"\n")+1);
		//file_put_contents('/tmp/x',$raw);
		$param=['Data'=>$raw];
		if(!$parser->Decode($param,$decoded))
		{
			var_dump($parser->error);
			var_dump($parser->error_position);
			fatal("aborting error\n");
		}
		
		$decoded=$decoded[0];

		$analyzed=[];
		$parser->Analyze($decoded,$analyzed);

		//print_r($analyzed);
		//print_r($analyzed['Subject']);
		$subject=$analyzed['Subject'];
		if(isset($analyzed['SubjectEncoding'])){$subject=iconv($analyzed['SubjectEncoding'],'utf-8',$analyzed['Subject']);}
		//$dsubject=iconv('ISO-8859-1','utf-8',$dMessage->getSubject());
		$dsubject=$dMessage->getSubject();
		if($subject===$dsubject){echo "ok\n";}
		else
		{
			echo " subject::".$subject."\n";
			echo "dsubject::".$dsubject."\n";
		}
		
		continue;

		unset($decoded['Body']);
		print_r($decoded);
		//print_r($decoded['DecodedHeaders']);
		//print_r($decoded['Headers']);

		

		if(isset($decoded['DecodedHeaders']['subject:']))
		{
			if(count($decoded['DecodedHeaders']['subject:'])!=1)
			{
				print_r($decoded['DecodedHeaders']['subject:']);
				fatal("several subjects in DecodedHeaders:strange! aborting\n");
			}
			$header='';
			foreach($decoded['DecodedHeaders']['subject:'][0] as $headerPart)
			{
				$header.=iconv($headerPart['Encoding'],'utf-8',$headerPart['Value']);
			}
			echo "dsubject: $header\n";
		}
		else
		{
			echo " subject:".$decoded['Headers']['subject:']."\n";
		}

		//var_dump(count($decoded['Parts']));
		//echo $raw;
		fatal("\n");
	}	
	if(file_exists($testDir.'/tmp'))
	{
		$mm=new MimeMessage(file_get_contents($testDir.'/tmp'));
		$list=$mm->getPartList();
		//var_dump(array_keys($list));
		$contents=$mm->getPartContents($list['p-0-1']);
		//echo $contents;
	}

}

?>