<?php

require_once 'demosphere/test/test-demosphere.php';

require_once 'demosphere/mail-import/mail-import.php';
require_once 'dlib/testing.php';

function test_mime_message()
{
	//mime_message_test_basic('test');
	//mime_message_test_basic('demosphere/mail-import/test');
}

function mime_message_test_basic($testDir)
{
	//if(file_exists($testDir.'/source-mail-ubknownmimetype-problem'))
	//{
	// 	$mm=new MimeMessage(file_get_contents($testDir.'/source-mail-ubknownmimetype-problem'));
	// 	$list=$mm->getPartList();
	// 	//var_dump($list);
	//}
	//fatal('iii');

	if(file_exists($testDir.'/tmp'))
	{
		$mm=new MimeMessage(file_get_contents($testDir.'/tmp'));
		$list=$mm->getPartList();
		//var_dump(array_keys($list));
		$contents=$mm->getPartContents($list['p-0-1']);
		//echo $contents;
	}

	$mm=new MimeMessage(file_get_contents($testDir.'/testmail-1'));
	$list=$mm->getPartList();
	//var_dump($list);
	$names=array_keys($list);
	test_equals(count($list),1);
	test_equals($list[$names[0]]->mm_type,'text/plain');
	test_equals(strlen($mm->getPartContents($list[$names[0]])),35);
	test_equals($mm->getSubject(),'test 1 - simple text');
	test_equals($mm->getFrom   (),'sender@example.org');
	test_equals($mm->getTo     (),'receiver@demosphere.net');
	test_equals($mm->getDate   (),1265904301);


	$mm=new MimeMessage(file_get_contents($testDir.'/testmail-2'));
	$list=$mm->getPartList();
	$names=array_keys($list);
	test_equals(count($list),3);
	test_equals($list[$names[1]]->mm_type,'text/plain');
	test_equals($list[$names[2]]->mm_type,'image/png');
	$contents=$mm->getPartContents($list[$names[2]]);
	test_equals(md5($contents),'af9ec1bde75727a5f7b68c8d4ec5bb9a');

	$mm=new MimeMessage(file_get_contents($testDir.'/testmail-3'));
	$list=$mm->getPartList();
	$names=array_keys($list);
	test_equals($list[$names[1]]->mm_type,'text/plain');
	test_equals($list[$names[2]]->mm_type,'image/png');
	test_equals($list[$names[3]]->mm_type,'image/png');
	$contents=$mm->getPartContents($list[$names[2]]);
	test_equals(md5($contents),'af9ec1bde75727a5f7b68c8d4ec5bb9a');
	$contents=$mm->getPartContents($list[$names[3]]);
	test_equals(md5($contents),'af9ec1bde75727a5f7b68c8d4ec5bb9a');

	$mm=new MimeMessage(file_get_contents($testDir.'/testmail-4'));
	$list=$mm->getPartList();
	$names=array_keys($list);
	test_equals(count($list),3);
	test_equals($list[$names[1]]->mm_type,'text/plain');
	test_equals($list[$names[2]]->mm_type,'image/png');
	$contents=$mm->getPartContents($list[$names[2]]);
	test_equals(md5($contents),'af9ec1bde75727a5f7b68c8d4ec5bb9a');

	$mm=new MimeMessage(file_get_contents($testDir.'/testmail-5'));
	$list=$mm->getPartList();
	$names=array_keys($list);
	test_equals(count($list),7);
	test_equals($list['p-0-0-0-0']->mm_type,'text/plain');
	test_equals($list['p-0-0-0-1']->mm_type,'text/html');
	test_equals($list['p-0-0-1'  ]->mm_type,'image/gif');
	test_equals($list['p-0-1'    ]->mm_type,'image/png');
	$contents=$mm->getPartContents($list['p-0-1']);
	test_equals(md5($contents),'af9ec1bde75727a5f7b68c8d4ec5bb9a');
	$html=$mm->getPartContents($list['p-0-0-0-1']);
	test_contains($html,'mime_message_crossref:p-0-0-1:');
	$ignore=MimeMessage::multipartAltIgnore($mm->getPartDescriptions());
	test_equals($ignore,['p-0-0-0-0'=>'p-0-0-0-0']);
	$ignore=MimeMessage::multipartAltIgnore($mm->getPartDescriptions(),['text/plain']);
	test_equals($ignore,['p-0-0-0-1'=>'p-0-0-0-1']);

	//foreach($list as $name=>$part){echo "zzname:$name type:".$part->mm_type."\n";}

	if(file_exists($testDir.'/source-mail-rfc822'))
	{
		$mm=new MimeMessage(file_get_contents($testDir.'/source-mail-rfc822'));
		$list=$mm->getPartList();
		test_contains($mm->getPartContents($list['p-0-1-0']),'Environ 300 étudiants se');
	}

	if(file_exists($testDir.'/source-mail-rfc822-problem'))
	{
		$mm=new MimeMessage(file_get_contents($testDir.'/source-mail-rfc822-problem'));
		$list=$mm->getPartList();
		test_equals($list['p-0-1-0-1-0-0-1']->mm_type,'image/jpeg');
		test_contains($mm->getPartContents($list['p-0-1-0-1-0-0-0-0']),'LES ETUDES ET THÉORIES POSTCOLONIALES');
	}
}

?>