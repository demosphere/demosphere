<h1 class="mail-import-top-title">
	<?= t('Mail rules for mail !nb',['!nb'=>'<a href="'.ent($message->url()).'">'.$message->id.'</a>']) ?>
</h1>
<ul>
	<li><strong>_(Current importance:)</strong> <?: t($message->importance) ?></li>
	<li><strong>_(Current source:)    </strong> "$message->getSourceOrDefault()"</li>
</ul>
<p>_(This email currently matches the following rules):</p>
<div id="message-rule-manager-rules">
	<?= $mailRuleList ?>
</div>
<p></p>

<hr/>

<h2>_(Add a rule:)</h2>

<form id="manageRules" method="post" action="">
	<?= dlib_add_form_token('Message::manageRules') ?>
	<ul id="simple-rules-list">		
		<? foreach(['importance','source'] as $actionType){ ?>
			<? foreach(MailRule::$matchType_['values'] as $matchType){ ?>
				<? if($matchType==='from-undisclosed' && $message->from  !=''                 ){continue;} ?>
				<? if($matchType==='list-id'          && $message->listId==''                 ){continue;} ?>
				<li>
					<input type="radio" name="simple-rule" value="$matchType/$actionType"/>
					<?= MailRule::description($matchType,
											  '<input type="'.($matchType==='list-id' ? 'hidden' : 'text').'" '.
													 'name="match-string-'.$matchType.'-'.$actionType.'" '.
													 'value="'.ent(MailRule::suggestMatchStringFromMessage($message,$matchType)).'"/>',
											  $actionType,
											  ($actionType==='importance' ?
											      '<select name="'.$matchType.'-importance">'.
												      '<option>direct</option>'.
													  '<option>ordinary</option>'.
													  '<option>trash</option>'.
												  '</select>'  :
												  '<input type="text" name="'.$matchType.'-source" '.
														 'value="'.t('mail received on !date',['!date'=>'$date']).'"/>'
											  ),
											  false) ?>
			<?}?>
		<?}?>
	</ul>

	<input type="submit" value="_(save)"/>
</form>
<hr/>
<h1>Details on this email</h1>
<?= mail_import_view_message_details($message->id) ?>
