<?php

//! Main page for carpools to a specific event. Displays the destination and a list of carpools.
function demosphere_carpool($eventId)
{
	require_once 'demosphere-place.php';
	global $currentPage,$demosphere_config,$user;

	$currentPage->title=t('Share a ride');
	$currentPage->bodyClasses[]="notFullWidth";

	$event=Event::fetch($eventId);

	if(!$event->carpoolIsEnabled() && !$user->checkRoles('admin','moderator'))
	{
		return t('Sorry: share-a-ride is not available for this event.');
	}

	$place=$event->usePlace();
	require_once 'demosphere-event-map.php';
	$mapUrl=demosphere_get_map_url($place);
	$hasMapImage=$place->zoom!=0;
	$mapImage=false;
	if($hasMapImage)
	{
		$mapImage=demosphere_event_get_map_image($place->latitude,
												 $place->longitude,
												 $place->zoom);
	}

	$carpools=Carpool::fetchList('WHERE eventId=%d ORDER BY id DESC',$eventId);

	require_once 'demosphere-htmlview.php';
	require_once 'demosphere-date-time.php';
	require_once 'demosphere-misc.php';
	$currentPage->addJs("function unobfuscate(el){el.href='mailto:'+el.innerHTML.replace(/<span [^>]*none[^>]*>[^>]*>/g,'').replace(/<\/?span[^>]*>/g,'').replace(/\s/g,'');}",'inline');
	$currentPage->addCssTpl('demosphere/css/demosphere-carpool.tpl.css');
	$currentPage->addCssTpl('demosphere/css/demosphere-big-button.tpl.css');
	return template_render('templates/demosphere-carpool.tpl.php',
						   [compact('event','place','hasMapImage','mapImage','mapUrl','carpools')]);
}

//! Form to create or edit a carppol (driver or passenger).
function demosphere_carpool_form($id=false)
{
	global $demosphere_config,$currentPage,$user;
	require_once 'dlib/form.php';
	require_once 'demosphere-date-time.php';

	if($id===false)
	{
		// this is a new Carpool
		if(!isset($_GET['eventId' ])){dlib_bad_request_400("Missing event for new Carpool");}
		if(!isset($_GET['isDriver'])){dlib_bad_request_400("Missing driver for new Carpool");}
		$eventId=intval($_GET['eventId']);
		$event=Event::fetch($eventId,false);
		if($event===null){dlib_not_found_404();}
		if(!$event->access('view')){dlib_permission_denied_403();}
		$isDriver=(bool)intval($_GET['isDriver']);
		$carpool=new Carpool();
		$carpool->isDriver=$isDriver;
		$carpool->eventId=$eventId;
		$carpool->userId=$user->id;

		// Fill name and contact for identified user
		if($user->id!=0)
		{
			$carpool->name=$user->login;
			$carpool->contact=$user->email;
		}
	}
	else           
	{
		// User wants to edit an existing Carpool.
		$carpool=Carpool::fetch($id,false);
		if($carpool===null){dlib_not_found_404();}
		$event=$carpool->useEvent();
		if(!$event->access('view')){dlib_permission_denied_403();}
		
		// Check edit access rights on this event
		if(!$carpool->canEdit($user)){dlib_permission_denied_403();}
		// Allow this user to re-edit later, without reclicking on auth self edit link.
		$_SESSION['carpools'][]=$carpool->id;
	}

	if(!$event->carpoolIsEnabled())
	{
		return t('Sorry: share-a-ride is not available for this event.');
	}

	$currentPage->title=$carpool->isDriver ? 
		t('Driver form') :
		t('Passenger form') ;

	list($objForm,$formOpts)=form_dbobject($carpool);

	$form['top']=['html'=>'<p id="top-message" class="'.($carpool->isDriver ? 'driver' : 'passenger').'">'.
				  ($carpool->isDriver ?
				   t('You are volunteering to drive to the !link event scheduled @date</a>',
					 ['@date'=>demos_format_date('day-month-abbrev',$event->startTime),
					  '!link'=>'<a href="'.$event->url().'">',]) :
				   t('You are requesting a ride for the !link event scheduled @date</a>',
					 ['@date'=>demos_format_date('day-month-abbrev',$event->startTime),
					  '!link'=>'<a href="'.$event->url().'">',]) ).
				  '</p>',
				  ];

	$form['isDriver']=$objForm['isDriver'];
	$form['isDriver']['wrapper-attributes']['class'][]='hidden';

	$form['name']=$objForm['name'];
	$form['name']['title']=t('Your name');
	$form['name']['attributes']=['placeholder'=>t('Ex: Michael')];
	$form['name']['required']=true;

	$form['contact']=$objForm['contact'];
	$form['contact']['title']=t('Contact');
	$form['contact']['description']=t('Email address or phone number, ... If you enter an email address, you can choose to keep it hidden.');
	$form['contact']['attributes']=['placeholder'=>t('Ex: michael@example.org')];
	$form['contact']['prefix']='<div id="contact-and-hide-wrapper">';
	$form['contact']['required']=!$carpool->isDriver;
	$form['contact']['validate']=function($val)
		{
			if(strpos($val,'@')===false){return true;}
			if(form_validate_email($val,['check-dns'=>true])!==null)
			{
				return t('You entered an email address, but it does not seem to be valid.');
			}
		};

	$form['hideEmail']=$objForm['hideEmail'];
	$form['hideEmail']['title']=t('hide your email');
	$form['hideEmail']['suffix']='</div>';

	$form['from']=$objForm['from'];
	$form['from']['type']='textarea';
	$form['from']['title']=t('Leaving from');
	$form['from']['attributes']=['placeholder'=>t("Ex: North east Paris, Gamebta."),
								 'rows'=>3];
	$form['from']['required']=true;

	$form['comment']=$objForm['comment'];
	$form['comment']['type']='textarea';
	$form['comment']['title']=t('Comment');
	$form['comment']['attributes']=['rows'=>3];

	// Time : passenger do not get to enter time
	if($carpool->isDriver)
	{
		$form['time']=$objForm['time'];
		$form['time']['type']='hidden';

		$hours=[24=>t('(hour)')];
		$refHour=date('G',$event->startTime);
		for($i=0;$i<24;$i++){$h=(24+$refHour-$i)%24;$hours[$h]=sprintf('%02d',$h);}
		$form['hour']=['type'=>'select',
					   'title'=>t('Leaving at'),
					   'options'=>$hours,
					   'default-value'=>$carpool->time ? date('G',$carpool->time) : 24,
					   'prefix'=>'<div id="time-wrapper">',
					   'description'=>t('(The event is scheduled for @date.)',
										['@date'=>demos_format_date('full-date-time',$event->startTime)]) ,
					   'validate'=>function($val)use($carpool)
				{
					return ($carpool->isDriver && $val==24) ? t('You must say when you are leaving') : true;
				}
					   ];

		$minutes=[60=>t('(minutes)')];
		for($i=0;$i<60;$i+=5){$minutes[$i]=sprintf('%02d',$i);}
		$form['minute']=['type'=>'select',
						 'options'=>$minutes,
						 'default-value'=>$carpool->time ? date('i',$carpool->time) : 60,
						 'validate'=>function($val)use($carpool)
				{
					return ($carpool->isDriver && $val==60) ? t('You must say when you are leaving') : true;
				}
						 ];

		//$days=[t('same day as the event'),t('the day before the event'),t('2 days before'),t('3 days before')];
		$days=[];
		for($i=0;$i<4;$i++){$days[$i]=demos_format_date('day-month-abbrev',strtotime($i.' days ago',$event->startTime));}
		$form['day']=['type'=>'select',
					  'options'=>$days,
					  'default-value'=>$carpool->time ? 
					  array_search(demos_format_date('day-month-abbrev',$carpool->time),$days) : 0,
					  'suffix'=>'</div>',
					  ];
	}

	$form['isRoundTrip']=$objForm['isRoundTrip'];
	unset($form['isRoundTrip']['title']);
	$form['isRoundTrip']['type']='radios';
	$form['isRoundTrip']['options']=[0=>t('One way'),1=>t('Round trip')];

	if(!$carpool->id){$form['spamcheck']=['type'=>'spamcheck'];}

	$form['save']=$objForm['save'];
	$form['save']['attributes']['class'][]='big-button';
	$form['save']['attributes']['class'][]='big-button-green';
	$form['save']['redirect']=$event->url().'/carpool';
	$form['save']['submit-3']=function (&$items) use ($event)
		{
			if(!isset($items['time'])){return;}
			$h=$items['hour']['value'];
			$m=$items['minute']['value'];
			$d=$items['day']['value'];
			if($h==24 || $m==24){$ts=0;}
			else
			{
				$str=$d.' day ago '.sprintf('%02d:%02d',$h,$m);
				$ts=strtotime($str,$event->startTime);
			}
			$items['time']['value']=$ts;
		};
	$form['save']['submit']=function()use($carpool)
		{
			global $base_url,$demosphere_config,$user;
			$auth=substr(hash('sha256',$carpool->id.':carpool:'.$demosphere_config['secret']),0,10);
			$link=$base_url.'/carpool/'.$carpool->id.'/edit?auth='.$auth;

			dlib_message_add($carpool->isDriver ? 
							  t('Your proposal was succesfully added.') :
							 t('Your request was succesfully added.') );

			// Give user a link so that he can edit his carpool later.
			// If the given contact is a mail, sen the link by mail (only once).
			if($user->id==0)
			{
				if(strpos($carpool->contact,'@')!==false)
				{
					if($carpool->editLinkWasSentTo!==$carpool->contact)
					{
						global $demosphere_config;
						$carpool->editLinkWasSentTo=$carpool->contact;
						$carpool->save();
						require_once 'lib/class.phpmailer.php';
						$mail=new PHPMailer();
						$mail->CharSet="utf-8";
						$mail->SetFrom($demosphere_config['email_subscription_from']);// usually : NOREPLY@example.org
						$mail->AddAddress($carpool->contact);
						$mail->Subject=t('!site_name : share a ride',['!site_name'=>$demosphere_config['site_name']]);
						$mail->MsgHTML('<p>'.t('Thanks for using this "share-a-ride" service.').'</p>'.
									   '<p>'.t('Here is a link that will allow you to edit your information:').'<br />'.
									   '<a href="'.ent($link).'">'.ent($link).'</a></p>');
						if(!$mail->Send()) 
						{
							dlib_message_add(t("Error while sending message to @email.",['@email'=>$carpool->contact]),'error');
						}
						else
						{
							dlib_message_add(t('An email was sent to you with a link that will allow you to edit it later.'));
						}
					}
				}
				else
				{
					dlib_message_add(t('Please keep this link, so that you can change it later:').'<br/>'.
									 '<a href="'.ent($link).'">'.ent($link).'</a>');
				}
			}
			if($carpool->isDriver){dlib_message_add(t('Thanks for sharing this ride!'));}

			if(!$user->id){$_SESSION['carpools'][]=$carpool->id;}
		};

	
	if(isset($objForm['delete']))
	{
		$form['delete']=$objForm['delete'];
		$form['delete']['attributes']['class'][]='big-button';
		$form['delete']['attributes']['class'][]='big-button-red';
		$form['delete']['submit']=function(){dlib_message_add(t('Your ride was deleted.'));};
		$form['delete']['redirect']=$event->url().'/carpool';
	}
	else
	{
		$form['save']['suffix']='<a class="big-button big-button-blue" href="'.$event->url().'/carpool">'.t('Cancel').'</a>';
	}

	// Extra whole-form validtion 
	$formOpts['validate']=function(&$items)
		{
			// Contact is required only for passenger
			if(trim($items['contact']['value'])==='' &&  $items['isDriver']['value']===false)
			{
				$items['contact']['error']=ent(t('You must fill this field so that a driver can contact you.'));
			}

			// Cannot have hidden email if contact is not email
			if(strpos($items['contact']['value'],'@')===false &&  $items['hideEmail']['value']===true)
			{
				$items['contact']['error']='Strange: hide email but contact is not email';
			}
		};

	$currentPage->addJs('lib/jquery.js');
	$currentPage->addJs('demosphere/js/demosphere-carpool-form.js');
	$currentPage->addCssTpl('demosphere/css/demosphere-carpool.tpl.css');
	$currentPage->addCssTpl('demosphere/css/demosphere-big-button.tpl.css');
	return form_process($form,$formOpts);
}

//! A form so that other users can contact a carpool that hides his email address
function demosphere_carpool_contact($id=false)
{
	global $demosphere_config,$currentPage,$user;
	require_once 'dlib/form.php';
	require_once 'demosphere-date-time.php';

	$currentPage->title=t('Share a ride - contact');

	$carpool=Carpool::fetch($id,false);
	if($carpool===null){dlib_not_found_404();}
	$event=$carpool->useEvent();
	if(!$event->access('view')){dlib_permission_denied_403();}

	if(!$event->carpoolIsEnabled())
	{
		return t('Sorry: share-a-ride is not available for this event.');
	}
		
	$form['top']=['html'=>t('Send a message to @name to share a ride for the !link event scheduled @date</a>.',
							['@name'=>$carpool->name,
							 '!link'=>'<a href="'.$event->url().'">',
							 '@date'=>demos_format_date('day-month-abbrev',$event->startTime)]),
				  ];

	$form['contact']=['type'=>'textfield',
					  'title'=>t('How can @name reply to you?',['@name'=>$carpool->name]),
					  'required'=>true,
					  'description'=>t('You need to provide an email or a phone number so that @name can contact you.',['@name'=>$carpool->name]),
					  'default-value'=>$user->id!==0 ? $user->email : '',
					  ];

	$form['message']=['type'=>'textarea',
					  'required'=>true,
					  'title'=>t('Message'),
					  ];
	$form['submit']=['type'=>'submit',
					 'value'=>t('Send'),
					 'redirect'=>$event->url().'/carpool',
					 'submit'=>function($items)use($carpool)
			{
				global $demosphere_config,$base_url;
				require_once 'lib/class.phpmailer.php';
				$event=$carpool->useEvent();
				$senderContact=$items['contact']['value'];
				$message      =$items['message']['value'];
				$lang=dlib_get_locale_name();
				$html=template_render('templates/demosphere-carpool-contact.tpl.php',
									  compact('event','carpool','senderContact','message','lang'),
									  []);

				$text=$html;
				$text=str_replace('<h3>',"<h3>******* ",$text);
				$text=str_replace('</h3>',' *******</h3>',$text);
				$text=str_replace('<div id="disclaimer','<br/>===<div id="disclaimer',$text);
				$text=preg_replace('@<a [^>]*mailto[^>]*>([^<]*)</a>@','$1',$text);
				$text=Html2Text::convert($text);
				$text=preg_replace('@\[([^\]]*)\]\((http[^)]*)\)([^\n]*)@','$1$3'."\n".'$2',$text);
				$text=str_replace($base_url."\n",'',$text);

				//echo $html;echo '<hr/>';echo '<hr/>';echo '<pre>'.ent($text).'</pre>';die('pp');

				$mail=new PHPMailer();
				$mail->CharSet="utf-8";
				$mail->SetFrom($demosphere_config['email_subscription_from']);// usually : NOREPLY@example.org
				$mail->AddAddress($carpool->contact);
				$mail->Subject=t('!site_name : share a ride',['!site_name'=>$demosphere_config['site_name']]);
				$mail->MsgHTML($html);
				$mail->AltBody = $text;
				if(!$mail->Send()) 
				{
					dlib_message_add(t("Error while sending message to @name.",['@name'=>$carpool->name]),'error');
				}
				else
				{
					dlib_message_add(t("Your message has been sent to @name.",['@name'=>$carpool->name]));
				}
			},
					 
					 ];
	

	$currentPage->addCssTpl('demosphere/css/demosphere-carpool.tpl.css');
	$currentPage->addCssTpl('demosphere/css/demosphere-big-button.tpl.css');
	return form_process($form,['id'=>'carpool-contact-form']);
}

//! Admin page for carpools.
function demosphere_carpool_manage()
{
	global $currentPage,$demosphere_config;

	$ids=db_one_col("SELECT Carpool.id FROM Carpool,Event WHERE Carpool.eventId=Event.id AND Event.startTime > %d ORDER BY Event.startTime ASC",Event::today()-3600*24*5);
	$carpools=Carpool::fetchListFromIds($ids);
	require_once 'demosphere-date-time.php';
	$currentPage->addCssTpl('demosphere/css/demosphere-carpool.tpl.css');
	$currentPage->addCssTpl('dlib/table-tools.css');
	return template_render('templates/demosphere-carpool-manage.tpl.php',
						   [compact('event','place','hasMapImage','mapImage','mapUrl','carpools')]);

}
?>