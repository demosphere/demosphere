<h1 class="page-title">
	<?if($cuser->id==$user->id){?>
		_(My opinions)
	<?}else{?>
		<?: t('Opinions of:') ?> <a href="$base_url/user/$cuser->id">$cuser->login</a>
	<?}?>
</h1>
<?if($isEventAdmin){?>
	<p>
		_(Reputation:) <strong><?: round($userReputation*100) ?></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		_(Level:) <strong>$cuser->contributorLevel</strong></p>
	<p>
		<?: t('"!name" has expressed !nb opinions',
			['!name'=>$cuser->login,
			 '!nb'  =>$nbOpinions])?>
		<br/>
	</p>
<?}?>
<div id="charts">
	<?= $histogram ?>
	<?if($isEventAdmin && count($opinions)>4){?>
		<canvas id="reputation-chart" width="<?: min(600,count($opinions)*15) ?>" height="200"></canvas>
		<script>
		    var reputationChart = new Chart(document.getElementById("reputation-chart"),reputationChartData);
		</script>
	<?}?>
</div>
<?= render('demosphere-opinions-list.tpl.php') ?>
