<?= $form['form-start'] ?>
<h1 class="page-title">_(Confirm event repetitions)</h1>
<p id="confirm-top-message">
	<?if(count($events)){?>
		_(Check events that are confirmed or uncheck events that are canceled. In every case, you need to press the "Save" button.)
	<?}else{?>
		_(No future events available.)
	<?}?>
</p>
<? if($group->ruleEnd){?>
	<p class="rule-end">
		<?: t('This repetition ends on !date. No new events will be created after that date.',
			  ['!date'=>demos_format_date('short-day-month-year',$group->ruleEnd)])?>
		<?if(count($events)){?> 
			<br/>_(Make sure you check / uncheck events that are already on this list.)
		<?}?>
	</p>
<?}?>
<table>
<?foreach($events as $id=>$event){?>
	<tr class="<?: !isset($form['event-'.$id]) ? 'uncancelable' : '' ?>
			   " >
			<td class="confirm"><?= val($form,'event-'.$id,'✓') ?></td>
			<td class="dayofweek"><?:strftime('%a',$event->startTime)?></td>
			<?
				$day  ='<td class="date day  ">'.ent(strftime('%e',$event->startTime)).'</td>';
				$month='<td class="date month">'.ent(strftime('%b',$event->startTime)).'</td>';
				echo demosphere_date_time_swap_endian($day,$month,''); 
			?>
			<td class="time"><?: strftime('%R',$event->startTime)=='03:33' ?'': strftime('%R',$event->startTime)?></td>
			<td class="title">
				<? if($event->access('view')){?>
					<a href="$event->url()">
				<?}?>
					<?= $event->safeHtmlTitle()?>
				<? if($event->access('view')){?>
					</a>
				<?}?>
			</td>
			<td class="place"><?: $event->usePlace()->useCity()->getShort()?></td>

	</tr>
<?}?>
</table>

<? if(!$group->autoIsEnabled){?>
	<p>
		_(This repetition has been disabled. This generally happens after two emails are sent to you and you do not confirm or cancel any events. You can re-enable this repetition by hitting the "save" button below.)
	</p>
<?}?>

<div class="form-submit-buttons">
	<?= $form['submit'] ?>
	<?= $form['stop'] ?? '' ?>
</div>
<?= $form['form-end'] ?>

<? if($hasUncancelable){?>
<p id="uncancelable-message">
	<?: t('Published events that occur less than !uncancelableDelay days from now cannot be canceled. You should edit them and add "CANCELED" in the title and a message in the body explaining that it is canceled.',['!uncancelableDelay'=>round($uncancelableDelay/(24*3600))]) ?>
</p>
<?}?>
