<div id="opinions-event">
	<h2 id="opinions-title">
		<? if(!$isEventAdmin && $user->contributorLevel<=1){?> 
			_(Your opinion on this event)
		<?}else{?>
			_(Opinions on this event)
			<? if($eventRatingData['nbRatings']>1){?>
				<span id="event-rating">
					<span>_(Average:)
						<strong id="event-average">
							<?: round($eventRatingData['average'],2)?>
						</strong>
						<? if($isEventAdmin){?>
							= $eventRatingData['display']
						<?}?>
					</span>
        			<? if($demosphere_config['opinions_auto_publish']){?>
						<span id="event-rating-yes-rep">
							_(Publish:) <?: round(max(0,$eventRatingData['agreement']*100)) ?>%
						</span>
					<?}?>
				</span>
			<?}?>
		<?}?>
		<?if($isEventAdmin){?>
			<?= $disAllForm ?>
		<?}?>
	</h2>
	<? if($form===false){?>
		<p id="opinions-closed">
			<?if($event->status){?>
				_(This event has been published, opinions are now closed)
			<?}else if($event->getNeedsAttention()==='rejected-shown' ||
				       $event->getNeedsAttention()==='rejected'){?>
				_(This event was rejected, opinions are now closed)
			<?}else{?>
				_(Opinions are closed for this event)
			<?}?>
		</p>
	<?}?>
	<? if($form!==false && !$opinion->id){?>
		<div id="new-opinion-form" class="opinion">
			<?= $form ?>
		</div>
	<?}?>
	<?if(count($opinions)===0){?>
		<? if($form==false && 
			  ($isEventAdmin || $user->contributorLevel>1)){?> 
			_(no opinions)
		<?}?>
	<?}else{?>
		<? foreach($opinions as $op){	?>	
			<? $repData=$op->calcReputation(true); ?>
			<div id="opinion-$op->id" class="opinion <?: $op->isEnabled ? '' : 'disabled' ?> <?:$op->modReputation!=0 || $op->modComment!='' ? 'open-mod' : '' ?>">
				<? if(!$op->isEnabled){ ?>
					<p class="disabled-message">
						_(This opinion has been disabled.)
						<? if($form!==false && $opinion->id && $opinion->id==$op->id){?>
							_(If you save it again, it will be re-enabled.)
						<?}?>
					</p>
				<?}?>
				<? if($form!==false && $opinion->id && $opinion->id==$op->id){?>
					<?= $form ?>
				<?}else{?>
					<div class="opinion-text"        >
						<div class="opinion-rating" title="<?: val($ratingLabels,$op->rating)?>">
							<?: $op->rating>0 ? '+':''?><?: round($op->rating,1) ?>
						</div>
						<?if($isEventAdmin){?>
							<?= demosphere_opinion_display_reputation($op,$repData) ?>
						<?}?>

						<div class="opinion-information">
							<? $rep=demosphere_opinion_user_reputation($op->useUser());?>
								<?= t('!user !date at !time',[
								'!user'=>($isEventAdmin ? '<a href="'.ent($base_url).'/user/'.$op->userId.'/opinions">' : '').
										 ent(mb_substr($op->useUser()->login,0,25)).
										 ($isEventAdmin ? 	' ('.round($rep*100).')</a>' : '').
										 ($isEventAdmin || $op->userId==$user->id ? demosphere_opinion_user_stars($op->useUser()) : ''),
								'!date'=>demos_format_date('day-month-abbrev',$op->time),
								'!time'=>strftime('%H:%M',$op->time),]) ?>
							<?if($isEventAdmin){?>
								<?= $disForms[$op->id] ?>
							<?}?>
						</div>
						<?= preg_replace('@\n@','<br/>',ent($op->text)) ?>
						<? if($op->pubGuideline!==''){?>
							<div class="opinion-pubGuideline">
								<div class="opinion-pubGuideline-inner">_(Guidelines:) <?= preg_replace('@<br/>@','<span class="pg-more">[...]</span><br/>',preg_replace('@\n@','<br/>',ent($op->pubGuideline)),1) ?> </div>
							</div>
						<?}?>
						<?if($isEventAdmin){?>
							<span class="opinion-mod-open" onmousedown="
										var opinion=this.parentNode.parentNode;
										var open=opinion.className.indexOf(' open-mod')==-1;
										opinion.className= open ? 
												opinion.className+' open-mod' :
												opinion.className.replace(' open-mod','');
									   ">
								<span class="open-but">[+]</span>
								<span class="close-but">[-]</span>
							</span>
						<?}?>
					</div>
				<?}?>
				<?if($isEventAdmin){?>
					<div class="opinion-mod">
						<span class="opinion-id">id:<?: $op->id ?></span>
						<?= $modForms[$op->id] ?>
						<div class="opinion-reputation-details">
							<pre>$repData['display']</pre>
						</div>
					</div>
				<?}?>
			</div>
		<?}?>
	<?}?>
</div>
