<span id="mobile-menu-button"></span>
<nav id="mobile-menu" class="not-init">
<ul>
	<li id="mobile-menu-topics">
		<? foreach(Topic::getAll('WHERE showOnFrontpage=1') as $tid=>$topic){?>
			<? if($topic->icon===''){continue;}?>
			<a class="mobile-menu-topic" id="topic-$tid" href="/?selectTopic=$tid"></a>
		<?}?>
	    <?if($demosphere_config['topics_required']!=='mandatory'){?>
			<a class="mobile-menu-topic" id="topic-others" href="/?selectTopic=others"></a>
	    <?}?>
	</li>
	<li id="nearby-line">
		<span id="nearby">_(Nearby)</span>
		<span id="nearby-1km" >1km</span>
		<span id="nearby-3km" >3km</span>
		<span id="nearby-10km">10km</span>
	</li>
	<li id="search-line">	
		<form method="get" action="/search">
			<input id="search-field" type="search" placeholder="_(Search)" name="search"/><button id="search-submit"  type="submit">&nbsp;</button>
		</form>
	</li>
	<li><a class="mobile-menu-link" href="/publish">_(Submit an event)</a></li>
	<li><a class="mobile-menu-link" rel="nofollow" href="<?: dlib_url_add_get(dlib_current_url()) ?>switchMobile=nomobile">_(Wide screen site)</a></li>
	<?if($user->id!=0){?>
		<li><a class="mobile-menu-link" rel="nofollow" href="/user/$user->id">_(My profile)</a></li>
	<?}?>
	<?if($user->id==0){?>
		<li><a class="mobile-menu-link" rel="nofollow" href="/login">_(Login)</a></li>
	<?}else{?>
		<li><a class="mobile-menu-link" rel="nofollow" href="/logout">_(Logout)</a></li>
	<?}?>
</ul>
</nav>