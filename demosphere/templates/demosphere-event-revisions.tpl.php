<h1 class="page-title"><?: t('Old revisions for event !eid',['!eid'=>$event->id]) ?></h1>
<p><?: demos_format_date('short-date-time',$event->startTime) ?> : $event->title</p>
<table id="revisions">
<tr>
	<th></th>
	<th></th>
	<th></th>
	<th>_(Date)</th>
	<th>_(User)</th>
	<th>_(Message)</th>
</tr>

<?	// Line for current event version ?>
<tr id="current">
	<td colspan="3">_(current)</td>
	<td><?: demos_format_date('short-date-time',$event->changed) ?></td>
	<td><?: $event->changedById!=0 ? $event->useChangedBy()->login : 'anonymous' ?></td>
	<td><?: demosphere_event_revisions_last_log($event) ?></td>
</tr>

<? // Lines for each revision ?>
	<? foreach($revisions as $revision){ ?>
		<? $ruser=User::fetch($revision['changed_by_id'],false); ?>
		<tr>
			<td><a href="$base_url/event-revision/$revision['id']"        >_(show)     </a></td>
			<td><a href="$base_url/event-revision-diff/$revision['id']"   >_(show diff)</a></td>
			<td><a href="$base_url/event-revision-restore/$revision['id']">_(restore)  </a></td>
			<td><?: demos_format_date('short-date-time',$revision['changed']) ?></td>
			<td><?: $ruser!==null ? $ruser->login : 'anonymous' ?></td>
			<td><?: $revision['last_log'] ?></td>
		</tr>
	<?}?>
</table>
