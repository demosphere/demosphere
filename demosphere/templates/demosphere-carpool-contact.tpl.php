<!DOCTYPE html>
<html xml:lang="$lang" lang="$lang">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?: t('Carpool - !site_name',['!site_name'=>$demosphere_config['site_name']]) ?></title>
     <style type="text/css">
       <!--
         #fcontainer {margin:0 auto; width:670px; font:normal 10pt arial,helvetica,sans-serif;background-color: #fff;}
         #fbody {width:630px; margin:0; padding:5px 20px; text-align:left;background-color:#fff;}
         h3 {font-size:10pt;}
		#drive-from {margin-left: 2em;border-left:1px solid #aaa;padding-left:.5em;}
		.fmessage {margin-left: 2em;border-left:1px solid #aaa;padding-left:.5em;}
         #disclaimer { font-size:8pt;border-top: 1px solid black;padding-top:.5em;color:#555;margin-top: 2em;}
       -->
     </style>
</head>
<body>
<div id="fcontainer">
    <div id="fbody">
		<p><?= t('A user of !site_link wants to share a ride with you to the following event:',
					['!site_link'=>'<a href="'.$base_url.'">'.ent($demosphere_config['site_name']).'</a>']) ?><br />
			<a href="$event->url()">$event->title</a><br/>
			<?: t('The event is scheduled !date',['!date'=>demos_format_date('full-date-time',$event->startTime)]) ?><br />
			<a href="$event->url()/carpool">_(More information here.)</a>
		</p>

		<?if($carpool->isDriver){?>
			<h3>_(Departure:)</h3>
			<div id="driver-info">
		  		_(You volunteered to drive from:)
				<div id="drive-from"><?= str_replace("\n",'<br />',ent($carpool->from))?></div><br/>
				<?: t('You wanted to leave !date',['!date'=>demos_format_date('full-date-time',$carpool->time)]) ?>
			</div>
		<?}?>

		<h3>_(Contact:)</h3>
		<div id="sender-contact">
			_(You can answer to this message here:) 
			<? if(strpos($senderContact,'@')===false){?>$senderContact
			<?}else{?>
				<a href="mailto:$senderContact">$senderContact</a>
			<?}?>
		</div>

		<h3>_(The message:)</h3>
		<div class="fmessage"><?= str_replace("\n",'<br />',ent($message)) ?></div>
		<p></p>
        <div id="disclaimer">_(If you notice that somebody is misusing this email service, please contact: contact@demosphere.net)</div>
	</div>
</div></body></html>
