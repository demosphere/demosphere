<div id="place-view" class="stdHtmlContent <?: $place->description  && $place->id==$place->referenceId ? 'hasDescription' :'noDescription' ?>">
	<? if($place->hasDescription && $place->id==$place->referenceId){ ?>
		<p class="place-description-label">_(Place description)</p>
		<h1 id="topTitle">$place->nameOrTitle()</h1>
	<?}?>
	<?= $placeHtml ?>
	<? if($isEventAdmin){ ?>
	<div class="moderator-info">
		<span class="small-title">_(moderator info : )</span>
		<h3>_(Place id:) $place->id</h3>
		<? if($place->inChargeId!=0 && $place->inChargeChanged!=0){ ?>
			<?= t('Place edited by !user on @date',
					['!user'=>'<a href="'.ent($base_url).'/user/'.$place->inChargeId.'">'.ent($place->useInCharge()->login).'</a>',
					 '@date'=>demos_format_date('full-date-time',$place->inChargeChanged)]) ?>
	    <?}?>
		<? if($place->referenceId!=$place->id){ ?>
			<p class="referenced">
				_(This place is only a variant of place:)
				<a href="$base_url/$place_url_name/$place->referenceId">$place->referenceId</a>
			</p>
		<?}?>
		<p class="has-description">
			<? if(!$place->hasDescription  && $place->id==$place->referenceId){ ?>
				_(This place does not have a description.)
			<?}?>
			<? if($place->hasDescription && $place->id!=$place->referenceId){ ?>
				_(This place has a description but is only a variant. You should erase the description!)
			<?}?>
		</p>
	</div><!-- end moderator-info -->
	<?}?>
	<? if($place->description && $place->id==$place->referenceId){?>
		<div id="htmlView">
			<?= $description ?>
		</div><!-- end htmlView -->
		<hr/>
	<?}?>
	<div class="place-events">
	<? if(count($futureEvents)){?>
		<h2>_(Future events at this place:)
			<span>(<?: count($futureEvents) ?>)</span>
		</h2>
	<?}?>
	<table>
		<? foreach($events as $event){?>
			<? // Add title for past events ?>
			<? if($event===false){?>
				<? if(count($pastEvents)){?>
					</table>
					<h2>_(Past events at this place:)
						<span>(<?: count($pastEvents)?>)</span>
					</h2>
					<table>
				<?}?>
				<?continue;?>
			<?}?>
			<? // Actual event row ?>
			<tr class="
					<? if($isEventAdmin){?>
						mstatus-$event->getModerationStatus() nattention-level-<?: ceil($event->needsAttention/100)?>
					<?}?>
					">
				<td>
					<?= implode('</td><td>',
						array_map('ent',explode('|',demos_format_date('panel-list',$event->startTime)))); ?>
				</td>
				<td>
					<a href="$event->url()">
						<?= filter_xss(strlen($event->htmlTitle) ? $event->htmlTitle : ent($event->title),['strong']) ?>
					</a>
				</td>
			</tr>
		<?}?>
	</table>
	</div>
	<script type="application/ld+json">
		<?= dlib_json_encode_esc($placeRdf) ?>
	</script>
</div><!-- end stdHtmlContent -->
