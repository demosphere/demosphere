<div class="search-results">
	<? if($foundDocs===false){?>
		_(Search failed)
	<? }elseif(count($foundDocs)==0 && $optionsLoc['include-events'] && $optionsLoc['event-date']==='future'){ ?>
		<p id="empty-search">
			_(Your search did not match any future events.)<br/>
		</p>
	<?}elseif(count($foundDocs)==0 && $optionsLoc['include-events'] && $optionsLoc['event-date']==='past'){ ?>
		<p id="empty-search">
			_(Your search did not match any past events.)<br/>
		</p>
	<?}elseif(count($foundDocs)==0 && !$isBox){?>
		<p id="empty-search">
			<?: t('Your search did not match any documents.') ?>		
		</p>
	<? }else{?>

		<? // ****** pager ?>
		<? if(count($foundDocs)!==0 && $pager!==null){?>
			<?= render('dlib/pager.tpl.php') ?>
		<? }?>

		<? // ****** Render each search result ?>
		<? foreach($foundDocs as $id=>$doc){?>
			<? extract($doc);?><? // $type, $obj and $snippets ?>
			
			<div class="search-result doc-type-$type  
				<? if($type==='event'){?>
					<?: $obj->status      ? 'published' : 'not-published'?>
				<?}?>
				<? if($type==='comment'){?>
					<?: $obj->isPublished ? 'published' : 'not-published'?>
				<?}?>
				<? if($type==='event'){?>
					event-$obj->getModerationStatus()
					<? if($isEventAdmin){ ?>
						nattention-level-<?: !$obj->needsAttention ? 0:1+floor($obj->needsAttention/100) ?>
					<?}?>
				<?}?>
				<? if($type==='mail'){?>
					<?: $obj->finishedReading || $obj->isAutoHidden() ||  $obj->importance==='trash' ? 'finished' : 'not-finished'?>
				<?}?>
				<? if($type==='feed'){?>
					<?: $obj->finishedReading ? 'finished' : 'not-finished'?>
				<?}?>
				">
				<? // ****** Each result type is rendered differently ?>
				<? switch($type){
				   case 'event': $event=$obj; ?>
					<h3><span class="type"></span><a href="$event->url()"><?= filter_xss($snippets['title'][0]) ?></a></h3>
					<p class="search-date"><?= filter_xss($snippets['extra'][0]) ?></p>
					<p class="search-body"><?= filter_xss($snippets['publicText'][0]) ?></p>
					<? if($isEventAdmin){?>
						<? $okPrivate=preg_grep('@<strong>@',$snippets['privateText']);?>
						<? if(count($okPrivate)){?>
							<p class="search-private"><?= filter_xss(implode(' ... ',$okPrivate)) ?></p>
						<?}?>
					<?}?>
				<? break; 
				   case 'mail': $message=$obj;  ?>
					<h3><span class="type"></span><a href="$message->url()"><?= filter_xss($snippets['title'][0]) ?></a></h3>
					<p class="search-date"><?: demos_format_date('full-date-time',$message->dateFetched) ?></p>
					<p class="search-mail-from">_(From:) <span><?= filter_xss($snippets['extra'][0]) ?></span></p>
					<p class="search-body"><?= filter_xss($snippets['privateText'][0]) ?></p>
				<? break; 
				   case 'feed': $article=$obj;  ?>
					<h3><span class="type"></span><a href="$base_url/feed-import/article/$article->id/view-single"><?= filter_xss($snippets['title'][0]) ?></a></h3>
					<p class="search-feed-name"><span><?: $article->useFeed()->name ?></span></p>
					<p class="search-fetched"><?: demos_format_date('full-date-time',$article->lastFetched) ?></p>
					<p class="search-body"><?= filter_xss($snippets['privateText'][0]) ?></p>
				<? break; 
				   case 'comment': $comment=$obj; $event=Event::fetch($comment->pageId); ?>
					<div class="comment-event comment-event-<?: $event->status ? 'published' : 'not-published'?>">
						<h3><a href="$event->url()#comment-$comment->id"><?: $event->title ?></a></h3>
						<p class="search-date"><?: demos_format_date('full-date-time',$event->startTime) ?></p>
					</div>
					<div class="comment-part">
						<h3><span class="type"></span><a href="$event->url()#comment-$comment->id">
							<?= strlen(filter_xss($snippets['title'][0])) ? filter_xss(trim($snippets['title'][0])) : ent(t('no title')) ?>
						</a></h3>
						<p class="search-body"><?= filter_xss($snippets['publicText'][0]) ?></p>
						<? if($isEventAdmin){?>
							<? $okPrivate=preg_grep('@<strong>@',$snippets['privateText']);?>
							<? if(count($okPrivate)){?>
								<p class="search-private"><?= filter_xss(implode(' ... ',$okPrivate)) ?></p>
							<?}?>
						<?}?>
					</div>
				<? } ?>
			</div>
		<?}?>
	<?}?>
</div>
