<!DOCTYPE html
     PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style type="text/css">
		table
		{
			border-collapse: collapse;
		}
		h3
		{
			margin-top: 1.3em;
			margin-bottom: .3em;
		}
		.event-title 
		{ 
			padding: 0 1em;
			width: 48em;
		}
		#footer		
		{
			margin-top: 2em;
			font-style: italic;
		}
    </style>
  </head>
<body>
<?if(count($events)===0){?>
	<p id="is-empty">_(This email contains no events, because no events currently match your preferences. Normally an empty event email would not be sent to you. It was only sent because this is a test.)</p>
<?}else{?>
<h2><?= t('Events from !site_link.',['!site_link'=>'<a href="'.$base_url.'">'.ent($demosphere_config['site_name']).'</a>'])?></h2>
<div id="calendar">
	<? foreach($events as $nb=>$event){ ?>
		<? if($event->render['is_first_event_of_month'] && $nb>0){?>
			<h2><?= strftime('%B %Y', $event->startTime) ?></h2>
		<?}?>
		<? if($event->render['is_first_event_of_week'] && $nb>0){?>
			<hr class="week"/>
		<?}?>
		<?if($event->render['is_first_event_of_day']){?>
			<h3><?: demos_format_date('full-date-time-no-year',strtotime('3:33',$event->startTime)) ?></h3>
			<table>
		<?}?>
		<tr class="<?: $event->lastBigChange<$values['last-sent-time-actual'] ? 'notnew' : 'new' ?>">
		<td>$event->render['time']</td>
		
		<td class="event-title"><a href="$event->render['url']">$event->title</a></td>

		<td><?: $event->usePlace()->useCity()->getShort()?></td>
		</tr>
		
		<? if($event->render['is_last_event_of_day']){ ?>
			</table>
		<?}?>
	<?}?>
</div>
<?}?>
<ul id="footer">
    <li><a href="$base_url/email-subscription-form">_(Change the options for this email.)</a></li>
    <li><a href="$base_url/unsubscribe/$uid?auth=$auth">_(Quick unsubscribe.)</a></li>
</ul>
</body>
</html>