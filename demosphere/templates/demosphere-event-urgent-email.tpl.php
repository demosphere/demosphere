<? echo '<?xml version="1.0" encoding="UTF-8"?>';?>
<!DOCTYPE html
     PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>$site_name - $event->title</title>
     <style type="text/css">
       <!--
         #fcontainer {margin:0 auto; width:670px; font:normal 10pt arial,helvetica,sans-serif;background-color: #fff;}
         #fheader {width:670px; margin:.2em; margin-bottom:.5em; text-align:center;}
         #fbody {width:630px; margin:0; padding:5px 20px; text-align:left;background-color:#fff;}
         h3 {font-size:10pt;}
         #disclaimer { font-size:8pt;border-top: 1px solid black;padding-top:.5em;color:#555;}
		.faddress {margin-left: 2em;border-left:1px solid #aaa;padding-left:.5em;}
		.fmessage {margin-left: 2em;border-left:1px solid #aaa;padding-left:.5em;}
		.flabel{margin-bottom:.2em;margin-top:.2em;}
       -->
     </style>
</head>
<body>
<div id="fcontainer">
    <p id="fheader">
       <?= t('Urgent event from !sitename',['!sitename'=>'<a href="'.$base_url.'">'.ent($site_name).'</a>']) ?>
    </p>
    <div id="fbody">
		<h3><a href="$event->url()">$event->title</a></h3>
       <p class="flabel">
            <strong>_(Event's date : )</strong><?: demos_format_date('full-date-time',$event->startTime)?><br/>
            <strong>_(Event's location : )</strong>
       </p>
       <div class="faddress">
            ($city) <br/>
            <?= $place->htmlAddress() ?>
            <? if($map_url!==''){?>
				<br/>
				<span class="node-map">[<a href="$map_url">_(view map)</a>]</span>
			<?}?>
       </div>
       <p>
           <a href="$event->url()">
             (_(for more information on this event, click here))
           </a>
       </p>
       <div id="disclaimer"><a href="$base_url/email-subscription-form">_(Change the options for this email or stop receiving it.)</a></div>
       </div>
</div></body></html>
