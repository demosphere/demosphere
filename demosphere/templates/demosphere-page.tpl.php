<? // Template used for all pages  (except frontpage) ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="$lang" xml:lang="$lang" class="h">
<head>
	<title>$title</title>
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<? if($isMobile){ ?>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
	<?}?>
	<?= $robots ?>
	<?= implode("\n",$head) ?>
	<link rel="shortcut icon" href="$base_url/<?: demosphere_cacheable('files/images/favicon.ico')?>" type="image/x-icon" />
	<? // Tells google which page it should really index ?>
	<? if(isset($canonicalUrl)){ ?><link rel="canonical" href="$canonicalUrl"/><? }?>
	<? // Tell Google about mobile version: https://developers.google.com/webmasters/mobile-sites/mobile-seo/separate-urls  ?>
	<? if(isset($canonicalUrl) && !demosphere_is_mobile())
	{ ?><link rel="alternate" media="only screen and (max-width: 640px)" 
			  href="<?: preg_replace('@^(https?://)(www\.)?@','$1mobile.',$canonicalUrl)?>"/><? 
	}?>
	<?= $css ?>
	<?= $js ?>
</head>
<body class="<?: implode(' ',$bodyClasses) ?>">
	<?= $bodyJavascript ?>
	<?= $eventAdminMenubar ?>
	<?= isset($dcomponentMenubar) ? $dcomponentMenubar : '' ?>
	<?= isset($dcomponentLogBox ) ? $dcomponentLogBox  : '' ?>
	<div id="wrap">
		<?= isset($wrapTop) ? $wrapTop : '' ?>
		<div id="header">
			<? if($showPageTop){?>
				<? if($userCalUid===false){?>
					<div id="siteName">
						<a href="$base_url">
							<div id="page-logo"></div>
							<h1><span id="big-site-name">$demosphere_config['big_site_name']</span><span id="slogan"><?= t('FIXME: wrapped<br />2 line site slogan') ?></span></h1>
							
						</a>
					</div>
					<? if(isset($_GET['politis'])){ ?>
						<div id="politis-backlink"><a href="http://www.politis.fr/-L-agenda-militant-.html">Retour à l'agenda Politis</a></div><?//'?>
					<?}?>
				<?}else{?>
					<div class="userCalendarSiteName">
						<a href="$base_url/usercal/$userCalUid">$calLinkName</a>
					</div>
				<?}?>
				<div id="right">
					<a href="/hprobzarc" class="hprob"></a><?/*honeypot for bad robots*/?>
					<div id="rightInfo">
						<ul id="primary-links"> 
							<? foreach($demosphere_config['primary_links'] as $link){ ?>
								<li><a href="$link['url']" <?= $link['url']=='/publish' ? ' rel="nofollow" ' : '' ?>>$link['title']</a></li>
							<?}?>
						</ul>
						<?= $secondary_links ?>
						<?= $nonAdminHelpButton ?>
					</div>
				</div>
			<?}else if($isMobile){ ?>
				<h1 id="siteName" class="<?: strlen($demosphere_config['site_name'])>22 ? 'long-name':''?>"><a href="$base_url"><?: $siteName ?></a></h1>
				<?= render('demosphere-mobile-menu.tpl.php'); ?>
			<? } ?>
		</div>
		<div id="columnWrapper">
			<div id="main">
				<?= $messages ?>
				<div id="content-and-tabs">
					<? if(!empty($tabs)){ ?>
						<ul class="tabs primary">
						<? foreach($tabs as $name=>$tab){ ?>
							<li class="<?: isset($tab['active']) ? 'active' :'' ?> tab-$name <?:val($tab,'class','') ?>">
								<a href="$tab['href']">$tab['text']</a></li>
						<? } ?>
						</ul>
					<? } ?>
					<!-- begin content -->
					<div id="content"><?= $content ?></div>
					<!-- end content -->
				</div>
			</div><!-- end main -->
		</div><!-- end columnWrapper -->
	</div><!-- end wrap -->
</body>
</html>
