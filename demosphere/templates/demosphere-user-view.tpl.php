
<h1 class="page-title">
	<? if($cuser->id!=$user->id){echo t('Settings for "@name"',['@name'=>$cuser->login]);}
	   else{ ?>_(My account)<?}?>
</h1>

<div id="user-sections">

	<section id="personal-info" class="user-section">
		<a class="button big-button big-button-green" href="$base_url/user/$cuser->id/edit">_(Configure)</a>
		<h3><span></span>_(Personal info)</h3>
		<ul>
			<li><strong>_(Login:)</strong> $cuser->login</li>
			<li><strong>_(Email:)</strong> 
			<? if($cuser->email!==''){ ?>
				<a href="mailto:$cuser->email">$cuser->email</a><br/>
			<? }else{ ?>
				<?: $cuser->data['email-verify']['email'] ?> (_(not yet verified))
			<? }?>
			<? if($user->checkRoles('admin')){?>
				<li><strong>_(Since:)      </strong> <?: demos_format_date('full-date-time',$cuser->created)?>
				<li><strong>_(Role):       </strong> <?: $cuser->getRole() ?></li>
				<li><strong>_(Last login): </strong> <?: demos_format_date('full-date-time',$cuser->lastLogin )?></li>
				<li><strong>_(Last access):</strong> <?: demos_format_date('full-date-time',$cuser->lastAccess)?></li>
				<li><strong>_(data):	   </strong> 
					<? if(!count($cuser->data)){echo '[empty]';}else{?> 
					<span id="user-data-toggle">[+]</span><pre id="user-data"><?: print_r($cuser->data,true) ?></pre><?}?>
			<?}?>
		</ul>
	</section>

	<section id="email-subscriptions" class="user-section">
		<? if(!$cuser->checkRoles('admin','moderator')){ ?>
			<a  class="button big-button big-button-green" 
				href="$base_url/email-subscription-form<?: $cuser->id!=$user->id ? '?uid='.$cuser->id : ''?>">_(Configure)</a>
		<?}?>
		<h3><span></span>_(Email subscriptions)</h3> 
		<p class="description">_(Emails that you receive from this site.)</p>
		<ul>
			<li>
				<a href="$base_url/email-subscription-form<?: $cuser->id!=$user->id ? '?uid='.$cuser->id : ''?>">
					_(Receive daily/weekly emails with your selection of events.)
				</a><br/>
				<?: $emailSubscription ? t('Status: active') : t('Status: off') ?> <br/>
				_(Last sent:) <?: $emailSubsLastSent ? 
								 demos_format_date('full-date-time',$emailSubsLastSent) : 
								 t('never') ?>
			</li>
			<? if($cuser->checkRoles('admin','moderator')){ ?>
				<li>
					<a href="$base_url/user/$cuser->id/emails-received-options">_(Emails sent to you as a moderator.)</a>
				</li>
			<?}?>
			<? if($opinions!==false){?>
				<li>
					 <a href="$base_url/user/$cuser->id/opinion-options">_(Email with events that need my opinion)</a>
				</li>
			<?}?>
		</ul>
	</section>

	<? if(count($inChargePlaces)){?>
		<section id="in-charge-places" class="user-section">
			<h3><span></span>_(In charge of places)</h3> 
			<ul>
				<?foreach($inChargePlaces as $place){?>
					<li><a href="$place->url()">$place->nameOrTitle()</a></li>
				<?}?>
			</ul>
		</section>
	<?}?>

	<? if(count($myEvents) || count($myRepetitions) || count($carpools)){?>
		<section id="my-events" class="user-section">
			<h3><span></span>_(My events)</h3> 
			<? if(count($myEvents)){ ?>
				<? $ct=0;?>
				<table id="my-events-table">
					<? foreach($myEvents as $event){?>
						<tr <?= $ct++>10 ? 'class="smore-line"' : ''?>>
							<td><?: demos_format_date('day-month-abbrev',$event->startTime)?></td>
							<td>
								<?$url=false;?>
								<?if($event->access('view')){$url=$event->url();}?>
								<? // just in case. don't think this really happens ?>
								<?if($event->access('edit') && !$event->access('view')){$url=$event->url().'/edit';}?>
								<?if($url!==false){?><a href="$url"><?}?>
									$event->title
								<?if($url!==false){?></a><?}?>
							</td>
						</tr>
					<?}?>
				</table>
			<?}?>
			<? if(count($myRepetitions)){ ?>
				<h4>_(Confirm event repetitions)</h4>
				<ul id="my-repetitions">
					<? foreach($myRepetitions as $repetition){?>
						<li><a href="$base_url/repetition-group/$repetition->id/confirm"><?: $repetition->useReference()->title?></a></li>
					<?}?>
				</ul>
			<?}?>
			<? if(count($carpools)){?>
				_(Share a ride:)
				<? foreach($carpools as $carpool){?>
					<a href="<?: $carpool->useEvent()->url() ?>/carpool"><?: demos_format_date('day-month-abbrev',$carpool->useEvent()->startTime) ?></a>,
				<?}?>
			<?}?>
		</section>
	<?}?>

	<? if(count($myComments) || $cuser->checkRoles('admin','moderator')){?>
		<section id="my-comments" class="user-section">
			<h3><span></span>_(My comments)</h3> 
			<? if($cuser->checkRoles('admin','moderator')){?>
				<a href="$base_url/comment/notifications/manager<?: $cuser->id!=$user->id ? '?userId='.$cuser->id: '' ?>">notifications</a>
			<?}?>
			<ul>
				<? $ct=0; ?>
				<? foreach($myComments as $comment){?>
					<? $link=$myCommentsLinks[$comment->id]; ?>
					<li <?= $ct++>10 ? 'class="smore-line"' : ''?>>
						<? if($link['url']!==false){?><a href="<?: $link['url'] ?>"><?}?><?
							?><?: $link['title'] ?>
						<? if($link['url']!==false){?></a><?}?>
					</li>
				<?}?>
			</ul>
		</section>
	<?}?>

	<? if($opinions!==false){?>
		<section id="my-opinions" class="user-section">
			<a class="button big-button big-button-green" href="$base_url/user/$cuser->id/opinion-options">_(Configure)</a>
			<h3><span></span>_(Opinions)</h3>
			<p class="description">_(My opinions on event event moderation.)</a></p>
			<table id="my-opinions-list">
				<?foreach($opinions as $opinion){?>
					<? $event=$opinion->useEvent();?>
					<tr>
						<td class="rating">$opinion->rating</td>
						<td class="decision">
							<? if($event->status==1){?>_(published)
							<? }else ?>
							<? if($event->getModerationStatus()==='rejected-shown' ||
								  $event->getModerationStatus()==='rejected'		  ){?>_(rejected)<?}?>
						</td>
						<td class="title"><a href="$event->url()#opinion-$opinion->id"><?: $event->title?></a></td>
					</tr>
				<?}?>
			</table>
			<? if(count($opinions)){?>
				<div id="opinion-list-details"><a href="$base_url/user/$cuser->id/opinions">+</a></div>
			<?}?>
			<h4>_(Events that need my opinion:)</h4>
			<table id="events-for-opinions">
				<? $ct=0;?>
				<? foreach($opEvents as $event){?>
					<tr <?= $ct++>20 ? 'class="smore-line"' : ''?>>
						<td><?: demos_format_date('day-month-abbrev',$event->startTime)?></td>
						<td><a href="$event->url()">$event->title</a></td>
					</tr>
				<?}?>
			</table>
		 
			<? $howOften=val(val($cuser->data,'opinion-options',[]),'email-how-often'); ?>
			<p>_(Email me:) <?: $howOften ? t('max every !nb day(s)',['!nb'=>$howOften]) : t('no') ?>
		</section>
	<?}?>

	<section id="on-my-website" class="user-section">
		<a class="button big-button big-button-green" href="$base_url/widget-config<?: $cuser->id!=$user->id ? '?uid='.$cuser->id : ''?>">_(Configure)</a>
		<h3><span></span>_(Show on my own web site)</h3>
		<p class="description">_(Show a calendar with your selection of events on your own website or blog)</p>
	</section>
	
	<section id="my-calendar" class="user-section">
		<a class="button big-button big-button-green" href="$base_url/user-calendar-config?uid=$cuser->id">_(Configure)</a>
		<h3><span></span>_(My custom calendar)</h3>
		<p class="description">_(Create a calendar on this site with your own events.)</p>
		<ul>
			<li>
				<?= t("The personal calendar for @name has the following address:",['@name'=>$cuser->login]) ?><br/>
				<a href="$base_url/usercal/$cuser->id">$base_url/usercal/$cuser->id</a>
			</li>
			<li>
				_(The RSS feed for this calendar is:)<br/>
				<a href="$base_url/events.xml?userCalendar=$cuser->id">$base_url/events.xml?userCalendar=$cuser->id</a>
			</li>
			<li>
				<?: t('The calendar file (ical format) is :')?><br/>
				<a href="$base_url/events.ics?userCalendar=$cuser->id">$base_url/events.ics?userCalendar=$cuser->id</a>
			</li>
		</ul>
	</section>

</div>
