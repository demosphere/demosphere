<h2 id="page-title">_(Share a ride)</h2>

<? if(!$event->carpoolIsEnabled()){?>
<p id="carpool-not-enabled">_(These shared rides are not visible to ordinary users.)</p>
<?}?>
<div id="destination">
    <h3>_(Destination:)</h3>
    <div id="event-title"><a href="$event->url()">$event->title</a></div>
	<div class="centered">
		<div class="left">
			<div id="date"><span>_(Arrival at:)</span><?: demos_format_date('full-date-time',$event->startTime) ?></div>
		    <div id="address">
				<?= $place->htmlAddress() ?>
			</div>
		</div>
		<? if($hasMapImage){?>
		    <div id="map">
		        <a class="mapimage-link" href="$mapUrl">
			     	<img   src="$mapImage['url']" alt="_(map)" 
						width ="$mapImage['width']" 
						height="$mapImage['height']" />
		        </a>
		    </div>
		<?}?>
	</div>
</div>

<table class="carpool-list">
	<? foreach($carpools as $id=>$carpool){ ?>
		<tr class="carpool <?: $carpool->isDriver ? 'driver' : 'passenger' ?>">
		    <td class="carpool-driver" >
				<span class="driver-label"><?: $carpool->isDriver ? t('driver') : t('passenger') ?></span>
			</td>
			<td class="carpool-name-and-contact" >
				<div class="carpool-name" >
					<span class="label">_(Name:)</span><br/>$carpool->name
				</div>	
				<div class="carpool-contact <?: $carpool->hideEmail ? 'hide-email' :'' ?>">
					<? if($carpool->hideEmail){?>
						<a href="$base_url/carpool/$carpool->id/contact">_(Contact me here)</a>
					<? }else if($carpool->contact===''){?>
						<span class="label"><?: t('(no contact)') ?></span>
					<? }else{ ?>
						<span class="label">_(Contact me:)</span><br/>
						<? if(strpos($carpool->contact,'@')!==false){ ?>
							<a href="mailto:..." onmouseover="unobfuscate(this);" onmousedown="unobfuscate(this);">
								<?= demosphere_misc_obfuscate_email($carpool->contact,false) ?>
							</a>
						<? }else{ ?>
							$carpool->contact
						<?}?>
					<? } ?>
				</div>
			</td>
			<td class="carpool-time-and-from <?: $carpool->time ? 'has-time':'no-time' ?>">
				<div class="carpool-time">
					<? if($carpool->time){ ?>
						<span class="label">_(I'm leaving at:)</span><?//'?>
					    <?: date('z Y',$carpool->time)==date('z Y',$event->startTime) ? 
							strftime('%R',$carpool->time) :
							demos_format_date('full-date-time',$carpool->time)  ?>
					<?}?>
				</div>
				<div class="carpool-from">
					<span class="label">_(I'm leaving from:)</span><br/><? //' ?>
					<?= str_replace("\n",'<br/>',ent($carpool->from))?>
				</div>
			</td>
			<td class="carpool-comment"	 >
				<? if($carpool->comment!==''){ ?>
					<span class="label">_(Comment:)</span><br/>
					<?= str_replace("\n",'<br/>',ent($carpool->comment))?>
				<?}?>
			</td>
			<td class="carpool-roundtrip"><?= $carpool->isRoundTrip ?  t('Round <br />trip') : t('One <br />way') ?></td>
			<td class="carpool-controls">
				<? if($carpool->canEdit($user)){?>
					<a href="$base_url/carpool/$carpool->id/edit">_(Edit)</a>
				<?}?>
			</td>
		</tr>
	    <tr class="spacer-line"><td></td><td></td><td></td><td></td><td></td><td></td></tr><?//ugly hack ?>
	<?}?>
</table>

<div id="button-line-message" class="<?: count($carpools)===0 ? 'no-carpools': ''?>">
	<span>
		<? if(count($carpools)===0){?>
			_(Be the first one to share a ride:)
		<?}else{?>
			_(Add a new ride:)
		<?}?>
	</span>
</div>

<div id="new-carpool-button-line">
	<a id="new-carpool-driver-button"  class="big-button big-button-green" href="$base_url/carpool/add?eventId=$event->id&amp;isDriver=1">
		_(I'm a driver)    <span class="plus"></span>
	</a>
	<a id="new-carpool-passenger-button" class="big-button big-button-green" href="$base_url/carpool/add?eventId=$event->id&amp;isDriver=0">
		_(I'm a passenger) <span class="plus"></span>
	</a>
</div>
