<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="$lang" xml:lang="$lang">
<head>
	<title>$title</title>
	<meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<?= $robots ?>
	<?= implode("\n",$head) ?>
	<link rel="shortcut icon" href="$base_url/<?: demosphere_cacheable('files/images/favicon.ico')?>" type="image/x-icon" />
	<link rel="alternate" type="application/rss+xml" title="$siteName RSS" href="$rssLink" />
	<link rel="alternate" type="text/calendar" title="$siteName iCalendar" href="$icalLink" />
	<link rel="canonical" href="$std_base_url"/>
	<? // Tell Google about mobile version: https://developers.google.com/webmasters/mobile-sites/mobile-seo/separate-urls  ?>
	<link rel="alternate" media="only screen and (max-width: 640px)" href="<?: preg_replace('@^(https?://)(www\.)?@','$1mobile.',$std_base_url)?>"/>
	<? // Tell browser to start downloading png now. Don't wait for CSS to request it. ?>
	<link rel="preload" as="image" href="$base_url/<?: demosphere_cacheable('files/images/sprites/frontpage.png') ?>">
	<? // Tell browser to download event page png when it is idle. It will speed the next (future) event page display ?>
	<? // Unfortunately Chrome starts downloading this now... so it blocks other stuff :-(   ?>
	<? // <link rel="prefetch" as="image" href="$base_url/ demosphere_cacheable('files/images/sprites/page.png') "/>  ?>
	<meta name="description" content="$metaDescription"/>
	<meta name="keywords" content="$metaKeywords"/>
	<?= $css ?>
</head>
<body class="<?: implode(' ',$bodyClasses) ?>">
<?= $eventAdminMenubar ?>
<div id="wrap">
	<a href="/flaveolaria" class="hprob"></a><?/*honeypot for bad robots*/?>
	<?= $bodyJavascript ?>
	<div id="main">
		<div id="header">
			<? if($userCalBanner===false){ ?>
				<div id="siteName">
					<h1>
						<a id="home-link" href="$base_url">
							<span id="fp-logo"></span>
							<span id="fp-big-name">$demosphere_config['big_site_name']</span>
						</a>
					</h1>
					<h2 id="slogan">$demosphere_config['site_slogan']</h2>
				</div>
				<? if(strlen($demosphere_config['site_mission'])){ ?>
					<div id="mission">$demosphere_config['site_mission']</div>
				<?}?>
			<?}else{?><?= $userCalBanner ?><?}?>
			<?= $secondary_links ?>
			<?= $calTopics ?>
			<div id="searchButton">	
				<form method="get" action="$base_url/search">
					<input id="search-field" type="search" placeholder="_(Search)" name="search"/><button id="search-submit"  type="submit">&nbsp;</button>
				</form>
			</div>
			<div id="options-wrap">
				<div id="options">
					<?= $options ?>
				</div>
				<div id="options-extra">
					<a id="options-link"></a>
					<a id="options-clear" href="$base_url<?: $userCalendar ? '/usercal/'.intval($userCalendar) : '' ?>">x</a></div>
			</div>
		</div>
		<a href="/hprobzarc" class="hprob"></a><?/*honeypot for bad robots*/?>
		<?= $annouce ?>
		<?= $featured ?>
		<?= $message ?>
		<?= $cities ?>
		<div id="calendar">
			<?= $calendar ?>
			<? if($autoLimitMoreEventsUrl!==false){?>
				<div id="autoLimitMoreEventsUrl"><a href="$autoLimitMoreEventsUrl">_(show more events)</a></div>
			<?}?>
		</div>
	</div>
	<div id="sidebar">
		<?= $calnavigation ?>
		<?= $infoboxes ?>
	</div>
	<div id="footer">
		<div id="extra-back-to-mobile"><a href="$base_url/?switchMobile=mobile">_(mobile)</a>
	</div>
	</div>
</div><!-- end wrap -->
<? // Javascript is here for performance ?>
<?= $js ?>
</body>
</html>
