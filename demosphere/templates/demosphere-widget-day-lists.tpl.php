<div class="demosphere-widget">
	<? foreach($events as $event){ ?>
		<? if($event->render['is_first_event_of_day']){ ?>
			<? $format=$options['day-header-format']; ?>
			<? if($format!==false){ ?>
				<h4 class="day">
					<?: strpos($format,'|')===0 ? 
							demos_format_date(substr($format,1),$event->startTime):
							strftime($format,$event->startTime) ?>
				</h4>
			<?}?>
			<ul>
		<?}?>
		<li>
			<a class="event <?: $event->render['row_classes'] ?>" target="_top" href="$event->render['url']">
				<span class="date">
					<? $format=$options['time-format']; ?>
					<?: preg_replace('@0?3[:h]33@','-',strpos($format,'|')===0 ? 
							   demos_format_date(substr($format,1),$event->startTime):
							   strftime($format,$event->startTime)) ?>
				</span>
				<? if(val($options,'show-city')){ ?>
					<span class="location city"><?: $event->usePlace()->useCity()->getShort() ?></span> - 
				<?}?>
				<span>$event->title</span>
			</a>
		</li>
		<? if($event->render['is_last_event_of_day']){ ?> </ul> <?}?>
	<?}?>
	<?if(count($events)===0){?><?: t('(no events)') ?><?}?>
	<p class="extra-message"><?: filter_xss($options['extra-message']) ?></p>
</div>
