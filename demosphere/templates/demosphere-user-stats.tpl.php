<fieldset class="collapsible collapsed">
	<legend><span>_(admins:) <?:count($stats['admins'])?> </span></legend>
	<div class="collapsible-content">
		<ul>
			<? foreach($stats['admins'] as $cuser){?>
				<li><a href="$base_url/user/$cuser->id">$cuser->login</a>
				<?= isset($authored[$cuser->id])? '('.$authored[$cuser->id].')' :''?></li>
			<?}?>
		</ul>
	</div>
</fieldset>

<fieldset class="collapsible collapsed">
	<legend><span>_(moderators:) <?:count($stats['moderators'])?> </span></legend>
	<div class="collapsible-content">
		<ul>
			<?foreach($stats['moderators'] as $cuser){?>
				<li>
					<a href="$base_url/user/$cuser->id">$cuser->login</a>
					<?= isset($authored[$cuser->id])? '('.$authored[$cuser->id].')' :''?>
				</li>
			<?}?>
		</ul>
	</div>
</fieldset>

<fieldset class="collapsible collapsed">
	<legend><span>_(ordinary:) <?:count($stats['ordinary'])?> </span></legend>
	<div class="collapsible-content">
		<ul>
			<?foreach($stats['ordinary'] as $cuser){?>
				<li>
					<a href="$base_url/user/$cuser->id">$cuser->login</a>
					<?= isset($authored[$cuser->id])? '('.$authored[$cuser->id].')' :''?>
				</li>
			<?}?>
		</ul>
	</div>
</fieldset>

<fieldset class="collapsible collapsed">
	<legend><span>_(Logged-in during last 3 months:) <?:count($stats['active'])?> </span></legend>
	<div class="collapsible-content">
		<ul>
			<?foreach($stats['active'] as $cuser){?>
				<li>
					<a href="$base_url/user/$cuser->id">$cuser->login</a>
					<?= isset($authored[$cuser->id])? '('.$authored[$cuser->id].')' :''?>
				</li>
			<?}?>
		</ul>
	</div>
</fieldset>

<fieldset class="collapsible collapsed">
	<legend><span>_(Subscribed to email:) <?:count($stats['email'])?>, _(sent past 24h: )$sentEmails </span></legend>
	<div class="collapsible-content">
		<ul>
			<?foreach($stats['email'] as $cuser){?>
				<li>
					<a href="$base_url/user/$cuser->id">$cuser->login</a>
					<?= isset($authored[$cuser->id])? '('.$authored[$cuser->id].')' :''?>
				</li>
			<?}?>
		</ul>
	</div>
</fieldset>

<fieldset class="collapsible collapsed">
	<legend><span>_(Customized email settings :) </span></legend>
	<div class="collapsible-content">
		<ul>
			<?foreach($stats['email-settings'] as $sname=>$setting){?>
				<li>$sname: <?: count($stats['email']) ? round(100*$setting/count($stats['email']),1) : 0 ?>%</li>
			<?}?>
		</ul>
	</div>
</fieldset>

<fieldset class="collapsible collapsed">
	<legend><span>_(Public form contacts :) (<?: count($publicFormContacts) ?>)</span></legend>
	<div class="collapsible-content">
		<table>
			<?foreach($publicFormContacts as $email=>$nb){?>
				<tr><td>$nb</td><td>$email</td></tr>
			<?}?>
		</ul>
	</div>
</fieldset>
