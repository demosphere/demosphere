<div class="post stdHtmlContent <?: $post->status ? ' published' : ' not-published' ?>">
	<h1 id="topTitle">$post->title</h1>
	<div id="htmlView">
		<?= $body ?>
	</div><!-- end htmlView -->
</div><!-- end stdHtmlContent -->
<?= $comments ?>