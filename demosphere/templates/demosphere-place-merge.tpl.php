<h1 class="page-title">_(Merge places)</h1>
<p class="place-admin-help">
	_(Select all places that are very similar to the first one and press the "merge places" button.)<br/>
	_(The selected places will be considered as only small variations of the reference place.)<br/>
	_(These small variations will not apear as independent places in suggestions and will not be displayed as public place pages.)<br/>
	_(This "merge places" operation is only necessary if users have entered similar places, without correctly using the suggest button.)<br/>
	_(Note that this merge does not change the information actually displayed on any event.)
</p>

<form method="post">
<?= dlib_add_form_token('demosphere_place_merge');?>
<input type="submit" name="submit" value="_(merge places)"/>
<table id="place-merge" class="place-diff-list">
	<tr>
		<th></th>
		<th title="_(Distance, in meters)">_(dist.)</th>
		<th title="_(Number of events at this place)">_(ev.)</th>
		<th>_(place)</th>
		<th>_(address)</th>
	</tr>
	<? foreach($places as $pid => $place){ ?>
		<tr class="<?: isset($nearby[$pid]) ? 'nearby' : 'similar' ?>
                   <?: $pid==$mergeTo->id   ? 'first'  : ''        ?> ">
			<td>
				<? if($pid!==$mergeTo->id){?>
					<input type="checkbox" name="cb-$place->id"/>
				<?}?>
		</td>
			<td>
				<?: $pid!=$mergeTo->id && $mergeTo->zoom!=0 && $place->zoom!=0 ? 
				    	round(demosphere_place_sphere_distance_place($mergeTo,$place)).'m' : '' ?>
			</td>
			<td><?: $nbEvents[$pid] ?? 0 ?></td>
			<td>[<a href="$place->url()">$place->id</a>]</td>
			<td><?= demosphere_place_diff($mergeTo,$place) ?></td>
		</tr>
	<?}?>
</table>
