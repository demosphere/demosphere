<?php

class Topic extends DBObject
{
	public $id=0;
	static $id_           =['type'=>'autoincrement'];
	public $name;
	static $name_         =[];
	public $description;
	static $description_  =[];
	public $color;
	static $color_        =[];
	public $icon;
	static $icon_         =[];
	public $frontpageLineBreak=false;
	static $frontpageLineBreak_=['type'=>'bool'];
	public $showOnEventPage=true;
	static $showOnEventPage_=['type'=>'bool'];
	public $showOnFrontpage=true;
	static $showOnFrontpage_=['type'=>'bool'];
	public $weight=false;
	static $weight_       =['type'=>'int'];

	static $generatedIconDir='files/images/topics';

 	static $dboStatic=false;

	function __construct($name='no-name')
	{
		$this->name=$name;
	}

	function delete()
	{
		db_query("DELETE FROM Event_Topic WHERE topic=%d",$this->id);
		parent::delete();
	}

	function save($clearCache=true)
	{
		if($this->weight===false)
		{
			$this->weight=1+(int)db_result_check("SELECT MAX(weight) FROM Topic");
		}
		parent::save();
		if($clearCache)
		{
			require_once 'demosphere-common.php';
			demosphere_cache_clear();
		}
	}

	static function getAll($where=false)
	{
		return Topic::fetchList(($where!==false ? $where.' ' : '').
								'ORDER BY weight ASC');
	}

	static function getAllNames()
	{
		return db_one_col("SELECT id,name FROM Topic ORDER BY weight ASC");
	}

	static function stdIcons()
	{
		return ['club'  =>'demosphere/css/images/topic-club.svg',
				'chart' =>'demosphere/css/images/topic-chart.svg',
			   ];
	}

	static function stdColors()
	{
		return [
				'ff9000'=>'orange',	
				'3fc600'=>'green' ,	
				'f00023'=>'red'   ,	
				'2bafff'=>'blue'  ,	
				'ecec00'=>'yellow',	
				'd76aff'=>'violet',	
				];
	}

	static function createTable()
	{
		db_query("DROP TABLE IF EXISTS Topic");
		db_query("CREATE TABLE Topic (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `color` varchar(250)  NOT NULL,
  `icon` longblob NOT NULL,
  `frontpageLineBreak` TINYINT(1) NOT NULL,
  `showOnEventPage`  TINYINT(1) NOT NULL,
  `showOnFrontpage`  TINYINT(1) NOT NULL,
  `weight` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `name` (`name`),
  KEY `weight` (`weight`)
) DEFAULT CHARSET=utf8mb4");

		db_query("DROP TABLE IF EXISTS Event_Topic");
		db_query("CREATE TABLE Event_Topic (
  `event` int(11) NOT NULL,
  `topic` int(11) NOT NULL,
  PRIMARY KEY  (`event`,`topic`),
  KEY `event` (`event`),
  KEY `topic` (`topic`)
) DEFAULT CHARSET=utf8mb4");
	}

}
?>
