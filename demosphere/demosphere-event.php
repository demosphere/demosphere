<?php

//! Displays the event page 
function demosphere_event_view($id)
{
	global $user,$base_url,$demosphere_config;
	$event=Event::fetch($id,false);
	if($event===null){dlib_not_found_404();}

	// redirect for externalSourceId and duplicate events
	if(!$user->checkRoles('admin','moderator')) 
	{
		if($event->externalSourceId!=0)
		{
			dlib_redirect($event->extraData['external']['url']);
		}
		if(isset($event->extraData['duplicate']))
		{
			dlib_redirect($base_url.'/'.$demosphere_config['event_url_name'].'/'.$event->extraData['duplicate']);
		}
	}

	return demosphere_event_render($event);
}

//! Called from demosphere_paths()
function demosphere_event_access($euser,$eventId,$op)
{
	$event=Event::fetch((int)$eventId,false);
	if($event===null){dlib_not_found_404();}
	if($op===''){$op='view';}
	return $event->access($op);
}

function demosphere_event_render($event,$overrides=[])
{
	global $base_url,$demosphere_config,$user,$currentPage;
	$isEventAdmin=$user->checkRoles('admin','moderator');
	require_once 'demosphere-event.php';
	require_once 'demosphere-common.php';
	require_once 'demosphere/demosphere-page.php';
	require_once 'demosphere-date-time.php';
	require_once 'dlib/filter-xss.php';

	$isMobile=demosphere_is_mobile();

	$currentPage->bodyClasses[]="notFullWidth";
	$currentPage->bodyClasses[]='unPaddedContent';
	$currentPage->bodyClasses[]="nonStdContent"; 

	$currentPage->canonicalUrl=$demosphere_config['std_base_url'].'/'.$demosphere_config['event_url_name'].'/'.intval($event->id);

	$currentPage->addCssTpl('demosphere/css/demosphere-event-view.tpl.css');
	$currentPage->addCssTpl('demosphere/css/demosphere-htmlview.tpl.css');

	$place=$event->usePlace();
	$miscEventInfo=false;
	$rightColOffset=false;

	$mainImage=demosphere_event_facebook_image($event);

	// ********* Html head : title and meta tags

	$currentPage->bodyClasses[]='event-page';
	$tags='';
	// $currentPage->robots defaults to noindex nofollow
	// Don't index old events. Don't index rejected events.
	$archiveTime=time()-$demosphere_config['event_archive_delay'];
	if($event->startTime>=$archiveTime &&
	   $event->getModerationStatus()!=='rejected-shown' &&
	   count($_GET)==1 /* only $_GET['q'] */)
	{
		$currentPage->robots=[];
	}

	// tell google to remove this event from its index after the event + a delay
	$currentPage->robots[]='unavailable_after: '.date("j-M-Y H:i:s T",$event->startTime+
													  $demosphere_config['event_archive_delay']);
	// set page title
	$title=demos_format_date('day-month-abbrev',$event->startTime).': '.$event->title." / ".
		ent($place->getCityName());
	$currentPage->title=val($event->extraData,'seo-title', $title);
	// Add html head meta tags keywords 
	if(isset($event->extraData['seo-keywords']))
	{$currentPage->head[]='<meta name="keywords" content="'.ent($event->extraData['seo-keywords']).'"/>';}
	else
	{
		// Just insist on weekday and city (everything is already in the title)
		// (actually keywords are not used very much by search engines)
		$currentPage->head[]='<meta name="keywords" content="'.
			ent(strftime('%A',$event->startTime)).','.
			ent($place->getCityName()).'"/>'."\n";
	}
	// custom description
	if(isset($event->extraData['seo-description']))
	{$currentPage->head[]='<meta name="description" content="'.ent($event->extraData['seo-description']).'"/>';}
	// Facebook image
	if($mainImage!==false)
	{
		$currentPage->head[]='<meta property="og:image" content="'.ent($base_url.'/'.$mainImage).'"/>';
	}

	// ********* page top
	// If event is published but not on frontpage, then it is user cal.
	if($event->status && !$event->showOnFrontpage && $event->authorId!=0)
	{
		$currentPage->pageTopUserCalUid=$event->authorId;
	}

	// ********* 

	// *** actionbox (comment+send email)
	require_once 'dlib/comments/comments.php';
	$currentPage->addCssTpl('demosphere/css/demosphere-comments.tpl.css');
	$currentPage->addJs('dlib/comments/comments.js','file',['loadFunction'=>'load_comments_js']);
	$useComments=comments_page_settings('Event',$event->id)!=='disabled';
	$nbComments=Comment::nb('Event',$event->id);
	if(!isset($overrides['comments'])){$comments=comments_page_render('Event',$event->id);}
	
	$carpools=[];
	if($event->carpoolIsEnabled())
	{
		$carpools=Carpool::fetchList('WHERE eventId=%d ORDER BY id DESC',$event->id);
	}

	$facebookUrl='https://www.facebook.com/sharer.php?'.
		'u='.urlencode($event->url()).'&'.
		't='.urlencode(demos_format_date('short-week-date-time',$event->startTime).
					   ': '.$event->title);
	$twitterUrl='https://twitter.com/share?'.
		'url='.urlencode($event->url()).'&'.
		'text='.urlencode(demos_format_date('short-week-date-time',$event->startTime).
						  '- '.$event->title);

	// *** Topics
	$topics=$event->getTopics();
	$topics=array_filter($topics,function($t){return $t->showOnEventPage;});

	// *** miscEventInfo
	$dayrank=false;
	$drdate=false;
	if($demosphere_config['dayrank_enable'])
	{
		$dayrank=oval($event,'dayrank',0);
		if(oval($event,'visits',0)>=$demosphere_config['dayrank_min_visits'] &&
		   ($dayrank>=1 && $dayrank<=2))
		{
			$drdate=['@date'=>demos_format_date('short-day-month',$event->startTime)];
		}
		else
		{$dayrank=false;}
	}

	// date/time:
	$startTime=$event->startTime;
	$dateText=demos_format_date('full-date-time-date',$startTime);
	$timeText=demos_format_date('full-date-time-time',$startTime);

	$dateHtml='<span class="date" data-val="'.
		(date('Y',$startTime)<=1970 ? '' :demos_format_date('short-day-month-year',$startTime)).'">'.
		ent($dateText).'</span>';
	$timeHtml='<span class="time" data-val="'.($startTime==0              ? '' : date('H:i'  ,$startTime)).'">'.ent($timeText).'</span>';
	$time=date('H:i',$startTime);


	require_once 'demosphere-event-map.php';
	$mapUrl=demosphere_get_map_url($place,$event);

	// *** place (for either event,place_description and pseudo-place types)
	$rightColOffset=0;
	require_once 'demosphere-place.php';
	$placeHtml=demosphere_place_render($event->usePlace(),!$isMobile,$mapUrl,$rightColOffset);

	$needsAttentionLevel='';
	if($isEventAdmin)
	{
		$needsAttentionLevel=
			($event->needsAttention>0 && 
			 $event->needsAttention<100 ? 'nattention-level-1' : '').
			($event->needsAttention>100 ? 'nattention-level-2' : '');
	}

	$showSmallFrontpageTitle=$demosphere_config['show_title_in_event']!=='always' &&
		$event->access('edit');
	if($showSmallFrontpageTitle){$rightColOffset-=10;}

	$warnEventNotPublished=false;
	if($user->checkRoles('none') && $event->status===0)
	{
		$warnEventNotPublished=t('This event is not published.');
	}

	// FIXME: moderator-info size should change $rightColOffset, but its too complex to do it here.

	$opinions='';
	$nbOpinions=false;
	$priceForContributors='';
	if($demosphere_config['enable_opinions'] && val($overrides,'opinions')!=='')
	{
		$nbOpinions=db_result('SELECT COUNT(*) FROM Opinion WHERE eventId=%d',$event->id);
		require_once 'demosphere-opinion.php';
		$opinions=demosphere_opinions_event($event);

		if($event->getModerationStatus()==='rejected' ||
		   $event->getModerationStatus()==='rejected-shown'){$warnEventNotPublished=t('This event has been rejected.');}

		if(!$isEventAdmin && $user->contributorLevel>0 &&
		   isset($event->extraData['public-form']) && 
		   isset($event->extraData['public-form']['price']))
		{  
			require_once 'demosphere-event-publish-form.php';
			$prices=demosphere_event_publish_form_prices(); 
			$priceForContributors=$prices[$event->extraData['public-form']['price']];
		}
	}

	$rightColOffset-=35+21;//35px date height +21px date bottom margin
	$rightColOffset+=5;// a few missing pixels somewhere (? paddings?)
	$rightColOffset+=16;// extra space "just in case" something is wrong (also adds a bit of margin)

	// *** incompleteMessage
	$incompleteMessage=false;
	if($event->getModerationStatus()==='incomplete' && $event->incompleteMessage!=='')
	{
		$incompleteMessage=$event->incompleteMessage;
		if(strpos($incompleteMessage,'%dateTime')!==false)
		{
			$msg=str_replace('%dateTime',demos_format_date('full-date-time',$event->startTime),$incompleteMessage);
		}
		require_once 'demosphere-htmlview.php';
		$incompleteMessage=demosphere_htmlview_punctuation_nbsp($incompleteMessage);
		require_once 'dlib/filter-xss.php';
		$incompleteMessage=filter_xss_admin($incompleteMessage);
		$incompleteMessage=demosphere_htmlview_emails_add_mailto_and_obfuscate($incompleteMessage);

		// FIXME: warning unicode char is not supported on android browser

		// incomplete-message size changes $rightColOffset. 
		// It is arbitrary HTML so we can't reliably compute its height.
		// We try to compute a rough lower bound to mitigate its effect.
		$nbLines=preg_match_all('@<(p|h[1-6]|li)[^>]*>@',$incompleteMessage);
		$msgHeight=6+(9.6+19)*$nbLines+9.6+17;
		$msgHeight-=5;// just in case 
		$rightColOffset-=$msgHeight;
	}


	// *** body
	$body='';
	
	// Use event title if no title is found in body (or if config says so)
	$autoTitle=$demosphere_config['show_title_in_event']==='automatic' &&
		!preg_match('@^\s*(<h[23]|<p>\s*<strong>Attention)@sU',$event->body);
	if($demosphere_config['show_title_in_event']==='always' || $autoTitle)
	{
		$body.='<h2 id="eventTitle" itemprop="name">'.ent($event->title).'</h2>';
	}
	else
	{	
		$body.='<meta itemprop="name" content="'.ent($event->title).'" />';
	}

	$body.=$event->body;

	$rightColOffset=max(0,$rightColOffset);

	require_once 'demosphere-htmlview.php';
	$body=demosphere_htmlview_body($body,$rightColOffset);
	$body=demosphere_event_hidden_links($body,$event);
	require_once 'demosphere-dtoken.php';
	$body=demosphere_dtokens($body);

	demosphere_event_tabs($event,'view');

	// *** Structured Data  / RDF : LD+JSON for Google snippets 

	if(isset($event->extraData['seo-description']))
	{
		$rdfDescription=$event->extraData['seo-description'];
	}
	else
	{
		// Html2Text::convert() is slow on large texts, use mb_substr() for performance
		$rdfDescription=Html2Text::convert(mb_substr($event->body,0,3000)); 
		$rdfDescription=dlib_truncate(trim($rdfDescription),1000);
	}
	//https://developers.google.com/search/docs/data-types/event
	$eventRdf=
		[
			'@context'=> "http://www.schema.org",
			'@type'=>'Event',
			'name'=>$event->title,
			'url'=>$event->url(),
			'startDate'=>date('Y-m-d',$event->startTime).(date('H:i',$event->startTime)!=='03:33' ? 'T'.date('H:i',$event->startTime):''),
			'endDate'  =>date('Y-m-d',$event->startTime), // Google complains if this is absent.
			'location'=>demosphere_place_rdf($place,false),
			'description'=>$rdfDescription,
		];
	// Google wants big images (min 720px)  ... but later says > 50000px = 220x220
	if($mainImage!==false){$eventRdf['image']=$base_url.'/'.$mainImage;}

	extract($overrides);

	return template_render('templates/demosphere-event.tpl.php',
						   [compact('event','body','placeHtml','needsAttentionLevel','topics','dayrank','drdate','nbOpinions','opinions','useComments','nbComments','comments','carpools','facebookUrl','twitterUrl','time','dateHtml','timeHtml','placeHtml','showSmallFrontpageTitle','warnEventNotPublished','incompleteMessage','isEventAdmin','autoTitle','priceForContributors','eventRdf'),
						   ]);
}

//! Add hidden links to this event which show up when users copy paste (like wikipedia does)
function demosphere_event_hidden_links($body,$event)
{
	global $demosphere_config,$user;
	if(!val($demosphere_config,'hidden_event_links') || $user->checkRoles('admin','moderator')){return $body;}

	// use canonical url for links (not mobile)
	$url=$demosphere_config['std_base_url'].'/'.$demosphere_config['event_url_name'].'/'.intval($event->id);

	$hiddenLink='<p class="demosphere-source-link-top"><a href="'.ent($url).'">'.ent($url).'</a></p>';
	$firstP=strpos($body,'<p');
	$body=substr($body,0,$firstP).$hiddenLink.substr($body,$firstP);

	$hiddenSource='<span class="demosphere-source-link">'.t('Link : ').
		'<a href="'.ent($url).'">'.ent($url).'</a>'.
		'<br/></span>';
	// If there already is a demosphere-sources, add it inside first
	if(strpos($body,'demosphere-sources')!==false)
	{
		$body=preg_replace('@(class="[^"]*demosphere-sources[^>]*>)@','$1'.$hiddenSource,$body,1);
	}
	else
	{
		// Otherwise add a demosphere-sources after first closingBlock (this is a bit hackish)
		$addSrc='<p class="closingBlock demosphere-sources">'.$hiddenSource.'</p>';
		$body=preg_replace('@(<([a-z0-9]+)[^>]*class="[^"]*)\bclosingBlock\b(.*</\2>)@s','$1$3'.$addSrc,$body,1);
	}
	return $body;
}

//! Choose an image for Facebook share
//! The default Facebook algo is described here:
//! https://stackoverflow.com/questions/1138460/how-does-facebook-sharer-select-images-and-other-metadata-when-sharing-my-url
//! Our algo is similar, but more relaxed.
function demosphere_event_facebook_image($event)
{
	if(!preg_match_all('@<img [^>]*>@',$event->body,$matches)){return false;}
	$images=[];
	foreach($matches[0] as $n=>$match)
	{
		if(!preg_match_all('@([a-z]+)=[\'"]([^\'"]*)[\'"]@',$match,$attrMatches)){continue;}
		$attributes=[];
		foreach($attrMatches[1] as $na=>$attrName){$attributes[$attrName]=dlib_html_entities_decode_all($attrMatches[2][$na]);}
		$width =intval($attributes['width' ] ?? 0);
		$height=intval($attributes['height'] ?? 0);
		// ignore small images
		if($width <130  ){continue;}
		if($height<130/4){continue;}
		// ignore large aspect ratios
		if((max($width,$height)/min($width,$height))>4){continue;}
		
		// ignore strange image paths
		$src=$attributes['src'] ?? '';
		if(strpos($src,'/files/')!==0){continue;}
		$path=substr($src,1);
        if(!file_exists($path)){continue;}

		return $path;
	}
	return false;
}

function demosphere_event_tabs($event,$active)
{
	global $user,$base_url,$demosphere_config,$currentPage;
	$isEventAdmin=$user->checkRoles('admin','moderator');
	
	// https://example.org/event/add => no tabs
	if(!$event->id){return;}

	// If post is viewed in dbobject-ui context, use dbobject-ui tabs instead.
	if(isset($_GET['dbobject-ui']))
	{
		require_once 'dlib/dbobject-ui.php';
		dbobject_ui_tabs('Event',$event,'event-'.$active);
		return;
	}

	$tabs=[];
	// *** view, edit
	if($event->access('edit'))
	{
		$tabs['view']=['text'=>t('View'),'href'=>$event->url()];
		$tabs['edit']=['text'=>t('Edit'),'href'=>$event->url().'/edit'];
		// Editable pages should not be cached (example: selfEdit in session)
		demosphere_page_cache_is_cacheable(null,null);
	}


	// *** self edit repetition group
	require_once 'demosphere-self-edit.php';
	$isRepGroupSelfEdit=!$isEventAdmin && $event->repetitionGroupId && 
		demosphere_self_edit_check_group($event->repetitionGroupId);
	if($isRepGroupSelfEdit)
	{
		if($event->useRepetitionGroup()->ruleType==0)
		{
			$tabs['repetitions-self-edit']=['text'=>t('Repetitions'),
											'href'=>$base_url.'/repetition-group/'.$event->repetitionGroupId.'/self-edit?event='.$event->id
											];
		}
		else
		{
			$tabs['repetitions-confirm'  ]=['text'=>t('Repetitions'),
											'href'=>$base_url.'/repetition-group/'.$event->repetitionGroupId.'/confirm?event='.$event->id
											];
		}
	}

	// *** revisions and repetitions
	if($isEventAdmin)
	{
		$nbRevisions=db_result('SELECT count(*) FROM event_revisions WHERE event_id=%d',$event->id);
		if($nbRevisions)
		{
			$tabs['revisions']=['text'=>t('Revisions'),'href'=>$event->url().'/revisions'];
		}

		if($event->getModerationStatus()!=='trash')
		{
			$text=t('Repetitions');
			// Add number of repetitions to repetitions tab
			require_once 'demosphere-event-repetition.php';
			$repetitions=$event->getAllRepetitions();
			if(count($repetitions)>1){$text.=' ('.count($repetitions).')';}
			$tabs['repetition']=['text'=>$text,'href'=>$event->url().'/repetition'];

			// Add tab for repetition copy only if we are in repetition manager
			// FIXME: using $_GET['q'] is hackish
			if(count($repetitions)>1 && preg_match('@/[0-9]+/repetition@',$_GET['q'] ?? ''))
			{
				$tabs['repetition-copy']=['text'=>t('Repetition copy'),'href'=>$event->url().'/repetition-copy'];
			
			}
		}
	}
	if(count($tabs)===0){return;}
	if(isset($tabs[$active])){$tabs[$active]['active']=true;}
	$currentPage->tabs=$tabs;
}

function demosphere_event_old_url_redirect($nid,$edit=false)
{
	global $base_url,$demosphere_config;
	$map=db_array('SELECT id,is_event FROM migrate_nodes WHERE nid=%d',$nid);
	if($map===null){dlib_not_found_404();}
	if($map['is_event'])
	{
		$url=$base_url.'/'.$demosphere_config['event_url_name'].'/'.$map['id'];
	}
	else
	{
		$url=$base_url.'/post/'.$map['id'];
	}

	dlib_redirect($url,true);
}

//! Ajax call to get place and date/time suggestions
function demosphere_event_suggest_date_time_place()
{
	if(trim($_POST['address'] ?? '')!=='')
	{
		$searchText=$_POST['address'];
	}
	else
	{
		$searchText=dlib_fast_html_to_text($_POST['body']);
	}
	$isAnonymous=isset($_POST['anonymous']);
	$cleanBody=val($_POST,'body','');

	require_once 'demosphere-place-search.php';
	$placeSelector=demosphere_place_search_suggestions($searchText,$isAnonymous);

	// special case hack: remove "source" lines to avoid suggesting dates&times contained in them
	$cleanBody=preg_replace('@'.preg_quote(t('_source_'),'@').
							' *: *'.preg_quote(t('message received'),'@').'[^<\n]*@u','',$cleanBody);
	$cleanBody=preg_replace('@<p [^>]*title="[^"]*AltSource.*>@u','<p>',$cleanBody);
	$dateSelector   =demosphere_event_parsed_dates_selector($cleanBody);
	$timeSelector	=demosphere_event_parsed_times_selector($cleanBody);
	$res=['dateSelector'=>$dateSelector,
		  'timeSelector'=>$timeSelector,
		  'placeSelector'=>$placeSelector,
		 ];

	return $res;
}

/** Builds a select  option list that suggests  dates that  have been
 * parsed from contents.
 * @param html or text content which might contain dates.
 * @return html select option list for selecting found dates.
 */
function demosphere_event_parsed_dates_selector($content)
{
	global $user,$demosphere_config;

	// display a select box with dates found (parsed) in event body
	require_once 'dlib/find-date-time/find-date-time.php';
	require_once 'demosphere-date-time.php';
	$timestamps=array_unique(find_dates_and_times($content));
	$parsedDates='<select id="parsed-dates">';
	$parsedDates.= '<option value="" data-deprecated="">('.t('choose a date').')</option>';
	$count=0;

	foreach($timestamps as $ts)
	{
		if($ts < time()){continue;}// ignore dates in the past
		$parsedDates.= '<option value="'.date('Y-m-d\TH:i',$ts).'" '.
			'data-deprecated="'.demos_format_date('short-day-month-year',$ts).' '.str_replace('03:33',' ',strftime('%R',$ts)).'">'.
			demos_format_date('full-date-time',$ts).
			'</option>';
		$count++;
	}
	$parsedDates.='</select>';
	if($count===0){$parsedDates="";}
	return $parsedDates;
}

/** Builds a select  option list that suggests  times that  have been
 * parsed from contents.
 * @param html or text content which might contain times.
 * @return html select option list for selecting found times.
 */
function demosphere_event_parsed_times_selector($content)
{
	// display a select box with dates found (parsed) in event body
	require_once 'dlib/find-date-time/find-date-time.php';
	$times=array_unique(find_times($content,':'));
	$parsedTimes='<select id="parsed-times">';
	$parsedTimes.= '<option value="">('.t('choose time').')</option>';
	foreach($times as $ts)
	{
		$parsedTimes.= '<option value="'.$ts.'">'.$ts.'</option>';
	}
	$parsedTimes.='</select>';
	if(count($times)==0){$parsedTimes="";}
	return $parsedTimes;
}



// ***************************************************
// *************    text matching  *********************
// ***************************************************

// Which events are indexed:
// - all events (published or not) with no start_date OR with start_date > now - 1 month
function demosphere_event_text_matching_hook($op,$arg1=null,$arg2=null)
{
	global $demosphere_config,$base_url,$currentPage;
	// operations that d'ont need full $event
	switch($op)
	{
	case 'allTids':
		return db_one_col("SELECT id FROM Event WHERE moderationStatus!=%d AND (moderationStatus=%d OR startTime>%d )",
						  Event::moderationStatusEnum('trash'),
						  Event::moderationStatusEnum('created'),
						  time()-$demosphere_config['event_text_matching_keep_old']);
	case 'displayListInit':
		$currentPage->addJs('demosphere/dcomponent/dcomponent-dcitems.js');
		return;
	case 'isExpired':
		$eventData=db_array('SELECT startTime,moderationStatus FROM Event WHERE id=%d',$arg1);
		if($eventData===null){return true;}
		$mstatus=Event::$moderationStatus_['values'][$eventData['moderationStatus']];
		return $mstatus==='trash' ||
			($mstatus!=='created' && $eventData['startTime']<=(time()-$demosphere_config['event_text_matching_keep_old']));
	case 'matchInfo': 
		// Performance: this is called often, so don't load full event.
		$eventData=db_array('SELECT startTime,moderationStatus,status,needsAttention,title FROM Event WHERE id=%d',$arg1);
		if($eventData===null){return [];}
		$mstatus=Event::$moderationStatus_['values'][$eventData['moderationStatus']];

		// Check for same date
		$sameDate=false;
		$ts=intval($eventData['startTime']);
		if(is_array($arg2))
		{
			foreach($arg2 as $ts1)
			{if(date('z:Y',$ts)==date('z:Y',$ts1)){$sameDate=true;}}
		}

		require_once 'demosphere-date-time.php';
		return ['class'=>
					 ($eventData['status'] ? 'published ' : 'unpublished ').
					 ($eventData['startTime'] < time() ? 'old ' : 'recent ').
					 ($sameDate ? 'sameDateEvent ' : '').
					 'status-'.$mstatus.' '.
					 'nattention-level-'.(1+floor($eventData['needsAttention']/100)).' ',
				'title'=>demos_format_date('full-date-time',$eventData['startTime']).' :: '.$eventData['title'],
				'url'  =>$base_url.'/'.$demosphere_config['event_url_name'].'/'.intval($arg1),
			   ]; 
	}

	$event=Event::fetch($arg1,false);

	switch($op)
	{
	case 'getContents':
		if($event===null){return false;}
		return [$event->body,'html'];
	case 'displayInList': 
		if($event===null){echo '<p>event '.intval($arg1).' does not exist</p>';return;}

		$ref=null;
		if($arg2 instanceof TMDocument){$ref=$arg2;}
		$refEvent=null;
		if($ref!==null && $ref->type==='event'){$refEvent=Event::fetch($ref->tid,false);}
		require_once 'demosphere-date-time.php';
		echo template_render('text-matching/text-matching-similar-doc.tpl.php',
							 [
								 'content'=>$event->body,
								 'type'=>'event',
								 'typeId'=>$event->id,
								 'title'=>demos_format_date('short-date-time',$event->startTime).' : '.$event->title,
								 'titleUrl'=>$event->url(),
								 'titleRight'=>ent($event->usePlace()->useCity()->getShort()),
								 'classes'=>($refEvent!==null && date('z:Y',$event->startTime)==date('z:Y',$refEvent->startTime) ? 'sameDateEvent' :'' ).' '.
								            ($event->startTime < time() ? 'old ' : 'recent ').
								            'status-'.ent($event->getModerationStatus()).' '.
								            'nattention-level-'.(1+floor($event->needsAttention/100)),
								 'infoLeft'=>'<li class="status">'.ent(t($event->getModerationStatus())).'</li>',
							 ]
							);
		return;
	}
}


// ***************************************************
// 
// ***************************************************

//! used through ajax on frontpage
function demosphere_event_set_featured()
{
	global $base_url;
	dlib_check_form_token('demosphere_event_set_featured');
	$event=Event::fetch(intval($_POST['eid']));
	$value=(bool)($_POST['value'] ?? false);
	if(!$value){variable_delete($event->id,'fp-featured');}
	else       {variable_set   ($event->id,'fp-featured',true);}
	demosphere_page_cache_clear($base_url.'/');
	demosphere_page_cache_clear($base_url.'/?%');
	require_once 'demosphere-frontpage.php';
	$html=demosphere_frontpage_featured_events(false);
	return ['html'=>$html];
}

/**
 * Direct link to import an event using icalendar
 */
function demosphere_event_ical($id)
{
	$event=Event::fetch($id,false);
	if($event===null){dlib_not_found_404();}
	if(!$event->access('view')){dlib_permission_denied_403();}
	
	require_once 'demosphere-feeds.php';
	$calendar=demosphere_feeds_icalendar_object($event);
	$calendar->setMethod("REQUEST");
	header('Content-Type: text/calendar');
	echo $calendar->createCalendar();
}

//! Setup for class information displayed/edited by dbobject-ui
function demosphere_event_class_info(&$classInfo)
{
	$classInfo['tabs']=function(&$tabs,$event)
		{
			if($event)
			{
				$tabs['event-view']=['href'=>$event->url().     '?dbobject-ui','text'=>t('View')];
				$tabs['event-edit']=['href'=>$event->url().'/edit?dbobject-ui','text'=>t('Edit')];
				$tabs['view']['text']=t('Admin: view');
				$tabs['edit']['text']=t('Admin: edit');
			}
			unset($tabs['add']);
		};

	// Change urls in list so that view & edit links point to Event view & edit (not backend view & edit)
	$classInfo['list_action_url']=function($event,$action,$oldActionUrl)
		{
			if($action==='view'){return $event->url().     '?dbobject-ui';}
			if($action==='edit'){return $event->url().'/edit?dbobject-ui';}
			return $oldActionUrl($event,$action);
		};
}

//! $title is the *text* version of the title (not the HTML version)
function demosphere_event_title_length_is_ok($title,$city)
{
	global $demosphere_config;
	require_once 'dlib/html-tools.php';
	$maxCityWidth=89;
	$title=str_replace('_','',$title);
	$wTitle=dlib_html_estimate_display_width($title)/1.3757575757576;
	$cityId=City::findId($city);
	if($cityId!==false){$city=City::fetch($cityId)->getShort();}
	$wCity=dlib_html_estimate_display_width($city)/1.47075208913647743149;
	$wCity=min($maxCityWidth,$wCity);
	//vd($title,$wTitle,$city,$wCity,$wTitle+$wCity);die('pp');
	return $wTitle+$wCity<=($demosphere_config['event_title_max_width']+$maxCityWidth-15);
}

?>