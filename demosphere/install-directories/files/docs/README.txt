Directory for uploaded documents  and derived images.

Documents are automatically uploaded when links are used in Event/Post/... contents.
Derived images: pdf page icons, resized images

This directory was previously called "document-conversion".
An apache alias is necessary to access documents on old sites.

Do not delete these files.

FIXME:
Setup a procedure for checking which docs & images are no longer referenced by site contents.
Then delete unused docs & images.
