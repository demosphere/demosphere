<?php

// ****************************************************************
// **********  customizable topic colors and icons **
// ****************************************************************


//! Setup for class information for topics displayed/edited by dbobject-ui
function demosphere_topics_class_info(&$classInfo)
{
	global $currentPage,$base_url;
	// *********** common
	$currentPage->addJs('lib/jquery.js');
	$classInfo['field_desc']=
		['description'=>
		 ['description'=>t("Some text describing your topic. Currently this is only displayed as a tooltip on the frontpage topics (when the user leaves the mouse pointer a few seconds over a topic)."),
		 ],
		 'color'=>
		 ['description'=>t('You can either select a standard color or choose any color you want. Please note that most users will not be able to distinguish or remember many colors, so it is a good idea to limit the number of different colors.'),
		 ],
		 'icon'=>
		 ['description'=>t('You can either select a standard icon or provide a 13x18 image file (Example: files/images/my-icon.png). You must first put the file in the files/images/ directory.').t('You can add the file by using the !link file browser here</a>',['!link'=>'<a href="'.$base_url.'/browse/images">'])],
		 'weight'=>['description'=>t('Do not change manually. Instead, drag topics in topics list to change their order.')],
		 'frontpageLineBreak'=>['description'=>t("If you select this, the display of topics on the frontpage will start a new line after this topic.")],
		 'showOnEventPage'=>['description'=>t("Should this topic be shown on event pages. Usually you should select this. However, if you have a lot of topics, you might not want to display them all on event pages.")],
		 'showOnFrontpage'=>['description'=>t("Should this topic be shown on the front page. Usually you should select this. However, if you have a lot of topics, you might not want to display them all on the front page.")],
		];

	$classInfo['tabs']=function(&$tabs,$object)
		{
			$tabs['add']['text']=t('Add topic');
		};

	// *********** list
	$currentPage->addJs('lib/jquery-ui-1.12.1.custom/jquery-ui.js');
	$currentPage->addCss('lib/jquery-ui-1.12.1.custom/jquery-ui.min.css');
	$currentPage->addJs('demosphere/js/demosphere-topic-list.js');

	$classInfo['list_orderBy' ]='weight';
	$classInfo['list_orderDir']='ASC';
	$classInfo['list_noSort']=true;
	$classInfo['list_top_text']=t('You can change the order of topics by dragging them from one line to another.').
		dlib_add_form_token('topic-list');
	$classInfo['list_columns']=
		function(&$columns)
		{
			$columns['color']['show']=function($topic,&$show)
			{
				$color=$topic->color;
				if($color!=='')
				{$show['disp']='<span style="font-family: mono;border-left: 8px solid '.ent($color).'">'.ent($color).'</span>';}
			};
		};
	$classInfo['list_bottom_text']=
		'<p><a href="'.ent($base_url.'/configure/misc-config#topics_required').'">'.t('Configure whether specifying a topic in an event is required or not.').'</a></p>';

	// *********** edit form
	$currentPage->addJs('lib/jquery.js');
	$classInfo['edit_form_alter']=function(&$items,&$options)
	{
		$stdIcons =Topic::stdIcons();
		$stdColors=Topic::stdColors();

		//array_unshift($items,array('html'=>'iiiii'));
		$items['description']['type']='textarea';

		$items['color']['type']='color';
		$colorSel='<select onchange="$(\'#edit-color\').val($(this).val());$(\'#edit-color\').css(\'background-color\',$(this).val()===\'\' ? \'\' : \'#\'+$(this).val());">';
		$colorSel.='<option value="">(select)</option>';
		$colorSel.='<option value="">'.t('no color').'</option>';
		foreach($stdColors as $hex=>$colName){$colorSel.='<option value="'.$hex.'" style="background-color:#'.$hex.'">'.ent($colName).'</option>';}
		$colorSel.='</select>';
		$items['color']['field-suffix']=$colorSel;

		$iconSel='<select onchange="$(\'#edit-icon\').val($(this).val());">';
		$iconSel.='<option value="">(select)</option>';
		foreach($stdIcons as $name=>$file){$iconSel.='<option value="'.ent($file).'">'.ent($name).'</option>';}
		$iconSel.='</select>';
		$items['icon']['field-suffix']=$iconSel;
		$items['icon']['validate-regexp']='@^[a-z0-9._/-]+[.](png|jpg|jpeg|gif|svg)$@i';
		$items['icon']['validate-regexp-message']=t('Invalid icon image path name: use simple letters, numbers and one of these extensions: png,jpg,jpeg,gif,svg');
		if(val($items['color'],'default-value')!==''){$items['icon']['default-value']='';}
		$items['icon']['validate']=function($v) use ($stdIcons)
		{
			global $demosphere_config;
			return $v==='' || array_search($v,$stdIcons)!==false || file_exists($v) ? 
			true : t('Cannot find icon image file, are you sure you specified the full path? (files/images/example.png)');
		};

		$items['save']['validate']=function(&$items)
		{
			if($items['color']['value']!=='' && $items['icon']['value']!='')
			{$items['icon']['error']=t('You cannot use both a color and an icon, please leave either color or icon empty.');}
		};
		$items['save']['submit']='demosphere_topics_edit_submit';
	};
}


function demosphere_topics_edit_submit()
{
	demosphere_topics_create_color_images();
}

/** Computes images for color topics
 *
 * Two types of images are handled: 
 * 1) color icons (frontpage) 
 * 2) custom icons (frontpage)
 *
 * 1) is built from template image : frontpage-color-icon.png
 * 2) is copied from images uploaded by user to files/images/
 */
function demosphere_topics_create_color_images()
{
	global $demosphere_config;

	$topics=Topic::getAll();

	// *** build icons for each topic color
	foreach($topics as $tid=>$topic)
	{
		if($topic->color==''){continue;}

		$r=hexdec(substr($topic->color,1,2))/256;
		$g=hexdec(substr($topic->color,3,2))/256;
		$b=hexdec(substr($topic->color,5,2))/256;
		
		// build a temporary image from a template icon, by changing its color
		$topic->icon=Topic::$generatedIconDir.'/topic-icon-'.$tid.'.png';
		if(file_exists($topic->icon)){unlink($topic->icon);}
		dlib_logexec('convert -colorspace sRGB '.'demosphere/css/images/frontpage-color-icon.png  '.
					 '-channel red   -fx "'.$r.'" '.
					 '-channel green -fx "'.$g.'" '.
					 '-channel blue	 -fx "'.$b.'" '.
					 $topic->icon,false,
					 'icon processing: convert failed 1');
		$topic->save();
	}
}

//! Ajax call to reorder topics (change Topic::weight), used from topic list.
function demosphere_topics_list_order()
{
	dlib_check_form_token('topic-list');
	$order=$_POST['order'];
	foreach($order as $weight=>$id)
	{
		db_query('UPDATE Topic SET weight=%d WHERE id=%d',$weight,$id);
	}
}

//! Ajax call to get list of topics from remote site
function demosphere_topics_json()
{
	header('Access-Control-Allow-Origin: *');
	$topics=Topic::getAll();
	$res=[];
	foreach($topics as $id=>$topic)
	{
		$res[$id]=array_intersect_key((array)$topic,array_flip(['id','name','color','body','weight','icon']));
	}
	return $res;
}

?>