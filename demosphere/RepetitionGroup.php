<?php

//! A description of a recurring group of events.
class RepetitionGroup extends DBObject
{
	//! unique auto-incremented id
	public $id=0;
	static $id_                   =['type'=>'autoincrement'];
	
	//! The event in the group that is used as a "template" to create new events in this group.
	public $referenceId=0;
	static $referenceId_          =['type'=>'foreign_key','class'=>'Event'];

	//! Whether events are automatically created, and at what intervals.
	public $ruleType=0;
	static $ruleType_             =['type'=>'enum','values'=>[0=>'none',1=>'daily',2=>'weekly',3=>'semimonthly',4=>'monthly',5=>'dayofmonth']];
	//! ruleData values are different for each ruleType :
	public $ruleData=[];
	static $ruleData_             =['type'=>'array'];
	static $ruleFields=[
		'none'=>        [],
		'daily'=>       ['time'                                                                          ], 
		'weekly'=>      ['time', 'dayOfWeek' 												             ], 
		'semimonthly'=> ['time', 'dayOfWeek',               'weekInMonth1', 'weekInMonth2'	             ], 
		'monthly'=>     ['time', 'dayOfWeek', 'weekInMonth' 								             ],
		'dayofmonth'=>  ['time',                                                           'dayInMonth'  ] 
					   ];
	//! Start creating new events only after this date.
	public $ruleStart=0;
	static $ruleStart_            =['type'=>'timestamp'];
	//! Stop auto creating new events after this date.
	public $ruleEnd=0;
	static $ruleEnd_              =['type'=>'timestamp'];
	//! Auto repetitions are disabled if organizer doesn't confirm events after several reminders.
	public $autoIsEnabled=true;
	static $autoIsEnabled_          =['type'=>'bool'];
	//! This person receives emails asking him to confirm the next dates.
	public $organizerEmail='';
	static $organizerEmail_       =['type'=>'email'];
	//! How much time (in seconds) before the event should we send a request for confirmation.
	public $minConfirmPrior=0;
	static $minConfirmPrior_      =['type'=>'timestamp'];
	//! How much time (in seconds) before an event can a organizer confirm it.
	public $maxConfirmPrior=0;
	static $maxConfirmPrior_      =['type'=>'timestamp'];
	//! Log containing sent emails, confirmations, etc.
	public $log=[];
	static $log_                  =['type'=>'array'];

 	static $dboStatic=false;

	public function save()
	{
		$isNew=$this->id===0;
		parent::save();
		// When a new repetition is created, existing self-edits of all events of this group are upgraded from "event" to "group" self-edits.
		if($isNew && $this->referenceId)
		{
			db_query("UPDATE self_edit SET repetition_group=%d WHERE event=%d AND repetition_group=0",
			 		 $this->id,$this->referenceId);
		}
	}
	
	static function getRuleTypeLabels()
	{
		return [0=>t('None'),1=>t('Daily'),2=>t('Weekly'),3=>t('Semimonthly'),4=>t('Monthly'),5=>t('Day of month')];
	}

	//! Returns a list of all future dates for an automatic repetition. (max 1 year).
	//! This includes dates where there might already exist events.
	public function ruleDates()
	{
		$start=max(time(),$this->ruleStart);
		$end=$this->ruleEnd ? $this->ruleEnd : $start+3600*24*365;
		$res=demosphere_event_repetition_rule_dates($this->ruleType,$this->ruleData,$start,$end);
		//var_dump(array_map(function($v){return date("r",$v);},$res));
		return $res;
	}

	//! Returns a list of all future dates, where no event has been created yet.
	public function datesNotYetCreated()
	{
		static $periodStrs=['daily'      =>'%Y-%j',
							'weekly'     =>'%Y-%W',
							'semimonthly'=>'%Y-%W',
							'monthly'    =>'%Y-%m',
							'dayofmonth' =>'%Y-%m',
							];
		$periodStr=$periodStrs[self::$ruleType_['values'][$this->ruleType]];
		
		
		$ruleDates=$this->ruleDates();
		//var_dump(array_map(function($v){return date("r",$v);},$ruleDates));
		$createdIds=$this->getAllEventIds();
		if(count($createdIds)===0){$createdPeriods=[];}
		else
		{
			$createdPeriods=db_one_col('SELECT id,startTime FROM Event WHERE id IN ('.implode(',',$createdIds).') AND moderationStatus!=%d',
									Event::moderationStatusEnum('trash'));
			foreach($createdPeriods as &$c){$c=strftime($periodStr,$c);}
		}
		$createdPeriods=array_flip($createdPeriods);
		
		$res=[];
		foreach($ruleDates as $ruleDate)
		{
			$t=strftime($periodStr,$ruleDate);
			if(!isset($createdPeriods[$t])){$res[]=$ruleDate;}
		}
		return $res;
	}

	//! Creates future events for automatic repetitions that are sooner than maxConfirmPrior seconds from now.
	//! This should be called from cron.
	function autoCreateEvents()
	{
		if(!$this->autoIsEnabled){return;}
		$now=time();
		if(isset($GLOBALS['dbgtime'])){$now=$GLOBALS['dbgtime'];}
		// create uncreated repetitions
		$uncreated=$this->datesNotYetCreated();
		foreach($uncreated as $ts)
		{
			if($ts<$now){continue;}
			if($ts>$now+$this->maxConfirmPrior){continue;}
			$event=$this->createEvent($ts,['status'=>0,
									       'moderationStatus'=>Event::moderationStatusEnum('waiting'),
									       'moderationMessage'=>'automatic-repetition',
									      ]);
		}
	}

	//! Sends an email to event organizer requesting confirmation of some future events.
	//! This should be called from cron, after autoCreateEvents().
	function sendConfirmEmail()
	{
		global $demosphere_config,$base_url;
		if(!$this->autoIsEnabled){return false;}
		$now=time();
		if(isset($GLOBALS['dbgtime'])){$now=$GLOBALS['dbgtime'];}
		$events=Event::fetchList('WHERE repetitionGroupId=%d AND moderationStatus IN (%d,%d) AND startTime>%d ORDER BY startTime ASC',
								 $this->id,
								 Event::moderationStatusEnum('waiting'),
								 Event::moderationStatusEnum('incomplete'), 
								 $now
								 );
		if(count($events)===0){return false;}

		// only send if next event needing confirmation happens soon enough.
		if($now < dlib_first($events)->startTime-$this->minConfirmPrior){return false;}

		$logEmails  =array_filter($this->log,function($v){return $v['type']=='email';});
		$logConfirms=array_filter($this->log,function($v){return $v['type']=='confirm';});
		$lastConfirmK=dlib_last_key($logConfirms); // null if no confirm ever
		$logRestarts=array_filter($this->log,function($v){return $v['type']=='restart';});
		$lastRestartK=dlib_last_key($logRestarts); // null if no restart ever
		//echo '$lastConfirmK:';var_dump($lastConfirmK);
		//echo '$lastRestartK:';var_dump($lastRestartK);
		$lastConfirm=$lastConfirmK===null ? 0 : $logConfirms[$lastConfirmK]['ts'];
		$lastSentEmail=count($logEmails) ? dlib_last($logEmails)['ts'] : 0;
		$nbEmailsSinceConfirm=count(array_filter(array_keys($logEmails),
				function($v)use($lastConfirmK,$lastRestartK){return $v>=$lastConfirmK && $v>=$lastRestartK;}));
		//var_dump('$nbEmailsSinceConfirm',$nbEmailsSinceConfirm);

		// Wait at least minConfirmPrior before we resend an email (ie. wait until after first event).
		if($now<$lastSentEmail+$this->minConfirmPrior+3600*24){return false;}
		// unresponsive organizer: one mail missed. Only send after $waitMissed
		$waitMissed=['daily'      =>3600*24*4,
					 'weekly'     =>3600*24*7,
					 'semimonthly'=>3600*24*14,
					 'monthly'    =>3600*24*30,
					 'dayofmonth' =>3600*24*30,
					];
		if($nbEmailsSinceConfirm===1 && $now<$lastSentEmail+$waitMissed[$this->getRuleType()]){return false;}
		// unresponsive organizer: two mails missed. Wait for $waitMissed 
		if($nbEmailsSinceConfirm===2 && $now<$lastSentEmail+$waitMissed[$this->getRuleType()]){return false;}
		// unresponsive organizer: 2 mails missed. We are after 2*maxConfirmPrior => stop sending.
		if($nbEmailsSinceConfirm>=2)
		{
			$nbDays=round(($now-$lastConfirm)/(24*3600));
			$this->addLog(['type'=>'stop','email'=>$this->organizerEmail,'delay'=>$lastConfirm ? $now-$lastConfirm : false]);
			$this->autoIsEnabled=false;
			$this->save();

			// Tell moderators that there is a problem.
			$subject=mb_substr(($this->useReference()->title),0,100);
			$body=t("The automatic repetitions for the following event have been disabled:\n!title\nThe organizer !email has not confirmed after two emails were sent.\nMore information here:\n!url\n",
					['!title'=>$this->useReference()->title,
					 '!email'=>$this->organizerEmail,
					 '!url'=>$this->useReference()->url().'/repetition',]);
			require_once 'demosphere-emails.php';
			demosphere_emails_notification_send('repetition-disabled',$subject,$body);
			return false;
		}

		require_once 'demosphere-date-time.php';
		$eventList='';
		foreach($events as $event)
		{
			$eventList.=demos_format_date('short-date-time',$event->startTime).'  ::  ';
			$eventList.=$event->title;
			$eventList.="\n";
		}

		$editUrl=$base_url.'/repetition-group/'.$this->id.'/confirm';
		require_once 'demosphere-self-edit.php';
		$selfEdit=demosphere_self_edit_create($this->organizerEmail,false,$this->id);

		$emailSubject=$demosphere_config['auto_repetition_confirm_email_subject'];
		$emailBody   =$demosphere_config['auto_repetition_confirm_email_body'];
		foreach(['Subject','Body'] as $where)
		{
			$n='email'.$where;
			$text=&$$n;
			$text=str_replace('%site_name',$demosphere_config['site_name']              ,$text);
			$text=str_replace('%event-list',$eventList                                  ,$text);
			$text=str_replace('%self-edit-link',$editUrl.'?selfEdit=%self_edit_password',$text);
			$text=str_replace('%self_edit_password',$selfEdit['password']               ,$text);
			$text=str_replace('%sitename' ,$demosphere_config['site_name']              ,$text);
			$text=str_replace('%site_name',$demosphere_config['site_name']              ,$text);
			$text=str_replace('%event_id',$this->referenceId                            ,$text);
			$text=str_replace('%title',$this->useReference()->title                     ,$text);
			//echo $text;
		}

		require_once 'lib/class.phpmailer.php';
		$mail=new PHPMailer(false);
		$mail->CharSet="utf-8";
		$mail->SetFrom($demosphere_config['contact_email']);
		$mail->AddAddress($this->organizerEmail);
		$mail->Subject=$emailSubject;
		$mail->Body=$emailBody;
		if($mail->Send())
		{
			$this->addLog(['type'=>'email','email'=>$this->organizerEmail,'events'=>array_keys($events)]);
			$this->save();
			return true;
		}
		else
		{
			$this->addLog(['type'=>'sendfailed','email'=>$this->organizerEmail]);
			$this->save();
			return false;
		}
	}

	function addLog($line)
	{
		global $user;
		$now=time();
		if(isset($GLOBALS['dbgtime'])){$now=$GLOBALS['dbgtime'];}
		$line['ts']=$now;
		$line['uid']=$user->id;
		$this->log[]=$line;
	}

	//! Tell this group that an event has been removed.
	//! referenceId will be updated if needed.
	//! Warning: this function may save or delete this RepetitionGroup
	function eventHasBeenRemoved($eventId)
	{
		// If the removed event is the reference, we need to set a new reference
		if($this->referenceId==$eventId)
		{
			// Find first event that is in this RepetitionGroup
			$repetitions=$this->getAllEventIds(false);
			if(count($repetitions)>0)
			{
				$this->referenceId=$repetitions[0];
				$this->save();
			}
			else
			{
				$this->delete();
			}
		}
	}

	//! Create an event using the reference (template) for this RepetitionGroup.
	//! This is used both for auto or manual repetitions.
	function createEvent($date,$custom=[])
	{
		$refEvent=$this->useReference();
		$topics=$refEvent->getTopicNames();
		$terms =$refEvent->getAllTerms();

		$newEvent=clone $refEvent;
		$newEvent->repetitionGroupId=$this->id;
		$newEvent->startTime=$date;
		$newEvent->dayrank=0;
		$newEvent->visits =0;
		$newEvent->created =time();
		$newEvent->changed =$newEvent->created;
		$newEvent->lastBigChange =$newEvent->created;
		foreach($custom as $field=>$value){$newEvent->$field=$value;}
		$newEvent->save();
		$newEvent->setTopics(array_keys($topics));
		$newEvent->setTerms(array_keys($terms));
		return $newEvent;
	}

	//! Returns a list of all ids of events in this RepetitionGroup (not in trash).
	function getAllEventIds($includeRef=true)
	{
		return db_one_col('SELECT id FROM Event WHERE repetitionGroupId=%d '.
						  ($includeRef   ? '' : 'AND id!='.intval($this->referenceId).' ' ).
						  'AND moderationStatus!=%d '.
						  'ORDER BY startTime ASC',
						  $this->id,Event::moderationStatusEnum('trash'));
	}

	static function createTable()
	{
		db_query("DROP TABLE IF EXISTS RepetitionGroup");
		db_query("CREATE TABLE RepetitionGroup (
  `id` int(11) NOT NULL auto_increment,
  `referenceId` int(11) NOT NULL,
  `ruleType` int(11) NOT NULL,
  `ruleData` longblob NOT NULL,
  `ruleStart` int(11) NOT NULL,
  `ruleEnd` int(11) NOT NULL,
  `autoIsEnabled` int(1) NOT NULL,
  `organizerEmail` varchar(330)  NOT NULL,
  `minConfirmPrior` int(11) NOT NULL,
  `maxConfirmPrior` int(11) NOT NULL,
  `log` longblob NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `referenceId` (`referenceId`),
  KEY `ruleType` (`ruleType`),
  KEY `autoIsEnabled` (`autoIsEnabled`),
  KEY `minConfirmPrior` (`minConfirmPrior`),
  KEY `maxConfirmPrior` (`maxConfirmPrior`),
  KEY `ruleStart` (`ruleStart`),
  KEY `ruleEnd` (`ruleEnd`)
) DEFAULT CHARSET=utf8mb4");

	}

}
?>