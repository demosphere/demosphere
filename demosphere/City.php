<?php

//! A very small class for a city name (and sometimes, a shortened name).
class City extends DBObject
{
	//! cityId=1 is used for "no city"
	public $id=0;
	static $id_           =['type'=>'autoincrement'];
	public $name='';
	static $name_         =[];
	public $shortName='';
	static $shortName_    =[];

 	static $dboStatic=false;

	function __construct($name=null)
	{
		$this->name=$name;
	}

	function delete()
	{
		if($this->id===1){fatal('Deleting city that has id=1 is not allowed. This city is used as "No City".');}
		db_query("UPDATE Place SET cityId=1 WHERE cityId=%d",$this->id);

		parent::delete();
	}

	function getShort()
	{
		return $this->shortName!='' ? $this->shortName:
			$this->name;
	}

	static function findOrCreate($cityName)
	{
		$cityName=trim(dlib_cleanup_plain_text($cityName,false));
		$existingId=City::findId($cityName);
		if(!$existingId){return new City($cityName);}
		else            {return City::fetch($existingId);}
	}
	// returns false on fail
	static function findId($cityName)
	{
		$cityName=trim(dlib_cleanup_plain_text($cityName,false));
		$id=db_result_check("SELECT id FROM City WHERE LOWER(name) = LOWER('%s')",$cityName);
		return $id;
	}

	static function cleanupUnusedCities()
	{
		db_query("DELETE FROM City WHERE NOT EXISTS (SELECT * FROM Place WHERE Place.cityId=City.id)");
	}
	// 	if($id===false && $create!==false)
	// 	{
	// 		$city=new City($cityName);
	// 		$city->save();
	// 		$id=$city->id;
	// 	}
	// 	return $id;

	static function createTable()
	{
		db_query("DROP TABLE IF EXISTS City");
		db_query("CREATE TABLE City (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(250) NOT NULL,
  `shortName` varchar(250) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `name` (`name`),
  KEY `shortName` (`shortName`)
) DEFAULT CHARSET=utf8mb4");
	}

}
?>
