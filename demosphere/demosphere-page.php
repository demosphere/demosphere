<?php

/**
 * Renders a standard demosphere html page. 
 * This function is used for most pages. Notable exceptions are: frontpage, html ajax snippets...
 * It is set as the default_output_render option for dlib_paths_exec(). 
 * It is called automatically when no 'output'=>... is specified in path defined in demosphere_paths() 
 * Rendering is done with demosphere-page.tpl.php
 *
 * @param the main html content for this page
 * @return Entire html page.
 */
function demosphere_page_render($content)
{
	global $user,$base_url,$demosphere_config,$currentPage;
	$isLoggedIn=$user->id!=0;
	$isEventAdmin=$user->checkRoles('admin','moderator');

	$isMobile=demosphere_is_mobile();
	$currentPage->bodyClasses[]=$isMobile ? 'mobile' : 'not-mobile';
	if(!empty($currentPage->tabs)){$currentPage->bodyClasses[]='has-tabs';}

	require_once 'demosphere-common.php';

	// ***** add support for help (top right blue question mark) for non admins
	$nonAdminHelpButton='';
	if(!$isEventAdmin)
	{
		require_once 'demosphere-help.php';
		$nonAdminHelpButton=val(demosphere_help_buttons(true),'help'  ,'');
	}

	// common CSS
	if($isEventAdmin){$currentPage->addCssTpl("demosphere/css/demosphere-admin.tpl.css");}
	$currentPage->addCssTpl('demosphere/css/demosphere-common.tpl.css');
	$currentPage->addCssTpl("demosphere/css/demosphere-page.tpl.css");
	if($isMobile)
	{
		$currentPage->addCssTpl('demosphere/css/demosphere-page-mobile.tpl.css');
		$currentPage->addCssTpl('demosphere/css/demosphere-mobile-common.tpl.css');
	}

	// common inline JS
	$currentPage->addJsConfig('demosphere',['std_base_url','files','locale']);
	$currentPage->addJs(demosphere_js_shorten(file_get_contents('demosphere/js/add-script.js')),'inline',['prepend'=>true]);
	$bodyJavascript=demosphere_browser_detect_css_javascript();
	foreach($currentPage->js as $desc)
	{
		if($desc['location']==='inline-body'){$bodyJavascript.='<script type="text/javascript">'.$desc['js'].'</script>'."\n";}
	}
	if($isMobile){$currentPage->addJs(file_get_contents('demosphere/js/demosphere-mobile-common.js'),'inline');}

	$siteName  =$demosphere_config['site_name'];


	// Change the top of page for usercalendars, so that visitors will return to 
	// usercal when clicking on the main logo (instead of returning to demosphere homepage)
	$userCalUid=val($_GET,'userCalendar',false);
	if(!$userCalUid && isset($currentPage->pageTopUserCalUid))
	{
		$userCalUid=$currentPage->pageTopUserCalUid;
	}
	$calLinkName=false;
	if($userCalUid!==false)
	{
		$userCalUid=intval($userCalUid);
		$userCalUser=User::fetch($userCalUid,false);
		if($userCalUser===null){dlib_not_found_404();}
		$userCalOptions=val($userCalUser->data,'calendar',[]);
		$calLinkName=t('Back to @username\'s calendar',['@username'=>$userCalUser->login]);
		if(isset($userCalOptions['title']) && trim($userCalOptions['title'])!=='')
		{
			$calLinkName=$userCalOptions['title'];
		}
	}

	// ******** simplify interface for admin and mobile
	if(!isset($currentPage->showPageTopForEventAdmin)){$currentPage->showPageTopForEventAdmin=false;}
	$showPageTop=!$isMobile && (!$isEventAdmin || $currentPage->showPageTopForEventAdmin || $userCalUid!==false);
	$currentPage->bodyClasses[]=$isEventAdmin ? 'eventAdmin' : 'notEventAdmin';

	// ******** menus : 

	$eventAdminMenubar='';
	$secondary_links=demosphere_secondary_links();
	if($isEventAdmin)
	{
		$eventAdminMenubar=demosphere_event_admin_menubar();
	}

	// ******** Centered, fixed-width display is default for non admin and is disabled on mobile 
	if($isMobile && array_search('notFullWidth',$currentPage->bodyClasses)!==false)
	{
		$currentPage->bodyClasses=array_diff($currentPage->bodyClasses,['notFullWidth']);
	}
	if(array_search('fullWidth'   ,$currentPage->bodyClasses)===false && 
	   array_search('notFullWidth',$currentPage->bodyClasses)===false   )
	{
		$currentPage->bodyClasses[]=$isEventAdmin || $isMobile ? 'fullWidth' : 'notFullWidth';
	}

	$currentPage->bodyClasses[]=$demosphere_config['extra_body_class'];

	$lang=dlib_get_locale_name();

	$messages=dlib_message_display();
	if($messages!=='')
	{
		demosphere_page_cache_is_cacheable(null,null);
		$currentPage->bodyClasses[]='has-messages';
	}


	// FIXMEd kinimatorama
	//if(function_exists('custom_demos_preprocess_page'))
	//{
	// 	custom_demos_preprocess_page($variables);
	//}

	require_once 'dlib/template.php';
	return template_render('templates/demosphere-page.tpl.php',
						   [$currentPage->variables($demosphere_config['compress_css']),
							compact('lang','eventAdminMenubar','bodyJavascript','siteName','content','messages','isEventAdmin','secondary_links','isMobile','showPageTop','nonAdminHelpButton','userCalUid','calLinkName'),
						   ]);
}


function demosphere_secondary_links($isFrontPage=false)
{
	global $user,$base_url,$demosphere_config;
	$links=$demosphere_config['secondary_links'];
	$role=$user->getRole();
	$allRoles=User::$role_['values'];
	$res='<ul id="secondary-links">';
	foreach($links as $link)
	{
		// *** Check "where" conditions
		$where=explode(',',$link['where']);
		if($where===['']){$where=[];}
		$negated=preg_replace('@^not:@','',preg_grep('@^not:@',$where));
		$where  =preg_grep('@^not:@',$where,PREG_GREP_INVERT);

		// Frontpage 
		$frontPageRule=null;
		if(array_search('frontpage',$where  )!==false){$frontPageRule=true;}
		if(array_search('frontpage',$negated)!==false){$frontPageRule=false;}
		if($frontPageRule!==null && $isFrontPage!==$frontPageRule){continue;}

		// isLoggedin
		$isLoggedin=$user->id!=0;
		$loggedinRule=null;
		if(array_search('loggedin',$where  )!==false){$loggedinRule=true;}
		if(array_search('loggedin',$negated)!==false){$loggedinRule=false;}
		if($loggedinRule!==null && $isLoggedin!==$loggedinRule){continue;}
		
		// Roles
		if($isLoggedin)
		{
			if(array_search($role,$negated)!==false){continue;}
			if(array_search($role,$where  )===false && count(array_intersect($where,$allRoles))>0){continue;}
		}
		else
		{
			// any role-related rules imply $isLoggedin
			if((count(array_intersect($where  ,$allRoles))>0 ||
				count(array_intersect($negated,$allRoles))>0  )){continue;}
		}

		// *** Display link
		$url=$link['url'];
		$url=preg_replace('@^/event/@','/'.$demosphere_config['event_url_name'].'/',$url);
		$url=preg_replace('@^/@',$base_url.'/',$url);
		$url=str_replace('[userid]',$user->id,$url);
		$res.='<li '.dlib_render_html_attributes(array_intersect_key($link,['id'=>true,'class'=>true])).'>'.
			'<a href="'.ent($url).'">'.ent($link['title']).'</a></li>';
	}
	$res.='</ul>';

	return $res;
}

// Creates classes  for different browers, so that  certain css styles
// can be made  browser specific. This doesn't work  if js is disabled
// :-( This script  should be included right after  the <body> opening
// tag.
function demosphere_browser_detect_css_javascript()
{
	return <<<EOF
<!--[if IE ]>
<script type="text/javascript">
document.body.className+=" IE IE"+navigator.appVersion.match(/MSIE ([0-9]+)/i)[1];
</script>
<![endif]-->
EOF;
}

?>