<?php

//! Returns default values for user calendar options.
function demosphere_user_calendar_defaults($cuser)
{
	return ['title'=>t('@calUser\'s calendar',['@calUser'=>$cuser->login]),
			'ispublic'=>true];
}

/**
 * Builds the form that allows users to choose set options for their calendar.
 * Note that this form is never actually submited but instead uses ajax for updating.
 * This function just adds a few items to the items it gets from demosphere_widget_config_form().
 */
function demosphere_user_calendar_config_form()
{
	global $user,$base_url,$currentPage,$demosphere_config;
	if(!isset($_GET['uid'])){dlib_bad_request_400("missing uid");}
	$uid=intval($_GET['uid']);
	if($user->id!=$uid && !$user->checkRoles('admin')){dlib_permission_denied_403();}
	$cuser=User::fetch($uid,false);
	if($cuser===null){dlib_not_found_404();}

	$currentPage->addJs( 'lib/jquery.js');
	$currentPage->addJs( "demosphere/js/demosphere-user-calendar-config-form.js");

	$options=demosphere_user_calendar_defaults($cuser);
	$options=array_merge($options,val($cuser->data,'calendar',[]));
	
	require_once 'demosphere-widget.php';

	$uform=[];
	$uform['form-title']=
		['html'=>'<h1 class="page-title">'.t('Configure and select events for your personal calendar').
		 ($uid!=$user->id ? ' (user: '.ent($cuser->id).' "'.ent($cuser->login).'" )':'').
		 '</h1>'.
		 '<p>'.
		 '<a target="_blank" href="'.$base_url.'/usercal/'.intval($cuser->id).'">'.
		 t('View the calendar on @siteName.',
		   ['@siteName'=>$demosphere_config['site_name']]).'</a> &nbsp; '.
		 '<a href="'.$base_url.'/widget-config?uid='.intval($cuser->id).'">'.
		 t('Display the calendar on your own web page (widget)').'</a>'.
		 '</p>'];
	$uform['title'     ]=
		['type'=>'textfield',
		 'title'=>t('Title of your calendar'),
		 'default-value'=>$options['title'],
		];
	$uform['ispublic'  ]=
		['type'=>'checkbox',
		 'title'=>t('this calendar is publicly visible'),
		 'default-value'=>$options['ispublic'],
		];

	$form=demosphere_widget_config_form(true);
	$currentPage->title=t('User calendar configuration');
	unset($form['title']);
	unset($form['head']);
	unset($form['embed']);
	unset($form['embed-advanced']);
	unset($form['example']);
	// remove all form eelements between 'display' and 'display-end'
	array_splice($form,dlib_key_pos($form,'display'),1+dlib_key_pos($form,'display-end')-dlib_key_pos($form,'display'));

	$form=array_merge($uform,$form);

	require_once 'dlib/form.php';
	$formOpts=['id'=>'demosphere_user_calendar_config_form'];
	return form_process($form,$formOpts);
}

//! This is called when a user selects or de-selects an individual event 
//! in both user cal and widget config.
function demosphere_user_calendar_config_ajax()
{
	// CSRF protection using form tokens
	if(val($_POST,'form_token')!==
	   dlib_get_form_token('demosphere_user_calendar_config_form')){dlib_bad_request_400('invalid form token (u)');}

	global $user;
	$uid=intval(val($_GET,'uid'));
	if($user->id!=$uid && !$user->checkRoles('admin')){dlib_permission_denied_403();}
	$cuser=User::fetch($uid,false);
	if($cuser===null){dlib_not_found_404();}

	$options=val($cuser->data,'calendar',[]);
	$errors=[];

	$title=trim(val($_POST,'title',''));
	if($title==''){$errors['title']='title cannot be empty';}
	else{$options['title']=$title;}

	$isPublic=val($_POST,'ispublic')==='true';
	$options['ispublic']=$isPublic;
	if(count($errors)===0)
	{
		global $base_url;
		$cuser->data['calendar']=$options;
		$cuser->save();

		demosphere_page_cache_clear('%/widget-html?uid='.$cuser->id.'%');
		demosphere_page_cache_clear('%/widget-js?uid='.  $cuser->id.'%');
	}
	return $errors;
}


/** 
 * Return an html string for displaying the banner of a user's personal calendar.
 */
function demosphere_user_calendar_sitebanner($uid)
{
	global $base_url,$user,$demosphere_config,$currentPage;	
	require_once 'dlib/filter-xss.php';

	$userCalUser=User::fetch($uid);
	$siteName  =$demosphere_config['site_name'];
	$link=val(val($userCalUser->data,'calendar',[]),'link','');
	// FIXME is $link used anymore ? why not?

	$calData=val($userCalUser->data,'calendar',[]);
	$defaultTitle=t('@calUser\'s calendar',['@calUser'=>$userCalUser->login]);
	$title=val($calData,'title',$defaultTitle);
	$currentPage->title=$title;

	$siteBanner='<div id="userCalSiteName">'.
		'<p><a href="'.$base_url.'">'.t('@siteName Home',['@siteName'=>$siteName]).'</a></p>'.
		'<h1 class="siteName">'.
		($link=='' ? '' : '<a href="'.filter_xss_check_url($link).'">').
		ent($title).
		($link=='' ? '' : '</a>');
	if($link!='')
	{
		$siteBanner.='<br/><a id="userCalLink" '.
			'href="'.filter_xss_check_url($link).'">'.ent($link).
			'</a>';
	}
	$siteBanner.='</h1>';
	if($user->id==intval($uid) || $user->checkRoles('admin'))
	{
		$siteBanner.=
			t('<a href="!url1">Configure</a> and <a href="!url2">select events</a> for '.
			  'this personal calendar',
			  ['!url1'=>$base_url.'/user-calendar-config?uid='.$uid,
			   '!url2'=>$base_url.'/user-calendar-config?uid='.$uid.'#select']).'</a>';
	}
	$siteBanner.='</div>';
	
	return $siteBanner;
}

//! Ajax call to select/unselect an event in user calendar.
function demosphere_user_calendar_select()
{
	global $user,$base_url,$demosphere_config;
	if($user->id==0){dlib_permission_denied_403("not logged in",false);}
	if(val($_POST,'form_token')!==dlib_get_form_token('demosphere_widget_config_form') &&
	   val($_POST,'form_token')!==dlib_get_form_token('demosphere_user_calendar_config_form'))
	{dlib_permission_denied_403('demosphere_user_calendar_select: invalid form token',false);}

	$eid=intval($_POST['eid']);
	$userCalUid=intval($_POST['userCalUid']);
	$yes=$_POST['yes']==='true';
	$no =$_POST['no' ]==='true';

	if($user->id!=$userCalUid && !$user->checkRoles('admin'))
	{
		dlib_permission_denied_403();
	}

	if($yes && $no){dlib_bad_request_400('demosphere_user_calendar_select: yes and no! argh. failed.');}

	// clear caches
	demosphere_page_cache_clear('%/usercal/'.$userCalUid.'%');

	$ok=demosphere_user_calendar_set_select($eid,$userCalUid,$yes ? 1 : ($no ? 0 : false));
	echo $ok? "ok" : "failed";
}
   
function demosphere_user_calendar_set_select($eid,$uid,$value)
{
	if($value!==false && $value!==1 && $value!==0){fatal("user cal set select:bad value");}

	if($value===false)
	{
		// remove 
		$r=db_query("DELETE FROM user_calendar_selection ".
					"WHERE event=%d AND user=%d",$eid,$uid);
		return $r!==false;
	}
	$r=db_query("REPLACE INTO user_calendar_selection ".
				"(user,event,selected) VALUES (%d,%d,%d)",
				$uid,$eid,$value);
	return $r!==false;
}

/**
 * Add an "external" event.
 *
 * This is called directly from a request.
 * It is used through an ajax interaction from the user cal or widget config forms.
 *
 * External events are ordinary Event objects. When a non admin 
 * user that visits them, he is automatically redirected to a user supplied (external) url.
 */
function demosphere_user_calendar_external_event()
{
	global $user,$base_url,$demosphere_config;

	// CSRF protection using form tokens
	if(val($_POST,'form_token')!==dlib_get_form_token('demosphere_widget_config_form') &&
	   val($_POST,'form_token')!==dlib_get_form_token('demosphere_user_calendar_config_form')    )
	{dlib_permission_denied_403('invalid form token (x)',false);}

	// setup widget user and check permissions
	if($user->id==0){dlib_permission_denied_403("not logged in",false);}
	if(!isset($_GET['uid'])){dlib_bad_request_400("uid not specified");}
	$widgetUid=intval($_GET['uid']);
	$wuser=User::fetch($widgetUid,false);
	if($wuser===null || $user->id==0 || 
	   !($wuser->id===$user->id || $user->checkRoles('admin')) )
	{dlib_permission_denied_403(false,false);}

	switch($_POST['action'])
	{
	case 'add':
		require_once 'demosphere-date-time.php';
		require_once 'dlib/filter-xss.php';

		$startTime=demosphere_date_time_parse_short($_POST['date'],$_POST['time']);
		if($startTime<time())
		{
			echo t('Invalid date or time. Example: !example',
									 ['!example'=>demos_format_date('short-day-month-year',time()).' 20:00']);
			break;
		}
		if(!preg_match('@^https?://@',$_POST['url'])){echo "bad url";break;}

		$event=new Event();

		$event->title			=$_POST['title'];
		$event->htmlTitle		=filter_xss($_POST['title'],['strong']);
		$event->startTime		=$startTime;
		$event->externalSourceId=1;// 1 is reserved for manually created external events
		$event->extraData['external']['url']=$_POST['url'];
		$event->body			    ='<p>'.t('External:').' '.filter_xss_check_url($_POST['url']).'</p>';
		$event->authorId=$wuser->id;
		$event->showOnFrontpage=0;
		$event->status =1;
		$event->setModerationStatus('published');
		$event->needsAttention=0;
		$place=new Place();
		$place->setCity(City::findOrCreate($_POST['city']));
		$event->setPlace($place);
		$event->save();
		// auto-select this external event for this user cal
		demosphere_user_calendar_set_select($event->id,$wuser->id,1);
		echo 'add ok';		
		break;
	default: dlib_bad_request_400('invalid action');
	}
}

function demosphere_user_calendar_install()
{
	db_query("DROP TABLE IF EXISTS user_calendar_selection");
	db_query("CREATE TABLE user_calendar_selection (
  `user` int(10) NOT NULL,
  `event` int(10) NOT NULL,
  `selected` tinyint(1) NOT NULL,
  PRIMARY KEY  (`user`,`event`),
  KEY `user` (`user`),
  KEY `event` (`event`),
  KEY `selected` (`selected`)
) DEFAULT CHARSET=utf8mb4");

}

?>