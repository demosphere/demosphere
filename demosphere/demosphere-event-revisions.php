<?php

function demosphere_event_revisions($eid)
{
	global $base_url,$currentPage;
	$event=Event::fetch($eid,false);
	if($event===null){dlib_not_found_404();}

	$currentPage->title=t('Revisions: ').$event->title;
	$out='';
	require_once 'demosphere-event.php';
	$currentPage->addCssTpl('demosphere/css/demosphere-event-revisions.tpl.css');
	demosphere_event_tabs($event,'revisions');
	$revisions=db_arrays('SELECT * FROM event_revisions WHERE event_id=%d ORDER BY id DESC',$event->id);
	require_once 'demosphere-date-time.php';
	return template_render('templates/demosphere-event-revisions.tpl.php',
						   [compact('event','revisions')]);
	return $out;
}

//! Creates a revision from an event that already exists in the database.
//! You should call this *before* saving an event with changes, so that the old existing event is archived and can be restored.
function demosphere_event_revisions_create($eid)
{
	$event=Event::fetch($eid);
	$place=Place::fetch($event->placeId);
	$data=['place' =>$place,
		   'city'	=>City::fetch($place->cityId,false),
		   'topics'=>$event->getTopics(),
		  ];
	$lastLog=demosphere_event_revisions_last_log($event);
	db_query("INSERT INTO event_revisions (event_id,changed_by_id,changed,last_log,event,data) VALUES (%d,%d,%d,'%s','%s','%s')",
			 $event->id,$event->changedById,$event->changed,$lastLog,serialize($event),serialize($data));
}

function demosphere_event_revisions_last_log($event)
{
	if(count($event->log)===0){return '';}
	return dlib_last($event->log)['message'];
}
function demosphere_event_revisions_view($rid)
{
	global $base_url,$demosphere_config,$currentPage;

	$revision=db_array('SELECT * FROM event_revisions WHERE id=%d',$rid);
	$current=Event::fetch($revision['event_id'],false);
	if($current===null){dlib_not_found_404();}

	$other=unserialize($revision['event'],["allowed_classes" => ['Event']]);
	$data =unserialize($revision['data' ],["allowed_classes" => ['Place','City','Topic']]);
	$other->setPlace($data['place']);
	if($data['city']!==null){$other->usePlace()->setCity($data['city']);}
	// topics
	$topics=$data['topics'];
	$topics=array_filter($topics,function($t){return $t->showOnEventPage;});

	require_once 'demosphere-event.php';
	$out='<div id="revision-label">'.t('Revision !rid',['!rid'=>$rid]).'</div>';
	$out.=demosphere_event_render($other,['topics'=>$topics]);

	$currentPage->title=t('Revision: ').$current->title;
	return $out;
}

//! Display side-by-side editable diffs between current version of an event and a revision
//! This calls demosphere_event_hdiff()
function demosphere_event_revisions_hdiff($rid)
{
	global $base_url,$demosphere_config,$currentPage;

	$revision=db_array('SELECT * FROM event_revisions WHERE id=%d',$rid);
	$current=Event::fetch($revision['event_id'],false);
	if($current===null){dlib_not_found_404();}

	$currentPage->title=t('Diff: ').$current->title;

	$other=unserialize($revision['event'],["allowed_classes" => ['Event']]);
	$data =unserialize($revision['data' ],["allowed_classes" => ['Place','City','Topic']]);
	require_once 'demosphere-event.php';
	demosphere_event_tabs($current,'revisions');

	require_once 'dlib/filter-xss.php';
	require_once 'demosphere-date-time.php';

	$hdOptions=[];

 	$out='<h1 class="page-title">Diff:</h1>';
	for($j=0;$j<2;$j++)
	{
		$event=($j==0 ? $current : $other);

		if($j==1)
		{
			$event->setPlace($data['place']);
			$event->usePlace()->setCity($data['city']);
		}

		$title='';
		$title.='<a target="_blank" href="'.
			ent(($j==0? $event->url() : $base_url.'/event-revision/'.$revision['id'])).'">';
		$nt='t';
		if($j==0){$title.=ent(t('current revision')).' ';}
		else
		{
			$title.=ent($nt('revision').' : '.$revision['id']);
		}
		$changedById=$j===0 ? $event->changedById : $revision['changed_by_id'];
		$changedBy=$changedById ? db_result_check('SELECT login FROM User WHERE id=%d',$changedById) : '';
		if($changedBy===''){$changedBy=t('Anonymous');}
		$changedDate=$j===0 ? $event->changed : $revision['changed'];
		$title.=' <span class="title-changed-date">'.
			t('(changed on @date by @user)',['@date'=>demos_format_date('shorter-date-time',$changedDate),
											 '@user'=>$changedBy]).
			'</span>';
		$title.='</a>';
		$hdOptions['cols'][$j]['title']=$title;
		
		if($j===1){$hdOptions['cols'][$j]['topics']=implode(', ',dlib_object_column($data['topics'],'name'));}
	}

	$hdOptions['cols'][1]['editable']=false;

	$out.=demosphere_event_hdiff($current,$other,$hdOptions);
	return $out;
}

function demosphere_event_revisions_restore($rid)
{
	global $base_url,$demosphere_config,$currentPage;

	$revision=db_array('SELECT * FROM event_revisions WHERE id=%d',$rid);
	$current=Event::fetch($revision['event_id'],false);
	if($current===null){dlib_not_found_404();}
	$other=unserialize($revision['event'],["allowed_classes" => ['Event']]);
	//var_dump($current->id,$other->id);
	require_once 'demosphere-date-time.php';

	$items['message']=['html'=>'<p>'.ent(t('This will backup the current event and then copy the revision !rid (!date) to the current event.',
										   ['!rid'=>$rid,'!date'=>demos_format_date('short-date-time',$revision['changed'])])).'</p>'];
	$items['confirm']=
		['type'=>'submit',
		 'value'=>t('Confirm'),
		 'submit'=>function()use($current,$other,$revision)
			{
				// backup current version
				demosphere_event_revisions_create($current->id);
				// Simply save other revision
				unset($other->dboNewObjectNotSaved);
				// *id,
				// *title,*htmlTitle,*startTime,
				// ?placeId,
				// log,
				// *body,*authorId,*status,*showOnFrontpage,*created,
				// changed,changedById,
				// *moderationStatus,*moderationMessage,*needsAttention,
				// dayrank,visits,
				// repetitionGroupId,
				// *externalSourceId,*extraData
				$other->log=$current->log;
				$other->logAdd('Restored revision '.$revision['id']);
				$other->dayrank=$current->dayrank;
				$other->visits=$current->visits;
				$other->repetitionGroupId=$current->repetitionGroupId;

				$data=unserialize($revision['data'],["allowed_classes" => ['Place','City','Topic']]);
				// ************** handle place/city changes
				// serialized place stored in revision
				$oldPlace=$data['place'];
				// current db version of $oldPlace
				$existingPlace=Place::fetch($other->placeId,false);
				// place has not changed from its stored version
				if($existingPlace!==null && $existingPlace->equals($oldPlace))
				{$other->setPlace($existingPlace);}
				else
					// place has changed
				{
					$newPlace=clone $oldPlace;
					if($oldPlace->referenceId===$oldPlace->id){$newPlace->referenceId=0;}
					// very rare case: stale referenceId
					if($newPlace->getReference(false)===null)
					{
						$newPlace->referenceId=$newPlace->id;
					}
					// very rare case: stale cityId
					if($newPlace->cityId!==0 && City::fetch($newPlace->cityId,false)===null)
					{
						$newPlace->setCity(City::findOrCreate($data['city']->name));
					}
					$other->setPlace($newPlace);
				}

				$other->setTopics(array_keys($data['topics']));

				$other->save();
			},
		];
	$items['cancel']=['type'=>'submit',
					  'value'=>t('Cancel')];

	require_once 'dlib/form.php';
	$formOpts=['redirect'=>$current->url().'/revisions'];
	return form_process($items,$formOpts);

}

//! Display and edit side by side differencences between two events
//! This uses several instances of hdiff()
//! Note: this function is not revision specific, it could be in a separate file
function demosphere_event_hdiff($event1,$event2,$hdOptions=[])
{
	global $base_url,$currentPage;

	require_once 'dlib/html-tools.php';
	require_once 'dlib/hdiff.php';
	require_once 'demosphere/demosphere-date-time.php';

	if(!isset($hdOptions['cols'][0])){$hdOptions['cols'][0]=[];}
	if(!isset($hdOptions['cols'][1])){$hdOptions['cols'][1]=[];}

	$currentPage->addCssTpl('demosphere/css/demosphere-event-hdiff.tpl.css');
	
	$out='<div class="event-hdiff-multipart">';

	// ************* First diff part: title

	$diffCols=[];
	for($colNb=0;$colNb<2;$colNb++)
	{
		$event=($colNb==0 ? $event1 : $event2);
		$diffTitle='<a href="'.ent($event->url()).'"><span class="tmdoc-tid">'.ent('#'.$event->id.': ').'</span>'.ent($event->title).'</a>';
		$diffTitle=val($hdOptions['cols'][$colNb],'title',$diffTitle);

		$diffCols[$colNb]=
			['html'=>$event->safeHtmlTitle(),
			 'title'=>$diffTitle,
			 'sanitize'=>false,
			 'class'=>'event-hdiff-part event-hdiff-title event-id-'.$event->id,
			 ];
		if(val($hdOptions['cols'][$colNb],'editable',true))
		{
			$diffCols[$colNb]['save']=$base_url.'/event-hdiff-save/'.$event->id.'?part=title&changeNumber='.$event->changeNumber;
		}
	}
	$out.=hdiff($diffCols);

	// ************* Second diff part: non-editable : time, place, city, topics, moderationStatus

	$diffCols=[];
	for($colNb=0;$colNb<2;$colNb++)
	{
		$event=($colNb==0 ? $event1 : $event2);

		$html='';
		$html.='<p><strong>'.t('Date').'</strong> : '.
			ent(demos_format_date('full-date-time',$event->startTime)).'</p>';
		$html.='<p><strong>'.t('City').' :</strong> '.
			ent($event->usePlace()->getCityName()).'<br/>'.
			$event->usePlace()->htmlAddress().'</p>';
		if(isset($hdOptions['cols'][$colNb]['topics'])){$topics=$hdOptions['cols'][$colNb]['topics'];}
		else{$topics=implode(', ',$event->getTopicNames());}
		$html.='<p><strong>'.t('Topics').' :</strong> '.ent($topics).'</p>';
		$mstatusLabels=Event::moderationStatusLabels();
		$html.='<p><strong>'.t('Status').' :</strong> '.ent($mstatusLabels[$event->moderationStatus]['title']).'</p>';
		
		$diffCols[$colNb]=
			['html'=>$html,
			 'class'=>'event-hdiff-part event-hdiff-address event-id-'.$event->id,
			 ];
	}
	$out.=hdiff($diffCols);


	// ************* Third diff part: editable body

	$diffCols=[];
	for($colNb=0;$colNb<2;$colNb++)
	{
		$event=($colNb==0 ? $event1 : $event2);
		$html=$event->body;
		$diffCols[$colNb]=
			['html'=>$html,
			 'class'=>'event-hdiff-part event-hdiff-body event-id-'.$event->id,  
			 ];
		if(val($hdOptions['cols'][$colNb],'editable',true))
		{
			$diffCols[$colNb]['save']=$base_url.'/event-hdiff-save/'.$event->id.'?part=body&changeNumber='.$event->changeNumber;
		}
	}
	require_once 'htmledit/demosphere-htmledit.php';
	$token=demosphere_htmledit_hdiff();
	$out.=hdiff($diffCols);
	$out.=$token;
	$out.='</div>';

	return $out;
}

//! Ajax call from side by side diff js to save an edited event's body or title
//! Note: this is not revision specific, it could be in a separate file
function demosphere_event_hdiff_save($id)
{
	dlib_check_form_token('hdiff');
	require_once 'demosphere/htmledit/demosphere-htmledit.php';
	$event=Event::fetch($id);
	$part=$_GET['part'];
	$changeNumber=$_GET['changeNumber'];
	if($event->changeNumber>$changeNumber)
	{
		return ['ok'=>false,
				'message'=>t('Save failed : the event was edited by another user.'),
				];
	}
	if($part==='body')
	{
		$event->body=demosphere_htmledit_submit_cleanup($_POST['html']);
	}
	if($part==='title') 
	{
		$html=$_POST['html'];
		$tag1='IJM!P*IO%J9865';
		$tag2='I(FM!P*IO%J936_';
		$html=str_replace(['<strong>','</strong>'],[$tag1,$tag2],$html);
		$text=dlib_fast_html_to_text($html);
		$text=dlib_cleanup_plain_text($text);
		$text=trim($text);
		$html=ent($text);
		$html=str_replace([$tag1,$tag2],['<strong>','</strong>'],$html);
		$event->htmlTitle=$html;
		$event->title=str_replace([$tag1,$tag2],['',''],$text);
	}
	$event->save();
	return ['ok'=>true,
			'changeNumber'=>$event->changeNumber,
			'id'=>$event->id,
			];
}


function demosphere_event_revisions_install()
{
	db_query("DROP TABLE IF EXISTS event_revisions");
	db_query("CREATE TABLE event_revisions (
  `id` int(11) NOT NULL auto_increment,
  `event_id` int(11) NOT NULL,
  `changed_by_id` int(11) NOT NULL,
  `changed` int(11) NOT NULL,
  `last_log` longtext NOT NULL,
  `event` longblob NOT NULL,
  `data` longblob NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `event_id` (`event_id`),
  KEY `changed_by_id` (`changed_by_id`),
  KEY `changed` (`changed`)
) DEFAULT CHARSET=utf8mb4");

}

?>