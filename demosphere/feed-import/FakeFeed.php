<?php

/** \addtogroup feed_import 
 * @{ */


//! A FakeFeed can be used to create a Feed from sources of information other than an RSS/Atom feed.
//! 
//! A FakeFeed is always associated to a Feed. A Feed can have 0 or 1 FakeFeed's.
//! There are several types of FakeFeeds for handling different types of information sources.
class FakeFeed extends DBObject
{
	//! unique autoincremented ID. 
	public $id=0;
	static $id_           =['type'=>'autoincrement'];

	//! Which type of approach should be used to generate this feed: 
	//! Custom hand-written php code; Extract links from part of a page; ...
	public $type=1;
	static $type_       =['type'=>'enum','values'=>[0=>'custom-code',1=>'links-in-page',2=>'icalendar',3=>'page-parts',4=>'facebook',5=>'twitter']];

	//! Source URL of main page containing information for this feed. 
	//! Certain types, like "Links In Page", "page parts", "facebook", "twitter" also use this url
	public $url;
	static $url_          =['type'=>'url'];

	//! The id of an associated Feed object (this is not optional: all FakeFeed must have a Feed) 
	public $feedId;
	static $feedId_       =['type'=>'foreign_key','class'=>'Feed'];

	//! custom-code: The PHP code that the user enters to extract information and build this FakeFeed.
	public $ccPhpCode;
	static $ccPhpCode_    =[];

	//! links-in-page: which part of page are links extracted from
	public $lipHtmlSelector=[];
	static $lipHtmlSelector_ =['type'=>'array'];

	//! links-in-page: regexp selecting which links should be used
	public $lipLinkRegexp='';
	static $lipLinkRegexp_  =[];

	//! iCalendar: link to ics data
	public $iCalUrl='';
	static $iCalUrl_ =[];

	//! Page part: one or more parts of page that can be directly used as items or (optionally) further split into more parts by ppSplitRegex
	public $ppCssSelector='';
	static $ppCssSelector_ =[];

	//! Page part: (optional) Each page part found by ppCssSelector can be further split into parts by this regex
	public $ppSplitRegex='';
	static $ppSplitRegex_  =[];	

	//! Facebook id 
	public $fbId;
	static $fbId_  =[];	

	//! Misc data for statistics, debugging and experimental features.
	//! List: facebook events page throttle, 
	public $data;
	static $data_         =['type'=>'array'];

	//! information for database load / save in DBObject.
	static $dboStatic=false;

	static $facebookDebug=false;

	function __construct($feedId,$url,$type=1)
	{
		$this->feedId=$feedId;
		$this->type=$type;
		$this->url=$url;
	}

	static function typeFields()
	{
		return 
			[
				'custom-code'  =>['ccPhpCode'],
				'links-in-page'=>['lipHtmlSelector','lipLinkRegexp'],
				'icalendar'    =>['iCalUrl'],
				'page-parts'   =>['ppCssSelector','ppSplitRegex'],
				'facebook'     =>['fbId'],
				'twitter'      =>[],
			];
	}

	static function error($label,$data=[])
	{
		$e=new Exception($label);
		$e->demosphere_is_fakefeed_error=true;
		$e->demosphere_data=$data;
		throw($e);
	}

	//! Returns the RSS/ATOM XML of this FakeFeed
	//! $debug : null (=silent), false, true
	function feedXml($debug=false)
	{
		require_once 'dlib/html-tools.php';
		require_once 'dlib/download-tools.php';

		// *** Most FakeFeeds are first built as an array of items, that is then translated into an XML feed
		if($debug===null){ob_start();}
		$items=false;
		$xml=false;
		switch($this->getType())
		{
		case 'custom-code'  : $items=$this->customCodeBuildItems( $debug);break;
		case 'links-in-page': $items=$this->linksInPageBuildItems($debug);break;
		case 'icalendar'    : $items=$this->iCalendarBuildItems(  $debug);break;
		case 'page-parts'   : $items=$this->pagePartsBuildItems(  $debug);break;
		case 'facebook'     : $items=$this->facebookBuildItems(   $debug);break;
		case 'twitter'      : $items=$this->twitterBuildItems(    $debug);break;
		default: fatal('FakeFeed:feed: unknown type');
		}
		if($debug===null){ob_end_clean();} 

		// *** debugging
		if($debug && $items!==false)
		{
			global $currentPage;
			$currentPage->addCss('dlib/table-tools.css');
			echo '<h2>Generated feed items:</h2>';
			echo '<table class="gray-table">';
			echo '<tr><th></th>';
			foreach(val($items,0,[]) as $k=>$v)
			{
				echo '<th>'.ent($k).'</th>';				
			}
			echo '</tr>';
			foreach($items as $n=>$item)
			{
				echo '<tr>';
				echo '<td>'.ent($n).'</td>';
				foreach($item as $k=>$v)
				{
					echo '<td class="col-'.ent($k).'">'.ent($v).'</td>';
				}
				echo '</tr>';
			}
			return;
		}
		if($debug && $xml!==false)
		{
			echo '<pre id="fake-feed-xml">';
			echo ent($xml);
			echo '</pre>';
			return;
		}

		if($xml===false){$xml=self::makeFeedFromItems($items);}
		return $xml;
	}

	function customCodeBuildItems($debug=false)
	{
		// Change function calls to methods for functions starting with "ff_" (example: ff_create_items_from_contents())
		$code=$this->ccPhpCode;
		$code=preg_replace('@\b(ff_[a-zA-Z0-9_]+)[(]@','$this->$1(',$code);

		if($debug)
		{
			echo "<h2>".t("Custom code:")."</h2>\n";
			echo '<pre id="custom-code">'.ent(trim($code)).'</pre>';
			echo "<h2>".t("Output:")."</h2>";
		}
		
		try
		{
			if($debug){$evalRes= eval($code);}
			else      {$evalRes=@eval($code);}
		}
		catch(Throwable $e)
		{
			if(isset($e->demosphere_error_label)){throw $e;}
			self::error('ff_cc_parse',['extra'=>$e->getMessage()]);
		}

		if(!is_array($evalRes)){self::error("ff_cc_return");}

		// If $evalRes are links, turn them into items
		$res=[];
		foreach($evalRes as $r)
		{
			if(is_array($r)){$res[]=$r;}
			else
			{
				$res[]=['link'=>$r];
			}
		}

		return $res;
	}

	//! Download a page and extract links from it (or optionally part of it).
	function linksInPageBuildItems($debug=false)
	{
		$html=self::ff_fetch_page($this->url,$this->lipHtmlSelector);
		if(strlen($html)===0){self::error('ff_lip_empty_html');}
		$lipLinkRegexp=$this->lipLinkRegexp;
		if($lipLinkRegexp===''){$lipLinkRegexp='@^'.preg_quote(dlib_host_url($this->url),'@').'@';}
		$links=self::ff_get_all_inner_links($html,$lipLinkRegexp,false);
		$links=preg_grep('@\.ics$@i',$links,PREG_GREP_INVERT);
		$res=[];
		foreach($links as $link){$res[]=['link'=>$link];}
		return $res;
	}

	//! Parse an iCalendar link using iCalcreator
	function iCalendarBuildItems($debug=false)
	{
		global $demosphere_config,$feed_import_config;
		require_once 'lib/iCalcreator/iCalcreator.php';

		$events=ical_parse($this->iCalUrl,'url',time(),strtotime('2 months'),'feedimportx',$error);
		if($events===false)
		{
			self::error($error['label'],$error);
		}

		$res=[];
		foreach($events as $event)
		{
			$html='';
			$html.=// date is added later, in Article::fetchAndCleanupContent() using 'start'
				'<p style="margin-left: 5px;margin-top:0;"><span style="color: #a00;">'.ent($event['summary']).'</span><br/>'.
				($event['location']!==false ? $event['location'].'<br/>'    : '').
				($event['description']!==false ? '<span style="color: #777;">'.str_replace("\n","<br/>",ent($event['description'])).'</span>' : '').
				"</p>\n";
			$res[]=
				[
					'title'=>$event['summary'],
					'description'=>$html,
					'link'=>$event['url'],
					'guid'=>md5($this->id.':'.$event['dtstart'].':'.$html),
					'start'=>$event['dtstart']
				];
		}

		return $res;
	}

	//! Split page into multiple parts based on CSS selector and, optionally, a regex.
	function pagePartsBuildItems($debug=false)
	{
		// We don't use our usual html selector because we want to split into multiple parts 
		$html=self::ff_fetch_page($this->url);
		$path=dlib_css_selector_to_xpath($this->ppCssSelector);
		$doc=dlib_dom_from_html($html,$xpath);
		$selected=$xpath->evaluate($path);
		if($selected===null || $selected===false)
		{
			self::error('ff_pp_select_error',['extra'=>"strlen(input html)=".strlen($html)]);
		}
		if($selected->length==0)
		{
			self::error('ff_pp_select');
		}

		$articles=[];
		foreach($selected as $node)
		{
			$article=trim($doc->saveXML($node));
			if($this->ppSplitRegex===''){$articles[]=$article;continue;}

			$subParts=preg_split($this->ppSplitRegex,$article);
			foreach($subParts as $subPart)
			{
				$articles[]=$subPart;
			}
		}
		
		$res=[];
		foreach($articles as $article)
		{
			$res[]=
				[
					'title'=>'',
					'description'=>$article,
					'link'=>$this->url,
					'guid'=>md5($this->id.':'.$article),
				];
		}
		return $res;
	}

	//! Creates a basename from a Facebook URL. This is used for debugging.
	static function facebookUrlToFilename($url)
	{
		$res=$url;
		$res=preg_replace('@https://[^/]*/@','',$res);
		$res=preg_replace('@/about.*$@','',$res);
		$res=preg_replace('@/$@','',$res);
		$res=preg_replace('@[^a-z0-9-]@i','-',$res);
		return $res;
	}

	//! Download a page from Facebook.
	//! If FakeFeed::$facebookDebug is set, then downloaded files are cached to avoid too many requests on FB while testing.
	function facebookDownload($url,$tag,$useBrowser=false,$options=[])
	{
		global $feed_import_config;

		if(self::$facebookDebug)
		{
			$fileBase=FakeFeed::facebookUrlToFilename($url);
			$fileName ='files/private/tmp/fb-debug/'.$fileBase.'.'.$tag.'.html';
			if(file_exists($fileName) && filemtime($fileName)>time()-3600*24*15)
			{
				return file_get_contents($fileName);
			}
		}
		sleep(2);
		if(!$useBrowser)
		{
			$html=dlib_download_and_clean_html_page($url,false,
													['user-agent'=>'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0',
													 'log'=>$feed_import_config['log_file'],
													 'info'=>&$dlInfo,
													 'timeout'=>180]);
			if($html===false){self::error('dl_facebook',['download'=>$dlInfo]);}
		}
		else
		{
			$options['log']=$feed_import_config['log_file'];
			$html=dcomponent_browser_download($url,false,1,$options);
			if($html===false){self::error('dl_facebook',['extra'=>'browser download failed']);}
			$html=dlib_html_links_to_urls($html,$url);
		}

		if(self::$facebookDebug)
		{
			if(!file_exists(dirname($fileName))){mkdir(dirname($fileName));}
			file_put_contents($fileName,$html);
			$tidied=dlib_html_cleanup_page_with_tidy($html);
			$tidied=preg_replace('@^(.{2000}).*$@m','$1',$tidied);
			file_put_contents(preg_replace('@\.html$@','.tidy.html',$fileName),$tidied);
		}
		return $html;
	}

	//! These minimized classes/id's used in Facebook's HTML may change. Not sure how often, but it seems infrequent.
	//! Efficient debug: turn on $facebookDebug, update all facebook feeds and grep for classes in files/private/tmp/fb-debug/*profile.tidy.html
	static $fbNames=
		[
			// Profile page
			'avatar'         =>'_38vo', // contains image with avatar, beside each content header: <div class="_38vo"><img ...>
			'content-header' =>'_14f3', // h5 or h6 only : main line of header block: <h6 class="_14f3 ... 
			'author'         =>'fwb'  , // mostly in header block :  link to a user/page that authored some content: <span class="fwb"><a class="profileLink" 
			'publish-time'   =>'_5ptz', // Under content header, inside link to post/event,..., displays pub time : <abbr data-utime="" class="_5ptz">
			'event-date'     =>'_5x8v', // aria-label with good time for quoted event: <span aria-label="jeudi 19 avril" class="_5x8v">
			'nb-interested'  =>'_fwz' , // nb people interested by an event quoted in another post: <div class="_fwz">146 personnes intéressées
			'nb-views'       =>'_2ezg', // <div class="_2ezg"><div class="clearfix _3-8n"><div data-hover="tooltip"...>768 vues
			'nb-likes'       =>'_52db', // <div class="_52db">+7
			'video-error'    =>'_npo' , // <div class="_npo hidden_elem><div><div><div><div><span>Vous semblez rencontrer des problèmes pour lire cette vidéo
			'video-controls' =>'_5mly', // <div class="_5mly _45oh" ...> contains inputs or buttons
			// Event page
			'details-label'  =>'_38my', // "À propos du lieu", "Détails" : <span class="_4l8q _38my" itemcomponent="span">Détails</span>
			'details-div'    =>'_63eu', // big div containing full description, after "Détails:". Not sure if we should use _63eu or _63ew
			'event-time'     =>'_2ycp', // <div class="_2ycp _5xhk" content="2018-06-17T07:00:00-07:00..."><span><span itemprop="startDate">dimanche 17 juin
		];

	//! Query selector: returns list of nodes matching CSS selector 
	//! Facebook names ($fbNames) are replaced.
	static function qs($node,$css)
	{
		foreach(self::$fbNames as $name=>$fb){$css=str_replace('%'.$name,$fb,$css);}
		return dlib_dom_query_selector($node,$css);
	}

	//! Returns feed items for a FakeFeed Feed
	function facebookBuildItems($debug=false)
	{
		global $feed_import_config;
		$fbNames=self::$fbNames;
		require_once 'dlib/download-tools.php';
		require_once 'dlib/html-tools.php';
		echo "#################################################\n";
		echo ent($this->url)."\n";
		echo "#################################################\n";

		// Download fb profile or page (homepage)

		// To save ressources, only use browser download once a day (or even less if feed is inactive)
		$mostRecent=db_result('SELECT MAX(lastFetched) FROM Article WHERE feedId = %d',$this->feedId);
		$wait=3600*24;
		if($mostRecent!==null && $mostRecent< time()-3600*24*30*1){$wait=3600*24*2;}
		if($mostRecent!==null && $mostRecent< time()-3600*24*30*3){$wait=3600*24*4;}
		if(mt_rand()%2){$wait+=3600;}
		$useBrowser=time()>($this->data['fb-last-profile-page'] ?? 0)+$wait;

		$html=$this->facebookDownload($this->url,'profile',$useBrowser, $useBrowser ? ['scroll_down'=>true] : []);

		if($useBrowser)
		{
			$this->data['fb-last-profile-page']=time();
			$this->save();
		}

		$doc=dlib_dom_from_html($html,$xpath);
		$xpath2 = new DOMXPath($doc);
		$xpath3 = new DOMXPath($doc);

		$extraPages=[];

		// **** Fetch urls of all future events from events page. 

		// Determine link to events page ("Events" link on left "tab" (menu))
		$eventsPageLink=self::qs($doc,'[data-key=tab_events] a[href*=/events/]');
		$throttleEventPageDl=time()<($this->data['fb-last-events-page'] ?? 0)+3600*23;
		if($eventsPageLink->length && !$throttleEventPageDl)
		{
			// Download events page
			$eventsUrl=$eventsPageLink[0]->getAttribute('href');
			$eHtml=$this->facebookDownload($eventsUrl,'events',true);
			// find links to future events
			$eDoc=dlib_dom_from_html($eHtml,$eXpath);
			foreach(self::qs($eDoc,'#upcoming_events_card a[href*=/events/]') as $a)
			{
				$eUrl=$a->getAttribute('href');
				if(strpos($eUrl,'/')===0){$eUrl=dlib_host_url($this->url).$eUrl;}
				$eUrl=dcomponent_url_clean_facebook($eUrl);
				$extraPages[]=$eUrl;
			}
			$this->data['fb-last-events-page']=time();
			$this->save();
		}

		$res=[];
		// **** Find userContentWrapper blocks (posts, events, videos... on homepage)
		// Note: userContentWrapper may be nested, and sometimes actually removed by facebookCleanupCommon()
		// This happens if userContentWrapper is inside a comment (for example: a comment shares a video)
		// In that case $content no longer exists.
		// NodeList behaves strangely if one of its elements is removed, so we copy the nodelist into a regular array.
		$tmpNodeList=[];
		foreach(self::qs($doc,'.userContentWrapper') as $content){$tmpNodeList[]=$content;}
		foreach($tmpNodeList as $nn=>$content)
		{
			// Check if node has disappeared (see comment above)
			if(@$content===null || @$content->ownerDocument===null){continue;}

			// Find url of this content block by looking for <abbr...>
			$abbrs=self::qs($content,'abbr.%publish-time[data-utime]');
			if($abbrs->length===0){continue;}// Just in case. Haven't seen this really happen.
			$abbr=$abbrs[0];
			$contentLink=$abbr->parentNode->nodeName==='a' ?  $abbr->parentNode->getAttribute('href') : false;	
			
			if($contentLink!==false){$contentLink=dcomponent_url_clean_facebook($contentLink);}
			echo "-----\n";
			echo "Content link:".ent($contentLink)."\n";
			//vd(dlib_simplify_text($content->textContent));

			// recognize $contentLink type
			// FIXME $contentType is not currently used. 
			$contentType=false;
			if(preg_match('@^https://[^/]*facebook\.[a-z]+/([^/?&#]*/)?posts/[0-9]{4,}@' ,$contentLink)){$contentType='post';}
			if(preg_match('@^https://[^/]*facebook\.[a-z]+/([^/?&#]*/)?events/[0-9]{4,}@',$contentLink)){$contentType='event';}
			if(preg_match('@^https://[^/]*facebook\.[a-z]+/([^/?&#]*/)?photos/@'         ,$contentLink)){$contentType='photo';}
			if(preg_match('@^https://[^/]*facebook\.[a-z]+/([^/?&#]*/)?videos/@'         ,$contentLink)){$contentType='video';}
			// FIXME: what is permalink type  ? 
			// https://fr-fr.facebook.com/permalink.php?story_fbid=582335855498654&id=187045108361066  <=> 
			// https://fr-fr.facebook.com/187045108361066/posts/582335855498654
			// permalink  seems to be used by fb pages. 
			// /posts/... seems to be used by fb profiles (user)
			if(preg_match('@^https://[^/]*facebook\.[a-z]+/permalink.php@'        ,$contentLink)){$contentType='permalink';}
			echo "Content type:".ent($contentType)."\n";

			// *** This $content block is a short piece of content that can be
			// - a direct reference to some content published elsewhere (post, event, photo, or video)
			//   sometimes the referenced content is included verbatim (or with "see more" button)
			//   sometimes the content is incomplete, with a link ""
			// - a shared content (post, event, photo, or video)
			//    In that case this $content is a post that can add some text (usually a comment) and then link to the shared content
			//
			// By default, we just create an $item with whatever $content is available.
			// This is a problem because:
			// 1) the text can be incomplete
			// 2) if this is a reference to other content, the rss guid created will block further publication of that content (only on the same feed)
			//
			// We ignore these problems except for events (in the future we might also deal with posts, photos...)
			// For events, we fetch the actual event page and create an item for it.
			// In that case we don't create an item for this $content
			//
			// We also look for links to other content (only events for now) inside the current $content.
			// If links are found we download the page and create an extra item.
			// Problem: if current $content is a post that only shares another page (only events for now) without adding any info, 
			// then keeping $content is useless.
			// FIXME: we should identify that case and skip $content.

			if($contentType==='event'){$extraPages[]=$contentLink;}

			// *** Check for linked pages (events), and create extra items with their full text
			foreach($xpath2->query(".//a[contains(@href,'/events/')]",$content) as $a)
			{
				$url=$a->getAttribute('href');
				if(preg_match('@^https://[^/]*facebook\.[a-z]+/([^/?&#]*/)?events/[0-9]{4,}@',$url,$m))
				{
					$extraPages[]=dcomponent_url_clean_facebook($m[0]);
				}
			}

			// Don't create an item for an event, as it was added to $extraPages
			if($contentType==='event'){continue;}

			// ****** Ok, cleanup this content and create item.

			// *** DOM fixes
			self::facebookCleanupCommon($content);
			
			// Pubdate is first date. Get it and remove it.
            $pubDate=false;
			$pubDateEl=self::qs($content,'[demos-utime-ts]');
			if($pubDateEl->length)
			{
				$pubDate=intval($pubDateEl[0]->getAttribute('demos-utime-ts'));
				$pubDateEl[0]->parentNode->removeChild($pubDateEl[0]);
			}


			// Remove first profile link (own link). This avoids redundant text and spurious text matching.
			// Most links have .profileLink but some posts dont, so we use .fwb
			$profileHeads=self::qs($content,'.%content-header');
			if($profileHeads->length)
			{
				$l=self::qs($profileHeads[0],'.%author a');
				if($l->length){$l[0]->parentNode->removeChild($l[0]);}
			}
			
			$contentHtml=$doc->saveHTML($content);
			
			// FIXME: think about title
			// FIXME: think about removing comments
			// FIXME:  "continue reading" text_exposed_link>a (without .see_more_link) ("lire la suite") =>  need to fetch page, or click?

			$item=
				[
					'title'=>'',
					'description'=>$contentHtml,
					'link'=>$contentLink,
					'guid'=>md5($contentLink),
				];
			if($pubDate!==false){$item['pubDate']=$pubDate;}
			$res[]=$item;
		}

		// *** create extra items for all $extraPages found

		$extraPages=array_unique($extraPages);
		echo 'extraPages:';vd($extraPages);
		foreach($extraPages as $pageUrl)
		{
			// check if this page has already been fetched as an Article
			$nb=db_result("SELECT COUNT(*) FROM Article WHERE guid='%s'",md5($pageUrl));
			if($nb>0){echo "Skipping existing page / event\n";continue;}
			 
			$pageHtmlRaw=$this->facebookDownload($pageUrl,'page',true);
			//file_put_contents('/tmp/fff',$pageHtmlRaw);
			//vd($pageUrl);die('pp');
			$pageHtml=FakeFeed::facebookEventPage($pageHtmlRaw);
			if($pageHtml===false){self::error('ff_fb_full_page_parse');}
				
			$title='';
			if(preg_match('@<h1[^>]*demosfb-event-title[^>]*>(.*)</h1>@sU',$pageHtml,$m)){$title=$m[1];}

			$item=
				[
					'title'=>$title,
					'description'=>$pageHtml,
					'link'=>$pageUrl,
					'guid'=>md5($pageUrl),
				];
			vd($item);
			$res[]=$item;
		}

		return $res;
	}

	//! Fix a lot of things on any Facebook page (profile, event, post,...).
	//! This works on DOM only
	static function facebookCleanupCommon($base)
	{
		$doc=$base->ownerDocument!==null ? $base->ownerDocument : $base;
		$fbNames=self::$fbNames;
		$xpath =new DOMXPath($doc);
		$xpath2=new DOMXPath($doc);
		foreach(self::qs($base,'abbr.%publish-time[data-utime]') as $utime)
		{
			$ts=(int)$utime->getAttribute('data-utime');
			$tsSpan=self::qs($utime,'span.timestampContent');
			if($tsSpan->length)
			{
				$tsSpan[0]->nodeValue=dcomponent_format_date('full-date-time',$ts);
				$tsSpan[0]->setAttribute('demos-utime-ts',$ts);
			}
		}

		// Replace quoted event date, with its aria-label
		//<span aria-label="jeudi 19 avril" class="_5x8v
		foreach(self::qs($base,'span.%event-date[aria-label]') as $evTime)
		{
			$label=$evTime->getAttribute('aria-label');
			$evTime->nodeValue=' ['.$label.'] ';
		}

		// Remove "See more" "..." and link 
		foreach(self::qs($base,'span.text_exposed_hide,span.see_more_link_inner') as $remove)
		{
			$remove->parentNode->removeChild($remove);
		}

		// Remove small profile image (beside each link)
		foreach(self::qs($base,'.%avatar') as $remove)
		{
			$remove->parentNode->removeChild($remove);
		}

		// remove little dots around date-time
		foreach(self::qs($base,'[aria-hidden=true][role=presentation]') as $remove)
		{
			$remove->parentNode->removeChild($remove);
		}

		// remove number of people interested by an event
		foreach(self::qs($base,'.%nb-interested') as $remove)
		{
			$remove->parentNode->removeChild($remove);
		}

		// remove number of views & number of likes
		foreach(self::qs($base,'.%nb-views,.%nb-likes') as $remove)
		{
			$remove->parentNode->removeChild($remove);
		}

		// remove strange "SpSonsSoriSséS" link (looks like anti-scraping)
		foreach(self::qs($base,'span.b_c3pyn-24p') as $remove)
		{
			$remove->parentNode->removeChild($remove);
		}

		// fix Facebook external links: http://l.facebook.com/l.php?u=http%3A%2F%2Fexample.org...
		foreach($xpath->query(".//a/@href",$base) as $attr)
		{
			$parsed=parse_url($attr->value);
			if($parsed===false){continue;}
			if(strpos(val($parsed,'host',''),'facebook.')!==false &&  val($parsed,'path')==='/l.php' && isset($parsed["query"]))
			{
				parse_str($parsed["query"],$query);
				if(isset($query["u"]) && preg_match('@^https?://@',$query["u"]))
				{
					$attr->value=ent($query["u"]);
				}
			}
		}

		// Add spaces in shared event time/place (FIXME: this might only be needed if we dont import full event )
		foreach(self::qs($base,'.eventTime') as $eventTime)
		{
			foreach($xpath2->query(".//span",$eventTime->parentNode) as $span)
			{
				$span->parentNode->insertBefore($doc->createTextNode(' '),$span);
				$span->parentNode->insertBefore($doc->createTextNode(' '),$span->nextSibling);
			}
		}
		
		// Video error messages. 
		foreach(self::qs($base,'.%video-error') as $remove)
		{
			$remove->parentNode->removeChild($remove);
		}

		// Video
		foreach($xpath->query(".//video",$base) as $remove)
		{
			$remove->parentNode->removeChild($remove);
		}

		// Video controls
		foreach(self::qs($base,'.%video-controls') as $remove)
		{
			$remove->parentNode->removeChild($remove);
		}

		// inputs (ex: video button)
		foreach(self::qs($base,'input,button') as $remove)
		{
			$remove->parentNode->removeChild($remove);
		}

		// commentable_item
		foreach(self::qs($base,'form.commentable_item') as $remove)
		{
			$remove->parentNode->removeChild($remove);
		}

		// Link to hires images
		foreach($xpath->query(".//a[.//img and @data-ploi]",$base) as $hiresLink)
		{
			$url=$hiresLink->getAttribute('data-ploi');
			$hiresLink->setAttribute('href',$url);
			// facebook a>div>img is invalid and confuses tidy => make img direct (only) child of "a"
			$img=$xpath2->query(".//img",$hiresLink);
			$img[0]->parentNode->removeChild($img[0]);
			$a=[];
			foreach($hiresLink->childNodes as $n){$a[]=$n;}
			foreach($a as $n){$hiresLink->removeChild($n);}
			$hiresLink->appendChild($img[0]);
		}

		// remove duplicate images (example: shared video)
		$srcs=[];
		foreach($xpath->query(".//img/@src",$base) as $attr)
		{
			$src=$attr->value;
			if(isset($srcs[$src]))
			{
				$attr->ownerElement->parentNode->removeChild($attr->ownerElement);
			}
			$srcs[$src]=true;
		}
		
	}


	//! Returns an HTML fragment with a cleaned up facebook event.
	//! $html => full html of a Facebook event page (downloaded with browser having JS)
	static function facebookEventPage($html)
	{
		$fbNames=self::$fbNames;
		$doc=dlib_dom_from_html($html,$xpath);

		self::facebookCleanupCommon($doc);
		
		// Get image
		$dataPloi=$xpath->query("//div[@id='event_header_primary']//@data-ploi");
		$imgSrc=$dataPloi->length ? $dataPloi[0]->value : false;

		// Get title
		$h1=$xpath->query("//h1[@id='seo_h1_tag']");
		if($h1->length===0){echo "Failed to find title\n";return false;}
		$title=$h1[0]->textContent;

		// Get organizers (hosted-by)
		$hostedByDiv=$xpath->query("//div[@data-testid='event_permalink_feature_line']");
		$hostedBy=$hostedByDiv->length ? $doc->saveHTML($hostedByDiv[0]) : false;
		if($hostedBy!==false){$hostedBy=preg_replace('@>\s*·@','>',$hostedBy);}

		// Get date + time
		$timeDiv=self::qs($doc,'.%event-time');
		if($timeDiv->length===0){echo "Failed to find time\n";return false;}
		$timeRaw=$timeDiv[0]->getAttribute('content');
		$ts=strtotime(preg_replace('@ .*$@','',$timeRaw));

		// Get venue
		$venueEl=$xpath->query("//li[contains(@class,'_3slj') and .//u[text()='pin']]");
		// some events don't have a venue
		if($venueEl->length!==0)
		{
			foreach($xpath->query(".//*[@role='button' or @aria-hidden='true' or contains(@class,'hidden_elem')]",$venueEl[0]) as $button){$button->parentNode->removeChild($button);}
			$venue=$doc->saveHTML($venueEl[0]);
			$venue=preg_replace('@<(/?)(table|li|tbody|td|tr)@','<$1div',$venue);
		}
		else{$venue='';}
		//echo "VENUE:$venue\n";

		// Get details
		$detailsHead=self::qs($doc,'.%details-label');
		if($detailsHead->length){$detailsHead[0]->parentNode->removeChild($detailsHead[0]);}
		$more=$xpath->query("//a[@morelabel]");
		if($more->length!==0){$more[0]->parentNode->removeChild($more[0]);}
		
		// 3/2020: Old details had data-testid, now it doesnt $detailsDiv=$xpath->query("//*[@data-testid='event-permalink-details']");
		$detailsDiv=self::qs($doc,'.%details-div');
		// Som events don't have a description
		$details=$detailsDiv->length!=0 ? $doc->saveHTML($detailsDiv[0]) : false;
		
		$res='';
		$res.='<h1 id="demosfb-event-title">'.ent($title)."</h1>\n";
		if($hostedBy!==false){$res.='<div id="demosfb-event-hosted-by">'.$hostedBy."</div>\n";}
		$res.='<div id="demosfb-event-start">'.ent(dcomponent_format_date('full-date-time',$ts))."</div>\n";
		$res.='<div id="demosfb-event-venue">'.$venue."</div>\n";
		if($details!==false){$res.='<div id="demosfb-event-details">'.$details."</div>\n";}
		if($imgSrc !==false){$res.='<div><img id="demosfb-event-image" class="floatRight" src="'.ent($imgSrc).'"/>'."</div>\n";}
		return $res;
	}


	//! Downloads a page from a url, and optionally selects a part of it.
	//! $selector is an array, as used in dlib_html_apply_selectors()
	//! This is meant to be called from custom code.
	function ff_fetch_page($url,$selector=false,$rawNotHtml=false)
	{
		global $feed_import_config;
		// download url
		echo "ff_fetch_page:$url<br/>";
		$fct='dlib_download_and_clean_html_page';
		if($rawNotHtml){$fct='dlib_curl_download';}
		$html=$fct($url,false,['log'=>$feed_import_config['log_file'],
							   'info'=>&$dlInfo,
							   'timeout'=>180]);
		if($html===false){self::error('ff_fetch_page',['download'=>$dlInfo]);}
		// apply selector
		if($selector!==false)
		{
			if(is_string($selector)){$selector=[['type'=>'XPATH','arg'=>$selector]];}
			$html=dlib_html_apply_selectors($html,$selector);
			if(strlen($html)==0)
			{
				self::error('ff_fetch_selector');
			}
		}
		return $html;
	}

	//! Scrape tweets on a Twitter page into feed items.
	function twitterBuildItems()
	{
		global $feed_import_config;

		// Do not use ff_fetch_page, since html cleanup breaks white-spaces used in tweet formating.
		$html=dlib_download_html_page($this->url,false,['log'=>$feed_import_config['log_file'],
														'info'=>&$dlInfo,
														'timeout'=>180]);
		if($html===false){self::error('ff_twitter_dl',['download'=>$dlInfo]);}

		$doc=dlib_dom_from_html($html,$xpath);
		$tweetList=$xpath->evaluate(dlib_css_selector_to_xpath('.tweet'));
		if($tweetList===null || $tweetList===false)
		{
			self::error('ff_twitter_parse',['extra'=>".tweet select failed; input html size=".strlen($html)]);
		}
		if($tweetList->length==0)
		{
			self::error('ff_twitter_parse',['extra'=>'.tweet not found']);
		}

		$res=[];
		foreach($tweetList as $tweet)
		{
			$link=dlib_dom_query_selector($tweet,'.js-permalink');
			if($link->length===0){self::error('ff_twitter_parse',['extra'=>'permalink not found']);}
			$url=$link[0]->getAttribute('href');

			$tsNode=dlib_dom_query_selector($tweet,'.js-short-timestamp');
			if($tsNode->length===0){self::error('ff_twitter_parse',['extra'=>'timestamp not found']);}
			$ts=intval($tsNode[0]->getAttribute('data-time'));


			// *** Extract and cleanup main  content
			$xpath2=new DOMXPath($doc);

			$contentNode=$xpath2->query("./div[contains(concat(' ', normalize-space(@class), ' '),' content ')]",$tweet);
			if($contentNode->length!==1){self::error('ff_twitter_parse',['extra'=>'found '.$contentNode->length.' .content (expected 1)']);}

			// Remove lots of things that we don't want.
			$removeSel='.hidden,.stream-item-header,.stream-item-footer,.self-thread-context,.self-thread-head,.self-thread-tweet-cta,.media-tags-container';
			foreach(dlib_dom_query_selector($contentNode[0],$removeSel) as $remove)
			{
				$remove->parentNode->removeChild($remove);
			}

			// Move certain links to a separate line.
			foreach(dlib_dom_query_selector($contentNode[0],'.twitter-timeline-link.u-hidden') as $a)
			{
				$p=$doc->createElement('div');
				$a->parentNode->insertBefore($p,$a);
				$p->appendChild($a);
			}

			// Make emoji images smaller.
			foreach(dlib_dom_query_selector($contentNode[0],'img.Emoji--forText') as $img)
			{
				$img->setAttribute('width','20');
			}

			// Show video as a link with a thumbnail.
			foreach(dlib_dom_query_selector($contentNode[0],'.AdaptiveMedia.is-video .PlayableMedia-player') as $video)
			{
				$style=$video->getAttribute('style');
				if(preg_match('@background-image\s*:\s*url\(\s*[\'"]([^\'")]*)@',$style,$m))
				{
					$imgUrl=$m[1];
					$a=$doc->createElement('a');
					$a->setAttribute('href',$url);
					$a->setAttribute('class','twitter-video-link');
					$a->nodeValue=t('Video:');
					$br=$doc->createElement('br');
					$img=$doc->createElement('img');
					$img->setAttribute('src',$imgUrl);
					$a->appendChild($br);
					$a->appendChild($img);
					$video->appendChild($a);
				}
			}

			// Twitter shows some links as spans for unclear reasons. We transform them back to a real link, to avoid broken text urls.
			foreach(dlib_dom_query_selector($contentNode[0],'span.twitter-timeline-link[data-expanded-url]') as $span)
			{
				$a=$doc->createElement('a');
				$a->setAttribute('href',$span->getAttribute('data-expanded-url'));
				$span->parentNode->insertBefore($a,$span);
				$a->appendChild($span);
			}

			// In main tweet text, transform newlines into <br/>'s that will not be destroyed somewhere down the processing chain.
			$brTag=dlib_random_string();
			foreach($xpath2->query(".//*[contains(concat(' ', normalize-space(@class), ' '),' tweet-text ')]//text()",$contentNode[0]) as $text)
			{
			 	$text->nodeValue=str_replace("\n",$brTag,$text->nodeValue);
			}

			// Remove "twitter-hashtag" class to avoid hiding by add-block
			foreach(dlib_dom_query_selector($contentNode[0],'.twitter-hashtag') as $el)
			{
				$el->setAttribute('class',preg_replace('@\btwitter-hashtag\b@','mtwi-tter-has-h-tag',$el->getAttribute('class')));
			}

			$contentHtml=$doc->saveHTML($contentNode[0]);
			$contentHtml=str_replace($brTag,'<br/>',$contentHtml);

			$res[]=
				[
					'title'=>'',
					'pubDate'=>$ts,
					'description'=>$contentHtml,
					'link'=>$url,
					'guid'=>$url,
				];
		}
		return $res;
	}

	//! Returns an array of all <a href="..."> urls in some html. Only links matching $linkRegexp are kept.
	//! This is meant to be called from custom code.
	function ff_get_all_inner_links($html,$linkRegexp,$failIfNoLinks=true)
	{
		$hrefs=dlib_html_select_xpath($html,'//a/@href');
		$nbMatches=preg_match_all('@href="([^"]*)@',$hrefs,$matches);
		$links=[];
		foreach($matches[1] as $link)
		{
			// Remove fragment (is this really a good idea?)
			$link=preg_replace('@#.*$@','',$link);
			echo "link: ".ent($link)."\n";
			if(!preg_match($linkRegexp,$link)){echo "(linkRegexp: no match, ignoring)<br/>\n";continue;}
			echo "(ok, matches: keeping)<br/>\n";
			$links[]=$link;
		}
		if($failIfNoLinks && count($links)==0)
		{
			self::error('ff_no_links');
		}
		$links=array_unique($links);
		return $links;
	}

	//! Creates items from an array of HTML.
    //! These items can be later transformed into an XML RSS/Atom feed by makeFeedFromItems()
	//! This is meant to be called from custom code.
	function ff_create_items_from_contents($contents,$srcUrl=false)
	{
		global $base_url;
		$res=[];
		foreach($contents as $content)
		{
			$res[]=
				[
					'link'=>$this->url,// bogus, but better than leaving it empty
					'description'=>$content,
					'guid'=>md5($this->id.':'.$content),
				];
		}
		return $res;
	}

	//! Returns a full XML RSS/Atom feed, built from an array of items.
	//! item fields:
	//! * title: optional
	//! * link: mandatory
	//! * description: optional
	//! * guid: optional
	function makeFeedFromItems($items)
	{
		$out='';
		$out.='<?xml version="1.0" encoding="utf-8"?>'."\n";
		$out.='<rss version="2.0" '.
			'xml:base="https://demosphere.net" '.
			'xmlns:demos="https://demosphere.net/custom/" '.
			'xmlns:dc="http://purl.org/dc/elements/1.1/" '.
			'xmlns:dcterms="http://purl.org/dc/terms/">'."\n";
		$out.='<channel>'."\n";
		$out.='<title>fake feed '.$this->id.'</title>'."\n";
		$out.='<link></link>'."\n";

		foreach($items as $item)
		{
			// Guessing a title may require downloading page. That would generate too many requests to remote site. 
			// By leaving a title empty, we postpone that to Article::fetchAndCleanupContent()
			$out.='<item>'."\n";
			$out.='    <title>'.xent(val($item,'title','')).'</title>'."\n";
			$out.='    <link>'.xent($item['link']).'</link>'."\n";
			$out.='    <description>'.xent(val($item,'description','')).'</description>'."\n";
			if(isset($item['pubDate']))
			{
				$out.='    <pubDate>'.date('r', $item['pubDate']).'</pubDate>'."\n";
			}
			if(isset($item['guid']))
			{
				$out.='    <guid isPermaLink="false">'.xent($item['guid']).'</guid>'."\n";
			}
			if(isset($item['start']))
			{
				$out.='    <demos:start>'.ent($item['start']).'</demos:start>'."\n";
			}
			$out.='</item>'."\n";
		}
		$out.='</channel>'."\n";
		$out.='</rss>';
		return $out;
	}

	static function createTable()
	{
		db_query("DROP TABLE IF EXISTS FakeFeed");
		db_query("CREATE TABLE FakeFeed (
  `id` int(11) NOT NULL auto_increment,
  `type` TINYINT(1) NOT NULL,
  `url` text  NOT NULL,
  `feedId` int(11) NOT NULL,
  `ccPhpCode` longblob NOT NULL,
  `lipHtmlSelector` longblob  NOT NULL,
  `lipLinkRegexp` text  NOT NULL,
  `iCalUrl` text  NOT NULL,
  `ppCssSelector` varchar(1000)  NOT NULL,
  `ppSplitRegex`  varchar(1000)  NOT NULL,
  `fbId`  varchar(100)  NOT NULL,
  `data` longblob NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `type` (`type`),
  KEY `feedId` (`feedId`),
  KEY `fbId` (`fbId`)
) DEFAULT CHARSET=utf8mb4;");
	}

}


/** @} */

?>