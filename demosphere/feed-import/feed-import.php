<?php
/** @defgroup feed_import Feed Import
 *  @{
 */

//! $feed_import_config : configuration for feed_import.
//! List of $feed_import_config keys:
//! - log_file
//! - tmp_dir
//! - delete_old_articles
//! - dir_url
//! -::-;-::-
$feed_import_config;


function feed_import_add_paths(&$paths,&$options)
{
	require_once 'dcomponent/dcomponent-common.php';

	$paths[]=['path'=>'feed-import',
			  'roles' => ['admin','moderator'],
			  'file'=>'feed-import/feed-import-view.php',
			  'function'=>'feed_import_view_main',
			 ];

	$paths[]=['path'=>'feed-import/feed',
			  'roles' => ['admin','moderator'],
			  'file'=>'feed-import/feed-import-view.php',
			  'function'=>'feed_import_view_feed_list',
			 ];
	$paths[]=['path'=>'@^feed-import/feed/(\d+)$@',
			  'roles' => ['admin','moderator'],
			  'file'=>'feed-import/feed-import-view.php',
			  'function'=>'feed_import_view_feed',
			 ];

	$paths[]=['path'=>'@^feed-import/feed/(\d+)/details$@',
			  'roles' => ['admin','moderator'],
			  'file'=>'feed-import/feed-import-view.php',
			  'function'=>'feed_import_view_feed_details',
			 ];

	$paths[]=['path'=>'@^feed-import/feed/(\d+)/delete$@',
			  'roles' => ['admin','moderator'],
			  'file'=>'feed-import/feed-import-view.php',
			  'function'=>'feed_import_view_feed_delete',
			 ];

	$paths[]=['path'=>'@^feed-import/feed/(\d+)/edit$@',
			  'roles' => ['admin','moderator'],
			  'file'=>'feed-import/feed-import-view.php',
			  'function'=>'feed_import_view_feed_edit',
			 ];

	$paths[]=['path'=>'@^feed-import/feed/(\d+)/edit-advanced$@',
			  'roles' => ['admin','moderator'],
			  'file'=>'feed-import/feed-import-view.php',
			  'function'=>'feed_import_view_feed_edit_advanced',
			 ];

	$paths[]=['path'=>'feed-import/stats',
			  'roles' => ['admin','moderator'],
			  'file'=>'feed-import/feed-import-view.php',
			  'function'=>'feed_import_view_stats',
			 ];

	$paths[]=['path'=>'feed-import/errors',
			  'roles' => ['admin','moderator'],
			  'file'=>'feed-import/feed-import-view.php',
			  'function'=>'feed_import_view_errors',
			 ];

	$paths[]=['path'=>'feed-import/error-articles',
			  'roles' => ['admin','moderator'],
			  'file'=>'feed-import/feed-import-view.php',
			  'function'=>'feed_import_view_error_articles',
			 ];

	$paths[]=['path'=>'@^feed-import/feed/(\d+)/errors$@',
			  'roles' => ['admin','moderator'],
			  'file'=>'feed-import/feed-import-view.php',
			  'function'=>'feed_import_view_feed_errors',
			 ];

	$paths[]=['path'=>'@^feed-import/article/(\d+)/finish$@',
			  'roles' => ['admin','moderator'],
			  'output'=>false,
			  'function'=>function($id)
		      {
				  $article=Article::fetch($id,false);
				  if($article===null){dlib_not_found_404();}
				  $article->setFinishedReading(false,val($_POST,'finish','true')==='true');
				  $article->save();
				  echo "finished-ok-".$id;
			  }
			 ];
	$paths[]=['path'=>'@^feed-import/article/(\d+)/import-to-demosphere$@',
			  'roles' => ['admin','moderator'],
			  'output'=>false,
			  'function'=>function($id)
		       {
				   $article=Article::fetch($id,false);
				   if($article===null){dlib_not_found_404();}
				   // FIXME  very special case: ignore urls from searchterm-feed
				   // if(strpos($url,'feed-import/searchterm-feed/article')!==false){$url=false;}
				   dcomponent_import_to_demosphere($article->title,
												   $article->content,
												   'article-'.$article->id);
			   }
			 ];

	$paths[]=['path'=>'@^feed-import/feed/(\d+)/hide-errors$@',
			  'roles' => ['admin','moderator'],
			  'output'=>'json',
			  'function'=>function($id)
		      {
				  $feed=Feed::fetch($id,false);
				  if($feed===null){dlib_not_found_404();}
				  $feed->hideErrorsUntil=time()+val($_POST,'days')*3600*24;
				  $feed->save();
				  return ['hideLabel'=>dcomponent_format_date("relative-short-full",$feed->hideErrorsUntil),
						  'hide'=>$feed->hideErrorsUntil>time()
						 ];
			  }
			 ];

	$paths[]=['path'=>'@^feed-import/fake-feed/(\d+)/debug$@',
	 		  'roles' => ['admin','moderator'],
			  'file'=>'feed-import/feed-import-view.php',
			  'function'=>'feed_import_view_fake_feed_debug',
	 		 ];

	$paths[]=['path'=>'@^feed-import/fake-feed/(\d+)/lip-html-selector-editor$@',
			  'roles' => ['admin','moderator'],
			  'function'=>function($id)
		      {
				  $ff=FakeFeed::fetch($id,false);
				  if($ff===null){dlib_not_found_404();}
				  dcomponent_progress_log_start();
				  $editorUrl=feed_import_fake_feed_lip_html_selector_editor($ff);
				  dcomponent_progress_log_end(['redirect'=>$editorUrl,'boxDisplay'=>'fold']);
			  },
			 ];

	$paths[]=['path'=>'@^feed-import/article/(\d+)/details$@',
			  'roles' => ['admin','moderator'],
			  'file'=>'feed-import/feed-import-view.php',
			  'function'=>'feed_import_view_article_details',
			 ];

	$paths[]=['path'=>'feed-import/view-recently-finished-articles',
			  'roles' => ['admin','moderator'],
			  'file'=>'feed-import/feed-import-view.php',
			  'function'=>'feed_import_view_recently_finished_articles',
			 ];

	$paths[]=['path'=>'feed-import/feed/update-all',
			  'roles' => ['admin'],
			  'function'=>function()
		      {
				  global $base_url;
				  dcomponent_progress_log_start();
				  Feed::updateAll();
				  dcomponent_progress_log_end(['redirect'=>$base_url.'/feed-import']);
			  },
			 ];

	$paths[]=['path'=>'@^feed-import/feed/(\d+)/update$@',
			  'roles' => ['admin','moderator'],
			  'file'=>'feed-import/feed-import-view.php',
			  'function'=>'feed_import_view_feed_update',
			 ];

	$paths[]=['path'=>'@^feed-import/feed/(\d+)/finish$@',
			  'roles' => ['admin','moderator'],
			  'function'=>function($id)
		      {
				  $feed=Feed::fetch($id,false);
				  if($feed===null){dlib_not_found_404();}
				  $n=$feed->finishArticles();
				  dlib_message_add(t('Finished !nb articles',['!nb'=>$n]));
				  dlib_redirect($feed->link());
			  },
			 ];

	$paths[]=['path'=>'@^feed-import/article/(\d+)/reset$@',
			  'roles' => ['admin','moderator'],
			  'function'=>function($id)
		      {
				  global $base_url;
				  $article=Article::fetch($id,false);
				  if($article===null){dlib_not_found_404();}
				  dcomponent_progress_log_start();
				  $article->reset();
				  try{$article->update();}
				  catch (Exception $e)
				  {
					  dcomponent_progress_log('Article update failed:'.ent($e->getMessage()));
				  }
				  dcomponent_progress_log_end(['redirect'=>$base_url.'/feed-import/article/'.$id.'/view-single']);
			  }
			 ];
	$paths[]=['path'=>'@^feed-import/article/(\d+)/display-html@',
			  'access' => 'demosphere_safe_domain_check', 
			  'output'=>false,
			  'function'=>function($id)
		      {
				  global $base_url,$demosphere_config;
				  $article=Article::fetch($id,false);
				  if($article===null){dlib_not_found_404();}
				  echo  $article->displayHTML();
			  }
			 ];
	$paths[]=['path'=>'@^feed-import/feed/(\d+)/configure-content-selector$@',
			  'roles' => ['admin','moderator'],
			  'function'=>function($id)
		      {
				  $feed=Feed::fetch($id,false);
				  if($feed===null){dlib_not_found_404();}
				  dcomponent_progress_log_start();
				  $editorUrl=feed_import_create_html_selector_editor($feed);
				  dcomponent_progress_log_end(['redirect'=>$editorUrl!==false ? $editorUrl : $feed->link().'/errors','boxDisplay'=>'fold']);
			  },
			 ];
	$paths[]=['path'=>'@^feed-import/feed/(\d+)/automatic-content-selector$@',
			  'roles' => ['admin','moderator'],
			  'file'=>'feed-import/feed-import-view.php',
			  'function'=>'feed_import_view_feed_automatic_content_selector',
			 ];
	$paths[]=['path'=>'@^feed-import/article/finish-confirm/(all|no-future-date|feed)$@',
			  'roles' => ['admin','moderator'],
			  'file'=>'feed-import/feed-import-view.php',
			  'function'=>'feed_import_view_article_finish_confirm',
			 ];

	$paths[]=['path'=>'@^feed-import/article/(\d+)/view-single$@',
			  'roles' => ['admin','moderator'],
			  'file'=>'feed-import/feed-import-view.php',
			  'function'=>'feed_import_view_article_view_single',
			 ];

	$paths[]=['path'=>'feed-import/searchterm-feed',
			  'roles' => true,
			  'output'=>false,
			  'function'=>'feed_import_searchterm_feed',
			 ];

	$paths[]=['path'=>'@^feed-import/searchterm-feed/article/(\d+)$@',
			  'roles' => true,
			  'output'=>'html',
			  'function'=>'feed_import_searchterm_feed_article',
			 ];

	$paths[]=['path'=>'feed-import/form',
			  'roles' => ['admin','moderator'],
			  'file'=>'feed-import/feed-import-form.php',
			  'function'=>'feed_import_form',
			 ];

	$paths[]=['path'=>'@^feed-import/feed/(\d+)/try-auto-selector$@',
			  'roles' => ['admin','moderator'],
			  'file'=>'feed-import/feed-import-form.php',
			  'function'=>'feed_import_form_feed_try_auto_selector',
			 ];
}

function feed_import_cron()
{
	Feed::updateAll(true);
	Article::cleanupOld();
	Article::finishOldUnreadArticles();
	feed_import_bad_feed_email_warning();
}

//! Parses the xml of a feed and returns an array of feed items. 
//! Each item is an array containing a simplexml object for the item, the link (url) and the guid.
//! This works for several feed types (Atom, RSS).
function feed_import_feed_items($fullXml,$siteUrl,$ignoreEmptyFeedError=false,$errorHandler=false,$fixHttpsToHttp=false)
{
	$xml= simplexml_load_string($fullXml);
	if($xml===false)
	{
		//var_dump($fullXml);
		if(!$errorHandler){return false;}
		$errorHandler("xml parse Feed failed in feed_import_feed_items().\n".
					  "This is strange, normally feed parse should fail earlier...");
	}
	// what type of feed is this ?
	$isAtom=isset($xml->entry);
	$ns=$xml->getNamespaces();
	$isRdf=isset($ns['rdf']);
	// get a list of entries/items
	if(isset($xml->channel->item)){$entries=$xml->channel->item;}
	else
	if($isAtom)                   {$entries=$xml->entry;}
	else
	if($isRdf )                   {$entries=$xml->item;}
	else
	{$entries=false;}

	//var_dump($entries);
	//var_dump(isset($xml->channel->item));
	//var_dump(isset($xml->entry));
	//foreach($xml->entry as $item){echo"hhh<br/>";}		
	if($entries===false){$entries=[];}

	$res=[];
	foreach($entries as $item)
	{
		$guid=$isAtom ? $item->id : $item->guid;
		if($isAtom)
		{
			// several links in atom feeds : choose rel="alternate"
			$link=false;
			foreach($item->link as $l)
			{
				//echo "rel:".$l['rel']." type".$l['type']." href:".$l['href'].".<br/>";
				if($l['rel']=='alternate'){$link=(string)($l['href']);}
			}		
			if($link===false){$link=(string)($item->link['href']);}
		}
		else
		{
			$link=(string)($item->link);
		}
		// some sites put url in guid and not in link!
		if($link==='' && strpos((string)$guid,'http')===0){$link=(string)$guid;}

		// Some sites add spurious whitespace (overblog!!)
		$link=trim($link);
			
		// fix protocol relative links => https
		if(substr($link,0,2)=='//')
		{
			$link='https:'.$link;
		}

		// fix relative links :-(
		if(preg_match('@^[.]?(/.*)$@',$link,$m))
		{
			$link=dlib_host_url($siteUrl)."".$m[1];
		}

		if(!$isAtom && !isset($item->guid))
		{
			//dcomponent_progress_log("no guid, using link");;
			$guid=$link;
		}

		if($fixHttpsToHttp)
		{
			$link=preg_replace('@^https://@','http://',$link);
		}

		// urls should begin with https?:// 
		// Enforce https?:// ... avoid XSS vectors
		if(!preg_match('@^https?://@',$link))
		{
			// .invalid is a reserved tld
			$link='http://url-in-feed-is.invalid/'.$link;
		}

		$res[]=['xml'=>$item,
				'link'=>$link,
				'guid'=>$guid,];
	}
	return $res;
}


function feed_import_create_html_selector_editor($feed)
{
	global $base_url;
	require_once 'html-selector/html-selector.php';
	require_once 'dlib/html-tools.php';
	try{$feed->update();}
	catch(Exception $e)
	{
		require_once 'dlib/filter-xss.php';
		dlib_message_add('Failed to fetch Feed','error');
		return false;
	}
	
	$urls=dlib_array_column($feed->getFeedItems(),'link');
	$urls=array_slice($urls,0,4);
	if(count($urls)===0){dlib_message_add(t('Cannot create selector editor: there are no items in this feed.'),'error');return false;}
	$editor=HtmlSelectorEditor::create($feed->htmlContentSelector,$urls,
									   ['require_once'=>'feed-import/feed-import.php',
										'function'    =>'feed_import_html_selector_finished_callback',
										'data'        =>$feed->id],
									   ['title'=>$feed->name,
										'tab-selector'=>t('Find article'),
										'main-help'=>t('You need to select the main article contents inside this page, excluding any extra information such as left or right columns, page top...  Click inside the area of a page containing the article. Then click on the "grow" button until you get a valid selection that is large enough to fit all of the article. You can check the extracted article in the "Selected result" tab. Don\'t forget to save.'),
										'display-callback' =>['require_once'=>'feed-import/feed-import-view.php',
															  'function'=>'feed_import_view_html_selector_display_callback',
															  'data'=>$feed->id],
										'append-selectors'=>Article::$unwantedPageParts,
									   ]);
	$editor->save();
	$editorUrl=$base_url.'/html-selector/editor/'.$editor->id;
	return $editorUrl;
}

function feed_import_fake_feed_lip_html_selector_editor($ff)
{
	global $base_url;
	require_once 'html-selector/html-selector.php';
	require_once 'dlib/html-tools.php';
	$editor=HtmlSelectorEditor::create($ff->lipHtmlSelector,[$ff->url],
									   ['require_once'=>'feed-import/feed-import.php',
										'function'=>'feed_import_fake_feed_lip_html_selector_finished_callback',
										'data'=>$ff->id],
									   ['title'=>$ff->useFeed()->name,
										'display-callback' =>['require_once'=>'feed-import/feed-import-view.php',
															  'function'=>'feed_import_view_fake_feed_lip_html_selector_callback',
															  'data'=>$ff->id],
										
									   ]);
	$editor->save();
	$editorUrl=$base_url.'/html-selector/editor/'.$editor->id;
	return $editorUrl;
}


function feed_import_html_selector_finished_callback($feedId,$selector)
{
	require_once 'dlib/html-tools.php';
	$feedId=intval($feedId);
	$feed=Feed::fetch($feedId);
	$feed->htmlContentSelector=$selector;
	$feed->save();
	dlib_message_add(t('Html content selector successfully saved.'));

	foreach(Article::fetchList('WHERE finishedReading=0 AND feedId=%d',$feed->id) as $article)
	{
		$article->reset();
		$article->save();
	}

	dcomponent_progress_log_start('html');
	$feed->update();
	$feed->updateArticles();

	dlib_message_add(t('Feed and unread articles updated with new selector.'));

	$feed->screenshotUpdate();
	dcomponent_progress_log_end(['boxDisplay'=>'fold','redirect'=>$feed->link()]);
}

function feed_import_fake_feed_lip_html_selector_finished_callback($ffId,$selector)
{
	$ffId=intval($ffId);
	$ff=FakeFeed::fetch($ffId);
	$ff->lipHtmlSelector=$selector;
	$ff->save();
	dlib_message_add(t('Html content selector successfully saved.'));
	dlib_redirect($ff->useFeed()->link());
}

function feed_import_searchterm_feed()
{
	global $demosphere_config,$base_url;
	if(!isset($_GET['terms']) || strlen(trim($_GET['terms']))===0){dlib_bad_request_400('missing or invalid terms');}
	$terms=$_GET['terms'];
	$terms=explode(',',$terms);

	// *** Build sql query with simple string matches
	$query="SELECT id,title,content,lastFetched,pubDate FROM Article WHERE lastFetched>%d AND (";
	$args=[];
	$args[]=time()-3600*24*30;
	foreach($terms as $term)
	{
		$query.="simpleText LIKE '%% %s %%' OR ";
		$args[]=dlib_simplify_text($term);
	}
	$query=preg_replace('@ OR $@','',$query);
	$query.=') ORDER BY id DESC';
	array_unshift($args,$query);
	$articles=call_user_func_array('db_arrays_keyed',$args);

	// *** Generate XML feed

	$out='';
	$out.='<?xml version="1.0" encoding="utf-8"?>'."\n";
	$out.='<rss version="2.0" '.
		'xml:base="https://demosphere.net" '.
		'xmlns:dc="http://purl.org/dc/elements/1.1/" '.
		'xmlns:dcterms="http://purl.org/dc/terms/">'."\n";
	$out.='<channel>'."\n";
	$out.='<title>searchterm feed</title>'."\n";
	$out.='<link>'.ent($base_url).'</link>'."\n";
	foreach($articles as $id=>$article)
	{
		// cannot use demosphere_safe_domain_url() because this feed is viewed without being logged-in
		$url=$demosphere_config['safe_base_url'].'/feed-import/searchterm-feed/article/'.$id;
		$out.='<item>';
		$out.='<title>'.xent(html_entity_decode($article['title'],ENT_COMPAT,'UTF-8')).'</title>'."\n";
		$out.='<link>'.xent($url).'</link>'."\n";
		$out.='<pubDate>'.date('r', $article['pubDate']>0 ? $article['pubDate'] : $article['lastFetched']).'</pubDate>';
		$out.='<description></description>'."\n";
		$out.='</item>'."\n";
	}
	$out.='</channel>'."\n";
	$out.='</rss>';

	header('Content-Type: application/rss+xml; charset=utf-8');
	echo $out;
}

function feed_import_searchterm_feed_article($articleId)
{
	global $user,$demosphere_config;
	require_once 'dlib/filter-xss.php';
	// cannot use demosphere_safe_domain_check() because the is generated without being logged-in
	if($user->id!=0){dlib_permission_denied_403();}
	if(strpos(dlib_current_url(),$demosphere_config['safe_base_url'])!==0){dlib_permission_denied_403('This page must be viewed on safe domain');}
	$out='';
	$article=Article::fetch($articleId);
	$out.=$article->content;
	$shortUrl=mb_substr($article->url,0,45,'UTF-8');
	if($shortUrl!=$article->url){$shortUrl.='...';}
	$out.='<p>'.t('_source_').' : '.'<a href="'.ent(u($article->url)).'">'.ent($shortUrl).'</a></p>';
	return $out;
}

//! Send an email to demosphere admin when feeds have been bad for a long time.
function feed_import_bad_feed_email_warning()
{
	global $feed_import_config,$base_url;
	$badFeedsIds=
		db_one_col("SELECT id,name FROM Feed WHERE status=0 AND lastOk<%d AND hideErrorsUntil<%d",
					 time()-3600*24*7,time());
	if(count($badFeedsIds)===0){return;}
	
	// dont send emails too often
	$last=$feed_import_config['tmp_dir'].'/feed_import_bad_feed_email_warning';
	if(file_exists($last) && filemtime($last)> time()-3600*24*7)
	{
		echo "Not sending (too soon): feed_import_bad_feed_email_warning<br/>\n";
		return;
	}

	$siteName=preg_replace('@^https?://([^/]*)/.*$@','$1',$feed_import_config['dir_url']);
	$body='';
	$body.="Bad feeds on site: ".$siteName."\n";
	foreach($badFeedsIds as $id=>$name)
	{
		$body.=$name.": ".$base_url.'/feed-import/feed/'.intval($id)."\n";
	}

	require_once 'lib/class.phpmailer.php';
	global $demosphere_config;
	$mail=new PHPMailer();
	$mail->CharSet="utf-8";
	$mail->SetFrom($demosphere_config['contact_email']);
	$mail->AddAddress('admin@demosphere.net');
	$mail->Subject=count($badFeedsIds).' bad feeds on '.$siteName;
	$mail->Body=$body;
	$mail->AddCustomHeader('List-Id','<badfeeds.demosphere.net>');
	if(!$mail->Send()){echo "Failed sending feed_import_bad_feed_email_warning<br/>\n";}
	echo $last;
	touch($last);
}

function feed_import_text_matching_hook($op,$arg1=null)
{
	global $feed_import_config,$currentPage,$base_url;

	// operations that don't need full $article
	switch($op)
	{
	case 'allTids': return db_one_col("SELECT Article.id FROM Article WHERE ".
									  "Article.lastFetched > %d",time()-3600*24*7*2);
	case 'displayListInit':
		$currentPage->addCssTpl('demosphere/feed-import/article-item.tpl.css');
		require_once 'dlib/filter-xss.php';
		return;
	case 'isExpired':
		$lastFetched=(int)db_result_check('SELECT lastFetched FROM Article WHERE id=%d',$arg1);
		return $lastFetched < time()-3600*24*7*2;
	case 'matchInfo': 
		global $base_url;
		$articleData=db_array('SELECT title,isHighlighted,finishedReading FROM Article WHERE Article.id=%d',$arg1);
		if($articleData===null){return [];}
		return ['class'=>$articleData['finishedReading'] ? 'old ' : 'recent ',
				'title'=>$articleData['title'],
				'url'=>$base_url.'/feed-import/article/'.intval($arg1).'/view-single',
			   ];
	}

	$article=Article::fetch($arg1,false);

	switch($op)
	{
	case 'getContents':
		if($article===null){return false;}
		return [$article->getTmIndexContent(),'html'];
	case 'displayInList': 
		if($article===null){return '<p>message '.intval($arg1).' does not exist</p>';}
		require_once 'feed-import-view.php';
		require_once 'dlib/template.php';
		$vars=['item'=>$article,
			   $article->viewData(),
			   'scriptUrl'=>$base_url.'/feed-import'];
		$parts=template_render_parts('article-item.tpl.php',$vars);
		echo template_render('dcomponent/dcitem.tpl.php',array_merge($vars,$parts));
		return;
	}
}

function feed_import_log($msg)
{
	global $base_url,$feed_import_config;
	// only log on local test site (??? too much log? check...FIXME)
	if(strpos($base_url,'demolocal')!==false)
	{
		dlib_log($msg,$feed_import_config['log_file']);
	}
}
function feed_import_log_pos()
{
	global $feed_import_config;
	return filesize($feed_import_config['log_file']);
}
function feed_import_log_get($start)
{
	global $feed_import_config;
	// This fails as file_get_contents wants to read the whole file into memory,
	// even though the offset is set. (This looks like a php bug).
	// file_get_contents($feed_import_config['log_file'],false,NULL,$start);

	$f=fopen($feed_import_config['log_file'],"r");
	fseek($f,$start);
	$res=fread($f,1000000);
	fclose($f);
	return $res;
}


//! Returns html with contents of a simplexml feed item. 
//! Checks for different types of descriptions (different for rss, atom ... and other uses)
//! $fieldChoose can be  'full' or 'bestField'
//!  - 'full' builds a result from multiple fields and includes attached media files ("enclosures" : images, pdf)
//!  - 'bestField' returns a single rss item field (the first non empty of: 'encoded','content','description','summary')
//! This has been tested on a lot of real feeds.
//! Most common cases (order of frequency):
//! - description+encoded (generaly description is included at the bbegining of encoded, but not always)
//! - description only
//! - content alone
//! - encoded alone (rare)
//! - content+summary (rare)
function feed_import_feed_item_description($item,$fieldChoose='full')
{
	require_once 'dlib/html-tools.php';

	// *** Build a list of fields in this item
	$found=[];
	foreach(['encoded','content','description','summary'] as $field)
	{
		if($field==='encoded')
		{
			if(!isset($item->children('http://purl.org/rss/1.0/modules/content/')->encoded)){continue;}
			$html    =$item->children('http://purl.org/rss/1.0/modules/content/')->encoded->__toString();
		}
		else
		{
			if(!isset($item->$field)){continue;}
			$html=(string)$item->$field;
		}
		$html=trim($html);
		if(($field=='summary' || $field==='content') && $item->summary['type']=='text'){$html=ent($html);}
		if(strlen($html)<9){continue;}

		if($fieldChoose==='bestField'){return $html;}
		$text=trim(dlib_dom_from_html($html)->textContent);
		$found[$field]=['html'=>$html,'text'=>$text,'simple'=>dlib_simplify_text($text)];
	}
	if($fieldChoose==='bestField'){return false;} 

	// ** Choose fields and merge if needed
	$res='';
	if(isset($found['encoded']) && isset($found['description']))
	{
		// two cases: 
		// 1) most common: 'description' is more or less a shortened version of 'encoded' => use 'encoded'
		// 2) rare: 'description' is first part followed by 'encoded' => use 'description' + 'encoded'
		if(mb_substr($found['encoded']['simple'],0,50)===mb_substr($found['description']['simple'],0,50))
		{
			$res=$found['encoded']['html'];
		}
		else
		{
			$res='<div>'.$found['description']['html'].'</div>'.
				 '<div>'.$found['encoded'    ]['html'].'</div>';
		}
	}
	else
	if(count($found)===0){$res='';}
	else
	{
		$res=dlib_first($found)['html'];
	}

	// *** Add attached files (images, pdf)
    $enclosuresHtml='';	
    foreach($item->enclosure as $enclosure)
	{
		if(strpos($enclosure['type'],'image/')===0)
		{
			$enclosuresHtml.='<div><img src="'.ent(u($enclosure['url'])).'"/></div>';
		}
		else
		{
			$enclosuresHtml.='<div><a href="'.ent(u($enclosure['url'])).'">'.ent($enclosure['type']).'</a></div>';
		}
	}

	return $res.$enclosuresHtml;
}


function feed_import_install()
{
	require_once 'Feed.php';
	require_once 'Article.php';
	require_once 'FakeFeed.php';

	Feed::createTable();
	Article::createTable();
	FakeFeed::createTable();
}

/** Check if config is ok, this is called during install and from cron.
 * @see http://api.drupal.org/api/function/hook_requirements
 * @param phase can be 'install' or 'runtime'. Some checks are different depending on phase.
 * @param the array that will be filled 
 */
function feed_import_config_check($phase,&$req)
{
	global $feed_import_config;
	global $dlib_config;
	global $template_config;

	if($phase==='install'){return;}

	if(!isset($feed_import_config)){fatal("feed_import_config_check_fast: no config!");}

	if(!isset($feed_import_config['tmp_dir']))
	{fatal("feed_import_config_check_fast: tmp_dir not set");}

	if(!isset($template_config['dir'])){fatal("feed_import_config_check_fast: template_config['dir'] is not set");}

	dcomponent_config_check_file('feed-import:tmp_dir',$req,
							 $feed_import_config['tmp_dir']);

}

/** @} */

?>