<? // ************** info-left ******** ?>
<? begin_part('infoLeftBottom') ?>
	<li class="infoPart feedname">
		<a href="$feed->link()">$feed->name
			<? if($feed->comment!=''){?><span class="feed-comment" title="$feed->comment"><?: preg_replace("@\n.*@s",'',$feed->comment) ?></span><?}?>
		</a>
	</li>

	<li class="infoPart import">
		<a target="_blank" href="$scriptUrl/article/$item->id/import-to-demosphere">
		_(import)
		</a>
	</li>

	<li class="infoPart alink">
		<a href="<?: u($alink) ?>"></a>
	</li>

	<? if($item->pubDate){?>
		<li class="infoPart pubdate 
					<?:  $item->pubDate && $item->pubDate<time()-3600*24*90 ? 'old-date' :'' ?>" 
			title="<?: t('published !time',['!time'=>dcomponent_format_date('full-date-time',$item->pubDate)])?>">
			<?: $item->pubDate ? 
				dcomponent_format_date('relative-short-full',$item->pubDate): 
				t('bad-pubdate') ?>
		</li>
	<?}?>

	<? if(!$item->lastFetched || abs($item->lastFetched-$item->pubDate)>3600*12){ ?>
		<li class="infoPart lastFetched" title="<?: t('downloaded !time',['!time'=>dcomponent_format_date('full-date-time',$item->lastFetched)])?>">
			<?: $item->lastFetched ?
				dcomponent_format_date('relative-short',$item->lastFetched) :
				t('not downloaded') ?>
		</li>
	<?}?>

	<? if($item->finishedReading){?>
		<li class="infoPart">
			_(finished at:)<?: dcomponent_format_date('shorter-date-time',$item->finishedReading) ?>
		</li>
	<?}?>

	<? if(!$item->downloadStatus){ ?>
		<li class="infoPart reset">
			<a href="$scriptUrl/article/$item->id/reset" title="_(reset)">
			</a>
		</li>
	<?}?>

	<li class="infoPart more-info">
		<a href="$scriptUrl/article/$item->id/details">+</a>
	</li>
<? end_part('infoLeftBottom') ?>
