<?php
/** \addtogroup feed_import 
 * @{ */
require_once 'dlib/template.php';

//! Setup common variables, menus and tabs for the feed-import GUI
//! This is called on every page displayed in the feed-import interface
function feed_import_view($feed=false,$article=false,array $topRenderVars=[])
{
	global $feed_import_config,$currentPage,$base_url;
	require_once 'dcomponent/dcomponent-common.php';	
	require_once 'dlib/filter-xss.php';

	if($article!==false && $feed===false){$feed=$article->getFeed();}

	dcomponent_page_css_and_js();

	$scriptUrl=$base_url.'/feed-import';
	$currentPage->addJsVar('feedImportScriptUrl',$scriptUrl);

	$currentPage->addCssTpl('demosphere/feed-import/article-item.tpl.css');
	$currentPage->addCssTpl('demosphere/feed-import/feed-import.tpl.css');
	$currentPage->addJs    ('demosphere/feed-import/feed-import.js');

	$currentPage->addJsTranslations(
		[
			'unsaved-in-feed-edit-form'=>t('You have changed things on this page without saving. You need to save before changing the selector.'),
		]);

	$currentPage->bodyClasses[]="feedImport"; 

	feed_import_view_title($feed,$article);


	// Top of page, with menus, that is common to all feed-import pages
	if(!val($topRenderVars,'noTop'))
	{
		$currentPage->addJs ('dlib/dropdown.js');
		$currentPage->addCss('dlib/dropdown.css');

		// error counts for top menu
		$badFeedIds=
			db_one_col("SELECT id,name FROM Feed WHERE status=0 AND lastOk<%d AND hideErrorsUntil<%d",
					   time()-3600*24,time());
		$badArticleIds=
			db_one_col("SELECT id FROM Article WHERE downloadStatus=0 AND finishedReading=0");

		// finish article counts for dropdown in top menu
		$finishNoFutureDate=(int)db_result("SELECT COUNT(*) FROM Article WHERE finishedReading=0 AND downloadStatus!=0 AND firstFutureHLDate<%d",time());
		$finishAll         =(int)db_result("SELECT COUNT(*) FROM Article WHERE finishedReading=0 AND downloadStatus!=0");
		$finishFeed=false;
		if($feed!==false)
		{
			$finishFeed    =(int)db_result("SELECT COUNT(*) FROM Article WHERE finishedReading=0 AND downloadStatus!=0 AND feedId=%d",$feed->id);
		}

		$currentPage->dcomponentMenubar=
			template_render('feed-import-top.tpl.php',
							[compact('scriptUrl','badFeedIds','badArticleIds','feed','article',
									 'finishNoFutureDate','finishAll','finishFeed'),
							 $topRenderVars]);
	}

	// *** tabs for all pages of a specific feed 
	if($feed!==false && $article===false && !val($topRenderVars,'noTop'))
	{
		$tabs=[];
		$tabs['name'   ]=['text'=>$feed->name  ,'href'=>$feed->link()]; // not really a tab... just display feed's name
		$tabs['view'   ]=['text'=>t('View'    ),'href'=>$feed->link()];
		$tabs['edit'   ]=['text'=>t('Edit'    ),'href'=>$feed->link().'/edit'];
		$tabs['details']=['text'=>t('Details' ),'href'=>$feed->link().'/details'];
		$hasBadArticles=db_result("SELECT COUNT(*) FROM Article WHERE downloadStatus=0 AND finishedReading=0 AND feedId=%d",$feed->id);
		$tabs['errors' ]=['text'=>t('Errors'  ),'href'=>$feed->link().'/errors',
						  'class'=>(!$feed->status  || $hasBadArticles ? 'has-errors' : '')]; 

		foreach($tabs as $k=>$tab)
		{
			$isActive=preg_match('@^'.preg_quote(substr($tab['href'],strlen($base_url)),'@').'([?&]|$)@',$_SERVER['REQUEST_URI']);;
			if($isActive){$tabs[$k]['active']=true;}
		}

		$currentPage->tabs=$tabs;
	}

	// Common variables used in templates
	return compact('scriptUrl');
}

//! Automatically sets $currentPage->title
function feed_import_view_title($feed,$article)
{
	global $currentPage;
	$title='fi-';
	if(preg_match('@feed-import(/(feed|article|fake-feed)/\d+)?/([^/?&]+)@',val($_GET,'q'),$matches))
	{
		$title.=$matches[3];
	}
	if($article!==false){$title.=': '.$article->title;}
	if($feed   !==false){$title.=': '.$feed->name;}
	if($title==='fi-'){$title=t('feed-import');}
	$currentPage->title=$title;
}

//! The main display in feed-import: a list of articles waiting to be read (finished)
function feed_import_view_main()
{
	variable_set('feed-import-last-view-articles','',time());

	Article::reHighlightIfNeededAll();

	$pager=new Pager(['itemName'=>t('articles'),'alwaysShow'=>true]);
	$articles=Article::fetchList('SELECT SQL_CALC_FOUND_ROWS * FROM Article WHERE '.
								 "Article.finishedReading=0 AND ".
								 "Article.downloadStatus!=0     ".
								 " ORDER BY firstFutureHLDate=0 ASC, firstFutureHLDate ASC, isHighlighted DESC, id ASC ".
								 $pager->sql());
	$pager->foundRows();

	return template_render('article-list.tpl.php',
						   [feed_import_view(),
							compact('articles','pager')]);
}

//! Display a list of all feeds that have errors, with information about current errors.
function feed_import_view_errors()
{
	$feeds=Feed::fetchList("WHERE status=0 ORDER BY hideErrorsUntil<%d DESC, lastOk DESC",time());
	
	return '<h1 class="page-title">'.t('Feeds with errors').'</h1>'.
		template_render('feed-error-list.tpl.php',
						[feed_import_view(),
						 compact('feeds')]);	
}

//! Display a single feed
//! Simplified display, with most its important attributes and all of it's unread articles.
function feed_import_view_feed($feedId)
{
	global $base_url,$currentPage;
	$currentPage->addCssTpl('demosphere/feed-import/feed.tpl.css');

	$feed=Feed::fetch($feedId,false);
	if($feed===null){dlib_not_found_404();}

	$pager=new Pager(['itemName'=>t('articles'),'alwaysShow'=>true]);
	$articles=Article::fetchList("SELECT SQL_CALC_FOUND_ROWS * FROM Article WHERE ".
								 "Article.feedId=%d AND ".
								 "Article.finishedReading=0 AND ".
								 "Article.downloadStatus!=0     ".
								 " ORDER BY downloadStatus!=0 ASC, firstFutureHLDate=0 ASC, firstFutureHLDate ASC, isHighlighted DESC, id ASC ".
								 $pager->sql(),
								 $feed->id);
	$pager->foundRows();
	
	$nbErrorArticles=db_result("SELECT COUNT(*) FROM Article WHERE ".
							   "Article.feedId=%d AND ".
							   "Article.finishedReading=0 AND ".
							   "Article.downloadStatus=0 ",
							   $feed->id);
	
	foreach($articles as $article){$article->reHighlightIfNeeded();}

	$fakeFeed=$feed->getFakeFeed();

	$stats=$feed->stats();

	$latestFinished=Article::fetchList("WHERE ".
									   "Article.feedId=%d AND ".
									   "Article.finishedReading!=0 ".
									   "ORDER BY Article.finishedReading DESC ".
									   "LIMIT 3 ",$feed->id);

	$latestArticle=db_result("SELECT MAX(lastFetched) FROM Article WHERE downloadStatus!=0 AND feedId=%d",$feed->id);

	return template_render('feed.tpl.php',
						   [feed_import_view($feed),
							compact('feed','articles','pager','nbErrorArticles','fakeFeed','stats','latestFinished','latestArticle'),
							'ff'=>$feed->getFakeFeedId(),
						   ]);
}

//! Displays all fields of a Feed using dbobject_ui_view() 
//! Also displays some of FakeFeed fields, if needed. 
function feed_import_view_feed_details($feedId)
{
	global $base_url;
	$classInfo=[];
	$classInfo['main_url']=$base_url.'/feed-import/feed';
	$classInfo['view']=function(&$display,$feed)
	{
		global $base_url,$currentPage;
		require_once 'dlib/html-tools.php';

		// Selectors
		$currentPage->addCss('dlib/table-tools.css');
		$currentPage->addCssTpl('demosphere/feed-import/feed.tpl.css');
		$display['htmlContentSelector']['disp']=dlib_html_selectors_display($feed->htmlContentSelector);

		// Also show Fakefeed inside Feed view
		$fakeFeed=$feed->getFakeFeed();
		if($fakeFeed!==null)
		{
			$ffClassInfo=
				[
					'view_vars'=>function(){return ['topText'=>'<h3>FakeFeed:</h3>'];},
					'view'=>function(&$display,$fakeFeed)
					{
						// Remove fields that are not used by this FakeFeed type
						$fields=FakeFeed::typeFields();
						$unusedFields=array_diff(call_user_func_array("array_merge",$fields),$fields[$fakeFeed->getType()]);
						$display=array_diff_key($display,array_flip($unusedFields));
						unset($display['feedId']);
					},
				];
			$display['url']['after']=dbobject_ui_view('FakeFeed',$fakeFeed->id,false,$ffClassInfo);
		}

		// Error log
		if(count($feed->errors))
		{
			$display['errors']['disp']='<div id="feed-error-log-wrap">'.feed_import_view_error_log($feed).'</div>';
		}
		else
		{
			$display['errors']['disp']=t('none');
		}
		$display['errors']['description']='';

		// Do this after FakeFeed view, to override tabs set by FakeFeed view.
		feed_import_view($feed);
	};

	return dbobject_ui_view('Feed',$feedId,false,$classInfo);
}

//! Confirm delete feed form
function feed_import_view_feed_delete($id)
{
	global $base_url;
	$feed=Feed::fetch($id,false);
	if($feed===null){dlib_not_found_404();}

	$fakeFeed=$feed->getFakeFeed();

	feed_import_view($feed);
	require_once 'dlib/form.php';

	$items=[];
	$items['title']=['html'=>'<h3>'.t('Delete').'</h3><p>'.t('Are you sure you want to delete this Feed and all of its articles ?').'</p>'];

	$items['delete']=['type'=>'submit',
					  'value'=>'delete',
					  'submit'=>function()use($feed){$feed->delete();},
					  'redirect'=>$base_url.'/feed-import',
					 ];
	$items['cancel']=['type'=>'submit',
					  'value'=>'cancel',
					  'redirect'=>$feed->link(),
					 ];
	$items['advanced-fieldset']=
		[
			'type' => 'fieldset',
			'title' => ent(t('Advanced')),
			'collapsible' => true,
			'collapsed' => true,
		];

	$items['delete-articles']=
		[
			'type'=>'submit',
			'value'=>t('delete articles, not feed'),
			'submit'=>function()use($feed)
			{
				$ids=db_one_col("SELECT id FROM Article WHERE feedId=%d",$feed->id);
				dcomponent_search_proxy_call_catch('demosphere_search_dx_delete_documents','log','feed',$ids);

				db_query("DELETE FROM Article WHERE feedId=%d",$feed->id);

				dlib_message_add(t('All Articles successfully deleted for feed !id.',['!id'=>$feed->id]));
				dlib_redirect($feed->link());
			}
		];
	$items['advanced-fieldset-end']=['type' => 'fieldset-end'];

	return form_process($items);
}

//! Returns HTML displaying a screenshot and selector of a feed
//! Also updates screenshot if needed.
function feed_import_view_screenshot($feed,$imgLink=false)
{
	global $currentPage;
	$currentPage->addCss('dlib/table-tools.css');

	if($feed->screenshotNeedsUpdate())
	{
		try
		{
			$ok=$feed->screenshotUpdate();
			$feed->save();
		}
		catch(Exception $e)
		{
			dlib_message_add('Screenshot update failed','error');
			dlib_log_error('Screenshot update failed:'.$e->getMessage(),$e->getTrace());
			$ok=false;
		}
		if(!$ok){return '';}
	}
	if(!file_exists($feed->screenshotFile())){return '';}
	$out='';
	$out.='<div class="screenshot">';
	if($imgLink!==false){$out.='<a href="'.u($imgLink).'">';}
	$out.='<img src="'.ent($feed->screenshotImgUrl()).'" alt="screenshot"/>';
	if($imgLink!==false){$out.='</a>';}
	$width =150;
	$height=195;
	if(isset($feed->data['screenshot']['selbox']))
	{
		$box=$feed->data['screenshot']['selbox'];
		$out.='<div class="selbox">';
		if($box['x0']>=0 && $box['x0']<=$width ){$out.='<div style="left:'.round($box['x0'],1).'px;top:-10px;width:0;height:'.($height+20).'px"></div>';}
		if($box['x1']>=0 && $box['x1']<=$width ){$out.='<div style="left:'.round($box['x1'],1).'px;top:-10px;width:0;height:'.($height+20).'px"></div>';}
		if($box['y0']>=0 && $box['y0']<=$height){$out.='<div style="left:-10px;top:'.round($box['y0'],1).'px;width:'.($width+20).'px;height:0"></div>';}
		if($box['y1']>=0 && $box['y1']<=$height){$out.='<div style="left:-10px;top:'.round($box['y1'],1).'px;width:'.($width+20).'px;height:0"></div>';}
		$out.='</div>';
	}
	// Display compacted contentSelector
	if(!$feed->getContentFromFeedXml && count($feed->htmlContentSelector))
	{
		require_once 'dlib/html-tools.php';
		$sel=$feed->htmlContentSelector;
		foreach($sel as &$v){$v['type']=str_replace(['XPATH-REMOVE','ERASE','XPATH','CSS-REMOVE','CSS'],['-','-','+','-','+'],$v['type']);}
		$out.=dlib_html_selectors_display($sel);
	}
	$out.='</div>';
	return $out;
}

//! User friendly form for editing a Feed and it's FakeFeed
function feed_import_view_feed_edit($id)
{
	global $base_url;
	$feed=Feed::fetch($id,false);
	if($feed===null){dlib_not_found_404();}

	$fakeFeed=$feed->getFakeFeed();

	feed_import_view($feed);
	require_once 'dlib/form.php';

	list($feedItems,$feedFormOpts)=form_dbobject($feed,true);
	if($fakeFeed!==null)
	{
		list($ffItems,$ffFormOpts)=form_dbobject($fakeFeed,true);
	}
	feed_import_view_feed_class_info($classInfo);
	feed_import_view_fake_feed_class_info($ffClassInfo);

	$items=[];
	$items['advanced']=['html'=>'<a id="advanced-edit-link" href="'.ent($feed->link().'/edit-advanced').'">'.t('advanced').'</a>'];
	$items['name']=$feedItems['name'];
	$items['name']['required']=true;
	if($fakeFeed===null)
	{
		$items['url' ]=$feedItems['url' ];
		$items['url' ]['required']=true;
		// Make sure user is not changing type of feed (managing that would be a bit complicated, just ask user to create a new feed)
		// This also guesses the right url (ex: user enters site, not feed)
		$items['url']['validate']=function(&$url)
			{
				require_once 'feed-import-form.php';
				$guessed=feed_import_form_guess_feed_type($url);
				if(isset($guessed['error'])){return $guessed['error'];}
				if($guessed['type']!=='normal' ||
				   (val($guessed,'getContentFromFeedXml')==true && !$feed->getContentFromFeedXml))
				{
					return t('Cannot change feed type to "@type". If you want a feed of another type, it is better to create a new feed.',
							['@type'=>$guessed['type']]);
				}
				$url=$guessed['url'];
			};
	}
	else
	{
		$items['fakeFeed-fieldset']=
			[
				'type' => 'fieldset',
				'title' => ent(t('Special feed:').' '.$fakeFeed->getType()),
			];

		$items['fakeFeed-debug']=['html'=>
		 			'<a id="fake-feed-debug-link" target="_blank" href="'.ent($base_url.'/feed-import/fake-feed/'.$fakeFeed->id.'/debug').'">'.
		 						  t('test').'</a>'];
		// Wait for form save if requested (see feed_import_view_fake_feed_debug())
		$items['fakeFeed-debug-wait-for-save']=['type'=>'hidden',
											 'default-value'=>'',
											 'pre-submit'=>function($v)
			                                 {
												 if(!ctype_digit($v)){return;}
												 // cleanup old
												 $all=variable_get_all('fakeFeed-debug-wait-for-save');
												 if(count($all) && max($all)<time()-100)
												 {
													 variable_delete(false,'fakeFeed-debug-wait-for-save');
												 }
												 variable_set($v,'fakeFeed-debug-wait-for-save',time());
												 dlib_message_add(t('Feed saved before debug.'));
											 },
											];
		$items['fakeFeed-url']=$ffItems['url'];
		$items['fakeFeed-url']['required']=true;
		$items['fakeFeed-url']['description']=val(['links-in-page'=>t('The URL of the page where links should be extracted.'),
												   'page-parts'   =>t('The URL of the page that should be split into parts.'),
												  ],$fakeFeed->getType(),
												  t('A URL of a page that represents this feed. This is just informative, you can set whatever you want.'));
		switch($fakeFeed->getType())
		{
			case 'custom-code':
				$items['fakeFeed-ccPhpCode']=$ffItems['ccPhpCode'];
				$items['fakeFeed-ccPhpCode']['type']='textarea';
				$items['fakeFeed-ccPhpCode']['attributes']['cols']='100';
				$items['fakeFeed-ccPhpCode']['attributes']['rows']='10';
				break;
			case 'links-in-page':
				$items['fakeFeed-lipHtmlSelector']=
					[
						'html'=>'<div id="lipHtmlSelector-item" class="form-item">'.
						'<label>'.ent($ffClassInfo['field_desc']['lipHtmlSelector']['title']).'</label>'.
						'<a id="fakeFeed-lipHtmlSelector" href="'.ent($base_url.'/feed-import/fake-feed/'.$fakeFeed->id.'/lip-html-selector-editor').'">'.
						    t('configure').'</a>'.
						'<div class="description">'.ent($ffClassInfo['field_desc']['lipHtmlSelector']['description']).'</div>'.
						'</div>'
					];
				$items['fakeFeed-lipLinkRegexp']=$ffItems['lipLinkRegexp'];
				$hostRegex='@^'.preg_quote(dlib_host_url($items['fakeFeed-url']['default-value']),'@').'@';
				$items['fakeFeed-lipLinkRegexp']['field-prefix']='<ul id="lip-filters">'.
					'<li><label><input type="radio" name="lip-filter"/> '.t('Only keep links starting with:').
					' <input id="lipLinkStarts" size="60" autocomplete="off" pattern="https?://.*"/></label></li>'.
					'<li><label><input type="radio" name="lip-filter"/> <span id="lip-filter-advanced">'.t('Advanced').'</span>'.
					'<span>: '.t('Only keep links matching:').' ';
				$items['fakeFeed-lipLinkRegexp']['field-suffix']='</label></span></li></ul>';
				$items['fakeFeed-lipLinkRegexp']['attributes']['pattern']='@.*@[a-df-zA-DF-Z]*';
				$items['fakeFeed-lipLinkRegexp']['validate-regexp']='#^@.*@[a-df-z]*$#i';
				$items['fakeFeed-lipLinkRegexp']['pre-submit']=function(&$v)use($hostRegex)
					{
						if($v===$hostRegex || $v==='@^@'){$v='';}
					};
				break;
			case 'icalendar':
				$items['fakeFeed-iCalUrl']=$ffItems['iCalUrl'];
				break;
			case 'page-parts':
				$items['fakeFeed-ppCssSelector']=$ffItems['ppCssSelector'];
				$items['fakeFeed-ppSplitRegex']=$ffItems['ppSplitRegex'];
				break;
			case 'facebook':
				$items['fakeFeed-fbId']=$ffItems['fbId'];
				break;
		}
		$items['fakeFeed-fieldset-end']=['type' => 'fieldset-end'];
	}

	if($fakeFeed!==null && $fakeFeed->getType()==='icalendar')
	{
		$items['getContentFromFeedXml']=$feedItems['getContentFromFeedXml'];
		$items['getContentFromFeedXml']['title']=t('Use iCalendar description');
		$items['getContentFromFeedXml']['description']=t('A description of the event is normally provided in the calendar. However, it is sometimes too short. In that case, you can deselect this to use the links provided in the calendar. You will also need to configure a content selector.');
	}

	if((!$feed->getContentFromFeedXml ||
		($fakeFeed!==null && $fakeFeed->getType()==='icalendar')) && 
	   ($fakeFeed===null || 
		$fakeFeed->getType()==='custom-code' ||
		$fakeFeed->getType()==='links-in-page' ||
		$fakeFeed->getType()==='icalendar'
	   ))
	{
		$items['selector']=['html'=>'<div id="selector-item" class="form-item">'.
							feed_import_view_screenshot($feed).
							'<label>'.$classInfo['field_desc']['htmlContentSelector']['title'].'</label>'.
							'<a id="selector-manual" href="'.ent($feed->link().'/configure-content-selector').'">'.t('Manual').'</a>'.
							'<a id="selector-auto"   href="'.ent($feed->link().'/automatic-content-selector').'">'.t('Automatic').'</a>'.							
							'<div class="description">'.$classInfo['field_desc']['htmlContentSelector']['description'].
							t('The automatic option usually works, but if it fails you will need to use the manual option.').
							'</div>'.
							'</div>'];
	}
	$items['comment']=$feedItems['comment'];
	$items['comment']['type']='textarea';
	$items['comment']['attributes']['cols']='100';
	$items['email']=$feedItems['email'];
	$items['save']=$feedItems['save'];
	$items['save']['redirect']=$feed->link();
	// Also save FakeFeed items using std submit hook provided by form_dbobject()
	if($fakeFeed!==null)
	{
		$items['save']['submit']=function($items)use($ffItems,$feed)
		{
			$ffItemsTmp=[];
			foreach($items as $name=>$item)
			{
				if(strpos($name,'fakeFeed-')===0){$ffItemsTmp[substr($name,strlen('fakeFeed-'))]=$item;}
			}
			$ffItemsTmp['save']=$ffItems['save'];
			$ffItemsTmp['save']['submit-2']($ffItemsTmp,'save');
			dlib_redirect($feed->link().($items['fakeFeed-debug-wait-for-save']['value']==='' ? '' : '/edit'));
		};
	}
	$items['delete']=
		['type'=>'submit',
		 'value'=>t('Delete'),
		 'skip-processing'=>true,
		 'redirect'=>$feed->link().'/delete',
		];

	return form_process($items,['id'=>'feed-edit']);

}

function feed_import_view_fake_feed_debug($id)
{
	// Wait for feed form to finish saving
	$token=val($_GET,'wait-for-save');
	if(ctype_digit($token))
	{
		for($i=0;$i<30;$i++)
		{
			if(variable_get($token,'fakeFeed-debug-wait-for-save')!==false){break;}
			usleep(100000);
		}
	}

	ob_start();
	echo '<div id="fake-feed-debug">';
	echo '<h1 class="page-title">'.t('Special feed debug').'</h1>';
	$ff=FakeFeed::fetch($id,false);
	if($ff===null){dlib_not_found_404();}
	$ff->feedXml(true);
	echo '</div>';
	$out=ob_get_contents();
	ob_end_clean();
	require_once 'feed-import-view.php';
	feed_import_view($ff->useFeed());
	return $out;
}

//! "Advanced" edit form for Feed using dbobject_ui_edit() 
function feed_import_view_feed_edit_advanced($feedId)
{
	global $base_url;
	$classInfo=[];
	feed_import_view_feed_class_info($classInfo);
	$classInfo['main_url']=$base_url.'/feed-import/feed';
	$classInfo['edit_form_alter']=
		function(&$items,&$options,$feed)use($classInfo)
		{
			feed_import_view($feed);

			$items['status']['type']='custom';
			$items['status']['custom']=function($value){return $value ? 'true':'false';};
			$items['status']['value']=$feed->status;

			$items['lastOk']['type']='custom';
			$items['lastOk']['custom']=function($value){return date('r',$value);};
			$items['lastOk']['value']=$feed->lastOk;

			$items['hideErrorsUntil']['type']='int';
			$items['hideErrorsUntil']['default-value']='';
			$items['hideErrorsUntil']['pre-submit']=function(&$v)use($feed){$v=$v!='' ? strtotime('+'.$v.' days') : $feed->hideErrorsUntil;};

			$items['comment']['type']='textarea';

			$selTypes=
			[
				'CSS'         =>t('CSS selector'  ),
				'XPATH'       =>t('XPATH selector'),
				'ERASE'       =>t('Erase regex'   ),
				'CSS-REMOVE'  =>t('CSS remove'    ),
				'XPATH-REMOVE'=>t('XPATH remove'  ),
			];
			$selItem=['title'=>$classInfo['field_desc']['htmlContentSelector']['title'],
					  'type'=>'array_of_arrays',
					  'aofa-options'=>[['type','type','select','values'=>$selTypes],['arg','arg']],
					  'default-value'=>$feed->htmlContentSelector,
					  'description'=>$classInfo['field_desc']['htmlContentSelector']['description'].' '.
					  '<a href="'.ent($feed->link().'/configure-content-selector').'">'.t('Graphical interface').'</a>',
					 ];
			$items=dlib_array_insert_assoc($items,'url','htmlContentSelector',$selItem,true);
			unset($items['fullXml']);
		};
	return dbobject_ui_edit('Feed',$feedId,$classInfo);
}

//! Updates a feed and its article, and shows message with status.
function feed_import_view_feed_update($id)
{
	ini_set('max_execution_time', max(1200,ini_get('max_execution_time')));
	$feed=Feed::fetch($id,false);
	if($feed===null){dlib_not_found_404();}
	dcomponent_progress_log_start();
	try
	{
		$feed->update();
		$status=$feed->updateArticles();
		foreach($status as $type=>$ct)
		{
			if($ct>0)
			{
				$messages=['ignored'=>t('skipped @nb already downloaded articles',['@nb'=>$ct]),
						   'updated'=>t('@nb articles successfully fetched.'      ,['@nb'=>$ct]),
						   'error'  =>t('@nb articles failed'                    ,['@nb'=>$ct]),];
				dlib_message_add($messages[$type],$type==='error' ? 'error':'status');
			}
		}
	}
	catch(Throwable $e)
	{
		if(isset($e->demosphere_is_feed_error))
		{
			dlib_message_add(t('Feed update failed.').
							 '<p>'.ent(feed_import_view_error_message($feed,'short_message')).'</p>'.
							 '<p style="font-size: 80%;">'.ent(feed_import_view_error_message($feed,'message')).'</p>','error');
		}
		ob_start();
		dlib_debug_exception_display($e);
		$message=ob_get_contents();
		ob_end_clean();
		dcomponent_progress_log($message,'error',true);
	}
	dcomponent_progress_log_end(['redirect'=>$feed->link(),'boxDisplay'=>'fold']);
}

//! Tries to automatically determine a content selector for a feed.
//! Progress is displayed and then it redirects to feed display (or edit if fail). 
function feed_import_view_feed_automatic_content_selector($id)
{
	require_once 'feed-import/feed-import-form.php';
	$feed=Feed::fetch($id,false);
	if($feed===null){dlib_not_found_404();}
	$report=[];
	echo '<div style="position: fixed;background-color: green;color: white;padding: .2em;margin-left: 50%;font-size: 20px;">'.
		t('Studying site layout... please wait').'</div>';
	feed_import_form_progress("======== guess_selector start:");
	$selector=feed_import_form_guess_selector($feed->url,$report);
	if($selector===false && count($report['errors']))
	{
		foreach($report['errors'] as $error){dlib_message_add($error,'warning');}
		dlib_message_add(t('Failed to determine selector automatically. Please set it manually.'),'error');
		dcomponent_progress_log_end(['redirect'=>$feed->link().'/edit',
									 'boxDisplay'=>'fold']);
	}
	$feed->htmlContentSelector=[['type'=>'CSS','arg'=>$selector]];
	$feed->save();
	$feed->update();
	$feed->updateArticles();
	$feed->screenshotUpdate();
	dlib_message_add(t('Succesfully determined a selector.').'<br/>('.ent($selector).')');
	dcomponent_progress_log_end(['redirect'=>$feed->link(),
								 'boxDisplay'=>'fold']);
}

function feed_import_view_stats()
{
	global $currentPage;
	feed_import_view();
	$currentPage->addCss('dlib/table-tools.css');
	$currentPage->addJs('dlib/table-tools.js');

	$stats=[];
	$feeds=Feed::fetchList("WHERE 1 ORDER BY id DESC");
	foreach($feeds as $feed)
	{
		$stats[$feed->id]=$feed->stats();
	}
	return template_render('feed-import-stats.tpl.php',
						   [feed_import_view(),
							compact('feeds','stats')]);

}

//! Display a feed's errors and its error articles
function feed_import_view_feed_errors($feedId)
{
	global $currentPage;
	$currentPage->addCssTpl('demosphere/feed-import/feed.tpl.css');
	$currentPage->addCss('dlib/table-tools.css');
	$feed=Feed::fetch($feedId,false);
	if($feed===null){dlib_not_found_404();}
	$pager=new Pager(['itemName'=>t('articles'),'alwaysShow'=>true]);
	$articles=Article::fetchList("SELECT SQL_CALC_FOUND_ROWS * FROM Article WHERE ".
								 "Article.feedId=%d AND ".
								 "Article.finishedReading=0 AND ".
								 "Article.downloadStatus=0 ".
								 "ORDER BY Article.created DESC, ".
								 "Article.id ASC ".
								 $pager->sql(),
								 $feed->id);
	$pager->foundRows();
	
	$fakeFeed=$feed->getFakeFeed();

	return template_render('feed-errors.tpl.php',
						   [feed_import_view($feed),
							compact('feed','articles','pager','fakeFeed'),
							'ff'=>$feed->getFakeFeedId(),
						   ]);
}

function feed_import_view_error_message($feed,$type='short_message')
{
	if(!isset($feed->errors[0])){return '';}
	return feed_import_view_error_messages($feed->errors[0]['label'],$feed->errors[0],$feed,$type);
}

//! Description of Feed errors, meant for display.
//! Errors are internally stored as a short label.
//! Error message types:
//! * short_message: very short, non technical. Try to keep under 20 letters
//! * message: longer, non technical.
//! * recommend: what the user can do about it, non technical
//! * details: technical details about this error. 
function feed_import_view_error_messages($label,$error,$feed,$type=false)
{
	switch($label)
	{
	// backward compatibility with old errors
	case 'generic': $res=
			[
				'short_message'=>$error['short_message'],
				'message'=>$error['message'],
			];break;
	case 'timeout': $res=
			[
				'short_message'=>t('Site too slow.'),
				'message'=>t('Download of feed aborted after !nb minutes.',['!nb'=>round($error['download']['time']/60)]),
			];break;
	case 'site_down': $res=
			[
				'short_message'=>t('Site is down.'),
				'message'=>t('Download of both the feed and the site\'s url failed.'),
				'recommend'=>t('Usually, there is not much you can do about it. Make sure the site hasn\'t moved to a new address, and just hide errors.'),
			];break;
	case 'feed_failed_no_site': $res=
			[
				'short_message'=>t('Feed download failed.'),
				'message'=>t('The feed did not download and we can\'t determine the site\'s url.'),
			];break;
	case 'feed_failed_site_ok': $res=
			[
				'short_message'=>t('Feed download failed.'),
				'message'=>t("Feed download failed, but the site seems OK."),
				'recommend'=>val($error,'new_feed_url')!==false ? 
				             t("The site's feed url seems to have changed, you should try:")."\n".$error['new_feed_url'] :
				             t("You need to visit the site, to see what is wrong."),
			];break;
	case 'dl_failed_unknown': $res=
			[
				'short_message'=>t('Feed download failed.'),
				'message'=>t('Failed download, undetermined problem.'),
			];break;
	case 'dl_empty_file': $res=
			[
				'short_message'=>t('Empty download.'),
				'message'=>t('The feed downloaded correctly, but it is empty.'),
			];break;
	case 'empty_after_cleanup': $res=
			[
				'short_message'=>t('Empty feed.'),
				'message'=>t('The feed downloaded ok, but it is empty after cleanup.'),
			];break;
	case 'feed_is_html': $res=
			[
				'short_message'=>t('Downloaded feed is HTML.'),
				'message'=>t('The downloaded feed should be an XML feed, but an HTML page was found.'),
				'recommend'=>t('Make sure the url of the feed is ok. This can also happen if the site is down and is displaying an error page instead of the feed.'),
			];break;
	case 'xml_parse': $res=
			[
				'short_message'=>t('Invalid feed data.'),
				'message'=>t('Parsing of feed XML failed. This probably means that the xml of the feed is badly broken.'),
				'recommend'=>t('Make sure the url of the feed is ok. This can also happen if the site is down and is displaying an error page instead of the feed.'),
			];break;
// 	case 'no_items_in_feed': $res=
// 			[
// 				'short_message'=>t('Valid but empty feed.'),
// 				'message'=>t("No entries found in feed.\n". 
// 							 "Normally sites have published at least one article,\n".
// 							 "so their feed should not be empty."),
// 			];break;
	case 'no_selector': $res=
			[
				'short_message'=>t('You must set a selector.'),
				'message'=>t("No html content selector is set for this feed. This is not allowed.\n".
							 "You must configure a content selector for this feed.\n".
							 "Aborting articles update."),
			];break;
	case 'xml_parse_2': $res=
			[
				'short_message'=>t('Invalid feed data.'),
				'message'=>t('Parsing of feed XML failed. This probably means that the xml of the feed is badly broken.')
			];break;
	case 'ff_cc_parse': $res=
			[
				'short_message'=>t('Invalid custom code PHP.'),
				'message'=>'Parse error in FakeFeed custom PHP code.',
			];break;
	case 'ff_cc_return': $res=
			[
				'short_message'=>t('Custom code bad value'),
				'message'=>'Invalid return value for Fakefeed custom PHP code. It should return a list of links or a list of items."',
			];break;
	case 'dl_ical': $res=
			[
				'short_message'=>t('iCalendar download error'),
				'message'=>t('iCalendar download failed.'),
			];break;
	case 'not_ical': $res=
			[
				'short_message'=>t('Invalid iCalendar'),
				'message'=>t('This does not look like an iCalendar file.'),
			];break;
	case 'ical_parse': $res=
			[
				'short_message'=>t('Invalid iCalendar'),
				'message'=>t('Unable parse this iCalendar file, something seems wrong.'),
			];break;
	case 'dl_facebook': $res=
			[
				'short_message'=>t('Facebook page download error'),
				'message'=>t('Failed to download the Facebook page.'),
			];break;
	case 'ff_fetch_page': $res=
			[
				'short_message'=>t('Download failed'),
				'message'=>t('Special feed failed to download a page.'),
			];break;
	case 'ff_fetch_selector': $res=
			[
				'short_message'=>t('Select part of page error'),
				'message'=>t('Special failed to find the specified part of the page.'),
			];break;
	case 'ff_no_links': $res=
			[
				'short_message'=>t('No links found'),
				'message'=>t('Special feed searched for links inside part of page, but did not find any.'."\n".
							 'This can happen when the section of a page you are interested in is empty.'),
			];break;
	case 'ff_pp_select_error': $res=
			[
				'short_message'=>t('Page parts: error in select'),
				'message'=>t('Something went wrong while trying to extract part of this page.'),
				'details'=>'xpath->evaluate failed',
			];break;
	case 'ff_pp_select': $res=
			[
				'short_message'=>t('Page part not found'),
				'message'=>t('Did not find the part of the page that was selected.'),
			];break;
	case 'no_items_longtime': $res=
			[
				'short_message'=>t('No items for a longtime'),
				'message'=>t('There haven\'t been any items (articles) in this feed for a longtime:').dcomponent_format_date('relative-short-full',$error['last']),
				'recommend'=>t('You should check if everything is OK and then hide errors.'),
			];break;
	case 'ff_twitter_dl': $res=
			[
				'short_message'=>t('Twitter download failed'),
				'message'=>t('Failed to download the page from Twitter.'),
			];break;
	case 'ff_twitter_parse': $res=
			[
				'short_message'=>t('Twitter parse failed'),
				'message'=>t('Twitter page downloaded successfully but the program was unable to find some information on the downloaded page.'),
			];break;
	default:
		$res=['short_message'=>$label,
			  'message'=>'Undocumented error.'
			 ];
	}

	$lastOkDelay=floor((time()-$feed->lastOk)/(3600*24*30.5));
	if($feed->lastOk!=0 && $lastOkDelay>=6)
	{
		$res['recommend']=val($res,'recommend','')."\n".
			t('This feed has been down for more than !nb months. You should consider deleting it.',['!nb'=>$lastOkDelay]);
	}

	if(!isset($res['details'])){$res['details']='';}
	if(isset($error['extra']))
	{
		$res['details'].=$error['extra']."\n";
	}
	if(isset($error['download']))
	{
		require_once 'dlib/download-tools.php';
		$dlInfo=$error['download'];
		//$res['details'].='HTTP status: '.$dlInfo['status'].' ('.dlib_http_status_code($dlInfo['status']).')'."\n";
		if(isset($dlInfo['log'])){$res['details'].=$dlInfo['log']."\n";}
	}
	if(isset($error['site_download']) && 
	   ($error['site_download']['status']===0 || 
		$error['site_download']['status']>=400))
	{
		require_once 'dlib/download-tools.php';
		$dlInfo=$error['site_download'];
		$res['details'].="Site:\n";
		//$res['details'].='HTTP status: '.$dlInfo['status'].' ('.dlib_http_status_code($dlInfo['status']).')'."\n";
		$res['details'].=$dlInfo['log']."\n";
	}
	if($type!==false){return val($res,$type);}
	return $res;
}


//! Customize Feed dbobject-ui interface. 
//! Note: the dbobject-ui interface is now used by calling dbobject_ui function directly and using $classInfoOverride 
//! @see for example feed_import_view_feed_details()
function feed_import_view_feed_class_info(&$classInfo)
{
	global $base_url;

	$classInfo['field_desc']=
		[
			'name'=>
			['title'=>t('Name'),
			 'description'=>t('A short name for this feed. You can choose anything you want.'),],
			'url'=>
			['title'=>t('Url'),
			 'description'=>t('The URL of the RSS/Atom feed.'),],
			'status'=>
			['title'=> t('Feed status'),
			 'description'=> t('Whether the last attempt resulted in a fully updated feed (including XML download, parse and starting article updates). This does not say if the article updates actually succeeded, but only that they were tried.'),],
			'lastOk'=>
			['title'=>t('Last time Ok'),
			 'description'=> t('Last time the status of this feed was ok.'),],
			'htmlContentSelector'=>
			['title'=>t('Content selector'),
			 'description'=>t('Select where the actual article is inside each page of this site.')
			],
			'fixHttpsToHttp'=>
			['title'=>t('Fix https to http'),
			 'description'=>t('If selected, urls using https://... will be changed to http://..... NOTE: if you have a problem with invalid ssl certificates, then it is better to just <a href="!url">add the sites url here</a>',['!url'=>$base_url.'/configure/misc-config#no_ssl_cert_sites'])],
			'getContentFromFeedXml'=>
			['title'=>t('Get content from feed XML'),
			 'description'=>t('Normally, feed import downloads articles from links in feeds. With this option, feed import directly uses the html that is embeded in the feed\'s XML, without downloading the articles. If you select this, don\'t use a content selector.')
			],
			'hideErrorsUntil'=>
			['title'=>t('Hide errors until'),
			 'description'=>t('Disable the display of errors for this number of days. The feed itself is not disabled, only the error display.')],
			'comment'=>
			['title'=>t('Comment'),
			 'description'=>t('A comment on this feed. The first line will be displayed in articles under the feed\'s name. The full comment is displayed as a tooltip.')],
			'email'=>
			['title'=>t('Email'),
			 'description'=>t('An optional email associated to this feed. This makes it easier to send an email after importing an article into an event.')],
		];
}

//! "Feed manager" displays a list of all feeds.
//! The list is displayed using table_render_query() and not the dbobject_ui().
//! This way FakeFeed information can be added / displayed more naturally (sort, search,...).
function feed_import_view_feed_list()
{
	global $base_url;
	feed_import_view();

	$lastCompleted=variable_get('feed_import','cron_last_completed',0);
	$lastStarted  =variable_get('feed_import','cron_last_started'  ,0);
	$lastUpdate=t('updated:').' ';
	if($lastCompleted>=$lastStarted){$lastUpdate.=dcomponent_format_date("relative-short-full",$lastCompleted);}
	else                            {$lastUpdate.=t('ongoing').' ('.dcomponent_format_date("relative-short",$lastStarted).')';}
	$lastUpdateTitle=dcomponent_format_date('full-date-time',max($lastStarted,$lastCompleted));

	feed_import_view_feed_class_info($classInfo);
	feed_import_view_fake_feed_class_info($ffClassInfo);

	require_once 'dlib/table-tools.php';
	$tableRenderConfig=
		[
			'pager_itemName'=>t('feeds'),
			'table_class'=>['feed-list',
							'dbobject-ui-list',// used only to enlarge longtext cols
						   ],
			'search_fields_alter'=>function(&$fields)use(&$colNames)
			{
				// Add most Feed and FakeFeed fields for searching (some exceptions)
				$feedFields=array_diff(array_keys(    Feed::getSchema()),array_keys($colNames),['data','fullXml','errors']);
				$ffFields  =array_diff(array_keys(FakeFeed::getSchema()),array_keys($colNames),[]);
				$feedFields=preg_replace('@^@','Feed.'    ,$feedFields);
				$ffFields  =preg_replace('@^@','FakeFeed.',$ffFields  );
				$fields=array_merge($fields,$feedFields,$ffFields);
			},
			'columns'=>function(&$columns)use($classInfo,&$colNames)
			{
				// Build name => columnid index (example: lastOk => C5). This is mainly used for code readability.
				$colNames=[];
				foreach($columns as $name=>$col){if(isset($col['sql']) && $col['sql']->name!==''){$colNames[$col['sql']->name]=$name;}}
				$colNames['nurl']='C4';
				$colNames['unread']='C5';

				$columns[$colNames['nurl'  ]]['title']='url';
				$columns[$colNames['nurl'  ]]['type' ]='url';
				$columns[$colNames['id'    ]]['type' ]='int';
				$columns[$colNames['unread']]['title']=t('Unread');

				// Add translated collumn titles from $classInfo
				foreach($columns as &$col)
				{
					$title=val(val($classInfo['field_desc'],$col['title'],[]),'title');
					if($title!==false){$col['title']=$title;}
				}
				unset($col);
			},
			// adds delete, view and edit columns
			'actionUrl'=>function($row,$action)
			{
				global $base_url;
				$id=intval($row[0]);
				return $base_url.'/feed-import/feed/'.$id.
				    ($action!=='view'       ? '/'.$action     : '').
				    ($action==='view-field' ? '?field=$field' : '');
			},
			// Change color of bad feeds and use relative dates for lastOk and hideErrorsUntil
			'line'=>function($row,&$line,&$lineAttr,$columns)use(&$colNames)
			{
				$feed=Feed::fetch($row[0]);


				if($feed->status){$line[$colNames['lastOk']]['disp']='';}
				else
				{
					$line[$colNames['lastOk']]['attr']['class'][]='bad-status';
					$lineAttr['class'][]='bad-status';
					$line[$colNames['lastOk']]['disp']=dcomponent_format_date('relative-short-full',$feed->lastOk);
				}

				if($feed->hideErrorsUntil<time()){$line[$colNames['hideErrorsUntil']]['disp']='';}
				else
				{
					$lineAttr['class'][]='hide-errors';
					$line[$colNames['hideErrorsUntil']]['disp']=dcomponent_format_date('relative-short-full',$feed->hideErrorsUntil);
				}
			},
			'search_concat_where_and'=>'WHERE',
		];
	$tableRender=table_render_query('SELECT Feed.id,name,FakeFeed.type,COALESCE(FakeFeed.url,Feed.url),'.
									'(SELECT COUNT(*) FROM Article WHERE feedId=Feed.id AND finishedReading=0 AND downloadStatus!=0),'.
									'lastOk,hideErrorsUntil,comment,email '.
									'FROM Feed LEFT OUTER JOIN FakeFeed ON FakeFeed.feedId=Feed.id',[],$tableRenderConfig);
	return 
		'<div id="feed-manager">'.
		'    <a id="new-feed" href="'.$base_url.'/feed-import/form">'.t('Create feed').'</a>'.
		'    <h1 class="page-title">'.t('Feeds').
		         (trim(val($_GET,'search'))!=='' ? 
				   ' <span id="feeds-search-terms">('.t('search results for:').' '.ent('"'.$_GET['search'].'"').')</span>' : '' ).
		'    </h1>'.
		'    <span id="last-update" title="'.ent($lastUpdateTitle).'">'.ent($lastUpdate).'</span>'.
		'    <a id="stats-link" href="'.$base_url.'/feed-import/stats">'.t('stats').'</a>'.
		     $tableRender.
		'</div>';
}


//! Returns an HTML table displaying a feed's errors
function feed_import_view_error_log($feed)
{
	$res='<table  class="feed-error-log gray-table">';
	foreach($feed->errors as $error)
	{
		$res.='<tr>'.
			'<td>'.ent(dcomponent_format_date('relative-short-full',$error['time'])).
			'<div>'.ent(dcomponent_format_date('shorter-date-time',$error['time'])).'</div></td>'.
			'<td>'.ent(feed_import_view_error_message($feed,'short_message')).'</td>'.
			'<td>'.ent(feed_import_view_error_message($feed,'message')).
			'    <i>'.ent(feed_import_view_error_message($feed,'recommends')).'</i></td>'.
			'<td><pre>'.ent(feed_import_view_error_message($feed,'details')).'</pre></td>'.
			'</tr>';
	}
	$res.='</table>';
	return $res;
}

function feed_import_view_article_view_single($articleId)
{
	$article=Article::fetch($articleId,false);
	if($article===null){dlib_not_found_404();}
	$feed=$article->getFeed();
	$viewData=$article->viewData(true);
	$v=feed_import_view($feed,$article);
	return template_render('dcomponent/dcitem.tpl.php',
						   [$v,
							$viewData,
							'item'=>$article,
							template_render_parts('article-item.tpl.php',['item'=>$article,$viewData,$v,]),
						   ]
						  );
}

function feed_import_view_recently_finished_articles()
{
	$feed=false;
	if(isset($_GET['feedId']))
	{
		$feed=Feed::fetch($_GET['feedId'],false);
		if($feed===null){dlib_not_found_404();}
	}

	$nbArticles=db_result("SELECT count(*) FROM Article WHERE finishedReading!=0 ".
						  ($feed!==false ? "AND feedId=".$feed->id : ""));

	$pager=new Pager(['itemName'=>t('articles'),'alwaysShow'=>true]);
	$articles=Article::fetchList('SELECT SQL_CALC_FOUND_ROWS * FROM Article '.
								 // avoid old cleaned-up articles 
								 "WHERE fullXML!='' AND ".
								 "finishedReading!=0 ".
								 ($feed!==false ? "AND feedId=".$feed->id : "").
								 ' ORDER BY finishedReading DESC '.
								 $pager->sql());
	$pager->foundRows();

	return '<h1 class="page-title">'.t('Recently finished articles').'</h1>'.
		template_render('article-list.tpl.php',
						[feed_import_view($feed),
						 compact('articles','pager'),
						 'badFeedIds'=>[],
						 'badArticleIds'=>[],]);
}

function feed_import_view_error_articles()
{
	$pager=new Pager(['itemName'=>t('articles'),'alwaysShow'=>true]);
	$articles=Article::fetchList("SELECT SQL_CALC_FOUND_ROWS * FROM Article WHERE downloadStatus=0 AND finishedReading=0".$pager->sql());
	$pager->foundRows();

	return '<h1 class="page-title">'.t('Articles with errors').'</h1>'.
		template_render('article-list.tpl.php',
						[feed_import_view(),
						 compact('articles','pager'),
						]);
}


//! display full info of article (only used for debug)
function feed_import_view_article_details($articleId)
{
	global $base_url,$currentPage;
	$article=Article::fetch($articleId,false);
	if($article===null){dlib_not_found_404();}
	feed_import_view(false,$article);
	$out='';
	$out.='<div id="article-details-head">';
	$out.='    <h3>'.
		           t('Article !nb in feed "!name"',
					 ['!nb'  =>'<a href="'.ent($base_url.'/feed-import/article/'.$article->id.'/view-single').'">'.$article->id.'</a>',
					  '!name'=>'<a href="'.ent($base_url.'/feed-import/feed/'.$article->feedId).'">'.ent($article->useFeed()->name).'</a>']).
		       '</h3>';
	$out.='    <p><a href="'.$base_url.'/feed-import/article/'.$article->id.'/reset">'.t('reset & update').'</a></p>';
	$out.='</div>';
	$out.=dbobject_ui_view('Article',$articleId);
	$currentPage->tabs=[];
	return $out;
}

//! FakeFeed dbobject-ui is customized here.
//! Note: the dbobject-ui interface for FakeFeed is no longer used  (but field titles and descriptions are still used).
function feed_import_view_fake_feed_class_info(&$classInfo)
{
	global $base_url,$currentPage,$user;

	$classInfo['field_desc']=
		[
			'ccPhpCode'=>
			[
				'title'=>t('PHP code'),
				'description'=>t('Custom PHP code that generates a feed. This requires PHP coding skills and can only be used by admins.'),
			],
			'url'=>
			[
				'title'=>t('Url'),
			],
			'lipHtmlSelector'=>
			[
				'title'=>t('Where in page'),
				'description'=>t('Define the area of the page inside which this special feed will search for links.'),
			],
			'lipLinkRegexp'=>			
			[
				'title'=>t('Filter links'),
				'description'=>t('This special feed finds all of the links in part of a page. Some of these links are probably not what you want. For example, any links pointing to other sites will not be handled correctly. This option allows you to select which links you want to keep. Generally, it is safe to keep all links that are on the same site. Sometimes, you might want to be more selective. With the "advanced" option, you can use a regular expression.'),
			],
			'iCalUrl'=>			
			[
				'title'=>t('iCalendar URL'),
				'description'=>t('The link to the iCalendar/ics online calendar. Example: https://www.google.com/calendar/ical/example%40gmail.com/public/basic.ics?hl=fr'),
			],
			'fbId'=>
			[
				'title'=>t('Facebook ID'),
				'description'=>t('The numeric ID of the Facebook page or profile. You can easily find instructions on the web on how to find the Facebook ID of a page or profile.'),
			],
			'ppCssSelector'=>
			[
				'title'=>t('CSS selector'),
				'description'=>t('An ordinary CSS selector designating one or more page parts.'),
			],
			'ppSplitRegex'=>
			[
				'title'=>t('Split regex'),
				'description'=>t('An optional regular expresion to split each page part into several more parts. You can leave it blank if your "CSS selector" already designates several page parts.'),
			],

		];
}

function feed_import_view_html_selector_display_callback($feedId,$editorHtml)
{
	require_once 'feed-import.php';
	$feed=Feed::fetch($feedId);
	feed_import_view($feed,false,['noTop'=>true]);
	$title='<h1 class="page-title">'.t('Selector for:').
		' <a href="'.ent($feed->link()).'">'.ent($feed->name).'</a></h1>';
	return $title.$editorHtml;
}

function feed_import_view_fake_feed_lip_html_selector_callback($ffId,$editorHtml)
{
	require_once 'feed-import.php';
	feed_import_view(false,false,['noTop'=>true]);
	return $editorHtml;
}


//! Requests user to confirm finishing a list of articles
function feed_import_view_article_finish_confirm($which)
{
	global $base_url;
	
	$feed=Feed::fetch(val($_GET,'feedId'),false);

	switch($which)
	{
	case 'all':            $sqlCond='';                                break;
	case 'no-future-date': $sqlCond='AND firstFutureHLDate<'.time();   break;
	case 'feed':           $sqlCond='AND feedId='.$feed->id;           break;
	default: fatal('strange');
	}

	$articles=Article::fetchList("FROM Article  WHERE ".
								 "finishedReading=0 AND ".
								 "downloadStatus!=0 ".
								 ' '.$sqlCond.' '.
								 "ORDER BY lastFetched DESC");
	if(isset($_POST['confirm']))
	{
		dlib_check_form_token('Article::finishNoFutureDate');
		foreach($articles as $a)
		{
			$a->setFinishedReading();
			$a->save();
			dlib_message_add(t("finished article @id",['@id'=>$a->id]));
		}
		dlib_redirect($base_url.'/feed-import'.($feed!==null ? '/feed/'.$feed->id : '' ));
	}

	return template_render('article-finish-confirm.tpl.php',
						   [feed_import_view($feed===null ? false : $feed),
							compact('articles','which','feed')]);
}

/** @} */
?>