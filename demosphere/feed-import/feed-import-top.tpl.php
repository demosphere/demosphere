<nav id="feed-import-top">
	<a id="feed-dowload-errors"    class="indicator status-<?: (int)!count($badFeedIds   ) ?>" href="$scriptUrl/errors">
		<?:count($badFeedIds) ? count($badFeedIds) : ''?> _(feeds)	
	</a>
	<a id="article-dowload-errors" class="indicator status-<?: (int)!count($badArticleIds) ?>" href="$scriptUrl/error-articles">
		<?:count($badArticleIds) ? count($badArticleIds) : '' ?> _(articles)
	</a>
	<a id="manage-feeds-link" class="" href="$scriptUrl/feed">
			_(Manage feeds)
	</a>
	<div id="finish-choices" class="dropdown dropdown-left">
		<span class="dropdown-button top-icon-button"></span>
		<ul>
			<li class="<?: $finishNoFutureDate ? '' : 'disabled'?>" >
				<a href="$scriptUrl/article/finish-confirm/no-future-date">
					<?: t('Finish: !nb articles with no future date',['!nb'=>$finishNoFutureDate])?>
				</a>
			</li>
			<li class="<?: $finishAll          ? '' : 'disabled'?>" >
				<a href="$scriptUrl/article/finish-confirm/all">
					<?: t('Finish: all !nb articles',['!nb'=>$finishAll])?>
				</a>
			</li>
			<?if($feed!==false){?>
				<li class="<?: $finishFeed ? '' : 'disabled'?>" >
					<a href="$scriptUrl/article/finish-confirm/feed?feedId=$feed->id">
						<?: t('Finish: all !nb articles of this feed',['!nb'=>$finishFeed])?>
					</a>
				</li>
			<?}?>
			<li>
				<a href="$scriptUrl/view-recently-finished-articles">
					_(View recently finished.)
				</a>
			</li>
		</ul>
	</div>
</nav>
