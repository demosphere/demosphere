<div id="confirm-finish">
<h1 class="page-title">
	<?if($which==='all'){?>
		<span class="finish-all">_(Finish all)</span>
	<?}?>
	<?if($which==='no-future-date'){?>
		_(Finish articles that have no future dates)</span>
	<?}?>
	<?if($which==='feed'){?>
		_(Finish articles in feed:) $feed->name
	<?}?>
</h1>
<form method="post" action="">
	<p>
		<?= dlib_add_form_token('Article::finishNoFutureDate') ?>
		
		<?: t('Are you sure you want to finish these @nb articles?',
			['@nb'=>count($articles)])?>
	</p>
	<p>
		<input type="submit" name="confirm" value="_(confirm)"/>
	</p>
</form>
<ul>
	<?foreach($articles as $article){?>
		<li>
			<span><?: dcomponent_format_date('relative-short',$article->lastFetched)?> : </span>
			<? require_once 'lib/htmLawed.php'; // filter_xss messes up datetime in title attribute ?>
			<a href="$scriptUrl/article/$article->id/view-single">
				<?= htmLawed(dcomponent_highlight(ent($article->title),false,false,false),['safe'=>1,'deny_attribute'=>'style']) ?>
			</a>
		</li>
	<?}?>
</ul>
</div>