<table id="feed-errors">
	<?foreach($feeds as $feed){?>
		<tr data-id="$feed->id" class="feed <?: $feed->hideErrorsUntil>time() ? 'hide-errors'  : ''?> feed-status-<?: intval($feed->status) ?>
									   error-label-<?: val(val($feed->errors,0,[]),'label','unknown')?> ">
			<td class="info">
				<a class="feed-name" href="$feed->link()">$feed->name</a>
				<?if(!$feed->getFakeFeedId()){?>
					<a    class="link indicator status-<?: intval($feed->status) ?>" href="<?: u($feed->url) ?>">_(feed url)</a>
				<?}else{?>
					<span class="link indicator status-<?: intval($feed->status) ?>"                           >_(special feed)</span>
				<?}?>
				<a class="site indicator status-<?: (int)val(val($feed->errors,0,[]),'site_status',2) ?>" href="<?: u($feed->guessSitePage())?>">_(site url)</a>
			</td>
			<td class="summary">
				<div class="short-message">
					<?if($feed->status){?>
						_(OK)
					<?}else{?>
						<?: feed_import_view_error_message($feed,'short_message') ?>
					<?}?>
				</div>
				<?if($feed->getFakeFeed()!==null){?>
					<div class="fake-feed-type">_(Special feed:) <?:$feed->getFakeFeed()->getType() ?></div>
				<?}?>
				<div class="until">
					_(Hide err until:) <span><?: dcomponent_format_date("relative-short-full",$feed->hideErrorsUntil) ?></span>
				</div>
				<div>_(Last ok:) <?: dcomponent_format_date("relative-short-full",$feed->lastOk) ?></div>
				<?if($feed->lastArticleTs()!==null){?>
					<div class="last-article">
						_(Last article:) <?: dcomponent_format_date("relative-short-full",$feed->lastArticleTs()) ?>
					</div>
				<?}?>
			</td>
			<td class="message">
				<?spaceless?>
					<div class="message-wrap">
						<?if(!isset($feed->errors[0])){?>
							<?if($feed->lastOk===0){?>
								<span>_(Never updated.)</span>
							<?}else{?>
								<span>_(No error messages)</span>
							<?}?>
						<?}?>
						<?if(!$feed->status){?>
							<div class="main-message"><?: feed_import_view_error_message($feed,'message') ?></div>
							<?if(feed_import_view_error_message($feed,'recommend')!==false){?>
								<div class="recommend">
									<?if(!isset($feed->errors[0]) && $feed->lastOk===0){?>
										_(This is not really an error. It just means that this feed has never been updated yet.)<br/>
										_(Feeds are automatically updated several times a day. You can also update them manually on the "view" page.)
									<?}else{?>
										<?: feed_import_view_error_message($feed,'recommend') ?>
									<?}?>
								</div>
							<?}?>
							<pre class="details"><?: feed_import_view_error_message($feed,'details') ?></pre>
						<?}?>
					</div>
				<?end_spaceless?>
			</td>
			<td class="actions">
					
					<input type="button" data-duration="0"  value="_(Unhide)"  /><br/>  
					<span>_(Hide errors)</span>
					<input type="button" data-duration="3"  value="_(3 days)"  /><br/>  
					<input type="button" data-duration="21" value="_(3 weeks)" /><br/> 
					<input type="button" data-duration="92" value="_(3 months)"/><br/> 
			</td>
		</tr>
	<?}?>
</table>