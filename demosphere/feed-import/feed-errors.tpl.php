<div id="single-feed-errors">	
	<?= render('feed-error-list.tpl.php',['feeds'=>[$feed]]) ?>
</div>
<div id="feed-error-log-fold" class="hidden">	
	<button id="log-expand">_(Old errors)</button>
	<?= feed_import_view_error_log($feed) ?>
</div>	


<hr/>
<?= render('dlib/pager.tpl.php') ?>
<? foreach($articles as $item){ ?>
	<? extract($item->viewData()) ?>
	<?= render('dcomponent/dcitem.tpl.php',
			render_parts('article-item.tpl.php')); ?>
<?}?>

