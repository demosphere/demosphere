<?php

// ******* page-list.tpl.php ******************* 
t('page dowload errors');//page-list.tpl.php

// ******* page.tpl.php ******************* 
t('click');//page.tpl.php
t('revisions');//page.tpl.php
t('error displaying page iframe');//page.tpl.php

// ******* page-item.tpl.php ******************* 
t('link');//page-item.tpl.php
t('old version');//page-item.tpl.php
t('new version');//page-item.tpl.php
t('html-diffs');//page-item.tpl.php

// ******* page-watch-admin.tpl.php ******************* 
t('Admin operations');//page-watch-admin.tpl.php
t('don\'t use unless you really know what you are doing');//page-watch-admin.tpl.php

// ******* demosphere-mobile-menu.tpl.php ******************* 
t('Nearby');//demosphere-mobile-menu.tpl.php
t('Search');//demosphere-mobile-menu.tpl.php
t('Submit an event');//demosphere-mobile-menu.tpl.php
t('Wide screen site');//demosphere-mobile-menu.tpl.php
t('My profile');//demosphere-mobile-menu.tpl.php
t('Login');//demosphere-mobile-menu.tpl.php
t('Logout');//demosphere-mobile-menu.tpl.php

// ******* demosphere-event-repetition-group-self-edit.tpl.php ******************* 
t('Repetitions');//demosphere-event-repetition-group-self-edit.tpl.php

// ******* demosphere-event-repetition-groups.tpl.php ******************* 
t('Repetition groups');//demosphere-event-repetition-groups.tpl.php
t('Include old repetitions');//demosphere-event-repetition-groups.tpl.php
t('Choose');//demosphere-event-repetition-groups.tpl.php
t('2 months');//demosphere-event-repetition-groups.tpl.php
t('1 year');//demosphere-event-repetition-groups.tpl.php
t('all');//demosphere-event-repetition-groups.tpl.php

// ******* demosphere-place.tpl.php ******************* 
t('Where');//demosphere-place.tpl.php
t('map');//demosphere-place.tpl.php
t('Warning: not positioned on map');//demosphere-place.tpl.php
t('Event selection tools for emails subscriptions, feeds, widgets, frontpage... will not work correctly with this place.');//demosphere-place.tpl.php

// ******* demosphere-event.tpl.php ******************* 
t('Some information or a confirmation is missing for this event. Please contact us if you have information.');//demosphere-event.tpl.php
t('incomplete');//demosphere-event.tpl.php
t('add more information or a comment');//demosphere-event.tpl.php
t('Comment this');//demosphere-event.tpl.php
t('share a ride to this event');//demosphere-event.tpl.php
t('Share a ride');//demosphere-event.tpl.php
t('more...');//demosphere-event.tpl.php
t('email this event to someone');//demosphere-event.tpl.php
t('Send');//demosphere-event.tpl.php
t('import this event into your calendar');//demosphere-event.tpl.php
t('iCal');//demosphere-event.tpl.php
t('share this event on social networking sites');//demosphere-event.tpl.php
t('Share');//demosphere-event.tpl.php
t('frontpage title: ');//demosphere-event.tpl.php
t('opinions');//demosphere-event.tpl.php
t('moderator info : ');//demosphere-event.tpl.php
t('opinions');//demosphere-event.tpl.php
t('This is an external event. Normal users will be automatically redirected to the following page.');//demosphere-event.tpl.php
t('This is an duplicate event. Normal users will be automatically redirected to the following event.');//demosphere-event.tpl.php
t('This is event is published but is not shown on the frontpage.');//demosphere-event.tpl.php
t('Needs attention:');//demosphere-event.tpl.php
t('No title found in text, automatically adding it.');//demosphere-event.tpl.php

// ******* demosphere-search-results.tpl.php ******************* 
t('Search failed');//demosphere-search-results.tpl.php
t('Your search did not match any future events.');//demosphere-search-results.tpl.php
t('Your search did not match any past events.');//demosphere-search-results.tpl.php
t('From:');//demosphere-search-results.tpl.php

// ******* demosphere-event-revisions.tpl.php ******************* 
t('Date');//demosphere-event-revisions.tpl.php
t('User');//demosphere-event-revisions.tpl.php
t('Message');//demosphere-event-revisions.tpl.php
t('current');//demosphere-event-revisions.tpl.php
t('show');//demosphere-event-revisions.tpl.php
t('show diff');//demosphere-event-revisions.tpl.php
t('restore');//demosphere-event-revisions.tpl.php

// ******* demosphere-opinions-list.tpl.php ******************* 
t('Rating');//demosphere-opinions-list.tpl.php
t('Rep.');//demosphere-opinions-list.tpl.php
t('Op. date');//demosphere-opinions-list.tpl.php
t('Event');//demosphere-opinions-list.tpl.php

// ******* demosphere-calendar.tpl.php ******************* 
t('today');//demosphere-calendar.tpl.php
t('tomorrow');//demosphere-calendar.tpl.php

// ******* demosphere-event-urgent-email.tpl.php ******************* 
t('Event\'s date : ');//demosphere-event-urgent-email.tpl.php
t('Event\'s location : ');//demosphere-event-urgent-email.tpl.php
t('view map');//demosphere-event-urgent-email.tpl.php
t('for more information on this event, click here');//demosphere-event-urgent-email.tpl.php
t('Change the options for this email or stop receiving it.');//demosphere-event-urgent-email.tpl.php

// ******* demosphere-email-subscription.tpl.php ******************* 
t('This email contains no events, because no events currently match your preferences. Normally an empty event email would not be sent to you. It was only sent because this is a test.');//demosphere-email-subscription.tpl.php
t('Change the options for this email.');//demosphere-email-subscription.tpl.php
t('Quick unsubscribe.');//demosphere-email-subscription.tpl.php

// ******* demosphere-search-page.tpl.php ******************* 
t('Search for events');//demosphere-search-page.tpl.php
t('Search');//demosphere-search-page.tpl.php
t('Events:');//demosphere-search-page.tpl.php
t('Include');//demosphere-search-page.tpl.php
t('Comments:');//demosphere-search-page.tpl.php
t('Mails:');//demosphere-search-page.tpl.php
t('Feeds:');//demosphere-search-page.tpl.php
t('Extended:');//demosphere-search-page.tpl.php
t('Only search title:');//demosphere-search-page.tpl.php

// ******* demosphere-event-repetition-copy-form.tpl.php ******************* 
t('Copy repetitions');//demosphere-event-repetition-copy-form.tpl.php
t('Do not change any other repetition');//demosphere-event-repetition-copy-form.tpl.php
t('Change selected fields');//demosphere-event-repetition-copy-form.tpl.php
t('More options');//demosphere-event-repetition-copy-form.tpl.php
t('The event you have just changed:');//demosphere-event-repetition-copy-form.tpl.php
t('Select the fields of repetitions you also want to change:');//demosphere-event-repetition-copy-form.tpl.php
t('before');//demosphere-event-repetition-copy-form.tpl.php
t('after');//demosphere-event-repetition-copy-form.tpl.php
t('toggle');//demosphere-event-repetition-copy-form.tpl.php
t('Legend:');//demosphere-event-repetition-copy-form.tpl.php
t('grey text');//demosphere-event-repetition-copy-form.tpl.php
t('This field has not changed. You do not need to copy it.');//demosphere-event-repetition-copy-form.tpl.php
t('red text');//demosphere-event-repetition-copy-form.tpl.php
t('If you select this field, you might lose some information. This field in this repetition was different to the field in the event before you saved it.');//demosphere-event-repetition-copy-form.tpl.php
t('Normal text');//demosphere-event-repetition-copy-form.tpl.php
t('You can safely select this field. It was identical to the field in the event before you saved it.');//demosphere-event-repetition-copy-form.tpl.php
t('green background');//demosphere-event-repetition-copy-form.tpl.php
t('This is the event you have just saved. The first two lines show the event before and after you saved it. The third line, shows your event inside the list of repetitions.');//demosphere-event-repetition-copy-form.tpl.php
t('This is the current event.');//demosphere-event-repetition-copy-form.tpl.php
t('green and orange');//demosphere-event-repetition-copy-form.tpl.php
t('This is a field that you have just changed in the event you have just saved.');//demosphere-event-repetition-copy-form.tpl.php
t('Yellow border');//demosphere-event-repetition-copy-form.tpl.php

// ******* demosphere-opinions-event.tpl.php ******************* 
t('Your opinion on this event');//demosphere-opinions-event.tpl.php
t('Opinions on this event');//demosphere-opinions-event.tpl.php
t('Average:');//demosphere-opinions-event.tpl.php
t('Publish:');//demosphere-opinions-event.tpl.php
t('This event has been published, opinions are now closed');//demosphere-opinions-event.tpl.php
t('This event was rejected, opinions are now closed');//demosphere-opinions-event.tpl.php
t('Opinions are closed for this event');//demosphere-opinions-event.tpl.php
t('no opinions');//demosphere-opinions-event.tpl.php
t('This opinion has been disabled.');//demosphere-opinions-event.tpl.php
t('If you save it again, it will be re-enabled.');//demosphere-opinions-event.tpl.php
t('Guidelines:');//demosphere-opinions-event.tpl.php

// ******* demosphere-frontpage-mobile.tpl.php ******************* 
t('show more events');//demosphere-frontpage-mobile.tpl.php

// ******* demosphere-event-send-by-email-text.tpl.php ******************* 
t('is sending you the following event from the calendar : ');//demosphere-event-send-by-email-text.tpl.php
t('Event\'s date : ');//demosphere-event-send-by-email-text.tpl.php
t('Event\'s location : ');//demosphere-event-send-by-email-text.tpl.php
t('For more information on this event, follow this link :');//demosphere-event-send-by-email-text.tpl.php
t('If you notice that somebody is misusing this email service, please contact: contact@demosphere.net');//demosphere-event-send-by-email-text.tpl.php

// ******* demosphere-event-repetition-group-confirm.tpl.php ******************* 
t('Confirm event repetitions');//demosphere-event-repetition-group-confirm.tpl.php
t('Check events that are confirmed or uncheck events that are canceled. In every case, you need to press the "Save" button.');//demosphere-event-repetition-group-confirm.tpl.php
t('No future events available.');//demosphere-event-repetition-group-confirm.tpl.php
t('Make sure you check / uncheck events that are already on this list.');//demosphere-event-repetition-group-confirm.tpl.php
t('This repetition has been disabled. This generally happens after two emails are sent to you and you do not confirm or cancel any events. You can re-enable this repetition by hitting the "save" button below.');//demosphere-event-repetition-group-confirm.tpl.php

// ******* demosphere-opinions-of-user.tpl.php ******************* 
t('My opinions');//demosphere-opinions-of-user.tpl.php
t('Reputation:');//demosphere-opinions-of-user.tpl.php
t('Level:');//demosphere-opinions-of-user.tpl.php

// ******* demosphere-place-variants.tpl.php ******************* 
t('Place variants');//demosphere-place-variants.tpl.php
t('Manage variants of this place. Check the first column to delete a variant. The second column is the replacement. Deleted variants will be replaced by the chosen replacement.');//demosphere-place-variants.tpl.php
t('Delete selected variants');//demosphere-place-variants.tpl.php
t('Number of events at this place');//demosphere-place-variants.tpl.php
t('ev.');//demosphere-place-variants.tpl.php
t('place');//demosphere-place-variants.tpl.php
t('address');//demosphere-place-variants.tpl.php

// ******* demosphere-carpool-manage.tpl.php ******************* 
t('Share a ride admin');//demosphere-carpool-manage.tpl.php

// ******* demosphere-map.tpl.php ******************* 
t('save bookmark');//demosphere-map.tpl.php
t('change');//demosphere-map.tpl.php
t('save bookmark');//demosphere-map.tpl.php
t('show options');//demosphere-map.tpl.php
t('help');//demosphere-map.tpl.php

// ******* demosphere-frontpage.tpl.php ******************* 
t('Search');//demosphere-frontpage.tpl.php
t('show more events');//demosphere-frontpage.tpl.php
t('mobile');//demosphere-frontpage.tpl.php

// ******* demosphere-place-merge.tpl.php ******************* 
t('Merge places');//demosphere-place-merge.tpl.php
t('Select all places that are very similar to the first one and press the "merge places" button.');//demosphere-place-merge.tpl.php
t('The selected places will be considered as only small variations of the reference place.');//demosphere-place-merge.tpl.php
t('These small variations will not apear as independent places in suggestions and will not be displayed as public place pages.');//demosphere-place-merge.tpl.php
t('This "merge places" operation is only necessary if users have entered similar places, without correctly using the suggest button.');//demosphere-place-merge.tpl.php
t('Note that this merge does not change the information actually displayed on any event.');//demosphere-place-merge.tpl.php
t('merge places');//demosphere-place-merge.tpl.php
t('Distance, in meters');//demosphere-place-merge.tpl.php
t('dist.');//demosphere-place-merge.tpl.php
t('Number of events at this place');//demosphere-place-merge.tpl.php
t('ev.');//demosphere-place-merge.tpl.php
t('place');//demosphere-place-merge.tpl.php
t('address');//demosphere-place-merge.tpl.php

// ******* demosphere-user-view.tpl.php ******************* 
t('My account');//demosphere-user-view.tpl.php
t('Configure');//demosphere-user-view.tpl.php
t('Personal info');//demosphere-user-view.tpl.php
t('Login:');//demosphere-user-view.tpl.php
t('Email:');//demosphere-user-view.tpl.php
t('not yet verified');//demosphere-user-view.tpl.php
t('Since:');//demosphere-user-view.tpl.php
t('Role');//demosphere-user-view.tpl.php
t('Last login');//demosphere-user-view.tpl.php
t('Last access');//demosphere-user-view.tpl.php
t('data');//demosphere-user-view.tpl.php
t('Configure');//demosphere-user-view.tpl.php
t('Email subscriptions');//demosphere-user-view.tpl.php
t('Emails that you receive from this site.');//demosphere-user-view.tpl.php
t('Receive daily/weekly emails with your selection of events.');//demosphere-user-view.tpl.php
t('Last sent:');//demosphere-user-view.tpl.php
t('Emails sent to you as a moderator.');//demosphere-user-view.tpl.php
t('Email with events that need my opinion');//demosphere-user-view.tpl.php
t('In charge of places');//demosphere-user-view.tpl.php
t('My events');//demosphere-user-view.tpl.php
t('Confirm event repetitions');//demosphere-user-view.tpl.php
t('Share a ride:');//demosphere-user-view.tpl.php
t('My comments');//demosphere-user-view.tpl.php
t('Configure');//demosphere-user-view.tpl.php
t('Opinions');//demosphere-user-view.tpl.php
t('My opinions on event event moderation.');//demosphere-user-view.tpl.php
t('published');//demosphere-user-view.tpl.php
t('rejected');//demosphere-user-view.tpl.php
t('Events that need my opinion:');//demosphere-user-view.tpl.php
t('Email me:');//demosphere-user-view.tpl.php
t('Configure');//demosphere-user-view.tpl.php
t('Show on my own web site');//demosphere-user-view.tpl.php
t('Show a calendar with your selection of events on your own website or blog');//demosphere-user-view.tpl.php
t('Configure');//demosphere-user-view.tpl.php
t('My custom calendar');//demosphere-user-view.tpl.php
t('Create a calendar on this site with your own events.');//demosphere-user-view.tpl.php
t('The RSS feed for this calendar is:');//demosphere-user-view.tpl.php

// ******* demosphere-user-stats.tpl.php ******************* 
t('admins:');//demosphere-user-stats.tpl.php
t('moderators:');//demosphere-user-stats.tpl.php
t('ordinary:');//demosphere-user-stats.tpl.php
t('Logged-in during last 3 months:');//demosphere-user-stats.tpl.php
t('Subscribed to email:');//demosphere-user-stats.tpl.php
t('sent past 24h: ');//demosphere-user-stats.tpl.php
t('Customized email settings :');//demosphere-user-stats.tpl.php
t('Public form contacts :');//demosphere-user-stats.tpl.php

// ******* demosphere-carpool-contact.tpl.php ******************* 
t('More information here.');//demosphere-carpool-contact.tpl.php
t('Departure:');//demosphere-carpool-contact.tpl.php
t('You volunteered to drive from:');//demosphere-carpool-contact.tpl.php
t('Contact:');//demosphere-carpool-contact.tpl.php
t('You can answer to this message here:');//demosphere-carpool-contact.tpl.php
t('The message:');//demosphere-carpool-contact.tpl.php
t('If you notice that somebody is misusing this email service, please contact: contact@demosphere.net');//demosphere-carpool-contact.tpl.php

// ******* demosphere-carpool.tpl.php ******************* 
t('Share a ride');//demosphere-carpool.tpl.php
t('These shared rides are not visible to ordinary users.');//demosphere-carpool.tpl.php
t('Destination:');//demosphere-carpool.tpl.php
t('Arrival at:');//demosphere-carpool.tpl.php
t('map');//demosphere-carpool.tpl.php
t('Name:');//demosphere-carpool.tpl.php
t('Contact me here');//demosphere-carpool.tpl.php
t('Contact me:');//demosphere-carpool.tpl.php
t('I\'m leaving at:');//demosphere-carpool.tpl.php
t('I\'m leaving from:');//demosphere-carpool.tpl.php
t('Comment:');//demosphere-carpool.tpl.php
t('Edit');//demosphere-carpool.tpl.php
t('Be the first one to share a ride:');//demosphere-carpool.tpl.php
t('Add a new ride:');//demosphere-carpool.tpl.php
t('I\'m a driver');//demosphere-carpool.tpl.php
t('I\'m a passenger');//demosphere-carpool.tpl.php

// ******* demosphere-event-send-by-email-html.tpl.php ******************* 
t('Event\'s date : ');//demosphere-event-send-by-email-html.tpl.php
t('Event\'s location : ');//demosphere-event-send-by-email-html.tpl.php
t('view map');//demosphere-event-send-by-email-html.tpl.php
t('for more information on this event, click here');//demosphere-event-send-by-email-html.tpl.php
t('If you notice that somebody is misusing this email service, please contact: contact@demosphere.net');//demosphere-event-send-by-email-html.tpl.php

// ******* demosphere-place-page.tpl.php ******************* 
t('Place description');//demosphere-place-page.tpl.php
t('moderator info : ');//demosphere-place-page.tpl.php
t('Place id:');//demosphere-place-page.tpl.php
t('This place is only a variant of place:');//demosphere-place-page.tpl.php
t('This place does not have a description.');//demosphere-place-page.tpl.php
t('This place has a description but is only a variant. You should erase the description!');//demosphere-place-page.tpl.php
t('Future events at this place:');//demosphere-place-page.tpl.php
t('Past events at this place:');//demosphere-place-page.tpl.php

// ******* demosphere-places.tpl.php ******************* 
t('List of places');//demosphere-places.tpl.php
t('All places');//demosphere-places.tpl.php
t('Only places with a description');//demosphere-places.tpl.php
t('Show only:');//demosphere-places.tpl.php
t('all');//demosphere-places.tpl.php
t('No places found');//demosphere-places.tpl.php
t('n° ev.');//demosphere-places.tpl.php

// ******* feed-errors.tpl.php ******************* 
t('Old errors');//feed-errors.tpl.php

// ******* feed-import-top.tpl.php ******************* 
t('feeds');//feed-import-top.tpl.php
t('articles');//feed-import-top.tpl.php
t('Manage feeds');//feed-import-top.tpl.php
t('View recently finished.');//feed-import-top.tpl.php

// ******* article-finish-confirm.tpl.php ******************* 
t('Finish all');//article-finish-confirm.tpl.php
t('Finish articles that have no future dates');//article-finish-confirm.tpl.php
t('Finish articles in feed:');//article-finish-confirm.tpl.php
t('confirm');//article-finish-confirm.tpl.php

// ******* feed.tpl.php ******************* 
t('Feed url:');//feed.tpl.php
t('Site url:');//feed.tpl.php
t('Special feed:');//feed.tpl.php
t('Url:');//feed.tpl.php
t('Parts of page defined by:');//feed.tpl.php
t('Parts of page split by:');//feed.tpl.php
t('Latest article:');//feed.tpl.php
t('Feed down since:');//feed.tpl.php
t('Hide errors until:');//feed.tpl.php
t('Latest finished:');//feed.tpl.php
t('None');//feed.tpl.php
t('[more]');//feed.tpl.php
t('Update feed');//feed.tpl.php
t('Delete feed');//feed.tpl.php

// ******* feed-import-stats.tpl.php ******************* 
t('Stats');//feed-import-stats.tpl.php
t('Name');//feed-import-stats.tpl.php
t('Articles');//feed-import-stats.tpl.php
t('Finished');//feed-import-stats.tpl.php
t('Error art.');//feed-import-stats.tpl.php
t('Last OK');//feed-import-stats.tpl.php
t('Most recent');//feed-import-stats.tpl.php

// ******* feed-error-list.tpl.php ******************* 
t('feed url');//feed-error-list.tpl.php
t('special feed');//feed-error-list.tpl.php
t('site url');//feed-error-list.tpl.php
t('OK');//feed-error-list.tpl.php
t('Special feed:');//feed-error-list.tpl.php
t('Hide err until:');//feed-error-list.tpl.php
t('Last ok:');//feed-error-list.tpl.php
t('Last article:');//feed-error-list.tpl.php
t('Never updated.');//feed-error-list.tpl.php
t('No error messages');//feed-error-list.tpl.php
t('This is not really an error. It just means that this feed has never been updated yet.');//feed-error-list.tpl.php
t('Feeds are automatically updated several times a day. You can also update them manually on the "view" page.');//feed-error-list.tpl.php
t('Unhide');//feed-error-list.tpl.php
t('Hide errors');//feed-error-list.tpl.php
t('3 days');//feed-error-list.tpl.php
t('3 weeks');//feed-error-list.tpl.php
t('3 months');//feed-error-list.tpl.php

// ******* article-item.tpl.php ******************* 
t('import');//article-item.tpl.php
t('finished at:');//article-item.tpl.php
t('reset');//article-item.tpl.php

// ******* html-selector.tpl.php ******************* 
t('Selected');//html-selector.tpl.php
t('error displaying iframe');//html-selector.tpl.php
t('original page');//html-selector.tpl.php
t('raw html');//html-selector.tpl.php
t('no display');//html-selector.tpl.php

// ******* text-matching-similar-docs.tpl.php ******************* 
t('Reference doc:');//text-matching-similar-docs.tpl.php
t('view diff');//text-matching-similar-docs.tpl.php

// ******* text-matching-similar-doc.tpl.php ******************* 
t('bigger viewing area');//text-matching-similar-doc.tpl.php
t('smaller viewing area');//text-matching-similar-doc.tpl.php

// ******* text-matching-keyphrase-diff.tpl.php ******************* 
t('Matching keyphrases:');//text-matching-keyphrase-diff.tpl.php

// ******* mail-import-view-mailing-lists.tpl.php ******************* 
t('Mailing lists');//mail-import-view-mailing-lists.tpl.php
t('past year');//mail-import-view-mailing-lists.tpl.php
t('tot');//mail-import-view-mailing-lists.tpl.php
t('unread');//mail-import-view-mailing-lists.tpl.php
t('list name');//mail-import-view-mailing-lists.tpl.php
t('last');//mail-import-view-mailing-lists.tpl.php
t('rules');//mail-import-view-mailing-lists.tpl.php
t('subs-address');//mail-import-view-mailing-lists.tpl.php
t('unsubscribe');//mail-import-view-mailing-lists.tpl.php

// ******* message-item.tpl.php ******************* 
t('thread');//message-item.tpl.php
t('Reply to this email');//message-item.tpl.php
t('Reply to all or forward');//message-item.tpl.php
t('Reply to all');//message-item.tpl.php
t('Forward');//message-item.tpl.php
t('Remove rule that says:');//message-item.tpl.php
t('advanced rules');//message-item.tpl.php
t('Create event from this email');//message-item.tpl.php
t('import');//message-item.tpl.php
t('Click here to unblock images. Privacy warning: by unblocking images, the sender might be notified that you have read this email. ');//message-item.tpl.php
t('finished at:');//message-item.tpl.php
t('list');//message-item.tpl.php
t('attachments');//message-item.tpl.php

// ******* message-details.tpl.php ******************* 
t('Detailed information:');//message-details.tpl.php
t('Attachments / parts');//message-details.tpl.php
t('Full headers:');//message-details.tpl.php
t('Download raw mail data');//message-details.tpl.php

// ******* message-rule-manager.tpl.php ******************* 
t('Current importance:');//message-rule-manager.tpl.php
t('Current source:');//message-rule-manager.tpl.php
t('This email currently matches the following rules');//message-rule-manager.tpl.php
t('Add a rule:');//message-rule-manager.tpl.php
t('save');//message-rule-manager.tpl.php

// ******* mail-import-top.tpl.php ******************* 
t('Write and send a new mail');//mail-import-top.tpl.php
t('New Mail');//mail-import-top.tpl.php
t('click here to re-fetch new mail from mailbox');//mail-import-top.tpl.php
t('Sent');//mail-import-top.tpl.php
t('Lists');//mail-import-top.tpl.php
t('Finish: ordinary with no future date');//mail-import-top.tpl.php
t('Finish: old');//mail-import-top.tpl.php
t('Display list with full mails');//mail-import-top.tpl.php
t('Display compact list of mail');//mail-import-top.tpl.php
t('Sort mail starting with most urgent.');//mail-import-top.tpl.php
t('Sort mail starting with most recent.');//mail-import-top.tpl.php
t('search');//mail-import-top.tpl.php
t('Mailing list');//mail-import-top.tpl.php
t('Thread:');//mail-import-top.tpl.php
t('Finish all messages in this thread');//mail-import-top.tpl.php
t('unread');//mail-import-top.tpl.php
t('finished');//mail-import-top.tpl.php
t('unread');//mail-import-top.tpl.php
t('finished');//mail-import-top.tpl.php
t('hidden');//mail-import-top.tpl.php
t('unread');//mail-import-top.tpl.php
t('finished');//mail-import-top.tpl.php
t('hidden');//mail-import-top.tpl.php
t('all');//mail-import-top.tpl.php

// ******* message-list-compact.tpl.php ******************* 
t('list');//message-list-compact.tpl.php

// ******* dcitem.tpl.php ******************* 
t('previous highlighted date or keyword');//dcitem.tpl.php
t('next highlighted date or keyword');//dcitem.tpl.php
t('bigger viewing area');//dcitem.tpl.php
t('smaller viewing area');//dcitem.tpl.php
t('loading...');//dcitem.tpl.php
?>
