<?php

/**
 * @file
 * Stuff that doesn't fit elsewhere
 */

//! $custom_config : Common configuration options for site custom php code.
//! List of $custom_config keys:
//! - sitemap
//! - dtoken_list
//! - cron
//! - add_paths
//! - widget_parse_client_options
//! - widget_html
//! - widget_events
//! - login_form
//! - demosphere_external_events_postsave
//! - custom_demosphere_event_edit_form_submit
//! - feed_import_form_download_and_clean_html_page
//! - feed_import_form_curl_download
//! -::-;-::-
$custom_config;


// ************************************************************
// log
// ************************************************************

//! Add a log entry 
//! type can be 'Event' or 'Post' (will be stored in db as 0 or 1)
//! FIXME: (11/2016): the log table is not really used. 
//! Currently all of its information is also carried by Event::changed and Post::changed
//! We need to either drop the log table or make some better use of it 
function demosphere_log($type,$message,$typeId=0,$data=null)
{
	global $user;
	$types=['Event'=>0,'Post'=>1];
	$type=$types[$type];
	$ip=isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : 'cmdline';
	$timestamp=time();
	db_query("INSERT INTO log (type,message,type_id,data,user_id,timestamp,ip) VALUES (%d,'%s',%d,'%s',%d,%d,'%s')",
			 $type,$message,$typeId,serialize($data),isset($user) ? $user->id : 0,$timestamp,$ip);
}

function demosphere_log_install()
{
	db_query("DROP TABLE IF EXISTS log");
	db_query("CREATE TABLE log (
  `id` int(11) NOT NULL auto_increment,
  `type` tinyint(3) NOT NULL,
  `message` longtext NOT NULL,
  `type_id` int(11) NOT NULL,
  `data` longblob NOT NULL,
  `user_id` int(11) NOT NULL,
  `timestamp` int(11) NOT NULL,
  `ip` varchar(250) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `timestamp` (`timestamp`),
  KEY `type` (`type`),
  KEY `type_id` (`type_id`),
  KEY `ip` (`ip`)
) DEFAULT CHARSET=utf8mb4");
}


// ***************************************
// ***************************************
// ***************************************

// ********* manage private file (non public urls) *********************
// ********* like mail attchments, public form attachments, docconvert files... *********

//! Check if a url is a link to a private file and return info parsed from url.
function demosphere_private_file($url)
{
	global $base_url,$demosphere_config;
	$res=['type'=>false];
	$safe_base=$demosphere_config['safe_base_url'];

	if(strpos($url,$base_url)===0){$url=substr($url,strlen($base_url));}
	else
	if(strpos($url,$safe_base)===0){$url=substr($url,strlen($safe_base));}
	else
	if(strpos($url,'/')!==0){return $res;} // this is not a local url or abs path

	// initial .* necessary for editor broken relative urls
	if(preg_match('@^.*/mail-import/message/([0-9]+)/(display-part|view-attachment)\?part=(p-[0-9-]+)@',
				  $url,$matches))
	{$res=['type'=>'mail','id'=>$matches[1],'part'=>$matches[3]];}
	else
	if(preg_match('@^.*/docconvert/doc/([a-z0-9-]+)/view-file\?'.
				  'file=([0-9]+)@',
				  $url,$matches))
	{$res=['type'=>'docconvert','docid'=>$matches[1],'file'=>$matches[2]];}
 	else 
 	if(preg_match('@^.*/files/images/proxy/([0-9a-f]{1,20}\.[a-z]{1,5})\?.*\burl=([0-9a-f]+)@',$url,$matches))
 	{$res=['type'=>'proxy','cachedFname'=>$matches[1],'remoteUrl'=>demosphere_image_proxy_remote_url_decode($matches[1],$matches[2])];}
 	else 
 	if(preg_match('@^.*/(files/images/data-url/[0-9a-f]{1,20}\.[a-z]{1,5})@',$url,$matches))
 	{$res=['type'=>'data-url','filename'=>$matches[1]];}

	return $res;
}

//! Download a file from a url.
//! This function automatically recognizes private urls.
//! Private urls are urls of files, on this site, that are not publicly available (login required).
//! Examples of private files are: mail-import attachments, docconvert documents...
//! This fct will download normal (non-private) using dlib_curl_download().
//! @param a url that can be both public (anywhere on the web) or private
//! @param full filename
function demosphere_download_file($url,$dest,$dlOptions=[])
{
	global $user;
	$private=demosphere_private_file($url);

	switch($private['type'])
	{
	case 'mail':
		if(!$user->checkRoles('admin','moderator')){dlib_permission_denied_403();}
		require_once 'mail-import/mail-import.php';
		$message=Message::fetch($private['id'],false);
		if($message===null){return false;}
		file_put_contents($dest,$message->getPartContents($private['part']));
		$info['headers']=['content-type'=>$message->getPartType($private['part'])];
		$info['filename']=val($message->parts[$private['part']],'filename','empty-filename');
		break;
	case 'docconvert':
		require_once 'docconvert/docconvert.php';
		try
		{
			$doc=new DocconvertDoc($private['docid']);
			$doc->copyFile(intval($private['file']),$dest);
		}
		catch(Exception $e){return false;}
		$info['headers']=['content-type'=>$doc->fileType($private['file'])];
		$info['filename']=dlib_remove_filename_extension($doc->srcOrigFname);
		break;
	case 'data-url':
		$info['headers']=['content-type'=>dlib_finfo($private['filename'])];
		$info['filename']=basename($private['filename']);
		copy($private['filename'],$dest);
		break;
	case 'proxy':
		$info['filename']=basename($private['remoteUrl']);
	default:
		require_once 'dlib/download-tools.php';
		$dlOptions['headers']=&$headers;
		$ok=dlib_curl_download($url,$dest,$dlOptions);
		if($ok===false){return false;}
		$info['headers']=$headers;
		if(!isset($info['filename'])){$info['filename']=basename($url);}
	}
	return $info;
}

//! Gets content-type from a url.
//! This function automatically recognizes private urls.
//! @param a url that can be both public (anywhere on the web) or private
function demosphere_get_content_type($url,$options=[])
{
	global $user;
	$private=demosphere_private_file($url);

	switch($private['type'])
	{
	case 'mail':
		if(!$user->checkRoles('admin','moderator')){dlib_permission_denied_403();}
		require_once 'mail-import/mail-import.php';
		$message=Message::fetch($private['id']);
		return $message->getPartType($private['part']);
	case 'docconvert':
		require_once 'docconvert/docconvert.php';
		try
		{
			$doc=new DocconvertDoc($private['docid']);
			return $doc->fileType(intval($private['file']));
		}
		catch(Exception $e){return false;}
		break;
	default: 
		require_once 'dlib/download-tools.php';
		$options['status']=&$status;
		$options['timeout']=3;
		$headers=dlib_curl_get_header($url,$options);
		if($headers===false){return false;}
		// Strange : Varnish cache refuses our request. Try full request.
		// Other sites too ?
		if($status==412 || $status==403)
		{
			$options['headers']=&$headers;
			$options['info']['status' ]=&$status;
			$ok=dlib_curl_download($url,'/dev/null',$options);
		}
		if($status>=400){return false;}
		$res=val($headers,'content-type');
		// If content type is "octet-stream", try to guess from filename in header attachment (some spip sites do this )
		if($res==='application/octet-stream')
		{
			if(preg_match('@attachment.*\.pdf\b@' ,val($headers,'content-disposition'))){$res='application/pdf';}
			if(preg_match('@attachment.*\.docx\b@',val($headers,'content-disposition'))){$res='application/vnd.openxmlformats-officedocument.wordprocessingml.document';}
		}
		return $res;
	}
}

// ***************************************

//! Finds images in an html string and downloads all non local images.
//! Warning: this is not XSS foolproof. Any html processed by this should be XSS filtered before being sent to the client.
function demosphere_make_images_local($html,$checkIfAlreadyExists=false,$testDir=false)
{
	global $base_url,$demosphere_config;

	$html=preg_replace_callback('@(<img[^>]*\bsrc\s*=\s*")([^"\'>]*)@s',function($m)use($checkIfAlreadyExists,$testDir)
    {
		$url=html_entity_decode($m[2],ENT_COMPAT,'UTF-8');
		$newUrl=demosphere_make_image_url_local($url,$checkIfAlreadyExists,$testDir);
		return $m[1].ent($newUrl);
	},$html);
	return $html;
}


//! Download an image and use imagemagick convert to a simple format and put it into the standar dimage directory.
//! The image directory can be changed (used for testing).
function demosphere_make_image_url_local($url,$checkIfAlreadyExists=false,$destDir=false)
{
	global $demosphere_config,$base_url;
	static $goodTypes=['image/png'=>'png',
					   'image/jpg'=>'jpg',
					   'image/jpeg'=>'jpg',
					   'image/gif'=>'gif'];

	if($destDir===false){$destDir='files/import-images';}

	$brokenImage=$base_url.'/demosphere/css/images/broken-image.png';

	// Optionally, do not re-download if file exists
	if($checkIfAlreadyExists)
	{
		$imgPrefix=$destDir.'/'.md5($url);
		foreach(array_unique($goodTypes) as $ext)
		{
			if(file_exists($imgPrefix.'.'.$ext)){return $base_url."/".$imgPrefix.'.'.$ext;}
		}
	}

	if(strpos($url,'/')===0){$url=$base_url.''.$url;}
	// if this image is already local, ignore it
	if(strpos($url,$base_url.'/'.$destDir)===0){return $url;}
	// download the images
	$downloadFname=tempnam($demosphere_config['tmp_dir'],"make_images_local-");
	//dlib_message_add(t("downloading image:").ent($url));
	$info=demosphere_download_file($url,$downloadFname);
	if($info===false)
	{
		dlib_message_add(t('download image failed for:').ent(mb_substr($url,0,300)),'error');
		if(file_exists($downloadFname)){unlink($downloadFname);}
		return $brokenImage;
	}
	// figure out the image type
	$headerContentType=val($info['headers'],'content-type');
	$ufname=val($info,'filename');
	$suffix=val($goodTypes,$headerContentType,'png');
	// don't use downloaded image directly (security & reliability), 
	// always do conversion with Imagemagick "convert"
	$imgFname=$destDir.'/'.md5($url).".".$suffix;
	// Convert image (errors are common and normal, so don't log them)
	$ret=dlib_logexec("convert ".escapeshellarg($downloadFname)." ".escapeshellarg($imgFname),false,null);
	unlink($downloadFname);
	if($ret!=0)
	{
		dlib_message_add(t('convert image failed for:').ent($url),'error');
		return $brokenImage;
	}
	// FULL URL (not absolute path)  is needed, otherwise paste in htmleditor breaks
	$importedImageUrl=$base_url."/".$imgFname;
	return $importedImageUrl;
}

//! Check if $linkUrl image is higher res version of $srcImg
//! Returns a similarity measure: > 1 means probably same images.
//! Returns null on failure, false if linkImg is smaller.
//! Identical images will return 100000
function demosphere_highres_img_check($srcImg,$linkImg)
{
	global $demosphere_config;
	// Get image sizes and check if link is bigger image than src
	$srcSize =getimagesize($srcImg );
	$linkSize=getimagesize($linkImg);
	$linkIsBigger=$linkSize[0]*$linkSize[1] >= $srcSize[0]*$srcSize[1];
	if(!$linkIsBigger){return false;}

	// Resize link to same size as src image
	$shrunkImg=tempnam($demosphere_config['tmp_dir'],"highres-img-compare-");
	$ret=dlib_logexec("convert ".escapeshellarg($linkImg).
					  " -geometry ".intval($srcSize[0]).'x'.intval($srcSize[1])."\\! ".
					  escapeshellarg($shrunkImg));
	if($ret!==0){unlink($shrunkImg);return null;}
	$linkImg=$shrunkImg;

	// Now compare images that have same size using Imagemagick "compare" cmd with PSNR
    // FIXME: https://www.marxiste.org Joomla: resize + crop :-(
    // Imagemagick compare has a -subimage-search option. This doesn't work because images are scaled too.
    // We could try to guess scale, assuming crop was done only in one direction (probably horizontal).
    // Probably not worth the effort.
	$cmd="compare -metric PSNR ".escapeshellarg($srcImg)." ".escapeshellarg($linkImg)." /dev/null 2>&1";
	exec($cmd,$output,$ret);
	if($ret==2 || !is_array($output) || count($output)==0){return null;}
	if($output[0]==='inf'){return 100000;}
	if(!is_numeric($output[0])){return null;}
	$psnr=(float)$output[0];
	return $psnr/20;
}

// ***************************************


//! Transforms all image src urls into local, proxied urls.
//! This is used by feed import to avoid mixed content (https inside http) and to protect privacy of moderators.
function demosphere_image_proxy($html,$downloadNow=false)
{
	global $demosphere_config,$base_url;
	require_once 'dlib/html-tools.php';

	$doc=dlib_dom_from_html($html,$xpath);

	foreach($xpath->query("//img/@src") as $attr)
	{
		$remoteUrl=$attr->value;
		// FIXME: we should handle data: urls 
		// (note: feed import deletes them in Article::downloadHtml(), immediately after download...)
		if(!preg_match('@^https?://@i',$remoteUrl))
		{
			$attr->value='proxy-found-invalid-url-please-check-logs';
			dlib_log('demosphere_image_proxy: invalid remoteUrl: '.$remoteUrl);
			continue;
		}

		// We do not support srcset (responsive)
		if($attr->ownerElement->hasAttribute('srcset')){$attr->ownerElement->removeAttribute('srcset');}

		list($fname,$encUrl)=demosphere_image_proxy_remote_url_encode($remoteUrl);
		$cachedFname='files/images/proxy/'.$fname;

		// don't download if file already exists (avoid too many requests on remote server if same image used often)
		if($downloadNow && !file_exists($cachedFname))
		{
			demosphere_image_proxy_download($remoteUrl,$cachedFname);
		}

		$attr->value=ent($base_url.'/'.$cachedFname.'?url='.$encUrl);
	}

	$html=dlib_dom_to_html($doc);

	return $html;
}

//! Encodes a remote url into a filename and an encoded url.
//! The filename is a short hash that can be checked for security.
//! This ensures that the file name is short (avoids apache and filesystem errors)
//! The encoded url is meant to be passed as a GET arg: ?url=abcd123...
//! Example:
//! $remoteUrl : https://example.org/abc.png
//! return value: ['c9c90a66055bfa645ce5.png','cb28292928b6d2d74fad48cc2dc849d5cb2f4ad74f4c4ad62bc84b0700']
function demosphere_image_proxy_remote_url_encode($remoteUrl)
{
	global $demosphere_config;
	$hash=substr(hash('sha256',$remoteUrl.':'.$demosphere_config['secret']),0,20);

	// Guess a suffix. Ok if it's wrong : the downloaded image will be converted to the right format.
	$suffix=preg_replace('@^.*\.@','',$remoteUrl);
	$suffix=preg_replace('@\?.*$@','',$suffix);
	$suffix=strtolower($suffix);
	// others default to "png"
	$suffixes=['webp'=>'webp',
			   'jpeg'=>'jpg',
			   'gif' =>'gif',
			   'jpg' =>'jpg',];
	$suffix=val($suffixes,$suffix,'png');

	$encUrl=bin2hex(gzdeflate($remoteUrl));
	return [$hash.'.'.$suffix,$encUrl];
}


function demosphere_image_proxy_ajax_remote_url_encode()
{
	global $base_url;
	$remoteUrl=$_GET['remoteUrl'];
	list($fname,$encUrl)=demosphere_image_proxy_remote_url_encode($remoteUrl);
	$url=$base_url.'/files/images/proxy/'.$fname.'?url='.$encUrl;
	return ['proxiedUrl'=>$url];
}

//! Extracts the remote url that is encoded in the cached file name and checks the hash.
//! Also checks that the hash in the cached file name is valid.
//! Example:
//! $cachedFname: 63719a4cbaa4de3d1431.png, $encUrl: cb28292928b6d2d74fad48cc2dc849d5cb2f4ad74f4c4ad62bc84b0700
//! return value: https://example.org/abc.png
function demosphere_image_proxy_remote_url_decode($cachedFname,$encUrl)
{
	global $demosphere_config;
	// backwards compatibility (remove after 4/2016)
	if(preg_match('@^(.*/)?([0-9a-f]+)-([0-9a-f]+)\.[a-z]{1,5}$@',$cachedFname,$m))
	{
		$encUrl=$m[2];
		$foundSecHash=$m[3];
		$goodSecHash=substr(hash('sha256',$encUrl.':'.$demosphere_config['secret']),0,10);
		if($foundSecHash!==$goodSecHash){return false;}
		$remoteUrl=gzinflate(hex2bin($encUrl));
		return $remoteUrl;
	}

	if(!preg_match('@^(.*/)?([0-9a-f]{1,20})\.[a-z]{1,5}$@',$cachedFname,$m) ||
	   !preg_match('@^[0-9a-f]+$@',$encUrl)
	   )
	{dlib_bad_request_400('demosphere_image_proxy_remote_url_decode: invalid filename or enc url');}
	
	$foundHash=$m[2];
	$remoteUrl=gzinflate(hex2bin($encUrl));
	$goodHash=substr(hash('sha256',$remoteUrl.':'.$demosphere_config['secret']),0,20);
	if($foundHash!==$goodHash){return false;}	

	return $remoteUrl;
}

//! Downloads a remote image, caches it and serves it.
//! Example:
//! https://example.demosphere.net/files/images/proxy/12345789abcd-123456890.png 
//! Apache is configured to serve the image file directly if it exists and to call PHP (this function) only if the file doesn't exist.
function demosphere_image_proxy_view($cachedFname)
{
	$dir='files/images/proxy';
	$cachedFname=$dir.'/'.$cachedFname;
	$encUrl=val($_GET,'url');
	$remoteUrl=demosphere_image_proxy_remote_url_decode($cachedFname,$encUrl);
	if($remoteUrl===false){dlib_permission_denied_403();}
	$ok=demosphere_image_proxy_download($remoteUrl,$cachedFname);
	if($ok && file_exists($cachedFname))
	{
		require_once 'dlib/mail-and-mime.php';
		$mimeType=dlib_finfo($cachedFname);
		if($mimeType===false){fatal('demosphere_image_proxy_view: cannot determine mime type for cached file');}
		if(headers_sent()){fatal('Headers already sent, so we can\'t set Content-type for proxied image.');}
		header('Content-Type: '.$mimeType);
		readfile($cachedFname);
	}
	else
	{
		header("HTTP/1.0 502 Bad Gateway");
		header('Content-Type: text/html');
		echo $ok;
	}
}


//! Downloads a remote image and caches it.
function demosphere_image_proxy_download($remoteUrl,$cachedFname)
{
	global $demosphere_config;
	$tmpFname=tempnam($demosphere_config['tmp_dir'],"image_proxy-");
	// ** download the image to tmp file
	require_once 'dlib/download-tools.php';
	$log='';
	$headers=[];
	$ok=dlib_curl_download($remoteUrl,$tmpFname,['headers'=>&$headers,'info'=>['log'=>&$log]]);
	if(!$ok)
	{
		require_once 'dlib/filter-xss.php';
		@unlink($tmpFname);
		return 'Proxy image download failed for :<br/><a href="'.filter_xss_check_url($remoteUrl).'">'.
			filter_xss_check_url($remoteUrl).'</a><br/>'."\n".
			'<pre>'.ent($log).'</pre>';
	}

	exec("convert ".escapeshellarg($tmpFname)." ".escapeshellarg($cachedFname)." 2>&1",$output,$ret);
	@unlink($tmpFname);
	if($ret!==0)
	{
		require_once 'dlib/filter-xss.php';
		return 'Proxy image conversion failed for :<br/><a href="'.filter_xss_check_url($remoteUrl).'">'.
			filter_xss_check_url($remoteUrl).'</a><br/>'."\n".
			'<pre>'.ent(implode("\n",$output)).'</pre>';
	}
	return true;
}

//! Deletes old proxied images
function demosphere_image_proxy_cron()
{
	$tmpDir='files/images/proxy';
	$old=time()-3600*24*15;// delete more than 15 days old
	foreach(scandir($tmpDir) as $dirEntry)
	{
		if(!preg_match('@^([0-9a-f]{1,20})\.[a-z]{1,5}$@',$dirEntry)){continue;}
		$mtime=filemtime($tmpDir.'/'.$dirEntry);
		if($mtime<$old)
		{
			unlink($tmpDir.'/'.$dirEntry);
		}
	}
}


// ***************************************
// ********** translation sync  **********
// ***************************************


//! Returns a translation file. Used to fetch translations from language reference sites.
//! This is just a frontend for translation_backend_export_po (/translation-backend/export-po)
//! that is only accessible by admin.
function demosphere_remote_translation()
{
	require_once 'dlib/translation-backend.php';
	$file=val($_GET,'file');
	if($file!==false && !preg_match('@^(demosphere|dlib)\.[a-z]+\.po$@',$file)){dlib_permission_denied_403();}
	translation_backend_export_po();
}

// ***************************************
// ********** color and css template *****
// ***************************************

//! Returns a list of variables (such as palette colors) that can be used inside all .tpl.css files.
function demosphere_css_template_variables()
{
	global $currentPage,$css_template_config,$demosphere_config;
	// Hack so that sprite rebuild errors are not displayed inside css 
	static $firstTime=true;
	require_once 'dlib/sprite.php';
	if($firstTime){$firstTime=false;sprite_setup();}

	$paletteFg  =$demosphere_config['color_palette_fg'];
	$paletteBg  =$demosphere_config['color_palette_bg'];
	$paletteBg2 =$demosphere_config['color_palette_bg2'];
	if($demosphere_config['color_palette_text']!==false)
	{
		$paletteText=$demosphere_config['color_palette_text'];
	}
	else
	{
		$paletteText=css_saturate(css_darken($paletteFg,10),20);
	}

	$paletteFlashy=$demosphere_config['color_palette_flashy'];

	$topics=db_arrays_keyed("SELECT id,icon,color FROM Topic ORDER BY weight DESC");
	$topicIcons  =preg_replace('@.*/@','',preg_grep('@.@',dlib_array_column($topics,'icon' )));
	$topicColors =                        preg_grep('@.@',dlib_array_column($topics,'color'));
	unset($topics);

	//$paletteFg='#994433';
	//$paletteBg='#ddb58d';
	//$paletteBg2='#f3dec8';


	//$paletteFg='#672178';
	//$paletteBg='#eeeeee';
	//$paletteBg2='#efe1f5';

	//$paletteFg='#678514';
	//$paletteBg='#cedea9';
	//$paletteBg2='#cedea9';
	// 
	//$paletteFg='#932626';
	//$paletteBg='#eeeeee';
	//$paletteBg2=css_mix($paletteFg,'#fff',30);

	//$paletteText=css_darken($paletteFg,10);

	$tmDocEvent='#a62';
	$feedColor='#7869ff';
	$mailColor='#fcf';

	$isLightBg=$demosphere_config['color_is_light_background'];
	return get_defined_vars();
}

//! Changes $sprite_config when it is really needed. This is not done during demosphere_setup, to avoid a performance hit on every page load.
function demosphere_sprite_setup_extra()
{
	global $sprite_config;
	// Add topic and map icons to frontpage.png
	$topics=Topic::getAll();
	foreach($topics as $tid=>$topic)
	{
		if($topic->icon !=='')
		{
			$sprite_config['sprites']['frontpage.png']['images'][basename($topic->icon)]=['src_dir'=>dirname($topic->icon)];
			$sprite_config['sprites']['mobile.png'   ]['images'][basename($topic->icon)]=['src_dir'=>dirname($topic->icon)];
		}
	}
	// always create chart icon (used as default for topic sprites in css)
	$sprite_config['sprites']['frontpage.png']['images']['topic-chart.svg']=[];
	$sprite_config['sprites']['mobile.png'   ]['images']['topic-chart.svg']=[];
}


//! Preprocess colors in svg file before svg_to_png conversion.
//! Called from css_template_svg_to_png() through hook defined by $css_template_config['colormap'] and set in demosphere-setup.php
function demosphere_svg_colormap($type,$svg)
{
	global $demosphere_config,$css_template_config;
	require_once 'demosphere-misc.php';

	// only colormap original svg's not overridden ones!
	if(dirname($svg)!=='demosphere/css/images'){return $svg;}

	$vars=$css_template_config['variables']();
	$map=['#994433'=>$vars['paletteFg'],
		  '#ddb58d'=>$vars['paletteBg'],];
	$fix=array_keys($map);
	$fix[]='#ae674f';// fp-email
	$fix[]='#a17448';// fp-logo
	$fix[]='#f3e4d6';// fp-logo
	$fix[]='#5c2d00';// fp-logo

	$tmp=$demosphere_config['tmp_dir'].'/'.dlib_unique_fname($demosphere_config['tmp_dir'],"colormaped.svg");
	$fullSvg=file_get_contents($svg);
	$fullSvg=preg_replace_callback('@#[0-9a-f]{6}(?![0-9a-f])@s',function($m)use($map,$fix)
								   {
									   $color=$m[0];
									   if(array_search($color,$fix)===false){return $color;}
									   return demosphere_map_color($color,$map);
								   },$fullSvg);
	file_put_contents($tmp,$fullSvg);
	return $tmp;
}

//! Very hackish color interpollation. Used in demosphere_svg_colormap().
function demosphere_map_color($color0,$map)
{
	require_once 'dlib/css-template.php';
	// **** parse colors ('#abcde'=>['color',r,g,b])
	$color=css_template_parse_color($color0);
	$map['#fff']='#fff';
	$map=dlib_array_insert_assoc($map,dlib_first_key($map),'#000','#000');
	//print_r($map);
	$mapSrc =[];
	$mapDest=[];
	foreach($map as $src=>$dest)
	{
		$mapSrc []=css_template_parse_color($src);
		$mapDest[]=css_template_parse_color($dest);
	}
	//echo '$mapSrc:';print_r($mapSrc);
	//echo '$mapDest:';print_r($mapDest);
	
	$res=['color',0,0,0];
	for($channel=1;$channel<4;$channel++)
	{
		//echo "channel:$channel\n";
		// build map specific to this rgb channel
		$cmap=[];
		foreach($mapDest as $i=>$unused)
		{
			$cmap[intval(round($mapSrc[$i][$channel]))]=$mapDest[$i][$channel];
		}
		ksort($cmap);
		//echo 'cmap:';print_r($cmap);
		// interpolate color for this channel
		$cmapSrc=array_keys($cmap);
		$cmapDst=array_values($cmap);
		$prev=false;
		$next=false;
		foreach($cmapSrc as $src)
		{
			if($color[$channel]<=$src){$next=$src;break;}
			$prev=$src;
		}
		//echo "col:".$color[$channel]."\n";
		//echo "prev:$prev next:$next\n";
		if($prev===false){$cres=$cmapDst[0];}
		else
		if($next===false){$cres=$cmapDst[count($cmapSrc)-1];}
		else
		{
			$cres=($cmap[$next]-$cmap[$prev])*($color[$channel]-$prev)/($next-$prev) + $cmap[$prev];
		}
		$res[$channel]=$cres;
	}

	return css_template_color_to_css($res);
}

// ***************************************
// ********** Websockets  ****************
// ***************************************

// See /usr/local/bin/demoswebsockets-server.js for an explanation

//! Returns a public (not secret) hashed token that is stored by demoswebsockets-server to later authentify connections from this site.
function demosphere_websockets_token()
{
	global $demosphere_config;
	$token=hash('sha256','websocket-token:'.$demosphere_config['secret']);
	echo hash('sha256',$token);
}

//! Called by demoswebsockets-server to authentify a browser connection and to register it.
//! Authentification is done automatically, using cookies... and checked by path/roles ... so there's nothing left to do here.
function demosphere_websockets_browser_auth()
{
	global $user;
	return ['authOk'=> true,'tags'=>['FIXME',]];
}

//! Sends message to websocket server. 
//! Connects and sends aut line if necessary.
function demosphere_websockets_send_message($message)
{
	static $socket=false;

	if($socket===null){return false;}
	if($socket===false)
	{
		// Create socket
		$socket = @socket_create(AF_UNIX, SOCK_STREAM, 0);
		if ($socket === false)
		{
			dlib_log('websocket : socket_create() failed: reason: ' . socket_strerror(socket_last_error()));
			$socket=null;
			return false;
		}

		$address='/run/demoswebsockets/demoswebsockets.sock';
		// FIXME: test this timeout before enabling. How?
		//socket_set_option($socket, SOL_SOCKET, SO_SNDTIMEO, array('sec' => 0, 'usec' => 20000));
		$result = @socket_connect($socket, $address);
		if ($result === false)
		{
			dlib_log('websocket : socket_connect() failed ('.$result.') : ' . socket_strerror(socket_last_error($socket)));
			$socket=null;
			return false;
		}

		// Send auth line
		global $base_url,$demosphere_config;
		$authLine=json_encode(['domain'=>preg_replace('@^https?://@','',$base_url),
							  'unhashedToken'=>hash('sha256','websocket-token:'.$demosphere_config['secret'])])."\r\n";
		
		// FIXME: in theory this wight not write all of the buffer. We need to loop! http://www.php.net/socket_write
		$written=socket_write($socket, $authLine, strlen($authLine));
		if($written===false)
		{
			dlib_log('websocket auth message write failed');
			$socket=null;
			return false;
		}
	}

	// FIXME: encode in JSON ? Handle newlines in JSON ?
	$message.="\r\n";
	// FIXME: in theory this wight not write all of the buffer. We need to loop! http://www.php.net/socket_write
	$written=socket_write($socket, $message, strlen($message));

	return $written!==false;
}

// ***************************************
// ********** remote auth   **************
// ***************************************

//! Remote page makes js ajax request here to get login information.
//! 1) If 3rd party cookies are enabled, this will return full user information (login, email, token...)
//! 2) Otherwise it return an answer saying if the user's IP has logged into this site as mod or admin lately.
//!    In this case the js page needs to redirect to https://this.site/remote-auth-redirect 
//!    https://this.site/remote-auth-redirect will then redirect back to src page with full user information
function demosphere_remote_auth_check()
{
	global $user,$demosphere_config;
	if(!isset($_SERVER['HTTP_ORIGIN'])){dlib_bad_request_400('No http origin header in request');}
	$origin=$_SERVER['HTTP_ORIGIN'];
	if(array_search($origin,demosphere_remote_auth_sites())===false){dlib_permission_denied_403('Origin not in list of sites allowed for remote auth',false);}
	header('Access-Control-Allow-Origin: '.$origin);
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Allow-Methods: POST, OPTIONS, GET');
	header('Access-Control-Max-Age: 1');
	header('Vary: Origin');

	if(!$user->checkRoles('admin','moderator'))
	{
		$lastAccess=db_result("SELECT MAX(lastAccess) FROM User WHERE User.role IN (1,2) AND lastIp='%s'",
							  $_SERVER['REMOTE_ADDR']);
		if($lastAccess===null || $lastAccess<time()-3600*24*2){return ['ok'=>false];}
		return ['ok'=>'ip',];
	}
	else
	{
		return ['ok'=>true,'demosauth'=>demosphere_remote_demosauth(),];
	}
}

//! If 3rd party cookies are not enabled, we can only check if user is logged in by redirecting them to this page.
//! This page will immediately redirect back to the src page, with either a demosauth GET arg or an error GET arg.
function demosphere_remote_auth_redirect()
{
	global $demosphere_config,$user,$base_url;
	$srcUrl=$_GET['src'];
	$srcUrl.=strpos($srcUrl,'?')===false ? '?' : '&';
	$ok=false;
	foreach(demosphere_remote_auth_sites() as $url){if(strpos($srcUrl,$url.'/')===0){$ok=true;}}
	if(!$ok){dlib_permission_denied_403('src url not in list of sites allowed for remote auth',false);}
	if($user->checkRoles('admin','moderator'))
	{
		dlib_redirect($srcUrl.'demosauth='.demosphere_remote_demosauth());
	}
	else
	{
		dlib_redirect($srcUrl.'redirect-auth-failed-'.$demosphere_config['site_id'].
					  (preg_match('@_$@',$_GET['siteId']) ? '_' : '').// only used for distinguishing http/https (remove when we switch to https only)
					  "=1");
	}
}


//! List of sites that are allowed to use remote authentification.
function demosphere_remote_auth_sites()
{
	return ['https://software.local','https://software.demosphere.net'];
}

//! Returns a authentication string that can be used in url GET arg.
//! The string includes information about the user and a signature proving it is emitted by this site.
function demosphere_remote_demosauth()
{
	global $demosphere_config,$user;
	$data=$demosphere_config['site_id'].':'.$user->id.':'.$user->login.':'.val($_GET,'hashedBrowserId','');
	$privateKeyPem=variable_get('site_private_key');
	openssl_sign($data, $signature, $privateKeyPem, OPENSSL_ALGO_SHA256);
	$signature=base64_encode($signature);

	$demosauth=json_encode(['site_id'=>$demosphere_config['site_id'],
							'id'=>$user->id,
							'login'=>$user->login,
							'email'=>$user->email,
							'role'=>$user->role,
							'signature'=>$signature,
							]);
	$demosauth=urlencode(base64_encode($demosauth));	
	return $demosauth;
}

//! Very hackish way of reusing demosauth functionality for doing remote commands.
function demosphere_remote_demosauth_cmd($cmd)
{
	global $demosphere_config;
	$now=time();
	$data=$demosphere_config['site_id'].':'.'0'.':'.'remote-cmd-'.$cmd.'-'.$now.':'.hash('sha256','');
	$privateKeyPem=variable_get('site_private_key');
	openssl_sign($data, $signature, $privateKeyPem, OPENSSL_ALGO_SHA256);
	$signature=base64_encode($signature);

	$demosauth=json_encode(['site_id'=>$demosphere_config['site_id'],
							'id'=>0,
							'login'=>'remote-cmd-'.$cmd.'-'.$now,
							'email'=>'',
							'role'=>'',
							'signature'=>$signature,
							]);
	$demosauth=urlencode(base64_encode($demosauth));	
	return $demosauth;
}


//! Displays public key of this site.
function demosphere_public_key()
{
	header('Content-Type: application/x-pem-file');
	$privateKeyPem=variable_get('site_private_key');
	$privateKeyRsr=openssl_get_privatekey($privateKeyPem);
	$publicKeyPem= openssl_pkey_get_details($privateKeyRsr)['key'];
	echo $publicKeyPem;
}


// ***************************************
// ********** Call web browser  **********
// ***************************************


//! Call headless browser using a shell command 
//! Do not use this function directly.
function demosphere_browser_exec($url,$action,$options=[])
{
	global $dlib_config;
	$lang=preg_replace('@_.*$@','',$options['locale'] ?? setlocale(LC_TIME,"0"));

	$logFile=$options['log'] ?? $dlib_config['log'];
	
	$destFile=tempnam('/tmp','demosphere-browser-download-');
	if($destFile===false){return false;}
	// Make sure timeout is larger than wait (default timeout is 60s)
	if(intval($options['wait'] ?? 0)>30000 && !isset($options['timeout'])){$options['timeout']=30000+intval($options['wait']);}

	$cmd="/var/local/docker/browser/server-browser ".
		(isset($options['selector'    ]) ? '--selector='.    escapeshellarg($options['selector'    ]   ).' ' : '').
		(isset($options['wait'        ]) ? '--wait='    .            intval($options['wait'        ]   ).' ' : '').
		(isset($options['timeout'     ]) ? '--timeout=' .          floatval($options['timeout'     ]   ).' ' : '').
		(isset($options['sel_bg_color']) ? '--sel-bg-color='.escapeshellarg($options['sel_bg_color']   ).' ' : '').
		(isset($options['sel_border'  ]) ? '--sel-border='.  escapeshellarg($options['sel_border'  ]   ).' ' : '').
		(isset($options['data_file'   ]) ? '--data-file='.   escapeshellarg($options['data_file'   ]   ).' ' : '').
		(($options['scroll_down' ] ?? false) ? '--scroll-down=yes'                                      .' ' : '').
		(isset($options['width'       ]) || 
		 isset($options['height'      ]) ? '--window-size='.         intval($options['width' ] ?? 1200).','.
		 intval($options['height'] ?? 1080).' '  : '').
		'--lang='.     escapeshellarg($lang    ).' '.
		'--action='.   escapeshellarg($action  ).' '.
		'--dest-file='.escapeshellarg($destFile).' '.
		escapeshellarg($url);
	dlib_log("command: ".$cmd,$logFile);
	exec($cmd."  >> ".escapeshellarg($logFile)." 2>&1",$output,$ret);

	if($ret!==0)
	{
		unlink($destFile);
		dlib_log_error('previous browser command failed',false,$logFile);
	}

	return [$ret==0,$destFile];
}

//! Returns the HTML of a page as seen by a headless browser.
//! Since the page is executed in a browser, this works for js-generated pages.
//! $wait => ms
function demosphere_browser_download($url,$selector=false,$wait=0,$options=[])
{
	if($selector!==false){$options['selector']=$selector;}
	if($wait    !==0    ){$options['wait'    ]=$wait;}
	list($ok,$destFile)=demosphere_browser_exec($url,'html',$options);
	if(!$ok){return false;}

	$html=file_get_contents($destFile);
	unlink($destFile);
	return $html;
}

//! Returns a screenshot of a page, rendered by a headless browser.
//! The returned image is temporary and should be deleted after it has been used.
function demosphere_browser_screenshot($url,$width=false,$height=false,$options=[])
{
	if($width !==false){$options['width' ]=$width ;}
	if($height!==false){$options['height']=$height;}
	list($ok,$destFile)=demosphere_browser_exec($url,'screenshot',$options);
	if(!$ok){return false;}

	return $destFile;
}

//! Makes a screenshot with a part of a page designated by selector in a different color.
//! Returns an array with the image filename and 'top','left','right','bottom' coordinates of the selected box
//! The returned image is temporary and must be copied elsewhere.
function demosphere_browser_screenshot_selected($url,$selector,$width=false,$height=false,$options=[])
{
	$options['selector']=$selector;
	if($width   !==false){$options['width' ]=$width ;}
	if($height  !==false){$options['height']=$height;}

	$dataFile=tempnam('/tmp','demosphere-browser-screenshot-selected-data-');
	if($dataFile===false){fatal('tempnam failed');}
	$options['data_file']=$dataFile;
	
	list($ok,$destFile)=demosphere_browser_exec($url,'screenshot-selected',$options);

	if($ok===false){unlink($dataFile);return false;}

	$json=file_get_contents($dataFile);
	unlink($dataFile);
	if($json===false){unlink($destFile);return false;}
	// if selector fails, $json==='' and is decoded to null
	$res=json_decode($json,true);
	if($res===null  ){unlink($destFile);return false;}
	$res['file']=$destFile;
	return $res;
}

// ***************************************
// ********** Comments *******************
// ***************************************

function demosphere_comment_form_alter(Comment $comment,array &$items,array &$options)
{
	global $demosphere_config,$user;
	require_once 'demosphere-common.php';

	// Remove tracking from urls at save time
	// (Note: urls are shortened by CSS)
	$items['body']['pre-submit']=function(&$v)
		{
			$httpUrl='https?://[^[:space:]<>"\']*[^[:space:]<>"\'.]';
			$v=preg_replace_callback('@(?<![a-z0-9._-])(?P<httpUrl>'.$httpUrl.')(\b|(?<=/))@sui',
			 						 function($m)
	                                 {
										 $res=demosphere_url_remove_tracking($m[0]);
										 return $res;
									 },
									 $v);
		};

	$items['save']['submit']=function($items)
		{
			global $user;
			$comment=$items['form_object']['data'];
			if(!$user->checkRoles('admin','moderator'))
			{
				demosphere_comment_send_mail($comment);
			}
		};
}

//! Mail that is sent when comment form is submitted, for notification and archival
function demosphere_comment_send_mail(Comment $comment)
{
	global $demosphere_config,$base_url,$comments_config;

	$subject=$comment->blurb();

	$msg='';
	$msg.='<h3>'.t('A comment was submitted:').'</h3>';
	$msg.='<p>'.t('You can click on this link to check if it is ok.').'<br/>';
	$url=$comments_config['link']($comment);
	$msg.='<a href="'.ent($url).'">'.ent($url).'</a></p>';
	$msg.="<hr/>";
	$msg.=$comment->body;
	
	require_once 'demosphere-emails.php';
	demosphere_emails_notification_send('comment',$subject,$msg);
}

function demosphere_comment_body_render(string $html): string
{
	// We do not shorten urls here, we use CSS max-width for simpler copy/paste to text
	$html=demosphere_misc_text_urls($html,['class'=>['text-url'],'rel'=>'nofollow']);
	return $html;
}

function demosphere_comment_body_post_render(string $html): string
{
	require_once 'demosphere-htmlview.php';
	$html=demosphere_htmlview_emails_add_mailto_and_obfuscate($html);
	return $html;
}

//! Customize IP address display in comments manager
function demosphere_comment_manager_ip(string $ip): string
{
	global $base_url;
	$out='';
	require_once 'demosphere-search.php';
	list($search,$options)=demosphere_search_parse_get_options(['search'=>'"'.$ip.'"','event-date'=>'all']);
	list($foundDocs,$nbMatches)=demosphere_search_results($search,$options);
	if($nbMatches>0)
	{
		$ipSearchUrl=$base_url.'/search?search='.urlencode('"'.$ip.'"').'&event-date=all';
		$out.='<a href="'.ent($ipSearchUrl).'">'.ent(t('!nb search matches',['!nb'=>$nbMatches])).'</a>';
	}
	return $out;
}

//! Customize Event title and user display in comments manager
function demosphere_comment_manager_item(Comment $comment): string
{
	global $demosphere_config,$base_url;
	if($comment->pageType==='Event')
	{
		require_once 'demosphere-date-time.php';
		$event=db_array('SELECT startTime,title FROM Event WHERE id=%d',$comment->pageId);
		if($event===null){return '<span>Event not found</span>';}
		$out='';
		$out.='<span>'.ent(demos_format_date('shorter-date-time',$event['startTime'])).'</span>';
		$out.='<span><a href="'.ent($base_url.'/'.$demosphere_config['event_url_name'].'/'.$comment->pageId).'">'.
			ent(dlib_truncate($event['title'],50)).'</a></span>';
		return $out;
	}

	return '<span>'.ent($comment->pageType).'</span>'.
		   '<span>'.ent($comment->pageId  ).'</span>';
}


//! Called by Comment::save()
function demosphere_comment_save_hook(Comment $comment)
{
	require_once 'demosphere-search.php';
	demosphere_search_reindex_comment($comment);
	if($comment->pageType==='Event'){demosphere_page_cache_clear(Event::fetch($comment->pageId)->url());}
	else{demosphere_page_cache_clear_all();}
}

//! Called by Comment::delete()
function demosphere_comment_delete_hook(Comment $comment)
{
	require_once 'demosphere-search.php';
	demosphere_search_proxy_call_catch('demosphere_search_dx_delete_document','log','comment',$comment->id);
	if($comment->pageType==='Event'){demosphere_page_cache_clear(Event::fetch($comment->pageId)->url());}
	else{demosphere_page_cache_clear_all();}
}


// ********END: Comments *****************
// ***************************************


//! Transforms URLs in text and www.... URLs into HTML links.
//! Warning: this is not XSS safe. You must sanitize after using this.
//! See also: demosphere_htmledit_fix_urls_in_text()
function demosphere_misc_text_urls($html,array $linkAttrs=[])
{
	$httpUrl='https?://[^[:space:]<>"\']*[^[:space:]<>"\'.]';
	$wwwUrl='www\.([a-z0-9-]+\.){1,2}[a-z]{2,5}(/[^[:space:]]*)?';
	for($i=0;$i<1000;$i++)
	{
		$count=0;
		$html=preg_replace_callback('@(?P<begin>(>|^)[^><]*)(?<![a-z0-9._-])('.
									'(?P<httpUrl>'.$httpUrl.')|'.
									'(?P<wwwUrl>(?<!://)'.$wwwUrl.')'.
									')(\b|(?<=/))'.
									'(?P<end>[^><]*(<|$)(?!/a\b))@sui',
			function($matches)use($linkAttrs)
			{
				require_once 'dlib/filter-xss.php';
				$url=html_entity_decode($matches['httpUrl'].$matches['wwwUrl'],ENT_QUOTES,'UTF-8');
				$fullUrl=$url;
				if($matches['wwwUrl']!==''){$fullUrl='http://'.$fullUrl;}
				// Shortening long URLs is not done here
				return $matches['begin'].
					'<a href="'.filter_xss_check_url($fullUrl).'"'.dlib_render_html_attributes($linkAttrs).'>'.ent($url).'</a>'.
					$matches['end'];
			},$html,-1,$count);
		if(!$count){break;}
	}
	return $html;
}

//! Renders an email address as obfuscated HTML.
//! Replaces letters with html entites and adds invisible spans.
//! See also: demosphere_htmlview_emails_add_mailto_and_obfuscate()
function demosphere_misc_obfuscate_email($address)
{
	$obfuscated="";
	$len = mb_strlen($address);
	for ($i=0;$i<$len;$i++)
	{
		$c=mb_substr($address,$i,1);
		$o="&#".ord($c).";";
		if(($i%12)==0)
		{
			$obfuscated.='<span class="xsd">'.$o.'</span>';
		}
		else
		if(($i%12)==6)
		{
			$obfuscated.='<span style="display:none;">ex</span>'.$o;
		}
		else
		{
			$obfuscated.=$o;
		}
	}
	return $obfuscated;
}

?>