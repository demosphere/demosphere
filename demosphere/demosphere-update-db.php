<?php

function demosphere_update_db_0()
{
	global $demosphere_config;
	echo "Creating terms\n";
	Term::createTable();
	demosphere_config_update();
}

function demosphere_update_db_1()
{
	echo "Adding select-vocab-term in email subscriptions\n";
	$userIds=db_one_col("SELECT id FROM User");
	foreach($userIds as $id)
	{
		$cuser=User::fetch($id);
		if(isset($cuser->data['demosphere_email_subscription']))
		{
			$values=&$cuser->data['demosphere_email_subscription'];
			if(!isset($values["select-vocab-term"   ])){$values["select-vocab-term"   ]='';}
			if(!isset($values["select-vocab-term-or"])){$values["select-vocab-term-or"]='';}
			$cuser->save();
		}
	}
}

function demosphere_update_db_2()
{
	global $demosphere_config;
	echo "Purging old items in demosphere_config\n";
	$old=['demosevent_extra_vocabs','logout_message_page','topic_selector_lines','email_footer','mail_import_alt_database','feed_import_alt_database','page_watch_alt_database','text_matching_doctypes','text_matching_includes','text_matching_alt_database','std_base_dir','data_dir','files','site'] ;
	foreach($old as $k){unset($demosphere_config[$k]);}
	demosphere_config_update();	
}

function demosphere_update_db_3()
{
	global $demosphere_config;
	echo "Fixing office url in demosphere_config\n";
	$demosphere_config['remote_docconvert_url']='http://webtools.demosphere.eu/office-convert/office-convert.php';
	demosphere_config_update();
}

function demosphere_update_db_4()
{
	global $demosphere_config;
	echo "Purging bad numeric keys in demosphere_config\n";
	foreach(preg_grep("@^\d+$@",array_keys($demosphere_config)) as $k)
	{
		echo "removing $k\n";
		unset($demosphere_config[$k]);
	}
	demosphere_config_update();
}

function demosphere_update_db_5()
{
	global $demosphere_config;
	echo "Big upgrade in fake feeds: support for simple links-in-page fake feeds\n";
	db_query("ALTER TABLE FakeFeed ADD COLUMN type TINYINT(1) NOT NULL AFTER name");
	db_query("ALTER TABLE FakeFeed ADD COLUMN url text NOT NULL AFTER type");
	db_query("ALTER TABLE FakeFeed ADD COLUMN lipHtmlSelector longblob NOT NULL");
	db_query("ALTER TABLE FakeFeed ADD COLUMN lipLinkRegexp text NOT NULL");
	db_query("ALTER TABLE FakeFeed CHANGE code ccPhpCode longblob NOT NULL AFTER lipLinkRegexp");
	db_query('CREATE INDEX type ON FakeFeed(type)');
	db_query('CREATE INDEX feedId ON FakeFeed(feedId)');

	db_query('UPDATE FakeFeed set type=0');
	foreach(FakeFeed::fetchList("WHERE 1") as $id=>$ff)
	{
		if(preg_match('@^\s*\$(?P<html>\w+)\s*=\s*ff_fetch_page\s*\(\s*[\'"](?P<url>[^\'"]+)[\'"]\s*(,\s*"(?P<selector>[^"]+)")?\s*\)\s*;'.
					  '\s*\$(?P<links>\w+)\s*=\s*ff_get_all_inner_links\s*\(\s*\$(?P=html)\s*,\s*\'(?P<domain>[^\']*)\''.
					  ''.'(\s*,\s*\'?(?P<extraRegexp>[^\',]+)\'?(\s*,\s*(?P<failIfNoLinks>false))?)?\s*\)\s*;'.
					  '\s*(\$(?P=links)\s*=\s*array_unique\s*\(\s*\$(?P=links)\s*\)\s*;)?'.
					  '\s*ff_make_feed_from_links\s*\(\$(?P=links)\s*\)\s*;'.
					  '\s*$@s',$ff->ccPhpCode,$matches))
		{
			echo $ff->name.": convert to links-in-page\n";			
			$ff->type=1;
			$ff->url=$matches['url'];
			$ff->ccPhpCode='';
			$ff->lipHtmlSelector=[['type'=>'XPATH','arg'=>$matches['selector']=='' ? '/' : $matches['selector']]];
			$ff->lipLinkRegexp='@^https?://'.
				(preg_match('@^www\.@',$matches['domain']) ? '' : '[^/]*').
				preg_quote($matches['domain'],'@').'@';
			$extraRegexp=val($matches,'extraRegexp','');
			if($extraRegexp==='false'){$extraRegexp='';}
			if($extraRegexp!=='')
			{
				if(preg_match('@https?://@',$extraRegexp)){$ff->lipLinkRegexp=$extraRegexp;}
				else
				{
					$ff->lipLinkRegexp=preg_replace('/@$/','',$ff->lipLinkRegexp).'/.*'.str_replace('@','',$extraRegexp);
				}
			}
			$ff->save();
		}
	}

	demosphere_config_update();
}

function demosphere_update_db_6()
{
	global $demosphere_config;
	echo "Adding speedup for log, panel is too slow\n";
	db_query("UPDATE log SET type=IF(type = 'Event', 0, 1)");
	db_query("ALTER TABLE log CHANGE type type tinyint(3) NOT NULL");
	db_query("ALTER TABLE log ADD COLUMN speedup INT(11) NOT NULL");
	db_query("UPDATE log SET speedup=100*type_id+type");
	db_query('CREATE INDEX speedup ON log(speedup)');
}

function demosphere_update_db_7()
{
	global $demosphere_config;
	echo "Adding email col to Feed\n";
	db_query("ALTER TABLE Feed ADD COLUMN email varchar(330) NOT NULL AFTER comment");
}

function demosphere_update_db_8()
{
	global $demosphere_config;
	echo "Adding last_big_change col to Event\n";
	db_query("ALTER TABLE Event ADD COLUMN last_big_change int(11) NOT NULL AFTER changed_by_id");
	db_query('CREATE INDEX last_big_change ON Event(last_big_change)');
	db_query('UPDATE Event SET last_big_change=changed');

	require_once 'demosphere/demosphere-email-subscription.php';
	demosphere_email_subscription_update_defaults();
}

function demosphere_update_db_9()
{
	global $demosphere_config;
	echo "Lowercase topic colors\n";
	db_query("UPDATE Topic SET color=LOWER(color)");
}

function demosphere_update_db_10()
{
	global $demosphere_config;
	echo "Adding getContentFromFeedXml col to Feed\n";
	db_query("ALTER TABLE Feed ADD COLUMN getContentFromFeedXml TINYINT(1) NOT NULL AFTER fixHttpsToHttp");
}

function demosphere_update_db_11()
{
	global $demosphere_config;
	echo "Updating gmap_key \n";
	$demosphere_config['gmap_key']='AIzaSyDzoGkADv4F1mzPZnvhXwKKYrquK8V3mns';
	demosphere_config_update();
}

function demosphere_update_db_12()
{
	global $demosphere_config;
	echo "Updating event_map_static_url with lang and region\n";
	if($demosphere_config['event_map_static_url']=='http://maps.google.com/maps/api/staticmap?center=%lat,%lng&zoom=%zoom&size=%widthx%height&maptype=roadmap&markers=color:red|label:|%lat,%lng&sensor=false&key=%gmap_key' ||
	   $demosphere_config['event_map_static_url']=='http://maps.google.com/maps/api/staticmap?center=%lat,%lng&zoom=%zoom&size=%widthx%height&maptype=roadmap&markers=color:red|label:|%lat,%lng&sensor=false&key=%gmap_key&language=fr' 
)
	{
		$demosphere_config['event_map_static_url']='http://maps.google.com/maps/api/staticmap?center=%lat,%lng&zoom=%zoom&size=%widthx%height&maptype=roadmap&markers=color:red|label:|%lat,%lng&sensor=false&key=%gmap_key&language=%language&region=%region';
		echo "updated url\n";
	}

	demosphere_config_update();
}

function demosphere_update_db_13()
{
	global $demosphere_config;
	require_once 'demosphere/demosphere-email-subscription.php';
	echo "Email subscriptions: remove email-body\n";
	demosphere_email_subscription_foreach(function(&$values,$euser)
	{
		if(!isset($values["email-body"])){return;}
		unset($values["email-body"]);
		return true;
	});
}

function demosphere_update_db_14()
{
	global $demosphere_config;
	echo "Update help data for tinymce4\n";
	$post=Post::fetch(variable_get('help_pid'));
	$post->body=str_replace('#edit-body_','.button-',$post->body);
	$post->body=preg_replace('@(\.button-[a-z0-9-]+)\b@','$1:yoffset=-20',$post->body);
	$post->save();
}

function demosphere_update_db_15()
{
	global $demosphere_config;
	echo "Update help data : broken image urls\n";
	$post=Post::fetch(variable_get('help_pid'));
	$post->body=str_replace('sites/all/modules/demosphere/htmledit/images','demosphere/htmledit/images',$post->body);
	$post->body=preg_replace_callback('@<img[^>]*src="([^"]*)"@',function($m)
									  {
										  $src0=$m[1];
										  $src=substr($m[1],1);
										  if(strpos($src,'files/document-conversion')!==0 || file_exists($src)){return $m[0];}
										  $src='http://paris.demosphere.eu/'.$src;
										  $res=str_replace($src0,$src,$m[0]);
										  return $res;
									  }
									  ,$post->body);
	$post->save();
}

function demosphere_update_db_16()
{
	global $demosphere_config;
	echo "Updating demosphere_config (comments_disable) \n";
	demosphere_config_update();
}

function demosphere_update_db_17()
{
	global $demosphere_config;
	echo "Update help data : change HELPDESC format\n";
	$post=Post::fetch(variable_get('help_pid'));
	$post->body=preg_replace_callback('@HELPDESC:([^<]+)@s',function($m)
			{
				$res=$m[0];
				$res=str_replace('HELPDESC:@','HELPDESC:@^/',$res);
				$res=str_replace('\\:','XLKJHLKJHKDF',$res);
				$res=str_replace(':',';',$res);
				$res=str_replace('XLKJHLKJHKDF',':',$res);
				return $res;
			},
									  $post->body);
	$post->save();
 
	demosphere_help_parse_data($post->body);
}

function demosphere_update_db_18()
{
	global $demosphere_config;
	echo "Built-in pages setup\n";

	echo "Alter Post\n";
	db_query("ALTER TABLE Post ADD COLUMN builtIn varchar(330) NOT NULL AFTER id");
	db_query('CREATE INDEX builtIn ON Post(builtIn)');

	echo "Updating demosphere_config \n";
	foreach(['user_cal_help_page','contact_page','about_us_page','map_help_page','publishing_guidelines_page','editor_buttons'] as $conf)
	{
		unset($demosphere_config[$conf]);
	}
	demosphere_config_update();

	$names=['contact','about_us','publishing_guidelines','map_help','user_cal_help','spread_the_word','event_sources','help_event_emails','help_feeds','help_ical','help_on_your_website'];

	$paths['fr']=['contact','qui-sommes-nous','charte','aide-carte-interactive','aide-agenda-personnalise','faites-connaitre','sources-des-rendez-vous','aide-rdv-par-courrier','aide-flux-rss','aide-ical','aide-sur-votre-site-web'];

	$paths['es']=['contacto','quienes-somos','criterios-publicacion','ayuda-mapa-interactivo','ayuda-agenda-personalizada','danos-a-conocer','origen-eventos','help-event-emailsl','ayuda-rss','ayuda-ical','ayuda-en-tu-sitio-web'];

	$paths['gl']=['contacto','sobre-nos','orientacion-editorial','axuda-mapa-interactivo','axuda-calendario-persoal','difunde','fontes-dos-eventos','axuda-por-email','axuda-rss','axuda-ical','axuda-para-o-teu-sitio-web'];

	$paths['el']=['contact','bio','TOS',false/*map_help*/,false/*user_cal_help*/,false/*spread_the_word*/,false/*event_sources*/,'email','rss','ical',false/*help_on_your_website*/];

	$lang=dlib_get_locale_name();
	$stdPaths=array_combine($names,$paths[$lang]);

	foreach($stdPaths as $name=>$path)
	{
		$site=$demosphere_config['site_id'];
		// **** exceptions 
		if($lang==='es' && $name==='help_event_emails'   ){$path='ayuda-eventos-por-email';}
		if($lang==='es' && $name==='help_on_your_website'){$path='ayuda-para-tu-sitio-web';}
		if($site==='liege' && $name==='about_us'         ){$path='apropos';}
		// missing posts
		if((array_search($site,['angers','ariege','gironde'])!==false && $name==='help_on_your_website') ||
		   (array_search($site,['angers','liege'])!==false && $name==='spread_the_word') ||
		   $path===false)
		{
			if($path===false){$path=t(str_replace('_','-',$name));}
			$post=new Post($name,1,'');
			$post->save();
			variable_set('Post-'.$post->id,'comments-settings','disabled');
			demosphere_path_alias_set('post/'.$post->id,$path);
		}

		// FIXME account for el

		// *** Find pid for this path
		require_once 'demosphere-path-alias.php';
		$postPath=demosphere_path_alias_src($path);
		if(!preg_match('@^post/([0-9]+)$@',$postPath,$m)){die("failed to find path for $path");}
		$pid=intval($m[1]);
		// *** make it a built-in with correct name
		db_query("UPDATE Post SET builtIn='%s' WHERE id=%d",$name,$pid);
	}
	// *** two special casses: control panel and help
	db_query("UPDATE Post SET builtIn='help_data' WHERE id=%d",variable_get('help_pid'));
	db_query("UPDATE Post SET builtIn='control_panel_data' WHERE id=%d",variable_get('control_panel_pid'));

	// **** Un-publish posts that shouldn't be published
	db_query('UPDATE Post SET status=0 WHERE id=%d',Post::builtInPid('control_panel_data'));
	db_query('UPDATE Post SET status=0 WHERE id=%d',Post::builtInPid('help_data'));
	foreach($demosphere_config['front_page_infoboxes'] as $desc)
	{
		db_query('UPDATE Post SET status=0 WHERE id=%d',$desc['pid']);
	}

	// don't  demosphere_built_in_pages_sync yet, first make sure all ref sites are updated

	/*
FR Paths that are customized (or problems): 
======= liege : :fr:qui-sommes-nous           => apropos
======= angers : :fr:faites-connaitre         => ?
======= liege : :fr:faites-connaitre          => ?
======= angers : :fr:aide-sur-votre-site-web  => NONE!
======= ariege : :fr:aide-sur-votre-site-web  => NONE!
======= gironde : :fr:aide-sur-votre-site-web => NONE!
FIXME: when first update over, update the 6 missing posts by hand


ES Paths that are customized (or problems): 
======= valladolid : :es:help-event-emailsl    => ayuda-eventos-por-email
======= henares : :es:help-event-emailsl       => ayuda-eventos-por-email
======= asturias : :es:help-event-emailsl      => ayuda-eventos-por-email
======= valladolid : :es:ayuda-en-tu-sitio-web => ayuda-para-tu-sitio-web
======= henares : :es:ayuda-en-tu-sitio-web    => ayuda-para-tu-sitio-web
======= asturias : :es:ayuda-en-tu-sitio-web   => ayuda-para-tu-sitio-web

contact about-us publishing-guidelines map-help-page user-cal-help spread-the-word event-sources help-event-emails help-feeds help-ical help-on-your-website


contact qui-sommes-nous charte aide-carte-interactive aide-agenda-personnalise faites-connaitre sources-des-rendez-vous aide-rdv-par-courrier aide-flux-rss aide-ical aide-sur-votre-site-web

contacto quienes-somos criterios-publicacion ayuda-mapa-interactivo ayuda-agenda-personalizada danos-a-conocer origen-eventos help-event-emailsl ayuda-en-tu-sitio-web  

contacto sobre-nos orientacion-editorial axuda-mapa-interactivo axuda-calendario-persoal difunde fontes-dos-eventos axuda-por-email axuda-rss axuda-ical axuda-para-o-teu-sitio-web
	*/

	// get rid of unused variables
	variable_delete('help_pid','');
	variable_delete('control_panel_pid','');
}

function demosphere_update_db_19()
{
	global $demosphere_config;
	echo "Updating demosphere_config\n";
	unset($demosphere_config['event_form_extra']);
	demosphere_config_update();
}

function demosphere_update_db_20()
{
	global $demosphere_config;
	echo "Updating demosphere_config\n";
	unset($demosphere_config['use_maps']);
	demosphere_config_update();
}

function demosphere_update_db_21()
{
	global $demosphere_config;
	echo "Installing carpool\n";
	Carpool::createTable();
	db_query("ALTER TABLE Event ADD COLUMN use_carpool SMALLINT(3) NOT NULL AFTER repetition_ref_id");
	db_query('CREATE INDEX type ON Event(use_carpool)');
	db_query('UPDATE Event SET use_carpool=2');
	demosphere_config_update();	
}

function demosphere_update_db_22()
{
	global $demosphere_config;
	echo "Update help data : HELPDESC page ->  wrap\n";
	$post=Post::fetch(Post::builtInPid('help_data'));
	$post->body=preg_replace_callback('@HELPDESC;([^<]+)@s',function($m)
			{
				$res=$m[0];
				$res=str_replace(';page',';#wrap;xoffset=11;yoffset=11',$res);
				return $res;
			},
									  $post->body);
	$post->save();
 
	demosphere_help_parse_data($post->body);
}

function demosphere_update_db_23()
{
	echo "Colors in widgets to #123456 format\n";
	$uids=db_one_col("SELECT id FROM User WHERE data LIKE '%%demosphere_widget_config%%' ");
	foreach($uids as $uid)
	{
		$u=User::fetch($uid);
		$colorItems=['city-color','text-color','day-header-color','time-color'];
		$changed=false;
		foreach($colorItems as $colorItem)
		{
			if(!isset($u->data['demosphere_widget_config'][$colorItem])){$u->data['demosphere_widget_config'][$colorItem]=false;$u->save();}
			$c=$u->data['demosphere_widget_config'][$colorItem];
			if($c===false){continue;}
			if(strlen($c)===3){$c=$c[0].$c[0].$c[1].$c[1].$c[2].$c[2];}
			$c='#'.strtolower($c);
			$u->data['demosphere_widget_config'][$colorItem]=$c;
			$changed=true;
		}
		if($changed){$u->save();}
	}
	echo "Colors in topics to #123456 format\n";
	db_query("UPDATE Topic SET color= CONCAT('#',color) WHERE color!=''");
}

function demosphere_update_db_24()
{
	global $demosphere_config;
	require_once 'demosphere-topics.php';

	echo "New files/images and css directories\n";
	exec('rsync -av --exclude=marker.png --exclude=*~ --exclude=favicon.ico --exclude=.gitignore demosphere/install-directories/files/css/ ./files/css/',$out,$ret); 
	if($ret){fatal('demosphere_update_db_23: failed1');}
	exec('rsync -av --exclude=marker.png --exclude=*~ --exclude=favicon.ico --exclude=.gitignore demosphere/install-directories/files/images/ ./files/images/',$out,$ret); 
	if($ret){fatal('demosphere_update_db_23: failed2');}
	exec('chmod -R og+w files/css files/images 2>/dev/null',$out,$ret); 

	echo "Topic icons\n";
	db_query("ALTER TABLE Topic DROP iconSpritePos");
	db_query("ALTER TABLE Topic DROP mapIconSpritePos");

	db_query("UPDATE Topic set icon=CONCAT('files/images/',icon) WHERE icon!='' AND icon!='std://club' AND icon !='std://chart'");

	db_query("UPDATE Topic set icon='demosphere/css/images/topic-club.svg' WHERE icon='std://club'");
	db_query("UPDATE Topic set icon='demosphere/css/images/topic-chart.svg' WHERE icon='std://chart'");
	
	demosphere_topics_create_color_images();

	demosphere_config_update();	

	// ********* 
	echo "Set sites colors from list\n";
	$colorPresets=demosphere_config_color_presets();
	$siteColors=
		[
		 'crieur'=>14,
		 'gironde'=>3,
		 'kinimatorama'=>1,
		 'marseille'=>4,
		 'toulouse'=>8,
		 'correze'=>7,
		 'nice'=>15,
		 'rennes'=>16,
		 'carcassonne'=>16,
		 'rouen'=>16,
		 'gard'=>8,
		 'lille'=>13,
		 'loire-atlantique'=>3,
		 'tarn'=>4,
		 'valladolid'=>8,
		 'berry'=>17,
		 'henares'=>16,
		 'asturias'=>16,
		 'alpeshp'=>16,
		 'liege'=>8,
		 'poitiers'=>8,
		 'ain'=>8,
		 'liegetest'=>8,
		 'irunerria'=>7,
		 'strasbourg'=>16,
		 'pays-basque'=>7,
		 'corunha'=>18,
		 ];

	$n=val($siteColors,$demosphere_config['site_id'],0);
	$siteColor=array_search($n,dlib_array_column($colorPresets,'n'));
	if($siteColor===false){$siteColor='brown';}
	foreach($colorPresets[$siteColor] as $name=>$color)
	{
		if(strpos($name,'color_palette')!==0){continue;}
		$demosphere_config[$name]=$color;
	}

	$demosphere_config['extra_body_class'].=' color-preset-'.$siteColor;

	require_once 'dlib/css-template.php';
	$hsl=css_template_to_hsl(css_template_parse_color($demosphere_config['color_palette_bg']));
	$demosphere_config['color_is_light_background']=$hsl[3]>85;

	demosphere_config_update();	

	// ********* 
	echo "Fix infobox titles\n";

	foreach($demosphere_config['front_page_infoboxes'] as $desc)
	{
		db_query("UPDATE Post SET title=CONCAT(UPPER(SUBSTR(title,1,1)),SUBSTR(title,2)) WHERE title!='démosphère ?' AND id=%d",$desc['pid']);
		db_query("UPDATE Post SET title=LOWER(title) WHERE title='démosphère ?' AND id=%d",$desc['pid']);
	}

}
function demosphere_update_db_25()
{
	echo "Fix rss-log in infobox\n";
	global $demosphere_config;
	foreach($demosphere_config['front_page_infoboxes'] as $desc)
	{
		if(strpos($desc['class'],'infoboxStayInformed')===false){continue;}
		db_query("UPDATE Post SET body=CONCAT('<p id=\"rss-logo\">rss-logo</p>',body) WHERE id=%d",$desc['pid']);
	}
}

function demosphere_update_db_26()
{
	echo "Update config\n";
	demosphere_config_update();	
}

function demosphere_update_db_27()
{
	global $demosphere_config;
	echo "Remove map links when not customized\n";
	if($demosphere_config['extra_map_links']==[['url'=>'http://fr.mappy.com','name'=>'mappy'],
											   ['url'=>'http://www.pagesjaunes.fr','name'=>'pages jaunes']] ||
	   $demosphere_config['extra_map_links']==[['url'=>'http://fr.mappy.com/','name'=>'mappy'],
											   ['url'=>'http://www.pagesjaunes.fr','name'=>'pages jaunes']]
	   )
	{
		$demosphere_config['extra_map_links']=[];
	}

	demosphere_config_update();	
}

function demosphere_update_db_28()
{
	global $demosphere_config;
	echo "Moderation message becomes longtext\n";
	db_query("ALTER TABLE Event CHANGE moderation_message moderation_message longtext NOT NULL");
}

function demosphere_update_db_29()
{
	global $demosphere_config;
	echo "Config update : public_form_ask_price\n";
	demosphere_config_update();	
}

function demosphere_update_db_30()
{
	global $demosphere_config;
	echo "Event::Incomplete message : add column \n";
	db_query("ALTER TABLE Event ADD COLUMN incomplete_message longtext NOT NULL AFTER moderation_status");

	$msg=t('This is a repeating event. We haven\'t checked if the date @dateTime was ok. You should contact the organizers before you go. If you find out that this date is ok, please contact us',['@dateTime'=>'%dateTime']);

	db_query("UPDATE Event SET incomplete_message='%s' WHERE moderation_status=2",$msg);

	db_query("UPDATE Event SET moderation_status=3 WHERE moderation_status=2");

	demosphere_config_update();	
}

function demosphere_update_db_31()
{
	global $demosphere_config;
	echo "Add donate message\n";
	$post=new Post('FIXME donate title',1/* admin uid=1 */,'FIXME: write a donate message');
	$post->builtIn='donate_message';
	$post->save();
	variable_set('Post-'.$post->id,'comments-settings','disabled');
	demosphere_config_update();	
}

function demosphere_update_db_32()
{
	global $demosphere_config;
	echo "Title and city max width\n";
	unset($demosphere_config['event_title_max_size']);
	unset($demosphere_config['city_max_size']);
	demosphere_config_update();	
}

function demosphere_update_db_33()
{
	global $demosphere_config;
	echo "Moderation messages\n";
	demosphere_config_update();	
}

function demosphere_update_db_34()
{
	global $demosphere_config;
	echo "email_subscription_disable_message\n";
	demosphere_config_update();	
}

function demosphere_update_db_35()
{
	echo "install daily stats\n";
	require_once 'demosphere/demosphere-stats.php';
	demosphere_stats_daily_visits_install();
	// import from daystats files int daily_vists
	$daystatsDir="files/private/daystats";
	foreach(scandir($daystatsDir) as $daystatsFile)
	{
		if(!preg_match('@daystats-(.*)\.events$@',$daystatsFile,$m)){continue;}
		$date=$m[1];
		$dayTs=strtotime($date);
		$dayTs=strtotime('3:33',$dayTs);
		if($dayTs<time()-3600*24*35){continue;}
		echo "$daystatsFile\n";
		$topEventLines=trim(file_get_contents($daystatsDir.'/'.$daystatsFile));
		if($topEventLines==''){$topEventLines=[];}
		else{$topEventLines=explode("\n",$topEventLines);}
		// parse file and compute total nb of hits
		$eids=[];
		$topEvents=[];
		$sql='';
		foreach($topEventLines as $nb=>$line)
		{
			$parts=explode(" ",$line);
			$hits=intval($parts[0]);
			$eid =intval($parts[1]);
			$sql.=sprintf('(%d,%d,%d),',$eid,$dayTs,$hits);
		}
		if($sql!==''){db_query('INSERT INTO daily_visits (event,day,hits) VALUES '.substr($sql,0,-1));}
	}
}

function demosphere_update_db_36()
{
	global $demosphere_config;
	echo "new config: new_account_email\n";
	$demosphere_config['new_account_email']=$demosphere_config['submit_form_email'];
	demosphere_config_update();	
}

function demosphere_update_db_37()
{
	global $demosphere_config,$base_url;
	echo "new config: admin_menu\n";
	$stats=$demosphere_config['web_stats_link'];
	unset($demosphere_config['web_stats_link']);
	demosphere_config_update();	
	$stats=str_replace($base_url,'',$stats);
	$demosphere_config['admin_menu']['stats']['path']=$stats;
	demosphere_config_update();	
}

function demosphere_update_db_38()
{
	global $demosphere_config,$base_url;
	echo "new config: websockets_enabled\n";
	demosphere_config_update();	
}

function demosphere_update_db_39()
{
	global $demosphere_config;
	echo "Adding change_number col to Event\n";
	db_query("ALTER TABLE Event ADD COLUMN change_number int(11) NOT NULL AFTER changed_by_id");
	db_query('CREATE INDEX change_number ON Event(change_number)');
}

function demosphere_update_db_40()
{
	global $demosphere_config;
	echo "Adding created col to Article\n";
	db_query("ALTER TABLE Article ADD COLUMN created int(11) NOT NULL AFTER lastFetched");
	db_query('CREATE INDEX created ON Article(created)');
}

function demosphere_update_db_41()
{
	global $demosphere_config;
	echo "Installing opinions / contributor\n";
	db_query("ALTER TABLE User ADD COLUMN contributorLevel int(11) NOT NULL AFTER lastAccess");
	db_query('CREATE INDEX contributorLevel ON User(contributorLevel)');
	Opinion::createTable();
	demosphere_config_update();	
}

function demosphere_update_db_42()
{
	global $demosphere_config;
	echo "Installing RepetitionGroup\n";
	db_query("ALTER TABLE Event ADD COLUMN repetition_group_id int(11) NOT NULL AFTER repetition_ref_id");
	db_query('CREATE INDEX repetition_group_id ON Event(repetition_group_id)');

	db_query("ALTER TABLE self_edit ADD COLUMN repetition_group int(11) NOT NULL AFTER event");
	db_query('CREATE INDEX repetition_group ON self_edit(repetition_group)');

	RepetitionGroup::createTable();

	$repetitionRefIds=db_one_col('SELECT repetition_ref_id FROM Event WHERE repetition_ref_id!=0 GROUP BY repetition_ref_id');
	foreach($repetitionRefIds as $repetitionRefId)
	{
		$rg=new RepetitionGroup();
		$rg->referenceId=$repetitionRefId;
		$rg->save();
		db_query("UPDATE Event SET repetition_group_id=%d WHERE repetition_ref_id=%d",$rg->id,$repetitionRefId);
	}

	db_query("ALTER TABLE Event DROP COLUMN repetition_ref_id");

	demosphere_config_update();	
}

function demosphere_update_db_43()
{
	echo "SKIP: Adding modComment and modReputation to Opinion\n";
	//db_query("ALTER TABLE Opinion ADD COLUMN modComment longtext NOT NULL AFTER pubGuideline");
	//db_query("ALTER TABLE Opinion ADD COLUMN modReputation float NOT NULL AFTER modComment");
	//db_query('CREATE INDEX modReputation ON Opinion(modReputation)');
}

function demosphere_update_db_44()
{
	echo "Adding lastIp to User\n";
	db_query("ALTER TABLE User ADD COLUMN lastIp varchar(100) NOT NULL AFTER lastAccess");
	db_query('CREATE INDEX lastIp ON User(lastIp)');
}

function demosphere_update_db_45()
{
	echo "Creating public/private key\n";
	$privateKeyRsr = openssl_pkey_new([
								 "private_key_bits" => 2048,
								 "private_key_type" => OPENSSL_KEYTYPE_RSA,
								 ]);

	openssl_pkey_export($privateKeyRsr, $privateKeyPem);
	variable_set('site_private_key','',$privateKeyPem);
}

function demosphere_update_db_46()
{
	echo "Opinion: add isEnabled\n";
	db_query("ALTER TABLE Opinion ADD COLUMN isEnabled float NOT NULL AFTER modReputation");
	db_query('CREATE INDEX isEnabled ON Opinion(isEnabled)');
	db_query('UPDATE Opinion SET isEnabled=1');
}

function demosphere_update_db_47()
{
	global $demosphere_config;
	echo "Updating google map urls, using https\n";

	unset($demosphere_config['syncevent_is_hub']);
	unset($demosphere_config['syncevent_hub_choose_site_pid']);

	if($demosphere_config['country_map_url'      ]=='http://www.google.fr/maps'){$demosphere_config['country_map_url'     ]='https://www.google.fr/maps';}
	if($demosphere_config['country_map_url'      ]=='http://maps.google.com/'  ){$demosphere_config['country_map_url'     ]='https://www.google.com/maps';}

	if($demosphere_config['country_map_url'      ]=='http://maps.google.es/?ie=UTF8t'  ){$demosphere_config['country_map_url'     ]='https://www.google.es/maps/?ie=UTF8t';}


	if($demosphere_config['event_map_link'       ]=='https://maps.google.com/?q=%kml_link&z=%zoom'){$demosphere_config['event_map_link'      ]='https://www.google.com/maps/place/%lat,%lng/@%lat,%lng,%zoomz';}


	if($demosphere_config['event_map_static_url' ]=='http://maps.google.com/maps/api/staticmap?center=%lat,%lng&zoom=%zoom&size=%widthx%height&maptype=roadmap&markers=color:red|label:|%lat,%lng&sensor=false&key=%gmap_key&language=%language&region=%region'){$demosphere_config['event_map_static_url']='https://maps.googleapis.com/maps/api/staticmap?center=%lat,%lng&zoom=%zoom&size=%widthx%height&maptype=roadmap&markers=color:red|label:|%lat,%lng&sensor=false&key=%gmap_key&language=%language&region=%region';}

	demosphere_config_update();
}


function demosphere_update_db_48()
{
	echo "Creating public/private key (new sites)\n";
	if(variable_get('site_private_key')===false)
	{
		$privateKeyRsr = openssl_pkey_new([
										   "private_key_bits" => 2048,
										   "private_key_type" => OPENSSL_KEYTYPE_RSA,
										   ]);

		openssl_pkey_export($privateKeyRsr, $privateKeyPem);
		variable_set('site_private_key','',$privateKeyPem);
	}
}

function demosphere_update_db_49()
{
	global $demosphere_config;
 	echo "emails options and aliases\n";
	require_once 'demosphere-emails.php';
	if($demosphere_config['hosted'])
	{
		$admin=User::fetch(1);
		if($admin->login==='admin')
		{
 			$admin->data['emails-received-options']['never']=true;
			$admin->save();
 		}
 	}

	if(demosphere_emails_hosted_domain()!==false)
	{
		$aliases=demosphere_emails_hosted_fetch_data()['aliases'];
		unset($aliases[$demosphere_config['submit_form_email'       ]]);
		unset($aliases[$demosphere_config['new_account_email'       ]]);
		unset($aliases[$demosphere_config['sent_mails_archive_email']]);
		$ok=demosphere_emails_hosted_write_data(['aliases'=>$aliases]);
		if($ok!==true){fatal('failed saving aliases');}
	}

	unset($demosphere_config['submit_form_email'       ]);
	unset($demosphere_config['new_account_email'       ]);
	unset($demosphere_config['sent_mails_archive_email']);

	demosphere_emails_received_update_aliases();
	demosphere_config_update();
}

function demosphere_update_db_50()
{
	global $demosphere_config;
 	echo "Fix links for http/https\n";

	// *********** Post
	require_once 'demosphere-post.php';
	$bodies=db_one_col("SELECT id,body FROM Post WHERE ".
					   "body LIKE '%%href=\"%s%%' OR ".
					   "body LIKE '%%src=\"http%%'",
					   $demosphere_config['std_base_url']);
	foreach($bodies as $id=>$body)
	{
		echo "Updating Post body: $id\n";
		$body=demosphere_post_body_cleanup($body);
		db_query("UPDATE Post SET body='%s' WHERE id=%d",$body,$id);
	}

	// *********** Event
	require_once 'htmledit/demosphere-htmledit.php';
	$bodies=db_one_col("SELECT id,body FROM Event WHERE ".
					   "body LIKE '%%href=\"%s%%' OR ".
					   "body LIKE '%%src=\"http%%'",
					   $demosphere_config['std_base_url']);
	foreach($bodies as $id=>$body)
	{
		echo "Updating Event body: $id\n";
		//file_put_contents("/tmp/d/$id-before.html",$body);
		$body=demosphere_htmledit_submit_cleanup($body);
		//file_put_contents("/tmp/d/$id-after.html",$body);
		db_query("UPDATE Event SET body='%s' WHERE id=%d",$body,$id);
		//if(!(rand()%5))die("ii\n");
	}
}

function demosphere_update_db_51()
{
	global $demosphere_config;
 	echo "Fix urls for http/https\n";
	if($demosphere_config['remote_docconvert_url']==='http://webtools.demosphere.eu/office-convert/office-convert.php')
	{
		$demosphere_config['remote_docconvert_url']='https://webtools.demosphere.eu/office-convert/office-convert.php';
	}

	$demosphere_config['safe_base_url']=preg_replace('@^http:@','https:',$demosphere_config['safe_base_url']);

	demosphere_config_update();
}

function demosphere_update_db_52()
{
	global $demosphere_config;
 	echo "Image proxy dir\n";
	mkdir("files/images/proxy");
	chmod("files/images/proxy",0777);
}

function demosphere_update_db_53()
{
	global $demosphere_config;
	echo "remove date_format_middle_endian from config \n";

	unset($demosphere_config['date_format_middle_endian']);
	demosphere_config_update();
}

function demosphere_update_db_54()
{
	global $demosphere_config;
	echo "FakeFeedInfo: hash sql length \n";
	db_query("ALTER TABLE FakeFeedInfo CHANGE hash hash varchar(330) NOT NULL AFTER expires");
}

function demosphere_update_db_55()
{
	global $demosphere_config;
	echo "Fix bad chars in addresses \n";
    $addresses=db_one_col("SELECT id,address FROM Place WHERE address LIKE CONCAT('%%',CHAR(1),'%%')");
    foreach($addresses as $id=>$address)
    {
        db_query("UPDATE Place SET address='%s' WHERE id=%d",trim(preg_replace('@[\1][\1\s]*@',"\n",$address)),$id);
    }
    echo "fixed ".count($addresses)." addresses\n";
}

function demosphere_update_db_56()
{
	global $demosphere_config;
	echo "New reputation formula, new opinions config options\n";
	db_query("ALTER TABLE Opinion ADD COLUMN weight float NOT NULL AFTER reputation");
	db_query('CREATE INDEX weight ON Opinion(weight)');

	db_query('UPDATE Opinion SET reputation=0,weight=0');

	unset($demosphere_config['opinions_disagree_reputation']);
	demosphere_config_update();

	require_once 'demosphere/demosphere-opinion.php';
    $eids=db_one_col('SELECT eventId FROM Opinion GROUP BY eventId ORDER BY eventId ASC');
    foreach($eids as $eid)
    {
        demosphere_opinions_update_all_reputations_for_event($eid);
    }
}

function demosphere_update_db_57()
{
	echo "Allow empty Message:rawMail in sql; Rebuild help data.\n";
	db_query("ALTER TABLE Message CHANGE rawMail rawMail longblob");
	require_once 'demosphere/demosphere-help.php';
	demosphere_help_parse_data(Post::fetch(Post::builtInPid('help_data'))->body);

	// $demosphere_config['use_proxy_for_sphinx_search']
	demosphere_config_update();
}

function demosphere_update_db_58()
{
	global $demosphere_config;
	echo "Update mailboxes and aliases \n";
	require_once 'demosphere-emails.php';

	if(demosphere_emails_hosted_domain()===false){echo "not hosted domain, skipping update\n";return;}

	$data=demosphere_emails_hosted_fetch_data();
	
	// Add mailbox site-mailbox@demosphere.eu => site-mailbox@site.demosphere.eu
	$domain=demosphere_emails_hosted_domain();
	$oldAddress=$demosphere_config['site_id'].'-mailbox@demosphere.eu';
	$newAddress=$demosphere_config['site_id'].'-mailbox@'.$domain;
	foreach($data['aliases'] as $address=>&$alias)
	{
		foreach($alias['dests'] as &$dest)
		{
			if($dest===$oldAddress){$dest=$newAddress; echo "Changing: $address : $oldAddress=>$newAddress\n";}
		}
		unset($dest);
	}
	unset($alias);
	$data['mailboxes'][$newAddress]=['password'=>$demosphere_config['mail_import_pop_imap_password']];
	demosphere_emails_hosted_write_data($data);

	if(preg_match('@-mailbox$@',$demosphere_config["mail_import_pop_imap_username"]))
	{
		$demosphere_config["mail_import_pop_imap_username"].='@'.$domain;
	}
	
	demosphere_config_update();
}

function demosphere_update_db_59()
{
	echo "Add new class MailRule to replace MailFilter.\n";
	MailRule::createTable();
	
	$oldFilters=db_arrays("SELECT * FROM MailFilter ORDER BY id ASC");
	foreach($oldFilters as $oldFilter)
	{
		$order=(int)$oldFilter['order'];
		$rules=unserialize($oldFilter['rules']);
		$actions=unserialize($oldFilter['actions']);
		
		if(count($rules)===2 && 
		   $rules[0]===['type'=>'undisclosed'] && 
		   $rules[1]['type'        ]==='from'  &&
		   $rules[1]['how-to-match']==='contains'  )
		{
			$newMatchType='from-undisclosed';
			$newMatchString=$rules[1]['expression'];
		}
		else
		if(count($rules)===1 && 
		   $rules[0]['type'        ]==='from'  &&
		   $rules[0]['how-to-match']==='contains'  )
		{
			$newMatchType='from';
			$newMatchString=$rules[0]['expression'];
		}
		else
		if(count($rules)===1 && 
		   $rules[0]['type'        ]==='to'  &&
		   $rules[0]['how-to-match']==='contains'  )
		{
			$newMatchType='to';
			$newMatchString=$rules[0]['expression'];
		}
		else
		if(count($rules)===1 && 
		   $rules[0]['type'        ]==='subject'  &&
		   $rules[0]['how-to-match']==='contains'  )
		{
			$newMatchType='subject';
			$newMatchString=$rules[0]['expression'];
		}
		else
		if(count($rules)===1 && 
		   $rules[0]['type'        ]==='listid'  &&
		   $rules[0]['how-to-match']==='exact'  )
		{
			$newMatchType='list-id';
			$newMatchString=$rules[0]['expression'];
		}
		else
		{
			echo "Skipping unknown rule\n";
			//vd('unknown rules',$oldFilter['id'],$rules,$actions);
			continue;
		}

		if($newMatchType!=='listid' &&  $newMatchType!=='subject' && preg_match('@^[^<>]*<([^<>]+)>$@',$newMatchString,$m))
		{
			$newMatchString=$m[1];
		}

		// *********** actions

		if(count($actions)===1 && 
		   isset($actions['mail-importance']) &&
		   ($actions['mail-importance']==='ordinary' ||
		    $actions['mail-importance']==='trash'       ))
		{
			$newActionType='importance';
			$newActionString=$actions['mail-importance'];
		}
		else
		if(count($actions)===1 && 
		   isset($actions['source']))
		{
			$newActionType='source';
			$newActionString=$actions['source'];
		}
		else
		{
			//vd('unknown action',$oldFilter['id'],$rules,$actions);
			echo "Skipping unknown action\n";
			continue;
		}
		// insert directly into db since new Message fields are not created yet and calling MailRule::save() would fail
		db_query("INSERT INTO MailRule (matchType,matchString,actionType,actionString,priority) VALUES (%d,'%s',%d,'%s',%d)",
				 MailRule::matchTypeEnum($newMatchType),$newMatchString,MailRule::actionTypeEnum($newActionType),$newActionString,0);
	}
	variable_delete(false,'cache-mail-source');
	variable_delete(false,'cache-mail-importance');
}


function demosphere_update_db_60()
{
	echo "Add lots of fields to Message.\n";

	Message::cleanupCachedFiles(true);

	db_query("CREATE TABLE raw_mail (
  `messageId` int(11)      NOT NULL,
  `data`      longblob     NOT NULL,
  PRIMARY KEY  (`messageId`)
) DEFAULT CHARSET=utf8mb4;");

	db_query('INSERT INTO raw_mail (messageId,data) SELECT id,rawMail FROM Message');
	
	db_query("ALTER TABLE Message DROP COLUMN rawMail");
	db_query("ALTER TABLE Message DROP COLUMN isHidden");

	db_query("ALTER TABLE Message ADD COLUMN firstTo         text         NOT NULL AFTER `to`");
	db_query("ALTER TABLE Message ADD COLUMN source          varchar(255) NOT NULL AFTER importance");
	db_query("ALTER TABLE Message ADD COLUMN messageIdHeader varchar(255) NOT NULL AFTER dateFetched");
	db_query("ALTER TABLE Message ADD COLUMN inReplyToHeader varchar(255) NOT NULL AFTER messageIdHeader");
	db_query("ALTER TABLE Message ADD COLUMN subjectHash     varchar(255) NOT NULL AFTER inReplyToHeader");
	db_query("ALTER TABLE Message ADD COLUMN threadNb        int(11)      NOT NULL AFTER subjectHash");
	db_query("ALTER TABLE Message ADD COLUMN listId          varchar(255) NOT NULL AFTER threadNb");
	db_query("ALTER TABLE Message ADD COLUMN isSent          boolean      NOT NULL AFTER source");
	db_query('CREATE INDEX messageIdHeader ON Message(messageIdHeader)');
	db_query('CREATE INDEX inReplyToHeader ON Message(inReplyToHeader)');
	db_query('CREATE INDEX threadNb        ON Message(threadNb)');
	db_query('CREATE INDEX listId          ON Message(listId)');
	db_query('CREATE INDEX isSent          ON Message(isSent)');
	db_query('CREATE INDEX subjectHash     ON Message(subjectHash)');

	$ids=db_one_col("SELECT id FROM Message ORDER BY id ASC");
	foreach($ids as $id)
	{
		$m=Message::fetch($id);
		$m->firstTo               =$m->to;
		$m->to                    =$m->getHeader('to'         );
		$m->messageIdHeader       =$m->getHeader('Message-Id' );
		$m->inReplyToHeader       =$m->getHeader('In-Reply-To');
		$m->listId                =$m->getHeader('List-id'    );
		if(!$m->listId){$m->listId=$m->getHeader('X-list'     );}

		if(preg_match('/^(undisclosed|unspecified|destinataires?)[- ]'.
					  '(recipients?|inconnus?):?(@"\.MISSING-HOST-NAME\.")?$/i',
					  trim($m->firstTo))>0)
		{
			$m->to='';
			$m->firstTo='';
		}
		$m->subjectHash=Message::hashSubject($m->subject);
		$m->save();
		//dlib_message_add('msg:'.$m->id);
		//echo 'msg:'.$m->id."\n";
	}
	foreach($ids as $id)
	{
		$m=Message::fetch($id);
		$m->linkIntoThreads();
	}
}
function demosphere_update_db_61()
{
	echo "Refresh MailRule's on all Message's.\n";
	$ids=db_one_col("SELECT id FROM Message ORDER BY id ASC");
	foreach($ids as $id)
	{
		$m=Message::fetch($id);
		$m->refreshMailRules();
		$m->save();
	}
}

function demosphere_update_db_62()
{
	echo "Fixing inconsistencies between database schemas in new sites and existing sites\n";

	db_query('DROP TABLE IF EXISTS migrate_nodes');
	db_query('DROP TABLE IF EXISTS BadSentence');
	db_query('DROP TABLE IF EXISTS MailFilter');
	db_query("ALTER TABLE dbsessions CHANGE id id varchar(250) NOT NULL");
	if( count(db_arrays("SHOW INDEX FROM Event WHERE key_name = 'type'"       ))){db_query('DROP INDEX type ON Event');}
	if(!count(db_arrays("SHOW INDEX FROM Event WHERE key_name = 'use_carpool'"))){db_query('CREATE INDEX use_carpool ON Event(use_carpool)');}
	db_query("ALTER TABLE Opinion CHANGE isEnabled isEnabled tinyint(1) NOT NULL");
	db_query("ALTER TABLE path_alias CHANGE dest dest varchar(250) NOT NULL");
	db_query("ALTER TABLE path_alias CHANGE src src varchar(250) NOT NULL");
	db_query("ALTER TABLE place_search_index CHANGE word word varchar(250) NOT NULL");
	db_query("ALTER TABLE tm_keyphrase_doc CONVERT TO CHARACTER SET utf8");
	if( count(db_arrays("SHOW INDEX FROM Article WHERE key_name = 'guid'"       ))){db_query('DROP INDEX guid ON Article');}
	db_query('CREATE INDEX guid ON Article(guid(200))');
	db_query("ALTER TABLE Feed CHANGE url url TEXT NOT NULL");

	db_query("ALTER TABLE TMDocument CHANGE type type varchar(50) NOT NULL");
	db_query('CREATE INDEX tid  ON TMDocument(tid)');
	db_query('CREATE INDEX type ON TMDocument(type)');

	db_query('CREATE INDEX password ON self_edit(password)');
	db_query('CREATE INDEX ip ON log(ip)');
	db_query('CREATE INDEX role ON User(role)');
	db_query('CREATE INDEX lastLogin ON User(lastLogin)');
	if(count(db_arrays("SHOW INDEX FROM Article WHERE key_name = 'updated'"       )))
	{
		db_query('DROP INDEX updated ON Article');
		db_query('CREATE INDEX pubDate ON Article(pubDate)');
	}
	db_query('CREATE INDEX downloadStatus ON Page(downloadStatus)');
	db_query('CREATE INDEX lastFetched ON Page(lastFetched)');
}

function demosphere_update_db_63()
{
	global $demosphere_config;
	echo "updating demosphere_config and reindexing text matching\n";

	unset($demosphere_config['mail_import_delete_old_delay']);
	unset($demosphere_config['mail_import_small_contents']);
	unset($demosphere_config['mail_import_default_importance']);

	require_once 'text-matching/text-matching.php';
	text_matching_reindex();
	text_matching_update_ignored_keyphrases();

	demosphere_config_update();
}

function demosphere_update_db_64()
{
	global $demosphere_config;
	echo "Add title to Article\n";

	db_query("ALTER TABLE Article ADD COLUMN title varchar(255) NOT NULL AFTER feedId");

	$ids=db_one_col("SELECT id FROM Article WHERE fullXml!=''");
	foreach($ids as $id)
	{
		$a=Article::fetch($id);
		$sXml=simplexml_load_string($a->fullXml);
		$a->title=html_entity_decode($sXml->title,ENT_COMPAT,'UTF-8');
		$a->title=mb_substr($a->title,0,100);
		if(strlen($a->title)===0){$a->title=t('(no title)');}
		$a->save();
	}
}


function demosphere_update_db_65()
{
	global $demosphere_config,$mail_import_config;
	echo "Change demosphere_config imap settings for mail_import.\n";
	$old=$demosphere_config['mail_import_pop_imap'];
	unset($demosphere_config['mail_import_pop_imap']);
	$demosphere_config['mail_import_imap_username']=$demosphere_config['mail_import_pop_imap_username'];
	$demosphere_config['mail_import_imap_password']=$demosphere_config['mail_import_pop_imap_password'];
	unset($demosphere_config['mail_import_pop_imap_username']);
	unset($demosphere_config['mail_import_pop_imap_password']);

	demosphere_config_update();

	// just for following updates
	$mail_import_config['imap_server'  ]=$demosphere_config['mail_import_imap_server'  ];
	$mail_import_config['imap_inbox'   ]=$demosphere_config['mail_import_imap_inbox'   ];
	$mail_import_config['imap_username']=$demosphere_config['mail_import_imap_username'];
	$mail_import_config['imap_password']=$demosphere_config['mail_import_imap_password'];

}

function demosphere_update_db_66()
{
	echo "Add imap fields to Message.\n";

	Message::cleanupCachedFiles(true);

	db_query("ALTER TABLE Message ADD COLUMN imapMailbox     varchar(255) NOT NULL AFTER id");
	db_query("ALTER TABLE Message ADD COLUMN imapUid         int(11)      NOT NULL AFTER imapMailbox");
	db_query("ALTER TABLE Message ADD COLUMN headersHash     varchar(255) NOT NULL AFTER headers");
	db_query('CREATE INDEX imapMailbox ON Message(imapMailbox)');
	db_query('CREATE INDEX imapUid     ON Message(imapUid)');
	db_query('CREATE INDEX headersHash ON Message(headersHash)');

	db_query('UPDATE Message SET headersHash    =MD5(headers         )');
	db_query('UPDATE Message SET messageIdHeader=TRIM(messageIdHeader)');
	db_query('UPDATE Message SET inReplyToHeader=TRIM(inReplyToHeader)');
	db_query('UPDATE Message SET listId         =TRIM(listId         )');
}


function demosphere_update_db_67()
{
	global $demosphere_config,$mail_import_config;
	echo "Copy raw mail to imap server\n";

	// remove duplicates
	$ids=db_one_col("SELECT id FROM Message ORDER BY id ASC");
	echo "before remove dups: ".count($ids)."\n";
	foreach($ids as $id)
	{
		$dids=db_one_col("SELECT id FROM Message WHERE id<%d AND headersHash=(SELECT headersHash FROM Message AS m2 WHERE m2.id=%d)",$id,$id);
		if(count($dids)==0){continue;}
		db_query("DELETE FROM Message WHERE id IN (".implode(',',$dids).")");
	}

	require_once 'mail-import/mail-import.php';
	$serverAndMbox=$mail_import_config['imap_server'].$mail_import_config['imap_inbox'];
	$imap=imap_open($serverAndMbox,
					$mail_import_config['imap_username'],
					$mail_import_config['imap_password']);
	mail_import_imap_errors($imap!==false);
	variable_set('mail-import-inbox-lock','',time());
	$ids=db_one_col("SELECT id FROM Message ORDER BY id ASC");
	echo "after remove dups: ".count($ids)."\n";
	foreach($ids as $id)
	{
		$raw=db_result("SELECT data FROM raw_mail WHERE messageId=%d",$id);
		$ok=imap_append($imap,$serverAndMbox,$raw);
		mail_import_imap_errors($ok);
	}
	$ok=imap_close($imap);
	mail_import_imap_errors($ok);
	variable_delete('mail-import-inbox-lock','');
	// this forces reindex of mailbox Messages
	$imap=mail_import_imap_get_connection();
	// double check
	$ok=mail_import_imap_check_sync();
	if(!$ok)
	{
		fatal('Message export to imap has problems');
	}
	// delete old raw mail table!
	db_query('DROP TABLE raw_mail');
}

function demosphere_update_db_68()
{
	echo "Rebuild all messages since header parsing has changed slightly. Also fix listId in MailRules\n";

	$listIds=db_one_col("SELECT id,matchString FROM MailRule WHERE matchType=4");
	foreach($listIds as $id=>$listId)
	{
		$l0=$listId;
		$listId=preg_replace('@([^ ])<@','$1 <',$listId);
		db_query("UPDATE MailRule SET matchString='%s' WHERE id=%d",$listId,$id);
	}

	$ids=db_one_col("SELECT id FROM Message ORDER BY id ASC");
	foreach($ids as $id)
	{
		$message=Message::fetch($id);
		$message->buildFromImap($message->getImapConnection(),$message->imapUid);
		$message->save();
	}

	$msgLists =db_one_col("SELECT listId FROM Message GROUP BY listId");
	$ruleLists=db_one_col("SELECT matchString FROM MailRule WHERE matchType=4");

	$fix=[];
	foreach($msgLists as $msgList)
	{
		foreach($ruleLists as $ruleList)
		{
			if(dlib_simplify_text($msgList,false)==dlib_simplify_text($ruleList,false) && $msgList!=$ruleList)
			{
				$fix[$ruleList]=$msgList;
			}
		}
	}
	foreach($fix as $old=>$new)
	{
		$rules=MailRule::fetchList("WHERE matchType=4 AND matchString='%s'",$old);
		foreach($rules as $rule)
		{
			echo "Fixing rule ".$rule->id." : $old => $new\n";
			$rule->matchString=$new;
			$rule->save();
		}
	}
}

function demosphere_update_db_69()
{
	echo "Rehash old passwords\n";
	$badPasswordhashes=db_one_col("SELECT id,password FROM User WHERE password NOT LIKE '$6$%%' AND password NOT LIKE 'rehash$%%'");
	foreach($badPasswordhashes as $uid=>$badPasswordhash)
	{
		$newHash='rehash'.crypt($badPasswordhash,'$6$rounds=5000$'.dlib_random_string(15).'$');
		db_query("UPDATE User SET password='%s' WHERE id=%d",$newHash,$uid);
	}
}


function demosphere_update_db_70()
{
	echo "Switch to SHA256 for hashes in Message\n";

	db_query('UPDATE Message SET headersHash=SHA2(headers,256)');
}

function demosphere_update_db_71()
{
	echo "Text-matching : remove trashed events\n";
	require_once 'text-matching/text-matching.php';
	text_matching_reindex('event');
}

function demosphere_update_db_72()
{
	echo "Event : rename fields to CamelCase\n";

 	db_query('ALTER TABLE Event CHANGE  `html_title`          `htmlTitle`          varchar(330)  NOT NULL');
 	db_query('ALTER TABLE Event CHANGE  `start_time`          `startTime`          int(11)       NOT NULL');
 	db_query('ALTER TABLE Event CHANGE  `place_id`            `placeId`            int(11)       NOT NULL');
 	db_query('ALTER TABLE Event CHANGE  `author_id`           `authorId`           int(11)       NOT NULL');
 	db_query('ALTER TABLE Event CHANGE  `show_on_frontpage`   `showOnFrontpage`    tinyint(1)    NOT NULL');
 	db_query('ALTER TABLE Event CHANGE  `changed_by_id`       `changedById`        int(11)       NOT NULL');
 	db_query('ALTER TABLE Event CHANGE  `change_number`       `changeNumber`       int(11)       NOT NULL');
 	db_query('ALTER TABLE Event CHANGE  `last_big_change`     `lastBigChange`      int(11)       NOT NULL');
 	db_query('ALTER TABLE Event CHANGE  `moderation_status`   `moderationStatus`   tinyint(2)    NOT NULL');
 	db_query('ALTER TABLE Event CHANGE  `incomplete_message`  `incompleteMessage`  longtext      NOT NULL');
 	db_query('ALTER TABLE Event CHANGE  `moderation_message`  `moderationMessage`  longtext      NOT NULL');
 	db_query('ALTER TABLE Event CHANGE  `needs_attention`     `needsAttention`     smallint(3)   NOT NULL');
 	db_query('ALTER TABLE Event CHANGE  `repetition_group_id` `repetitionGroupId`  int(11)       NOT NULL');
 	db_query('ALTER TABLE Event CHANGE  `use_carpool`         `useCarpool`         smallint(3)   NOT NULL');
 	db_query('ALTER TABLE Event CHANGE  `external_source_id`  `externalSourceId`   int(11)       NOT NULL');
 	db_query('ALTER TABLE Event CHANGE  `extra_data`          `extraData`          longblob      NOT NULL');
 
 
 	db_query('ALTER TABLE Post  CHANGE  `author_id`           `authorId`           int(11)       NOT NULL');

	$rename=['html_title'          =>'htmlTitle'        ,	
			 'start_time'          =>'startTime'        ,	
			 'place_id'            =>'placeId'          ,	
			 'author_id'           =>'authorId'         ,	
			 'show_on_frontpage'   =>'showOnFrontpage'  ,	
			 'changed_by_id'       =>'changedById'      ,	
			 'change_number'       =>'changeNumber'     ,	
			 'last_big_change'     =>'lastBigChange'    ,	
			 'moderation_status'   =>'moderationStatus' ,	
			 'incomplete_message'  =>'incompleteMessage',	
			 'moderation_message'  =>'moderationMessage',	
			 'needs_attention'     =>'needsAttention'   ,	
			 'repetition_group_id' =>'repetitionGroupId',	
			 'use_carpool'         =>'useCarpool'       ,	
			 'external_source_id'  =>'externalSourceId' ,	
			 'extra_data'          =>'extraData'        ,]	;
	// old serialized events in event_revisions
	foreach(db_one_col("SELECT id,event FROM event_revisions") as $id=>$sevent)
	{
		$event=unserialize($sevent);
		//vd($event);
		foreach($rename as $old=>$new)
		{
			$event->$new=oval($event,$old,null);
			if(isset($event->$old)){unset($event->$old);}
		}
		//vd('=============== new',$event);
		db_query("UPDATE event_revisions SET event='%s' WHERE id=%d",serialize($event),$id);
	}
}

function demosphere_update_db_73()
{
	echo "Event : remove soft hyphens and Zero width space\n";
	db_query("UPDATE Event SET body=REPLACE(body,'­','') WHERE body LIKE '%%­%%'");
	db_query("UPDATE Event SET body=REPLACE(body,'​' ,'') WHERE body LIKE '%%​%%'");
}

function demosphere_update_db_74()
{
	echo "rename : demosmain_sites => slm_sites\n";
	variable_set('slm_sites','',variable_get('demosmain_sites'));
	variable_delete('demosmain_sites');
}


function demosphere_update_db_75()
{
	global $dbConnection;
	echo "Event.log: text=>array\n";

 	db_query('ALTER TABLE Event CHANGE log log longblob NOT NULL');

	$ids=db_one_col('SELECT id FROM Event');

	foreach($ids as $id)
	{
		$res=[];
		$log=db_result('SELECT log FROM Event WHERE id=%d',$id);

		$log0=$log;

		$entry0=false;
		if(preg_match('@^(Created from public form\nContact : ([^\n]*)\n)+@s',$log,$m))
		{
			$log=substr($log,strlen($m[0]));
			$entry0=$m[0];
		}


		$entries=preg_split('@#~-_ @',$log);
		foreach($entries as $entry)
		{
			if(trim($entry)===''){continue;}
			if(preg_match('@([0-9:-]+) u-([0-9]+)-([^\s]*) @',$entry,$m))
			{
				$timestamp=date_create_from_format('Y-m-d-H:i-s',$m[1])->getTimestamp();
				$uid=$m[2];
				$userName=$m[3];
				$message=trim(substr($entry,strlen($m[0])));
				$message=dlib_cleanup_plain_text($message);
				$message=preg_replace('@^\s*(.*)\s*$@m','$1',$message);
				$res[]=['ts'=>$timestamp,'uid'=>$uid,'name'=>$userName,'message'=>$message];
			}
			else
			{
				// ignore: rare cases, for old events
				//echo "$id: ";vd($entry);
				//echo $log0."\n";
				//echo $log."\n";
				//vd($entries);
			}
		}
		if($entry0!==false && count($res)!=0)
		{
			array_unshift($res,['ts'=>$res[0]['ts'],'uid'=>0,'name'=>'?','message'=>$entry0]);
			//vd($entry0,$res);
		}
		db_query("UPDATE Event SET log='%s' WHERE id=%d",serialize($res),$id);
	}
}

function demosphere_update_db_76()
{
	echo "Remove speedup field in log table\n";

	db_query("ALTER TABLE log DROP COLUMN speedup");
}


function demosphere_update_db_77()
{
	global $demosphere_config;
	echo "Event edit form: incomplete_messages: consistent names & messages\n";

	foreach($demosphere_config['incomplete_messages'] as &$entry)
	{
		if(mb_strlen($entry['message'])<60){$entry['name']=$entry['message'];vd($entry['message']);}
	}
	unset($entry);

	demosphere_config_update();
}

function demosphere_update_db_78()
{
	echo "Slow sql query fix\n";

	db_query('CREATE INDEX status_showOnFrontPage ON Event(status,showOnFrontPage);');
}

function demosphere_update_db_79()
{
	echo "demosphere_config_update: log_slow_queries\n";

	demosphere_config_update();
}

function demosphere_update_db_80()
{
	global $db_config;
	echo "switch mysql to utf8mb4 (real utf8)\n";
	db_query('ALTER DATABASE `%s` CHARACTER SET = utf8mb4',$db_config['database']);

	$changes=
		[
			['Article'            ,'title'           ,'varchar(250)'],
			['City'               ,'name'            ,'varchar(250)'],
			['City'               ,'shortName'       ,'varchar(250)'],
			['Comment'            ,'ip'              ,'varchar(250)'],
			['Comment'            ,'title'           ,'varchar(250)'],
			['FakeFeedInfo'       ,'hash'            ,'varchar(250)'],
			['Feed'               ,'email'           ,'varchar(250)'],
			['MailRule'           ,'matchString'     ,'varchar(250)'],
			['MailRule'           ,'actionString'    ,'varchar(250)'],
			['Message'            ,'imapMailbox'     ,'varchar(100)'],
			['Message'            ,'headersHash'     ,'varchar(250)'],
			['Message'            ,'messageIdHeader' ,'varchar(250)'],
			['Message'            ,'inReplyToHeader' ,'varchar(250)'],
			['Message'            ,'subjectHash'     ,'varchar(250)'],
			['Message'            ,'listId'          ,'varchar(250)'],
			['Message'            ,'source'          ,'varchar(250)'],
			['Page'               ,'name'            ,'varchar(250)'],
			['Post'               ,'builtIn'         ,'varchar(250)'],
			['Post'               ,'title'           ,'varchar(250)'],
			['Term'               ,'name'            ,'varchar(250)'],
			['Topic'              ,'name'            ,'varchar(250)'],
			['Topic'              ,'color'           ,'varchar(250)'],
			['User'               ,'login'           ,'varchar(250)'],
			['User'               ,'password'        ,'varchar(250)'],
			['User'               ,'email'           ,'varchar(250)'],
			['log'                ,'ip'              ,'varchar(250)'],
			['place_search_index' ,'word'            ,'varchar(191)'],
			['self_edit'          ,'password'        ,'varchar(100)'],
			['self_edit'          ,'email'           ,'varchar(250)'],
			['variables'          ,'name'            ,'varchar( 96)'],
			['variables'          ,'type'            ,'varchar( 96)'],
			['dbsessions'         ,'id'              ,'varchar(191)'],
			['path_alias'         ,'src'             ,'varchar(191)'],
		];

	foreach($changes as $change)
	{
		echo $change[0].' '.$change[1].' '.$change[1].' '.$change[2]."\n";
		db_query('ALTER TABLE %s CHANGE %s %s %s',$change[0],$change[1],$change[1],$change[2]);
	}

	db_query('ALTER TABLE page_cache CHANGE url url varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL');

	foreach(['Article','Carpool','City','Comment','Event','Event_Term','Event_Topic','FakeFeed','FakeFeedInfo','Feed','MailRule','Message','Opinion','Page','Place','Post','RepetitionGroup','TMDocument','Term','Topic','User','daily_visits','dbsessions','event_revisions','log','page_cache','page_revision','path_alias','place_search_index','self_edit','tm_keyphrase_doc','translations','user_calendar_selection','variables'] as $table)
	{
		echo "converting $table to utf8mb4\n";
		db_query('ALTER TABLE `%s` CONVERT TO CHARACTER SET utf8mb4',$table);
	}

	db_query('ALTER TABLE page_cache CHANGE url url varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL');

}

function demosphere_update_db_81()
{
	global $dbConnection,$db_config;
	echo "switch mysql to utf8mb4 (real utf8) - cleanup after errors\n";

	//$qres=mysqli_query($dbConnection,"SELECT * FROM migrate_nodes LIMIT 1");
	//if($qres!==false){return;}

	db_query('CREATE TABLE IF NOT EXISTS `migrate_nodes` (
  `nid` int(11) NOT NULL,
  `is_event` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  PRIMARY KEY (`nid`),
  KEY `is_event` (`is_event`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ');

	foreach(['page_cache','page_revision','path_alias','place_search_index','self_edit','tm_keyphrase_doc','translations','user_calendar_selection','variables','migrate_nodes'] as $table)
	{

		$charset=db_result('SELECT CCSA.character_set_name FROM information_schema.`TABLES` T,information_schema.`COLLATION_CHARACTER_SET_APPLICABILITY` CCSA WHERE CCSA.collation_name = T.table_collation AND T.table_schema = "%s" AND T.table_name = "%s"',$db_config['database'],$table);
		if($charset==='utf8mb4'){continue;}
		echo "converting $table to utf8mb4\n";
		db_query('ALTER TABLE `%s` CONVERT TO CHARACTER SET utf8mb4',$table);
	}

	db_query('ALTER TABLE page_cache CHANGE url url varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL');
}

function demosphere_update_db_82()
{
	global $dbConnection,$db_config;
	echo "Fix wrong index Event.changedById\n";
	
	if(count(db_arrays("SHOW INDEX FROM Event WHERE KEY_NAME = 'changedById'  "))){db_query('DROP INDEX changedById   ON Event');}
	if(count(db_arrays("SHOW INDEX FROM Event WHERE KEY_NAME = 'changed_by_id'"))){db_query('DROP INDEX changed_by_id ON Event');}
	db_query('CREATE INDEX changedById ON Event(changedById)');
}

function demosphere_update_db_83()
{
	global $dbConnection,$db_config;
	echo "Add missing index Message.firstFutureHLDate\n";
	
	if(!count(db_arrays("SHOW INDEX FROM Message WHERE KEY_NAME = 'firstFutureHLDate'  ")))
	{
		echo "adding\n";
		db_query('CREATE INDEX firstFutureHLDate ON Message(firstFutureHLDate)');
	}
}

function demosphere_update_db_84()
{
	global $dbConnection,$db_config;
	echo "Fix mismatching table schemas (null, default, text/mediumtext)\n";

	// Fix sizes
	db_query('ALTER TABLE path_alias         CHANGE src      src      varchar(191) NOT NULL');
	db_query('ALTER TABLE path_alias         CHANGE dest     dest     varchar(191) NOT NULL');
	db_query('ALTER TABLE self_edit          CHANGE password password varchar(100) NOT NULL');

	// Fix NULL and default
	$makenotnull=[['dbsessions','id'],['path_alias','src'],['place_search_index','word'],['self_edit','password'],['variables','name'],['variables','type'],['Article','title'],['City','name'],['City','shortName'],['Comment','ip'],['Comment','title'],['FakeFeedInfo','hash'],['Feed','email'],['log','ip'],['MailRule','actionString'],['MailRule','matchString'],['Message','headersHash'],['Message','imapMailbox'],['Message','inReplyToHeader'],['Message','listId'],['Message','messageIdHeader'],['Message','source'],['Message','subjectHash'],['Page','name'],['Post','builtIn'],['Post','title'],['self_edit','email'],['Term','name'],['Topic','color'],['Topic','name'],['User','email'],['User','login'],['User','password']];
	foreach($makenotnull as $t)
	{
		list($table,$col)=$t;
		$type=db_result("SELECT COLUMN_TYPE FROM information_schema.COLUMNS WHERE table_schema = '%s' AND table_name = '%s' AND COLUMN_NAME='%s'",$db_config['database'],$table,$col);
		echo "$table:$col:$type\n";
		db_query('ALTER TABLE `%s` CHANGE `%s` `%s` '.$type.'  NOT NULL',$table,$col,$col);
		
	}

	// Fix text/mediumtext
	$maketext=[
		['Article','guid'],['Article','url'],['Comment','body'],['FakeFeed','lipLinkRegexp'],['FakeFeed','url'],['Feed','url'],['Message','firstTo'],['Message','from'],['Message','subject'],['Message','to'],['page_cache','headers'],['Page','url'],['Place','address'],['translations','description']
			  ];
	foreach($maketext as $t)
	{
		list($table,$col)=$t;
		echo "$table:$col\n";
		db_query('ALTER TABLE `%s` CHANGE `%s` `%s` text  NOT NULL',$table,$col,$col);
	}
}

function demosphere_update_db_85()
{
	echo "Trim feed import Article urls in db\n";
	db_query('UPDATE Article SET url=TRIM(TRIM(BOTH CHAR(10) FROM url ))');
}

function demosphere_update_db_86()
{
	global $demosphere_config;
	echo "Big changes in feed-import. FakeFeed refactored.\n";

	require_once 'dcomponent/dcomponent-common.php';

	// *** FakeFeed table

	db_query("ALTER TABLE FakeFeed DROP COLUMN name");
	db_query("ALTER TABLE FakeFeed ADD COLUMN iCalUrl text NOT NULL AFTER lipLinkRegexp");
	db_query("ALTER TABLE FakeFeed ADD COLUMN ppCssSelector varchar(1000) NOT NULL AFTER iCalUrl");
	db_query("ALTER TABLE FakeFeed ADD COLUMN ppSplitRegex varchar(1000) NOT NULL AFTER ppCssSelector");
	db_query("ALTER TABLE FakeFeed ADD COLUMN fbId varchar(100) NOT NULL AFTER ppCssSelector");
	db_query('CREATE INDEX fbId ON FakeFeed(fbId)');

	// * Fix FakeFeed::feedId

	db_query('UPDATE FakeFeed SET feedId=0');
	$urls=db_one_col("SELECT id,url FROM Feed WHERE url LIKE '%%/feed-import/fake-feed/%%'");
	$fakeFeedIds=preg_replace('@^.*/feed-import/fake-feed/([0-9]+)/feed$@','$1',$urls);
	$fakeFeedIds=array_filter($fakeFeedIds,'ctype_digit');
	foreach($fakeFeedIds as $feedId=>$fakeFeedId)
	{
		db_query('UPDATE FakeFeed SET feedId=%d WHERE id=%d',$feedId,$fakeFeedId);		
	}

	$badFakeFeeds=db_arrays("SELECT * FROM FakeFeed WHERE feedId=0");
	if(count($badFakeFeeds))
	{
		echo "Delete ".count($badFakeFeeds)." invalid FakeFeeds (backed up in files/private/bad-fake-feeds):\n";
		file_put_contents('files/private/bad-fake-feeds',json_encode($badFakeFeeds));
		db_query("DELETE FROM FakeFeed WHERE feedId=0 OR NOT EXISTS (SELECT * FROM Feed WHERE Feed.id=FakeFeed.feedId)");
	}

	db_query("ALTER TABLE Feed DROP COLUMN displayOnlyHighlighted");
	// * move Feed errors from data to errors
	db_query("ALTER TABLE Feed ADD COLUMN errors longblob NOT NULL AFTER email");
	$dataList=db_one_col("SELECT id,data FROM Feed");
	foreach($dataList as $id=>$data)
	{
		$data=unserialize($data);
		$errors=val($data,'errors',[]);
		foreach(array_keys($errors) as $k)
		{
			$short='';
			if(preg_match('@^HTTP Status.*$@m',$errors[$k]['message'],$m)){$short=mb_substr($m[0],0,50);}
			$errors[$k]['label']='generic';
			$errors[$k]['short_message']=$short;
		}
		$errors=array_reverse($errors);
		db_query("UPDATE Feed SET errors='%s' WHERE id=%d",serialize($errors),$id);
		unset($data['errors']);
		db_query("UPDATE Feed SET data='%s'   WHERE id=%d",serialize($data  ),$id);
	}

	// Removing url in Feeds that use FakeFeed
	echo "Removing url in Feeds that use FakeFeed\n";
	db_query("UPDATE Feed SET url='' WHERE EXISTS (SELECT * FROM FakeFeed WHERE feedId=Feed.id)");

	// *** Transform Facebook feeds into Feed+FakeFeed
	$feedIds=db_one_col("SELECT id FROM Feed WHERE url LIKE '%%wallflux.com%%'");
	foreach($feedIds as $feedId)
	{
		$feed=Feed::fetch($feedId);
		if(!isset($feed->data['facebook'])){continue;}

		$fbId=$feed->data['facebook']['id'];
		$pageUrl=$feed->data['facebook']['pageUrl'];

		$feed->url='';
		unset($feed->data['facebook']);
		$feed->save();

		$fakeFeed=new FakeFeed($feed->id,$pageUrl,FakeFeed::typeEnum("facebook"));
		$fakeFeed->fbId=$fbId;
		$fakeFeed->feedId=$feed->id;
		$fakeFeed->save();
	}

	// *** use blank lipLinkRegexp when it is just host url (this simplifies url changes) 
	$fakeFeeds=FakeFeed::fetchList("WHERE 1");
	foreach($fakeFeeds as $fakeFeed)
	{
		if($fakeFeed->lipLinkRegexp==='@^'.preg_quote(dlib_host_url($fakeFeed->url),'@').'@')
		{
			$fakeFeed->lipLinkRegexp='';
		}
		$fakeFeed->save();
	}	
	
	// *** FakeFeedInfo is no longer used (we now use feed XML description)

	// Rename ff_create_articles => ff_create_items_from_contents
	db_query("UPDATE FakeFeed SET ccPhpCode=REPLACE(ccPhpCode,'ff_create_articles','ff_create_items_from_contents') WHERE ccPhpCode!=''");

	// custom-code uses items instead of FakeFeedInfo 
	db_query("UPDATE Feed SET getContentFromFeedXml=1 WHERE EXISTS ".
			 "    (SELECT * FROM FakeFeed WHERE feedId=Feed.id AND type=%d AND ccPhpCode LIKE '%%ff_create_items_from_contents%%')",
			 FakeFeed::typeEnum("custom-code"));

	db_query("DROP TABLE FakeFeedInfo");

	// *** Add fields to Article
	echo "Adding fields to Article\n";
	db_query("ALTER TABLE Article ADD COLUMN simpleText       longtext NOT NULL AFTER highlightInfo");
	db_query("ALTER TABLE Article ADD COLUMN firstFutureHLDate int(11) NOT NULL AFTER highlightInfo");
	db_query('CREATE INDEX firstFutureHLDate ON Article(firstFutureHLDate)');
	db_query("ALTER TABLE Article DROP COLUMN lastHighlighted");

	// update simpleText field and firstFutureHLDate
	echo "update simpleText field and firstFutureHLDate\n";
	$ids=db_one_col("SELECT id FROM Article WHERE created>%d",time()-3600*24*30);
	foreach($ids as $id)
	{
	 	$article=Article::fetch($id);
	 	$article->highlightContent();
	 	$article->save();
	}
}

function demosphere_update_db_87()
{
	echo "email_subscription_update_defaults.\n";
	require_once 'demosphere/demosphere-email-subscription.php';
	demosphere_email_subscription_update_defaults();
}

function demosphere_update_db_88()
{
	echo "Place description : cleanup html.\n";
	require_once 'htmledit/demosphere-htmledit.php';
	foreach(Place::fetchList("WHERE description!=''") as $place)
	{
		echo "update place description ".$place->id."\n";
		$place->description=demosphere_htmledit_submit_cleanup($place->description);
		$place->save();
	}
}

function demosphere_update_db_89()
{
	echo "RepetitionGroup : add ruleStart\n";
	db_query("ALTER TABLE RepetitionGroup ADD COLUMN ruleStart int(11) NOT NULL AFTER ruleData");
	db_query('CREATE INDEX ruleStart ON RepetitionGroup(ruleStart)');
}

function demosphere_update_db_90()
{
	global $demosphere_config;
	echo "Reset config for secondary_links\n";
	unset($demosphere_config['secondary_links']);
	demosphere_config_update();	
}

function demosphere_update_db_91()
{
	echo "Adding user in charge of a Place\n";
	db_query("ALTER TABLE Place ADD COLUMN inChargeId int(11) NOT NULL AFTER hideFromAltSuggestion");
	db_query('CREATE INDEX inChargeId ON Place(inChargeId)');
	db_query("ALTER TABLE Place ADD COLUMN inChargeChanged int(11) NOT NULL AFTER inChargeId");
	db_query('CREATE INDEX inChargeChanged ON Place(inChargeChanged)');

	foreach(db_arrays('SELECT id,title,address FROM Place') as $place)
	{
		db_query("UPDATE Place SET title='%s',address='%s' WHERE id=%d",
				 Place::cleanupTitle(  $place['title'  ]),
				 Place::cleanupAddress($place['address']),
				 $place['id']);
	}
}

function demosphere_update_db_92()
{
	echo "Cleanup public-form-events in variables (no longer used)\n";
	variable_delete(false,'public-form-events');
}

function demosphere_update_db_93()
{
	global $demosphere_config;
	echo "Remove public_form_user (no longer used)\n";
	User::fetch($demosphere_config['public_form_user'])->delete();
	unset($demosphere_config['public_form_user']);
	demosphere_config_update();	
}

function demosphere_update_db_94()
{
	global $demosphere_config;
	echo "Fix links to very old document-conversion images and links\n";
	// We checked that the strong "/document-conversion/" is used nowhere else... which makes it really simple :-)
	db_query("UPDATE Event SET body=REPLACE(body,'/document-conversion/','/docs/') WHERE body LIKE '%%/document-conversion/%%'");
	db_query("UPDATE Post  SET body=REPLACE(body,'/document-conversion/','/docs/') WHERE body LIKE '%%/document-conversion/%%'");
}


function demosphere_update_db_95()
{
	echo "Delete sprite-lock variable\n";
	variable_delete('sprite-lock');
}

function demosphere_update_db_96()
{
	echo "public_form_self_edit_link\n";
	demosphere_config_update();	
}

function demosphere_update_db_97()
{
	echo "feed-import twitter https: http://twitrss.me => https://twitrss.me\n";
	db_query("UPDATE Feed SET url=REPLACE(url,'http://twitrss.me','https://twitrss.me')");
}

function demosphere_update_db_98()
{
	echo "add dir for data-url images\n";
	mkdir('files/images/data-url');
	chmod("files/images/data-url",0777);
}

function demosphere_update_db_99()
{
	echo "Remove 'others' topic\n";
	$othersTopicId=false;
	foreach(Topic::getAll() as $topic)
	{
		if(preg_match('@^(autre|autres|outros|άλλο|otros|other)$@i',$topic->name))
		{
			$othersTopicId=$topic->id;
			break;
		}
	}

	if($othersTopicId!==false)
	{
		foreach(User::fetchList("WHERE data LIKE '%%select-topic\";i:%d%%'",$othersTopicId) as $u)
		{
			if(isset($u->data['demosphere_email_subscription']) && $u->data['demosphere_email_subscription']['select-topic']===$othersTopicId)
			{
				$u->data['demosphere_email_subscription']['select-topic']='others';
			}
			if(isset($u->data['demosphere_widget_config'     ]) && $u->data['demosphere_widget_config'     ]['select-topic']===$othersTopicId)
			{
				$u->data['demosphere_widget_config']['select-topic']='others';
			}
			$u->save();
		}

		Topic::fetch($othersTopicId)->delete();
	}

	$ids=db_one_col('SELECT id FROM Event WHERE startTime>%d AND status=1 AND NOT EXISTS (SELECT * FROM Event_Topic WHERE Event_Topic.event=Event.id)',Event::today());
	foreach($ids as $id)
	{
		$event=Event::fetch($id);
		$event->extraData['topic-was-set']=true;
		$event->save();
	}
	demosphere_config_update();	
}
function demosphere_update_db_100()
{
	echo "Update config: mapbox_access_token\n";
	demosphere_config_update();	
}
function demosphere_update_db_101()
{
	echo "Update config: require_keyword_in_event_title\n";
	demosphere_config_update();	
}

function demosphere_update_db_102()
{
	echo "Replace 'Document in PDF format' by icon\n";
	$pdfText='t'('Document in PDF format:');
	$eids=db_one_col("SELECT id FROM Event WHERE body LIKE '%%%s%%'",$pdfText);
	foreach($eids as $id)
	{
		$body=db_result("SELECT body FROM Event WHERE id=%d",$id);
		$body=preg_replace_callback('@(<p\b)([^>]*)(>\s*<a\b)([^>]*)(>)\s*'.preg_quote($pdfText,'@').'\s*<br\s*/?>\s*@si',function($matches)
		{
			$pAttrs=$matches[2];
			if(!preg_match('@class\s*=\s*"@',$pAttrs)){$pAttrs=' class="" '.$pAttrs;}
			if(strpos($pAttrs,'composite-doc-container')===false){$pAttrs=preg_replace('@class\s*=\s*"@','$0composite-doc-container ',$pAttrs);}
			$pAttrs=preg_replace('@class\s*=\s*"@','$0doc-type-pdf ',$pAttrs);

			$aAttrs=$matches[4];
			if(!preg_match('@class\s*=\s*"@',$aAttrs)){$aAttrs=' class="" '.$aAttrs;}
			if(strpos($aAttrs,'composite-doc')===false){$aAttrs=preg_replace('@class\s*=\s*"@','$0composite-doc ',$aAttrs);}

			return $matches[1].$pAttrs.$matches[3].$aAttrs.$matches[5];
		},$body);
		$body=db_query("UPDATE Event SET body='%s' WHERE id=%d",$body,$id);
	}
}

function demosphere_update_db_103()
{
	global $demosphere_config;
	echo "Adding select-fp-region and select-place-reference-id to event list. Adding frontpage_options to config\n";
	demosphere_config_update();	
	if(count($demosphere_config['frontpage_regions']))
	{
		$demosphere_config['frontpage_options']['order-by-last-big-changes']=['where'=>2];
	}
	demosphere_config_update();	
	require_once 'demosphere/demosphere-email-subscription.php';
	demosphere_email_subscription_update_defaults();
}

function demosphere_update_db_104()
{
	global $demosphere_config;
	echo "Changing frontpage map\n";
	$boxKey=dlib_first_key(preg_grep('@demosphereMapLink@',dlib_array_column($demosphere_config['front_page_infoboxes'],'class')));
	if($boxKey!==null)
	{
		$demosphere_config['front_page_infoboxes'][$boxKey]['title-link']='map';
		$post=Post::fetch($demosphere_config['front_page_infoboxes'][$boxKey]['pid'],false);
		if($post!==null)
		{
			$post->body='';
			foreach(['20:30','18:30','19:30'] as $time)
			{
				$ts=strtotime('today '.$time);
				require_once 'demosphere-date-time.php';
				$post->body.='<p>'.ent(demos_format_date('time',$ts)).'</p>'."\n";
			}
			$post->save();
		}
	}
	require_once 'demosphere-common.php';
	demosphere_cache_clear();
	unset($demosphere_config['remote_docconvert_url']);
	unset($demosphere_config['remote_docconvert_uid']);
	unset($demosphere_config['remote_docconvert_pass']);
	demosphere_config_update();
}

function demosphere_update_db_105()
{
	echo "updating demosphere urls (eu=>net & http=>https)\n";

    // update url in  body and post
	db_query("UPDATE Post  SET body=REPLACE(body,'demosphere.eu','demosphere.net') WHERE body LIKE '%%demosphere.eu%%'");
	db_query("UPDATE Event SET body=REPLACE(body,'demosphere.eu','demosphere.net') WHERE body LIKE '%%demosphere.eu%%'");

    // http => https
	db_query("UPDATE Post  SET body=REGEXP_REPLACE(body,'http(://[a-z0-9.-]*demosphere.net)','https\\\\1') WHERE body LIKE '%%http:%%demosphere.net%%'");
	db_query("UPDATE Event SET body=REGEXP_REPLACE(body,'http(://[a-z0-9.-]*demosphere.net)','https\\\\1') WHERE body LIKE '%%http:%%demosphere.net%%'");
}

function demosphere_update_db_106()
{
	echo "Fixing stray demosphere.eu=>demosphere.net in config\n";
	global $demosphere_config;
	$url=$demosphere_config["built_in_pages_sync_ref"];
	$url=preg_replace('@^http://@','https://',$url);
	$url=preg_replace('@wurzburg.demosphere.eu@','wuerzburg.demosphere.net',$url);
	$url=preg_replace('@demosphere.eu@','demosphere.net',$url);
	if($url!==$demosphere_config["built_in_pages_sync_ref"]){$demosphere_config["built_in_pages_sync_ref"]=$url;}
	demosphere_config_update();
}

function demosphere_update_db_107()
{
	echo "Feed-import add FakeFeed type twitter\n";
	foreach(Feed::fetchList("WHERE url LIKE 'https://twitrss.me/twitter_user_to_rss/?user=%%'") as $feed)
	{
		$twitterName=str_replace('https://twitrss.me/twitter_user_to_rss/?user=','',$feed->url);
		$feed->url='';
		$fakeFeed=new FakeFeed($feed->id,'https://twitter.com/'.$twitterName,FakeFeed::typeEnum("twitter"));
		$fakeFeed->save();
		$feed->save();
	}
}

function demosphere_update_db_108()
{
	global $demosphere_config;
	echo "Delete demosphere_config: map_init_nb_days (and a few old page entries) \n";

	foreach(['map_init_nb_days',"contact_page","about_us_page","map_help_page","user_cal_help_page","publishing_guidelines_page"] as $k)
	{
		unset($demosphere_config[$k]);
	}	
	demosphere_config_update();
}


function demosphere_update_db_109()
{
	echo "Add data to FakeFeed \n";
	db_query("ALTER TABLE FakeFeed ADD COLUMN data longblob NOT NULL AFTER fbId");
	db_query("UPDATE FakeFeed SET data='%s'",serialize([]));
}

function demosphere_update_db_110()
{
	echo "Add lastUpdated to Feed\n";
	db_query("ALTER TABLE Feed ADD COLUMN lastUpdated int(11) NOT NULL AFTER lastOk");
}

function demosphere_update_db_111()
{
	global $dbConnection,$demosphere_config;
	echo "Very big changes in comments and in dbsessions\n";
	require_once 'dlib/comments/comments.php';

	db_query("UPDATE variables SET type='comments-page-settings' WHERE type='comments-settings'");

	echo "Comment title+html body => text body\n";
	$ids=db_one_col('SELECT id FROM Comment');
	foreach($ids as $id)
	{
		$title=db_result('SELECT title FROM Comment WHERE id=%d',$id);
		$body =db_result('SELECT body  FROM Comment WHERE id=%d',$id);
		$body=Html2Text::convert($body);
		$title=trim($title);
		if($title!==''){$body="* ".$title." *\n\n".$body;}
		db_query("UPDATE Comment SET body='%s' WHERE id=%d",$body,$id);
	}

	IpWhois::createTable();
	Ban::createTable();

	echo "Fix multiple copies of selfEdit in _SESSION\n";
	foreach(db_one_col("SELECT id,data FROM dbsessions WHERE data LIKE '%%selfEdit%%'") as $id=>$data)
	{
		$d=dbsessions_session_decode($data);
		if(isset($d['selfEdit'])){$d['selfEdit']=array_unique($d['selfEdit']);}
		$data=dbsessions_session_encode($d);
		db_query("UPDATE dbsessions SET data='%s' WHERE id='%s'",$data,$id);
	}

	echo "Change dbsessions table\n";
	db_query("ALTER TABLE dbsessions CHANGE id id varchar(50) CHARACTER SET ascii NOT NULL");
	db_query('ALTER TABLE dbsessions ADD COLUMN n            INT(11) NOT NULL auto_increment AFTER id , ADD KEY (n)');
	db_query('ALTER TABLE dbsessions ADD COLUMN first_access INT(11) NOT NULL                AFTER uid, ADD KEY (first_access)');

	echo "Change Comment table\n";
	db_query("ALTER TABLE Comment ADD COLUMN parentId INT(11) NOT NULL AFTER id");
	db_query("ALTER TABLE Comment CHANGE type pageType varchar(100) CHARACTER SET ascii NOT NULL");
	db_query("ALTER TABLE Comment CHANGE typeId pageId int(11) NOT NULL");
	db_query("ALTER TABLE Comment CHANGE status isPublished TINYINT(1) NOT NULL");
	db_query("ALTER TABLE Comment ADD COLUMN needsAttention TINYINT(1) NOT NULL AFTER isPublished");
	db_query("ALTER TABLE Comment CHANGE ip ip varchar(100) CHARACTER SET ascii NOT NULL");
	db_query("ALTER TABLE Comment ADD COLUMN sessionN  int(11) NOT NULL AFTER ip");
	db_query("ALTER TABLE Comment ADD COLUMN userAgent varchar(500) CHARACTER SET ascii NOT NULL AFTER sessionN");
	db_query("ALTER TABLE Comment ADD COLUMN userAgentHash varchar(100) CHARACTER SET ascii  NOT NULL AFTER userAgent");
	db_query("ALTER TABLE Comment ADD COLUMN ipWhoisId INT(11) NOT NULL AFTER userAgentHash");
	db_query("ALTER TABLE Comment DROP COLUMN title");
	db_query("ALTER TABLE Comment CHANGE lastChanged created INT(11) NOT NULL AFTER body");
	db_query("ALTER TABLE Comment ADD COLUMN sortKey INT(11) NOT NULL AFTER created");
	db_query("ALTER TABLE Comment ADD COLUMN extraData longblob NOT NULL AFTER created");
	db_query('CREATE INDEX parentId ON Comment(parentId)');
	db_query('CREATE INDEX needsAttention ON Comment(needsAttention)');
	db_query('CREATE INDEX sessionN ON Comment(sessionN)');
	db_query('CREATE INDEX userAgentHash ON Comment(userAgentHash)');
	db_query('CREATE INDEX ipWhoisId ON Comment(ipWhoisId)');
	db_query('CREATE INDEX sortKey ON Comment(sortKey)');

	db_query('UPDATE Comment SET sortKey=created');

	// Add session info from old dbsessions, only for anonymous sessions and Comments
	echo "Move session comment info from dbsessions to Comment\n";
	$sessionData=db_one_col("SELECT n,data FROM dbsessions WHERE data LIKE '%%comment%%' AND uid=0");
	foreach($sessionData as $n=>$data)
	{
		$data=dbsessions_session_decode($data);
		$commentIds=$data['comments'] ?? [];
		if(!count($commentIds)){continue;}
		db_query("UPDATE Comment SET sessionN=%d WHERE userId=0 AND id IN (".implode(',',array_map('intval',$commentIds)).")",$n);
		unset($data['comments']);
		$data['hasComments']=true;
		db_query("UPDATE dbsessions SET data='%s' WHERE n=%d",dbsessions_session_encode($data),$n);
	}

	echo "Delete old bans\n";
	variable_delete('banned-ips','comments');

	echo "Create comment guidelines builtin page\n";
	$post=new Post(t('Comment guidelines'),1/* admin uid=1 */,$demosphere_config['comment_guidelines']);
	$post->builtIn='comment_guidelines';
	$post->status=1;
	$post->save();
	variable_set('Post-'.$post->id,'comments-page-settings','disabled');

	$demosphere_config['comments_site_settings']=$demosphere_config['comments_disable'] ? 'disabled' : 'enabled';
	unset($demosphere_config['comments_disable']);

    // ip whois "not-country" ban for France
	if(dlib_get_locale_name()=='fr')
	{
		$ban=new Ban();
		$ban->banType ='not-country';
		$ban->value='FR';
		$ban->end  =Ban::permanent();
		$ban->label='@created-at-install';
		$ban->save();
	}

	// tor ips
	$ban=new Ban();
	$ban->banType ='ip-list';
	$ban->value='/var/local/demosphere/tor-ips';
	$ban->end  =Ban::permanent();
	$ban->label='@tor-ips-created-at-install';
	$ban->save();

	unset($demosphere_config['comment_guidelines']);
	demosphere_config_update();
}

function demosphere_update_db_112()
{
	echo "Comments: ban label change for mod-button\n";
	db_query("UPDATE Ban SET label='@mod-button' WHERE label LIKE '@mod-button-%%'"); 
}
?>