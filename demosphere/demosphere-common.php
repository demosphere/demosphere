<?php


//! Redirect unconnected user to login page instead of showing 403 error.
//! This function is called by dlib_permission_denied_403(). 
//! It is setup in $dlib_config['permission_denied_403_override']
function demosphere_permission_denied_403($message=false,$tryLogin=true)
{
	global $user,$base_url;
	if($tryLogin==false || !isset($user) || $user->id!==0 || 
	   ($_SERVER['REQUEST_METHOD'] ?? false)!=='GET' || 
	   !isset($_SERVER["REQUEST_URI"][0]) ||
	   $_SERVER["REQUEST_URI"][0]!=='/'){return;}
	dlib_message_add(ent(t('You are not currently allowed to access that page. Maybe you can try after logging-in.')).' (403)','error');
	dlib_redirect($base_url.'/login?permission-denied=1&destination='.urlencode(substr($_SERVER["REQUEST_URI"],1)));
}

//! Returns a list of identical filenames in css_override_path.
//! This allows for easy overriding of css files by simply adding a file with the same filename.
//! @see: HtmlPageData::renderCss()
function demosphere_css_overrides_list($path)
{
	global $demosphere_config;
	$res=[];
	$res[]=$path;
	$basename=basename($path);
	foreach($demosphere_config['css_override_path'] as $dir)
	{
		$override=$dir.'/'.$basename;
		if(file_exists($override)){$res[]=$override;}
	}
	return $res;	
}

//! Create a css file merging all overrides.
//! Normally you should use $currentPage->addCss. demosphere_merge_css() is only for special
//! cases where $currentPage->addCss is not usable (example: css dynamically included from js).
function demosphere_merge_css($path)
{
	global $demosphere_config;
	$basename=basename($path);
	$resFilename='files/css/merged/'.$basename;
	$files=demosphere_css_overrides_list($path);

	// first check if we really need to rebuild the file
	$needsRebuild=true;
	if(file_exists($resFilename))
	{
		$needsRebuild=false;
		$last=filemtime($resFilename);
		foreach($files as $file)
		{
			if(filemtime($file)>=$last){$needsRebuild=true;}
		}
	}
	if(!$needsRebuild){return $resFilename;}

	// now actually rebuild the file
	$res='';
	foreach($files as $file)
	{
		$res.="\n/* demosphere_merge_css from: ".$file." */\n\n";
		$res.=file_get_contents($file)."\n\n";
	}
	file_put_contents($resFilename,$res);
	return $resFilename;
}


//! Returns HTML for a <script> tag created with $currentPage->addJs(...)
//! This is a hook called from HtmlPageData::renderJs 
//! We use it to implement dynamic script loading, choosing which JS to load for either recent or old browsers.
function demosphere_js_script_render($src,$desc)
{
	global $base_url,$dlib_config,$demosphere_config;
    static $isFirst=true;

	$res='';

	$base_path=preg_replace('@^https?://[^/]*@','',$base_url).'/';

	// *** Cases where we don't want/need compat
	$path=substr($src,strlen($base_path));
	if($desc['location']!=='file' || strpos($path,'lib/')===0 || ($desc['options']['use-compat'] ?? true) === false)
	{
		$res.='<script src="' .ent($src).'"></script>'."\n";

		// If this is jQuery, we need to setup extra delay for $(document).ready(...). 
		// $(document).ready(...) needs to wait for JS loaded with add_script(), which might finish loading after DOMContentLoaded.
		if($desc['location']==='file' && $desc['js']==='lib/jquery.js')
		{
			$res.='<script>
                       (function(){
                       if(typeof add_script!=="function")return;
                       for(var i=0;i<add_script.scripts.length;i++)
                       {
                           if(add_script.scripts[i].is_loading){$.holdReady(true);}
                       }
                       })();
                   </script>'."\n";
		}

		$res=demosphere_js_shorten($res);
		return $res;
	}

	// In some dev environments, display error if js-compat is out of date: need to re-run Babel.
	if($dlib_config['debug'] &&
	   ($demosphere_config['debug_rule']!=='localhost-test' || strpos($_SERVER['HTTP_USER_AGENT'],'Trident')!==false))
	{
		if(filemtime($desc['js']) > filemtime('js-compat/'.$desc['js'])){fatal('js-compat is out of date. Please run babel.');}
	}

	//*** First time around, we need to add feature detection and define the js_compat() function.
	if($isFirst)
	{
		// JS that determines if it is an old browser. 
		// Browsers are considered old if they do not support async/await
		// Chrome<55 IE(all) Firefox<52 Opera<42 Safari<10.1 
		// Stats  7/2019 :  < 6% of browsers in France: mainly IE11, old Chrome and Android browser
		$res.='<script>
                   var supportsAsync=true;
                   try {new Function("async function a(){}")();}
                   catch(e) {supportsAsync=false;}
                   var isOldBrowser=!supportsAsync;
                   if(isOldBrowser){add_script("'.$base_path.'lib/old-browsers.js",false);}
               </script>'."\n";

		// Add "js-compat/" to file paths. "path" is relative to base root.
		$res.='<script>
	               function js_compat(path)
	               {
                        if(!isOldBrowser || /t[34]\.php$/.test(path)){add_script("'.$base_path.'"+path,false);}
                        else
                        {
	                         add_script("'.$base_path.'js-compat/"+path,false);
                        }
	               }
               </script>'."\n";
		$isFirst=false;
		$res=demosphere_js_shorten($res);
	}

	// ***  Add <script> tag.
	$loadFunction=false;
	if(isset($desc['options'])){$loadFunction=$desc['options']['loadFunction'] ?? false;}
	$path=substr($src,strlen($base_path));
	$res.='<script>'.
		($loadFunction!==false ? 'function '.$loadFunction.'(){' : '').
		'js_compat("' .ent($path).'")'.
		($loadFunction!==false ? '}' : '').
		'</script>'."\n";

	return $res;
}

//! "Minify" JS by stripping comments and stray white-space.
//! Warning: hackish, very very basic, no guarantees. Can break code in not-so-unusual cases. Only use it for short, simple code.
function demosphere_js_shorten($js)
{
	$js=preg_replace('@^\s*(//.*$)?@m','',$js);
	$js=preg_replace("@/\*.*\*/@U",'$1',$js);
	$js=preg_replace("@\n\n+@","\n",$js);
	$js=preg_replace("@\s*}\s*@",'}',$js);
	$js=preg_replace("@\s*{\s*@",'{',$js);
	$js=preg_replace("@;(}|$)@",'$1',$js);
	$js=preg_replace("@;\s*\n@",";",$js);
	return $js;
}

//! Replace a built-in image by another one in a user directory
//! This allows for easy overriding of images by simply adding an image with the same filename.
function demosphere_image_override($src)
{
	global $demosphere_config;
	$base=basename($src);
	foreach($demosphere_config['image_override_path'] as $dir)
	{
		if(file_exists($dir.'/'.$base)){$src=$dir.'/'.$base;}
	}
	return $src;
}

//! Convert abc/example.js to abc/example.cache-1ae5c.js (1ae5c is hashed from file mtime)
//! Note: for ordinary js don't use this. Use addJs with cache-checksum option (see HtmlPageData::renderJs).
//!       This function is used to create paths for dynamically loaded js.
//! Note: this isn't only used for performance, it is needed to avoid stale cached js, when using add_script
function demosphere_cacheable($srcFilename)
{
	$hash=substr(md5(filemtime($srcFilename)),0,5);
	return preg_replace('@(\.[^.]+$)@','.cache-'.$hash.'$1',$srcFilename);
}

//! Clear all cached information
//! Note: clearing CSS caches without clearing page caches would result in stale CSS file references in cached pages.
function demosphere_cache_clear()
{
	$out='';

	// Remove all page cache
	$nbPages=db_result('SELECT COUNT(*) FROM page_cache');
	demosphere_page_cache_clear_all();
	$out.='All '.$nbPages.' pages cleared from database page cache<br/>';

	// delete merged and compressed css files in files/css
	foreach(glob('files/css/{merged,compressed}/*.css',GLOB_BRACE) as $fname)
	{
		unlink($fname);
		$out.='Deleted file: '.ent($fname).'<br/>';
	}

	// Clear css template cache
	require_once 'dlib/css-template.php';
	css_template_cache_clear();

	// Clear mail import caches
	Message::cleanupCachedFiles(true);

	dlib_message_add('Cache cleared ok.');
	return $out;
}

function demosphere_cache_clear_slow_confirm()
{
	$items=[];
	$items['text']=['html'=>t('You are about to recompute the graphics on this site. This will take about a minute. Please do not visit other pages while it is working.'),];
	$items['ok']=['type'=>'submit',
				  'value'=>t('Confirm'),
				  'submit'=>'demosphere_cache_clear_slow',
				  ];
	require_once 'dlib/form.php';
	return form_process($items);
}

function demosphere_cache_clear_slow()
{
	demosphere_cache_clear();
	css_template_cache_clear(true);

	require_once 'demosphere-topics.php';
	demosphere_topics_create_color_images();

	require_once 'dlib/sprite.php';
	sprite_cache_clear();
}


//! Menubar with frequently used links for moderators.
//! $specialPath: "awstats" or false. Used by ajax menu fetch inside awstats 
function demosphere_event_admin_menubar($specialPath=false)
{
	global $user,$base_url,$demosphere_config,$currentPage;

	$out='';

	$out.= '<nav id="eventAdminMenubar">';

	$out.='<span id="menubar-right-buttons">';
	require_once 'demosphere-help.php';
	$buttons=demosphere_help_buttons();
	$out.=val($buttons,'config','');
	$out.=val($buttons,'help'  ,'');
	$out.='</span>';

	$out.='<ul>';
	foreach($demosphere_config['admin_menu'] as $name=>$menuItem)
	{
		$path=$menuItem['path'];
		if($path===false){$path='/';}

		$currentPath=$_SERVER['REQUEST_URI'];
		if($specialPath==='awstats'){$currentPath='/cgi-bin/awstats.pl';}
		$isActive=preg_match('@^'.preg_quote($path,'@').'([?&/]|$)@',$currentPath);

		$out.='<li class="'.($isActive ? "active" : "inactive").'">';
		if(!preg_match('@^(https?:)?//@',$path)){$path=$base_url.$path;}
		$out.='<a id="amenu-'.ent($name).'" href="'.ent($path).'">'.ent($menuItem['title']).'</a>';
		$out.='</li>';
	}
	$out.='</ul>';
	
	$out.='</nav>';
	if($demosphere_config['is_backup_site'])
	{
		$out.= '<div id="backupSiteWarning">'.
			t('The main site is down. You are on the backup site. Any changes will be lost.').
			'</div>';
	}
	return $out;
}

//! Adds JS that will add a class to <body> to show support (or not) for a CSS property+value.
//! This will eventually be replaced by "CSS @supports" 
//! https://developer.mozilla.org/en-US/docs/Web/CSS/@supports
//! Remove this when IE11 and safari9 become rare
function demosphere_css_supports_body_class($cssPropName,$cssPropValue,$bodyClass)
{
	global $currentPage;
	$currentPage->addJs('(function(){'.
						'var d=document.createElement("div");'.
						'd.style["'.$cssPropName.'"]="'.$cssPropValue.'";'.
						'document.body.className+=" '.$bodyClass.'"+(d.style["'.$cssPropName.'"]==="'.$cssPropValue.'" ? "" : "-no")})()',
						'inline-body');
}

// ***************************************
// ***************************************
// ***************************************


function highlight_future_dates_and_keywords($html,$moreInfo=false,$addStyle=true,$addJS=false)
{
	global $demosphere_config;
	require_once 'dlib/find-date-time/find-date-time.php';
	$str=$html;
	// FIXME: avoid problems in regexp pos from find date time
	$str=dlib_html_entities_decode_text($str);
	// strange, some entities left after first decode ?????! (check why)
	// two decodes fix it
	$str=dlib_html_entities_decode_text($str);
	
	// add syle for dates/times and keywords
	if($addStyle)
	{
		global $feed_import_is_test;
		if(!(isset($feed_import_is_test) && $feed_import_is_test))
		{
			require_once 'dlib/css-template.php';
			$str='<style type="text/css">'.
				file_get_contents(css_template('demosphere/css/demosphere-highlight.tpl.css')).
				'</style>'.$str;
		}
	}
	if($addJS)
	{
		//FIXME: this shouldn't be here and probably it all needs a cleanup.
		// Maybe use more inline styles and JS.
		require_once 'dlib/css-template.php';
		global $base_url;
		$str='<link type="text/css" rel="stylesheet" media="all" href="'.$base_url.'/'.css_template('demosphere/css/demosphere-highlight.tpl.css').'" />'.$str;
		$str='<script type="text/javascript" src="'.$base_url.'/demosphere/js/demosphere-event-near-date.js"></script>'.
			'<script type="text/javascript">'.
			'window.addEventListener("load", add_popup_event_to_highlighted_dates, true);'.
			'</script>'.
			$str;
	}

	$dateInfo=[];
	// highlight dates and times
	$times=find_dates_and_times($str,false,true);
	$pos=0;
	$res='';
	$ignore=time()+$demosphere_config['future_date_offset'];
	
	$startTag='<em class="demosphere_timestamp" title="';
	$endTag='</em>';
	$startMark="XYDZR#0_P";
	$endMark="XZAZEYR#0_EPX";
	foreach($times as $time)
	{
		$ts  =$time[0];
		$text=$time[1];
		$tpos=$time[2];

		$isTodayWithNoTime=(strftime("%d/%m/%Y %R"   ,$ts)==
							strftime("%d/%m/%Y 03:33",time()));

		if($isTodayWithNoTime && !$demosphere_config['future_date_today_with_no_time'])
		{continue;}
		if(!$isTodayWithNoTime && $ts<$ignore){continue;}

		$dateInfo[]=$ts;
		require_once 'demosphere-date-time.php';
		$res.=substr($str,$pos,$tpos-$pos).$startMark."".
			demos_format_date('demosphere-timestamp',$ts).
			"|".$text."".$endMark;
		$pos=$tpos+strlen($text);
	}
	$res.=substr($str,$pos);
	// undo replacement if this is inside an html tag
	do
	{
		$r0=$res;
		$res=preg_replace("@(<[^>]*)$startMark([0-9 :-]+\|(.*))$endMark@U",'$1$3',$res);
	}while($res!==$r0);
	// very special case: highligted dates might contain html tags => detect these cases and split highlight
	$res=preg_replace_callback("@($startMark"."[0-9 :-]+\|)(.*)($endMark)@Us",
							   function($m)
							   {
								   $res  =$m[0];
								   if(strpos($m[2],'<')===false){return $res;}
								   $start=$m[1];
								   $end  =$m[3];
								   $res=str_replace('<',$end.'<',$res);
								   $res=str_replace('>','>'.$start,$res);
								   return $res;
							   },
							   $res);
	// replace marks with tags
	$res=preg_replace('@'.$startMark.'([0-9/: -]+)\|@',$startTag.'$1">',$res);
	$res=str_replace($endMark  ,$endTag  ,$res);

	// highlight keywords
	$startTag='<em class="demosphere_keyword">';
	$endTag='</em>';
	$keywords=$demosphere_config['highlight_keywords'];
	$hasMatches=false;
	$startMark="XYR#0_P";
	$endMark="XZYR#0_EPX";
	$keywordMatches=[];
	foreach($keywords as $keyword)
	{
		$kw=explode('|keyword_class|',$keyword);
		$regexp=$kw[0];
		// check if this is a valid regexp (often problems in config!)
		if(preg_match('@^[a-z0-9]@',$regexp))
		{fatal("invalid regexp in keyword higlight, please check your config");}
		$class=val($kw,1,false);
		// first find matches for info if requested
		if($moreInfo)
		{
			preg_match_all($regexp,$res,$matches);
			$keywordMatches=array_merge($keywordMatches,$matches[0]);
		}
		// normal replace
		$res=preg_replace($regexp,$startMark.'$1'.$endMark,$res);
		// undo replacement if this is inside an html tag
		do
		{
			$r0=$res;
			$res=preg_replace("@(<[^>]*)$startMark(.*)$endMark@U",'$1$2',$res);
		}while($res!==$r0);
		// replace marks with tags
		$startTag1=str_replace('"demosphere_keyword"','"demosphere_keyword '.$class.'"',$startTag);
		$res=str_replace($startMark,$startTag1,$res);
		$res=str_replace($endMark  ,$endTag  ,$res);
	}
	return $moreInfo ? 
		['content'=>$res,
		 'timestamps'=>$dateInfo,
		 'keywordMatches'=>$keywordMatches] :
		$res;
}

// ***************************************
// Url tracking clean up
// ***************************************

//! Applies demosphere_url_remove_tracking() on all links, images and link texts of an html fragment or page.
function demosphere_url_remove_tracking_html(string $html)
{
	require_once 'dlib/html-tools.php';
	$doc=dlib_dom_from_html($html,$xpath);
	// Remove all sorts of tracking and proxied links
	foreach($xpath->query("//a/@href|//img/@src") as $attr)
	{
		$attr->value=ent(demosphere_url_remove_tracking($attr->value));
	}

	$httpUrl='\bhttps?://[^[:space:]<>"\']*[^[:space:]<>"\'.…]';
	foreach($xpath->query("//a/text()") as $textNode)
	{
		$textNode->nodeValue=preg_replace_callback('@('.$httpUrl.')@u',
												   function($m){return demosphere_url_remove_tracking($m[0]);},
												   $textNode->nodeValue);
	}
	return dlib_dom_to_html($doc);
}


//! Removes tracking data from an URL and also fixes some well known proxied links (Google, Facebook...).
function demosphere_url_remove_tracking(string $url)
{
	// dlib php-eval 'foreach(db_one_col("SELECT content FROM Article WHERE content!='"'"''"'"' ORDER BY id DESC LIMIT 1000  ") as $c){preg_match_all('"'"'@https?://[^[:space:]<>"]*@'"'"',$c,$m);foreach($m[0] as $u){echo demosphere_url_remove_tracking(dlib_html_entities_decode_all($u))."\n";}}'  | cut -d ':' -f 2-  | grep -v share | grep -v twitter.com/intent/ | grep -v plugins/like.php | grep http

	$parsed=parse_url($url);
	if($parsed===false || !isset($parsed['host'])){return $url;}

	$host=$parsed['host'];
	$query=[];
	if(isset($parsed["query"])){parse_str($parsed["query"],$query);}
	$path=$parsed['path'] ?? false;

	// fix proxied links (Facebook & Google) : http://l.facebook.com/l.php?u=http%3A%2F%2Fexample.org...
	if(preg_match('@\bfacebook\.[a-z]+$@',$host) && $path==='/l.php'          && preg_match('@^https?://@',$query["u"  ] ?? ''))
	{
		$url=$query["u"];
	}
	if(preg_match('@\bfbcdn\.net$@'      ,$host) && $path==='/safe_image.php' && preg_match('@^https?://@',$query["url"] ?? ''))
	{
		$url=$query["url"];
	}
	if(preg_match('@\bgoogle\.[a-z]+$@'  ,$host) && $path==='/url'            && preg_match('@^https?://@',$query["q"  ] ?? ''))
	{
		$url=$query["q"];
	}

	// Simplify Facebook links for events, posts, photos, videos, ... also removes __xts__ tracking 
	if(preg_match('@^https?://[^/]*\.facebook\.[a-z]+/@',$url))
	{
		$url=demosphere_url_clean_facebook($url);
	}

	// *** Remove url params

	$urlParts=explode('?',$url);
	if(count($urlParts)!==2){return $url;}

	$params=explode('&',$urlParts[1]);
	// https://en.wikipedia.org/wiki/UTM_parameters
	$params=preg_grep('@^(utm_source|utm_medium|utm_campaign|utm_term|utm_content|recruiter)=@',$params,PREG_GREP_INVERT);
	// fbclid: Facebook click tracking (added to non facebook links)
	$params=preg_grep('@^(fbclid)=@',$params,PREG_GREP_INVERT);

	if(count($params)===0){return $urlParts[0];}
	return $urlParts[0].'?'.implode('&',$params);
}

//! Remove spurious (tracking?) data at the end of a Facebook URL.
function demosphere_url_clean_facebook(string $url)
{
	$url=preg_replace('@^https://m\.facebook\.com@','https://www.facebook.com',$url);

	// known urls
	if(preg_match('@^https://[^/]*facebook\.[a-z]+/([^/?&#]*/)?events/[0-9]{4,}/permalink/[0-9]{4,}@'             ,$url,$m)){return $m[0];}
	if(preg_match('@^https://[^/]*facebook\.[a-z]+/([^/?&#]*/)?events/[0-9]{4,}@'                                 ,$url,$m)){return $m[0];}
	if(preg_match('@^https://[^/]*facebook\.[a-z]+/([^/?&#]*/)?posts/[0-9]{4,}@'                                  ,$url,$m)){return $m[0];}
	if(preg_match('@^https://[^/]*facebook\.[a-z]+/([^/?&#]*/)?permalink.php\?story_fbid=[0-9]{4,}&id=[0-9]{4,}@' ,$url,$m)){return $m[0];}
	if(preg_match('@^https://[^/]*facebook\.[a-z]+/([^/?&#]*/)?photos/[a-z]{1,4}[.][0-9.]{4,}/[0-9]{4,}@'         ,$url,$m)){return $m[0];}
	if(preg_match('@^https://[^/]*facebook\.[a-z]+/([^/?&#]*/)?videos/[0-9]{4,}@'                                 ,$url,$m)){return $m[0];}
	if(preg_match('@^https://[^/]*facebook\.[a-z]+/([^/?&#]*/)?profile.php\?id=[0-9]{4,}@'                        ,$url,$m)){return $m[0];}

	// other, unknown urls: remove __xts__,  __tn__ and other args
	if(preg_match('@^https://[^/]*facebook\.[a-z]+/.*[?&]__(xts|tn)__@',$url))
	{
		$res=$url;
		// FIXME: we do not know what these are for. Some are for tracking, but maybe not all...
		$res=preg_replace('@([?&])__xts__[^&]*=[^&]*@','$1',$res);
		$res=preg_replace('@([?&])__tn__=[^&]*@','$1',$res);
 		$res=preg_replace('@([?&])eid=[^&]*@','$1',$res); 
 		$res=preg_replace('@([?&])hc_ref=[^&]*@','$1',$res);
 		$res=preg_replace('@([?&])fref=[^&]*@','$1',$res);

		$res=preg_replace('@([&?])&+@','$1',$res);
		$res=preg_replace('@\?$@','',$res);
		$res=preg_replace('@/$@','',$res);
		return $res;
	}
	return $url;
}

