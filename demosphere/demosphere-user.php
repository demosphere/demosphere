<?php

//! Displays both the login and registration forms on one page
function demosphere_user_login_form()
{
	global $base_url,$currentPage,$demosphere_config,$user,$custom_config;

	if($user->id!=0){return t('You are alread logged in.');}

	// demosphere_permission_denied_403() redirects to login page. Is it a good idea to send a 403 status ? Seems so, but not sure.
	if(isset($_GET['permission-denied']))
	{
		header('HTTP/1.1 403 Permission denied');
		$currentPage->bodyClasses[]='login-only';
	}

	$currentPage->addCssTpl('demosphere/css/demosphere-user.tpl.css');
	if(isset($_GET['register-only'])){$currentPage->bodyClasses[]='register-only';}
	$currentPage->title=t('Login');

	$loginItems=[];
	$loginItems['login']=
		['type'=>'textfield',
		 'title'=>t('Name'),
		 'prefix'=>'<div id="login-section" class="section"><h2>'.t('Login').'</h2>',
		 'attributes'=>['autofocus'=>!isset($_POST['password'])],
		];
	$loginItems['password']=
		['type'=>'password',
		 'title'=>t('Password'),
		 'description'=>'<a href="'.$base_url.'/user/forgot-password">'.t('Forgot password?').'</a>',
		];

	$loginItems['submit' ]=
		['type'=>'submit',
		 'value'=>t('login'),
		 'suffix'=>'</div>',
		 'validate'=>
		 function(&$items)
			{
				$uid=db_result_check("SELECT id FROM User WHERE login='%s'",$items['login']['value']);
				if($uid!==false)
				{
					$cuser=User::fetch($uid);

					if($cuser->checkPassword($items['password']['value']))
					{
						// Mods or admins that did not verify their address are suspended. (Other users are deleted in cron)
						if($cuser->email==='' && ($cuser->data['email-verify']['ts']<time()-User::EMAIL_VERIFY_DELAY ||
												  $cuser->data['email-verify']['is-reminder']))
						{
							dlib_message_add(t('Your account does not have a verified email address.'),'error');
							if(!$cuser->data['email-verify']['is-reminder'])
							{
								demosphere_user_email_verify_send($cuser,$cuser->data['email-verify']['email'],true);
							}
							return false;
						}

						// temporary code: on login, migrate rehashed passwords to new passwords
						if(strpos($cuser->password,'rehash$')===0)
						{
							$cuser->setHashedPassword($items['password']['value']);
							$cuser->save();
						}
						return true;
					}
				}
				dlib_message_add(t('invalid name or password'),'error');
				return false;
			},
		 'submit'=>function($items)
			{
				global $base_url;
				$uid=db_result_check("SELECT id FROM User WHERE login='%s'",$items['login']['value']);
				$newUser=User::fetch($uid);
				demosphere_user_login($newUser);
				// redirect
				if(isset($_GET['destination'])){dlib_redirect($base_url.'/'.$_GET['destination']);}
				if($newUser->checkRoles('admin','moderator')){dlib_redirect($base_url.'/control-panel');}
				dlib_redirect($base_url.'/user/'.$newUser->id);
			}
		];

	//$loginItems['spamcheck']=
	// 	['type'=>'spamcheck',
	// 	 'spamcheck-delay'=>0];


	$registerItems=[];
	$registerItems['register-login']=
		['type'=>'textfield',
		 'title'=>t('Name'),
		 'prefix'=>'<div id="register" class="section">'.
		 '<h2>'.t('New to @site_name? Sign up',['@site_name'=>$demosphere_config['site_name']]).'</h2>',
		];

	// Invisible field, to avoid autocomplete : browsers force login/password autocomplete, even if you tell them not to.
	$registerItems['bogus-register-email']=
		['type'=>'textfield',
		 'title'=>t('Email'),
		];

	// Invisible field, to avoid autocomplete
	$registerItems['bogus-register-password']=
		['type'=>'password',
		 'title'=>t('Password'),
		];

	$registerItems['register-email']=
		['type'=>'email',
		 'check-dns'=>true,
		 'title'=>t('Email'),
		];

	$registerItems['register-password']=
		['type'=>'password',
		 'title'=>t('Password'),
		 'attributes'=>['autocomplete'=>'new-password'],
		 'validate'=>function($v){if(mb_strlen($v)<6){return t('Password is too short');}}
		];

	$registerItems['register']=
		['type'=>'submit',
		 'value'=>t('Register'),
		 'suffix'=>'</div>',
		 'validate'=>
		 function(&$items)
			{
				$ok=true;
				$reqErr=t('This field is required');
				if($items['register-login'   ]['value']==''){$items['register-login'   ]['error']=$reqErr;$ok=false;}
				if($items['register-email'   ]['value']==''){$items['register-email'   ]['error']=$reqErr;$ok=false;}
				if($items['register-password']['value']==''){$items['register-password']['error']=$reqErr;$ok=false;}

				$uid=db_result_check("SELECT id FROM User WHERE login='%s'",$items['register-login']['value']);
				if($uid!==false){$items['register-login']['error']=t('This name is already used');$ok=false;}

				$uid=User::searchByEmail($items['register-email']['value']);
				if($uid!==false){$items['register-email']['error']=t('This email is already used');$ok=false;}

				return $ok;
			},
		 'submit'=>function($items)
			{
				global $base_url,$demosphere_config;
				$values=dlib_array_column($items,'value');

				$email=$values['register-email'];

				// ** create user
				$newUser=new User();
				$newUser->login=$values['register-login'];
				$newUser->email='';
				$newUser->data['email-verify']=['email'=>$email];// avoid race condition... actually managed later
				$newUser->setHashedPassword($values['register-password']);
				$newUser->save();

				// ** login him in
				demosphere_user_login($newUser);

				demosphere_user_email_verify_send($newUser,$email);

				// ** send notification to admins
				$subject=t('Account created on !site_name',['!site_name'=>$demosphere_config['site_name']]);
				$message=t('A user has just registered.')."\n".
				t("Login:").' '.$newUser->login."\n".
				t("Email:").' '.$email."\n".
				t('You can edit his/her account settings and permissions here:')."\n".
				$base_url.'/user/'.$newUser->id."/edit\n\n";
				require_once 'demosphere-emails.php';
				demosphere_emails_notification_send('register',$subject,false,$message);

				dlib_redirect($base_url.'/user/'.$newUser->id);
			}
		];

	$registerItems['register-spamcheck']=
		['type'=>'spamcheck',];

	//if(isset($custom_config['login_form'])){$custom_config['login_form']($registerItems);}

	require_once 'dlib/form.php';
	$out='';
	$out.='<div id="login-and-register">';
	$out.=form_process($loginItems   ,['id'=>'login-form'   ]);
	$out.=form_process($registerItems,['id'=>'register-form','attributes'=>['autocomplete'=>'new-password']]);
	$out.='</div>';
	return $out;
}

//! Send an email with a link that allows $cuser to prove that his email address is valid.
function demosphere_user_email_verify_send($cuser,$email,$isReminder=false)
{
	global $demosphere_config,$base_url;
	$cuser->data['email-verify']=['email'=>$email,
								  'token'=>dlib_random_string(20),
								  'ts'=>time(),
								  'is-reminder'=>$isReminder,
								 ];
	$cuser->save();

	// ** send him an email 
	require_once 'lib/class.phpmailer.php';
	$mail=new PHPMailer();
	$mail->CharSet="utf-8";
	$mail->SetFrom($demosphere_config['contact_email']);
	$mail->AddAddress($email);
	$mail->Subject=t('Verify your email for !site_name',['!site_name'=>$demosphere_config['site_name']]);
	$mail->Body=t('You have selected an email address for your account on !site_name',['!site_name'=>$demosphere_config['site_name']])."\n\n".
		t('Please follow this link to validate this email address:')."\n".
		$base_url.'/user/'.$cuser->id.'/email-verify?token='.$cuser->data['email-verify']['token']."\n".
		t('If you do not validate, your account will be automatically deleted after @h hours.',['@h'=>round(User::EMAIL_VERIFY_DELAY/3600)])."\n\n".
		t('If you notice that somebody is misusing this service, please contact: !contact',['!contact'=>$demosphere_config['contact_email']]);
	if(!$mail->Send())
	{
		dlib_message_add(t("Error while sending email to @to.",['@to'=>$email]));
	}
	dlib_message_add(t("An email has been sent to @email. Please click on the link in that email to validate your account.",
					   ['@email'=>$email])."<br/>".
					 t('If you do not validate, your account will be automatically deleted after @h hours.',['@h'=>round(User::EMAIL_VERIFY_DELAY/3600)]));

}

//! Called when a user clicks on the email verify link in an email he received.
//! This validates his email address and also logs him in (if needed)
function demosphere_user_email_verify($id)
{
	global $user,$demosphere_config,$base_url;
	$cuser=User::fetch($id,false);
	if($cuser===null){dlib_permission_denied_403();}

	if(!isset($cuser->data['email-verify']) || 
	          $cuser->data['email-verify']['token']!==val($_GET,'token'))
	{
		dlib_message_add(t('Invalid token'),'error');
		return t('Email verification failed.');
	}

	// special case: old link, but user not deleted by cron (this can happen for mods or admins)
	if($cuser->data['email-verify']['ts'] < time()-User::EMAIL_VERIFY_DELAY)
	{
		dlib_message_add(t('The email verification is only valid for @h hours. A new mail will been sent to you.',
						   ['@h'=>round(User::EMAIL_VERIFY_DELAY/3600)]),
						 'error');
		demosphere_user_email_verify_send($cuser,$cuser->data['email-verify']['email']);
		return '';
	}

	$cuser->email=$cuser->data['email-verify']['email'];
	unset($cuser->data['email-verify']);
	$cuser->save();
	
	require_once 'lib/class.phpmailer.php';
	$mail=new PHPMailer();
	$mail->CharSet="utf-8";
	$mail->SetFrom($demosphere_config['contact_email']);
	$mail->AddAddress($cuser->email);
	$mail->Subject=t('Account created on !site_name',['!site_name'=>$demosphere_config['site_name']]);

	$mail->Body=t('Your account was successfuly created on !site_name',['!site_name'=>$demosphere_config['site_name']])."\n\n".
		t('You can edit your account settings here:')."\n".
		$base_url.'/user/'.$cuser->id."/edit\n\n".
		t('If you lose your password or have trouble accessing your account:')."\n".
		$base_url.'/user/forgot-password'."\n\n";
	if(!$mail->Send())
	{
		dlib_message_add(t("Error while sending email to @to.",['@to'=>$email]),'error');
	} 

	if($user->id!=$cuser->id)
	{
		demosphere_user_login($cuser);
	}

	dlib_message_add(t("Thank you! Your email was succesfully verified."));
	$isRandomName=preg_match('@^euser-[0-9]{5}$@',$cuser->login);
	if($isRandomName)
	{
		dlib_message_add(t('You might want to set your username and password so that in the future you can change your subscription options.'));
		dlib_redirect($base_url.'/user/'.$cuser->id.'/edit');
	}
	else
	{
		dlib_redirect($base_url.'/user/'.$cuser->id);
	}
}

//! Tiny form where user can enter his address to receive an email with a login link
function demosphere_user_forgot_password()
{
	global $base_url,$currentPage,$demosphere_config,$user;
	if($user->id!=0){dlib_redirect($base_url.'/user/'.$user->id.'/edit');}

	$currentPage->title=t('Forgotten password');

	$items=[];
	$items['top']=['html'=>'<p id="forgot-top">'.t('Enter the email address of your account. An email containing a special link will be sent to that address. That link will allow you to access your account and change your password. Please <a href="!contact">contact us</a> if you need help.',['!contact'=>ent(Post::builtInUrl('contact'))]).'</p>'];

	$items['email']=
		['type'=>'email',
		 'title'=>t('Your email address'),
		 'required'=>true,
		 'validate'=>
		 function($v)
			{
				if(User::searchByEmail($v)===false){return t('Unknown email address');}
			}
		];
	$items['submit']=
		['type'=>'submit',
		 'value'=>t('Send me the email'),
		 'suffix'=>'</div>',
		 'submit'=>function($items)
			{
				$uid=User::searchByEmail($items['email']['value']);
				demosphere_user_forgot_password_email($uid);
			}];

	require_once 'dlib/form.php';
	return form_process($items);
}

function demosphere_user_forgot_password_email($cuserId)
{
	global $demosphere_config,$base_url;
	$cuser=User::fetch($cuserId);
	if(isset($cuser->data['forgot-password']['sent']) &&
	   $cuser->data['forgot-password']['sent']>time()-15*60)
	{
		dlib_message_add(t('An email has already been sent to you less than 15 minutes ago. Please check your mailbox.'),'error');
		return;
	}

	$email=$cuser->email;
	if($email===''){$email=$cuser->data['email-verify']['email'];}

	$token=dlib_random_string(20);
	$cuser->data['forgot-password']=
		['sent'=>time(),
		 'token'=>$token,
		 'email'=>$email];
	$cuser->save();


	require_once 'lib/class.phpmailer.php';
	$mail=new PHPMailer();
	$mail->CharSet="utf-8";
	$mail->SetFrom($demosphere_config['contact_email']);
	$mail->AddAddress($email);
	$mail->Subject=t('Forgotten password on !site_name',['!site_name'=>$demosphere_config['site_name']]);

	$mail->Body=
		t('You have requested this email on !site_name',['!site_name'=>$demosphere_config['site_name']])."\n\n".
		t('You can access your account by following this link:')."\n".
		$base_url.'/user/'.$cuser->id.'/forgot-password?token='.$token."\n\n".
		t('You will be redirected to your profile where you can change your password.')."\n\n".
		t('This link will only work once and is only valid for 48 hours.').' '.
		t('After 48 hours, you can receive a new mail, like this one, by following this link:')."\n".
		$base_url.'/user/forgot-password'."\n\n\n".
		t('If you notice that somebody is misusing this service, or if you need any help, please contact: !contact',
		  ['!contact'=>$demosphere_config['contact_email']]);

	if(!$mail->Send())
	{
		dlib_message_add(t('Error while sending email.'),'error');
		unset($cuser->data['forgot-password']);
		$cuser->save();
	}
	else
	{
		dlib_message_add(t('An email has been successfully sent to you. Please check your mail.'));
	}
}

//! Process link received after using form in demosphere_user_forgot_password()
function demosphere_user_forgot_password_login($uid)
{
	global $base_url;
	$cuser=User::fetch($uid,false);
	if($cuser===null){dlib_permission_denied_403();}
	if(!isset($cuser->data['forgot-password']['sent']) ||
	   !isset($cuser->data['forgot-password']['token']) ||
	   $cuser->data['forgot-password']['token']!==$_GET['token'] ||
	   $cuser->data['forgot-password']['sent']<time()-User::EMAIL_VERIFY_DELAY )
	{
		dlib_permission_denied_403();
	}

	unset($cuser->data['forgot-password']);
	// special case: this also verifies email address 
	if(isset($cuser->data['email-verify']) && 
	   $cuser->data['email-verify']['email']===$cuser->data['forgot-password']['email'])// important check for security
	{
		$cuser->email=$cuser->data['email-verify']['email'];
		unset($cuser->data['email-verify']);
	}
	$cuser->save();

	demosphere_user_login($cuser);
	dlib_redirect($base_url.'/user/'.$cuser->id.'/edit');
}

//! This is the function that actually logs-in a user. 
//! It changes the global $user object and the $_SESSION['uid'].
function demosphere_user_login($newUser)
{
	global $user;
	$user=$newUser;

	// Empty the old sessions variable (exceptions: mobile)
	$_SESSION=array_intersect_key($_SESSION,array_flip(['mobile']));

	// session_regenerate_id will create a new session db entry. 
	// The "true" param tells it to delete the old session db entry.
	// The contents of $_SESSION is not changed by session_regenerate_id.
	// We don't need to delete cookie on browser, as a this sets a new value with for the existing cookie ('DEMSESSID').
	session_regenerate_id(true);

	// Set the user for the new session
	$_SESSION['uid']=$user->id;

	// change user last login 
	$user->lastLogin=time();
	$user->save();
}


//! Display users settings and account information
function demosphere_user_view($id)
{
	global $base_url,$currentPage,$demosphere_config,$user;

	require_once 'demosphere/demosphere-common.php';
	demosphere_css_supports_body_class('flex-wrap','wrap','supports-flex-wrap');

	$cuser=User::fetch($id,false);
	if($cuser===null){dlib_not_found_404();}

	if($cuser->email==='' && $cuser->data['email-verify']['ts']<time()-3600*0)
	{
		dlib_message_add(t('Your account does not have a verified email address.'),'warning');
	}

	$currentPage->bodyClasses[]='unPaddedContent';

	$currentPage->title=t('User: ').$cuser->login;
	$currentPage->addCssTpl('demosphere/css/demosphere-user.tpl.css');
	$currentPage->addCssTpl('demosphere/css/demosphere-big-button.tpl.css');
	$currentPage->addJs('lib/jquery.js');
	$currentPage->addJs('demosphere/js/demosphere-user-view.js');
	demosphere_user_tabs($cuser,'view');
	require_once 'demosphere-date-time.php';

	// *** Email subscriptions
	
	$emailSubscription=val(val($cuser->data,'demosphere_email_subscription',[]),'active');
	$emailSubsLastSent=val(val($cuser->data,'demosphere_email_subscription',[]),'last-sent-time-actual');

	$carpoolsIds=db_one_col('SELECT Carpool.id FROM Carpool,Event WHERE '.
							'Carpool.eventId=Event.id AND Carpool.userId=%d AND Event.startTime>%d',
							$id,time()-3600*24*5);

	// *** My Events

	require_once 'demosphere-self-edit.php';
	// Future events authored by user, contact in public form, or that user can self edit (either directly, or through repetition group) 
	// Broad, approx conditions in sql. Actual checks are done in PHP
	$myEvents=Event::fetchList('WHERE ('.
					 '    authorId=%d  OR '.
					 "    extraData LIKE '%%public-form%%%s%%' OR ".
					 "    EXISTS (SELECT * FROM self_edit WHERE self_edit.event=Event.id                                    AND email='%s')  OR ".
					 "    EXISTS (SELECT * FROM self_edit WHERE repetition_group=repetitionGroupId AND repetitionGroupId!=0 AND email='%s') ".
					 ')  AND '.
					 'startTime>%d ORDER BY startTime ASC, id ASC',
					 $id,$cuser->email,$cuser->email,$cuser->email,Event::today());
	$myEvents=array_filter($myEvents,function($event)use($cuser)
						   {
							   if($event->getModerationStatus()==='trash'){return false;}
							   if($event->authorId==$cuser->id ||
								  ($cuser->email!=='' && 
								   val(val($event->extraData,'public-form',[]),'contact')===$cuser->email))
							   {
								   return true;
							   }
							   return $event->canSelfEdit($cuser);
						   });

	$myRepetitions=RepetitionGroup::fetchList("WHERE organizerEmail='%s' AND ruleType!=0",$cuser->email);

	$carpools=Carpool::fetchListFromIds($carpoolsIds);

	// *** My Comments

	global $comments_config;
	$myComments=Comment::fetchList('WHERE userId=%d ORDER BY created DESC',$id);
	$myCommentsLinks=[];
	foreach($myComments as $commentId=>$comment)
	{
		$canView=$comment->canView();
		$link=
			[
				'url'  =>$canView ? $comments_config['link']($comment) : false,
				'title'=>demos_format_date('relative-short-full',$comment->created).($canView ? ': '.$comment->blurb() : '')
			];
		$myCommentsLinks[$commentId]=$link;
	}

	// *** Opinions

	$opinions=false;
	$opEvents=false;
	if($demosphere_config['enable_opinions'] && 
	   ($cuser->contributorLevel>0 || $cuser->checkRoles('admin','moderator')))
	{
		$opinions=Opinion::fetchList('WHERE userId=%d ORDER BY time DESC LIMIT 10',$cuser->id);
		// Events where user needs to give opinion .
		// Broad, approx conditions in sql. Actual checks are done in PHP
		$opEvents=Event::fetchList('WHERE '.
								   'startTime>%d AND '.
								   'NOT EXISTS (SELECT * FROM Opinion WHERE Opinion.eventId=Event.id AND Opinion.userId=%d) AND '.
								   "((moderationStatus=%d  AND needsAttention IN (%d,%d)  )  OR ".
								   " (status=1  AND needsAttention=%d)) ".
								   "ORDER BY startTime ASC",
								   time(),
								   $cuser->id,
								   Event::moderationStatusEnum('waiting'),
								   Event::needsAttentionEnum('publication-request-waiting'),
								   Event::needsAttentionEnum('publication-request'),
								   Event::needsAttentionEnum('non-event-admin'));
		$opEvents=array_filter($opEvents,function($event)use($cuser)
							   {
								   return $event->canGiveOpinion($cuser);
							   });
	}

	$inChargePlaces=Place::fetchList('WHERE inChargeId=%d',$cuser->id);

	return template_render('templates/demosphere-user-view.tpl.php',
						   [compact('cuser','emailSubscription','emailSubsLastSent','carpools','opinions','opEvents',
									'myEvents','myRepetitions','myComments','myCommentsLinks','inChargePlaces')]);
}

function demosphere_user_edit($id)
{
	global $base_url,$currentPage,$demosphere_config,$user;
	$currentPage->addCssTpl('demosphere/css/demosphere-user.tpl.css');

	$cuser=User::fetch($id,false);
	if($cuser===null){dlib_not_found_404();}
	if($demosphere_config['hosted'] && 
	   $user->id!==1 &&
	   $cuser->id===1 && 
	   $cuser->login='admin'){dlib_permission_denied_403();}

	$currentPage->title=t('Edit: ').$cuser->login;

	demosphere_user_tabs($cuser,'edit');

	require_once 'dlib/form.php';
	list($objItems,$formOpts)=form_dbobject($cuser);
	$items=[];

	// Invisible field, to avoid autocomplete : browsers force login/password autocomplete, even if you tell them not to.
	$items['bogus-login']=['type'=>'textfield',
						   'title'=>t('Email'),
						  ];

	// Invisible field, to avoid autocomplete
	$items['bogus-password']=['type'=>'password',
							  'title'=>t('Password'),
							 ];

	$isRandomName=preg_match('@^euser-[0-9]{5}$@',$cuser->login);
	$isEditable=$isRandomName  || $user->checkRoles('admin');

	$items['login']=$objItems['login'];
	$items['login']['title']=t('Login name');
	$items['login']['required']=true;
	$items['login']['validate']=function($v)use($cuser)
		{
			$uid=db_result_check("SELECT id FROM User WHERE login='%s' AND id!=%d",$v,$cuser->id);
			if($uid!==false){return t('This name is already used');}
		};
	if(!$isEditable)
	{
		$items['login']['attributes']=['disabled'=>true];
		$items['login']['value']=$cuser->login;
	}
	if($isEditable && !$user->checkRoles('admin'))
	{
		$items['login']['description']=t('Note: you can only change your login name once.');
	}
	
	
	$items['password']=$objItems['password'];
	$items['password']['title']=t('Password');
	$items['password']['type']='password';
	$items['password']['attributes']=['autocomplete'=>'new-password'];
	$items['password']['validate']=function($v){if(mb_strlen($v)<6){return t('Password is too short');}};
	$items['password']['description']=t('Leave it empty if you do not want to change it.');
	$items['password']['pre-submit']=function(&$v)use($cuser)
		{
			if($v==''){$v=$cuser->password;}// empty password => don't change
			else
			{
				$cuser->setHashedPassword($v);
				$v=$cuser->password;
			}
		};

	$items['email']=$objItems['email'];
	$items['email']['title']=t('Email address');
	$items['email']['validate']=function($v)use($cuser)
		{
			$uid=User::searchByEmail($v,true,$cuser->id);
			if($uid!==false){return t('This email is already used');}
		};
	// if email verification is pending, pretend this field is full
	if(isset($cuser->data['email-verify']))
	{
		$items['email']['default-value']=$cuser->data['email-verify']['email'];
		$items['email']['field-suffix' ]=' <span>('.ent(t('not yet verified')).')</span>';
	}
	$items['email']['required']=true;
	$items['email']['pre-submit']=function(&$v,&$item)use($cuser,$user)
		{
			// Email address changes require sending a verification email
			$oldEmail=isset($cuser->data['email-verify']) ?  $cuser->data['email-verify']['email'] : $cuser->email;
			if($v!==$oldEmail && !$user->checkRoles('admin')){demosphere_user_email_verify_send($cuser,$v);}

			if(isset($cuser->data['email-verify'])){$v='';}
		};
	
	if($demosphere_config['enable_opinions'] && $user->checkRoles('admin'))
	{
		$items['contributorLevel']=$objItems['contributorLevel'];
		$items['contributorLevel']['title']=t('Contributor Level');
		$items['contributorLevel']['type']='select';
		$items['contributorLevel']['options']=[0=>t('Not contributor'),
											   1=>t('Contributor'),
											   2=>t('Contributor, can see other opinions'),
											   ];
		$items['contributorLevel']['description']=t('Contributors can give their opinion on un-published events that are publication requests.');
	}

	if($user->checkRoles('admin'))
	{
		$items['role']=$objItems['role'];
		$items['role']['title']=t('Role');
		$items['role']['validate']=function($v)use($cuser)
			{
				global $demosphere_config;
				if(!$demosphere_config['hosted']){return;}
				if($v!=User::roleEnum('admin')){return;}
				$nbAdmin=(int)db_result('SELECT COUNT(*) FROM User WHERE role=%d',User::roleEnum('admin'));
				if($cuser->role==User::roleEnum('admin')){$nbAdmin--;}
				if($nbAdmin>3)
				{
					return t('For security, there can be no more than 3 admins on your site. For most use cases, the moderator role should be sufficient. If this is a big problem for you, please contact your server admin.');
				}
			};
		$items['role']['description']='<dl>'.
			'<dt>admin</dt> <dd> '.t('Can do everything, including changing the configuration of the site and managing users. This role is generally only meant for one user in a collective.').'</dd>'.
			'<dt>moderator</dt> <dd> '.t('Can do everything, except changing the configuration of the site or managing users. Can change any event, can use the import tools (feed import, mail import,...). This role is meant for most members of a collective.').'</dd>'.
			'<dt>frontpage event creator</dt> <dd> '.t('Can create events that are directly published on the frontpage. Cannot change other user\'s events. Cannot access the import tools. This role is meant for users that are not part of the collective, but that you trust enough to publish their own events directly.').'</dd>'.
			'<dt>private event creator</dt> <dd> '.t('Can create events that are visible to anyone that knows their url. But those events are not initially published on the frontpage. Cannot change other user\'s events. Cannot access the import tools. This is meant for users that want to manage a "private" sub-calendar.').'</dd>'.
			'<dt>none</dt> <dd> '.t('Plain user, no special rights. For example, a user that subscribes to events by email.').'</dd>'.
			'</dl>';
	}

	$items['save']=$objItems['save'];
	if(isset($items['delete']) && $user->checkRoles('admin'))
	{
		$items['delete']=$objItems['delete'];
	}
	$items['save']['submit'  ]=function()
		{
			require_once 'demosphere-emails.php';
			demosphere_emails_received_update_aliases();
		};
	$items['save']['redirect']='user/'.$cuser->id;

	return form_process($items,$formOpts);
}

//! Setup dbobject-ui config (backend/user...)
function demosphere_user_class_info(&$classInfo)
{
	global $base_url,$currentPage;
	$classInfo['tabs']=function(&$tabs,$cuser)
		{
			global $base_url;
			if($cuser)
			{
				$tabs['view'      ]['text']=t('Tech: view');
				$tabs['edit'      ]['text']=t('Tech: edit');
				$tabs['user-view' ]=['href'=>$base_url.'/user/'.$cuser->id.'?dbobject-ui'     ,'text'=>t('View')];
				$tabs['user-edit' ]=['href'=>$base_url.'/user/'.$cuser->id.'/edit?dbobject-ui','text'=>t('Edit'   )];
			}
			$tabs['add'       ]['text']=t('Create user');
		};

	// ******* edit form setup
	$classInfo['edit_form_alter']=
		function(&$items,&$options,$cuser)
		{
			global $base_url,$demosphere_config,$user,$currentPage;
			$currentPage->addCssTpl('demosphere/css/demosphere-user.tpl.css');

			// On hosted sites, do not allow editing the "admin" user (this is not really enforcable).
			if($demosphere_config['hosted'] && 
			   $user->id!==1 &&
			   $cuser->id===1 && 
			   $cuser->login='admin'){dlib_permission_denied_403();}

			// Invisible field, to avoid autocomplete : browsers force login/password autocomplete, even if you tell them not to.
			// autocomplete="new-password" is ignored :-(
			if($user->id!==$cuser->id)
			{
				$items=dlib_array_insert_assoc($items,'login','bogus-login'   ,['type'=>'textfield','title'=>'Ignore this']);
				$items=dlib_array_insert_assoc($items,'login','bogus-password',['type'=>'password' ,'title'=>'Ignore this']);
			}

			$items['login']['required']=true;
			$items['login']['validate']=function($v)use($cuser)
			{
				$uid=db_result_check("SELECT id FROM User WHERE login='%s' AND id!=%d",$v,$cuser->id);
				if($uid!==false){return t('This name is already used');}
			};

			$items['password']['type']='password';
			$items['password']['pre-submit']=function(&$v)use($cuser)
			{
				if($v==''){$v=$cuser->password;}// empty password => don't change
				else
				{
					$cuser->setHashedPassword($v);
					$v=$cuser->password;
				}
			};
			if(!$cuser->id){$items['password']['required']=true;}
			if($user->id!==$cuser->id)
			{
				// None of the 2 following are working (Firefox 58 2/2018)
				$options['attributes']['autocomplete']='new-password';
				$items['password']['attributes']['autocomplete']='new-password';
			}


			$items['email']['required']=true;
			$items['email']['validate']=function($v)use($cuser)
			{
				$uid=db_result_check("SELECT id FROM User WHERE email='%s' AND id!=%d",$v,$cuser->id);
				if($uid!==false){return t('This email is already used');}
			};


			$items['role']['validate']=function($v)use($cuser)
			{
				global $demosphere_config;
				if(!$demosphere_config['hosted']){return;}
				if($v!=User::roleEnum('admin')){return;}
				$nbAdmin=(int)db_result('SELECT COUNT(*) FROM User WHERE role=%d',User::roleEnum('admin'));
				if($cuser->role==User::roleEnum('admin')){$nbAdmin--;}
				if($nbAdmin>3)
				{
					return 'For security, there can be no more than 3 admins on your site. For most use cases, the moderator role should be sufficient. If this is a big problem for you, please contact your server admin.';
				}
			};

			$items=dlib_array_insert_assoc($items,'created','more-options',
										   ['type'=>'fieldset',
											'title' => t('more options'),
											'collapsible' => true,
											'collapsed' => true,]);
			$items=dlib_array_insert_assoc($items,'save','more-options-end',
										   ['type'=>'fieldset-end',]);
		};

	// ******* list
	$classInfo['list_columns']=
		function(&$columns)
		{
			$columns['subs']=
			['title'=>t('email'),
			 'show'=>function($cuser,&$show)
					{
						$emailSubscription=val(val($cuser->data,'demosphere_email_subscription',[]),'active');
						$show['disp']=$emailSubscription ? t('yes') : t('no');
					}
			 ];
		};

	// Change urls in list so that view & edit links point to user view & edit (not backend view & edit)
	$classInfo['list_action_url']=function($user,$action,$oldActionUrl)
		{
			global $base_url;
			if($action==='view'){return $base_url.'/user/'.$user->id.     '?dbobject-ui';}
			if($action==='edit'){return $base_url.'/user/'.$user->id.'/edit?dbobject-ui';}
			return $oldActionUrl($user,$action);
		};

}

function demosphere_user_tabs($cuser,$active)
{
	global $user,$base_url,$demosphere_config,$currentPage;

	if(!$user->checkRoles('admin') && $cuser->id!=$user->id){return;}

	// If user is viewed in dbobject-ui context, use dbobject-ui tabs instead.
	if(isset($_GET['dbobject-ui']))
	{
		require_once 'dlib/dbobject-ui.php';
		dbobject_ui_tabs('User',$cuser,'user-'.$active);
		return;
	}

	$tabs=[];
	if($user->checkRoles('admin'))
	{
		$tabs['users']=['text'=>t('List'),'href'=>$base_url.'/backend/user'];
	}
	$tabs['view']=['text'=>t('View'),'href'=>$base_url.'/user/'.$cuser->id];
	$tabs['edit']=['text'=>t('Edit'),'href'=>$base_url.'/user/'.$cuser->id.'/edit'];

	$tabs[$active]['active']=true;
	$currentPage->tabs=$tabs;
}


function demosphere_user_logout()
{
	global $user,$demosphere_config,$base_url;

	// Set $user to an empty anonymous user
	$user=new User();
	$user->login='anonymous';

	// Empty the old sessions variable (exceptions: mobile)
	$_SESSION=array_intersect_key($_SESSION,array_flip(['mobile']));

	// session_regenerate_id will create a new session db entry. 
	// The "true" param tells it to delete the old session db entry.
	// The contents of $_SESSION is not changed by session_regenerate_id.
	// We don't need to delete cookie on browser, as a this sets a new value with for the existing cookie ('DEMSESSID').
	session_regenerate_id(true);

	dlib_redirect($base_url);
}

function demosphere_user_autocomplete()
{
	global $demosphere_config;
	$typed=$_GET['term'];
	$typed=trim($typed);
	if($typed===''){return [];}
	$res=db_one_col("SELECT id,login FROM User WHERE ".
					"LOWER(login) LIKE LOWER('%%%s%%') ",$typed);
	return $res;
}

//! Returns of IP addresses of admins and moderators for since $delay seconds
function demosphere_user_admin_ips($delay=3600*24*30)
{
	$adminIpTs=time()-3600*24*30;

	$adminsUids=db_one_col("SELECT id FROM User WHERE role IN (%d,%d)",
						   User::roleEnum('admin'),
						   User::roleEnum('moderator'));
	$lastIps   =db_one_col("SELECT lastIp FROM User WHERE role IN (%d,%d) AND lastAccess>%d",
						   User::roleEnum('admin'),
						   User::roleEnum('moderator'),
						   $adminIpTs);
	$dbLogIps=[];
	if(count($adminsUids))
	{
		$dbLogIps=db_one_col("SELECT ip FROM log WHERE id in (".
							 implode(',',array_map('intval',$adminsUids)).")".
							 " AND timestamp>%d ".
							 "GROUP BY ip",$adminIpTs);
	}
	$dbLogIps=preg_grep('@^([0-9]{1,3}(\.|$)){4}$@',$dbLogIps,PREG_GREP_INVERT);

	return array_unique(array_merge($lastIps,$dbLogIps));
}

function demosphere_user_cron()
{
	global $demosphere_config;

	// Consistency check for empty email
	$bad=db_one_col("SELECT id FROM User WHERE email='' AND data NOT LIKE '%%email-verify%%'");
	if(count($bad)>0)
	{
		require_once 'lib/class.phpmailer.php';
		$mail=new PHPMailer();
		$mail->CharSet="utf-8";
		$mail->SetFrom($demosphere_config['contact_email']);
		$mail->AddAddress('admin@demosphere.net');
		$mail->Subject='bug: user with no email';
		$mail->Body='users id=('.implode(", ",$bad).') have an empty email and no email-verify entry, this should not happen';
		$mail->Send();
	}

	// Delete or logout users with unverified emails after 48h (User::EMAIL_VERIFY_DELAY)
	$allVerif=User::emailVerifyPending();
	foreach($allVerif as $uid=>$verif)
	{
		if(isset($verif['ts']) && $verif['ts']<time()-User::EMAIL_VERIFY_DELAY)
		{
			$cuser=User::fetch($uid);
			if(!$cuser->checkRoles('admin','moderator')){$cuser->delete();}
			else
			{
				// special case : mods & admins: log them out (future login attempts will be blocked until they verify)
				db_query("DELETE FROM dbsessions WHERE uid=%d",$uid);
			}
		}
	}

}

?>