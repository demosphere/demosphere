<?php

//! Create initial buit-in pages. Called from initial demosphere install.
function demosphere_built_in_pages_install()
{
	$pages=['contact','about_us','publishing_guidelines','map_help','user_cal_help','control_panel_data','help_data','spread_the_word','event_sources','help_event_emails','help_feeds','help_ical','help_on_your_website','donate_message','comment_guidelines'];
	foreach($pages as $page)
	{
		$post=new Post($page.'-page',1/* admin uid=1 */,'');
		$post->builtIn=$page;
		$post->save();
		variable_set('Post-'.$post->id,'comments-page-settings','disabled');
	}
	demosphere_built_in_pages_sync_all(true);
	// images in pages are downloaded from cmd line, so they have incorrect owner
	exec('chmod og+w files/import-images/*');
}


// FIXME: call this from cron
//! Sync (update) from reference site all built-in pages that have sync config option.
function demosphere_built_in_pages_sync_all($isInstall=false)
{
	global $demosphere_config,$base_url;
	
	// Do nothing if this is a reference site
	if($demosphere_config['std_base_url']===$demosphere_config['built_in_pages_sync_ref'] ||
	   $demosphere_config['built_in_pages_sync_ref']===false){return;}

	// Load json with all pages from remote reference site
	$remote=demosphere_built_in_pages_fetch_remote();
	if($remote===false){return;}
	
	// Now update each page
	$pages=Post::allBuiltIn();
	foreach($pages as $page=>$pid)
	{
		if(!val($demosphere_config,$page.'_page_sync') && !$isInstall){continue;}
		if(!isset($remote['pages'][$page])){echo "Warning: built_in_page sync: missing remote page ".$page."\n";continue;}
		demosphere_built_in_pages_sync_one($remote,$page,$isInstall);
	}
}

//! Updates the built-in page whose name is $page. Update is done using data in $remote array.
//! $remote is the full json response from refrence site, it contains all pages (not just this one).
function demosphere_built_in_pages_sync_one($remote,$page,$isInstall=false)
{
	//echo "demosphere_built_in_pages_sync_one: $page\n";
	//if($page!=='help_data'){return;}//  test
	global $demosphere_config;
	$remoteSiteUrl=$demosphere_config['built_in_pages_sync_ref'];
	$post=Post::fetch(Post::builtInPid($page));

	// Used to check for changes 
	$oldTitle=$post->title;
	$oldBody =$post->body;

	$post->title=$remote['pages'][$page]['title'];

	require_once 'dlib/html-tools.php';
	require_once 'htmledit/demosphere-htmledit.php';
	
	// Make links/src absolute
	$remoteUrl=$remoteSiteUrl.'/'.$remote['pages'][$page]['path'];
	$body=$remote['pages'][$page]['body'];
	$body=dlib_html_links_to_urls($body,$remoteUrl);
	// cleanup html
	$body=demosphere_htmledit_cleanup_html($body,['class-whitelist'=>false,'attributes'=>['id']]);

	// **** replace remote links with local versions

	// first build path matching between local and remote
	$localPages=Post::allBuiltIn();
	$localPaths=[];
	foreach($localPages as $name=>$pid){$localPaths[$name]=demosphere_path_alias_dest('post/'.$pid);}
	$remotePathName=array_flip(dlib_array_column($remote['pages'],'path'));
	$remotePathToLocal=[];
	foreach($remotePathName as $path=>$name){if(isset($localPaths[$name])){$remotePathToLocal[$path]=$localPaths[$name];}}

	// replace hrefs in links
	$body=preg_replace_callback('@(<a[^>]*\shref\s*=\s*["\'])([^\'"]*)(["\'])@s',function($m)use($remoteUrl,$remoteSiteUrl)
    {
		$thisUrl=dlib_html_entities_decode_all($m[2]);
		if(strpos($thisUrl,$remoteSiteUrl)!==0){return $m[0];}
		$path=substr($thisUrl,strlen($remoteSiteUrl)+1);
		$newPath=$path;
		if(isset($remotePathToLocal[$path])){$newPath=$remotePathToLocal[$path];}
		return $m[1].ent('/'.$newPath).$m[3];
	},$body);

	// **** replace remote images with local versions
	require_once 'demosphere-misc.php';
	$body=demosphere_make_images_local($body,true);

	// **** replace remote contact email with local contact
	$body=str_replace($remote['contact_email'],$demosphere_config['contact_email'],$body);

	// security (don't trust ref site for xss)
	require_once 'dlib/filter-xss.php';
	$body=filter_xss_admin($body);
	$post->body=$body;

	if($isInstall)
	{
		$newPath=$remote['pages'][$page]['path'];
		// sanitize:
		$newPath=preg_replace('@[^a-z0-9_-]@i','',$newPath);
		demosphere_path_alias_set('post/'.$post->id,$newPath);
	}

	// Only really save it things have actually changed
	if($isInstall || $post->title!==$oldTitle || $post->body!==$oldBody)
	{
		$post->save();
	}
}

//! Retrieves description of built-in pages from remote site using json query.
function demosphere_built_in_pages_fetch_remote()
{
	global $demosphere_config;
	$url=$demosphere_config['built_in_pages_sync_ref'].'/built-in-pages-json';
	$json=@file_get_contents($url);
	if($json===false){echo "built_in_pages_fetch_remote : failed\n";return false;}
	$remote=json_decode($json,true);
	return $remote;
}


//! Return a list of all built in pages on this site. We are on reference site.
function demosphere_built_in_pages_json()
{
	global $demosphere_config;
	$res=['pages'=>[]];
	$res['contact_email']=$demosphere_config['contact_email'];

	// build list of pages
	$pages=Post::allBuiltIn();
	foreach($pages as $name=>$pid)
	{
		$post=Post::fetch($pid);
		$desc=[];
		$desc['path' ]=demosphere_path_alias_dest('post/'.$pid);
		$desc['title']=$post->title;
		$desc['body' ]=$post->body;
		$res['pages'][$name]=$desc;
	}
	return $res;
}

//! Messages shown on the Post edit form for built-in pages
function demosphere_built_in_pages_post_edit_message($pid)
{
	global $demosphere_config,$base_url;
	$pages=Post::allBuiltIn();
	$name=array_search($pid,$pages);
	if($name===false){return false;}
	$res='<h4>'.$name.'</h4>';
	$res.='<p>'.t('This is a built-in page. You may edit it, but do not delete it. If you do not want to use it, just un-publish it and remove any links to it.').'</p>';
	$messages=
		[
		 'help_data'=>t('This page contains the help items that are displayed on many pages using the blue question mark buttons. Each help item is separated by a horizontal line. Help items start with a line that looks like this "HELPDESC;@path-regular-expression@;page-position-selector;options...". This line tells the help system on which page, and on what place of that page, it can show that help item.'),
		 'control_panel_data'=>t('This page contains data for your control panel. Each block of the control panel is separated by a horizontal line. It starts with a line with dpr_block(...). That decalation gives information about the block.'),
		 'contact'=>t('This page displays information on how to contact you.'),
			   ];
	if(isset($messages[$name])){$res.='<p>'.$messages[$name].'</p>';}
	if($demosphere_config['built_in_pages_sync_ref']===false)
	{
		require_once 'demosphere-config-form.php';
		$items=demosphere_config_description();
		if(demosphere_config_get_init($items[$name.'_page_sync'])===true)
		{
			$res.='<p>';
			$res.=t('Your site is a reference site for built-in pages. Other sites using your language might sync their page with this one.');
			$res.='</p>';
		}
	}
	else
	{
		$res.='<p>';
		$res.= ($demosphere_config[$name.'_page_sync'] ? 
				t('Once per day, this page is automatically updated from a reference site. Any changes you make to it will be lost.'):
				t('This page is NOT automatically updated from a reference site.')).'<br/>'.
			'[ '.
			'<a href="'.$base_url.'/configure/pages#'.$name.'_page_sync">'.t('Configure sync option for this page.'  ).'</a> | '.
			'<a href="'.$base_url.'/built-in-pages-diff/'.$name.'"      >'.t('View differences with the reference.'  ).'</a> | '.
			'<a href="'.$base_url.'/built-in-pages-sync-form/'.$name.'" >'.t('Update it manually from reference now.').'</a>'.
			' ]';
		$res.='</p>';
	}
	return $res;
}

//! A tiny form asking user to confirm if he wants to sync a page now.
function demosphere_built_in_pages_sync_form($page)
{
	global $demosphere_config;
	$pid=Post::builtInPid($page);
	if($pid===false){dlib_not_found_404('Unknown or invalid page: '.$page);}
	$remote=demosphere_built_in_pages_fetch_remote();
	if($remote===false){fatal('Failed to fetch remote info:'.$demosphere_config['built_in_pages_sync_ref'].'/built-in-pages-json');}
	$url=$demosphere_config['built_in_pages_sync_ref'].'/'.$remote['pages'][$page]['path'];

	$items=[];
	$items['message']=['html'=>t('Are you sure you want to copy the page called "@page" from !link ?',
								 ['@page'=>$page,
								  '!link'=>'<a href="'.ent($url).'">'.ent($url).'</a>'
								  ])];
	$items['ok']=
		[
		 'type'=>'submit',
		 'value'=>t('Ok'),
		 'redirect'=>'post/'.$pid,
		 'submit'=>function()use($remote,$page)
			 {
				 global $demosphere_config;
				 if($remote===false){fatal('Failed to fetch remote info:'.$demosphere_config['built_in_pages_sync_ref'].'/built-in-pages-json');}
				 demosphere_built_in_pages_sync_one($remote,$page);
				 dlib_message_add(t('Page update finished.'));
			 },
		 ];

	require_once 'dlib/form.php';
	return form_process($items);
}

//! A page displaying a diff between the local and remote versions of a built-in page.
function demosphere_built_in_pages_diff($page)
{
	global $demosphere_config,$currentPage,$base_url;
	$pid=Post::builtInPid($page);
	if($pid===false){dlib_not_found_404('Unkown or invalid page: '.ent($page));}
	$remote=demosphere_built_in_pages_fetch_remote();
	if($remote===false){fatal('Failed to fetch remote info:'.$demosphere_config['built_in_pages_sync_ref'].'/built-in-pages-json');}
	$remoteUrl=$demosphere_config['built_in_pages_sync_ref'].'/'.$remote['pages'][$page]['path'];

	$post=Post::fetch($pid);

	// *** fix content of remote page so that we can display it here
	$remoteBody=$remote['pages'][$page]['body'];
	require_once 'dlib/html-tools.php';
	$remoteBody=dlib_html_links_to_urls($remoteBody,$remoteUrl);
	require_once 'dlib/filter-xss.php';
	$remoteBody=filter_xss_admin($remoteBody);


	// display diffs
	$out='';
	$out.='<h2>'.t('@page : diferences between your version and reference.',
					   ['@page'=>$page]).
		'</h2>'.
		'<p>Note: if you sync, links and contact emails will be automatically fixed.</p>';
	$out.='<p>'.t('!update_link Click here</a> to manually update your version.',
		  ['!update_link'=>'<a href="'.$base_url.'/built-in-pages-sync-form/'.$page.'">']).
		'</p>';

	require_once 'dlib/hdiff.php';
	$options=[];
	$diffCols=[['html'=>filter_xss_admin($post->body),
				'sanitize'=>false,
				'title'=>'<a target="_blank" href="'.ent($base_url.'/post/'.$pid).'">'.ent(t('Your version')).'</a>',
				'save'=>$base_url.'/post-hdiff-save/'.$post->id
				],
			   ['html'=>$remoteBody,
				'sanitize'=>false,
				'title'=>'<a target="_blank" href="'.ent($remoteUrl).'">'.ent(t('Version on reference site')).'</a>',
				]];
	require_once 'htmledit/demosphere-htmledit.php';
	$token=demosphere_htmledit_hdiff();
	$out.=hdiff($diffCols,$options);
	$out.=$token;
	return $out;
}

//! Update built-in pages that are synced from language reference site.
function demosphere_built_in_pages_cron()
{
	demosphere_built_in_pages_sync_all();
}
// FIXME: should frontpage infoboxes use this system too? (ref sites might have customized them too much...)


// 	$res['contact_page']=>ct('Path to a page with information on how to contact you.') );
// 	$res['about_us_page']=>ct('Path to a page with information about your site.'),
// 	$res['publishing_guidelines_page']=>ct('Path to a page with information on what type of events site visitors can publish on your site.'),
// 	$res['map_help_page']=>ct('Path to a page with help for custom interactive map.'),
// 	$res['user_cal_help_page']=>ct('Path to a page with help for custom user calendars.'),
// 	$res['control_panel_data_page']=>ct('Do not change this.'),
// 	$res['help_data_page']=>ct('Do not change this.'),
// 	$res['spread_the_word_page']=>ct('A page with information so that users can help spread the word onyour site'),
// 	$res['event_sources_page']=>ct('A page with information on where your events come from.'),
// 	$res['help_event_emails_page']=>ct('A page telling user how to subscribe to emails.'),
// 	$res['help_feeds_page']=>ct('A page telling user how to use the rss feeds from your site.'),
// 	$res['help_ical_page']=>ct('A page telling user how to use the ical feeds from your site.'),
// 	$res['help_on_your_website_page']=>ct('A page telling user how to publish a calendar on their own web site.'),

?>