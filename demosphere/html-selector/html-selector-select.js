//! This javascript is inserted into a page imported from another site.
//! Normally, script tags were erased from the page, reducing the risk of
//! strange interactions with this javascript (for example different jquery versions).
(function()
{
	var demosSel=
	{
		inlineElements: ['A','ABBR','ACRONYM','B','BASEFONT','BDO','BIG','BR','CITE','CODE','DFN','EM','FONT','I','IMG','INPUT','KBD','LABEL','Q','S','SAMP','SELECT','SMALL','SPAN','STRIKE','STRONG','SUB','SUP','TEXTAREA','TT','U','VAR'],

		selectedElement: false,
		clickedElement: false,

		// downloads jquery javascript, then calls setup
		init: function()
		{
			// disable site's original jquery (might be very old version)
			if(typeof $!=='undefined'){var $disabled = jQuery.noConflict();}
			$=undefined;
			// Load our jquery
			var headEl = document.getElementsByTagName("head")[0];         
			var newScript = document.createElement('script');
			newScript.type = 'text/javascript';
			newScript.src = window.demosphere_url+'/lib/jquery.js';
			headEl.appendChild(newScript);

			// Wait for jquery load finished
			var ct=0;
			var interval=window.setInterval(function()
			{
				if(typeof $!=='undefined')
				{
					// If page is taking too long, call setup even if it says that it is not ready.
					if(ct>10 || (typeof document.readyState!=='undefined' && document.readyState==='complete'))
					{
						window.clearInterval(interval);
						demosSel.setup();
					}
					//else
					//{
					// 	console.log("not ready, wait with jquery");
					// 	$(document).ready(function(){console.log("jquery wait over");demosSel.setup();});
					//}
					ct++;
				}
			},300);
		},
		
		// Creates display areas and sets-up event handlers
		setup: function()
		{
			$('body').append('<div id="demos-sel-display"><span id="demos-path-parts"></span><a style="display:none" class="grow">'+demosphere_t.grow+'</a><a style="display:none" class="shrink">'+demosphere_t.shrink+'</a></div>');
			demosSel.createBox('demos-hover-box');
			demosSel.createBox('demos-selected-box');

			// Parent frame will send message describing initial selector
			window.addEventListener("message",function(e)
			{
				//console.log('recieve message inside frame:',e,e.data);
				var data=JSON.parse(e.data);

				if(data.hasOwnProperty('initialSelector') && data.initialSelector!=='')
				{
					var el=$(data.initialSelector)[0];
					$('#demos-sel-display a').show();
					demosSel.selectedElement=el;
					demosSel.clickedElement=el;
					demosSel.showBox('demos-selected-box',el);
					demosSel.displayPath();
				}
				if(data.hasOwnProperty('mousedown')){$(data.mousedown).trigger("mousedown");}
			}, false);


			// hide the hoverbox when using the grow/shrink buttons
			$('#demos-sel-display a').mouseover(function()
            {
				demosSel.showBox('demos-hover-box',false);
			});
			// "grow" button
			$('#demos-sel-display a.grow').click(function()
			{
				//console.log('grow',demosSel.selectedElement);
				if(demosSel.selectedElement===false)
				{alert('Nothing selected yet. Please click inside the page first');return;}
				var newEl=demosSel.selectedElement.parentNode;
				if(newEl.tagName==='BODY' || newEl.tagName==='HTML'){return;}
				demosSel.selectElement(newEl,false);
			});
			// "move-down" (shrink) button
			$('#demos-sel-display a.shrink').click(function()
			{
				//console.log('shrink',demosSel.selectedElement);
				if(demosSel.clickedElement===false)
				{alert('Nothing selected yet. Please click inside the page first');return;}
				var path=demosSel.buildPath(demosSel.clickedElement);
				var prev=demosSel.clickedElement;
				var found=false;
				$(demosSel.clickedElement).parents().each(function()
				{
					//console.log('loop',this,demosSel.selectedElement);
					if(this===demosSel.selectedElement){found=prev;return false;};
					prev=this;
				});
				if(found!==false){demosSel.selectElement(found,false);}
			});
			// disable all normal clicks (for ex: links)
			$('*').click(function(e){e.preventDefault();});

			// Click on an area to select it
			$('*').mousedown(function(e)
			{
				//console.log('mousedown',this.tagName,$(this).attr('id'),$(this).attr('class'));
				e.preventDefault();
				if(e.which!=1){return;}// only left button
				if($(this).parents('#demos-sel-display').size()>0){e.stopPropagation();return;}
				if($(this).hasClass('demos-sel-box')             ){e.stopPropagation();return;}
				if(this.tagName=='HTML'){return;}
				if(demosSel.inlineElements.indexOf(this.tagName)!==-1){return;}
				e.stopPropagation();
				demosSel.selectElement(this,true);
			});
			
			// Hover: show a box around the element under the mouse
			$('*').mouseover(function(e)
			{
				//console.log('mouseover',this.tagName,$(this).attr('id'),$(this).attr('class'));
				if(this.getAttribute('id')==='demos-sel-display' ||
					(this.parentNode.getAttribute && 
					 this.parentNode.getAttribute('id')==='demos-sel-display'))
					                                 {e.stopPropagation();return;}
				if($(this).hasClass('demos-sel-box')){e.stopPropagation();return;}
				if(demosSel.inlineElements.indexOf(this.tagName)!==-1){return;}
				e.stopPropagation();
				demosSel.showBox('demos-hover-box',this);
			});

			// Hide hover box if mouse leaves frame
			$(document.body      ).mouseleave(function(){demosSel.showBox('demos-hover-box',false);});
			// Hide hover box if mouse is in path display
			$("#demos-path-parts").on('mouseenter','.demos-path-part',
									  function(){demosSel.showBox('demos-hover-box',this.demosEl);});
			
			// tell main window that we are ready (it can now tell us what is init selector)
			window.parent.postMessage("ready:",window.demosphere_url);
		},

		//! Called when user clicks on an element. A fixed box will be shown around it.
		//! A message contining the full path of the selected element is sent to the main form 
		//! page. 
		selectElement: function(el,isClick)
		{
			$('#demos-sel-display a').show();
			demosSel.selectedElement=el;
			var path=demosSel.buildPath(el);
			if(isClick){demosSel.clickedElement=el;}
			// Path that we send to other frame (which sends to server). 
			// We need to copy the path, without "element" which is a DOM object that we don't want to encode to JSON
			var cleanPath=[];
			$.each(path,function(){var n=jQuery.extend({},this);delete n.element;cleanPath.push(n);});
			window.parent.postMessage("selectpath:"+JSON.stringify(cleanPath),
									  window.demosphere_url);
			demosSel.showBox('demos-selected-box',el);
			demosSel.displayPath();
		},
		displayPath: function()
		{
			var path=demosSel.buildPath(demosSel.clickedElement);
			var hpath=$('#demos-path-parts');
			hpath.html("");
			for(var i=path.length-1;i>=0;i--)
			{
				var pathPart=path[i];
				var span=$('<span class="demos-path-part"/>');
				var isSelected=(($(pathPart.element)[0])==($(demosSel.selectedElement)[0]));
				if(isSelected){span.addClass('selected');}
				var title='';
				title+=pathPart.tagName;
				span.append($('<span class="demos-path-tagname"/>').text(pathPart.tagName));
				var hasId=false;
				if(typeof pathPart.id!=='undefined' && pathPart.id.length)
				{
					span.append($('<span class="demos-path-id"/>').text('#'+pathPart.id));
					title+='#'+pathPart.id;
					hasId=true;
				}
				if(typeof pathPart.className!=='undefined' && pathPart.className.length)
				{
					var c='.'+$.trim(pathPart.className).replace(/\s+/g,'.');
					if(!hasId){span.append($('<span class="demos-path-class"/>').text(c));}
					title+=c;
				}
				span.attr('title',title);
				span[0].demosEl=pathPart.element;
				span.click(function(){demosSel.selectElement(this.demosEl);});
				hpath.append(span);
			}
			if(hpath[0].scrollWidth>hpath.innerWidth())
			{
				var sel=$('.demos-path-part.selected');
				if(sel.length){sel[0].scrollIntoView();}
			}
		},
		//! Builds an array describing each item in the path of this element.
		//! Path begins at selected element and moves up to the document body (not included).
		buildPath: function(el)
		{
			var path=[];
			var buildPathStep=function(el)
			{
				//console.log('buildPathStep',el.tagName,$(el).attr('id'),$(el).attr('class'));
				var pos=1,posOk=false;
				var siblings=$(el).parent().children(el.tagName.toLowerCase());
				siblings.each(function()
							  {
								  if(this===el){posOk=true;return false;}
								  pos++;
							  });
				//console.log('pos',posOk,pos);
				path.push(
					{
						tagName:el.tagName.toLowerCase(),
						id:$(el).attr('id'),
						className:$(el).attr('class'),
						pos:pos,
						nbSiblings:siblings.size(),
						element: el
					});
			};

			if(el.tagName!=='BODY'){buildPathStep(el);}
			$(el).parents().each(function()
			{
				//console.log('buildPathStep',this.tagName,$(this).attr('id'),$(this).attr('class'));
				if(this.tagName==='BODY' || 
				   this.tagName==='HTML'   ){return false;}
				buildPathStep(this);
			});

			return path;
		},

		//! Creates 4 divs that will be used to draw a box around an element.
		createBox: function(baseId)
		{
			var d=['t','b','l','r'];
			for(var i=0;i<d.length;i++)
			{
				var t=d[i];
				$('body').append($('<div></div>').
					attr('id',baseId+'-'+t).
					attr('class',"demos-sel-box demos-sel-box-"+t+" "+baseId));
			}
		},
		//! Positions 4 divs to draw a box around an element.
		//! The divs are absolutely positioned.
		showBox: function(baseId,el)
		{
			if(el===false){$('.'+baseId).hide();return;}
			// determine position and size of the box
			var pos=$(el).offset();
			var w=$(el).outerWidth();
			var h=$(el).outerHeight();

			// some elements have 0 height, try scrollHeight (example: sub element is float positioned?)
			if(h===0 && $(el)[0].scrollHeight!=0){h=$(el)[0].scrollHeight;}
			pos.left=Math.max(0,pos.left-2);
			pos.top =Math.max(0,pos.top -2);

			// Avoid creating box outside of visible window
			w=Math.min($(document).width() -5,w+4);
			h=Math.min($(document).height()-5,h+4);

			// position each div

			var div;
			div=$('#'+baseId+'-t');
			//console.log('div','#'+baseId+'-t',div);
			div.css('left',pos.left+"px");
			div.css('top',pos.top+"px");
			div.width(w);
			div.height(3);
			div.show();

			div=$('#'+baseId+'-l');
			div.css('left',pos.left+"px");
			div.css('top',pos.top+"px");
			div.width(3);
			div.height(h);
			div.show();

			div=$('#'+baseId+'-r');
			div.css('left',(pos.left+w-3)+"px");
			div.css('top',pos.top+"px");
			div.width(3);
			div.height(h);
			div.show();

			div=$('#'+baseId+'-b');
			div.css('left',pos.left+"px");
			div.css('top',(pos.top+h-3)+"px");
			div.width(w);
			div.height(3);
			div.show();
		}
	};
	demosSel.init();
})();

