/*! \mainpage Demosphere source code documentation.

Demosphere is fairly large app for activist web calendars.
For more information check : https://demosphere.eu 
It is based on dlib, a small framework / library of functions that was initially developped for Demosphere.
Demosphere is written in procedural PHP with OOP used only where it seems appropriate (\ref coding-style).

@htmlonly
<style type="text/css">h2{position: relative;left: -15px;font-size: 140%;}</style>
@endhtmlonly

Main classes: Event, Place, User, City

@section demos_comps Demosphere components:
- backend form : demosphere-event-edit-form.php, demosphere-event-edit-form.js
- public form : demosphere-event-publish-form.php
- repetitions : demosphere-event-repetition.php
- control panel : demosphere-panel.php, demosphere-panel.js
- configuration: demosphere-config-form.php, demosphere-config-check.php, demosphere-config-form.js
- edit multiple / pending events : demosphere-event-multiple-edit.php
- calendar (rendered list of events) : demosphere-event-list.php, demosphere-calendar.tpl.php
- list of events, and managing options for filtering and displaying a list : demosphere-event-list.php
- form items for filtering a list of events : demosphere-event-list-form.php
- widget (display a calendar on another website) : demosphere-widget.php, demosphere-widget.js
- user email subscription : demosphere-email-subscription.php, demosphere-email-subscription.js
- rss/ical feeds (and form for building url)  : demosphere-feeds.php
- user calendar: demosphere-user-calendar.php

@section dlib_desc Dlib

FIXME 

- tools.php
- \ref database
- \ref dbobject_ui
- \ref comments
- \ref dbsessions
- \ref form
- \ref template
- \ref variable
- DBObject


@section sep_comps Separate components:
These components are meant to be more fairly separate from Demosphere.

- \ref feed_import "feed import"
- \ref mail_import "mail import"
- \ref page_watch "page watch"
- \ref text_matching "text matching"
- \ref docconvert "docconvert.php"
- \ref html_selector "html selector"


*/

