<?php

function demosphere_install($args)
{
	global $appDir;
	$requiredArgs=['site-id','db-host','db-user','db-password-file','db-database','locale','timezone','hosted','admin-email','base-url'];
	if($args['hosted']!=='yes'){$requiredArgs[]='contact-email';}
	else
	{
		// Hackish check for email domain (if none, then we need contact-email)
		global $demosphere_config;
		$demosphere_config['std_base_url']=$args['base-url'];
		$demosphere_config['site_id']='none';
		require_once 'demosphere-emails.php';
		$domain=demosphere_emails_hosted_domain();
		if($domain===false)
		{
			echo "No valid email domain found for hosted site\n";
			$requiredArgs[]='contact-email';
		}
		unset($demosphere_config);
	}

	$missingArgs=array_diff($requiredArgs,array_keys($args));
	if(count($missingArgs))
	{
		echo "Missing arguments: --".implode(', --',$missingArgs)."\n";
		exit(1);
	}

	// **** Check that everything is ok before trying to install 
	require_once 'demosphere-config-check.php';
	$req=[];
	demosphere_config_check('install',$req,$args);
	if(count($req))
	{
		foreach($req as $name=>$val)
		{
			echo $val['severity'].':'.$name.': '.$val['description']."\n";
		}
		exit(1);
	}

	// **** create dabase configuration file 
	$config=[];
	$config['site_id']=$args['site-id'];
	$config['database']=[];
	$config['database']['host']=$args['db-host'];
	$config['database']['user']=$args['db-user'];
	$config['database']['password']=trim(file_get_contents($args['db-password-file']));
	$config['database']['database']=$args['db-database'];
	$config['is_backup_site']=false;// FIXMEx: shell script needs to be fixed on backup server

	// **** add commmand line credentials
	if(isset($args['commandline-admin-name'   ]))
	{
		$adminName=$args['commandline-admin-name'   ];
		if(isset($args['commandline-hash-dir'     ])){$config['commandline_hash_dirs'     ]=[$adminName=>$args['commandline-hash-dir'     ]];}
		if(isset($args['commandline-admin-pub-key'])){$config['commandline_admin_pub_keys']=[$adminName=>$args['commandline-admin-pub-key']];}
	}

	$configFile="<?php\n\nfunction site_config()\n{\n\treturn ";
	$configFile.=var_export($config,true);
	$configFile.=";\n}\n?>";
	file_put_contents($appDir.'/site-config.php',$configFile);

	// **** create file/ directory structure
	demosphere_install_create_directories_and_files($args);

	// **** connect to database
	require_once 'dlib/database.php';
	global $db_config;
	$db_config=$config['database'];
	db_setup();

	require_once 'dlib/tools.php';
	require_once 'dlib/DBObject.php';
	require_once 'dlib/comments/Comment.php';

	dlib_setup();
	setlocale(LC_ALL, $args['locale']);
	setlocale(LC_NUMERIC, 'C');
	ini_set('date.timezone',$args['timezone']);

	// **** install each module and create class tables
	echo "install dbsessions\n";
	require_once 'dlib/dbsessions.php';
	dbsessions_install();

	require_once 'dlib/variable.php';
	echo "install variable\n";
	variable_install();

	require_once 'dlib/translation-backend.php';
	require_once 'dlib/translation.php';
	echo "install translation\n";
	translation_install();

	require_once 'demosphere-page-cache.php';
	echo "install cache\n";
	demosphere_page_cache_install();

	require_once 'User.php';
	echo "install User\n";
	User::createTable();

	require_once 'Post.php';
	echo "install Post\n";
	Post::createTable();

	require_once 'Event.php';
	echo "install Event\n";
	Event::createTable();

	require_once 'RepetitionGroup.php';
	echo "install RepetitionGroup\n";
	RepetitionGroup::createTable();

	require_once 'Opinion.php';
	echo "install Opinion\n";
	Opinion::createTable();

	require_once 'demosphere/demosphere-place-search.php';
	echo "install place search\n";
	demosphere_place_search_index_install();

	require_once 'City.php';
	echo "install City\n";
	City::createTable();
	// create empty city: city->id=1 
	$empty=new City();
	$empty->save();

	require_once 'Place.php';
	echo "install Place\n";
	Place::createTable();
	// create empty place: place->id=1 
	$empty=new Place();
	$empty->save();

	require_once 'Topic.php';
	echo "install Topic\n";
	Topic::createTable();

	require_once 'Term.php';
	echo "install Term\n";
	Term::createTable();

	require_once 'Carpool.php';
	echo "install Carpool\n";
	Carpool::createTable();

	require_once 'dlib/comments/Comment.php';
	echo "install Comment\n";
	Comment::createTable();
	require_once 'dlib/comments/IpWhois.php';
	IpWhois::createTable();
	require_once 'dlib/comments/Ban.php';
	Ban::createTable();

	require_once 'demosphere/demosphere-path-alias.php';
	echo "install path alias\n";
	demosphere_path_alias_install();

	require_once 'demosphere/demosphere-user-calendar.php';
	echo "install user cal\n";
	demosphere_user_calendar_install();

	require_once 'demosphere/demosphere-event-revisions.php';
	echo "install revisions\n";
	demosphere_event_revisions_install();

	require_once 'mail-import/mail-import.php';
	require_once 'mail-import/MimeMessage.php';
	echo "install mail import\n";
	mail_import_install();
	require_once 'feed-import/feed-import.php';
	echo "install feed import\n";
	feed_import_install();
	require_once 'page-watch/page-watch.php';
	echo "install page watch\n";
	page_watch_install();
	require_once 'text-matching/text-matching.php';
	echo "install text matching\n";
	text_matching_install();


	require_once 'demosphere-misc.php';
	echo "install log\n";
	demosphere_log_install();

	require_once 'demosphere-self-edit.php';
	echo "install self edit\n";
	demosphere_self_edit_install();

	require_once 'demosphere-stats.php';
	echo "install daily visits\n";
	demosphere_stats_daily_visits_install();

	// unsued in new sites, but create so that all sites have same tables
	db_query('CREATE TABLE IF NOT EXISTS `migrate_nodes` (
  `nid` int(11) NOT NULL,
  `is_event` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  PRIMARY KEY (`nid`),
  KEY `is_event` (`is_event`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ');

	// **** 
	demosphere_install_configure_demosphere($args);
}

function demosphere_install_configure_demosphere($args)
{
	global $demosphere_config,$base_url,$appDir,$dlib_config,$mail_import_config;

	$base_url=$args['base-url'];
	if(!preg_match('@^https?://.*[^/]$@',$base_url)){fatal('Invalid base-url');}
	$demosphere_config['std_base_url']=$base_url;

	// **** load translations

	translation_po_file_import($appDir.'/dlib/translations/dlib.'.dlib_get_locale_name($args['locale']).'.po');
	translation_po_file_import($appDir.'/demosphere/translations/demosphere.'.dlib_get_locale_name($args['locale']).'.po');

	// **** create the demosphere_config variable with initial values

	require_once 'demosphere-config-form.php';
	$demosphere_config=[];
	$demosphere_config['hosted']=$args['hosted']==='yes';
	$demosphere_config['site_name']=val($args,'site-name','FIXME site-name');
	$demosphere_config['site_id']=$args['site-id'];
	$demosphere_config['std_base_url']=$base_url;
	if(isset($args['contact-email'])){$demosphere_config['contact_email']=$args['contact-email'];}

	// **** date and time

	// setup timezones
	$demosphere_config['timezone']=$args['timezone'];

	// locale
	$demosphere_config['locale' ]=$args['locale'];

	// set default values for all $demosphere_config items 
	$desc=demosphere_config_description();
	foreach($desc as $name=>$item)
	{
		if($name==='hosted' || $name==='timezone' || $name==='locale'){continue;}
		if(isset($demosphere_config[$name])){continue;}
		$demosphere_config[$name]=demosphere_config_get_init($item);
	}

	$demosphere_config['admin_email']=$args['admin-email'];

	if(isset($args['built-in-pages-sync-ref'])){$demosphere_config['built_in_pages_sync_ref']=$args['built-in-pages-sync-ref'];}

	$demosphere_config['tmp_dir' ]=exec('mktemp -d');

	// Quick fix to avoid crash during demosphere_cache_clear() which calls Message::cleanupCachedFiles()
	$mail_import_config['cache_dir']=$demosphere_config['tmp_dir'];
	$mail_import_config['cache_time']=0;

	// **** create users 

	$admin=new User();
	$admin->login=val($args,'admin-user','admin');
	if(isset($args['admin-password-file'])){$pass=trim(file_get_contents($args['admin-password-file']));}
	else                                   {$pass=dlib_random_string(15);}
	$admin->setHashedPassword($pass);
	$admin->setRole('admin');
	$admin->email=$args['admin-email'];
	if($demosphere_config['hosted']){$admin->data['emails-received-options']['never']=true;}
	$admin->save();
	if($demosphere_config['hosted'])
	{
		$demosphere_config['debug_rule']='uid='.$admin->id;
	}

	$topic=new Topic(t('example'));
	$topic->save(false);
	$topic=new Topic(t('example2'));
	$topic->save(false);

	$city=new City(t('Example City'));
	$city->save();

	// ********* create pages

	require_once 'demosphere-built-in-pages.php';
	$dlib_config['log']=$demosphere_config['tmp_dir' ].'/demosphere-general.log';
	demosphere_built_in_pages_install();
	// publish all buit-in pages except data (panel, help)
	db_query("UPDATE Post SET status=1 WHERE builtIn!='' AND builtIn NOT LIKE '%%_data'");

	// ******** create frontpage announcement and messages
	$post=new Post(t('Frontpage announcement'),$admin->id,'');
	$post->save();
	$demosphere_config['frontpage_announcement']=$post->id;
	$post=new Post(t('Frontpage messages'),$admin->id,'');
	$post->save();
	$demosphere_config['frontpage_messages']=$post->id;

	// ******** create infoboxes
	$infoboxes=[];
	$site_name=$demosphere_config['site_name'];
	$post=new Post('frontpage map link',$admin->id,'',['!site_name'    =>$site_name]);
	foreach(['20:30','18:30','19:30'] as $time)
	{
		$ts=strtotime('today '.$time);
		require_once 'demosphere-date-time.php';
		$post->body.='<p>'.ent(demos_format_date('time',$ts)).'</p>'."\n";
	}
	$post->save();
	$infoboxes[]=['pid'=>$post->id,
				  'class'=>'demosphereMapLink stdInfobox whiteInfobox',
				  'title-link'=>'map'];

	$post=new Post('Participez !',$admin->id,
				   '<h2 id="publicFormButton"><a href="'.$base_url.'/publish">'.t('submit an event').'</a></h2><h2><a href="'.Post::builtInUrl('contact').'">'.t('contact us').'</a></h2><h2><a href="'.$base_url.'/'.t('spread-the-word').'">'.t('spread the word').'</a></h2>');
	$post->save();

	$infoboxes[]=['pid'=>$post->id,
				  'class'=>'infoboxWithLogo stdInfobox',
				  'title-link'=>false];

	$post=new Post($site_name.' ?',$admin->id,
				   t('<p>!site_name is an independent web calendar</p><p>Every day we sort through <a href="!event_sources">hundreds of emails and web pages</a> to find and format and publish these events.</p><h4><a href="!about_us">More information about us.</a></h4>',
					 ['!site_name'    =>$site_name,
					  '!event_sources'=>$base_url.'/'.t('event-sources'),
					  '!about_us'     =>Post::builtInUrl('about_us'),
					 ]));
	$post->save();
	$infoboxes[]=['pid'=>$post->id,
				  'class'=>'stdInfobox',
				  'title-link'=>Post::builtInUrl('about_us',false)];

	$post=new Post(t('Stay informed!'),$admin->id,
				   '<p id="rss-logo">rss-logo</p>'.
				   '<ul>'.
				   '<li><a href="'.$base_url.'/'.t('help-event-emails').'">'.t('by email').' demosphere_dtoken_email_icon</a></li>'.
				   '<li><a href="'.$base_url.'/'.t('help-feeds')	    .'">'.t('RSS feeds').'</a></li>'.
				   '<li><a href="'.$base_url.'/'.t('help-ical')		.'">'.t('iCal').'</a></li>'.
				   '<li><a href="'.$base_url.'/'.t('help-on-your-website') .'">'.t('on your website').'</a></li>'.
				   '</ul>');
	$post->save();
	$infoboxes[]=['pid'=>$post->id,
				  'class'=>'infoboxStayInformed fullInfobox',
				  'title-link'=>false];

	$demosphere_config['front_page_infoboxes']=$infoboxes;

	// ******** place search index statistics
	$wordFreqFile='demosphere/translations/word-frequencies.'.dlib_get_locale_name().'.txt';
	if(file_exists($wordFreqFile))
	{
		$wordFreq=json_decode(file_get_contents($wordFreqFile),true);
	}
	else{$wordFreq=[];}
	variable_set('word_freq','',$wordFreq);

	// **** create cron key
	variable_set('key','cron',dlib_random_string(15));

	// **** create private key
	$privateKeyRsr = openssl_pkey_new([
									   "private_key_bits" => 2048,
									   "private_key_type" => OPENSSL_KEYTYPE_RSA,
									   ]);

	openssl_pkey_export($privateKeyRsr, $privateKeyPem);
	variable_set('site_private_key','',$privateKeyPem);

	// *** Set update-db to the last available update
	require_once "demosphere-update-db.php";
	$allFcts=get_defined_functions();
	$updates=preg_replace("@^demosphere_update_db_@",'',preg_grep("@^demosphere_update_db_[0-9]+$@",$allFcts["user"]));
	$updates=array_map('intval',array_values($updates));
	variable_set('update-db-current','',max($updates));


	// *** Setup comments 
    // ip whois "not-country" ban for France
	if(dlib_get_locale_name()=='fr')
	{
		$ban=new Ban();
		$ban->banType ='not-country';
		$ban->value='FR';
		$ban->end  =Ban::permanent();
		$ban->label='@created-at-install';
		$ban->save();
	}

	// tor ips
	$ban=new Ban();
	$ban->banType ='ip-list';
	$ban->value='/var/local/demosphere/tor-ips';
	$ban->end  =Ban::permanent();
	$ban->label='@tor-ips-created-at-install';
	$ban->save();

	// **** save values to $demosphere_config	
	variable_set('demosphere_config','',$demosphere_config);
}


function demosphere_install_create_directories_and_files($args)
{
	global $demosphere_config;
	require_once 'dlib/tools.php';
	exec('rsync -r --ignore-existing --exclude=*~ --exclude=.gitignore '.
		 (val($args,'use-custom') ? '' : '--exclude=/custom').' '.
		 'demosphere/install-directories/ ./',$out,$ret);
	if($ret){fatal('demosphere_install_create_directories: failed');}
	exec('chmod -R og+w files 2>/dev/null',$out,$ret); // re-install errors to chmod existing files owned by www-data
	// if($ret){fatal('demosphere_install_create_directories: failed');}

	// Create robots.txt
	exec('test -L robots.txt && rm robots.txt');
	exec('sed "s@_STD_BASE_URL_@'.$demosphere_config['std_base_url'].'@g" demosphere/robots.tpl.txt > robots.txt');
}


?>