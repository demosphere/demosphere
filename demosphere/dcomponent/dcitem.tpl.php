<? global $dcomponent_config ?>
<article id="$type$item->id" 
	class="$type dcitem <?: $isHighlighted ? 'highlighted' : 'nothighlighted' ?> $classes">
<h2 class="title-line">
	<img class="finish" src="$dcomponent_config['dir_url']/big-del.png" 
		id="delete$item->id" 
		onclick="dcomponent_finish_dcitem(this,$item->id,'$scriptUrl$finishUrl')" 
		alt="finish" />
	<span class="idNumber">$item->id</span>
	<a class="title-link <?: strlen($title) > 100 ? 'long-title' :'' ?>"
		href="$titleUrl">
		$title
	</a>
	<?= @$titleRight ?>
</h2>
<div class="main-line">
	<ul class="info-left">
		<li class="infoPart cycleButtons">
			<span class="prev" title="_(previous highlighted date or keyword)"></span> 
			<span class="next" title="_(next highlighted date or keyword)"    ></span>
		</li>
		<?= @$infoLeftBottom ?>
	</ul>

	<div class="dc-middle">
		<div class="info-top line-0">
			<?= @$infoTopLinePrepend ?>
			<? if(count($events)){?>
				<span  class="infoPart ref-demosphere-events">
					<? foreach($events as $nid=>$eventUrl){ ?>
						<a target="_blank" href="$eventUrl">$nid</a>
					<?}?>
				</span>
			<?}?>
			<span class="infoPart timestamps overflow-popup">
				<? foreach($highlightInfo['timestamps'] as $t){ ?>
					<em class="demosphere_timestamp" title="<?: 
						dcomponent_format_date('demosphere-timestamp',$t)?>"><?:
						dcomponent_format_date('short-date-time',$t)?></em>
				<?}?>
			</span>
			<span class="infoPart keywords">
				<? foreach(array_count_values(array_map('strtolower',val($highlightInfo,'keywordMatches',[]))) as $k=>$ct){ ?>
						<em class="demosphere_keyword">$k<?: $ct>1 ? ' x '.$ct : ''?></em>
				<?}?>
			</span>
			<span class="text-matching"><?= $textMatching ?></span>
		</div>
		<?= @$infoTopLine0After ?>
		<div class="frame-resize-buttons">
			<a href="#" title="_(bigger viewing area)">+</a><a href="#" title="_(smaller viewing area)">-</a>
		</div>
		<? // our use of sandbox is still experimental. ?>
		<? // Is it usefull ? : not too much: blocks forms, top-navigation... Test it for a while,if problems, then remove.  ?>  
		<? // Both "allow-popups" and "allow-same-origin" are needed to be able to click on office documents  ?>
		<? // "allow-scripts" is needed for cycle buttons, getheight and tracker links ?>
		<? // "allow-modals" is needed for confirm popup on disabled tracker links ?>
		<? // Since src is set dynamically Firefox complains about having "allow-same-origin" and "allow-scripts" ?>	
		<? // so allow-same-origin is added dynamically ?>
		<iframe data-src="$iframeUrl" sandbox="allow-scripts allow-popups allow-modals <? /* allow-same-origin */ ?>"
				class="cycleContainer content-frame" 
				<?= isset($iframeExtraAttrs) ? $iframeExtraAttrs : '' ?> > 
			_(loading...)
		</iframe>
	</div>
</div>
</article>
