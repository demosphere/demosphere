<?php

function parse_file(string $filename,array $options=[])
{
	$defaults=['rawData'=>false,
			   'separator'=>':',
			   'schema'=>false,
			   'key'=>false,
			   'trim'=>false,
			   'extendSchema'=>true,
			   'lineComment'=>'#',
			   'keepCommentLines'=>false,
			   'failOnError'=>true,];
	extract(array_merge($defaults,array_intersect_key($options,$defaults)));

	if($rawData===false){$file=file_get_contents($filename);}
	else{$file=$rawData;}

	if($file===false)
	{
		if($failOnError){fatal('parse_file: failed to open file');}
		return false;
	}

	$res=[];
	$lines=explode("\n",$file);
	foreach($lines as $line)
	{
		if(trim($line)===''){continue;}
		if($trim){$line=trim($line);}
		if($lineComment!==false && strpos($line,$lineComment)===0)
		{
			if($keepCommentLines){$res[]=$line;}
			continue;
		}
		$parts=explode($separator,$line);
		if($schema!==false)
		{
			$nschema=$schema;
			if($extendSchema)
			{
				for($i=count($nschema);$i<count($parts);$i++){$nschema[]='unnamed-col-'.$i;}
			}
			if(count($nschema)!==count($parts))
			{
				//var_dump($line);
				if($failOnError){fatal("parse_file: wrong number of cols\n");}
				return false;
			}
			$parts=array_combine($nschema,$parts);
		}
		if($key===false){$res[]=$parts;}
		else            {$res[$parts[$key]]=$parts;}
	}
	return $res;
}

function dlib_read_csv(string $fname,$firstLineIsIndex=false,$trim=true)
{
	if(($handle=fopen($fname, "r"))===false){return false;}
	$res=[];
    while(($data=fgetcsv($handle, 0, ";")) !== false) 
	{
		if($trim){$data=array_map('trim',$data);}
		$res[]=$data;
	}
    fclose($handle);

	if($firstLineIsIndex)
	{
		$lines=$res;
		$res=[];
		$keys=array_shift($lines);
		foreach($lines as $line)
		{
			$res[]=array_combine($keys,$line);
		}
	}

	return $res;
}

?>