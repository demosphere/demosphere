<?php

//! Returns an html table that displays the results of an sql SELECT query.
//! Rendering is done with table_render, and adds column sort, search...
//! For $config options see table_render()
function table_render_query($query,$queryArgs=[],$config=[])
{
	global $dbobject_ui_config;
	if(!preg_match('@^SELECT @',$query)){fatal('query_render: query must begin with "SELECT "');}
	list($columns,$defaultOrderField)=table_render_query_columns($query,$queryArgs);

	// Guess search concat (how to add search query : AND/WHERE/HAVING)
	if(!isset($config['search_concat_where_and']))
	{
		if(preg_match('@\sWHERE\s@',$query)){$config['search_concat_where_and']='AND';}
	}
	return table_render('query',$columns,['query'=>$query,'args'=>$queryArgs],$config,$defaultOrderField,val($config,'actionUrl'));
}

//! Translation table between mysql types (mysqli_fetch_fields()) and our type.
//! Type is same as used by DBObject and displayed in table_render
function table_mysqli_types()
{
	return [MYSQLI_TYPE_DECIMAL     =>'int'		  ,		
			MYSQLI_TYPE_NEWDECIMAL	=>'int'		  ,		
			MYSQLI_TYPE_BIT			=>'int'		  ,		
			MYSQLI_TYPE_TINY		=>'int'		  ,		
			MYSQLI_TYPE_SHORT		=>'int'		  ,		
			MYSQLI_TYPE_LONG		=>'int'		  ,		
			MYSQLI_TYPE_LONGLONG	=>'int'		  ,		
			MYSQLI_TYPE_INT24		=>'int'		  ,		
			MYSQLI_TYPE_FLOAT		=>'float'	  ,		
			MYSQLI_TYPE_DOUBLE		=>'float'	  ,		
			MYSQLI_TYPE_NULL		=>false		  ,	
			MYSQLI_TYPE_TIMESTAMP	=>false       ,
			MYSQLI_TYPE_DATE		=>false		  ,
			MYSQLI_TYPE_TIME		=>false		  ,
			MYSQLI_TYPE_DATETIME	=>false       ,
			MYSQLI_TYPE_YEAR		=>false		  ,	
			MYSQLI_TYPE_NEWDATE		=>false		  ,	
			MYSQLI_TYPE_INTERVAL	=>false		  ,	
			MYSQLI_TYPE_ENUM		=>false		  ,	
			MYSQLI_TYPE_SET			=>false		  ,	
			MYSQLI_TYPE_TINY_BLOB	=>'textfield' ,	
			MYSQLI_TYPE_MEDIUM_BLOB =>'textarea'  ,	
			MYSQLI_TYPE_LONG_BLOB	=>'textarea'  ,	
			MYSQLI_TYPE_BLOB		=>'textarea'  ,	
			MYSQLI_TYPE_VAR_STRING	=>'textfield' ,	
			MYSQLI_TYPE_STRING		=>'textfield' ,	
			MYSQLI_TYPE_CHAR		=>'int' ,	
			MYSQLI_TYPE_GEOMETRY	=>false		  ,	
			];
}

//! Returns a desciption of columns (schema) of a query by using mysqli_fetch_fields()
//! Also returns the $defaultOrderField
function table_render_query_columns($query,$queryArgs=[])
{
	global $dbobject_ui_config,$dbtable_ui_config;

	// dbobject-ui setup is necessary for automatic links to DBObjects when foreign key is shown
	require_once 'dlib/dbobject-ui.php';
	dbobject_ui_auto_setup();

	$mysqliTypes=table_mysqli_types();

	$qres=call_user_func_array('db_query',array_merge([$query.' LIMIT 0'],$queryArgs));
	$mysqlFields=mysqli_fetch_fields($qres);
	$defaultOrderField=false;

	foreach($mysqlFields as $nb=>$mysqlField)
	{
		$column=[];

		// Autodetect if this field is a foreign key to a DBObject
		if(isset($dbobject_ui_config['classes'][$mysqlField->orgtable]))
		{
			$class=$mysqlField->orgtable;
			$schema=$class::getSchema();
			if(isset($schema[$mysqlField->orgname]))
			{
				$column=$schema[$mysqlField->orgname];
				if(!isset($column['class'])){$column['class']=$class;}
			}
		}
		
		// Autodetect table types
		if(isset($dbtable_ui_config['tables'][$mysqlField->orgtable]))
		{
			$table=$mysqlField->orgtable;
			$schema=val($dbtable_ui_config['tables'][$table],'schema',[]);
			if(isset($schema[$mysqlField->orgname]))
			{
				$column=$schema[$mysqlField->orgname];
			}
		}

		// Map mysql field type to a simple type (int, float, timestamp that we know how to manage 
		if($column===[])
		{
			if(!isset($mysqliTypes[$mysqlField->type])){fatal("unknown mysqli type");}
			$type=$mysqliTypes[$mysqlField->type];
			if($type!==false){$column['type']=$type;}
		}
		if($defaultOrderField===false && 
		   (val($column,'type')==='int'	  || 
			val($column,'type')==='autoincrement'	|| 
			val($column,'type')==='foreign_key')){$defaultOrderField='C'.($nb+1);}

		$column['nb']=$nb;
		$column['sqlExpr']=$mysqlField->orgname==='' ? '('.$mysqlField->name.')' : 
			                                           '`'.$mysqlField->orgtable.'`.`'.$mysqlField->orgname.'`';
		$column['sql']=$mysqlField;
		$title='C'.($nb+1);
		if(preg_match('@^[a-zA-Z0-9_]{1,20}$@',$mysqlField->name)){$title=$mysqlField->name;}
		$column['title']=$title;
		$columns['C'.($nb+1)]=$column;
	}
	if($defaultOrderField===false){$defaultOrderField=dlib_first_key($columns);}

	return [$columns,$defaultOrderField];
}

//! Render an html table for an sql query or a list of DBObjects.
//! This should not be called directly. Use table_render_query(), dbobject_ui_list() or dbtable_ui_list()
//!
//! $columns: (name=>$column) For DBObject table this is simply $class::getSchema()
//! $column:
//!  * title    : 
//!	 * type		: 'int', 'bool', 'email', ... check 'case' statement below (default is 'longtext')
//!	 * head-attr:
//!	 * sql		: field description from mysqli_fetch_field
//!	 * nb		: column number (starts by 0)
//!	 * class	: class name if this column is a reference to a DBObject
//!	 * values	: for 'enum'
//!	 * show		: callback function for custom rendering
//!
//! $config:
//!  * noSort      :	
//!  * noRotate    :	
//!  * orderBy	   :	
//!  * orderDir	   :	
//!  * columns     : callback to change columns
//!  * where       : (only for $type=='dbobject'): extra sql after FROM, before ORDER (for example WHERE clause)
//!  * line        : callback to customize whole line.
//!  * top_text    :	
//!  * bottom_text :	
function table_render($type,$columns,$queryDesc,$config=[],$defaultOrderField=false,$actionUrl=false)
{
	global $currentPage,$base_url,$dbobject_ui_config;

	// Table label: a prefix for get arguments, needed when there are several tables displayed on a same page.
	static $tableCount=0;
	$tableCount++;
	$tableLabel=val($config,'tableLabel',$tableCount===1 ? '' : 'table'.$tableCount);

	require_once 'filter-xss.php';

	// **** css / js
	$currentPage->addCss('dlib/dbobject-ui.css');
	$currentPage->addJs ('lib/jquery.js');
	$currentPage->addJs ('dlib/dbobject-ui.js');
	$currentPage->addJs ('dlib/table-tools.js');
	$currentPage->addCss('dlib/table-tools.css');

	$fields=array_keys($columns);

	if($actionUrl!==false)
	{
		$start=dlib_first_key($columns);
		$columns=dlib_array_insert_assoc($columns,$start,'delete',['title'=>'','type'=>'delete','head-attr'=>['class'=>['no-sort action-head no-column-hover']]]);
		$columns=dlib_array_insert_assoc($columns,$start,'view'  ,['title'=>'','type'=>'view'  ,'head-attr'=>['class'=>['no-sort action-head no-column-hover']]]);
		$columns=dlib_array_insert_assoc($columns,$start,'edit'  ,['title'=>'','type'=>'edit'  ,'head-attr'=>['class'=>['no-sort action-head no-column-hover']]]);
	}

	if(isset($config['columns'])){$config['columns']($columns);}
	foreach($columns as $name=>&$column){if(!isset($column['sqlExpr'])){$column['sqlExpr']='`'.$name.'`';}}
	unset($column);

	// **** determine sort order
	$isSortable=!val($config,'noSort');
	$orderBy   = val($config,'orderBy',$defaultOrderField);
	$orderDir  = val($config,'orderDir','DESC');
	$orderSql  = val($config,'orderSql');
	if(isset($_GET[$tableLabel.'orderAsc']) && $isSortable)
	{
		$orderBy=$_GET[$tableLabel.'orderAsc'];
		if(array_search($orderBy,$fields)===false){dlib_bad_request_400('invalid orderBy');}
		$orderDir='ASC';
	}
	if(isset($_GET[$tableLabel.'orderDesc']) && $isSortable)
	{
		$orderBy=$_GET[$tableLabel.'orderDesc'];
		if(array_search($orderBy,$fields)===false){dlib_bad_request_400('invalid orderBy');}
		$orderDir='DESC';
	}
	if(array_search($orderBy,$fields)===false){dlib_bad_request_400('orderby: unknown field');}
	// $fieldName can be C1, C2... translate to actual sql full quoted column name or expression
	$orderBy=$columns[$orderBy]['sqlExpr'];

	// **** search

	$useSearch=false;
	$searchQuery='';
	$searchArgs=[];
	$searchTerms='';
	if(val($config,'search_enable',true) && !isset($config['where']))
	{
		$useSearch=true;
		$searchTerms=val($_GET,$tableLabel.'search','');
		if($searchTerms!=='')
		{
			$searchQuery='';
			$searchArgs=val($queryDesc,'args',[]);
			$searchFields=$fields;
			if(isset($config['search_fields_alter'])){$config['search_fields_alter']($searchFields);}
			foreach($searchFields as $fieldName)
			{
				$field=$fieldName;
				$col=val($columns,$fieldName);
				// $fieldName can be C1, C2... translate to actual sql full quoted column name or expression
				if($col!==false){$field=$col['sqlExpr'];}
				// Search enum values, not numbers
				if($col!==false && $col['type']==='enum')
				{
					$field='(CASE '.$field.' ';
					foreach($col['values'] as $nb=>$val){$field.=' WHEN '.$nb." THEN '".db_escape_string($val)."' ";}
					$field.=' END)';
				}
				$searchQuery.=$field." LIKE '%%%s%%' OR ";
				$searchArgs[]=$searchTerms;
			}
			$concat=val($config,'search_concat_where_and','WHERE');
			$searchQuery=' '.$concat.' '.substr($searchQuery,0,-strlen(' OR ')).' ';
		}
	}

	// **** build display for table head
	$head=[];
	$hasRotate=false;
	foreach($columns as $name=>$column)
	{
		$title=val($column,'title',$name);
		$head[$name]=['disp'=>'<div><span>'.ent($title).'</span></div>',
					  'attr'=>val($column,'head-attr',[]),
					  ];
		if(array_search($name,$fields)!==false){$head[$name]['attr']['data-name']=$name;}
		$head[$name]['class'][]='col-'.$name;
		//if(mb_strlen($title)>10){$head[$name]['attr']['class'][]='rotate';$hasRotate=max(mb_strlen($title),(int)$hasRotate);}
		if(mb_strlen($title)>5 && !val($config,'noRotate')){$head[$name]['attr']['class'][]='auto-rotate';}
	} 

	$csvExport=isset($_GET['csvExport']);

	// **** fetch $rows from database (objects or query result lines)
	$pagerOptions=['defaultItemsPerPage'=>100,'prefix'=>$tableLabel];
	foreach($config as $k=>$v){if(preg_match('@^pager_(.*)$@',$k,$m)){$pagerOptions[$m[1]]=$v;}}
	$pager=new Pager($pagerOptions);
	$qargs=val($queryDesc,'args',[]);
	$qargs=array_merge($qargs,$searchArgs);
	switch($type)
	{
	case 'query':
	case 'dbtable':
		$sql=preg_replace('@^SELECT @','SELECT SQL_CALC_FOUND_ROWS ',$queryDesc['query']).' '.
			$searchQuery.
			($orderSql===false ? 'ORDER BY '.$orderBy.' '.$orderDir.' ' : $orderSql.' ').
			($csvExport ? '' : $pager->sql());
		if($csvExport){table_render_csv($sql,$qargs);die();}
		$qres=call_user_func_array('db_query',array_merge([$sql],$qargs));
		$rows=[];
		while($row=mysqli_fetch_row($qres)){$rows[]=$row;}
		mysqli_free_result($qres);
		break;
	case 'dbobject':
		$class=$queryDesc['class'];
		$sql='SELECT SQL_CALC_FOUND_ROWS `'.$class.'`.`'.implode('`,`'.$class.'`.`',$fields).'`  '.
			'FROM `'.$class.'` '.
			val($config,'where','').' '.
			$searchQuery.
			($orderSql===false ? 'ORDER BY '.$orderBy.' '.$orderDir.' ' : $orderSql.' ').
			($csvExport ? '' : $pager->sql());
		if($csvExport){table_render_csv($sql,$qargs);die();}
		$rows=call_user_func_array($class.'::fetchList',
								   array_merge([$sql],$qargs));
		break;
	}
	$pager->foundRows();
	$hasPager=$pager->nbItems>$pager->itemsPerPage;

	// last table head shouldn't be rotated (displays outside of page). FIXME this is done in js now
	$l=dlib_last_key($head);
	if(isset($head[$l]['attr']) && isset($head[$l]['attr']['class']))
	{$head[$l]['attr']['class']=array_filter($head[$l]['attr']['class'],function($v){return $v!='rotate';});}

	// **** build display for each object
	$lines=[];
	$lineAttrs=[];
	$now=time();
	foreach($rows as $lineNb=>$row) 
	{
		$line=[];
		// tr attributes
		$lineAttrs[$lineNb]=[];
		if($actionUrl!==false){$lineAttrs[$lineNb]['data-view-field']=$actionUrl($row,'view-field');}
		$d='';
		// ** build display for each field in this object
		foreach($columns as $name=>$column)
		{ 
			$v=false;
			switch($type)
			{
			case 'dbtable':
			case 'query':
				if(isset($column['nb'])){$v=val($row,$column['nb']);}
				break;
			case 'dbobject':
				$v=oval($row,$name);
				break;
			}
			
			$attr=[];
			$attr['class'][]='col-'.$name;
			switch(val($column,'type','custom'))
			{
			case 'edit':
			case 'view':
			case 'delete':
				$action=$column['type'];
				$d='<a href="'.ent($actionUrl($row,$action)).'">'.
					'<img alt="'.$action.'" src="'.ent($base_url).'/dlib/'.$action.'.png"/></a>';
				$attr['class'][]='col--'.$action;
				break;
			case 'autoincrement': 
			case 'foreign_key': 
				$attr['class'][]='right';
				if($v==0){$d='0';}
				else
				{
					if(!isset($column['class'])){$d=$v;}
					else
					{
						$oclass=$column['class'];
						$oClassInfo=$dbobject_ui_config['classes'][$oclass];
						$d='<a href="'.ent(val($oClassInfo,'main_url',$base_url.'/backend/'.$oClassInfo['urlClass'])).'/'.
							intval($v).'">'.intval($v).'</a>';
					}
				}
				break;
			case 'enum': 
				// $v===false for NULL (example: left joins)
				$d=$v===false ? '' : ent($column['values'][$v]);
				break;
			case 'int': 
				$attr['class'][]='right';
				$d=$v;
				break;
			case 'bool': 
				$d=$v ? 'true' : 'false';
				break;
			case 'timestamp': 
				$attr['data-sortable']=(int)$v;
				$d=$v==0 ? '-' : ent(date('d/m/Y',$v).(abs($v-$now)<3600*24*10 ? date(' G:i',$v) : '' ));
				break;
			case 'email': 
				$attr['class'][]='longtext';
				$d='<a href="mailto:'.ent($v).'">'.ent(mb_substr($v,0,300)).'</a>';
				break;
			case 'url': 
				$attr['class'][]='longtext';
				$d='<a href="'.filter_xss_check_url($v).'">'.ent(mb_substr($v,0,300)).'</a>';
				break;
			case 'array': 
				$d='['.count($v).']';
				break;
			default:
				$attr['class'][]='longtext';
				$d=ent(mb_substr($v,0,300));
			}
			$line[$name]=['disp'=>$d,'attr'=>$attr];
			if(isset($column['show'])){$column['show']($row,$line[$name]);}
		}
		// call customization hook if requested
		if(isset($config['line'])){$config['line']($row,$line,$lineAttrs[$lineNb],$columns);}
		$lines[$lineNb]=$line;
	}

	$tableTitle=false;
	if($type==='dbobject'){$tableTitle=$queryDesc['class'];}
	if($type==='dbtable')
	{
		foreach($columns as $col){if(isset($col['sql'])){$tableTitle=$col['sql']->table;break;}}
	}
	require_once 'dlib/template.php';
	return template_render('table-render.tpl.php',
						   [compact('tableLabel','type','pager','isSortable','hasPager','hasRotate','head',
									'lines','lineAttrs','config','searchTerms','useSearch','tableTitle')]);
}

function table_render_csv($sql,$qargs)
{
	header('Content-Encoding: utf-8');
	header("Content-Type: text/csv; charset=utf-8");
	header("Content-Disposition: attachment;filename=output.csv");
	$out=fopen('php://output','w');

	$qres=call_user_func_array('db_query',array_merge([$sql],$qargs));
	while($row=mysqli_fetch_row($qres))
	{
		fputcsv($out,$row,';');		
	}
	die('');
}

?>