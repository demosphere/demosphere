<?php

/** @defgroup variable variable
 * Persistent variables identified by a arbitrary name and type.
 *
 * This system provides a simple solution to store values, without
 * having to create and manage a separate database table.
 * Values are identified by a name and an optional type.
 * This system is only meant for small data. 
 * For larger data you should use a separate table.
 *
 * Values are stored serialized.
 *
 * You can choose anything for "type", just make sure some other
 * code doesn't use that same "type". 
 * If you are storing a single value, then you can leave "type" empty.
 * (make sure that "name" isn't used somewhere else).
 *
 * **Module use**: (none)
 *  @{
 */


function variable_get(string $name,string $type='',$default=false)
{
	global $dlib_config;
	$res=db_result_check("SELECT value FROM variables WHERE name='%s' AND type='%s'",
						   $name,$type); 
	if($res===false){return $default;}
	else
	{
		return unserialize($res,
						   // security
						   ['allowed_classes'=>$dlib_config['variable_unserialize_allowed_classes'] ?? ['stdClass']]);
	}
}

function variable_get_all(string $type)
{
	global $dlib_config;
	$res=db_one_col("SELECT name,value FROM variables WHERE type='%s'",$type); 
	foreach(array_keys($res) as $name)
	{
		$res[$name]=unserialize($res[$name],
								// security
								['allowed_classes'=>$dlib_config['variable_unserialize_allowed_classes'] ?? ['stdClass']]);
	}
	return $res;
}

function variable_set(string $name,string $type,$value)
{
	db_query("REPLACE INTO variables (name, type,value) VALUES ('%s', '%s','%x')",$name,$type,$value);
}

function variable_delete($name,string $type='')
{
	if($name!==false)
	{
		db_query("DELETE FROM variables WHERE name='%s' AND type ='%s'", 
				 $name,$type);
	}
	else
	{
		db_query("DELETE FROM variables WHERE type ='%s'",$type);
	}
}

function variable_install()
{
	db_query("DROP TABLE IF EXISTS variables");
	db_query("CREATE TABLE variables (
  `name` varchar(96)  NOT NULL,
  `type` varchar(96)  NOT NULL,
  `value` longblob NOT NULL,
  PRIMARY KEY  (`name`,`type`),
  KEY `name` (`name`),
  KEY `type` (`type`)
) DEFAULT CHARSET=utf8mb4");

}

/** @} */

?>