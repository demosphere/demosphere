<?php

// functions  in this file  are just  switches to  localized (language
// specific) versions.

// Parses a  string for  all occurences of  date/times and  returns an
// array of timestamps.   When time in timestamp is  3h33 it means "no
// time was found"
function find_dates_and_times($src,$verbose=false,$fullResult=false)
{
	static $locale=false;
	if($locale===false || isset($GLOBALS['dlib_reset_locale']))
	{$locale=find_dates_and_times_get_locale();}
	$fct=__FUNCTION__.'_'.$locale;
	return $fct($src,$verbose,$fullResult);
}

// like find_dates_and_times but parses a single date/time
function find_date_and_time($src,$verbose=false)
{
	static $locale=false;
	if($locale===false || isset($GLOBALS['dlib_reset_locale']))
	{$locale=find_dates_and_times_get_locale();}
	$fct=__FUNCTION__.'_'.$locale;
	return $fct($src,$verbose);
}

function find_times($src,$format='h',$verbose=false)
{
	static $locale=false;
	if($locale===false || isset($GLOBALS['dlib_reset_locale']))
	{$locale=find_dates_and_times_get_locale();}
	$fct=__FUNCTION__.'_'.$locale;
	return $fct($src,$format,$verbose);
}


// day of week that starts on monday ... not sunday :-(
function day_of_week($ts=0){return (date('w',$ts ? $ts : time())+6)%7;}

//! guess year if it is missing (choose year with closest date to now)
function find_date_time_guess_year($dayNum,$monthNum)
{
	$now=time();
	//if(is_string($dayNum)){echo "daynum:$dayNum<br/>";}
	$d1=abs($now-mktime(1,0,0,$monthNum,$dayNum,date('Y')-1));
	$d2=abs($now-mktime(1,0,0,$monthNum,$dayNum,date('Y')  ));
	$d3=abs($now-mktime(1,0,0,$monthNum,$dayNum,date('Y')+1));
	//echo "guess year $d1 $d2 $d3 / ".($d2-$d1)." ".($d3-$d1)."\n";
	if($d1<$d2 && $d1<$d3){$year=date('Y')-1;}
	else
	if($d2<$d1 && $d2<$d3){$year=date('Y')  ;}
	else                  {$year=date('Y')+1;}

	return $year;
}

function find_dates_and_times_get_locale()
{
	$res=dlib_get_locale_name();
	switch($res)
	{
	case 'fr': 
	case 'el':
	case 'en':
	case 'es':
	case 'pt':
	case 'gl':
	case 'ca':
	case 'de':
		require_once 'find-date-time-'.$res.'.php';
		break;
	default: fatal("find_dates_and_times_get_locale: unsupported locale:$res");
	}
	return $res;
}

?>