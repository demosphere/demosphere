<?php

function find_dates_and_times_it($src,$verbose=false,$fullResult=false)
{
	static $monthIndex,$weekDayIndex;
	static $re=false;
	if($verbose){$re=false;}
	if($verbose){echo "find_dates_and_times_it\n";}
	
	// FIXME: this messes up positions for $fullResult :-( 
	// Quick fix: also call it on the text you will change.
	$src=dlib_html_entities_decode_text($src,true);
	// PREG bug for û ??
	$src=str_replace(['août','aoû','AOÛT','AOÛ'],
					 ['aout ','aou ','AOUT ','AOU '],$src);
	if(strlen($src)==0){return [];}

	// all of this is static / constant, only compute it once
	if($re===false)
	{
		list($monthList,$weekDayList)=date_name_elements_it();

		// create index for finding month-number 
		// from unaccented lowercase monthname
		$monthIndex=[];
		foreach($monthList as $m=>$n)
		{
			$monthIndex[dlib_remove_accents(mb_strtolower(str_replace('.','',$m),'UTF-8'))]=$n;
		}

		// create index for finding weekDay number
		// from unaccented lowercase weekday name
		$weekDayIndex=[];
		foreach($weekDayList as $m=>$n)
		{
			$weekDayIndex[dlib_remove_accents(mb_strtolower(str_replace('.','',$m),'UTF-8'))]=$n;
		}


		/******************** helper regular expressions *******************/
		
		// month RE
		$allMonths=array_keys($monthList);
		$allMonths=array_merge($allMonths,array_map('dlib_remove_accents',$allMonths));
		$allMonths=str_replace('.','',$allMonths);
		rsort($allMonths);// longer matches have to come first!!
		$reAnyMonth='(?P<month>'.implode('|',array_unique($allMonths)).')(\.|\b)';

		// weekday RE
		$allWeekDays=array_keys($weekDayList);
		$allWeekDays=str_replace('.','',$allWeekDays);
		rsort($allWeekDays);// longer matches have to come first!!
		$reAnyWeekday='('.implode('|',array_unique($allWeekDays)).')(\.|\b)';
		
		// note: <cite> is used for pagewatch diff highlight
		$spaces='(['." ".// this last string is not a space, it is a UTF8 nbsp
			',\t[:space:]]|</?(br|strong|b|em|span|font|cite)[^>]*/?>)*';
		$timeRE='\b(?P<hour1>[01][0-9]|2[0-3]|[1-9])'.
			'([[:space:] ]*(heures|heure|h)[[:space:] ]*|:)'.
			'(?P<minutes>[0-5][0-9]|)';
		$yearRE='(?P<year>\b20[0-9][0-9]\b|\b[012][0-9]\b(?!([[:space:] ]*(h|:[[:space:] ]*[0-9][0-9]|-[[:space:] ]*[0-9]))))';
		$yearRE2=str_replace('<year>','<year2>',$yearRE);

		// huge regular expression for matching dates and times
		$re="@".
			'(?![  \n\t<>,])'.// do not begin by whitespace (strange?... otherwise crashes)
			"((?P<weekday>\b$reAnyWeekday)$spaces)?".
			// alternative dates: 
			// alternative date 1: daynum month year
			"(".
			"(?<![0-9])(?P<daynum>[0-2][0-9]|3[01]|[1-9]|1er|premier)$spaces".
			"\b$reAnyMonth$spaces".
			"$yearRE?".
			"|".
			// alternative date 2: d/m/Y 
			"((?<![0-9/])(?P<daynum2>[0-2][0-9]|3[01]|[1-9])/(?P<month2>0[1-9]|1[012]|[1-9])((/".$yearRE2.")?))(?![0-9/])".
			")".$spaces.
			"(".
			// text before time
			"([àa]$spaces"."partir de$spaces|[àa]$spaces|dès$spaces|des$spaces|de$spaces|$spaces-$spaces"."de$spaces|-$spaces)?".
			'(?P<time>'.$timeRE.')?'.
			")".
			"@iu";
		if($verbose){echo "re:$re\n";}
	}

	$bigMatchRes=preg_match_all($re,$src,$pregMatches,
								($fullResult ? 
								 PREG_OFFSET_CAPTURE : 
								 PREG_PATTERN_ORDER));
	//echo $re."\n";
	if($verbose){echo "preg_match_all return value:";var_dump($bigMatchRes);}
	if(!$bigMatchRes){return [];}

	// if full result  (with pos), then rebuild an  array like the one
	// without full result
	if($fullResult)
	{
		$matches=[];
		foreach($pregMatches as $key => $val)
		{
			foreach($val as $key1 => $val1)
			{
				if($val1===''){$matches[$key][$key1]='';}
				else{$matches[$key][$key1]=$val1[0];}
			}
		}
	}
	else
	{
		$matches=&$pregMatches;
	}
	
	if($verbose)
	{
		foreach($matches as $k=>$ma){$m=$ma[0];if($m!==''){echo "$k : \"$m\"\n";}}
	}
	
	$timestamps=[];
	// compute a timestamp for each matching date and time.
	foreach($matches[0] as $nb => $match)
	{
		$weekday=$matches['weekday'][$nb];
		$daynum =$matches['daynum' ][$nb].$matches['daynum2' ][$nb];
		$month  =$matches['month'  ][$nb].$matches['month2'  ][$nb];
		$year   =$matches['year'   ][$nb].$matches['year2'   ][$nb];
		$hour   =$matches['hour1'  ][$nb];
		$minute =$matches['minutes'][$nb];

		if($verbose)
		{
			echo '$weekday:'.		 $weekday."\n"; 
			echo '$daynum :'.		 $daynum ."\n"; 
			echo '$month  :'.		 $month	 ."\n";		
			echo '$year	  :'.		 $year	 ."\n"; 
			echo '$hour	  :'.		 $hour	 ."\n"; 
			echo '$minute :'.		 $minute ."\n"; 
			echo "****************\n";
			echo "match  : $match\n";
		}
		if(mb_strtolower($daynum)=='1er' || mb_strtolower($daynum)=='premier'){$daynum=1;}

		if($daynum < 0 || $daynum>31)
		{
			if($verbose)echo "bad daynum:$daynum\n";
			continue;
		}

		if(strlen($year)===4 && $year<1971)
		{
			if($verbose)echo "Ignore very old dates\n";
			continue;			
		}

		if($year>=2038)
		{
			if($verbose)echo "Ignore dates 2038 or later.\n"; // FIXME tmp year 2038 workaround. (4 bytes overflow)
			continue;			
		}

		// compute month number from month name 
		$monthNum=0;
		if(preg_match('@^[0-9]+$@',$month)){$monthNum=$month;}
		else
		{
			$ualcMonth=dlib_remove_accents(mb_strtolower(str_replace('.','',$month),'UTF-8'));
			if(!isset($monthIndex[$ualcMonth])){fatal("bad month: '$month' !\n");}
			$monthNum=$monthIndex[$ualcMonth];
		}
		if($monthNum <= 0 || $monthNum>12){fatal("bad monthNum:$monthNum\n");}

		// compute weekday number from weekday name 
		$ualcWeekDay=dlib_remove_accents(mb_strtolower(str_replace('.','',$weekday),'UTF-8'));
		$weekdayNum=val($weekDayIndex,$ualcWeekDay,false);

		// guess year if it is missing (choose year with closest date to now)
		if($year==''){$year=find_date_time_guess_year($daynum,$monthNum);}
		// determine time
		if($minute===""){$minute=0;}
		if($hour  ===""){$hour=3;$minute=33;}

		if(!checkdate($monthNum,$daynum,$year))
		{
			if($verbose)echo "rejected $year-$monthNum-$daynum : invalid gregorian date (checkdate)\n";
			continue;
		}

		// now build the timestamp
		$ts=mktime($hour,$minute,0,$monthNum,$daynum,$year);
		if($verbose)echo "res: ".date('r',$ts)."  ($ts)\n";

		// if weekday is available, we can check if day is ok
		if($ts==0)
		{
			if($verbose)echo "bad date/time!\n";
		}
		else
		if($weekdayNum!==false && day_of_week($ts)!=($weekdayNum-1))
		{
			if($verbose)echo "WEEKDAY check REJECTED!\n";
		}
		else
		{
			if($fullResult)
			{
				$timestamps[]=[$ts,
							   $pregMatches[0][$nb][0],
							   $pregMatches[0][$nb][1]];
			}
			else
			{$timestamps[]=$ts;}
		}
	}
	return $timestamps;
}

function find_date_and_time_it($src,$verbose=false)
{
	$timestamps=find_dates_and_times($src,$verbose);
	if(count($timestamps)!=1){return false;}
	return $timestamps[0];
}


function find_times_it($src,$format='h',$verbose=false)
{
	$timeRE='\b([01][0-9]|2[0-3]|[1-9])([ ]*(heures|heure|h)[ ]*|:)([0-9][0-9]|)\b';
	if(!preg_match_all('@'.$timeRE.'@is',$src,$matches)){return [];}
	if($verbose)print_r($matches);
	$times=[];
	// compute a time for each matching date and time.
	foreach($matches[0] as $nb => $match)
	{
		$hour   =$matches[1][$nb];
		$minute =$matches[4][$nb];
		if(preg_match('@[^0-9]@',$hour)   ||
		   preg_match('@[^0-9]@',$minute) ||
		   $hour   < 0 || $hour  >24 || 
		   $minute < 0 || $minute>59)
		{
			$ts='invalid';
		}
		else
		{
			switch($format)
			{
			case 'h': $ts=intval($hour).'h'.
						  ($minute!=0 ? sprintf('%02d',$minute) : '');
			case ':': $ts=sprintf('%02d:%02d',$hour,$minute);break;
			default:  fatal("invalid time format requested");
			}
		}
		$times[]=$ts;
	}
	return $times;
}

// a list of different representations of month names and weekday names
function date_name_elements_it()
{
	static $res=false;
	if($res!==false){return $res;}

	/******************** months ************************/
	$months=['janvier'  =>1,
			 'février'  =>2,
			 'mars'     =>3,
			 'avril'    =>4,
			 'mai'      =>5,
			 'juin'     =>6,
			 'juillet'  =>7,
			 'aout'     =>8,
			 'septembre'=>9,
			 'octobre'  =>10,
			 'novembre' =>11,
			 'décembre' =>12,
			 'jan.'     =>1,
			 'fév.'	 =>2,
			 'mar.'	 =>3,
			 'avr.'	 =>4,
			 'jun.'	 =>6,
			 'jui.'	 =>7,
			 'aou.'	 =>8,
			 'sep.'	 =>9,
			 'oct.'	 =>10,
			 'nov.'	 =>11,
			 'déc.'	 =>12,
			 'janv.'    =>1,
			 'févr.'    =>2,
			 'juil.'    =>7,
			 'sept.'    =>9,
			 'oct.'     =>10,
			 'nov.'     =>11,
			 'déc.'     =>12,
			 // case insens. regexp fails for accents :-(
			 'FÉVRIER' =>2,
			 'AOÛT'    =>8,
			 'AOÛ'     =>8,
			 'DÉCEMBRE'=>12,
			 'FÉV'     =>2,
			 'DÉC'     =>12,
			];

	/******************** weeks ************************/

	$weekDays=[
		'lundi'    => 1,
		'mardi'    => 2,
		'mercredi' => 3,
		'jeudi'    => 4,
		'vendredi' => 5,
		'samedi'   => 6,
		'dimanche' => 7,
		'lun.'     => 1,
		'mar.'     => 2,
		'mer.'     => 3,
		'jeu.'     => 4,
		'ven.'     => 5,
		'sam.'     => 6,
		'dim.'     => 7,
			  ];
					
	$res=[$months,$weekDays];
	return $res;
}


?>