<?php

function find_dates_and_times_de($src,$verbose=false,$fullResult=false)
{
	static $monthIndex,$weekDayIndex;
	static $re=false;
	static $spaces=false;
	if($verbose){$re=false;}
	if($verbose){echo "find_dates_and_times_de\n";}
	
	// FIXME: this messes up positions for $fullResult :-( 
	// Quick fix: also call it on the text you will change.
	$src=dlib_html_entities_decode_text($src,true);
	if(strlen($src)==0){return [];}

	// all of this is static / constant, only compute it once
	if($re===false)
	{
		list($monthList,$weekDayList)=date_name_elements_de();

		// create index for finding month-number 
		// from unaccented lowercase monthname
		$monthIndex=[];
		foreach($monthList as $m=>$n)
		{
			$monthIndex[dlib_remove_accents(mb_strtolower(str_replace('.','',$m),'UTF-8'))]=$n;
		}

		// create index for finding weekDay number
		// from unaccented lowercase weekday name
		$weekDayIndex=[];
		foreach($weekDayList as $m=>$n)
		{
			$weekDayIndex[dlib_remove_accents(mb_strtolower(str_replace('.','',$m),'UTF-8'))]=$n;
		}


		/******************** helper regular expressions *******************/
		
		// month RE
		$allMonths=array_keys($monthList);
		$allMonths=array_merge($allMonths,array_map('dlib_remove_accents',$allMonths));
		$allMonths=str_replace('.','',$allMonths);
		rsort($allMonths);// longer matches have to come first!!
		$reAnyMonth='(?P<month>'.implode('|',array_unique($allMonths)).')(\.|\b)';

		// weekday RE
		$allWeekDays=array_keys($weekDayList);
		$allWeekDays=str_replace('.','',$allWeekDays);
		rsort($allWeekDays);// longer matches have to come first!!
		$reAnyWeekday='('.implode('|',array_unique($allWeekDays)).')(\.|\b)';
		
		// note: <cite> is used for pagewatch diff highlight
		$spaces='(['." ".// this last string is not a space, it is a UTF8 nbsp
			',\t[:space:]]|</?(br|strong|b|em|span|font|cite)[^>]*/?>)*';
		$daynumRE="(?<![0-9])(?P<daynum>[0-2][0-9]|3[01]|[1-9])";
		$daynumRE0=str_replace('<daynum>','<daynumrange0>',$daynumRE);;
		$timeRE='\b((?P<hour1>[01][0-9]|2[0-3]|[1-9])'.
			'([[:space:] ]*(Uhr|h|\.)[[:space:] ]*|:)'.
			'(?P<minutes>[0-5][0-9]\b|)|'.
			'(?P<hour2>mittag\b))';
		$yearRE='(?P<year>\b(20|19|18|17)[0-9][0-9]\b|\b[012][0-9]\b(?!(é|è|[[:space:] ]*(h|:[[:space:] ]*[0-9][0-9]|[–-][[:space:] ]*[0-9]))))';
		$yearRE2 =str_replace('<year>','<year2>' ,$yearRE);
		$yearRE2b=str_replace('<year>','<year2b>',$yearRE);

		// huge regular expression for matching dates and times
		$re="@".
			'(?![  \n\t<>,])'.// do not begin by whitespace (strange?... otherwise crashes)
			"((?P<weekday>\b$reAnyWeekday)$spaces,?(den)?$spaces)?".
			// alternative dates: 
			// alternative date 1: daynum month year
			"(".
			"(?P<daynumrange>$daynumRE0$spaces(bis|und)$spaces)?".
			"$daynumRE\.?$spaces".
			"\b$reAnyMonth$spaces".
			"$yearRE?".
			"|".
			// alternative date 2: d.m.Y 
			"((?<![0-9.])(?P<daynum2>[0-2][0-9]|3[01]|[1-9])\.(?P<month2>0[1-9]|1[012]|[1-9])(?![a-z_-])((\.".$yearRE2.")?))(?![0-9]|\. *[0-9])".
			"|".
			// alternative date 2b: d-m-Y 
			"((?<![0-9–-])(?P<daynum2b>[0-2][0-9]|3[01]|[1-9])[-–](?P<month2b>0[1-9]|1[012]|[1-9])(?![a-z_])(([-–]".$yearRE2b.")?))(?![0-9-–])".
			"|".
			// alternative date 3: YYYY-MM-DD (computer) 
			"((?<![0-9–-])(?P<year3>(20|19)[0-9][0-9]) ?[-–] ?(?P<month3>0[0-9]|1[012]) ?[-–] ?(?P<daynum3>[0-2][0-9]|3[01])(?![0-9–-]))".
			")".
			"(".$spaces.
			// text before time
			"(Um$spaces|ab$spaces|Beginn$spaces|".'[-–]'."$spaces"."de$spaces|".'[-–]'."$spaces)?".
			'(?P<time>'.$timeRE.')'.
			")?".
			"@iuS";
		if($verbose){echo "re:$re\n";}
	}

	$bigMatchRes=preg_match_all($re,$src,$pregMatches,
								($fullResult ? 
								 PREG_OFFSET_CAPTURE : 
								 PREG_PATTERN_ORDER));
	//echo $re."\n";
	if($verbose){echo "preg_match_all return value:";var_dump($bigMatchRes);}
	if(!$bigMatchRes){return [];}

	// if full result  (with pos), then rebuild an  array like the one
	// without full result
	if($fullResult)
	{
		$matches=[];
		foreach($pregMatches as $key => $val)
		{
			foreach($val as $key1 => $val1)
			{
				if($val1===''){$matches[$key][$key1]='';}
				else{$matches[$key][$key1]=$val1[0];}
			}
		}
	}
	else
	{
		$matches=&$pregMatches;
	}
	
	if($verbose)
	{
		foreach($matches as $k=>$ma){$m=$ma[0];if($m!==''){echo "$k : \"$m\"\n";}}
	}
	
	$timestamps=[];
	// compute a timestamp for each matching date and time.
	foreach($matches[0] as $nb => $match)
	{
		$weekday=$matches['weekday'][$nb];
		$daynum =$matches['daynum' ][$nb].$matches['daynum2' ][$nb].$matches['daynum2b'][$nb].$matches['daynum3' ][$nb];
		$daynumrange0=$matches['daynumrange0'][$nb];
		$month  =$matches['month'  ][$nb].$matches['month2'  ][$nb].$matches['month2b' ][$nb].$matches['month3'  ][$nb];
		$year   =$matches['year'   ][$nb].$matches['year2'   ][$nb].$matches['year2b'  ][$nb].$matches['year3'   ][$nb];
		$hour   =$matches['hour1'  ][$nb].$matches['hour2'   ][$nb];
		$minute =$matches['minutes'][$nb];

		if($verbose)
		{
			echo '$weekday:'.		 $weekday."\n"; 
			echo '$daynum :'.		 $daynum ."\n"; 
			echo '$daynumrange0 :'.	 $daynumrange0 ."\n"; 
			echo '$month  :'.		 $month	 ."\n";		
			echo '$year	  :'.		 $year	 ."\n"; 
			echo '$hour	  :'.		 $hour	 ."\n"; 
			echo '$minute :'.		 $minute ."\n"; 
			echo "****************\n";
			echo "match  : $match\n";
		}
		if(mb_strtolower($daynum)=='1er' || mb_strtolower($daynum)=='premier' ||
		   preg_match('@^1[^0-9]+@',$daynum)){$daynum=1;}

		if(mb_strtolower($daynumrange0)=='1er' || mb_strtolower($daynumrange0)=='premier' ||
		   preg_match('@^1[^0-9]+@',$daynumrange0)){$daynumrange0=1;}

		if($daynum <= 0 || $daynum>31)
		{
			if($verbose)echo "bad daynum:$daynum\n";
			continue;
		}

		if($daynumrange0 <= 0 || $daynumrange0>31)
		{
			if($verbose)echo "bad daynumrange0:$daynumrange0\n";
			$daynumrange0=false;
		}

		if(strlen($daynumrange0)==0){$daynumrange0=false;}

		if(strlen($year)===4 && $year<1971)
		{
			if($verbose)echo "Ignore very old dates\n";
			continue;			
		}

		if($year>=2038)
		{
			if($verbose)echo "Ignore dates 2038 or later.\n"; // FIXME tmp year 2038 workaround. (4 bytes overflow)
			continue;			
		}

		// FIXME: this is used in German. Check if it is very used or not. Try to find context.
		//if(preg_match('@^'.$spaces.'[0-9]+\.[0-9]+'.$spaces.'$@',$match))
		//{
		// 	if($verbose)echo "rejected num.num : this causes too many false positives\n";
		// 	continue;
		//}

		// compute month number from month name 
		$monthNum=0;
		if(preg_match('@^[0-9]+$@',$month)){$monthNum=$month;}
		else
		{
			$ualcMonth=dlib_remove_accents(mb_strtolower(str_replace('.','',$month),'UTF-8'));
			if(!isset($monthIndex[$ualcMonth])){fatal("bad month: '$month' !\n");}
			$monthNum=$monthIndex[$ualcMonth];
		}
		if($monthNum <= 0 || $monthNum>12){fatal("bad monthNum:$monthNum\n");}

		// compute weekday number from weekday name 
		$ualcWeekDay=dlib_remove_accents(mb_strtolower(str_replace('.','',$weekday),'UTF-8'));
		$weekdayNum=val($weekDayIndex,$ualcWeekDay,false);

		// guess year if it is missing (choose year with closest date to now)
		if($year==''){$year=find_date_time_guess_year($daynum,$monthNum);}
		// determine time
		if($minute===""    ){$minute=0;}
		if($hour  ===""    ){$hour= 3;$minute=33;}
		if(mb_strtolower($hour)  ==="mittag"){$hour=12;$minute=0;}

		if(!checkdate($monthNum,$daynum,$year))
		{
			if($verbose)echo "rejected $year-$monthNum-$daynum : invalid gregorian date (checkdate)\n";
			continue;
		}

		// now build the timestamp
		$ts=mktime($hour,$minute,0,$monthNum,$daynum,$year);
		if($verbose)echo "res: ".date('r',$ts)."  ($ts)\n";

		// if weekday is available, we can check if day is ok
		if($ts==0)
		{
			if($verbose)echo "bad date/time!\n";
		}
		else
		if($weekdayNum!==false && day_of_week($ts)!=($weekdayNum-1))
		{
			if($verbose)echo "WEEKDAY check REJECTED!\n";
		}
		else
		{
			if($daynumrange0===false)
			{
				if($fullResult)
				{
					$timestamps[]=[$ts,
								   $pregMatches[0][$nb][0],
								   $pregMatches[0][$nb][1]];
				}
				else
				{$timestamps[]=$ts;}
			}
			else
			{
				// special case for date ranges("Du 23 au 27 mars"):
				// Create two dates.
				$ts0=mktime($hour,$minute,0,$monthNum,$daynumrange0,$year);
				if($verbose)echo "daynumrange0: ".date('r',$ts0)."  ($ts0)\n";

				if($fullResult)
				{
					$timestamps[]=[$ts0,
								   $pregMatches['daynumrange0'][$nb][0],
								   $pregMatches['daynumrange0'][$nb][1]];
					$timestamps[]=[$ts,
								   $pregMatches['daynum'][$nb][0],
								   $pregMatches['daynum'][$nb][1]];
				}
				else
				{
					$timestamps[]=$ts0;
					$timestamps[]=$ts;
				}
				
			}
		}
	}
	return $timestamps;
}

function find_date_and_time_de($src,$verbose=false)
{
	$timestamps=find_dates_and_times($src,$verbose);
	if(count($timestamps)!=1){return false;}
	return $timestamps[0];
}


function find_times_de($src,$format='h',$verbose=false)
{
	$timeRE='\b('.
		// 20 uhr
		'(?P<hour1>[01][0-9]|2[0-3]|[1-9])([ ]*(uhr))|'.
		// 20:30 or 20.30
		'(?P<hour2>[01][0-9]|2[0-3]|[1-9])[:.](?P<minute2>[0-5][0-9])(?![ .]*[0-9])|'.
		// mittag
		'mittag'.
		')\b';
	if(!preg_match_all('@'.$timeRE.'@is',$src,$matches)){return [];}
	if($verbose)print_r($matches);
	$times=[];
	// compute a time for each matching date and time.
	foreach($matches[0] as $nb => $match)
	{
		$hour   =$matches['hour1'][$nb].$matches['hour2'][$nb];
		$minute =$matches['minute2'][$nb];
		if($matches[1][$nb]==='mittag'){$hour=12;$minute=0;}
		if(preg_match('@[^0-9]@',$hour)   ||
		   preg_match('@[^0-9]@',$minute) ||
		   $hour   < 0 || $hour  >24 || 
		   $minute < 0 || $minute>59)
		{
			$ts='invalid';
		}
		else
		{
			switch($format)
			{
			case 'h': $ts=intval($hour).'h'.
						  ($minute!=0 ? sprintf('%02d',$minute) : '');
			case ':': $ts=sprintf('%02d:%02d',$hour,$minute);break;
			default:  fatal("invalid time format requested");
			}
		}
		$times[]=$ts;
	}
	return $times;
}

// a list of different representations of month names and weekday names
function date_name_elements_de()
{
	static $res=false;
	if($res!==false){return $res;}

	/******************** months ************************/
	$months=['januar'  =>1,
			 'februar'  =>2,
			 'märz'     =>3,
			 'april'    =>4,
			 'mai'      =>5,
			 'juni'     =>6,
			 'juli'     =>7,
			 'august'   =>8,
			 'september'=>9,
			 'oktober'  =>10,
			 'november' =>11,
			 'dezember' =>12,
			 'jän.'     =>1,
			 'jan.'     =>1,
			 'feb.'	 =>2,
			 'mar.'	 =>4,
			 'apr.'	 =>4,
			 'jun.'	 =>6,
			 'jul.'	 =>7,
			 'aug.'	 =>8,
			 'sep.'	 =>9,
			 'sept.'	 =>9,
			 'okt.'	 =>10,
			 'nov.'	 =>11,
			 'dez.'	 =>12,
			 // case insens. regexp fails for accents :-(
			 'JÄN'  =>1,
			 'MÄRZ' =>3,
			 'MÄR'  =>3,
			];

	/******************** weeks ************************/

	$weekDays=[
		'Montag'    => 1,
		'Dienstag'  => 2,
		'Mittwoch'  => 3,
		'Donnerstag'=> 4,
		'Freitag'   => 5,
		'Samstag'   => 6,
		'Sonntag'   => 7,
		'mo.'      => 1,
		'di.'      => 2,
		'mi.'      => 3,
		'do.'      => 4,
		'fr.'      => 5,
		'sa.'      => 6,
		'so.'      => 7,
			  ];
					
	$res=[$months,$weekDays];
	return $res;
}


?>