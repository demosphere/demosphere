<?php

// ***************************************
// Cron helpers
// ***************************************


//! Call this before starting any cron tasks.
//! It checks for cron key.
//! It also sends an email if previous cron has failed.
function dlib_cron_setup($emailFrom,$emailTo,$emailBodyExtra='')
{
	global $base_url,$demosphere_config,$custom_config;

	// Cron output might (but shouldn't) contain dangerous html (XSS)
	if(headers_sent()){fatal('dlib_cron_setup: headers already sent. Cannot set text/plain');}
	header('Content-type: text/plain; charset=UTF-8');
	flush();// For some reason, without this flush, output is not done if some cron job fails with fatal()

	// Check permissions (must be run with: https://example.org/cron?cron_key=XYZ)
	$goodKey=variable_get('key','cron');
	if($goodKey===false){fatal('Cron key was not configured during install?!');}
	if(val($_GET,'cron_key')!==$goodKey){dlib_permission_denied_403('Invalid cron key. You need to specify a cron key: '.$base_url.'/cron?cron_key=XYZ');}

	// Check if previous cron is still running 
	// we need full path for register_shutdown_function():
	$cronFile=getcwd().'/files/private/cron-is-running';
	if(file_exists($cronFile))
	{
		if(filemtime($cronFile)<time()-3600*2.5)
		{
			echo date('r')." : Previous cron has been running for too long. This is strange. It probably aborted without shutdown ?! Starting new cron anyways.\n";
		}
		else
		{
			echo date('r')." : Previous cron is still running. Not starting new cron. Exiting.\n";
			exit(0);
		}
	}


	$last=variable_get('last','cron',0);

	// If previous cron failed and send an error email.
	if(variable_get('end_ok','cron')!==false &&
	   variable_get('end_ok','cron')<$last      )
	{
		echo date('r')." : PREVIOUS CRON EXITED PREMATURELY. It started at ".date('r',$last)."\n";

		require_once 'lib/class.phpmailer.php';
		$mail=new PHPMailer();
		$mail->CharSet="utf-8";
		$mail->SetFrom($emailFrom);
		$mail->AddAddress($emailTo);
		$mail->Subject='cron failed for '.$base_url;
		$mail->Body = "Previous cron exited prematurely.\n".
			"It started at ".date('r',$last)."\n".
			'Last successful cron ended at:'.date('r',variable_get('end_ok','cron')).".\n".
			$emailBodyExtra;
		$mail->Send();
	}

	variable_set('last','cron',time());

	// Use empty file to say that cron is running
	register_shutdown_function(function()use($cronFile){if(file_exists($cronFile)){unlink($cronFile);}});
	touch($cronFile);

}

//! Call this after all cron tasks have finished.
function dlib_cron_shutdown()
{
	// ******* output any messages
	if(isset($_SESSION['dlib_messages']))
	{
		foreach($_SESSION['dlib_messages'] as $type=>$messages)
		{
			foreach($messages as $message)
			{
				echo "MESSAGE[$type]: ".date('r').": ".$message."\n";
			}
		}
	}

	variable_set('end_ok','cron',time());
	echo date('r')." : ******************  FINISHED ALL CRON JOBS **********************\n";

	//file_put_contents("/tmp/dlib-cron-check",date('r')." : ".posix_getpid()." cron $base_url : last:$last now-last:".($now-$last)." :: FINISHED\n",FILE_APPEND);
}

/** Provide an easy to use function to program cron tasks at different intervals.
 * This function also does some logging.
 * @param a short name for this cron task.
 * @param a string or an array, examples: 'daily','weekly',['period'=>'daily','times'=>['5:00','13:00','21:00']]
 *        supported periods: delay,daily,weekly,monthly.
 * @return true if cron job should go ahead.
 */
function dlib_cron_can_start(string $name,$cron,$noDoS=true,$now=false)
{
	$lastCompleted=variable_get($name,'cron_last_completed',0);
	$lastStarted  =variable_get($name,'cron_last_started'  ,0);
	if($lastStarted>$lastCompleted)
	{
		dlib_message_add('cron for '.$name.' seems to have failed : '.
						   'lastStarted>lastCompleted :'.
						   date('r',$lastStarted).' > '.date('r',$lastCompleted),
						   'error');
	}
	$res=false;
	if($now===false){$now=time();}
	$today=mktime(0,0,0);

	if(!is_array($cron)){$cron=['period'=>$cron];}

	$next=false;
	switch($cron['period'])
	{
	case 'delay':
		$next=$lastCompleted+$cron['delay'];
		break;
	case 'daily':
		if(!isset($cron['times'])){$cron['times']=['3:00'];}
		foreach($cron['times'] as $time)
		{
			$t=strtotime($time,$lastCompleted);
			if($t>$lastCompleted){$next=$t;break;}
		}
		if($next===false){$next=strtotime('tomorrow '.$cron['times'][0],$lastCompleted);}
		break;
	case 'weekly':
		if(!isset($cron['time'   ])){$cron['time'   ]='3:00'  ;}
		if(!isset($cron['weekday'])){$cron['weekday']='sunday';}
		$next=strtotime($cron['weekday'].' '.$cron['time'],$lastCompleted);
		if($next<=$lastCompleted){$next=strtotime('+1 week',$next);}
		break;
	case 'monthly':
		if(!isset($cron['day'    ])){$cron['day' ]=1;}
		if(!isset($cron['time'   ])){$cron['time']='3:00';}
		$next=strtotime(date('F',$lastCompleted).' '.$cron['day'].' '.$cron['time']);
		if($next<=$lastCompleted){$next=strtotime('+1 month',$next);}
		break;
	default: fatal('invalid period');
	}
	$res=$now>=$next;
	echo date('r')." : cron_can_start for $name: ".($res?'yes':'no')."\n";
	if($res===false){return false;}

	//file_put_contents("/tmp/demosphere-".$name."-cron-check",date('r')." : ".posix_getpid()." $name cron :: START\n",FILE_APPEND);
	// no access control : avoid DoS attack, by calling too often
	if($noDoS && ($now-$lastStarted)<2700)
	{
		echo date('r')." : refusing to start cron $name, must wait at least 45min before previous\n";
		return false;
	}
	variable_set($name,'cron_last_started',$now);
	return true;
}

function dlib_cron_finished($name,$now=false)
{
	echo date('r')." : cron_finished for $name\n";
	if($now===false){$now=time();}
	//file_put_contents("/tmp/demosphere-".$name."-cron-check",date('r')." : ".posix_getpid()." $name cron :: FINISHED\n",FILE_APPEND);
	variable_set($name,'cron_last_completed',$now);
}


?>