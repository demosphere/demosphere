<?php


//! Replace image data-url's by urls of temporary locally served images: <img src="data:..." /> => <img src="http://..." />
//! Leaves data url's alone if they are shorter than $limit
//! This accepts (somewhat) malformed HTML and makes sure not to use regexps that would crash on huge data urls.
//! Large data urls will break lots of things, notably regexps. This should be called immediately after download.
//! If you set $dlib_config['data_url_image_dir'], this will be called automatically by dlib_download_html_page() 
//! This is not XSS proof. Output should be XSS filtered before display.
function dlib_html_data_url_remove($html,$limit=0)
{
	global $dlib_config,$base_url;
	if(strpos($html,'data:')===false){return $html;}
	
	for($imgPos=stripos($html,'<img');$imgPos!==false;$imgPos=stripos($html,'<img',$imgPos+1))
	{
		// determine end of <img...>
		$endImg=strpos($html,'>',$imgPos);
		if($endImg===false){$endImg=strlen($html);}
		$imgTag=substr($html,$imgPos,1+$endImg-$imgPos);

		// Parse <img..> tag and data: url into parts
		if(!preg_match('@src\s*=\s*[\'"]?(data):([^;]*);base64,(.)@is',substr($imgTag,0,2000),$matches,PREG_OFFSET_CAPTURE)){continue;}
		$dataUrl=substr($imgTag,$matches[1][1]);
		$mime=$matches[2][0];
		$base64Data=substr($imgTag,$matches[3][1]);
		$end=strpbrk($base64Data," \n\t\"'><");
		// very special malformed case
		if($end==='>' && $base64Data[strlen($base64Data)-2]==='/'){$base64Data.='>';$end=' />';}
		$base64Data=substr($base64Data,0,-strlen($end));
		if(strlen($base64Data)<$limit){continue;}

		$binData=base64_decode($base64Data);

		// Write decoded data into file and use ImageMagick convert to fix/check images
		$allowedExtensions=['jpeg','png','gif','webp'];
		require_once 'dlib/mail-and-mime.php';
		$extension=$allowedExtensions[dlib_mime_to_extension($mime)] ?? 'png';
		$tmpFname=tempnam($dlib_config['tmp_dir'],"data-url-");
		if($tmpFname!==false)
		{
			file_put_contents($tmpFname,$binData);	   
			$fileName=$dlib_config['data_url_image_dir'].'/'.substr(hash('sha256',$binData.':'.$dlib_config['secret']),0,20).'.'.$extension;
			exec("convert ".escapeshellarg($tmpFname)." ".escapeshellarg($fileName)." 2>&1",$cmdOutput,$ret);
		}
		if($tmpFname===false || $ret!==0){$fileName="dlib_html_data_url_remove-failed";}

		// Replace old <img ...> by new version.
		$newImageTag=substr($imgTag,0,$matches[1][1]).$base_url.'/'.$fileName.$end;
		$html=substr($html,0,$imgPos).$newImageTag.substr($html,$imgPos+strlen($imgTag));
	}
	return $html;
}

//! You should call this from cron
function dlib_html_data_url_cron()
{
	global $dlib_config;
	$tmpDir=$dlib_config['data_url_image_dir'];
	$old=time()-3600*24*60;// delete more than 60 days old
	foreach(scandir($tmpDir) as $dirEntry)
	{
		if(!preg_match('@^([0-9a-f]{1,20})\.[a-z]{1,5}$@',$dirEntry)){continue;}
		$mtime=filemtime($tmpDir.'/'.$dirEntry);
		if($mtime<$old)
		{
			unlink($tmpDir.'/'.$dirEntry);
		}
	}
}

//! Transform relative and absolute paths in html (img,a,link,css:@import url(...)) to full urls.
//! Note: this is meant for un-clean html. It is regexp based, not foolproof, but works pretty well in practice.
//!       The returned HTML  must be considered as potentially hostile (XSS).
//! FIXME: this fails on long data: urls.
//! @param $html
//! @param $url the url of this document: http://example.org/xyz/ex.html
function dlib_html_links_to_urls(string $html,string $url)
{
	//file_put_contents('/tmp/x1',$html);
	// $url='http://example.org/b/c/d/e/f'; => $dirurl=http://example.org/b/c/d/e
	$protocol=preg_match('@^https://@',$url) ? 'https' : 'http';
	$dirurl=$url;
	$ok=preg_match('@^(https?://)([^/]+)(/[^/]*)*@',$url,$matches);
	if(!$ok){return false;}
	if(isset($matches[3])){$dirurl=substr($url,0,-strlen($matches[3]));}
	if(preg_match('@<base(?=\s)[^>]*\bhref\s*=\s*["\']([^\'"]*)[\'"]@is',$html,$matches))
	{
		$dirurl=html_entity_decode($matches[1],ENT_QUOTES,'UTF-8');
	}
	$dirurl=preg_replace('@/$@','',$dirurl);
	$hosturl=dlib_host_url($dirurl);

	// *** fix missing quotes
	$html=preg_replace('@(<(a|link|img)(?=\s)[^>]*\s(src|href)\s*=\s*)([^"\'\s><]+)@is',
					   '$1"$4"',$html);
	if($html===null){return false;}

	// *** fix bad html links: spaces at begin/end of quotes
	$html=preg_replace('@(<(a|link|img)(?=\s)[^>]*\s(src|href)\s*=\s*["\'])\s+@is','$1',$html);
	$html=preg_replace('@(<(a|link|img)(?=\s)[^>]*\s(src|href)\s*=\s*["\'][^"\'><]*)\s+(["\'])@is','$1$2',$html);

	// *** Find and fix actual urls

	$urlEncode=function($m){return urlencode($m[0]);};
	// Fix a single url (regexp callback)
	$fixUrlFct=function($match)use($protocol,$dirurl,$hosturl,$urlEncode)
		{
			$url=$match['url'];
			$url=trim($url);
			// protocol relative url
			if(strpos($url,'//')===0){$url=$protocol.':'.$url;}
			// absolute path
			if(strpos($url,'/' )===0){$url=$hosturl.$url;}
			// relative path
			if(!preg_match('@^(https?|data|mailto):@i',$url)){$url=$dirurl.'/'.$url;}
			// Encode non-ascii (non-ascii is invalid, but is encountered. It can break after unicode normalize.)
			$url=preg_replace_callback('@[^\x00-\x7F]@u',$urlEncode,$url);

			return $match['begin'].$url.$match['end'];
		};
	$html=preg_replace_callback('@(?P<begin><(a|link)(?=\s)[^>]*\shref\s*=\s*["\'])(?P<url>[^"\'><]*)(?P<end>["\'])@is'                ,$fixUrlFct,$html);
	$html=preg_replace_callback('@(?P<begin><img(?=\s)[^>]*\ssrc\s*=\s*["\'])(?P<url>[^"\'><]*)(?P<end>["\'])@is'                      ,$fixUrlFct,$html);
	$html=preg_replace_callback('@(?P<begin><style(?=[\s>])[^<>]*>[^<>]*\@import\s+(url\()?[\s"\']*)(?P<url>[^)\s<>"\']+)(?P<end>.)@is',$fixUrlFct,$html);

	//file_put_contents('/tmp/x2',$html);
	return $html;
}


//! Guesses an html page's charset and converts the page to UTF-8.
//! Charset is guessed by using server response header, xml tag, meta charset declarations or heuristic encoding detection (in that order).
function dlib_html_convert_page_to_utf8(string $html,$httpContentType=false,$detectEnc='ascii,us-ascii,windows-1252,iso-8859-1,iso-8859-7,utf-8')
{
	// check server response headers - http content type  
	preg_match('@charset=(.*)$@',$httpContentType,$m);
	$ctypecharset=val($m,1,false);
	// Ignore invalid encoding
	// FIXME, we need a better way to identify all supported iconv charsets
	if($ctypecharset==='""'){$ctypecharset=false;}
	// check for xml tag with encoding
	preg_match('@^[ \n]*< *\? *xml[^>]*encoding=["\']([a-zA-Z0-9-]+)@i',$html,$m);
	$xmlcharset=val($m,1,false);
	// check for meta tag content-type (supports both http-equiv and html5 charset)
	preg_match('@<meta[^>]*charset=["\']?([a-z0-9-]+)@i',$html,$m);
	$htmlcharset=val($m,1,false);

	$ctypecharset =strtolower($ctypecharset );
	$xmlcharset   =strtolower($xmlcharset   );
	$htmlcharset  =strtolower($htmlcharset  );

	// choose the most reliable charset (differences are common)
	$charset=false;
	if($charset===false && strlen($ctypecharset)!=0){$charset=$ctypecharset;}
	if($charset===false && strlen($xmlcharset  )!=0){$charset=$xmlcharset  ;}
	if($charset===false && strlen($htmlcharset )!=0){$charset=$htmlcharset ;}
	if($charset===false){$charset=strtolower(mb_detect_encoding($html,$detectEnc));}

	//echo "CT :$ctypecharset<br/>\n";
	//echo "XML:$xmlcharset<br/>\n";
	//echo "HTM:$htmlcharset<br/>\n";
	//echo "RES:$charset<br/>\n";

	// convert to utf-8
	if($charset!="utf-8")
	{
		if($charset==='iso-latin-1' || $charset==='iso-latin1' || $charset=='iso-8856-15'){$charset='iso-8859-1';}
		// bad quotes (utf8 0xe28099 U+2019 inside iso)
		if($charset=='iso-8859-1'){$html=str_replace('’',"'",$html);}
		//echo "CONVERTING FROM: $charset \n";
		$html =iconv($charset, "UTF-8//IGNORE", $html);
	}
	else
	{
		// enforce valid utf-8
		$html=mb_convert_encoding($html,"UTF-8", "UTF-8");
		// This doesn't work any more (php>5.2 ??). Returns "false" instead of clean string.
		//$html =iconv($charset, "UTF-8//IGNORE", $html);
	}
	// change the declarations in the html
	$html=dlib_html_change_charset_declarations($html,'utf-8');

	// replace unicode composition (e+' => é) and some other stuff
	$html=dlib_normalize_utf8($html);

	return $html;
}

//! Changes charset/encoding declarations in an html string.
//! Both <.xml encoding="...".> and <meta http-equiv="Content-Type"> are changed.
//! If no meta or xml tags are found, then a meta tag is added.
function dlib_html_change_charset_declarations(string $html,string $newCharset)
{
	// change encoding in xml tag
	$html=preg_replace('@^[ \n]*(< *\? *xml [^>]*encoding=).[^"\'><]*.@',
					   '$1"'.$newCharset.'"',$html,-1,$nxml);

	// meta (works for both http-equiv and html5 charset)
	$html=preg_replace('@(<meta[^>]*charset=)[a-z0-9-]+@i',
					   '$1'.strtoupper($newCharset),$html,-1,$nhtml);

	// if no charset declarations, add one
	if($nxml==0 && $nhtml==0)
	{
		$html=preg_replace('@(<head[^>]*>)@i',
						   '$1'.'<meta http-equiv="Content-Type" '.
						   'content="text/html; charset='.$newCharset.'" />',
						   $html,-1,$nhead);
		// no head tag!!! , add one :-(
		if($nhead==0)
		{
			$html='<meta http-equiv="Content-Type" '.
				'content="text/html; charset='.$newCharset.'" />'.$html;
		}
	}
	return $html;
}

//! Uses tidy to completely reparse and reformat an html page.
//! The result is a valid, properly indented, full xhtml page.
//! @param $html5 : true/false/'convert' ('convert' replaces html5 tags with div/span)
function dlib_html_cleanup_page_with_tidy(string $html,bool $convertToUtf8=true,$html5=true)
{
	if($convertToUtf8){$html=dlib_html_convert_page_to_utf8($html);}

	// misspelled cdata in some pages confuses tidy + dom parser :-(
	$html=str_replace('<[!CDATA[','<![CDATA[',$html);
	$html=str_replace('<!]]&gt;','<!]]>',$html);

	$config = [
		'doctype'        => 'strict',
		'indent'         => true,
		'output-xhtml'   => true,
		'numeric-entities' => false,
		'wrap'           => 0,
		'quote-nbsp' => false,
			  ];

	// Temporary workaround.
	// On the long term, wait to see if libxml (DOMDocument) gets an html5 parser, or
	// consider alternatives like html5-php.
	// https://github.com/Masterminds/html5-php
	// or maybe html5-tidy (if it gets included in PHP)
	if($html5===true || $html5==='convert')
	{
		$config['new-blocklevel-tags']='article aside audio details dialog figcaption figure footer header hgroup main menuitem nav section source summary track video';
		$config['new-empty-tags'     ]='command embed keygen source track wbr';
		$config['new-inline-tags'    ]='bdi canvas command data datalist embed keygen mark meter output picture progress time wbr';
	}

	// tidy chokes on complex js <script> insert inside js (on drupal site)
	$html=preg_replace('@(document\.write\()(.)<script@Us','$1$2<s$2+$2cript',$html);
	$clean=dlib_tidy_repair_string($html,$config,"utf8");

	// convert html5 tags into div or span
	if($html5==='convert')
	{
		$clean=preg_replace('@<(/)?('.str_replace(' ','|',$config['new-blocklevel-tags']).')\b@' ,'<$1div' ,$clean);
		$clean=preg_replace('@<(/)?('.str_replace(' ','|',$config['new-inline-tags'    ]).')\b@' ,'<$1span',$clean);
		// FIXME empty-tags 
	}

	// argh! bad bug??????
	$clean=preg_replace('@^(<\?xml[^>]*)\?&gt[^>]*\?>@','$1"?>',$clean);

	//$html=dlib_fix_strange_chars($html); ???

	
	$clean=str_replace([
						   "&shy;" ,// 1: soft hyphen
						   "&#173;",// 2: soft hyphen
						   '​'      ,// 3: Zero width space (U+200B)
						   "‍"      ,// 4: Zero width joiner (U+200D)
						   "‌"      ,// 5: Zero width non-joiner (U+200C)

					   ],
					   [
						   ''      ,// 1: 	
						   ''      ,// 2: 	
						   ''      ,// 3: 	
						   ''      ,// 4: 	
						   ''      ,// 5: 	
					   ],$clean);

	// replace unicode composition (e+' => é) and some other stuff
	$html=dlib_normalize_utf8($html);

	return $clean;
}

//! Uses  tidy to  convert any  html into  well formed  xhtml.   An XSL
//! stylesheet "cleanup-xhtml.xsl" is used for more complex operations,
//! like removing most tags.
//! The result is very simplified html with only a few allowed tags.
//! options: xsl-filename, extra-attributes (array of allowed attributes)
//! Note : this does NOT guarantee XSS security filtering (although it might be close, it still needs full auditing and testing)
function dlib_html_cleanup_tidy_xsl(string $html,array $options=[])
{
	global $dlib_config;
	// tidy options defined in http://tidy.sourceforge.net/docs/quickref.html
	$config = dlib_html_cleanup_tidy_xsl_config();
	$config['numeric-entities']=true; // Why??

	$html=dlib_fix_strange_chars($html);

	// remove soft hyphen entities
	$html=str_replace("&shy;","",$html);
	$html=str_replace("&#173;","",$html);
	$html=str_replace("&#xad;","",$html);
	$html=str_replace("‍","",$html);

	// enforce valid utf-8
	// This doesn't work any more (php>5.2 ??). Returns "false" instead of clean string.
	//$html=@iconv("UTF-8","UTF-8//IGNORE", $html);
	$html=mb_convert_encoding($html,"UTF-8", "UTF-8");

	// replace unicode composition (e+' => é) and some other stuff
	$html=dlib_normalize_utf8($html);

	// Note: replace nbsp entity with unicode nbsp is done by tidy

	// remove msoffice xml stuff, that are not removed by tidy but confuse Dom parser
	$html=preg_replace('@<?xml:namespace[^>]*schemas-microsoft-com:office[^>]*/>@','',$html);
	$html=preg_replace('@<\?<[^>]*>@','',$html);
	$html=preg_replace('@</?o:(p|span)>@','',$html);
	//echo "cleanup_html_tidy_xsl1:".strlen($html)."<br/>\n$html<br/>\n";
	//echo "html:".$html."\n\n";
	$clean=dlib_tidy_repair_string($html,$config,"utf8");
	// strange case: tidy creates a badly form xml tag
	$clean=preg_replace('@(<\?xml[^?>]+)>@','$1?>',$clean);
	//echo "clean1:".$clean."\n\n";

	$cleanDoc=dlib_dom_from_html($clean);

	// use  xslt to  cleanup html.   this removes  unwanted  elements and attributes
	$xslDoc=new DOMDocument();
	$xslFname=val($options,'xsl-filename','dlib/cleanup-xhtml.xsl');
	$xslDoc->load($xslFname);
	if($xslDoc===false){fatal("cleanup_html_tidy_xsl: xslDoc load failed");}
	// modify xsl document with options
	if(isset($options['extra-attributes']))
	{
		$xslXXPath = new DOMXPath($xslDoc);
		$attributes=$xslXXPath->query("//*[@name='accepted-attributes']")->item(0);
		$attributes->setAttribute('match',
								  $attributes->getAttribute('match').
								  '|@'.implode('|@',$options['extra-attributes']));
	}
	$xsl = new XSLTProcessor();	
	$xsl->importStyleSheet($xslDoc);
	$cleanDoc=$xsl->transformToDoc($cleanDoc);
	$xpath=new DOMXPath($cleanDoc);

	// Remove block elements directly inside top block elements.
	// This is temporary. An effort is needed to replace tidy. 
	// This is insufficient, but helps. 
	// For example if document is wrapped in divs, the divs are transformed into paragraphs. This will remove a few layers.
	for($i=0;$i<4;$i++)
	{
	 	foreach($xpath->query("/*[(self::p or self::h2 or self::h3 or self::h4 or self::h5 or self::ul or self::ol)  and ./*[self::p or self::h2 or self::h3 or self::h4 or self::h5 or self::ul or self::ol] ]") as $block)
	 	{
			//echo $block->nodeName."\n";
	 		dlib_dom_flatten($block,['p','h2','h3','h4','h5','ul','ol']);
	 	}
	}
	//echo "AFTER flatten:".$cleanDoc->saveHTML()."|\n";

	// Fix all attributes
	foreach($xpath->query("//@*") as $attr)
	{
		$value=$attr->value;
		switch($attr->name)
		{
		case 'width':
		case 'height':
			if(!ctype_digit($value)){$value=false;}
			break;
		case 'alt':
			// important : remove double quotes. Otherwise, PHPDom outputs single quoted attributes in HTML.
			$value=trim(preg_replace('@[^\p{L}[0-9]_.-]@u',' ',$value));
			break;
		case 'class':
			$value=trim(preg_replace('@[^a-zA-Z0-9_ -]@',' ',$value));
			break;
		case 'href':
		case 'src':
			// This is a simplified version of filter_xss_strip_dangerous_protocols(). It should be as XSS resistant.
			if(preg_match('@^(?!(https?://|mailto:))[^/?#]*:@i',$attr->value)){$value=false;}
			break;
		default: // this should never happen
			// FIXME : temporary fix: extra attributes need to provide a validation function
			if(isset($options['extra-attributes']) && array_search($attr->name,$options['extra-attributes'])!==false)
			{
				$value=trim(preg_replace('@[^a-zA-Z0-9_ -]@',' ',$value));				
			}
			else
			{
				$value=false;
			}
		}
		// Remove empty attributes (except for "alt")
		if(trim($value)==='' && $attr->name!=='alt'){$value=false;}
		if($value===false){$attr->parentNode->removeAttribute($attr->name);}
		else
		{
			// PHP DOM quirk ?! : $attr->value=$attr->value is not ok. Needs ent() :-(
			$attr->value=ent($value);
		}
	}
	//echo "AFTER attr fix  :".$cleanDoc->saveHTML()."|\n";

	// Remove "a" links that only have whitespace contents, or that have no href attribute
	foreach($xpath->query('//a') as $a)
	{
		if((trim($a->nodeValue)=='' && $a->getElementsByTagName('img')->length==0) ||
		   trim($a->getAttribute('href'))==='')
		{
			dlib_dom_withdraw($a);
		}
	}

	// Remove leading and trailing whitespace and <br> inside <li>. 
    // Maybe we should also do this inside <p> and others ?
	foreach($xpath->query('//li') as $el)
	{
        if($el->firstChild!==null && $el->firstChild->nodeName=='#text')
        {
            $el->firstChild->nodeValue=ltrim($el->firstChild->nodeValue);
            if($el->firstChild->nodeValue===''){$el->removeChild($el->firstChild);}
        }
        if($el->lastChild!==null && $el->lastChild ->nodeName=='#text')
        {
            $el->lastChild ->nodeValue=rtrim($el->lastChild ->nodeValue);
            if($el->lastChild->nodeValue===''){$el->removeChild($el->lastChild);}
        }
        if($el->firstChild!==null && $el->firstChild->nodeName=='br'){$el->removeChild($el->firstChild);}
        if($el->lastChild !==null && $el->lastChild ->nodeName=='br'){$el->removeChild($el->lastChild);}

	}
    // Special case: handle leading/trailing <br> inside other element (like <a> or <strong>)
	foreach($xpath->query('//li/*/br') as $br)
	{
        if($br->previousSibling===null && $br->parentNode->previousSibling===null){$br->parentNode->removeChild($br);}
        else
        if($br->nextSibling    ===null && $br->parentNode->nextSibling    ===null){$br->parentNode->removeChild($br);}
    }

	// Remove images without src attribute
	foreach($xpath->query('//img[not(@src)]') as $img)
	{
		$img->parentNode->removeChild($img);
	}

	// remove strong or em  tags around br : <strong><br /></strong> => <br />
	foreach($xpath->query('(//strong|//em)[.//br and not(.//img)]') as $strong)
	{
		if(trim($strong->nodeValue)==''){dlib_dom_withdraw($strong);}
	}

	// merge two successive strong elements (that have no attributes, like class)
	// <strong>ab</strong><strong>cd</strong> => <strong>abcd</strong>
	foreach($xpath->query('//strong[not(@*)]/following-sibling::strong[not(@*)]') as $secondStrong)
	{
		$firstStrong=$secondStrong->previousSibling;
		if($firstStrong->nodeName!=='strong'){continue;}// there is a text node between the strongs
		$children=[];
		foreach($secondStrong->childNodes as $child){$children[]=$child;}
		foreach($children as $child){$firstStrong->appendChild($child);}
		$secondStrong->parentNode->removeChild($secondStrong);
	}

	// merge two successive em elements (that have no attributes, like class)
	foreach($xpath->query('//em[not(@*)]/following-sibling::em[not(@*)]') as $secondEm)
	{
		$firstEm=$secondEm->previousSibling;
		if($firstEm->nodeName!=='em'){continue;}// there is a text node between the ems
		$children=[];
		foreach($secondEm->childNodes as $child){$children[]=$child;}
		foreach($children as $child){$firstEm->appendChild($child);}
		$secondEm->parentNode->removeChild($secondEm);
	}

	// remove empty lines
	foreach($xpath->query('(//p|//h1|//h2|//h3|//h4|//li)[not(.//img)]') as $n)
	{
		if(trim($n->nodeValue)==''){$n->parentNode->removeChild($n);}
	}

	$clean=dlib_dom_to_html($cleanDoc);

	//echo "before retidy:\n";var_dump($clean);

	// Retidy processed HTML
	// Re-tidy is necessary as DOM manipulation does not fix all problems (far from it).
	// For example, div's are transformed into paragraphs that may contain other paragraphs.
	// Example 2: what do we do with block level elements (ex: <p>) inside inline (ex: <a>) ? 
	// Eventually we could tackle this and get rid of tidy, but that's a *lot* of work.
	// Tidy also outputs proper XHTML (with closing tags). 
	$config['numeric-entities']=false; 
	$clean=dlib_tidy_repair_string($clean,$config,"utf8");
	//echo "tidy all finished:".$clean."\n";
	return $clean;
}

function dlib_html_cleanup_tidy_xsl_config()
{
	$config = [
		'doctype'          =>'strict',
		'indent'           =>true,
		'output-bom'       =>false,// no Unicode Byte Order Mark
		'output-xhtml'     =>true,
		'numeric-entities' =>false, // FIXME : why?
		'wrap'             =>0,
		'enclose-text'     =>true, // Add <p> for top level text
		'word-2000'        =>true, // for ms documents
		'bare'             =>true, // for ms documents (this also transf nbsp to space)
		'hide-comments'    =>true, // remove html comments
		'newline'          =>'LF', // never use CR (carriage return) "\r"
		'logical-emphasis' =>true,
		'quote-nbsp'       =>false,
		'anchor-as-name'   =>false, // tidy adds name="..." attribute to links, which mess up in tinymce
		'add-xml-decl'     =>true,
		'char-encoding'    =>'utf8',
		'show-body-only'   =>true,
		//'drop-proprietary-attributes' => true,
			  ];

	return $config;
}

//! Changes the encoding of an xml string to utf-8 using iconv<.
//! The original encoding is extracted from the <.xml encoding="..." > tag.
//! The <.xml encoding="..." .> tag is changed in the result.
function dlib_xml_convert_to_utf8(string $xml)
{
	if(!preg_match('@^\s*<\?xml[^>]*encoding=["\']([^"\']*)["\']@si',$xml,
				   $charsetParts))
	{
		throw new Exception("convert_xml_to_utf8: could not find encoding");
	}

	$charset=$charsetParts[1];

	if(strcasecmp($charset,"UTF-8"))
	{
		$xml =iconv($charset, "UTF-8", $xml);
	}
	$xml=preg_replace('@([ \n]*<\?xml[^>]*encoding=["\'])([^"\']*)(["\'])@si','$1utf-8$3',$xml);

	return $xml;
}

//! Convert CSS selector to XPath
//! Note: this is a quick hack that handles most CSS2 selectors and a few CSS3 selectors too.
function dlib_css_selector_to_xpath(string $css)
{
	// Split css selector into parts by " " or "+" or ">" or ","
	$topParts=preg_split('@ *( |>|\+|,) *@',trim($css),-1,PREG_SPLIT_DELIM_CAPTURE);
	$conditionStarted=false;
	$res='//';
	foreach($topParts as $nPart=>$topPart)
	{
		// generate XPATH for the top part split operator (" ",">"...)
		if(($nPart%2)!==0)
		{
			$conditionStarted=false;
			$topPart=trim($topPart);
			if($topPart==='' ){$res.='//';}
			if($topPart==='>'){$res.='/';}
			if($topPart===','){$res.=' | //';}
			if($topPart==='+'){$res.='/following-sibling::*[1]';$conditionStarted=true;}
			continue;
		}
		// Now parse each part. Example: E.F#G[x]
		// Split topPart into elParts: E.F#G[x] => ['E','.F','#G','[x]']
		$elParts=preg_split('@(?=(\.|#|\[|:))@',trim($topPart));
		//var_dump($elParts);
		$elName='*';
		$elConditions=[]; // List of "and" conditions
		$firstChild=false;
		foreach($elParts as $ne=>$elPart)
		{
			if($ne===0 && $elPart===''){continue;}
			$elPart1=substr($elPart,1);
			if(ctype_alpha($elPart[0]) || $elPart[0]==='*'){$elName=$elPart;continue;}
			if($elPart===':first-child'){$firstChild=true;continue;}
			if($elPart===':checked'){$elPart='[checked]';}
			// Most elParts (.,#,[]) just translate into conditions.
			if($elPart===':empty'){$elConditions[]='count(*)=0';}
			if($elPart[0]==='.'){$elPart='[class~="'.$elPart1.'"]';}
			if($elPart[0]==='#'){$elPart='[id="'.$elPart1.'"]';}
			if($elPart[0]==='[')
			{
				//var_dump($elPart);
				if(!preg_match('@^\[([a-z0-9_-]+)((~=|=|\*=|\^=)"?([^"]*)"?)?\]$@i',$elPart,$m)){return false;}
				//var_dump($m);
				switch(val($m,3,''))
				{
				case '':  $elConditions[]="@".$m[1]; break;
				case '~=': 
					$elConditions[]="contains(concat(' ', normalize-space(@".$m[1]."), ' '),' ".$m[4]." ')";
					break;
				case '=': 
					$elConditions[]="@".$m[1]."='".$m[4]."'";
					break;
				case '*=': 
					$elConditions[]="contains(@".$m[1].",'".$m[4]."')";
					break;
				case '^=': 
					$elConditions[]="starts-with(@".$m[1].",'".$m[4]."')";
					break;
				}
			}
		}
		if(!$conditionStarted){$res.=$elName;}
		else
		{
			// special case for following-sibling
			if($elName!=='*'){$res.="[name()='".$elName."']";}
		}
		if($firstChild){$res.='[1]';}
		if(count($elConditions)){$res.='['.implode(' and ',$elConditions).']';}
	}
	return $res;
}

//! Returns the html selected by a CSS selector
//! Optionally removes (instead of selecting).
function dlib_html_select_css(string $html,string $css,bool $remove=false)
{
	$path=dlib_css_selector_to_xpath($css);
	return dlib_html_select_xpath($html,$path,$remove);
}

//! Returns the html selected by an xpath expression
//! Optionally removes (instead of selecting).
function dlib_html_select_xpath(string $html,string $path,bool $remove=false)
{
	$html0=$html;

	$doc=dlib_dom_from_html($html,$xpath);
	$selected=$xpath->evaluate($path);

	if($selected===null || $selected===false)
	{
		throw new Exception("xpath_select_document_parts: xpath->evaluate failed; strlen(html)=".strlen($html)." path=".ent($path));
	}

	// Special case: remove CDATA sections in <script> that nest each time this fct is called
	// FIXME : we should investigate why this is happening (maybe loadHtml followed by saveXML)
	foreach($xpath->query("//script") as $node)
	{
		$node->textContent=preg_replace('@(\n?\s*// *)?<!\[CDATA\[(\s*\n)?(.*)(\s*\n)?(// *)?\]\]>\s*([^\s]|$)@sU','$3$6',$node->textContent);
	}

	if(!$remove)
	{
		if($selected->length==0){return false;}
		$res='';
		foreach($selected as $node)
		{
			// FIXME : workaround for php segfault: probably bug in old libxml version
			if($node->nodeName=='href'){$a='href="'.$node->value.'"';}
			else{$a=trim($doc->saveXML($node));}
			$res.=$a."\n";
		}
		return trim($res);
	}
	else
	{
		// This is the reverse operation: remove selected nodes instead of keeping them
		foreach($selected as $node)
		{
			$node->parentNode->removeChild($node);
		}
		return dlib_dom_to_html($doc);
	}
}

//! Apply a list of selectors (regexps, xpath and CSS expressions) to select parts
//! of an html document.
//! Example of selectors:
//! $selectors=[ ['type'=>'CSS',       'arg'=>'#main'    ], 
//!              ['type'=>'CSS-REMOVE','arg'=>'#comments'] ]
function dlib_html_apply_selectors(string $html,array $selectors)
{
	$debug=false;
	if(count($selectors)==0){return $html;}
	if($debug){echo 'selector:';vd($selectors);}
	foreach($selectors as $n=>$sel)
	{
		$cleanupHtml=false;
		$type=$sel['type'];
		$arg =$sel['arg' ];
		if($debug)
		{
			echo "------<br/>selector part $n: type:".ent($type)." ".
				"arg:".ent($arg)."<br/>\n";
			echo "html length before:".strlen($html)."<br/>\n";
		}
		// sanity check
		if(trim($arg)===''){return false;}
		switch($type)
		{
		case 'ERASE': 
			$html=preg_replace($arg,'',$html); 
			$cleanupHtml=true;
            break;
		case 'CSS':
			$arg=dlib_css_selector_to_xpath($arg);
		case 'XPATH':
			//file_put_contents('/tmp/src',$html);
			$html=dlib_html_select_xpath($html,$arg);
			if(strlen($html)==0)
			{
				if($debug){echo "XPATH select returned empty string, aborting<br/>";}
				return false;
			}
			break;
		case 'CSS-REMOVE':
			$arg=dlib_css_selector_to_xpath($arg);
		case 'XPATH-REMOVE':
			$html=dlib_html_select_xpath($html,$arg,true);
			if(strlen($html)==0){return false;}
			break;
		default:fatal("invalid selector type:".$type);
		}
		if($cleanupHtml){$html=dlib_dom_to_html(dlib_dom_from_html($html));}
		if($debug){echo "html length after:".strlen($html)."<br/>\n";}
	}

	return $html;
}

function dlib_html_selectors_display(array $selectors)
{
	$out='';
	$out='<table class="html-selectors gray-table">';
	foreach($selectors as $selector)
	{
		$out.='<tr>'.
			'<td class="selector-type">'.ent($selector['type']).'</td>'.
			'<td class="selector-arg" >'.ent($selector['arg' ]).'</td>'.
			'</td>';
	}
	$out.='</table>';
	return $out;
}

//! This estimates the screen size of a *text* (not html) string.
//! Please note that this is not perfect.
//! It is better than estimating strlen*average letter size.
//! For standard latin charsets and greek on the same browser, the error margin is around .5%.
//! The size returned is that of a 18px bold Arial font. 
//! You should adjust the result for your font size & type.
//! Script available here : http://example.org/maintenance/estimate-html-display-width
function dlib_html_estimate_display_width(string $str)
{
	static $letterSizes=false;
	if($letterSizes===false)
	{
		// This is computed by a js script here http://example.org/maintenance/estimate-html-display-width
		// The reference is bold 18px Arial
		$letterSizes=json_decode('{"0":10,"1":10,"2":10,"3":10,"4":10,"5":10,"6":10,"7":10,"8":10,"9":10," ":5,"!":6,"\"":9,"#":10,"$":10,"%":16,"&":13,"\'":4,"(":6,")":6,"*":7,"+":11,",":5,"-":6,".":5,"/":5,":":6,";":6,"<":11,"=":11,">":11,"?":11,"@":18,"A":13,"B":13,"C":13,"D":13,"E":12,"F":11,"G":14,"H":13,"I":5,"J":10,"K":13,"L":11,"M":15,"N":13,"O":14,"P":12,"Q":14,"R":13,"S":12,"T":11,"U":13,"V":12,"W":17,"X":12,"Y":12,"Z":11,"[":6,"\\\\":5,"]":6,"^":11,"_":10,"`":6,"a":10,"b":11,"c":10,"d":11,"e":10,"f":6,"g":11,"h":11,"i":5,"j":5,"k":10,"l":5,"m":16,"n":11,"o":11,"p":11,"q":11,"r":7,"s":10,"t":6,"u":11,"v":10,"w":14,"x":10,"y":10,"z":9,"{":7,"|":5,"}":7,"~":11,"¡":6,"¢":10,"£":10,"¤":10,"¥":10,"¦":5,"§":10,"¨":6,"©":13,"ª":7,"«":10,"¬":11,"­":0,"®":13,"¯":10,"°":7,"±":10,"²":6,"³":6,"´":6,"µ":10,"¶":10,"·":5,"¸":6,"¹":6,"º":7,"»":10,"¼":15,"½":15,"¾":15,"¿":11,"À":13,"Á":13,"Â":13,"Ã":13,"Ä":13,"Å":13,"Æ":18,"Ç":13,"È":12,"É":12,"Ê":12,"Ë":12,"Ì":5,"Í":5,"Î":5,"Ï":5,"Ð":13,"Ñ":13,"Ò":14,"Ó":14,"Ô":14,"Õ":14,"Ö":14,"×":11,"Ø":14,"Ù":13,"Ú":13,"Û":13,"Ü":13,"Ý":12,"Þ":12,"ß":11,"à":10,"á":10,"â":10,"ã":10,"ä":10,"å":10,"æ":16,"ç":10,"è":10,"é":10,"ê":10,"ë":10,"ì":5,"í":5,"î":5,"ï":5,"ð":11,"ñ":11,"ò":11,"ó":11,"ô":11,"õ":11,"ö":11,"÷":10,"ø":11,"ù":11,"ú":11,"û":11,"ü":11,"ý":10,"þ":11,"ÿ":10,"Α":13,"Β":13,"Γ":11,"Δ":13,"Ε":12,"Ζ":11,"Η":13,"Θ":14,"Ι":5,"Κ":13,"Λ":12,"Μ":15,"Ν":13,"Ξ":12,"Ο":14,"Π":13,"Ρ":12,"Σ":11,"Τ":11,"Υ":12,"Φ":15,"Χ":12,"Ψ":15,"Ω":14,"α":11,"β":11,"γ":10,"δ":11,"ε":9,"ζ":8,"η":11,"θ":10,"ι":5,"κ":10,"λ":10,"μ":11,"ν":10,"ξ":8,"ο":11,"π":14,"ρ":11,"σ":12,"τ":8,"υ":10,"φ":13,"χ":10,"ψ":14,"ω":15,"Ά":13,"Έ":15,"Ή":16,"Ί":9,"Ό":15,"Ύ":17,"Ώ":15,"ΐ":5,"Ϊ":5,"Ϋ":12,"ά":11,"έ":9,"ή":11,"ί":5,"ΰ":10,"ϊ":5,"ϋ":10,"ό":11,"ύ":10,"ώ":15,"ς":9}',true);
	}
	$str=dlib_cleanup_plain_text($str,false,false);
	$str=trim($str);
	$str=preg_replace('@\s+@',' ',$str);
	$res=0;
	for($i=0;$i<mb_strlen($str);$i++)
	{
		$letter=mb_substr($str,$i,1);
		$res+=$letterSizes[$letter] ?? 1;
	}
	return $res;
}

// **************************************************
// DOM helper functions
// **************************************************


//! Creates a DOMDocument from an html fragment or a full document.
//! Fragment should be  UTF-8 encoded 
//! For convenience, can also optionally create a DOMXPath object.
function dlib_dom_from_html(string $html,&$xpath=false)
{
	$doc=new DOMDocument();
	if(!$doc->loadHTML('<?xml encoding="UTF-8">'.$html,LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD))
	{
		throw new Exception("dlib_dom_from_html: DOMDocument::loadHTML failed");
	}
	$doc->encoding='UTF-8';
	// Remove < xml ...> . Sometimes necessary. Unsure why.
	foreach($doc->childNodes as $item){if($item->nodeType == XML_PI_NODE){$doc->removeChild($item);break;}}
	if($xpath!==false)
	{
		$xpath=new DOMXPath($doc);
	}
	return $doc;	
}

//! Creates an HTML string from a DOMDocument
//! The output ressembles XHTML (without < ? xml ...  ? > )
function dlib_dom_to_html(DOMDocument $doc)
{
    // Note : saveXML has issues with XHTML. 
	// We need to fix problems like  <script/> => <script></script> (also important for <a>, <iframe> ...)
    // http://stackoverflow.com/questions/3145237/why-doesnt-php-dom-include-slash-on-self-closing-tags
	$xpath=new DOMXPath($doc);
	foreach($xpath->query("//*[not(node()) and not(self::area|self::base|self::br|self::col|self::embed|self::hr|self::img|self::input|self::keygen|self::link|self::meta|self::param|self::source|self::track|self::wbr)]") as $el)
	{
		$el->appendChild($doc->createTextNode(''));
	}

	$html=$doc->saveXML();
	$html=preg_replace('@^<\?xml[^>]*>@u','',$html);
	$html=trim($html);
	return $html;
}

//! Removes a DOM node from a document, replacing it with its children.
function dlib_dom_withdraw(DOMNode $element)
{
	$parent=$element->parentNode;
	$children=[];
	foreach($element->childNodes as $child){$children[]=$child;}
	foreach($children as $child){$parent->insertBefore($child,$element);}
	$parent->removeChild($element);
}

//! DOM node is moved one step up the tree. Its parent node is split.
//! FIXME: this is not used yet. It can be usefull to move up block elements that are inside inline elements.
//! Typical use case: <p> inside <a>: p moves up, a moves down.
function dlib_dom_bubble_up(DOMNode $element)
{
	$parent=$element->parentNode;
	// ************1********* : split parent into 3 : before / parent / after
	$leftOfParent =$parent->cloneNode();
	$rightOfParent=$parent->cloneNode();
	$parent->parentNode->insertBefore($leftOfParent  ,$parent);
	$parent->parentNode->insertBefore($rightOfParent ,$parent->nextSibling);
	
	$isBefore=true;
	$children=[];
	foreach($parent->childNodes as $child){$children[]=$child;}
	foreach($children as $child)
	{
		if($child===$element){$isBefore=false;continue;}
		$target=($isBefore ? $leftOfParent : $rightOfParent);
		$target->insertBefore($child);
	}

	// ************2********* : move element up and remove parent
	$parent->parentNode->insertBefore($element ,$parent);
	$parent->parentNode->removeChild($parent);

	// ************3********* : insert clones of old $parent between $element and each of its children
	$children=[];
	foreach($element->childNodes as $child){$children[]=$child;}
	foreach($children as $child)
	{
		$insert=$parent->cloneNode();
		$element->insertBefore($insert);
		$insert->insertBefore($child);
	}	
}

//! Removes one level from the tree : 
//! Example: <p>a<h3>b</h3>c<a>d</a></p> => <p>a</p><h3>b</h3><p>c<a>d</a></p>
//! FIXME: this temporary. We will probably use dlib_dom_bubble_up() instead.
function dlib_dom_flatten(DOMNode $element,array $noWrap)
{
	$parent=$element->parentNode;
	$children=[];
	foreach($element->childNodes as $child){$children[]=$child;}
	$wrap=false;
	foreach($children as $child)
	{
		if(array_search($child->nodeName,$noWrap)===false)
		{
			if($wrap===false){$wrap=$element->cloneNode();}
			$wrap->insertBefore($child);
		}
		else
		{
			if($wrap!==false){$parent->insertBefore($wrap,$element);$wrap=false;}
			$parent->insertBefore($child,$element);
		}
	}
	if($wrap!==false){$parent->insertBefore($wrap,$element);}
	$parent->removeChild($element);
}

//! Append all children of $element to $dest and then remove $element.
function dlib_dom_merge(DOMNode $element,DOMNode $dest)
{
	$children=[];
	foreach($element->childNodes as $child){$children[]=$child;}
	foreach($children as $child){$dest->appendChild($child);}
	$element->parentNode->removeChild($element);
}
//! Prepend all children of $element to $dest and then remove $element.
function dlib_dom_merge_before(DOMNode $element,DOMNode $dest)
{
	$children=[];
	foreach($element->childNodes as $child){$children[]=$child;}
	$first=$dest->firstChild;
	foreach($children as $child){$dest->insertBefore($child,$first);}
	$element->parentNode->removeChild($element);
}

//! Split parent of $element into two after $element.
function dlib_dom_split(DOMNode $element)
{
	$p=$element->parentNode;
	$newP=$element->ownerDocument->createElement($p->nodeName);
	$children=[];
	for($c=$element->nextSibling;$c!==null;$c=$c->nextSibling){$children[]=$c;}
	foreach($children as $c){$newP->appendChild($c);}
	$p->parentNode->insertBefore($newP,$p->nextSibling);
}

//! Debuging: displays a tree of DOM elements (for text, not html output).
function dlib_dom_print(DOMNode $element,$showClass=false,$d=0)
{
	echo str_repeat(' ',$d*4);
	echo $element->nodeName;
	if($element->nodeName=='#text')
	{
		echo ': "'.mb_substr(str_replace("\n",'\n',$element->textContent),0,100)."\"\n";
		return;
	}
	if(method_exists($element,'getAttribute'))
	{
		$id=$element->getAttribute('id');
		if($id!==''){echo '#'.$id;}
		if($showClass)
		{
			$class=$element->getAttribute('class');
			if(trim($class)!==''){echo '.'.preg_replace('@\s+@','.',$class);}
		}
	}
	if(isset($element->childNodes))
	{
		if(isset($element->childNodes->length)){echo '('.$element->childNodes->length.")";}
	}
	echo "\n";
	if(isset($element->childNodes))
	{
		foreach($element->childNodes as $child){dlib_dom_print($child,$showClass,$d+1);}
	}
}

function dlib_dom_has_class(DOMElement $el,string $searchClass)
{
	$classes=explode(" ",$el->getAttribute('class'));
	return array_search($searchClass,$classes)!==false;
}

function dlib_dom_add_class(DOMElement $el,string $class)
{
	$classes=explode(" ",$el->getAttribute('class'));
	if(array_search($class,$classes)===false)
	{
		$el->setAttribute('class',trim($el->getAttribute('class').' '.$class));
	}
}

function dlib_dom_remove_class(DOMElement $el,string $delClass)
{
	$class0=$el->getAttribute('class');
	if($class0==''){return;}
	$resClass=implode(" ",array_diff(explode(" ",$class0),[$delClass]));
	if($resClass==''){$el->removeAttribute('class');}
	else
	{$el->setAttribute('class',$resClass);}
}


//! Returns a node list of nodes matching a CSS selector.
//! $node is the root of the search. It can be a DOMDocument or any other DOMode.
function dlib_dom_query_selector($node,$css)
{
	if($node->nodeName==='#document')
	{
		$xpath=new DOMXPath($node);
		return $xpath->query(dlib_css_selector_to_xpath($css));
	}
	else
	{
		$xpath=new DOMXPath($node->ownerDocument);
		// .css2xpath(a,b,c) => .css2xpath(a) | .css2xpath(b) | .css2xpath(c)
		$q='';
		$parts=explode(',',$css);
		foreach($parts as $part){$q.='.'.dlib_css_selector_to_xpath($part).' | ';}
		$q=substr($q,0,-3);
		return $xpath->query($q,$node);
	}
}

?>