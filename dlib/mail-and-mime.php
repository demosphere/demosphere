<?php
require_once 'dlib/tools.php';

// FIXME: this file's name and function names need to be rethought.

// Actually encountered mail attachments:
//	  1 application/octet
//	  1 application/pgp-keys
//	  1 application/pgp-signature
//	  1 application/vnd.openxmlformats-officedocument.presentationml.presentation
//	  1 application/x-framemaker
//	  1 application/x-macbinary
//	  1 multipart/signed
//	  1 text/calendar
//	  1 text/rtf
//	  1 /unknown
//	  2 /acrobat-pdf
//	  2 audio/x-ms-wma
//	  3 application/rtf
//	  3 application/vnd.ms-excel
//	  3 application/vnd.oasis.opendocument.spreadsheet
//	  3 image/bmp
//	  3 image/tiff
//	  4 image/pdf
//	  7 application/vnd.oasis.opendocument.text
//	  7 image/pjpeg
//	  8 text/x-vcard
//	  9 application/vnd.openxmlformats-officedocument.wordprocessingml.document
//	  9 message/rfc822
//	 11 video/x-flv
//	 13 application/octet-stream
//	 20 image/jpg
//	197 application/msword
//	229 image/gif
//	299 image/png
//	454 application/pdf
//	581 multipart/related
//	747 image/jpeg
// 2188 multipart/mixed
// 3251 multipart/alternative
// 3499 text/html
// 6505 text/plain


//! Shortcut for finfo.
//! PHP finfo is two step, which is a hassle.
function dlib_finfo($filename,$type=FILEINFO_MIME_TYPE)
{
	static $ressource=false;
	if($ressource===false){$ressource=[];}
	if(!isset($ressource[$type]))
	{
		$ressource[$type]=finfo_open($type);
		if(!$ressource[$type]){fatal('dlib_finfo: finfo not working');}
	}
	return finfo_file($ressource[$type],$filename);
}


//! Returns the prefered filename extension for a mime type.
//! Example : application/pdf => pdf 
function dlib_mime_to_extension($mime)
{
	$mimeTypes=dlib_mime_types();
	if(!isset($mimeTypes[$mime])){return false;}
	return $mimeTypes[$mime][0];
}

//! This is a wild guess, use only in last resort.
function dlib_extension_to_mime($extension)
{
	foreach(dlib_mime_types() as $mimeType=>$extensions)
	{
		if($extensions!==false && array_search($extension,$extensions,true)!==false){return $mimeType;}
	}
	return false;
}

//! Returns a simple document type ('pdf','office','image',false) from a  mime type.
// Example: $mime='application/msword' => return: 'office'
function dlib_mime_to_simple_type($mime)
{
	global $docconvert_config;
	static $mimeTypes=false;
	if($mimeTypes===false){$mimeTypes=dlib_mime_types();}
	static $extensionToType=
		[
			'pdf' =>'pdf',
			'doc' =>'office',
			'docx'=>'office',
			'rtf' =>'office',
			'xls' =>'office',
			'xlsx'=>'office',
			'ppt' =>'office',
			'pptx'=>'office',
			'odt' =>'office',
			'ods' =>'office',
		];


	if($mime===false){$mime='unknown';}
	if(strpos($mime,'image/')===0 && $mime!=='image/pdf'){$type='image';}
	else						  
	{
		$type=false;
		$extensions=val($mimeTypes,$mime);
		$extension=$extensions!==false ? $extensions[0] : false;
		if($extension!==false)
		{
			$type=val($extensionToType,$extension);
		}
	}

	return $type;
}

//! Check if IP is listed as spammer on stopforumspam.com
//! returs null on error, true if listed on stopforumspam, false otherwise 
function dlib_check_ip_in_stopforumspam($ip)
{
	require_once 'dlib/download-tools.php';
	if(!preg_match('@^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$@',$ip)){return null;}
	$xml=dlib_curl_download('http://www.stopforumspam.com/api?ip='.$ip,false,['timeout'=>5]);
	if($xml===false){return null;}
	return (bool)preg_match('@<appears[^>]*>\s*yes\s*</appears>@s',$xml);
}

//! Check if IP is in DNS blocklist (spam)
//! Returns false if not listed on any dnsbl. Otherwise returns the name of the first server matched.
function dlib_check_dnsbl($ip,$servers="forum")
{
	if(!preg_match('@^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$@',$ip)){return false;}
	
    if($servers==='forum'){$servers=["cbl.abuseat.org"];}
    if($servers==='email'){$servers=["bl.spamcop.net","list.dsbl.org","sbl.spamhaus.org",'xbl.spamhaus.org'];}

	$rip=implode('.',array_reverse(explode(".",$ip)));
	foreach($servers as $server)
	{
		$host=$rip.'.'.$server.'.';
		//! PHP checkdnsrr() does not support timeouts. Some of these dnsbl DNS servers may be overloaded, so we use nslookup.
		// checkdnsrr($host,'A')
		// https://gist.github.com/kamermans/1548922
		$cmd=sprintf('nslookup -type=A -timeout=5 %s 2>&1', escapeshellarg($host));
		@exec($cmd, $response);
		// The first 3 lines (0-2) of output are not the DNS response
		for($i=3;$i<count($response);$i++) 
		{
			if(strpos(trim($response[$i]),'Name:')===0){return $server;}
		}
	}
    return false;
}


//! Returns the number of times that something ($name) happened during a given period of time ($period).
//! You should always use the same period for a given name.
//! Don't use this function directly. Use count and add.
//! Don't forget to call dlib_throttle_cron() for cleanup.
function dlib_throttle($name,$period=false,$addHit=false)
{
	// Note: saving 'period' is only necessary for cleanup in dlib_throttle()
	$throttle=variable_get($name,'throttle',['period'=>0,'hits'=>[]]);
	// Remove all hits older than $period
	if($period!==false)
	{
		$old=time()-$period;
		$throttle['hits']=array_filter($throttle['hits'],function($ts)use($old){return $ts>$old;});
		$throttle['period']=$period;
	}
	if($addHit){$throttle['hits'][]=time();}
	variable_set($name,'throttle',$throttle);
	return count($throttle['hits']);
}

function dlib_throttle_count($name,$period)
{
	return dlib_throttle($name,$period,false);
}

function dlib_throttle_add($name)
{
	dlib_throttle($name,false,true);
}

//! Delete old throttles
function dlib_throttle_cron()
{
	$throttles=variable_get_all('throttle');
	foreach($throttles as $name=>$throttle)
	{
		if($throttle['period']===0){continue;}// very special case (added but not counted yet)
		if(count($throttle['hits'])===0 ||
		   dlib_last($throttle['hits'])< time()-$throttle['period'])
		{
			variable_delete($name,'throttle');
		}
	}
}


//! Returns the decoded value of a single header out of the whole block of headers.
function dlib_mime_header_entry($headers,$name)
{
	$res=false;
	// Single regexp segfaults when headers contains many lines, so we do it in 2 steps :-( !!
	$headers=preg_replace("@\r\n[ \t]@",' ',$headers);
	if(preg_match("@^".preg_quote($name,'@').": (.*)\r$@im",
				  $headers,$matches))
	{
		$res=dlib_mime_header_decode($matches[1]);
		$res=str_replace("\n\t","",$res);
		$res=str_replace("\r","",$res);
	}
	return $res;
}

//! Decode a single header's value into utf8.
//! The value might be split into several parts, wich can each be encoded for a charset.
function dlib_mime_header_decode($rawValue)
{
	$valueParts=imap_mime_header_decode($rawValue);
	// ignore verbose imap errors and warnings
	imap_errors();
	imap_alerts();
	$res='';
	foreach($valueParts as $valuePart)
	{
		$charset=$valuePart->charset;
		if($charset=='default'){$charset='iso-8859-1';}
		$res.=@iconv($charset,"utf-8//IGNORE",$valuePart->text);
	}
	return $res;
}


function dlib_mime_types()
{
	// Extracted from /etc/mime.types 
	// Last update : 2016/08
	// grep -E '^[^#]' /etc/mime.types | sed -r 's/[ \t]+/:/g' | sed -r "s/(:|$)/'=>['/" | sed "s/^/\t'/" | sed "s/:/','/g" | sed "s/$/'],/" | sed "s/\[''\]/false/"
	$res=
		[
	'application/activemessage'=>false,
	'application/andrew-inset'=>['ez'],
	'application/annodex'=>['anx'],
	'application/applefile'=>false,
	'application/atom+xml'=>['atom'],
	'application/atomcat+xml'=>['atomcat'],
	'application/atomicmail'=>false,
	'application/atomserv+xml'=>['atomsrv'],
	'application/batch-SMTP'=>false,
	'application/bbolin'=>['lin'],
	'application/beep+xml'=>false,
	'application/cals-1840'=>false,
	'application/commonground'=>false,
	'application/cu-seeme'=>['cu'],
	'application/cybercash'=>false,
	'application/davmount+xml'=>['davmount'],
	'application/dca-rft'=>false,
	'application/dec-dx'=>false,
	'application/dicom'=>['dcm'],
	'application/docbook+xml'=>false,
	'application/dsptype'=>['tsp'],
	'application/dvcs'=>false,
	'application/ecmascript'=>['es'],
	'application/edi-consent'=>false,
	'application/edi-x12'=>false,
	'application/edifact'=>false,
	'application/eshop'=>false,
	'application/font-sfnt'=>['otf','ttf'],
	'application/font-tdpfr'=>['pfr'],
	'application/font-woff'=>['woff'],
	'application/futuresplash'=>['spl'],
	'application/ghostview'=>false,
	'application/gzip'=>['gz'],
	'application/hta'=>['hta'],
	'application/http'=>false,
	'application/hyperstudio'=>false,
	'application/iges'=>false,
	'application/index'=>false,
	'application/index.cmd'=>false,
	'application/index.obj'=>false,
	'application/index.response'=>false,
	'application/index.vnd'=>false,
	'application/iotp'=>false,
	'application/ipp'=>false,
	'application/isup'=>false,
	'application/java-archive'=>['jar'],
	'application/java-serialized-object'=>['ser'],
	'application/java-vm'=>['class'],
	'application/javascript'=>['js'],
	'application/json'=>['json'],
	'application/m3g'=>['m3g'],
	'application/mac-binhex40'=>['hqx'],
	'application/mac-compactpro'=>['cpt'],
	'application/macwriteii'=>false,
	'application/marc'=>false,
	'application/mathematica'=>['nb','nbp'],
	'application/mbox'=>['mbox'],
	'application/ms-tnef'=>false,
	'application/msaccess'=>['mdb'],
	'application/msword'=>['doc','dot'],
	'application/mxf'=>['mxf'],
	'application/news-message-id'=>false,
	'application/news-transmission'=>false,
	'application/ocsp-request'=>false,
	'application/ocsp-response'=>false,
	'application/octet-stream'=>['bin','deploy','msu','msp'],
	'application/oda'=>['oda'],
	'application/oebps-package+xml'=>['opf'],
	'application/ogg'=>['ogx'],
	'application/onenote'=>['one','onetoc2','onetmp','onepkg'],
	'application/parityfec'=>false,
	'application/pdf'=>['pdf'],
	'application/pgp-encrypted'=>['pgp'],
	'application/pgp-keys'=>['key'],
	'application/pgp-signature'=>['sig'],
	'application/pics-rules'=>['prf'],
	'application/pkcs10'=>false,
	'application/pkcs7-mime'=>false,
	'application/pkcs7-signature'=>false,
	'application/pkix-cert'=>false,
	'application/pkix-crl'=>false,
	'application/pkixcmp'=>false,
	'application/postscript'=>['ps','ai','eps','epsi','epsf','eps2','eps3'],
	'application/prs.alvestrand.titrax-sheet'=>false,
	'application/prs.cww'=>false,
	'application/prs.nprend'=>false,
	'application/qsig'=>false,
	'application/rar'=>['rar'],
	'application/rdf+xml'=>['rdf'],
	'application/remote-printing'=>false,
	'application/riscos'=>false,
	'application/rtf'=>['rtf'],
	'application/sdp'=>false,
	'application/set-payment'=>false,
	'application/set-payment-initiation'=>false,
	'application/set-registration'=>false,
	'application/set-registration-initiation'=>false,
	'application/sgml'=>false,
	'application/sgml-open-catalog'=>false,
	'application/sieve'=>false,
	'application/sla'=>['stl'],
	'application/slate'=>false,
	'application/smil+xml'=>['smi','smil'],
	'application/timestamp-query'=>false,
	'application/timestamp-reply'=>false,
	'application/vemmi'=>false,
	'application/whoispp-query'=>false,
	'application/whoispp-response'=>false,
	'application/wita'=>false,
	'application/x400-bp'=>false,
	'application/xhtml+xml'=>['xhtml','xht'],
	'application/xml'=>['xml','xsd'],
	'application/xml-dtd'=>false,
	'application/xml-external-parsed-entity'=>false,
	'application/xslt+xml'=>['xsl','xslt'],
	'application/xspf+xml'=>['xspf'],
	'application/zip'=>['zip'],
	'application/vnd.3M.Post-it-Notes'=>false,
	'application/vnd.accpac.simply.aso'=>false,
	'application/vnd.accpac.simply.imp'=>false,
	'application/vnd.acucobol'=>false,
	'application/vnd.aether.imp'=>false,
	'application/vnd.android.package-archive'=>['apk'],
	'application/vnd.anser-web-certificate-issue-initiation'=>false,
	'application/vnd.anser-web-funds-transfer-initiation'=>false,
	'application/vnd.audiograph'=>false,
	'application/vnd.bmi'=>false,
	'application/vnd.businessobjects'=>false,
	'application/vnd.canon-cpdl'=>false,
	'application/vnd.canon-lips'=>false,
	'application/vnd.cinderella'=>['cdy'],
	'application/vnd.claymore'=>false,
	'application/vnd.commerce-battelle'=>false,
	'application/vnd.commonspace'=>false,
	'application/vnd.comsocaller'=>false,
	'application/vnd.contact.cmsg'=>false,
	'application/vnd.cosmocaller'=>false,
	'application/vnd.ctc-posml'=>false,
	'application/vnd.cups-postscript'=>false,
	'application/vnd.cups-raster'=>false,
	'application/vnd.cups-raw'=>false,
	'application/vnd.cybank'=>false,
	'application/vnd.debian.binary-package'=>['deb','ddeb','udeb'],
	'application/vnd.dna'=>false,
	'application/vnd.dpgraph'=>false,
	'application/vnd.dxr'=>false,
	'application/vnd.ecdis-update'=>false,
	'application/vnd.ecowin.chart'=>false,
	'application/vnd.ecowin.filerequest'=>false,
	'application/vnd.ecowin.fileupdate'=>false,
	'application/vnd.ecowin.series'=>false,
	'application/vnd.ecowin.seriesrequest'=>false,
	'application/vnd.ecowin.seriesupdate'=>false,
	'application/vnd.enliven'=>false,
	'application/vnd.epson.esf'=>false,
	'application/vnd.epson.msf'=>false,
	'application/vnd.epson.quickanime'=>false,
	'application/vnd.epson.salt'=>false,
	'application/vnd.epson.ssf'=>false,
	'application/vnd.ericsson.quickcall'=>false,
	'application/vnd.eudora.data'=>false,
	'application/vnd.fdf'=>false,
	'application/vnd.ffsns'=>false,
	'application/vnd.flographit'=>false,
	'application/vnd.font-fontforge-sfd'=>['sfd'],
	'application/vnd.framemaker'=>false,
	'application/vnd.fsc.weblaunch'=>false,
	'application/vnd.fujitsu.oasys'=>false,
	'application/vnd.fujitsu.oasys2'=>false,
	'application/vnd.fujitsu.oasys3'=>false,
	'application/vnd.fujitsu.oasysgp'=>false,
	'application/vnd.fujitsu.oasysprs'=>false,
	'application/vnd.fujixerox.ddd'=>false,
	'application/vnd.fujixerox.docuworks'=>false,
	'application/vnd.fujixerox.docuworks.binder'=>false,
	'application/vnd.fut-misnet'=>false,
	'application/vnd.google-earth.kml+xml'=>['kml'],
	'application/vnd.google-earth.kmz'=>['kmz'],
	'application/vnd.grafeq'=>false,
	'application/vnd.groove-account'=>false,
	'application/vnd.groove-identity-message'=>false,
	'application/vnd.groove-injector'=>false,
	'application/vnd.groove-tool-message'=>false,
	'application/vnd.groove-tool-template'=>false,
	'application/vnd.groove-vcard'=>false,
	'application/vnd.hhe.lesson-player'=>false,
	'application/vnd.hp-HPGL'=>false,
	'application/vnd.hp-PCL'=>false,
	'application/vnd.hp-PCLXL'=>false,
	'application/vnd.hp-hpid'=>false,
	'application/vnd.hp-hps'=>false,
	'application/vnd.httphone'=>false,
	'application/vnd.hzn-3d-crossword'=>false,
	'application/vnd.ibm.MiniPay'=>false,
	'application/vnd.ibm.afplinedata'=>false,
	'application/vnd.ibm.modcap'=>false,
	'application/vnd.informix-visionary'=>false,
	'application/vnd.intercon.formnet'=>false,
	'application/vnd.intertrust.digibox'=>false,
	'application/vnd.intertrust.nncp'=>false,
	'application/vnd.intu.qbo'=>false,
	'application/vnd.intu.qfx'=>false,
	'application/vnd.irepository.package+xml'=>false,
	'application/vnd.is-xpr'=>false,
	'application/vnd.japannet-directory-service'=>false,
	'application/vnd.japannet-jpnstore-wakeup'=>false,
	'application/vnd.japannet-payment-wakeup'=>false,
	'application/vnd.japannet-registration'=>false,
	'application/vnd.japannet-registration-wakeup'=>false,
	'application/vnd.japannet-setstore-wakeup'=>false,
	'application/vnd.japannet-verification'=>false,
	'application/vnd.japannet-verification-wakeup'=>false,
	'application/vnd.koan'=>false,
	'application/vnd.lotus-1-2-3'=>false,
	'application/vnd.lotus-approach'=>false,
	'application/vnd.lotus-freelance'=>false,
	'application/vnd.lotus-notes'=>false,
	'application/vnd.lotus-organizer'=>false,
	'application/vnd.lotus-screencam'=>false,
	'application/vnd.lotus-wordpro'=>false,
	'application/vnd.mcd'=>false,
	'application/vnd.mediastation.cdkey'=>false,
	'application/vnd.meridian-slingshot'=>false,
	'application/vnd.mif'=>false,
	'application/vnd.minisoft-hp3000-save'=>false,
	'application/vnd.mitsubishi.misty-guard.trustweb'=>false,
	'application/vnd.mobius.daf'=>false,
	'application/vnd.mobius.dis'=>false,
	'application/vnd.mobius.msl'=>false,
	'application/vnd.mobius.plc'=>false,
	'application/vnd.mobius.txf'=>false,
	'application/vnd.motorola.flexsuite'=>false,
	'application/vnd.motorola.flexsuite.adsi'=>false,
	'application/vnd.motorola.flexsuite.fis'=>false,
	'application/vnd.motorola.flexsuite.gotap'=>false,
	'application/vnd.motorola.flexsuite.kmr'=>false,
	'application/vnd.motorola.flexsuite.ttc'=>false,
	'application/vnd.motorola.flexsuite.wem'=>false,
	'application/vnd.mozilla.xul+xml'=>['xul'],
	'application/vnd.ms-artgalry'=>false,
	'application/vnd.ms-asf'=>false,
	'application/vnd.ms-excel'=>['xls','xlb','xlt'],
	'application/vnd.ms-excel.addin.macroEnabled.12'=>['xlam'],
	'application/vnd.ms-excel.sheet.binary.macroEnabled.12'=>['xlsb'],
	'application/vnd.ms-excel.sheet.macroEnabled.12'=>['xlsm'],
	'application/vnd.ms-excel.template.macroEnabled.12'=>['xltm'],
	'application/vnd.ms-fontobject'=>['eot'],
	'application/vnd.ms-lrm'=>false,
	'application/vnd.ms-officetheme'=>['thmx'],
	'application/vnd.ms-pki.seccat'=>['cat'],
	'application/vnd.ms-powerpoint'=>['ppt','pps'],
	'application/vnd.ms-powerpoint.addin.macroEnabled.12'=>['ppam'],
	'application/vnd.ms-powerpoint.presentation.macroEnabled.12'=>['pptm'],
	'application/vnd.ms-powerpoint.slide.macroEnabled.12'=>['sldm'],
	'application/vnd.ms-powerpoint.slideshow.macroEnabled.12'=>['ppsm'],
	'application/vnd.ms-powerpoint.template.macroEnabled.12'=>['potm'],
	'application/vnd.ms-project'=>false,
	'application/vnd.ms-tnef'=>false,
	'application/vnd.ms-word.document.macroEnabled.12'=>['docm'],
	'application/vnd.ms-word.template.macroEnabled.12'=>['dotm'],
	'application/vnd.ms-works'=>false,
	'application/vnd.mseq'=>false,
	'application/vnd.msign'=>false,
	'application/vnd.music-niff'=>false,
	'application/vnd.musician'=>false,
	'application/vnd.netfpx'=>false,
	'application/vnd.noblenet-directory'=>false,
	'application/vnd.noblenet-sealer'=>false,
	'application/vnd.noblenet-web'=>false,
	'application/vnd.novadigm.EDM'=>false,
	'application/vnd.novadigm.EDX'=>false,
	'application/vnd.novadigm.EXT'=>false,
	'application/vnd.oasis.opendocument.chart'=>['odc'],
	'application/vnd.oasis.opendocument.database'=>['odb'],
	'application/vnd.oasis.opendocument.formula'=>['odf'],
	'application/vnd.oasis.opendocument.graphics'=>['odg'],
	'application/vnd.oasis.opendocument.graphics-template'=>['otg'],
	'application/vnd.oasis.opendocument.image'=>['odi'],
	'application/vnd.oasis.opendocument.presentation'=>['odp'],
	'application/vnd.oasis.opendocument.presentation-template'=>['otp'],
	'application/vnd.oasis.opendocument.spreadsheet'=>['ods'],
	'application/vnd.oasis.opendocument.spreadsheet-template'=>['ots'],
	'application/vnd.oasis.opendocument.text'=>['odt'],
	'application/vnd.oasis.opendocument.text-master'=>['odm'],
	'application/vnd.oasis.opendocument.text-template'=>['ott'],
	'application/vnd.oasis.opendocument.text-web'=>['oth'],
	'application/vnd.openxmlformats-officedocument.presentationml.presentation'=>['pptx'],
	'application/vnd.openxmlformats-officedocument.presentationml.slide'=>['sldx'],
	'application/vnd.openxmlformats-officedocument.presentationml.slideshow'=>['ppsx'],
	'application/vnd.openxmlformats-officedocument.presentationml.template'=>['potx'],
	'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'=>['xlsx'],
	'application/vnd.openxmlformats-officedocument.spreadsheetml.template'=>['xltx'],
	'application/vnd.openxmlformats-officedocument.wordprocessingml.document'=>['docx'],
	'application/vnd.openxmlformats-officedocument.wordprocessingml.template'=>['dotx'],
	'application/vnd.osa.netdeploy'=>false,
	'application/vnd.palm'=>false,
	'application/vnd.pg.format'=>false,
	'application/vnd.pg.osasli'=>false,
	'application/vnd.powerbuilder6'=>false,
	'application/vnd.powerbuilder6-s'=>false,
	'application/vnd.powerbuilder7'=>false,
	'application/vnd.powerbuilder7-s'=>false,
	'application/vnd.powerbuilder75'=>false,
	'application/vnd.powerbuilder75-s'=>false,
	'application/vnd.previewsystems.box'=>false,
	'application/vnd.publishare-delta-tree'=>false,
	'application/vnd.pvi.ptid1'=>false,
	'application/vnd.pwg-xhtml-print+xml'=>false,
	'application/vnd.rapid'=>false,
	'application/vnd.rim.cod'=>['cod'],
	'application/vnd.s3sms'=>false,
	'application/vnd.seemail'=>false,
	'application/vnd.shana.informed.formdata'=>false,
	'application/vnd.shana.informed.formtemplate'=>false,
	'application/vnd.shana.informed.interchange'=>false,
	'application/vnd.shana.informed.package'=>false,
	'application/vnd.smaf'=>['mmf'],
	'application/vnd.sss-cod'=>false,
	'application/vnd.sss-dtf'=>false,
	'application/vnd.sss-ntf'=>false,
	'application/vnd.stardivision.calc'=>['sdc'],
	'application/vnd.stardivision.chart'=>['sds'],
	'application/vnd.stardivision.draw'=>['sda'],
	'application/vnd.stardivision.impress'=>['sdd'],
	'application/vnd.stardivision.math'=>['sdf'],
	'application/vnd.stardivision.writer'=>['sdw'],
	'application/vnd.stardivision.writer-global'=>['sgl'],
	'application/vnd.street-stream'=>false,
	'application/vnd.sun.xml.calc'=>['sxc'],
	'application/vnd.sun.xml.calc.template'=>['stc'],
	'application/vnd.sun.xml.draw'=>['sxd'],
	'application/vnd.sun.xml.draw.template'=>['std'],
	'application/vnd.sun.xml.impress'=>['sxi'],
	'application/vnd.sun.xml.impress.template'=>['sti'],
	'application/vnd.sun.xml.math'=>['sxm'],
	'application/vnd.sun.xml.writer'=>['sxw'],
	'application/vnd.sun.xml.writer.global'=>['sxg'],
	'application/vnd.sun.xml.writer.template'=>['stw'],
	'application/vnd.svd'=>false,
	'application/vnd.swiftview-ics'=>false,
	'application/vnd.symbian.install'=>['sis'],
	'application/vnd.tcpdump.pcap'=>['cap','pcap'],
	'application/vnd.triscape.mxs'=>false,
	'application/vnd.trueapp'=>false,
	'application/vnd.truedoc'=>false,
	'application/vnd.tve-trigger'=>false,
	'application/vnd.ufdl'=>false,
	'application/vnd.uplanet.alert'=>false,
	'application/vnd.uplanet.alert-wbxml'=>false,
	'application/vnd.uplanet.bearer-choice'=>false,
	'application/vnd.uplanet.bearer-choice-wbxml'=>false,
	'application/vnd.uplanet.cacheop'=>false,
	'application/vnd.uplanet.cacheop-wbxml'=>false,
	'application/vnd.uplanet.channel'=>false,
	'application/vnd.uplanet.channel-wbxml'=>false,
	'application/vnd.uplanet.list'=>false,
	'application/vnd.uplanet.list-wbxml'=>false,
	'application/vnd.uplanet.listcmd'=>false,
	'application/vnd.uplanet.listcmd-wbxml'=>false,
	'application/vnd.uplanet.signal'=>false,
	'application/vnd.vcx'=>false,
	'application/vnd.vectorworks'=>false,
	'application/vnd.vidsoft.vidconference'=>false,
	'application/vnd.visio'=>['vsd','vst','vsw','vss'],
	'application/vnd.vividence.scriptfile'=>false,
	'application/vnd.wap.sic'=>false,
	'application/vnd.wap.slc'=>false,
	'application/vnd.wap.wbxml'=>['wbxml'],
	'application/vnd.wap.wmlc'=>['wmlc'],
	'application/vnd.wap.wmlscriptc'=>['wmlsc'],
	'application/vnd.webturbo'=>false,
	'application/vnd.wordperfect'=>['wpd'],
	'application/vnd.wordperfect5.1'=>['wp5'],
	'application/vnd.wrq-hp3000-labelled'=>false,
	'application/vnd.wt.stf'=>false,
	'application/vnd.xara'=>false,
	'application/vnd.xfdl'=>false,
	'application/vnd.yellowriver-custom-menu'=>false,
	'application/zlib'=>false,
	'application/x-123'=>['wk'],
	'application/x-7z-compressed'=>['7z'],
	'application/x-abiword'=>['abw'],
	'application/x-apple-diskimage'=>['dmg'],
	'application/x-bcpio'=>['bcpio'],
	'application/x-bittorrent'=>['torrent'],
	'application/x-cab'=>['cab'],
	'application/x-cbr'=>['cbr'],
	'application/x-cbz'=>['cbz'],
	'application/x-cdf'=>['cdf','cda'],
	'application/x-cdlink'=>['vcd'],
	'application/x-chess-pgn'=>['pgn'],
	'application/x-comsol'=>['mph'],
	'application/x-core'=>false,
	'application/x-cpio'=>['cpio'],
	'application/x-csh'=>['csh'],
	'application/x-debian-package'=>['deb','udeb'],
	'application/x-director'=>['dcr','dir','dxr'],
	'application/x-dms'=>['dms'],
	'application/x-doom'=>['wad'],
	'application/x-dvi'=>['dvi'],
	'application/x-executable'=>false,
	'application/x-font'=>['pfa','pfb','gsf'],
	'application/x-font-pcf'=>['pcf','pcf.Z'],
	'application/x-freemind'=>['mm'],
	'application/x-futuresplash'=>['spl'],
	'application/x-ganttproject'=>['gan'],
	'application/x-gnumeric'=>['gnumeric'],
	'application/x-go-sgf'=>['sgf'],
	'application/x-graphing-calculator'=>['gcf'],
	'application/x-gtar'=>['gtar'],
	'application/x-gtar-compressed'=>['tgz','taz'],
	'application/x-hdf'=>['hdf'],
	'application/x-hwp'=>['hwp'],
	'application/x-ica'=>['ica'],
	'application/x-info'=>['info'],
	'application/x-internet-signup'=>['ins','isp'],
	'application/x-iphone'=>['iii'],
	'application/x-iso9660-image'=>['iso'],
	'application/x-jam'=>['jam'],
	'application/x-java-applet'=>false,
	'application/x-java-bean'=>false,
	'application/x-java-jnlp-file'=>['jnlp'],
	'application/x-jmol'=>['jmz'],
	'application/x-kchart'=>['chrt'],
	'application/x-kdelnk'=>false,
	'application/x-killustrator'=>['kil'],
	'application/x-koan'=>['skp','skd','skt','skm'],
	'application/x-kpresenter'=>['kpr','kpt'],
	'application/x-kspread'=>['ksp'],
	'application/x-kword'=>['kwd','kwt'],
	'application/x-latex'=>['latex'],
	'application/x-lha'=>['lha'],
	'application/x-lyx'=>['lyx'],
	'application/x-lzh'=>['lzh'],
	'application/x-lzx'=>['lzx'],
	'application/x-maker'=>['frm','maker','frame','fm','fb','book','fbdoc'],
	'application/x-mif'=>['mif'],
	'application/x-mpegURL'=>['m3u8'],
	'application/x-ms-application'=>['application'],
	'application/x-ms-manifest'=>['manifest'],
	'application/x-ms-wmd'=>['wmd'],
	'application/x-ms-wmz'=>['wmz'],
	'application/x-msdos-program'=>['com','exe','bat','dll'],
	'application/x-msi'=>['msi'],
	'application/x-netcdf'=>['nc'],
	'application/x-ns-proxy-autoconfig'=>['pac'],
	'application/x-nwc'=>['nwc'],
	'application/x-object'=>['o'],
	'application/x-oz-application'=>['oza'],
	'application/x-pkcs7-certreqresp'=>['p7r'],
	'application/x-pkcs7-crl'=>['crl'],
	'application/x-python-code'=>['pyc','pyo'],
	'application/x-qgis'=>['qgs','shp','shx'],
	'application/x-quicktimeplayer'=>['qtl'],
	'application/x-rdp'=>['rdp'],
	'application/x-redhat-package-manager'=>['rpm'],
	'application/x-rss+xml'=>['rss'],
	'application/x-ruby'=>['rb'],
	'application/x-rx'=>false,
	'application/x-scilab'=>['sci','sce'],
	'application/x-scilab-xcos'=>['xcos'],
	'application/x-sh'=>['sh'],
	'application/x-shar'=>['shar'],
	'application/x-shellscript'=>false,
	'application/x-shockwave-flash'=>['swf','swfl'],
	'application/x-silverlight'=>['scr'],
	'application/x-sql'=>['sql'],
	'application/x-stuffit'=>['sit','sitx'],
	'application/x-sv4cpio'=>['sv4cpio'],
	'application/x-sv4crc'=>['sv4crc'],
	'application/x-tar'=>['tar'],
	'application/x-tcl'=>['tcl'],
	'application/x-tex-gf'=>['gf'],
	'application/x-tex-pk'=>['pk'],
	'application/x-texinfo'=>['texinfo','texi'],
	'application/x-trash'=>['~','%','bak','old','sik'],
	'application/x-troff'=>['t','tr','roff'],
	'application/x-troff-man'=>['man'],
	'application/x-troff-me'=>['me'],
	'application/x-troff-ms'=>['ms'],
	'application/x-ustar'=>['ustar'],
	'application/x-videolan'=>false,
	'application/x-wais-source'=>['src'],
	'application/x-wingz'=>['wz'],
	'application/x-x509-ca-cert'=>['crt'],
	'application/x-xcf'=>['xcf'],
	'application/x-xfig'=>['fig'],
	'application/x-xpinstall'=>['xpi'],
	'application/x-xz'=>['xz'],
	'audio/32kadpcm'=>false,
	'audio/3gpp'=>false,
	'audio/amr'=>['amr'],
	'audio/amr-wb'=>['awb'],
	'audio/annodex'=>['axa'],
	'audio/basic'=>['au','snd'],
	'audio/csound'=>['csd','orc','sco'],
	'audio/flac'=>['flac'],
	'audio/g.722.1'=>false,
	'audio/l16'=>false,
	'audio/midi'=>['mid','midi','kar'],
	'audio/mp4a-latm'=>false,
	'audio/mpa-robust'=>false,
	'audio/mpeg'=>['mpga','mpega','mp2','mp3','m4a'],
	'audio/mpegurl'=>['m3u'],
	'audio/ogg'=>['oga','ogg','opus','spx'],
	'audio/parityfec'=>false,
	'audio/prs.sid'=>['sid'],
	'audio/telephone-event'=>false,
	'audio/tone'=>false,
	'audio/vnd.cisco.nse'=>false,
	'audio/vnd.cns.anp1'=>false,
	'audio/vnd.cns.inf1'=>false,
	'audio/vnd.digital-winds'=>false,
	'audio/vnd.everad.plj'=>false,
	'audio/vnd.lucent.voice'=>false,
	'audio/vnd.nortel.vbk'=>false,
	'audio/vnd.nuera.ecelp4800'=>false,
	'audio/vnd.nuera.ecelp7470'=>false,
	'audio/vnd.nuera.ecelp9600'=>false,
	'audio/vnd.octel.sbc'=>false,
	'audio/vnd.qcelp'=>false,
	'audio/vnd.rhetorex.32kadpcm'=>false,
	'audio/vnd.vmx.cvsd'=>false,
	'audio/x-aiff'=>['aif','aiff','aifc'],
	'audio/x-gsm'=>['gsm'],
	'audio/x-mpegurl'=>['m3u'],
	'audio/x-ms-wma'=>['wma'],
	'audio/x-ms-wax'=>['wax'],
	'audio/x-pn-realaudio-plugin'=>false,
	'audio/x-pn-realaudio'=>['ra','rm','ram'],
	'audio/x-realaudio'=>['ra'],
	'audio/x-scpls'=>['pls'],
	'audio/x-sd2'=>['sd2'],
	'audio/x-wav'=>['wav'],
	'chemical/x-alchemy'=>['alc'],
	'chemical/x-cache'=>['cac','cache'],
	'chemical/x-cache-csf'=>['csf'],
	'chemical/x-cactvs-binary'=>['cbin','cascii','ctab'],
	'chemical/x-cdx'=>['cdx'],
	'chemical/x-cerius'=>['cer'],
	'chemical/x-chem3d'=>['c3d'],
	'chemical/x-chemdraw'=>['chm'],
	'chemical/x-cif'=>['cif'],
	'chemical/x-cmdf'=>['cmdf'],
	'chemical/x-cml'=>['cml'],
	'chemical/x-compass'=>['cpa'],
	'chemical/x-crossfire'=>['bsd'],
	'chemical/x-csml'=>['csml','csm'],
	'chemical/x-ctx'=>['ctx'],
	'chemical/x-cxf'=>['cxf','cef'],
	'chemical/x-embl-dl-nucleotide'=>['emb','embl'],
	'chemical/x-galactic-spc'=>['spc'],
	'chemical/x-gamess-input'=>['inp','gam','gamin'],
	'chemical/x-gaussian-checkpoint'=>['fch','fchk'],
	'chemical/x-gaussian-cube'=>['cub'],
	'chemical/x-gaussian-input'=>['gau','gjc','gjf'],
	'chemical/x-gaussian-log'=>['gal'],
	'chemical/x-gcg8-sequence'=>['gcg'],
	'chemical/x-genbank'=>['gen'],
	'chemical/x-hin'=>['hin'],
	'chemical/x-isostar'=>['istr','ist'],
	'chemical/x-jcamp-dx'=>['jdx','dx'],
	'chemical/x-kinemage'=>['kin'],
	'chemical/x-macmolecule'=>['mcm'],
	'chemical/x-macromodel-input'=>['mmd','mmod'],
	'chemical/x-mdl-molfile'=>['mol'],
	'chemical/x-mdl-rdfile'=>['rd'],
	'chemical/x-mdl-rxnfile'=>['rxn'],
	'chemical/x-mdl-sdfile'=>['sd','sdf'],
	'chemical/x-mdl-tgf'=>['tgf'],
	'chemical/x-mmcif'=>['mcif'],
	'chemical/x-mol2'=>['mol2'],
	'chemical/x-molconn-Z'=>['b'],
	'chemical/x-mopac-graph'=>['gpt'],
	'chemical/x-mopac-input'=>['mop','mopcrt','mpc','zmt'],
	'chemical/x-mopac-out'=>['moo'],
	'chemical/x-mopac-vib'=>['mvb'],
	'chemical/x-ncbi-asn1'=>['asn'],
	'chemical/x-ncbi-asn1-ascii'=>['prt','ent'],
	'chemical/x-ncbi-asn1-binary'=>['val','aso'],
	'chemical/x-ncbi-asn1-spec'=>['asn'],
	'chemical/x-pdb'=>['pdb','ent'],
	'chemical/x-rosdal'=>['ros'],
	'chemical/x-swissprot'=>['sw'],
	'chemical/x-vamas-iso14976'=>['vms'],
	'chemical/x-vmd'=>['vmd'],
	'chemical/x-xtel'=>['xtel'],
	'chemical/x-xyz'=>['xyz'],
	'image/cgm'=>false,
	'image/g3fax'=>false,
	'image/gif'=>['gif'],
	'image/ief'=>['ief'],
	'image/jp2'=>['jp2','jpg2'],
	'image/jpeg'=>['jpeg','jpg','jpe'],
	'image/jpm'=>['jpm'],
	'image/jpx'=>['jpx','jpf'],
	'image/naplps'=>false,
	'image/pcx'=>['pcx'],
	'image/png'=>['png'],
	'image/prs.btif'=>false,
	'image/prs.pti'=>false,
	'image/svg+xml'=>['svg','svgz'],
	'image/tiff'=>['tiff','tif'],
	'image/vnd.cns.inf2'=>false,
	'image/vnd.djvu'=>['djvu','djv'],
	'image/vnd.dwg'=>false,
	'image/vnd.dxf'=>false,
	'image/vnd.fastbidsheet'=>false,
	'image/vnd.fpx'=>false,
	'image/vnd.fst'=>false,
	'image/vnd.fujixerox.edmics-mmr'=>false,
	'image/vnd.fujixerox.edmics-rlc'=>false,
	'image/vnd.microsoft.icon'=>['ico'],
	'image/vnd.mix'=>false,
	'image/vnd.net-fpx'=>false,
	'image/vnd.svf'=>false,
	'image/vnd.wap.wbmp'=>['wbmp'],
	'image/vnd.xiff'=>false,
	'image/x-canon-cr2'=>['cr2'],
	'image/x-canon-crw'=>['crw'],
	'image/x-cmu-raster'=>['ras'],
	'image/x-coreldraw'=>['cdr'],
	'image/x-coreldrawpattern'=>['pat'],
	'image/x-coreldrawtemplate'=>['cdt'],
	'image/x-corelphotopaint'=>['cpt'],
	'image/x-epson-erf'=>['erf'],
	'image/x-icon'=>false,
	'image/x-jg'=>['art'],
	'image/x-jng'=>['jng'],
	'image/x-ms-bmp'=>['bmp'],
	'image/x-nikon-nef'=>['nef'],
	'image/x-olympus-orf'=>['orf'],
	'image/x-photoshop'=>['psd'],
	'image/x-portable-anymap'=>['pnm'],
	'image/x-portable-bitmap'=>['pbm'],
	'image/x-portable-graymap'=>['pgm'],
	'image/x-portable-pixmap'=>['ppm'],
	'image/x-rgb'=>['rgb'],
	'image/x-xbitmap'=>['xbm'],
	'image/x-xpixmap'=>['xpm'],
	'image/x-xwindowdump'=>['xwd'],
	'inode/chardevice'=>false,
	'inode/blockdevice'=>false,
	'inode/directory-locked'=>false,
	'inode/directory'=>false,
	'inode/fifo'=>false,
	'inode/socket'=>false,
	'message/delivery-status'=>false,
	'message/disposition-notification'=>false,
	'message/external-body'=>false,
	'message/http'=>false,
	'message/s-http'=>false,
	'message/news'=>false,
	'message/partial'=>false,
	'message/rfc822'=>['eml'],
	'model/iges'=>['igs','iges'],
	'model/mesh'=>['msh','mesh','silo'],
	'model/vnd.dwf'=>false,
	'model/vnd.flatland.3dml'=>false,
	'model/vnd.gdl'=>false,
	'model/vnd.gs-gdl'=>false,
	'model/vnd.gtw'=>false,
	'model/vnd.mts'=>false,
	'model/vnd.vtu'=>false,
	'model/vrml'=>['wrl','vrml'],
	'model/x3d+vrml'=>['x3dv'],
	'model/x3d+xml'=>['x3d'],
	'model/x3d+binary'=>['x3db'],
	'multipart/alternative'=>false,
	'multipart/appledouble'=>false,
	'multipart/byteranges'=>false,
	'multipart/digest'=>false,
	'multipart/encrypted'=>false,
	'multipart/form-data'=>false,
	'multipart/header-set'=>false,
	'multipart/mixed'=>false,
	'multipart/parallel'=>false,
	'multipart/related'=>false,
	'multipart/report'=>false,
	'multipart/signed'=>false,
	'multipart/voice-message'=>false,
	'text/cache-manifest'=>['appcache'],
	'text/calendar'=>['ics','icz'],
	'text/css'=>['css'],
	'text/csv'=>['csv'],
	'text/directory'=>false,
	'text/english'=>false,
	'text/enriched'=>false,
	'text/h323'=>['323'],
	'text/html'=>['html','htm','shtml'],
	'text/iuls'=>['uls'],
	'text/mathml'=>['mml'],
	'text/parityfec'=>false,
	'text/plain'=>['asc','txt','text','pot','brf','srt'],
	'text/prs.lines.tag'=>false,
	'text/rfc822-headers'=>false,
	'text/richtext'=>['rtx'],
	'text/rtf'=>false,
	'text/scriptlet'=>['sct','wsc'],
	'text/t140'=>false,
	'text/texmacs'=>['tm'],
	'text/tab-separated-values'=>['tsv'],
	'text/turtle'=>['ttl'],
	'text/uri-list'=>false,
	'text/vcard'=>['vcf','vcard'],
	'text/vnd.abc'=>false,
	'text/vnd.curl'=>false,
	'text/vnd.debian.copyright'=>false,
	'text/vnd.DMClientScript'=>false,
	'text/vnd.flatland.3dml'=>false,
	'text/vnd.fly'=>false,
	'text/vnd.fmi.flexstor'=>false,
	'text/vnd.in3d.3dml'=>false,
	'text/vnd.in3d.spot'=>false,
	'text/vnd.IPTC.NewsML'=>false,
	'text/vnd.IPTC.NITF'=>false,
	'text/vnd.latex-z'=>false,
	'text/vnd.motorola.reflex'=>false,
	'text/vnd.ms-mediapackage'=>false,
	'text/vnd.sun.j2me.app-descriptor'=>['jad'],
	'text/vnd.wap.si'=>false,
	'text/vnd.wap.sl'=>false,
	'text/vnd.wap.wml'=>['wml'],
	'text/vnd.wap.wmlscript'=>['wmls'],
	'text/x-bibtex'=>['bib'],
	'text/x-boo'=>['boo'],
	'text/x-c++hdr'=>['h++','hpp','hxx','hh'],
	'text/x-c++src'=>['c++','cpp','cxx','cc'],
	'text/x-chdr'=>['h'],
	'text/x-component'=>['htc'],
	'text/x-crontab'=>false,
	'text/x-csh'=>['csh'],
	'text/x-csrc'=>['c'],
	'text/x-dsrc'=>['d'],
	'text/x-diff'=>['diff','patch'],
	'text/x-haskell'=>['hs'],
	'text/x-java'=>['java'],
	'text/x-lilypond'=>['ly'],
	'text/x-literate-haskell'=>['lhs'],
	'text/x-makefile'=>false,
	'text/x-moc'=>['moc'],
	'text/x-pascal'=>['p','pas'],
	'text/x-pcs-gcd'=>['gcd'],
	'text/x-perl'=>['pl','pm'],
	'text/x-python'=>['py'],
	'text/x-scala'=>['scala'],
	'text/x-server-parsed-html'=>false,
	'text/x-setext'=>['etx'],
	'text/x-sfv'=>['sfv'],
	'text/x-sh'=>['sh'],
	'text/x-tcl'=>['tcl','tk'],
	'text/x-tex'=>['tex','ltx','sty','cls'],
	'text/x-vcalendar'=>['vcs'],
	'video/3gpp'=>['3gp'],
	'video/annodex'=>['axv'],
	'video/dl'=>['dl'],
	'video/dv'=>['dif','dv'],
	'video/fli'=>['fli'],
	'video/gl'=>['gl'],
	'video/mpeg'=>['mpeg','mpg','mpe'],
	'video/MP2T'=>['ts'],
	'video/mp4'=>['mp4'],
	'video/quicktime'=>['qt','mov'],
	'video/mp4v-es'=>false,
	'video/ogg'=>['ogv'],
	'video/parityfec'=>false,
	'video/pointer'=>false,
	'video/webm'=>['webm'],
	'video/vnd.fvt'=>false,
	'video/vnd.motorola.video'=>false,
	'video/vnd.motorola.videop'=>false,
	'video/vnd.mpegurl'=>['mxu'],
	'video/vnd.mts'=>false,
	'video/vnd.nokia.interleaved-multimedia'=>false,
	'video/vnd.vivo'=>false,
	'video/x-flv'=>['flv'],
	'video/x-la-asf'=>['lsf','lsx'],
	'video/x-mng'=>['mng'],
	'video/x-ms-asf'=>['asf','asx'],
	'video/x-ms-wm'=>['wm'],
	'video/x-ms-wmv'=>['wmv'],
	'video/x-ms-wmx'=>['wmx'],
	'video/x-ms-wvx'=>['wvx'],
	'video/x-msvideo'=>['avi'],
	'video/x-sgi-movie'=>['movie'],
	'video/x-matroska'=>['mpv','mkv'],
	'x-conference/x-cooltalk'=>['ice'],
	'x-epoc/x-sisx-app'=>['sisx'],
	'x-world/x-vrml'=>['vrm','vrml','wrl'],
		];
	
	// Reorder certain extensions (we want best extension first)
	$res['text/plain']=array_merge(['txt'],array_diff($res['text/plain'],['txt']));
	$res['image/jpeg']=array_merge(['jpg'],array_diff($res['image/jpeg'],['jpg']));

	// Add invalid, but actually encountered mime types
	$invalid=[
			'image/bmp' => ['bmp'],
			'image/pdf' => ['pdf'],
			'application/x-rar-compressed' => ['rar'],
			'application/x-msdownload' => ['exe'],
			'application/x-msdownload' => ['msi'],
			'application/vnd.ms-cab-compressed' => ['cab'],
			'application/acrobat' => ['pdf'],
			'application/vnd.pdf' => ['pdf'],
			'applications/vnd.pdf' => ['pdf'],
			'application/x-pdf' => ['pdf'],
			'/acrobat-pdf' => ['pdf'],
			'image/vnd.adobe.photoshop' => ['psd'],
			'application/vnd.ms-word' => ['doc'],
			'application/vnd.ms-office' => ['doc'],
			 ];
	$res=array_merge($res,$invalid);

	return $res;
}

/* copied from Horde */
function dlib_mime_text_enriched_render($text)
{
	if (trim($text) == '') {
		return $text;
	}
	// We add space at the beginning and end of the string as it will
	// make some regular expression checks later much easier (so we
	// don't have to worry about start/end of line characters)
	$text = ' ' . $text . ' ';

	// We need to preserve << tags, so map them to ascii 1 or ascii 255
	// We make the assumption here that there would never be an ascii
	// 1 in an email, which may not be valid, but seems reasonable...
	// ascii 255 would work if for some reason you don't like ascii 1
	// ascii 0 does NOT seem to work for this, though I'm not sure why
	$text = str_replace('<<', chr(1), $text);

	// Remove any unrecognized tags in the text (via RFC minimal specs)
	// any tags we just don't want to implement can also be removed here
	// Note that this will remove any html links, but this is intended
	$implementedTags = '<param><bold><italic><underline><fixed><excerpt>' .
		'<smaller><bigger><center><color><fontfamily>' .
		'<flushleft><flushright><flushboth><paraindent>';
	// $unImplementedTags = '<nofill><lang>';
	$text = strip_tags($text, $implementedTags);

	// restore the << tags as < tags now...
	$text = str_replace(chr(1), '<<', $text);
	// $text = str_replace(chr(255), '<', $text);

	// Get color parameters into a more useable format.
	$text = preg_replace('/<color><param>([\da-fA-F]+),([\da-fA-F]+),([\da-fA-F]+)<\/param>/Uis', '<color r=\1 g=\2 b=\3>', $text);
	$text = preg_replace('/<color><param>(red|blue|green|yellow|cyan|magenta|black|white)<\/param>/Uis', '<color n=\1>', $text);

	// Get font family parameters into a more useable format.
	$text = preg_replace('/<fontfamily><param>(\w+)<\/param>/Uis', '<fontfamily f=\1>', $text);

	// Just remove any remaining parameters -- we won't use
	// them. Any tags with parameters that we want to implement
	// will have to come before this Someday we hope to use these
	// tags (e.g. for <color><param> tags)
	$text = preg_replace('/<param>.*<\/param>/Uis', '', $text);

	// Single line breaks become spaces, double line breaks are a
	// real break. This needs to do <nofill> tracking to be
	// compliant but we don't want to deal with state at this
	// time, so we fake it some day we should rewrite this to
	// handle <nofill> correctly.
	$text = preg_replace('/([^\n])\r\n([^\r])/', '\1 \2', $text);
	$text = preg_replace('/(\r\n)\r\n/', '\1', $text);

	// We try to protect against bad stuff here.
	$text = @htmlspecialchars($text, ENT_QUOTES, 'UTF-8');

	// Now convert the known tags to html. Try to remove any tag
	// parameters to stop people from trying to pull a fast one
	$text = preg_replace('/(?<!&lt;)&lt;bold.*&gt;(.*)&lt;\/bold&gt;/Uis', '<span style="font-weight: bold">\1</span>', $text);
	$text = preg_replace('/(?<!&lt;)&lt;italic.*&gt;(.*)&lt;\/italic&gt;/Uis', '<span style="font-style: italic">\1</span>', $text);
	$text = preg_replace('/(?<!&lt;)&lt;underline.*&gt;(.*)&lt;\/underline&gt;/Uis', '<span style="text-decoration: underline">\1</span>', $text);
	$text = preg_replace_callback('/(?<!&lt;)&lt;color r=([\da-fA-F]+) g=([\da-fA-F]+) b=([\da-fA-F]+)&gt;(.*)&lt;\/color&gt;/Uis', 
								  function($colors)
								  {
									  for ($i = 1; $i < 4; $i++) {
										  $colors[$i] = sprintf('%02X', round(hexdec($colors[$i]) / 255));
									  }
									  return '<span style="color: #' . $colors[1] . $colors[2] . $colors[3] . '">' . $colors[4] . '</span>';
								  }
								  , $text);
	$text = preg_replace('/(?<!&lt;)&lt;color n=(red|blue|green|yellow|cyan|magenta|black|white)&gt;(.*)&lt;\/color&gt;/Uis', '<span style="color: \1">\2</span>', $text);
	$text = preg_replace('/(?<!&lt;)&lt;fontfamily&gt;(.*)&lt;\/fontfamily&gt;/Uis', '\1', $text);
	$text = preg_replace('/(?<!&lt;)&lt;fontfamily f=(\w+)&gt;(.*)&lt;\/fontfamily&gt;/Uis', '<span style="font-family: \1">\2</span>', $text);
	$text = preg_replace('/(?<!&lt;)&lt;smaller.*&gt;/Uis', '<span style="font-size: smaller">', $text);
	$text = preg_replace('/(?<!&lt;)&lt;\/smaller&gt;/Uis', '</span>', $text);
	$text = preg_replace('/(?<!&lt;)&lt;bigger.*&gt;/Uis', '<span style="font-size: larger">', $text);
	$text = preg_replace('/(?<!&lt;)&lt;\/bigger&gt;/Uis', '</span>', $text);
	$text = preg_replace('/(?<!&lt;)&lt;fixed.*&gt;(.*)&lt;\/fixed&gt;/Uis', '<font face="fixed">\1</font>', $text);
	$text = preg_replace('/(?<!&lt;)&lt;center.*&gt;(.*)&lt;\/center&gt;/Uis', '<div align="center">\1</div>', $text);
	$text = preg_replace('/(?<!&lt;)&lt;flushleft.*&gt;(.*)&lt;\/flushleft&gt;/Uis', '<div align="left">\1</div>', $text);
	$text = preg_replace('/(?<!&lt;)&lt;flushright.*&gt;(.*)&lt;\/flushright&gt;/Uis', '<div align="right">\1</div>', $text);
	$text = preg_replace('/(?<!&lt;)&lt;flushboth.*&gt;(.*)&lt;\/flushboth&gt;/Uis', '<div align="justify">\1</div>', $text);
	$text = preg_replace('/(?<!&lt;)&lt;paraindent.*&gt;(.*)&lt;\/paraindent&gt;/Uis', '<blockquote>\1</blockquote>', $text);
	$text = preg_replace('/(?<!&lt;)&lt;excerpt.*&gt;(.*)&lt;\/excerpt&gt;/Uis', '<blockquote>\1</blockquote>', $text);

	// Replace << with < now (from translated HTML form).
	$text = str_replace('&lt;&lt;', '&lt;', $text);

	// Now we remove the leading/trailing space we added at the
	// start.
	$text = preg_replace('/^ (.*) $/s', '\1', $text);

	// Make URLs clickable.
	//	require_once 'Horde/Text/Filter.php';
	//$text = Text_Filter::filter($text, 'linkurls', array('callback' => 'Horde::externalUrl'));

	// Wordwrap -- note this could impact on our above RFC
	// compliance *IF* we honored nofill tags (which we don't
	// yet).
	$text = str_replace("\t", '        ', $text);
	$text = str_replace('  ', ' &nbsp;', $text);
	$text = str_replace("\n ", "\n&nbsp;", $text);
	if ($text[0] == ' ') {
		$text = '&nbsp;' . substr($text, 1);
	}
	$text = nl2br($text);
	$text = '<p class="fixed">' . $text . '</p>';

	return $text;
}

?>