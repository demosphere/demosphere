<?php

/**
 * Simple Object relational mapper/ActiveRecord : 
 * load, save & delete objects from DB.
 *
 * This is deliberately kept fairly simple. A fully fledged ORM (like Propel) 
 * adds too much complexity.
 *
 * A description of fields is stored in an array called $dboStatic['dbschema'].
 * This description helps DBObject construct queries and can be used in other modules, like \ref form (form_dbobject()).
 * $dboStatic['dbschema'] is automatically built from each static member with an "_" appended to its name.
 * For example:
 * \code
 * class MyClass extends DBObject
 * {
 *   public $lastAccess;
 *   static $lastAccess_=array('type'=>'timestamp');
 *   ...
 * }
 * \endcode
 *
 * Syntax of each item in $dboStatic['dbschema'] :
 * - type : field type used for formating db queries.
 *	        Allowed values:autoincrement,int, enum, array, string, html, timestamp,bool,foreign_key... (unspecified types are considered as string)
 * - values: only for enum: list of values that the variable can take
 * - class: only for foreign_key : class name referenced by this foreign_key
 *
 *  The $dboStatic array holds the dbSchema and information pre-calculated from dbSchema for performance reasons.
 *
 */
class DBObject
{
	var $dboNewObjectNotSaved=true;

	//! Returns the name of the primary key (very often "id").
	static function dboGetPKeyName()
	{
		if(static::$dboStatic===false){static::dboStaticInit();}
		return static::$dboStatic['pk'];
	}

	//! Returns the value of the primary key (very often $this->id).
	function getPk()
	{
		if(static::$dboStatic===false){static::dboStaticInit();}
		$pk=static::$dboStatic['pk'];
		return $this->$pk;
	}

	//! Small fixes done when using the PHP clone construct
	//! Note : this is used in performance sensitive code
	function __clone()
	{
		// zero primary key (typically id)
		$pk=static::dboGetPKeyName();
		$this->$pk=null;
		// unset dboUseForeign_ objects (foreign key objects)
		if(isset($this->dboUseForeignList))
		{
			foreach($this->dboUseForeignList as $cachedName=>$u){unset($this->$cachedName);}
		}
		// This is a new object
		$this->dboNewObjectNotSaved=true;
	}

	//! This sets-up information about fields in $dboStatic.
	//! This is called only once for a given class. 
	static function dboStaticInit()
	{
		static::$dboStatic=[];
		
		static::$dboStatic['enum'   ]=[];
		static::$dboStatic['foreign']=[];
		// ********* build list of database fields
		// Use reflexion to find any static vars whose name finishes by "_" and that
		// match another variable with similar name.
		$r=new ReflectionClass(get_called_class());
		$names=dlib_object_column($r->getProperties(),'name');
		foreach($names as $name)
		{
			$varName=substr($name,0,-1);
			if(substr($name,-1)==='_' &&
			   isset(static::$$name) && 
			   array_search($varName,$names)!==false)
			{
				// Ok, we found a schema description, add it to dbschema
				$schema=static::$$name;
				// Add default type of "string"
				if(!isset($schema['type'])){$schema['type']='string';}
				static::$dboStatic['dbschema'][$varName]=$schema;

				// primary key is first field
				if(!isset(static::$dboStatic['pk'])){static::$dboStatic['pk']=$varName;}
				if($schema['type']==='foreign_key' ){static::$dboStatic['foreign'][]=$varName;}
				if($schema['type']==='enum'        ){static::$dboStatic['enum'   ][]=$varName;}
			}
		}
		static::$dboStatic['fields']=array_keys(static::$dboStatic['dbschema']);
	}

	//! Determine $dbschema array from static variables whose name finishes by "_" ($name_).
	//! This is more programmer friendly than having to declare a separate $dbschema array.
	static function getSchema()
	{
		if(static::$dboStatic===false){static::dboStaticInit();}
		return static::$dboStatic['dbschema'];
	}

	//! Returns an array of objects (indexed by id/pk) given a list of ids (primary keys).
	static function fetchListFromIds(array $pks,bool $exceptionOnFail=true)
	{
		$res=[];
		foreach($pks as $pk)
		{
			$obj=static::fetch($pk,$exceptionOnFail);
			if($obj!==null){$res[$pk]=$obj;}
		}
		return $res;
	}

	//! Returns an array of objects indexed by id (pk).
	//! The argument can be a full or incomplete sql query.
	//! If fields are missing from the query, the object will not be fully populated.
	//! In that case you should not call $obj->save().
	//!
	//! Examples: 
	//! \code
	//! $events=Event::fetchList('WHERE changed>%d',time()-3600);
	//! $events=Event::fetchList('FROM Event,Place WHERE Event.placeId=Place.id AND ...');
	//! $events=Event::fetchList('SELECT id,title,startTime FROM Event WHERE ...');
	//! $events=Event::fetchList('SELECT %no[log,body] FROM Event WHERE ...');
	//! \endcode
	static function fetchList()
	{
		if(static::$dboStatic===false){static::dboStaticInit();}
		$args=func_get_args();
		$sql=array_shift($args);
		$class=get_called_class();
		
		$fields=static::$dboStatic['fields'];

		// shortcuts 
		if(strpos($sql,'WHERE'   )===0){$sql='FROM `'.$class.'` '.$sql;}
		if(strpos($sql,'ORDER BY')===0){$sql='FROM `'.$class.'` '.$sql;}
		if(strpos($sql,'FROM'    )===0){$sql='SELECT `'.$class.'`.`'.implode('`,`'.$class.'`.`',$fields).'`  '.$sql;}

		//var_dump('x',$sql,$fields);
		// Replace %no[...] to remove unwanted fields
		if(preg_match('@^([^%]+)%no\[([^\]]*)\](.*)$@',$sql,$matches))
		{
			$nfields=str_replace('`','',explode(',',$matches[2]));
			if(count(array_diff($nfields,$fields))){trigger_error('fetchList: unknown fields in %no[...]',E_USER_ERROR);}
			$fields=array_diff($fields,$nfields);
			$sql=$matches[1].' '.$class.'.`'.implode('`,'.$class.'.`',$fields).'` '.$matches[3];
		}
		
		array_unshift($args,$sql);
		$qres=call_user_func_array('db_query',$args);


		$reflexion=new ReflectionClass($class);
		$res=[];
		while($line=mysqli_fetch_assoc($qres))
		{
			$obj=$reflexion->newInstanceWithoutConstructor();
			$obj->dboCopyFromStringArray($line);
			unset($obj->dboNewObjectNotSaved);
			$res[$obj->getPk()]=$obj;
		}
		return $res;
	}

	//! Loads object from db and returns it. 
	//! This function is the most common way to load objects from db.
	static function fetch($pk,bool $exceptionOnFail=true)
	{
		$pkName=static::dboGetPKeyName();
		$class=get_called_class();
		$res=(new ReflectionClass($class))->newInstanceWithoutConstructor();
		// Note: pk is reloaded in load() : so correct type (generally: int) is enforced
		$res->$pkName=$pk;

		if(!$exceptionOnFail)
		{
			try{$res->load();}
			catch (Exception $e){return null;}
		}
		else{$res->load();}
		return $res;
	}


	//! Automatically called by PHP when undefined methods are called.
	//! This is used to create "magic" methods for enums, foreign-keys, etc.
	//! It is performance sensitive, so name => function mapings are computed ony once and stored in $dboStatic['methods'].
	function __call($method,$args)
	{
		if(method_exists($this,$method))
		{
			return call_user_func_array([$this, $method], $args);
		}
		// Build a map of all possible function names (performance)
		if(!isset(static::$dboStatic['methods']))
		{
			$methods=[];
			$schema=static::getSchema();
			foreach($schema as $name=>$val)
			{
				$camelName=str_replace(' ','',ucwords(str_replace('_',' ',$name)));
				if($val['type']==='enum')
				{
					$methods['get'.$camelName]=['getEnum',$name];
					$methods['set'.$camelName]=['setEnum',$name];
					// PHP wierd behavior: Class::fct() is not static if called inside Class !!
					$methods[lcfirst($camelName).'Enum']=['EnumValue',$name];
				}
				if($val['type']==='foreign_key')
				{
					$shortName=$camelName;
					if(substr($camelName,-2)==='Id'){$shortName=substr($camelName,0,-2);}
					$methods['use'.$shortName]=['useForeign',$name];
					$methods['set'.$shortName]=['setForeign',$name];
				}
			}
			static::$dboStatic['methods']=$methods;
		}
		if(!isset(static::$dboStatic['methods'][$method]))
		{trigger_error("DBObject::__call: non-existant method:".get_called_class()."::$method",E_USER_ERROR);}
		$m=static::$dboStatic['methods'][$method];
		switch($m[0])
		{
		case    'getEnum': return $this->dboGetEnum($m[1]);
		case    'setEnum': $this->dboSetEnum($m[1],$args[0]);return;
		case 'useForeign': return $this->dboUseForeign($m[1],isset($args[0]) ? $args[0] : true);
		case 'setForeign': $this->dboSetForeign($m[1],$args[0]);return;
		case  'EnumValue': return self::dboEnumValue($m[1],$args[0]);
		}
	}

	//! Automatically called by PHP when undefined static functions are called.
	//! This is used to create "magic" static functions for enums.
	static function __callStatic($fct,$args)
	{
		if(static::$dboStatic===false){static::dboStaticInit();}
		//var_dump(static::$dboStatic);
		foreach(static::$dboStatic['enum'] as $name)
		{
			$camelName=str_replace(' ','',ucwords(str_replace('_',' ',$name)));
			if($fct===lcfirst($camelName).'Enum'){return self::dboEnumValue($name,$args[0]);}
		}		
		trigger_error("DBObject::__callstatic: non-existant method:".get_called_class()."::$fct",E_USER_ERROR);
	}

	//! Example: $obj->getModerationStatus() 
	//! (where static $moderationStatus_=array('type'=>'enum','values'=>array(0=>'created',1=>'waiting',...));
	function dboGetEnum(string $name)
	{
		$schema=static::getSchema();
		return $schema[$name]['values'][$this->$name];
	}

	//! Example: $obj->setModerationStatus('waiting') 
	function dboSetEnum(string $name,string $str)
	{
		$schema=static::getSchema();
		$this->$name=array_search($str,$schema[$name]['values']);
	}

	//! Example: MyClass::moderationStatusEnum('waiting')
	static function dboEnumValue(string $name,string $str)
	{
		$schema=static::getSchema();
		return array_search($str,$schema[$name]['values']);
	}

	//! Example: $event->usePlace()
	//! (where Event: static $placeId_ =array('type'=>'foreign_key','class'=>'Place');)
	//! in this example $name='placeId'
	//! Once an foreign object is used, you can change it. It will be saved when $this is saved.
	//! Example $event->usePlace()->address='bla bla';$event->save() will save both $event and the used place.
	//! Once a foreign object is used, you should not change the corresponding id. 
	//! BAD: $event->usePlace(); $event->placeId=123;
	//! Note: this fct asumes that the used class (ex:Place) has a constructor with no args (or default args).
	function dboUseForeign(string $name,bool $exceptionOnFail=true)
	{
		$cachedName='dboUseForeign_'.$name;
		if(!isset($this->$cachedName))
		{
			$schema=static::getSchema();
			$class=$schema[$name]['class'];
			if(!$this->$name){$this->$cachedName=new $class();}
			else{$this->$cachedName=$class::fetch($this->$name,$exceptionOnFail);}
			if(!isset($this->dboUseForeignList)){$this->dboUseForeignList=[];}
			$this->dboUseForeignList[$cachedName]=$name;
		}
		return $this->$cachedName;
	}

	//! Example: $event->setPlace($place) 
	//! Example: $event->setPlace(new Place())
	function dboSetForeign(string $name,$value)
	{
		$cachedName='dboUseForeign_'.$name;
		if($value!==false && $value!==null)
		{
			$this->$name=$value->getPk();
			$this->$cachedName=$value;
			if(!isset($this->dboUseForeignList)){$this->dboUseForeignList=[];}
			$this->dboUseForeignList[$cachedName]=$name;
		}
		else
		{
			$this->$name=null;
			unset($this->$cachedName);
			unset($this->dboUseForeignList[$cachedName]);
		}
	}

	//! Note this is only used internally, you should use ClassName::fetch().
	function load()
	{
		unset($this->dboNewObjectNotSaved);
		$schema=static::getSchema();
		$primaryKeyName  =static::$dboStatic['pk'];
		$primaryKeyFormat=static::dbFormat(static::$dboStatic['dbschema'][$primaryKeyName]['type']);

		$fields=static::$dboStatic['fields'];
		$qres=db_array("SELECT `".implode('`,`',$fields)."` FROM `".get_class($this)."` WHERE ".
					   $primaryKeyName."=".$primaryKeyFormat,$this->$primaryKeyName);
		if($qres===null)
		{
			throw new Exception(get_class($this)."::load failed for ".
								$primaryKeyName.'='.$this->$primaryKeyName);
		}
		$this->dboCopyFromStringArray($qres);
	}

	//! Sets all fields (attributes) of this object from values contained in an associative array.
	//! This function does type casting based on the schema.
	//! It only sets the fields that are present in the values array.
	//! This function is mainly used internally after fetching values from database (load() and fetchList()).
	function dboCopyFromStringArray(array $array,$namePrefix=false)
	{
		$schema=static::getSchema();
		foreach($schema as $name=>$val)
		{
			if(!isset($array[$namePrefix===false ? $name : $namePrefix.$name])){continue;}
			$v=$array[$namePrefix===false ? $name : $namePrefix.$name];
			switch($val['type'])
			{
			case 'int'  : 
			case 'timestamp': 
			case 'autoincrement'  : 
			case 'timestamp'  : 
			case 'foreign_key'  : 
			case 'enum'  : 
				$this->$name=(int)      ($v);break;
			case 'bool'  : 
				$this->$name=(bool)     ($v);break;
			case 'float'  : 
				$this->$name=(float)    ($v);break;
			case 'array': 
				$this->$name=unserialize($v,['allowed_classes'=>false]);break;
			default:
				$this->$name=$v;
			}
		}
	}

	//! Saves this object to database.
	//! Internally, this uses $object->dboNewObjectNotSaved to know if this an INSERT or an UPDATE.
	function save()
	{
		global $dbConnection;
		$schema=static::getSchema();

		// ******* save any cached foreign objects
		if(isset($this->dboUseForeignList))
		{
			foreach($this->dboUseForeignList as $cachedName=>$name)
			{
				$this->$cachedName->save();
				$this->dboSetForeign($name,$this->$cachedName);// set id
			}
		}

		// ******* save this object

		$getNewAutoincremented=false;
		// ignore primary key if it's unset and autoincremented
		$primaryKeyName=static::dboGetPKeyName();
		if($schema[$primaryKeyName]['type']==='autoincrement' &&
		   isset($this->dboNewObjectNotSaved))
		{
			$getNewAutoincremented=true;
			unset($schema[$primaryKeyName]);
		}

		$qargs=[];
		foreach($schema as $name=>$val){$qargs[]=$this->$name;}

		$fields=array_keys($schema);
		if(isset($this->dboNewObjectNotSaved))
		{
			unset($this->dboNewObjectNotSaved);
			$query='INSERT INTO `'.get_class($this).'` '.
				'(`'.implode('`,`',$fields).'`) VALUES(';
			foreach($schema as $name=>$val){$query.=self::dbFormat($val['type']).',';}
			$query=substr($query,0,-1).')';
		}
		else
		{
			$query='UPDATE `'.get_class($this).'` SET ';
			foreach($schema as $name=>$val)
			{
				$query.='`'.$name.'`='.self::dbFormat($val['type']).',';
			}
			$query=substr($query,0,-1).' WHERE `'.
				$primaryKeyName.'`='.self::dbFormat($schema[$primaryKeyName]['type']);
			$qargs[]=$this->$primaryKeyName;
		}

		array_unshift($qargs,$query);
		$qres=call_user_func_array('db_query',$qargs);
		if($getNewAutoincremented){$this->$primaryKeyName=mysqli_insert_id($dbConnection);}
	}

	//! Returns a dlib/db placeholder format (%d,%f,'%s' ...) from a dbschema type ('int','string'...)
	static function dbFormat(string $type)
	{
		switch($type)
		{
			case 'int'  : 
			case 'autoincrement'  : 
			case 'timestamp'  : 
			case 'foreign_key'  : 
			case 'enum'  : 
			case 'bool'  : 
				return '%d';
			case 'float'  : 
				return '%f';
			case 'array': 
				return "'%x'";
			default:
				return "'%s'";
		}
	}

	function delete()
	{
		$class=get_class($this);
		$pkName=static::dboGetPKeyName();
		$schema=static::getSchema();
		$pkFormat=static::dbFormat(static::$dboStatic['dbschema'][$pkName]['type']);
		db_query("DELETE FROM `".$class."` WHERE ".$pkName."=".$pkFormat,$this->$pkName);
	}
}

?>