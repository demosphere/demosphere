<?php

//! General purpose pager : limits the number of items shown on a single page.
//!
//! The pager rendered with pager.tpl.php
//! 
//! ** Usage example with foundRows:
//! $pager=new Pager();
//! $posts=example_query('SELECT SQL_CALC_FOUND_ROWS * FROM posts '.$pager->sql());
//! $pager->foundRows();
//! echo template_render('dlib/pager.tpl.php',array('pager'=>$pager));
//! 
//! ** Usage example without sql:
//! $posts=...;
//! $pager=new Pager(array('nbItems'=>count($posts)));
//! $posts=array_slice($posts,$pager->firstItem,$pager->itemsPerPage);
//! echo template_render('dlib/pager.tpl.php',array('pager'=>$pager));
class Pager
{
	var $nbItems=false;
	var $prefix='';
	var $firstItem;
	var $defaultItemsPerPage=20;
	var $itemsPerPage;
	var $itemsPerPageChoices=[5,10,20,40,100,1000];
	var $itemName=false;
	var $alwaysShow=false;
	var $shortDisplay=false;

	function __construct(array $options=[])
	{
		foreach($options as $opt=>$optVal){$this->$opt=$optVal;}
		if($this->itemName===false){$this->itemName=t('items');}

		// if the current system is using $currentPage, add css (otherwise its up to the user to do it manually)
		global $currentPage;
		if(isset($currentPage)){$currentPage->addCss('dlib/pager.css');}

		// the default items per page should always be in the choice list
		if(array_search($this->defaultItemsPerPage,$this->itemsPerPageChoices)===false)
		{
			$this->itemsPerPageChoices[]=$this->defaultItemsPerPage;
			sort($this->itemsPerPageChoices);
		}

		// Set firstItem from current URL
		$this->firstItem=intval(val($_GET,$this->prefix.'firstItem',0));
		if($this->firstItem<0   ){dlib_bad_request_400('Pager::invalid first item');}
		// Set itemsPerPage from current URL
		$this->itemsPerPage=intval(val($_GET,$this->prefix.'itemsPerPage',$this->defaultItemsPerPage));
		if($this->itemsPerPage<1){dlib_bad_request_400('Pager::bad nb of items per page');}

		// If calling program already knows the nb of items, 
		// we can check for out of bounds values (and redirect)
		if($this->nbItems!==false)
		{
			$this->checkOutOfBounds();
		}
	}

	//! Returns an sql fragment that you can append at the end of a query to add limit/offset.
	function sql()
	{
		$res=sprintf(' LIMIT %d OFFSET %d ',$this->itemsPerPage,$this->firstItem);
		return $res;
	}

	//! Call this after a query that uses SQL_CALC_FOUND_ROWS.
	//! It sets nbItems from number of potential results of previous query.
	//! It also redirects if the pager is out of bounds.
	function foundRows()
	{
		$this->nbItems=db_result('SELECT FOUND_ROWS()');
		$this->checkOutOfBounds();
	}

	function checkOutOfBounds()
	{
		if($this->nbItems==0){return;}
		// make sure we're not past end
		$goodFirstItem=min($this->firstItem,$this->nbItems-1);
		// Actually used first item should always be on page boundary
		$goodFirstItem=$this->itemsPerPage*floor($goodFirstItem/$this->itemsPerPage);
		if($goodFirstItem!=$this->firstItem)
		{
			// Redirect to good firstItem
			$url=$this->url($goodFirstItem);
			dlib_redirect($url);
		}
	}

	function nbPages()
	{
		return (int)(ceil($this->nbItems/$this->itemsPerPage));
	}

	//! Returns the url of the current page, with firstItem itemsPerPage GET args modified.
	//! It is used to provide links to other pages than the current one.
	function url($firstItem,$itemsPerPage=false)
	{
		if($firstItem   ===false){$firstItem   =$this->firstItem;}
		if($itemsPerPage===false){$itemsPerPage=$this->itemsPerPage;}
		$url=$_SERVER['REQUEST_URI'];
		$url=preg_replace('@([?&]'.$this->prefix.'firstItem)=\d+@','',$url);
		$url=preg_replace('@([?&]'.$this->prefix.'itemsPerPage)=\d+@','',$url);
		$url.=(strpos($url,'?')===false?'?':'&').
			$this->prefix.'firstItem='.intval($firstItem);
		$url.='&'.$this->prefix.'itemsPerPage='.intval($itemsPerPage);
		return $url;
	}
}

?>