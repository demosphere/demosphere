<?php

require_once 'test-find-date-time-common.php';
require_once 'test-dlib.php';

function test_find_date_time_pt()
{
	require_once 'dlib/tools.php';
	require_once 'dlib/find-date-time/find-date-time.php';
	require_once 'dlib/find-date-time/find-date-time-pt.php';
	setlocale(LC_TIME, "pt_PT.utf8");
	test_list_find_date_time_pt();
	test_find_times_pt();
	test_random_find_date_time_pt(true);
}

// Automatically generates a large number of random dates and times
// using predefined patterns.
function test_random_find_date_time_pt($verbose=false)
{
	$ok=0;
	$failed=0;
	// try different dates
	for($i=0;$i<100;$i++)
	{
		if($i<50)
		{$dateTS=mktime(0,0,0,1,1,mt_rand(2000,2020))+mt_rand(0,366*24*3600);}
		else{$dateTS=time()+mt_rand(-366*24*3600,366*24*3600);}
		$m=date('m',$dateTS);
		$d=date('d',$dateTS);
		$y=date('Y',$dateTS);
		$dateTS=mktime(0,0,0,$m,$d,$y);
		if($verbose)echo "date:".strftime('%A %e %B %Y - %R',$dateTS)."\n";
		for($t=0;$t<20;$t++)
		{
			$hasTime=true;
			if(true ){$h=mt_rand(0,23);$min=mt_rand(0,59);}
			if($t<15){$h=mt_rand(0,23);$min=30;}
			if($t<10){$h=mt_rand(0,23);$min= 0;}
			if($t==0){$h=			 3;$min=33;$hasTime=false;}
			$hasTime=intval($hasTime);

			$ts=mktime($h,$min,0,$m,$d,$y);

			// has min, has time, has year
			//       y       y      y 
			//       y       y      n   
			//       y       n      y   X
			//       y       n      n   X
			//       n		 y		y
			//		 n		 y		n 
			//		 n		 n		y
			//		 n		 n		n 
			for($type=0;$type<4;$type++)
			{
				$hasMin =$type%2;
				$hasYear=($type>>1)%2;
				if(!$hasTime && $hasMin){continue;}
				if($hasTime && !$hasMin  && $min!=0){continue;}
				if(!$hasYear && abs($ts-time())>(3600*24*30*5)){continue;}

			
				$formats=['%A %e %B %Y - %R',
						  '%e %b %Y - %R',
						  '%d/%m/%Y - %R'];
				if(!$hasYear)
				{
					$formats=array_merge($formats,['%A %e %B - %R',
												   '%d/%m - %R']);
				}

				if(!$hasTime && !$hasYear)
				{
					$formats=array_merge($formats,['%e %B',
												   '%d/%m']);
				}
				if(!$hasTime)
				{
					$formats=array_merge($formats,['%A %e %B %Y']);
					$f0=$formats;
					foreach($f0 as $k=>$f){$formats[$k]=str_replace(' - %R','',$f);}
				}
				foreach($formats as $format)
				{
					$string=strftime($format,$ts);

					//				echo "m:$hasMin t:$hasTime y:$hasYear ".
					//					"time:".strftime('%A %e %B %Y - %R',$ts)." :::: ".
					//					$string."\n";
					$contexts=['sdf sfd %s sdfsd',
							   '%s',
							   '%s csdsdf ',
							   'fsdf %s'];
					foreach($contexts as $context)
					{
						$fstring=$string;
						if(!(mt_rand()%2)){$fstring=str_replace(':','h',$fstring);}
						if(!(mt_rand()%2)){$fstring=str_replace(':','.',$fstring);}
// 						if(!(mt_rand()%4)){$fstring=str_replace('-','des',$fstring);}
// 						if(!(mt_rand()%4)){$fstring=str_replace('-','à',$fstring);}
 						if(!(mt_rand()%4)){$fstring=str_replace('-',',',$fstring);}
						if(!(mt_rand()%4)){$fstring=dlib_remove_accents($fstring);}
						if(!(mt_rand()%4)){$fstring=mb_strtoupper($fstring,'UTF-8');}
						if(!(mt_rand()%4)){$fstring=str_replace('-','&nbsp;',$fstring);}
						else
						if(!(mt_rand()%4)){$fstring=ent($fstring);}
						//echo 'pre-final string:'.$fstring."\n";
						$fstring=str_replace('%s',$fstring,$context);
						//if(!(mt_rand()%1000))echo 'final string:'.$fstring."\n";
						$res=find_dates_and_times($fstring);
						$rts=val($res,0,false);
						if(count($res)!=1 || $rts!=$ts)
						{
							echo "FAILED find_dates_and_times test for $ts \"$fstring\"\n";
							var_dump($res);
							echo "\n";
							$res=find_dates_and_times($fstring,true);
							fatal("abort after fail\n");
							$failed++;
						}
						else{$ok++;}
					}
				}
			}
		}
	}
	echo "test_random_find_date_time_pt: ok:$ok failed:$failed\n";
	test_equals($failed,0);
}

// Test a list of predefined strings.
function test_list_find_date_time_pt()
{
	for($i=0;$i<=7;$i++)
	{
		echo strftime('%a :  %A  :  %c',strtotime('+'.$i.' day',strtotime('monday')))."\n";
	}


	$tests=[
		["17 Fevereiro 2013",'17/2/2013 3:33'],
		["15 de novembro de 2010",'15/11/2010 3:33'],
		["2 e 3 de Fevereiro 2010",['2/2/2010 3:33','3/2/2010 3:33']],
		["3.Fev 2010",'3/2/2010 3:33'],
		["17 Fevereiro 2013 9.31h",'17/2/2013 9:31'],
		["17/03",'17/3 3:33'],
		["17 de março",'17/3 3:33'],
		["17 março 2014",'17/3/2014 3:33'],
		["17.03.2014",'17.3.2014 3:33'],
		["17.março",'17/3 3:33'],
		["17 mar",'17/3 3:33'],
		["1º de maio",'1/5 3:33'],
		["Primeiro de Maio",'1/5 3:33'],
		["12 de Maio",'12/5 3:33'],
		["dia 27 de maio às 19 H",'27/5 19:00'],
		["14 jun 15.30h",'14/6 15:30'],
		["20 março 2014 18h30	 ",'20/3/2014 18:30'],
		["20 março 2014 18h	 ",'20/3/2014 18:00'],
		["20 março 2014 18hs	 ",'20/3/2014 18:00'],
		["20 março 2014 às 18	 ",'20/3/2014 18:00'],
		["20 março 2014 às 18h30",'20/3/2014 18:30'],
		["20 março 2014 às 1810" ,'20/3/2014 3:33'],
		["20 março 2014 18 horas",'20/3/2014 18:00'],
		["20 março 2014 18:30	 ",'20/3/2014 18:30'],
		["20 março 2014 18:30h	 ",'20/3/2014 18:30'],
		["20 março 2014 18:30hs ",'20/3/2014 18:30'],
		["sexta feira 27 junho 2014 18:00 ",'27/6/2014 18:00'],
		["sexta-feira 27 junho 2014 18:00 ",'27/6/2014 18:00'],
		["sexta 27 junho 2014 18:00 ",'27/6/2014 18:00'],

// 18h30
// 18h
// 18hs
// às 18
// 18 horas
// 18:30
// 18:30h
// 18:30hs


// - DATE
// 17/03
// 17 de março
// 17/03/2014
// 17 março 2014
// 17.03.2014
// 17.março
// dia 17
// 17 mar
// segunda, 17
// segunda (17)
// segunda-feira, 17
// segunda-feira (17)
// 1º de maio
// seg 17
//  
// days of the week: segunda (or segunda-feira), terça(-feira),
// quarta(-feira), quinta(-feira), sexta(-feira), sábado, domingo
// abbreviated days of the week: seg, ter, qua, qui, sex, sáb/sab, dom
// months: janeiro, fevereiro, março, abril, junho, julho, agosto,
// setembro, outubro, novembro, dezembro
// abbreviated months: jan, fev, mar, abr, jun, jul, ago, set, out, nov, dez
//  
// - TIMES
// 18h30
// 18h
// 18hs
// às 18
// 18 horas
// 18:30
// 18:30h
// 18:30hs


//array("13-05-2011",'13/5/2011 3:33'),

//array("Venres 10 de Xuño",'10/6/2011 3:33'),
//array("venres, 17 xuño 2011",'17/6/2011 3:33'),
//array("11 de Xuño ás 11:00 ",'11/6 11:00'),
//array("31 de maio",'31/5 3:33'),
//array("18 de xuño, ás 12:00 horas",'18/6 12:00'),
//array("31 de maio 2011 10.00",'31/5/2011 10:00'),



//array("10/xuño   20:00",'10/6 3:33'),



		//			 array('',
		//				   'stamps'=>array()),
		   ];
	test_list_find_date_time('pt',$tests);
}

function specific_test_find_dates_and_times_pt()
{
	$text=file_get_contents('problem-text');
	echo strlen($text)."\n";
	$res=find_dates_and_times($text,true);
	print_r($res);
}

function test_find_times_pt()
{
	$tests=['3h'=>['03:00'],
			'15h20'=>['15:20'],
			'2h xxx 5:12'=>['02:00','05:12'],
			];
	test_find_times('pt',$tests);
}

?>