<?php

function test_dlib_all($short=false)
{
	require_once 'dlib/test/test-css-template.php';
	test_css_template();

	require_once 'dlib/test/test-tools.php';
	test_tools();

	require_once 'dlib/test/test-html-tools.php';
	test_html_tools();

	require_once 'dlib/test/test-DBObject.php';
	// test_DBObject(); FIXME

	test_dlib_cron_can_start();

	test_dlib_commandline_bufferpad();

	// ****** find dates and times

	if($short){return;}

	// HACK: avoid cached language
	// settings in static vars in tools.php 
	$GLOBALS['dlib_reset_locale']=true;

	setlocale(LC_TIME, "fr_FR.utf8");
	require_once 'dlib/test/test-find-date-time-fr.php';
	test_find_date_time_fr();

	setlocale(LC_TIME, "el_GR.utf8");
	require_once 'dlib/test/test-find-date-time-el.php';
	test_find_date_time_el();

	setlocale(LC_TIME, "en_US.utf8");
	require_once 'dlib/test/test-find-date-time-en.php';
	test_find_date_time_en();

	setlocale(LC_TIME, "es_ES.utf8");
	require_once 'dlib/test/test-find-date-time-es.php';
	test_find_date_time_es();


}

function test_dlib_cron_can_start()
{
	require_once 'dlib/dlib-cron.php';
	// basic test that checks variables
	$now=time();
	// cron accepted test
	variable_set('test','cron_last_started'  ,$now-3600*24);
	variable_set('test','cron_last_completed',$now-3600*24);
	$start=dlib_cron_can_start('test',['period'=>'delay','delay'=>3600],false);
	test_equals($start,true);
	$started=variable_get('test','cron_last_started',0);
	test_assert($started>=$now && $started<($now+10));
	dlib_cron_finished('test');
	$completed=variable_get('test','cron_last_completed',0);
	test_assert($completed>=$now && $completed<($now+10));

	// cron not accepted test
	variable_set('test','cron_last_started'  ,$now-100);
	variable_set('test','cron_last_completed',$now-100);
	$start=dlib_cron_can_start('test',['period'=>'delay','delay'=>3600],false);
	test_equals($start,false);
	$started=variable_get('test','cron_last_started',0);
	test_equals($started,($now-100));
	$completed=variable_get('test','cron_last_completed',0);
	test_equals($completed,($now-100));
	
	// check with times array
	$today=mktime(0,0,0);
	variable_set('test','cron_last_started'  ,$today+3600*10+100);
	variable_set('test','cron_last_completed',$today+3600*10+100);
	$start=dlib_cron_can_start('test',['period'=>'daily','times'=>['5:00','10:00','15:00','20:00']],false,$today+3600*10+1000);
	test_equals($start,false);
	$start=dlib_cron_can_start('test',['period'=>'daily','times'=>['5:00','10:00','15:00','20:00']],false,$today+3600*15+1000);
	test_equals($start,true);

	// check weekly
	$today=strtotime('2/2/2018'); // Fri, 02 Feb 2018 00:00
	variable_set('test','cron_last_started'  ,$today+3600*10+100);
	variable_set('test','cron_last_completed',$today+3600*10+100);
	$start=dlib_cron_can_start('test',['period'=>'weekly','weekday'=>'tuesday','time'=>'3:00'],false,$today+3600*15);
	test_equals($start,false);
	$start=dlib_cron_can_start('test',['period'=>'weekly','weekday'=>'tuesday','time'=>'3:00'],false,$today+3600*1+3600*24*4);
	test_equals($start,false);
	$start=dlib_cron_can_start('test',['period'=>'weekly','weekday'=>'tuesday','time'=>'3:00'],false,$today+3600*15+3600*24*4);
	test_equals($start,true);

	//fatal("FIXME test_dlib_cron_can_start");
}

function test_dlib_commandline_bufferpad()
{
	$GLOBALS['test_dlib_commandline']=true;
	ob_start();
	require_once 'dlib/dlib';
	ob_end_clean();
	
	$prevPart=false;
	test_equals(commandline_bufferpad_remove('[YQ==]',0,$prevPart),'a');

	$prevPart=false;
	test_equals(commandline_bufferpad_remove('[Yg==][Yg==]     [Yg==]',0,$prevPart),'bbb');

	$prevPart=false;
	test_equals(commandline_bufferpad_remove('   [YQ==]   [YQ==] [YQ==] ',0,$prevPart),'aaa');

	$prevPart=false;
	test_equals(commandline_bufferpad_remove('[YWJj',0,$prevPart),'');
	test_equals(commandline_bufferpad_remove('ZGVm]',0,$prevPart),'abcdef');

	$prevPart=false;
	test_equals(commandline_bufferpad_remove('[YWJj',0,$prevPart),'');
	test_equals(commandline_bufferpad_remove('ZGVm] [Z2hpamts]  ',0,$prevPart),'abcdefghijkl');
	
	$prevPart=false;
	test_equals(commandline_bufferpad_remove('[YWJj',0,$prevPart),'');
	test_equals(commandline_bufferpad_remove('ZGVm] [Z2hpamts]    [YQ==]',0,$prevPart),'abcdefghijkla');

	$prevPart=false;
	test_equals(commandline_bufferpad_remove('[MDE',0,$prevPart),'');
	test_equals(commandline_bufferpad_remove('yMzQ1Njc4OWFiY2',0,$prevPart),'');
	test_equals(commandline_bufferpad_remove('RlZmdoaWprbA==]',0,$prevPart),'0123456789abcdefghijkl');

}

?>