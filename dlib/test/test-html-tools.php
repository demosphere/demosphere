<?php

require_once 'test-dlib.php';

function test_html_tools()
{
	global $dlib_config;
	require_once 'dlib/html-tools.php';

	$s='@[[:space:]]@';


	test_matches(dlib_html_data_url_remove('<p>abd <img src="data:image/png;base64,R0lGODlhAQABAPAAAJejrwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" /> def</p>',0),
				'@<p>abd <img src="https://[^/]+/files/images/data-url/[a-z0-9]+.png" /> def</p>@');


	// ********** test dlib_html_select_xpath()
	


	$html='<p>hello</p>';
	$res=dlib_html_select_xpath($html,'//p');
	$res=preg_replace($s,'',$res);
	test_equals($res,'<p>hello</p>');

	$html='<div><span>zzz</span><p>hello</p></div>';
	$res=dlib_html_select_xpath($html,'//p');
	$res=preg_replace($s,'',$res);
	test_equals($res,'<p>hello</p>');

	$html='<div><span>zzz</span><p>hello</p></div>';
	$res=dlib_html_select_xpath($html,'//p',true);
	$res=preg_replace($s,'',$res);
	test_contains($res,'<div><span>zzz</span></div>');

	$html='<div><span>Au 28ème</span><p class="abc">hello</p></div>';
	$res=dlib_html_apply_selectors($html,[['type'=>'CSS-REMOVE','arg'=>'.abc']]);
	$res=preg_replace($s,'',$res);
	test_equals($res,'<div><span>Au28ème</span></div>');

	// ********** FIXME: Work in progress. Replace all of tidy's functionality in XSL + DOM.

	// debug: display how loadeHTML is changing html
	//$html='<span><div><span><div></div></span></div></span>';
	//$doc=new DOMDocument();
	//$doc->loadHTML($html,LIBXML_HTML_NOIMPLIED|LIBXML_HTML_NODEFDTD);
	//$res=$doc->saveHTML();
	//echo $res;die("\n");

	// Move up all elements using dlib_dom_bubble_up()
	//$html='<div id="top"><div id="block1"><a href="hh"><p>zz</p></a></div></div>';
	//$doc=new DOMDocument();
	//$doc->loadHTML($html,LIBXML_HTML_NOIMPLIED|LIBXML_HTML_NODEFDTD);
	//$xpath = new DOMXPath($doc);
	//for($i=0;$i<10000;$i++)
	//{
	// 	$found=$xpath->query("//*[self::a or self::strong or self::em]/*[self::p or self::ul or self::ol or self::li or self::h1 or self::h2 or self::h3 or self::h4 or self::h5 ]");
	// 	if($found->length===0){break;}
	// 	dlib_dom_bubble_up($found->item(0));
	//}
	//$res=$doc->saveHTML();
	//if($i===10000){$res='dlib_dom_bubble_up error';}
	//echo $res;die("\n");

	// Test dlib_dom_bubble_up (move up element)
	$html='<div><p id="p0"><span id="a11"></span><span id="a12"><span id="a21"></span><span id="p22"><span id="a31"></span><span id="a32"></span><span id="a33"></span></span><span id="a23"></span></span><span id="a13"></span></p></div>';
	$doc=new DOMDocument();
	$doc->loadHTML($html,LIBXML_HTML_NOIMPLIED|LIBXML_HTML_NODEFDTD);
	$xpath = new DOMXPath($doc);
	$p=$xpath->query("//*[@id='p22']")->item(0);
	dlib_dom_bubble_up($p);
	$res=$doc->saveHTML();
	test_equals(trim($res),'<div><p id="p0"><span id="a11"></span><span id="a12"><span id="a21"></span></span><span id="p22"><span id="a12"><span id="a31"></span></span><span id="a12"><span id="a32"></span></span><span id="a12"><span id="a33"></span></span></span><span id="a12"><span id="a23"></span></span><span id="a13"></span></p></div>');

	// Test dlib_dom_flatten (move up blocks inside top level blocks)
	$html='<p><h2>uu</h2>a<span>x</span><h3>b</h3><p>c<a>d</a></p></p>';
	$doc=new DOMDocument();
	$doc->loadHTML($html,LIBXML_HTML_NOIMPLIED|LIBXML_HTML_NODEFDTD);
	dlib_dom_flatten($doc->documentElement,['p','h1','h2','h3','h4','h5','h6','ul','ol']);
	$res=$doc->saveHTML();
	test_equals(trim($res),'<h2>uu</h2><p>a<span>x</span></p><h3>b</h3><p>c<a>d</a></p>');
	//die("\n");


	test_equals(dlib_html_links_to_urls("xx<a href='abc'>yy"            ,'http://yyy.zz/rrr/sss/ttt.html' ),"xx<a href='http://yyy.zz/rrr/sss/abc'>yy");
	test_equals(dlib_html_links_to_urls('xx<a href="abc">yy'            ,'http://yyy.zz/rrr/sss/ttt.html' ),'xx<a href="http://yyy.zz/rrr/sss/abc">yy');
	test_equals(dlib_html_links_to_urls('xx<a href=abc>yy'              ,'http://yyy.zz/rrr/sss/ttt.html' ),'xx<a href="http://yyy.zz/rrr/sss/abc">yy');
	test_equals(dlib_html_links_to_urls('xx<img src=abc>yy'             ,'http://yyy.zz/rrr/sss/ttt.html' ),'xx<img src="http://yyy.zz/rrr/sss/abc">yy');
	test_equals(dlib_html_links_to_urls('xx<a href=//abc.com>yy'        ,'http://yyy.zz/rrr/sss/ttt.html' ),'xx<a href="http://abc.com">yy');
	test_equals(dlib_html_links_to_urls('xx<a href=//abc.com>yy'        ,'https://yyy.zz/rrr/sss/ttt.html'),'xx<a href="https://abc.com">yy');
	test_equals(dlib_html_links_to_urls('xx<a href=/abc>yy'             ,'http://yyy.zz/rrr/sss/ttt.html' ),'xx<a href="http://yyy.zz/abc">yy');
	test_equals(dlib_html_links_to_urls('xx<style>@import abc</style>'  ,'http://yyy.zz/rrr/sss/ttt.html' ),'xx<style>@import http://yyy.zz/rrr/sss/abc</style>');
	test_equals(dlib_html_links_to_urls('xx<style>@import "abc"</style>','http://yyy.zz/rrr/sss/ttt.html' ),'xx<style>@import "http://yyy.zz/rrr/sss/abc"</style>');
	test_equals(dlib_html_links_to_urls('xx<style x=y>@import "abc"</style>' , 'http://yyy.zz/rrr/sss/ttt.html'),
				'xx<style x=y>@import "http://yyy.zz/rrr/sss/abc"</style>');
	test_equals(dlib_html_links_to_urls('xx<style>a @import url(abc)</style>', 'http://yyy.zz/rrr/sss/ttt.html'),
				'xx<style>a @import url(http://yyy.zz/rrr/sss/abc)</style>');
	test_equals(dlib_html_links_to_urls('xx<a href=abcdéf>yy'              ,'http://yyy.zz/rrr/sss/ttt.html' ),'xx<a href="http://yyy.zz/rrr/sss/abcd%C3%A9f">yy');

	$html='<html>
<head>
<XXXX href="http://abc.de/uuu/iii" />
</head>
<body>
<a href="a">xyz</a>
<a href="/a">xyz</a>
<a href="http://example.org">xyz</a>
<a href="http://example.org/a">xyz</a>
<style type="text/css">@import url(/a/b/c.css);</style>
<style type="text/css">@import url(r/t.css);</style>

<img src="a"/>
<img src="/a"/>
<img src="http://example.org"/>
<img src="http://example.org/a"/>

</body>
</html>';

	test_equals(dlib_html_links_to_urls($html,'http://yyy.zz/rrr/sss/ttt.html'),
'<html>
<head>
<XXXX href="http://abc.de/uuu/iii" />
</head>
<body>
<a href="http://yyy.zz/rrr/sss/a">xyz</a>
<a href="http://yyy.zz/a">xyz</a>
<a href="http://example.org">xyz</a>
<a href="http://example.org/a">xyz</a>
<style type="text/css">@import url(http://yyy.zz/a/b/c.css);</style>
<style type="text/css">@import url(http://yyy.zz/rrr/sss/r/t.css);</style>

<img src="http://yyy.zz/rrr/sss/a"/>
<img src="http://yyy.zz/a"/>
<img src="http://example.org"/>
<img src="http://example.org/a"/>

</body>
</html>');
	$html=str_replace('XXXX','base',$html);
	test_equals(dlib_html_links_to_urls($html,'http://yyy.zz/rrr/sss/ttt.html'),
'<html>
<head>
<base href="http://abc.de/uuu/iii" />
</head>
<body>
<a href="http://abc.de/uuu/iii/a">xyz</a>
<a href="http://abc.de/a">xyz</a>
<a href="http://example.org">xyz</a>
<a href="http://example.org/a">xyz</a>
<style type="text/css">@import url(http://abc.de/a/b/c.css);</style>
<style type="text/css">@import url(http://abc.de/uuu/iii/r/t.css);</style>

<img src="http://abc.de/uuu/iii/a"/>
<img src="http://abc.de/a"/>
<img src="http://example.org"/>
<img src="http://example.org/a"/>

</body>
</html>');

	// *** test dlib_dom
	test_equals(dlib_dom_to_html(dlib_dom_from_html('<img src="uuu">')),'<img src="uuu"/>');

	$doc=dlib_dom_from_html('<img src="uuu">');
	dlib_dom_add_class($doc->getElementsByTagName('img')->item(0),'zz');
	test_matches(dlib_dom_to_html($doc),'@<img src="uuu" class=" *zz *"/>@');

	// *** test cleanup_html_tidy_xsl
	$s='@[[:space:]]@';

	test_equals(preg_replace($s,'',dlib_html_cleanup_tidy_xsl('<p>x</p>')),
				'<p>x</p>');
	test_equals(preg_replace($s,'',dlib_html_cleanup_tidy_xsl('<a href="abc">x</a>')),
				'<p><ahref="abc">x</a></p>');
	test_equals(preg_replace($s,'',dlib_html_cleanup_tidy_xsl("x")),
				'<p>x</p>');
	test_equals(preg_replace($s,'',dlib_html_cleanup_tidy_xsl("<b>x</b>")),
				'<p><strong>x</strong></p>');
	test_equals(preg_replace($s,'',dlib_html_cleanup_tidy_xsl('<p><a href="   ">x</a></p>')),'<p>x</p>');
	test_equals(preg_replace($s,'',dlib_html_cleanup_tidy_xsl("<p> \n <br/></p>")),'');
	test_equals(preg_replace($s,'',dlib_html_cleanup_tidy_xsl("<p> \n&nbsp; <br/></p>")),'');
	test_equals(preg_replace($s,'',dlib_html_cleanup_tidy_xsl("<p> &#160;\n <br/></p>")),'');
	//test_not_contains(dlib_html_cleanup_tidy_xsl('<a href="javascript:alert()">zz</a>'),'javascript');
	//test_not_contains(dlib_html_cleanup_tidy_xsl('<a href="javascript:alert">zz</a>'),'javascript');
	test_contains    (dlib_html_cleanup_tidy_xsl('é&#233;&eacute;'),'ééé');
	test_contains    (dlib_html_cleanup_tidy_xsl('&#931;&#0931;&#x3A3;&#x3a3;'),'ΣΣΣΣ');
	test_not_contains(dlib_html_cleanup_tidy_xsl('<!-- zzz -->'),'zzz');

	// FIXME: this doesnt work: div becomes p, and but is not correctly removed.
	//test_equals(preg_replace($s,'',dlib_html_cleanup_tidy_xsl("<div><p>y</p></div>")),"<p>y</p>");
	test_equals(preg_replace($s,'',dlib_html_cleanup_tidy_xsl("<p><br/><br/><br/><br/></p><p>x</p>")),'<p>x</p>');
	test_equals(preg_replace($s,'',dlib_html_cleanup_tidy_xsl("<p> \n <em><br/></em></p>")),'');
	test_equals(preg_replace($s,'',dlib_html_cleanup_tidy_xsl("<p> \n <strong>   <br/>\n \n</strong></p>")),'');

	test_equals(preg_replace($s,'',dlib_html_cleanup_tidy_xsl("<a href='abc'> <br/> <em>\n </em> \n </a>")),'');

	test_equals(preg_replace($s,'',dlib_html_cleanup_tidy_xsl('<p>AAA <strong>BBB BBB <strong>CCC CCC</strong>.</strong></p>')),
				'<p>AAA<strong>BBBBBBCCCCCC.</strong></p>');
	test_equals(preg_replace($s,'',dlib_html_cleanup_tidy_xsl('<p>AAA <em>BBB BBB <em>CCC CCC</em>.</em></p>')),
				'<p>AAA<em>BBBBBBCCCCCC.</em></p>');

	test_equals(preg_replace($s,'',dlib_html_cleanup_tidy_xsl('<img src="uuu">')),'<p><imgsrc="uuu"/></p>');
	test_equals(preg_replace($s,'',dlib_html_cleanup_tidy_xsl('<p><!-- abc --></p>')),'');

	test_equals(preg_replace($s,'',dlib_html_cleanup_tidy_xsl('<p><IMG SRC="jav	ascript:alert(123);"></p>')),
				'');
	test_equals(preg_replace($s,'',dlib_html_cleanup_tidy_xsl('<p><IMG SRC=&#x6A&#x61&#x76&#x61&#x73&#x63&#x72&#x69&#x70&#x74&#x3A&#x61&#x6C&#x65&#x72&#x74&#x28&#x27&#x58&#x53&#x53&#x27&#x29></p>')),
				'');
	test_equals(preg_replace($s,'',dlib_html_cleanup_tidy_xsl('<p><IMG SRC=&#0000106&#0000097&#0000118&#0000097&#0000115&#0000099&#0000114&#0000105&#0000112&#0000116&#0000058&#0000097&#0000108&#0000101&#0000114&#0000116&#0000040&#0000039&#0000088&#0000083&#0000083&#0000039&#0000041></p>')),
				'');
	test_equals(preg_replace($s,'',dlib_html_cleanup_tidy_xsl('<p><IMG SRC=&#106;&#97;&#118;&#97;&#115;&#99;&#114;&#105;&#112;&#116;&#58;&#97;&#108;&#101;&#114;&#116;&#40;&#39;&#88;&#83;&#83;&#39;&#41;></p>')),
				'');
	test_equals(preg_replace($s,'',dlib_html_cleanup_tidy_xsl('<p>u<img src="xyz:abc"/>v</p>')),
				'<p>uv</p>');
	test_equals(preg_replace($s,'',dlib_html_cleanup_tidy_xsl('<p>u<img src="http://abc"/>v</p>')),
				'<p>u<imgsrc="http://abc"/>v</p>');
	test_equals(preg_replace($s,'',dlib_html_cleanup_tidy_xsl('<p>u<a href="abc/def">v</a>w</p>')),
				'<p>u<ahref="abc/def">v</a>w</p>');
	test_equals(preg_replace($s,'',dlib_html_cleanup_tidy_xsl('<p>u<a href="Http://abc">v</a>w</p>')),
				'<p>u<ahref="Http://abc">v</a>w</p>');
	test_equals(preg_replace($s,'',dlib_html_cleanup_tidy_xsl('<p>u<a href="mailto:abc">v</a>w</p>')),
				'<p>u<ahref="mailto:abc">v</a>w</p>');
	test_equals(preg_replace($s,'',dlib_html_cleanup_tidy_xsl('<p>u<a href="xyz:abc">v</a>w</p>')),
				'<p>uvw</p>');

	// merge successive strongs
	test_equals(preg_replace($s,'',dlib_html_cleanup_tidy_xsl('<p><strong>ab</strong><strong>cd</strong></p>')),
				'<p><strong>abcd</strong></p>');

	test_equals(preg_replace($s,'',dlib_html_cleanup_tidy_xsl('<p><strong>ab</strong> <strong>cd</strong></p>')),
				'<p><strong>ab</strong><strong>cd</strong></p>');

	test_equals(preg_replace($s,'',dlib_html_cleanup_tidy_xsl('<p><strong class="pp">ab</strong><strong>cd</strong></p>')),
				'<p><strongclass="pp">ab</strong><strong>cd</strong></p>');

	test_equals(preg_replace($s,'',dlib_html_cleanup_tidy_xsl('<p><em>ab</em><em>cd</em></p>')),
				'<p><em>abcd</em></p>');


	// *** test clean html page

	require_once 'dlib/download-tools.php';
	$base='dlib/test/test-pages'.'/ex1';
	$url=file_get_contents($base.'.url');
	$dl=dlib_download_and_clean_html_page($url,false,['testfile'=>$base.'.html']);
	// check css url fix 
	test_contains($dl,'href="http://www.europalestine.com/prive/spip_style.css"');
	// check image url fix 
	test_contains($dl,"src='http://www.europalestine.com/local/cache-vignettes/L295xH171/musee_tolerance_squelettes-0018f.jpg'");
	// check accents (iso to utf8 convert)
	test_contains($dl,'Musée de la Tolérance pour');
	// check numeric entities (appostrophe)
	test_contains($dl,"A la suite d’une investigation du journal");
	// check utf8 nbsp (not entity)
	test_contains($dl,"Immigration et Islam : retour");

	//file_put_contents('/tmp/x',$dl);fatal('ee');

	for($i=0;$i<200;$i++)
	{
		//echo "test page $i\n";
		$base='dlib/test/test-pages/'.$i;
		if(!file_exists($base.'.html')){continue;}
		$url=file_get_contents($base.'.url');
		$dl=dlib_download_and_clean_html_page($url,false,['testfile'=>$base.'.html']);
		// this only tests for crashes or failures
		test_different($dl,false);
	}
	//fatal('ee');

	// *** test download
	$html=dlib_download_and_clean_html_page('http://example.org/uuuu/a.php',false,['testfile'=>'dlib/test/test-pages/urls.html']);
	test_different($html,false);

	test_contains($html,'test urls');
	// check if css rel path/ full url conversion ok
	//file_put_contents('/tmp/t',$html);
	test_matches($html,'@<link type="text/css"[^>]*http://example.org/c1@');
	test_contains($html,'"http://example.org/abc"');
	test_contains($html,'"http://example.org/def"');
	test_contains($html,'"http://example.org/uuuu/ghi"');
	$html=dlib_download_and_clean_html_page('http://www.demosphere.net/demosphere/css/fp-chop.png');
	test_equals($html,false);


	// *** test dlib_css_selector_to_xpath

	$html='<div id="aa"><p class="ppp">p1<strong id="s1" class="cc">s1</strong><a>a1</a><a class="x">a2</a></p><p class="ppp">p2<strong id="s2" class="cc dd">s2</strong></p></div>';
	$xpath=dlib_css_selector_to_xpath('a+a.x');
	//var_dump($xpath);
	//echo dlib_html_select_xpath($html,$xpath);

	test_equals(dlib_css_selector_to_xpath('E'),'//E');
	test_equals(dlib_css_selector_to_xpath('E F'),'//E//F');
	test_equals(dlib_css_selector_to_xpath('.E'),"//*[contains(concat(' ', normalize-space(@class), ' '),' E ')]");
	test_equals(dlib_css_selector_to_xpath('E,F'),'//E | //F');
	test_equals(dlib_css_selector_to_xpath('E#F'),"//E[@id='F']");
	test_equals(dlib_css_selector_to_xpath('E>F'),"//E/F");
	test_equals(dlib_css_selector_to_xpath('E[a=b]'),"//E[@a='b']");
	test_equals(dlib_css_selector_to_xpath('E[a="b"]'),"//E[@a='b']");
	//die("\n");

}
?>