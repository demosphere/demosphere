<?php

require_once 'test-find-date-time-common.php';
require_once 'test-dlib.php';

function test_find_date_time_de()
{
	require_once 'dlib/tools.php';
	require_once 'dlib/find-date-time/find-date-time.php';
	require_once 'dlib/find-date-time/find-date-time-de.php';
	setlocale(LC_TIME, "de_DE.utf8");
	test_list_find_date_time_de();
	test_find_times_de();
	test_random_find_date_time_de(true);
}

// Automatically generates a large number of random dates and times
// using predefined patterns.
function test_random_find_date_time_de($verbose=false)
{
	$ok=0;
	$failed=0;
	// try different dates
	for($i=0;$i<100;$i++)
	{
		if($i<50)
		{$dateTS=mktime(0,0,0,1,1,mt_rand(2000,2020))+mt_rand(0,366*24*3600);}
		else{$dateTS=time()+mt_rand(-366*24*3600,366*24*3600);}
		$m=date('m',$dateTS);
		$d=date('d',$dateTS);
		$y=date('Y',$dateTS);
		$dateTS=mktime(0,0,0,$m,$d,$y);
		if($verbose)echo "date:".strftime('%A %e %B %Y - %R',$dateTS)."\n";
		for($t=0;$t<20;$t++)
		{
			$hasTime=true;
			if(true ){$h=mt_rand(0,23);$min=mt_rand(0,59);}
			if($t<15){$h=mt_rand(0,23);$min=30;}
			if($t<10){$h=mt_rand(0,23);$min= 0;}
			if($t==0){$h=			 3;$min=33;$hasTime=false;}
			$hasTime=intval($hasTime);

			$ts=mktime($h,$min,0,$m,$d,$y);

			// has min, has time, has year
			//       y       y      y 
			//       y       y      n   
			//       y       n      y   X
			//       y       n      n   X
			//       n		 y		y
			//		 n		 y		n 
			//		 n		 n		y
			//		 n		 n		n 
			for($type=0;$type<4;$type++)
			{
				$hasMin =$type%2;
				$hasYear=($type>>1)%2;
				if(!$hasTime && $hasMin){continue;}
				if($hasTime && !$hasMin  && $min!=0){continue;}
				if(!$hasYear && abs($ts-time())>(3600*24*30*5)){continue;}

			
				$formats=['%A %e %B %Y - %R',
						  '%e %b %Y - %R',
						  '%d.%m.%Y - %R'];
				if(!$hasYear)
				{
					$formats=array_merge($formats,['%A %e %B - %R',
												   '%d.%m - %R']);
				}

				if(!$hasTime && !$hasYear)
				{
					$formats=array_merge($formats,['%e %B',
												   '%d.%m']);
				}
				if(!$hasTime)
				{
					$formats=array_merge($formats,['%A %e %B %Y']);
					$f0=$formats;
					foreach($f0 as $k=>$f){$formats[$k]=str_replace(' - %R','',$f);}
				}
				foreach($formats as $format)
				{
					$string=strftime($format,$ts);

					//				echo "m:$hasMin t:$hasTime y:$hasYear ".
					//					"time:".strftime('%A %e %B %Y - %R',$ts)." :::: ".
					//					$string."\n";
					$contexts=['sdf sfd %s sdfsd',
							   '%s',
							   '%s csdsdf ',
							   'fsdf %s'];
					foreach($contexts as $context)
					{
						$fstring=$string;
						//if(!(mt_rand()%2)){$fstring=str_replace(':','h',$fstring);}
						//if(!(mt_rand()%4)){$fstring=str_replace('-','des',$fstring);}
						if(!(mt_rand()%4)){$fstring=str_replace('-','um',$fstring);}
						if(!(mt_rand()%4)){$fstring=str_replace('-',',',$fstring);}
						if(!(mt_rand()%4)){$fstring=dlib_remove_accents($fstring);}
						if(!(mt_rand()%4)){$fstring=mb_strtoupper($fstring,'UTF-8');}
						if(!(mt_rand()%4)){$fstring=str_replace('-','&nbsp;',$fstring);}
						else
						if(!(mt_rand()%4)){$fstring=ent($fstring);}
						//echo 'pre-final string:'.$fstring."\n";
						$fstring=str_replace('%s',$fstring,$context);
						//if(!(mt_rand()%1000))echo 'final string:'.$fstring."\n";
						$res=find_dates_and_times($fstring);
						$rts=val($res,0,false);
						if(count($res)!=1 || $rts!=$ts)
						{
							echo "FAILED find_dates_and_times test for $ts \"$fstring\"\n";
							var_dump($res);
							echo "\n";
							$res=find_dates_and_times($fstring,true);
							fatal("abort after fail\n");
							$failed++;
						}
						else{$ok++;}
					}
				}
			}
		}
	}
	echo "test_random_find_date_time_de: ok:$ok failed:$failed\n";
	test_equals($failed,0);
}

// Test a list of predefined strings.
function test_list_find_date_time_de()
{
	$tests=[
			['28.01.2017','28/1/2017 3:33'],
			['30.04.2016','30/4/2016 3:33'],
			['am 19. Mai','19/5 3:33'],
			['08. Mar 2016 Um 15.30 Uhr','8/3/2016 15:30'],
			['21.11.2014 13:00h','21/11/2014 13:00'],
			['1. okt 10:00','1/10 10:00'],
			['28.7','28/7 3:33'],
			['sa 14.mai 2016','14/5/2016 3:33'],
			['23.april.2016','23/4/2016 3:33'],
			['23.april.2016 18 uhr','23/4/2016 18:00'],
			['14-05-2016','14/5/2016 3:33'],
			['Samstag den 25.4.2015','25/4/2015 3:33'],
			['Sa. 18.April 2015 13:30','18/4/2015 13:30'],
			['Sa.10.Okt 2015 12uhr','10/10/2015 12:00'],
			['10.Okt.12uhr','10/10 12:00'],
			['13.jan.2012 17.30 h','13/1/2012 17:30'],
			['Mi. 9. Dez. 2009','9/12/2009 3:33'],
			['21.11.2014 Mittag','21/11/2014 12:00'],
			['25.4.16, 18 Uhr','25/4/2016 18:00'],
			['Montag, den 25. April 2016 um 19:00 Uhr','25/4/2016 19:00'],
			['27.4.','27/4 3:33'],
			['27.april - 08:00','27/4 8:00'],

			// 18:00 Uhr
			// 18 Uhr



			// Um => à 
			// Beginn => à 
			// vom 7. bis 27.  => du 7 au 27
			// keywords: Kundgebung 

//array('28 janvier à 14h 110 rue de Grenelle','28/1 14:00'),
//array('abc2/3cde',false),
//array('3 juin 2015 à midi','3/6/2015 12:00'),
//array('2012-10-03','3/10/2012 3:33'),
//array('les 15 et 17 mars 2012',array('15/3/2012 3:33','17/3/2012 3:33')),
//array('du 15 au 17 mars 2012',array('15/3/2012 3:33','17/3/2012 3:33')),
//array('17 mars, 16éme anniversaire','17/3 3:33'),
//array('2 Mars, 19–22h','2/3 3:33'),
//array('28 juillet 1961',false),
//array("Ven 1
//<sup>er</sup>
//juillet 2011",'1/7/2011 3:33'),
//array("11 \r\nmars 2010, à 18 h 30",'11/3/2010 18:30'),
//array(strftime('%a',strtotime('3/8/'.find_date_time_guess_year(8,3))).' 8 mars, 19-21h','8/3 3:33'),
//array(strftime('%A',strtotime('2/20/'.find_date_time_guess_year(20,2))).' 20 février, 14-23h','20/2 3:33'),
//array('VENDREDI 22/01/10 à 19H30','22/01/2010 19:30'),
//array('1/2/2021','01/2/2021 3:33'),
//array('Dimanche 6&#160; décembre 2009 à 17h15','06/12/2009 17:15'),
//array('03/06/09 :','03/06/2009 3:33'),
//array('<em class="zzz">      one paragraph </em><span><em class="zzz"><em class="demosphere_timestamp">20 sep</em></span><em class="zzz"> 2012</em>','20/9/2012 3:33'),
//array('samedi 30 </span></span></b></font><span
// class="A1"><span style="font-family: "Calibri","sans-serif";"><b><font
// color="#ff6600">mai 2009 à 15h</font>','30/5/2009 15:00'),
//array('05/14/09 06:28',false),
//array('donc le</em> <em>4 mars 2009</em> <em>de 19h30 à 21h30</em>','4/3/2009 19:30'),
//array('Vendredi 9 JANV 2009','9/1/2009 3:33'),
//array('29 janvier 2009','29/1/2009 3:33'),
//array('lun 14 Sept 2009','14/9/2009 3:33'),
//array('08/09/2007 03:33','08/09/2007 03:33'),
//array('28 sep','28/9 3:33'),
//array('Le 3 sep 19h','3/9 19:00'),
//array('MANIFESTATION  23 AOÛT A 14H00 DEPAR','23/8 14:00'),
//array('MANIFESTATION  23 AOUT A 14H00 DEPAR','23/8 14:00'),
//array('jeudi 5 avril 2007 19h30','5/4/2007 19:30'),
//array('jeudi 5/4/07 19h30','5/4/2007 19:30'),
//array('jeudi 05/04/2007 19h30','5/4/2007 19:30'),
//array('5/4/2007 19h31','5/4/2007 19:31'),
//array('jeudi 5 avril 2007 - de 19h30 à 20h30','5/4/2007 19:30'),
//array('jeudi 5 avril 2007 de 19h30 à 20h30','5/4/2007 19:30'),
//array('jeudi 5 avril 2007 - 19h30','5/4/2007 19:30'),
//array("LE  17 AVRIL À PARTIR DE 18H30",'17/4 18:30'),
//array("LE  17 AVRIL DES 18H30",'17/4 18:30'),
//array("le  17 avril dès 18h30",'17/4 18:30'),
//array("le 1er aout 2008",'1/8/2008 3:33'),
//array("VENDREDI 1ER AOUT 2008",'1/8/2008 3:33'),
//array("le <strong>".strftime('%a',strtotime('4/17/'.find_date_time_guess_year(17,4)))." 17</strong> avril dès 18h30",'17/4 18:30'),
// 				 //			 array('',
// 				 //				   'stamps'=>array()),
				 ];
	test_list_find_date_time('de',$tests);
}



function specific_test_find_dates_and_times_de()
{
	$text=file_get_contents('problem-text');
	echo strlen($text)."\n";
	$res=find_dates_and_times($text,true);
	print_r($res);
}

function test_find_times_de()
{
	$tests=['3 Uhr'=>['03:00'],
			'15:20'=>['15:20'],
			'2 uhr xxx 5:12'=>['02:00','05:12'],
			];
	test_find_times('de',$tests);
}


?>