<?php

require_once 'test-find-date-time-common.php';
require_once 'test-dlib.php';

function test_find_date_time_es()
{
	require_once 'dlib/tools.php';
	require_once 'dlib/find-date-time/find-date-time.php';
	require_once 'dlib/find-date-time/find-date-time-es.php';
	setlocale(LC_TIME, "es_ES.utf8");
	test_list_find_date_time_es();
	test_find_times_es();
	test_random_find_date_time_es(true);
}

// Automatically generates a large number of random dates and times
// using predefined patterns.
function test_random_find_date_time_es($verbose=false)
{
	$ok=0;
	$failed=0;
	// try different dates
	for($i=0;$i<100;$i++)
	{
		if($i<50)
		{$dateTS=mktime(0,0,0,1,1,mt_rand(2000,2020))+mt_rand(0,366*24*3600);}
		else{$dateTS=time()+mt_rand(-366*24*3600,366*24*3600);}
		$m=date('m',$dateTS);
		$d=date('d',$dateTS);
		$y=date('Y',$dateTS);
		$dateTS=mktime(0,0,0,$m,$d,$y);
		if($verbose)echo "date:".strftime('%A %e %B %Y - %R',$dateTS)."\n";
		for($t=0;$t<20;$t++)
		{
			$hasTime=true;
			if(true ){$h=mt_rand(0,23);$min=mt_rand(0,59);}
			if($t<15){$h=mt_rand(0,23);$min=30;}
			if($t<10){$h=mt_rand(0,23);$min= 0;}
			if($t==0){$h=			 3;$min=33;$hasTime=false;}
			$hasTime=intval($hasTime);

			$ts=mktime($h,$min,0,$m,$d,$y);

			// has min, has time, has year
			//       y       y      y 
			//       y       y      n   
			//       y       n      y   X
			//       y       n      n   X
			//       n		 y		y
			//		 n		 y		n 
			//		 n		 n		y
			//		 n		 n		n 
			for($type=0;$type<4;$type++)
			{
				$hasMin =$type%2;
				$hasYear=($type>>1)%2;
				if(!$hasTime && $hasMin){continue;}
				if($hasTime && !$hasMin  && $min!=0){continue;}
				if(!$hasYear && abs($ts-time())>(3600*24*30*5)){continue;}

			
				$formats=['%A %e %B %Y - %R',
						  '%e %b %Y - %R',
						  '%d/%m/%Y - %R'];
				if(!$hasYear)
				{
					$formats=array_merge($formats,['%A %e %B - %R',
												   '%d/%m - %R']);
				}

				if(!$hasTime && !$hasYear)
				{
					$formats=array_merge($formats,['%e %B',
												   '%d/%m']);
				}
				if(!$hasTime)
				{
					$formats=array_merge($formats,['%A %e %B %Y']);
					$f0=$formats;
					foreach($f0 as $k=>$f){$formats[$k]=str_replace(' - %R','',$f);}
				}
				foreach($formats as $format)
				{
					$string=strftime($format,$ts);

					//				echo "m:$hasMin t:$hasTime y:$hasYear ".
					//					"time:".strftime('%A %e %B %Y - %R',$ts)." :::: ".
					//					$string."\n";
					$contexts=['sdf sfd %s sdfsd',
							   '%s',
							   '%s csdsdf ',
							   'fsdf %s'];
					foreach($contexts as $context)
					{
						$fstring=$string;
						if(!(mt_rand()%2)){$fstring=str_replace(':','h',$fstring);}
						//if(!(mt_rand()%4)){$fstring=str_replace('-','des',$fstring);}
						if(!(mt_rand()%4)){$fstring=str_replace('-','a las',$fstring);}
						if(!(mt_rand()%4)){$fstring=str_replace('-',',',$fstring);}
						if(!(mt_rand()%4)){$fstring=dlib_remove_accents($fstring);}
						if(!(mt_rand()%4)){$fstring=mb_strtoupper($fstring,'UTF-8');}
						if(!(mt_rand()%4)){$fstring=str_replace('-','&nbsp;',$fstring);}
						else
						if(!(mt_rand()%4)){$fstring=ent($fstring);}
						//echo 'pre-final string:'.$fstring."\n";
						$fstring=str_replace('%s',$fstring,$context);
						//if(!(mt_rand()%1000))echo 'final string:'.$fstring."\n";
						$res=find_dates_and_times($fstring);
						$rts=val($res,0,false);
						if(count($res)!=1 || $rts!=$ts)
						{
							echo "FAILED find_dates_and_times test for $ts \"$fstring\"\n";
							var_dump($res);
							echo "\n";
							$res=find_dates_and_times($fstring,true);
							fatal("abort after fail\n");
							$failed++;
						}
						else{$ok++;}
					}
				}
			}
		}
	}
	echo "test_random_find_date_time_es: ok:$ok failed:$failed\n";
	test_equals($failed,0);
}

// Test a list of predefined strings.
function test_list_find_date_time_es()
{
	$tests=[

// interesting: http://spanish.about.com/od/writtenspanish/qt/dates.htm
		['jueves  9 mayo 2019 - 19h30','9/5/2019 19:30'],
		['12 de junio de 2006','12/06/2006 3:33'],
		['12 de junio del 2006','12/06/2006 3:33'],
		['16 de noviembre a las 15:30 h','16/11 15:30'],
		['16 de noviembre 2006 a las 22 h','16/11/2006 22:00'],
		['16 de noviembre 2006 a las 22.30 h','16/11/2006 22:30'],
		['10 de octubre a las 20.00 hs','10/10 20:00'],
		['17 de diciembre del año 2014','17/12/2014 3:33'],
		['noviembre 18 2015 a las 8:00 am','18/11/2015 8:00'],
		['noviembre 18 2015 a las 8:00 pm','18/11/2015 20:00'],
		['noviembre 18 a las 8:00 p.m.','18/11 20:00'],
		['enero 5 de 2012"','5/1/2012 3:33'],
		['enero 5 de 2.012"','5/1/2012 3:33'],
		['primero de enero de 2012"','1/1/2012 3:33'],
		['1ero de marzo de 2012"','1/3/2012 3:33'],
		['1° de marzo de 2012"','1/3/2012 3:33'],
		['1o de marzo de 2012"','1/3/2012 3:33'],
		['noviembre 20/5:00 am','20/11 5:00'],
		['noviembre 4/5:00 pm','4/11 17:00'],
		['noviembre 18/8:30 am - noviembre 19/5:00 pm',['18/11 8:30','19/11 17:00']],

//array('abc2/3cde',false),
//array('2012-10-03','3/10/2012 3:33'),
//array('les 15 et 17 mars 2012',array('15/3/2012 3:33','17/3/2012 3:33')),
//array('du 15 au 17 mars 2012',array('15/3/2012 3:33','17/3/2012 3:33')),
//array('17 mars, 16éme anniversaire','17/3 3:33'),
//array('2 Mars, 19–22h','2/3 3:33'),
//array('28 juillet 1961',false),
//array("Ven 1
//<sup>er</sup>
//juillet 2011",'1/7/2011 3:33'),
//array("11 \r\nmars 2010, à 18 h 30",'11/3/2010 18:30'),
//array(strftime('%a',strtotime('3/8/'.find_date_time_guess_year(8,3))).' 8 mars, 19-21h','8/3 3:33'),
//array(strftime('%A',strtotime('2/20/'.find_date_time_guess_year(20,2))).' 20 février, 14-23h','20/2 3:33'),
//array('VENDREDI 22/01/10 à 19H30','22/01/2010 19:30'),
//array('1/2/2021','01/2/2021 3:33'),
//array('Dimanche 6&#160; décembre 2009 à 17h15','06/12/2009 17:15'),
//array('03/06/09 :','03/06/2009 3:33'),
//array('<em class="zzz">      one paragraph </em><span><em class="zzz"><em class="demosphere_timestamp">20 sep</em></span><em class="zzz"> 2012</em>','20/9/2012 3:33'),
//array('samedi 30 </span></span></b></font><span
// class="A1"><span style="font-family: "Calibri","sans-serif";"><b><font
// color="#ff6600">mai 2009 à 15h</font>','30/5/2009 15:00'),
//array('05/14/09 06:28',false),
//array('donc le</em> <em>4 mars 2009</em> <em>de 19h30 à 21h30</em>','4/3/2009 19:30'),
//array('Vendredi 9 JANV 2009','9/1/2009 3:33'),
//array('29 janvier 2009','29/1/2009 3:33'),
//array('lun 14 Sept 2009','14/9/2009 3:33'),
//array('08/09/2007 03:33','08/09/2007 03:33'),
//array('28 sep','28/9 3:33'),
//array('Le 3 sep 19h','3/9 19:00'),
//array('MANIFESTATION  23 AOÛT A 14H00 DEPAR','23/8 14:00'),
//array('MANIFESTATION  23 AOUT A 14H00 DEPAR','23/8 14:00'),
//array('jeudi 5 avril 2007 19h30','5/4/2007 19:30'),
//array('jeudi 5/4/07 19h30','5/4/2007 19:30'),
//array('jeudi 05/04/2007 19h30','5/4/2007 19:30'),
//array('5/4/2007 19h31','5/4/2007 19:31'),
//array('jeudi 5 avril 2007 - de 19h30 à 20h30','5/4/2007 19:30'),
//array('jeudi 5 avril 2007 de 19h30 à 20h30','5/4/2007 19:30'),
//array('jeudi 5 avril 2007 - 19h30','5/4/2007 19:30'),
//array("LE  17 AVRIL À PARTIR DE 18H30",'17/4 18:30'),
//array("LE  17 AVRIL DES 18H30",'17/4 18:30'),
//array("le  17 avril dès 18h30",'17/4 18:30'),
//array("le 1er aout 2008",'1/8/2008 3:33'),
//array("VENDREDI 1ER AOUT 2008",'1/8/2008 3:33'),
//array("le <strong>".strftime('%a',strtotime('4/17/'.find_date_time_guess_year(17,4)))." 17</strong> avril dès 18h30",'17/4 18:30'),
// 				 //			 array('',
// 				 //				   'stamps'=>array()),
		   ];
	test_list_find_date_time('es',$tests);
}


function specific_test_find_dates_and_times_es()
{
	$text=file_get_contents('problem-text');
	echo strlen($text)."\n";
	$res=find_dates_and_times($text,true);
	print_r($res);
}

function test_find_times_es()
{
	$tests=['3am'=>['03:00'],
			'3AM'=>['03:00'],
			'3pm'=>['15:00'],
			'3PM'=>['15:00'],
			'3 P.M.'=>['15:00'],
			'3 A.M.'=>['03:00'],
			'3.30pm'=>['15:30'],
			'3.30am'=>['03:30'],
			];
	test_find_times('es',$tests);
}

?>