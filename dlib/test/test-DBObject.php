<?php

function test_DBObject()
{
	require_once 'dlib/tools.php';
	require_once 'dlib/testing.php';
	require_once 'dlib/database.php';
	test_db_build_query();
	test_simple_DBObject();
}

function test_db_build_query()
{
	test_equals(db_build_query('x %d y %s z',5,'test'),'x 5 y test z');
	test_equals(db_build_query('%s','\\'.'a'),'\\'.'\\'.'a');

	fatal('tablename_map no longer supported');

	unset($GLOBALS['db_config']["tablename_map"]);
	test_equals(db_build_query('{xyz}'),'xyz');

	unset($GLOBALS['db_config']["tablename_map"]);
	$GLOBALS['db_config']["tablename_map"]=['t'=>'aaa'];
	test_equals(db_build_query('{xyz}'),'xyz');

	$GLOBALS['db_config']["tablename_map"]=['xyz'=>'aaa'];
	test_equals(db_build_query('{xyz}'),'aaa');
}


function test_simple_DBObject()
{
	require_once 'dlib/DBObject.php';

	class TestObj extends DBObject
	{
		public $id;
		static $id_        =['type'=>'autoincrement'];
		public $name;
		static $name_      =[];
		public $content;
		static $content_   =[];
		public $age;
		static $age_       =['type'=>'int'];

		static $dboStatic=false;

		function __construct($name,$age)
		{
			$this->name=$name;
			$this->age =$age;
		}
	}
	class TestObj2 extends DBObject
	{
		public $id;
		static $id_        =['type'=>'autoincrement'];
		public $name;
		static $name_      =[];
		public $age;
		static $age_       =['type'=>'int'];

		static $dboStatic=false;

		function __construct($name,$age)
		{
			$this->name=$name;
			$this->age =$age;
		}
	}


	test_DBObject_setup_cleandb();
	
	$o=new TestObj('toto',123);	
	$o->save();
	
	test_equals('toto',     db_result('SELECT name FROM TestObj WHERE id=%d',$o->id));
	test_equals(123   ,(int)db_result('SELECT age  FROM TestObj WHERE id=%d',$o->id));

	$o1=new TestObj('zz',50);
	$o1->name='titi';
	$o1->age=100;
	$o1->save();

	test_equals('titi',     db_result('SELECT name FROM TestObj WHERE id=%d',$o1->id));
	test_equals(100   ,(int)db_result('SELECT age  FROM TestObj WHERE id=%d',$o1->id));

	$ox=TestObj::fetch(1);
	test_equals($ox->name,'toto');


	// second object init type
	unset($o);
	unset($o1);
	unset($ox);
	$o=new TestObj2('toto',123);
	$o->save();
	
	test_equals('toto',     db_result('SELECT name FROM TestObj2 WHERE id=%d',$o->id));
	test_equals(123   ,(int)db_result('SELECT age  FROM TestObj2 WHERE id=%d',$o->id));

	$o1=new TestObj2('zz',50);
	$o1->name='titi';
	$o1->age=100;
	$o1->save();
	test_equals('titi',     db_result('SELECT name FROM TestObj2 WHERE id=%d',$o1->id));
	test_equals(100   ,(int)db_result('SELECT age  FROM TestObj2 WHERE id=%d',$o1->id));

	$ox=TestObj2::fetch(1);
	test_equals($ox->name,'toto');

	// test fetch with null return
	$o=TestObj2::fetch(1,false);
	test_different($o,null);
	test_equals($o->name,'toto');
	$o=TestObj2::fetch(200,false);
	test_equals($o,null);

	// test bad chars
	//$bc=new TestObj('bc',0);	
	//$bc->content=file_get_contents('badstr0');
	//$bc->save();
	//$lc=TestObj::retrieveByPK($bc->id,'TestObj');
	//test_equals($lc->content,$bc->content);
	//echo strlen($lc->content).':'.strlen($bc->content)."\n";

	//fatal("finished..abort\n");
}

function test_DBObject_setup_cleandb()
{
	// use different tables for testing
	global $db_connection;

	db_query("DROP TABLE IF EXISTS TestObj");
	db_query("CREATE TABLE TestObj (".
			   "`id` int(11) NOT NULL auto_increment,".
			   "`name` varchar(1024) NOT NULL default '',".
			   "`age`  int(11) NOT NULL,".
			   "`content` longblob  NOT NULL,".
			   "PRIMARY KEY  (`id`)".
			   ") DEFAULT CHARSET=utf8mb4;");	

	db_query("DROP TABLE IF EXISTS TestObj2");
	db_query("CREATE TABLE TestObj2 (".
			   "`id` int(11) NOT NULL auto_increment,".
			   "`name` varchar(1024) NOT NULL default '',".
			   "`age`  int(11) NOT NULL,".
			   "PRIMARY KEY  (`id`)".
			   ") DEFAULT CHARSET=utf8mb4;");	
}


?>