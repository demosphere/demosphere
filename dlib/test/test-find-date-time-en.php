<?php

require_once 'test-find-date-time-common.php';
require_once 'test-dlib.php';

function test_find_date_time_en()
{
	require_once 'dlib/tools.php';
	require_once 'dlib/find-date-time/find-date-time.php';
	require_once 'dlib/find-date-time/find-date-time-en.php';
	setlocale(LC_TIME, "en_US.utf8");
	test_list_find_date_time_en();//fatal("\n");
	test_find_times_en();
	test_random_find_date_time_en(true);
}

// Automatically generates a large number of random dates and times
// using predefined patterns.
function test_random_find_date_time_en($verbose=false)
{
	$ok=0;
	$failed=0;
	//echo "FIXME (remove srand)\n";mt_srand(1234);  

	$altMonths=[
		['January'],
		['February'],
		['March'],
		['April'],
		['May'],
		['June'],
		['July'],
		['August'],
		['September'],
		['October'],
		['November'],
		['December'],
			   ];
	$t=$altMonths;
	foreach($t as $k=>$a)
	{
		unset($altMonths[$k][2]);
		$altMonths[$k][]='%B';
		$altMonths[$k]=array_values($altMonths[$k]);
	}
	//print_r($altMonths);
	// try different dates
	for($i=0;$i<100;$i++)
	{
		if($i<50)
		{$dateTS=mktime(0,0,0,1,1,mt_rand(2000,2020))+mt_rand(0,366*24*3600);}
		else{$dateTS=time()+mt_rand(-366*24*3600,366*24*3600);}
		$m=date('m',$dateTS);
		$d=date('d',$dateTS);
		$y=date('Y',$dateTS);
		$dateTS=mktime(0,0,0,$m,$d,$y);
		if($verbose)echo "date:".strftime('%A %e %B %Y - %R',$dateTS)."\n";
		for($t=0;$t<20;$t++)
		{
			$hasTime=true;
			if(true ){$h=mt_rand(0,23);$min=mt_rand(0,59);}
			if($t<15){$h=mt_rand(0,23);$min=30;}
			if($t<10){$h=mt_rand(0,23);$min= 0;}
			if($t==0){$h=			 3;$min=33;$hasTime=false;}
			$hasTime=intval($hasTime);
			if($hasTime && $h==3 && $min==33){$min+=1;}

			$ts=mktime($h,$min,0,$m,$d,$y);

			// has min, has time, has year
			//       y       y      y 
			//       y       y      n   
			//       y       n      y   X
			//       y       n      n   X
			//       n		 y		y
			//		 n		 y		n 
			//		 n		 n		y
			//		 n		 n		n 
			for($type=0;$type<4;$type++)
			{
				$hasMin =$type%2;
				$hasYear=($type>>1)%2;
				if(!$hasTime && $hasMin){continue;}
				if($hasTime && !$hasMin  && $min!=0){continue;}
				if(!$hasYear && abs($ts-time())>(3600*24*30*5)){continue;}

			
				$formats=['%A %B %e %Y - %R',
						  '%e %b %Y - %R',
						  '%b %e %Y - %R',
						  '%m/%d/%Y - %R',
						  '%A %B %e %Y - %l:%M %p',
						  '%e %b %Y - %l:%M %p',
						  '%b %e %Y - %l:%M %p',
						  '%m/%d/%Y - %l:%M %p',
						 ];
				if(!$hasYear)
				{
					$formats=array_merge($formats,['%A %e %B - %R',
												   '%A %B %e - %R',
												   '%m/%d - %R',
												   '%A %e %B - %l:%M %p',
												   '%A %B %e - %l:%M %p',
												   '%m/%d - %l:%M %p'
												  ]);
				}

				if(!$hasTime && !$hasYear)
				{
					$formats=array_merge($formats,['%e %B',
												   '%B %e',
												   '%m/%d']);
				}
				if(!$hasTime)
				{
					$formats=array_merge($formats,['%A %e %B %Y']);
					$f0=$formats;
					foreach($f0 as $k=>$f)
					{
						$formats[$k]=str_replace(' - %R'      ,'',$formats[$k]);
						$formats[$k]=str_replace(' - %l:%M %p','',$formats[$k]);
					}
				}
				foreach($formats as $format)
				{
					// use alt months instead of %B
					$anm=mt_rand(0,count($altMonths[$m-1])-1);
					$altMonth=$altMonths[$m-1][$anm];
					$format1=str_replace('%B',$altMonth,$format);

					if(!$hasMin && !(mt_rand()%2))
					{
						$format1=preg_replace('@%l:%M %p$@','%l %p',$format1);
					}

					$string=strftime($format1,$ts);


					//				echo "m:$hasMin t:$hasTime y:$hasYear ".
					//					"time:".strftime('%A %e %B %Y - %R',$ts)." :::: ".
					//					$string."\n";
					$contexts=['sdf sfd %s sdfsd',
							   '%s',
							   '%s csdsdf ',
							   'fsdf %s'];
					foreach($contexts as $context)
					{
						$fstring=$string;
						//if(!(mt_rand()%2)){$fstring=str_replace(':','h',$fstring);}
						if(!(mt_rand()%4)){$fstring=str_replace('-',' around ',$fstring);}
						if(!(mt_rand()%4)){$fstring=str_replace('-',' at ',$fstring);}
						if(!(mt_rand()%4)){$fstring=str_replace('-',',',$fstring);}
						if(!(mt_rand()%4)){$fstring=dlib_remove_accents($fstring);}
						if(!(mt_rand()%4)){$fstring=mb_strtoupper($fstring,'UTF-8');}
						if(!(mt_rand()%4)){$fstring=str_replace('-','&nbsp;',$fstring);}
						else
						if(!(mt_rand()%4)){$fstring=ent($fstring);}
						//echo 'pre-final string:'.$fstring."\n";
						$fstring=str_replace('%s',$fstring,$context);
						//if(!(mt_rand()%1000))echo 'final string:'.$fstring."\n";
						$res=find_dates_and_times($fstring);
						$rts=val($res,0,false);
						if(count($res)!=1 || $rts!=$ts)
						{
							echo "FAILED find_dates_and_times test for $ts \"$fstring\"\n";
							echo "Format: $format1\n";
							echo "Expected :".strftime('%A %F - %R',$ts)."\n";
							var_dump($res);
							echo "\n";
							$res=find_dates_and_times($fstring,true);
							fatal("abort after fail\n");
							$failed++;
						}
						else{$ok++;}
					}
				}
			}
		}
	}
	echo "test_random_find_date_time_en: ok:$ok failed:$failed\n";
	test_equals($failed,0);
}

// Test a list of predefined strings.
function test_list_find_date_time_en()
{
	// http://en.wikipedia.org/wiki/Calendar_date
	$tests=[
 
		['Thurs. June 10 2011',false],
		['Dec. 1 at noon','1/12 12:00'],
		['Dec. 1, noon','1/12 12:00'],
		['Sunday, March 7<sup>th</sup> 2010','7/3/2010 3:33'],
		['March 18<sup>th</sup> 2009','18/3/2009 3:33'],
		['xyz $10.00 abc',false],
		['5.3in',false],
		['Dec. 1, 11pm','1/12 23:00'],
		['Dec. 21st 2009 3pm','21/12/2009 15:00'],
		['November 2nd 2009 3pm','2/11/2009 15:00'],
		['November 1st 2009 3pm','1/11/2009 15:00'],
		['November 2','2/11 3:33'],
		['Thursday Nov 5th, 2009 3:41 P.M.','5/11/2009 15:41'],
		['Thursday Nov 5th, 2009 3:41 PM','5/11/2009 15:41'],
		['Thursday Nov 5th, 2009 3:41 pm','5/11/2009 15:41'],
		['Thursday Nov 5th, 2009 3:41 AM','5/11/2009 3:41'],
		['Thursday Nov 5th, 2009 3:41 am','5/11/2009 3:41'],
		[strftime('%A',strtotime('11/9/'.find_date_time_guess_year(9,11))).', November 9th 7:30pm-9pm',
		 '9/11 19:30'],
		[strftime('%A',strtotime('11/8/'.find_date_time_guess_year(8,11))).', November 8, 11 AM – 5PM',
		 '8/11 11:00'],
		[strftime('%a',strtotime('11/8/'.find_date_time_guess_year(8,11))).', Nov. 8',
		 '8/11 3:33'],
		[strftime('%a',strtotime('11/8/'.find_date_time_guess_year(8,11))).' Nov 08',
		 '8/11 3:33'],
		['11/8/2009 8;00 AM ','8/11/2009 8:00'],
		['Thursday,November 12, 2009','12/11/2009 3:33'],
		['11/8/2009 8:00 AM ','8/11/2009 8:00'],
		['11/8/2009 8:00 PM ','8/11/2009 20:00'],
		['11/8/2009 8:00 am ','8/11/2009 8:00'],
		['11/8/2009 8:00 pm ','8/11/2009 20:00'],
		['December 3, 2003','3/12/2003 3:33'],
		['11.16.2003','16/11/2003 3:33'],
		['11.16.03','16/11/2003 3:33'],
		['11/16/2003','16/11/2003 3:33'],
		['11-16-2003','16/11/2003 3:33'],
		['Nov. 16, 2003','16/11/2003 3:33'],
		['Sunday, November 16, 2003','16/11/2003 3:33'],
		['January 23rd, 2009','23/1/2009 3:33'],
		['06/03/09 :','03/06/2009 3:33'],
		['5/31/09','31/5/2009 3:33'],
		['13/1',false],
		['1/13','13/1 3:33'],
		['6/35',false],
		['6/35/02',false],
		['6/5/02','5/6/2002 3:33'],
		['6/5','5/6 3:33'],
		['14/05/09 06:28',false],
		['5 Nov 2009','5/11/2009 3:33'],
		['5 Nov. 2009','5/11/2009 3:33'],
		['                                                                                                                                    jhslgfdj 17 May 2009','17/5/2009 3:33'],
		['&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;17 May 2009','17/5/2009 3:33'],
		['0.1',false],
		['15 May 2009','15/5/2009 3:33'],
		['                            17 May 2009','17/5/2009 3:33'],
		['         xxx 2/12/2009 10:00','12/2/2009 10:00'],// php regex spaces bug!
		   ];
	test_list_find_date_time('en',$tests);
}

function specific_test_find_dates_and_times_en()
{
	$text=file_get_contents('problem-text');
	echo strlen($text)."\n";
	$res=find_dates_and_times($text,true);
	print_r($res);
}

function test_find_times_en()
{
	$tests=['3am'=>['03:00'],
			'3AM'=>['03:00'],
			'3pm'=>['15:00'],
			'3PM'=>['15:00'],
			'3 P.M.'=>['15:00'],
			'3 A.M.'=>['03:00'],
			'3.30pm'=>['15:30'],
			'3.30am'=>['03:30'],
			];
	test_find_times('en',$tests);
}

?>