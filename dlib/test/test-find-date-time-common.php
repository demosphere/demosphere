<?php

function test_list_find_date_time($lang,$tests0)
{
	// build complete tests from test0
	$tests=[];
	foreach($tests0 as $test)
	{
		if($test[1]===false)
		{
			$tests[]=['text'=>$test[0],'stamps'=>[]];
			continue;
		}
		// parse good date(s) (first guess year if missing)
		$rawStamps=is_array($test[1]) ? $test[1] : [$test[1]];
		$stamps=[];
		foreach($rawStamps as $rawStamp)
		{
			if(preg_match('@^ *([0-9]+)/([0-9]+)([^/]*)$@',$rawStamp,$matches))
			{
				$rawStamp=$matches[1].'/'.$matches[2].'/'.
					find_date_time_guess_year($matches[1],$matches[2]).''.$matches[3];
			}
			$en=preg_replace('@^ *([0-9]+)/([0-9]+)@','$2/$1',$rawStamp);
			$ts=strtotime($en);
			if($ts===false)
			{
				echo "ignoring test :$en (bad date)\n";
				echo "ts: $ts\n";
				continue;
			}
			$stamps[]=$ts;
		}
		$tests[]=['text'=>$test[0],'stamps'=>$stamps];
	}

	//var_dump($tests);fatal('ii');

	$ok=true;
	foreach($tests as $nb=>$test)
	{
		$res=find_dates_and_times($test['text']);
		if($res!=$test['stamps'])
		{
			echo "test $nb failed !! \n";
			echo 'input  :'.$test['text']."\n";
			if(count($test['stamps'])===0){echo "correct: should not find anything\n";}
			else
			{
				foreach($test['stamps'] as $ngs=>$goodTS)
				{
					echo 'correct:'.$ngs.':'.strftime('%A %e %B %Y - %R',$goodTS)."\n";
					echo 'correct:'.$ngs.':'.date('r',$goodTS)."	 (".$goodTS.")\n";
				}
			}
			$ok=false;
			echo "found:\n";
			foreach($res as $ts)
			{
				echo "$ts : ".strftime('%A %e %B %Y - %R',$ts)." | ".date('r',$ts)."\n";
			}
			echo "\n";

			$fct='find_times_'.$lang;
			$times=$fct($test['text']);echo "times:";print_r($times);
			//fatal("abort\n");
			// verbose retry
			echo "verbose:\n";
			find_dates_and_times($test['text'],true);
			echo "over\n";
			fatal("abort after failed test\n");
		}
		else
		{echo "test $nb ok\n";}
		//echo "\n";exit(0);
	}

	if(!$ok){echo "SOME TESTS FAILED!\n";}			 
	test_assert($ok);
}

function test_find_times($lang,$tests)
{
	$ok=true;
	foreach($tests as $str=>$good)
	{
		$fct='find_times_'.$lang;
		$found=$fct($str);
		if($found!==$good)
		{
			$ok=false;
			echo "FAILED for: $str\n";
			echo "found:\n";
			print_r($found);
			echo "should be:\n";
			print_r($good);			
			fatal("abort after failed test\n");
		}
	}
	if(!$ok){echo "SOME TESTS FAILED!\n";}			 
	test_assert($ok);
}

?>