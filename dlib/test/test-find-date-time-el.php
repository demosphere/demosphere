<?php

require_once 'test-find-date-time-common.php';
require_once 'test-dlib.php';

function test_find_date_time_el()
{
	require_once 'dlib/tools.php';
	require_once 'dlib/find-date-time/find-date-time.php';
	require_once 'dlib/find-date-time/find-date-time-el.php';
	setlocale(LC_TIME, "el_GR.utf8");
	test_list_find_date_time_el();
	test_find_times_el();
	test_random_find_date_time_el(true);
}

// Automatically generates a large number of random dates and times
// using predefined patterns.
function test_random_find_date_time_el($verbose=false)
{
	$ok=0;
	$failed=0;
	//echo "FIXME (remove srand)\n";mt_srand(1234);  

	$altMonths=[
		['Ιανουάριος','Ianouarios','January','Ιανουαρίου'],
		['Φεβρουάριος','Febrouarios','february','Φεβρουαρίου','Fevrouarios'],
		['Μάρτιος','Martios','March','Μαρτίου'],
		['Απρίλιος','Aprilios','April','Απριλίου'],
		['Μάιος','Maios','May','Μαίου','Μαΐου'],
		['Ιούνιος','Iounios','June','Ιουνίου'],
		['Ιούλιος','Ioulios','July','Ιουλίου'],
		['Αύγουστος','Augoystos','August','Αυγούστου','Avgoustos'],
		['Σεπτέμβριος','Septembrios','September','Σεπτεμβρίου','Septemvrios'],
		['Οκτώβριος','Oktobrios','Oktober','Οκτωβρίου','Οκτώμβριος','Octovrios'],
		['Νοέμβριος','Noembrios','November','Νοεμβρίου','Noemvrios'],
		['Δεκέμβριος','Dekembrios','December','Δεκεμβρίου','Dekemvrios'],
			   ];
	$t=$altMonths;
	foreach($t as $k=>$a)
	{
		unset($altMonths[$k][2]);
		$altMonths[$k][]='%B';
		$altMonths[$k]=array_values($altMonths[$k]);
	}
	//print_r($altMonths);
	// try different dates
	for($i=0;$i<100;$i++)
	{
		if($i<50)
		{$dateTS=mktime(0,0,0,1,1,mt_rand(2000,2020))+mt_rand(0,366*24*3600);}
		else{$dateTS=time()+mt_rand(-366*24*3600,366*24*3600);}
		$m=date('m',$dateTS);
		$d=date('d',$dateTS);
		$y=date('Y',$dateTS);
		$dateTS=mktime(0,0,0,$m,$d,$y);
		if($verbose)echo "date:".strftime('%A %e %B %Y - %R',$dateTS)."\n";
		for($t=0;$t<20;$t++)
		{
			$hasTime=true;
			if(true ){$h=mt_rand(0,23);$min=mt_rand(0,59);}
			if($t<15){$h=mt_rand(0,23);$min=30;}
			if($t<10){$h=mt_rand(0,23);$min= 0;}
			if($t==0){$h=			 3;$min=33;$hasTime=false;}
			$hasTime=intval($hasTime);
			//********** FIXME
			if($hasTime && $h<=9){$h+=12;}
			if($hasTime && $h==3 && $min==33){$min+=1;}

			$ts=mktime($h,$min,0,$m,$d,$y);

			// has min, has time, has year
			//       y       y      y 
			//       y       y      n   
			//       y       n      y   X
			//       y       n      n   X
			//       n		 y		y
			//		 n		 y		n 
			//		 n		 n		y
			//		 n		 n		n 
			for($type=0;$type<4;$type++)
			{
				$hasMin =$type%2;
				$hasYear=($type>>1)%2;
				if(!$hasTime && $hasMin){continue;}
				if($hasTime && !$hasMin  && $min!=0){continue;}
				if(!$hasYear && abs($ts-time())>(3600*24*30*5)){continue;}

			
				$formats=['%A %e %B %Y - %R',
						  '%e %b %Y - %R',
						  '%d/%m/%Y - %R'];
				if(!$hasYear)
				{
					$formats=array_merge($formats,['%A %e %B - %R',
												   '%d/%m - %R']);
				}

				if(!$hasTime && !$hasYear)
				{
					$formats=array_merge($formats,['%e %B',
												   '%d/%m']);
				}
				if(!$hasTime)
				{
					$formats=array_merge($formats,['%A %e %B %Y']);
					$f0=$formats;
					foreach($f0 as $k=>$f){$formats[$k]=str_replace(' - %R','',$f);}
				}
				foreach($formats as $format)
				{
					// use alt months instead of %B
					$anm=mt_rand(0,count($altMonths[$m-1])-1);
					$altMonth=$altMonths[$m-1][$anm];
					$format1=str_replace('%B',$altMonth,$format);

					$string=strftime($format1,$ts);

					//				echo "m:$hasMin t:$hasTime y:$hasYear ".
					//					"time:".strftime('%A %e %B %Y - %R',$ts)." :::: ".
					//					$string."\n";
					$contexts=['sdf sfd %s sdfsd',
							   '%s',
							   '%s csdsdf ',
							   'fsdf %s'];
					foreach($contexts as $context)
					{
						$fstring=$string;
						//if(!(mt_rand()%2)){$fstring=str_replace(':','h',$fstring);}
						if(!(mt_rand()%4)){$fstring=str_replace('-',' στις ',$fstring);}
						if(!(mt_rand()%4)){$fstring=str_replace('-',' από τις ',$fstring);}
						if(!(mt_rand()%4)){$fstring=str_replace('-',',',$fstring);}
						if(!(mt_rand()%4)){$fstring=dlib_remove_accents($fstring);}
						if(!(mt_rand()%4)){$fstring=mb_strtoupper($fstring,'UTF-8');}
						if(!(mt_rand()%4)){$fstring=str_replace('-','&nbsp;',$fstring);}
						else
						if(!(mt_rand()%4)){$fstring=ent($fstring);}
						//echo 'pre-final string:'.$fstring."\n";
						$fstring=str_replace('%s',$fstring,$context);
						//if(!(mt_rand()%1000))echo 'final string:'.$fstring."\n";
						$res=find_dates_and_times($fstring);
						$rts=val($res,0,false);
						if(count($res)!=1 || $rts!=$ts)
						{
							echo "FAILED find_dates_and_times test for $ts \"$fstring\"\n";
							var_dump($res);
							echo "\n";
							$res=find_dates_and_times($fstring,true);
							fatal("abort after fail\n");
							$failed++;
						}
						else{$ok++;}
					}
				}
			}
		}
	}
	echo "test_random_find_date_time_el: ok:$ok failed:$failed\n";
	test_equals($failed,0);
}

// Test a list of predefined strings.
function test_list_find_date_time_el()
{
	$tests=[
		['30/01/20','30/01/2020 3:33'],
 		['Δευτέρα  7 Μαρτίου 2005  στις  22:00 csdsdf','7/3/2005 22:00'],
		['Θεσσαλονίκη, Πέμπτη 11 Μαρτίου 2010, 10:30','11/3/2010 10:30'],
		['1/5','1/05 3:33'],
		['5/1','5/01 3:33'],
		['Παρασκευή 9/1/2010',false],
		['03/06/09 :','03/06/2009 3:33'],
		['28-31/5/09','31/5/2009 3:33'],
		['1/13',false],
		['35/6',false],
		['35/6/02',false],
		['5/6/02','5/6/2002 3:33'],
		['5/6','5/6 3:33'],
		['05/14/09 06:28',false],
		['                                                                                                                                    jhslgfdj 17 Μαίου 2009','17/5/2009 3:33'],
		['&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;17 Μαίου 2009','17/5/2009 3:33'],
		['0.1',false],
		['15 Μαϊ 2009','15/5/2009 3:33'],
		['15 MAH 2009','15/5/2009 3:33'],
		['                            17 Μαίου 2009','17/5/2009 3:33'],
		['INTERZUM , 13 - 16 Μαίου 2009 ΚΟΛΩΝΙΑ','16/5/2009 3:33'],
		['         xxx 12/2/2009 10:00','12/2/2009 10:00'],// php regex spaces bug!
		['ύ Παιχνιδιού | 16 Μαΐου','16/5 3:33'],
		['Παιχνιδιού, 16 Μαϊου','16/5 3:33'],
		['18/ΜΑΙΟΥ 19.00','18/5 19:00'],
		['2008/09',false],
		['Τρίτη 5 Μαΐου 2009, 18:00','5/05/2009 18:00'],
		['Παρασκευή, 15 Μαΐου 2009 και ώρα 7:00 μ.μ.','15/05/2009 19:00'],
		['κυριακή 26 Απριλίου 2009 στις 19,00','26/04/2009 19:00'],
		['Savvato 25/4/2009 stis 12 to meshmeri','25/04/2009 12:00'],
		['Savvato 25/4/2009 stis 12:10 to meshmeri','25/04/2009 12:10'],
		['triti 28/4/2009, stis 8.00','28/04/2009 20:00'],
		['Τρίτη, 28/4/2009 στο Στέκι, στις 8.00','28/04/2009 20:00'],
		['(21-4) , γύρω στις 12','21/04 12:00'],
		['Τετάρτη 22 Απριλίου 2009, 22:00','22/04/2009 22:00'],
		['Τετάρτη 1 Απριλίου 2009 από τις 17.00 εως τις 20.00','01/04/2009 17:00'],
		['1 Απριλίου από τις 17.00 εως τις 20.00','01/04 17:00'],
		['ΤΡΙΤΗ 31/3/2009, 8.30','31/03/2009 20:30'],
		['31/3, 8.30','31/03 20:30'],
		['Σάββατο 28.3.2009 στις 18.00','28/03/2009 18:00'],
		['28.3 στις 18.00','28/03 18:00'],
		['28 Μαρτίου 2009','28/03/2009 3:33'],
		['Κυριακή 29 Μάρτη 2009 12 το μεσημέρι','29/03/2009 12:00'],
		['29 Μάρτη   12 το μεσημέρι','29/03 12:00'],
		['7 Απριλίου στις 20.30 μ.μ','07/04 20:30'],
		['7 Απριλίου στις 8.30 μ.μ','07/04 20:30'],
		['7 Απριλίου στις 8.30 Μ.Μ','07/04 20:30'],
		['7 Απριλίου στις 8.30 π.μ','07/04 8:30'],
		['7 Απριλίου στις 8.30 Π.Μ','07/04 8:30'],
		['Τρίτη 7 Απριλίου 2009 στις 20.30 μ.μ','07/04/2009 20:30'],
		['Τρίτη 24 Μαρτίου 2009 στις 7μ.μ.','24/03/2009 19:00'],
		['24 Μαρτίου στις 7μ.μ.','24/03 19:00'],
		['ΣΑΒΒΑΤΟ 21 ΜΑΡΤΙΟΥ 2009','21/03/2009 3:33'],
		['21 ΜΑΡΤΙΟΥ','21/03 3:33'],
		['17 Μαρτίου 2009, 6.30 μ.μ','17/03/2009 18:30'],
		['ΤΡΙΤΗ 17/03/2009','17/03/2009 3:33'],
		['17.03.2009','17/03/2009 3:33'],
//array('0930 – 2200','09:30 - 22:00'),
		['25 του Μάρτη 2009','25/3/2009 3:33'],
		['21ης Απρίλη 2009','21/4/2009 3:33'],
		['Δευτέρα, Οκτώβριος 06, 2008','6/10/2008 3:33'],
		['Δευτέρα, Οκτώβριος 15th, 2007','15/10/2007 3:33'],
		['Δευτέρα, Οκτώβριος 26 2009','26/10/2009 3:33'],
		['Οκτώβριος 02 2009','2/10/2009 3:33'],
		['Οκτώβριος 09 @ 15:59','9/10 15:59'],
		['1 Απρ. 2009','1/4/2009 3:33'],
		['27 Φεβ 2009','27/2/2009 3:33'],
		['28η Φεβρουαρίου 2005 ','28/2/2005 3:33'],
		['16, Φεβρουάριος 2005','16/2/2005 3:33'],
		['5 - Φεβρουάριος 2005','5/2/2005 3:33'],
		['Νοέμβριος 01, 2006, 04:55','1/11/2006 16:55'],
		['25-6-2008','25/6/2008 3:33'],

		   ];

	$months=['Ιανουάριος'  =>1,
			 'Ianouarios'  =>1,
			 'Ιανουαρίου'  =>1,
			 'Φεβρουάριος'  =>2,
			 'Febrouarios'  =>2,
			 'Φεβρουαρίου'  =>2,
			 'Μάρτιος'     =>3,
			 'Martios'     =>3,
			 'ΜΑΡΤΙΟΥ'     =>3,
			 'Μαρτίου'     =>3,
			 'Μάρτη'       =>3,
			 'Απρίλιος'    =>4,
			 'Aprilios'    =>4,
			 'Απριλίου'    =>4,
			 'Μάιος'      =>5,
			 'Maios'      =>5,
			 'Μαΐου'      =>5,
			 'Μαίου'      =>5,
			 'Ιούνιος'     =>6,
			 'Iounios'     =>6,
			 'Ιουνίου'     =>6,
			 'Ιούλιος'  =>7,
			 'Ioulios'  =>7,
			 'Ιουλίου'  =>7,
			 'Αύγουστος'     =>8,
			 'Augoystos'     =>8,
			 'Αυγούστου'     =>8,
			 'Σεπτέμβριος'=>9,
			 'Septembrios'=>9,
			 'Σεπτεμβρίου'=>9,
			 'Οκτώβριος'  =>10,
			 'Oktobrios'  =>10,
			 'Οκτωβρίου'  =>10,
			 'Οκτώμβριος'  =>10,
			 'Νοέμβριος' =>11,
			 'Noembrios' =>11,
			 'Νοεμβρίου' =>11,
			 'Δεκέμβριος' =>12,
			 'Dekembrios' =>12,
			 'Δεκεμβρίου' =>12,
			 'Ιαν.'  =>1, 
			 'Φεβ.'  =>2, 
			 'Μάρ.'	  =>3, 
			 'Απρ.'  =>4, 
			 'Μάι.'	  =>5, 
			 'Ιούν.' =>6, 
			 'Ιούλ.' =>7, 
			 'Αύγ.'	  =>8, 
			 'Σεπ.'  =>9, 
			 'Οκτ.'  =>10, 
			 'Νοέ.'	  =>11, 
			 'Δεκ.'  =>12, 
			];
	foreach($months as $m=>$n){$tests[]=["5 ".$m." 2009",'5/'.$n.'/2009 3:33'];}

	test_list_find_date_time('fr',$tests);

}

function specific_test_find_dates_and_times_el()
{
	$text=file_get_contents('problem-text');
	echo strlen($text)."\n";
	$res=find_dates_and_times($text,true);
	print_r($res);
}

function test_find_times_el()
{
	$tests=['3πμ'=>['03:00'],
			'3ΠΜ'=>['03:00'],
			'3μμ'=>['15:00'],
			'3ΜΜ'=>['15:00'],
			'3 Μ.Μ.'=>['15:00'],
			'3 Π.Μ.'=>['03:00'],
			'3.30μμ'=>['15:30'],
			'3.30πμ'=>['03:30'],
			'3.30πμ 9:00'=>['03:30','09:00'],
			];
	test_find_times('el',$tests);
}

?>