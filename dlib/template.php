<?php

/** @defgroup template template
 * A very simple template engine.  
 * Write your template in ordinary PHP, with a few small syntax additions to make it easier.
 * Your template file is "parsed" into a standard PHP file written into template_dir.
 *
 * Features:
 * - translations: < p>_(hello)</ p> => < p>< ?= ent(t('hello')); ? > </ p>
 * - variables: < p>hello $name</ p> => < p>hello < ?= ent($name); ? > </ p>
 * - escaped php short tag: < p>< ?: xyz('abc') ?></ p> => < p>hello < ?= ent(xyz('abc')); ? > </ p>
 * - "render" function propagates variables to called template.
 *
 * **Module use**: (none)
 *  @{
 */

//! $template_config : configuration for template.
//! List of $template_config keys:
//! - dir
//! - variables_hook
//! - path
//! - is_setup
//! - filename_cleanup_regexp
//! - common_vars
//! -::-;-::-
$template_config;


function template_auto_setup()
{
	global $template_config;
	if(isset($template_config['is_setup'])){return;}
	$defaults=['is_setup'=>true,
			   'path'=>[],
			   'filename_cleanup_regexp'=>'@^(/var/www/|/home/[^/]*/)@',
			   'common_vars'=>[]];
	$template_config=array_merge($defaults,$template_config);
}

//! Render a template that uses several parts into an array
function template_render_parts(string $fileName,array $variables=[])
{
	$raw=template_render($fileName,$variables);
	// result into parts and return array instead of string

	$partSeparator1='----a111bf07d8f7----PART';
	$partSeparator2='----276dc6e4d5cd----';
	if(strpos($raw,$partSeparator1)===false){fatal('template_render_parts: This template ('.$fileName.') does not contain parts');}

	$parts=[];
	$pos=false;
	while(($pos=strpos($raw,$partSeparator1.'BEGIN'))!==false)
	{
		$partName=substr($raw,$pos+strlen($partSeparator1.'BEGIN'),300);
		$partName=substr_replace($partName,'',strpos($partName,$partSeparator2));
		$start=$partSeparator1.'BEGIN'.$partName.$partSeparator2;
		$end  =$partSeparator1.'END'  .$partName.$partSeparator2;
		$endPos=strpos($raw,$end);
		$part=substr($raw,$pos+strlen($start),$endPos-($pos+strlen($start)));
		$raw=substr_replace($raw,'',$pos,$endPos+strlen($end)-$pos);
		$parts[$partName]=$part;
	}
	//$parts[0]=$raw; FIXME: optional
	if(isset($variables['template-part'])){return $parts[$variables['template-part']];}
	return $parts;
}

function template_render(string $fileName,array $variables=[])
{
	global $template_config;
	template_auto_setup();
	$variables=array_merge($template_config['common_vars'],$variables);

	// expand variable list (array variables without a key)
	$varLists=[];
	foreach(array_keys($variables) as $k)
	{
		if(is_int($k))
		{
			$varLists[]=$variables[$k];
			unset($variables[$k]);
		}
	}
	foreach($varLists as $varList){$variables=array_merge($variables,$varList);}

	// Call hook for changing variables
	if($template_config['variables_hook']!==false)
	{
		$template_config['variables_hook']($fileName,$variables);
	}

	ob_start();
	template_render_stage2($fileName,$variables);
	$res=ob_get_contents();
	ob_end_clean();

	return $res;
}

// A seperate ("stage2") fct is used to avoid adding local variables to templates.
function template_render_stage2(string $templateFileName,array $templateVariables)
{
	// avoid recursion when including all vars with get_defined_vars()
	unset($templateVariables['templateVariables']);
	unset($templateVariables['templateFileName']);

	extract($templateVariables);
	include template_compile($templateFileName,$templateVariables);
}

function template_compile(string $fileName,array $templateVariables)
{
	global $template_config;

	$template_dir=$template_config['dir'];

	$isStringTemplate=strpos($fileName,'template://string:')===0;

	if($isStringTemplate)
	{
		$template=substr($fileName,strlen('template://string:'));
		$processed=$template_dir.'/template--string--'.md5($template);
	}
	else
	{
		// Static var to keep track of procesed templatename=> actual template name
		// only used to determine dir in call stack
		static $templateFiles=[];
		// determine source directory and filename
		$stack=debug_backtrace();
		$currentDir=false;
		//var_dump($templateFiles,$stack);
		foreach($stack as $call)
		{
			if(isset($call['file']) &&
			   !preg_match('@(/|^)template.php$@',$call['file']) && 
			   !preg_match('@(/|^)template--string--@',$call['file']))
			{
				$includedFrom=$call['file'];
				$includedFrom=val($templateFiles,basename($includedFrom),$includedFrom);
				$currentDir=dirname($includedFrom);
				break;
			}
		}
		if($currentDir===false){template_error('template internal: failed finding currentDir');}
		$path=$template_config['path'];
		array_push($path,$currentDir);
		$ok=false;
		foreach($path as $dir)
		{
			if($dir==='.'){$dir=getcwd();}
			$fullFileName=$dir.'/'.$fileName;
			if(file_exists($fullFileName)){$ok=true;break;}
			$fullFileName=$dir.'/'.basename($fileName);
			if(file_exists($fullFileName)){$ok=true;break;}
		}
		if(!$ok)
		{template_error('template file not found: '.$fileName.': tried :'.implode(':',$path));}

		// determine cached filename
		$processed=$fullFileName;
		$processed=preg_replace($template_config['filename_cleanup_regexp'],'',$processed);
		$processed=str_replace('/','--',$processed);
		$processed=$template_dir.'/template--'.$processed;

		$templateFiles[basename($processed)]=$fullFileName;

		// check for cached version
		if(file_exists($processed) && 
		   filemtime($processed)>filemtime($fullFileName))
		{return $processed;}

		$template=file_get_contents($fullFileName);
	}

	// ************ actually compile the template **********
	
	// html entities function default: ent()
	$ent=val($templateVariables,'templateHtmlEntitiesFunction','ent');
	$tag='|Fx5CKy5f5zhqiQbA';
	// Add empty php tags in < ? spaceless ? > parts
	$template=preg_replace_callback('@<\?\s*spaceless\s*\?>(.*)<\?\s*end_spaceless\s*\?>@sU',
									function($m){return preg_replace('@>(\s*)<@s','><?$1?><','<? ?>'.$m[1].'<? ?>');},
									$template);

	$phpCode=[];
	// remove all php code from template
	$template=preg_replace_callback('@<\?.*\?>@Us',
									function($m) use(&$phpCode,$tag)
									{$t=$tag.count($phpCode).'|';$phpCode[$t]=$m[0];return $t;},
									$template);
	$template=preg_replace_callback('@(<script[^><]*>)(.*)(</script>)@Us',
									function($m) use(&$phpCode,$tag)
									{$t=$tag.count($phpCode).'|';$phpCode[$t]=$m[2];return $m[1].$t.$m[3];},
									$template);
	// replace translations in remaining html:  _(bla'n bla) -> < ?= ent(t('bla\'n bla')) ? >
	$template=preg_replace_callback('@_\(([^()]*)\)@',
									function($m)use($ent)
									{return "<?= ".$ent."(t('".str_replace("'","\\'",$m[1])."')) ?>";},
									$template);
	// replace variables in remaining html: $example -> < ?= ent($example) ? >
	// $ab ; $ab->cd  ; $ab->cd->ef ; $ab['cd'] ; $ab->cd['ef'] ; $ab[$c]
	// $ab[$c->d] ; $ab[$c][$d] ; $ab->cd->ef ; $a->c->e['x'][$y]  ; $a->b()
	$template=preg_replace('@(\$\w+->\w+\(\)|\$\w(\w|->\w|\[\s*([0-9]+|\'[^\']*\'|\$\w(\w|->)*)\s*\])*)~?@i',
						   '<?= '.$ent.'($1) ?>',$template);

	// replacements in PHP code:
	foreach(array_keys($phpCode) as $k)
	{
		// render( -> template_render_wrap(get_defined_vars(),
		$phpCode[$k]=preg_replace('@\b(render|render_parts)\s*\(@sU',
								  'template_$1_wrap(get_defined_vars(),',
								  $phpCode[$k]);		
		// short tag with html escape
		$phpCode[$k]=preg_replace('@^<\?:(.*)\?>$@sU','<?= '.$ent.'($1) ?>',$phpCode[$k]);
	}
	// re-merge removed php back into html
	$template=str_replace(array_keys($phpCode),$phpCode,$template);
	// special case: apply replacement twice : for php inside <script>
	$template=str_replace(array_keys($phpCode),$phpCode,$template);

	//echo "template compile:\n".$template."\n";
	file_put_contents($processed,$template);
	return $processed;
}

function template_error(string $msg)
{
	trigger_error("template error:$msg", E_USER_ERROR);
	fatal('abort');
}

// ****************************************************
// ** functions called from template
// ****************************************************

//! This is a convenience function for internal use by template code only.
//! Template parser: transforms render('xyz.tpl.php',$vars) into -> template_render_wrap(get_defined_vars(),'xyz.tpl.php',$vars)
//! template_render_wrap just takes the first arg, adds it to $vars and calls standard template_render('xyz.tpl.php',$vars)
function template_render_wrap()
{
	$args=func_get_args();
	$localVars=array_shift($args);
	if(!isset($args[1])){$args[1]=[];}
	array_unshift($args[1],$localVars);
	return call_user_func_array('template_render',$args);
}
function template_render_parts_wrap()
{
	$args=func_get_args();
	$localVars=array_shift($args);
	if(!isset($args[1])){$args[1]=[];}
	array_unshift($args[1],$localVars);
	return call_user_func_array('template_render_parts',$args);
}

//! typical usage "aaa, bbb, ccc," cleanup_output('@,\ s*$@s') => "aaa, bbb, ccc"
function cleanup_output(string $regexp,string $replacement='')
{
	$c=ob_get_contents();
	ob_clean();
	echo preg_replace($regexp,$replacement,$c);
}

//! Create a temporary label in HTML, that can be changed later on by calling this function again (with $value).
//! Example :
//! 	< div class="< ? template_tmp_label('mytmplabel') ? >"> 
//!        ... lots of conditions and html ...
//!     </div>
//!     < ? template_tmp_label('mytmplabel', $notEmpty ? 'not-empty' : 'empty'); ? >
function template_tmp_label(string $name,$value=false)
{
	static $labels=[];
	if($value===false)
	{
		// Use an unpredictable random label to avoid potential XSS
		$label=$name.'-'.mt_rand();
		echo $label;
		$labels[$name]=$label;
	}
	else
	{
		if(!isset($labels[$name])){fatal('unknown label');}
		$c=ob_get_contents();
		ob_clean();
		echo str_replace($labels[$name],$value,$c);
	}
}


function begin_part(string $name)
{
	$partSeparator1='----a111bf07d8f7----PART';
	$partSeparator2='----276dc6e4d5cd----';
	echo $partSeparator1.'BEGIN'.$name.$partSeparator2;
}
function end_part(string $name)
{
	$partSeparator1='----a111bf07d8f7----PART';
	$partSeparator2='----276dc6e4d5cd----';
	echo $partSeparator1.'END'  .$name.$partSeparator2;
}

/** @} */

?>