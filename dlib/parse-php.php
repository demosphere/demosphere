<?php

//! Returns an array with comments for a DBObject class and all of the fields in its schema.
function parse_php_dbobject_comments($className)
{
	$reflector = new ReflectionClass($className);
	$fileName=$reflector->getFileName();

	$searchWords=[];
	$searchWords[]=$className;

	$schema=$className::getSchema();
	foreach($schema as $field=>$unused)
	{
		$searchWords[]='$'.$field;
		$searchWords[]='$'.$field.'_';
	}

	// First pass: tokenize the file
	$parsed=parse_php_tokenize(file_get_contents($fileName),$searchWords);
	//echo $parsed['tokenized'];

	// Second pass: use the tokens to find comments
	$res=[];
	$res[$className]=parse_php_class_comments($parsed,0);
	foreach($schema as $field=>$unused)
	{
		$res[$field]=parse_php_dbobject_field_comments($parsed,
											  array_search('$'.$field    ,$searchWords),
											  array_search('$'.$field.'_',$searchWords));
	}
	return $res;
}


//! Returns the comments right before a class declaration. 
//! Uses the parsed tokens returned by parse_php_tokenize() to find the comments.
//! $parsed is the result of the call to parse_php_tokenize().
//! $classNameNb is the index of the className in the $searchWords array provided to parse_php_tokenize()
//! The result is run through parse_php_comment_to_text() to extract the actual comment text.
function parse_php_class_comments($parsed,$classNameNb)
{
	$found=false;
	$res='';
	if(preg_match('@((C[\n])+[\n]*|D[\n]*)L(S'.sprintf('%04d',$classNameNb).')@s',$parsed['tokenized' ],$matches,PREG_OFFSET_CAPTURE))
	{
		// comments
		for($pos=$matches[1][1];$pos<$matches[1][1]+strlen($matches[1][0]);$pos++)
		{
			if(isset($parsed['tokensDesc'][$pos]))
			{
				$found=true;
				$res.=$parsed['tokensDesc'][$pos][1];
			}
		}
	}
	if(!$found){return false;}
	return parse_php_comment_to_text($res);
}

function parse_php_dbobject_field_comments($parsed,$fieldNb,$fieldSchemaNb)
{
	$found=false;
	$res='';
	if(preg_match('@((C[\n])+[\n]*|D[\n]*)'.
				  '[P]V'.sprintf('%04d',$fieldNb      ).'[^\n]*[\n]'.
				  'TV'  .sprintf('%04d',$fieldSchemaNb).
				  '@s',
				  $parsed['tokenized' ],$matches,PREG_OFFSET_CAPTURE))
	{
		// comments
		for($pos=$matches[1][1];$pos<$matches[1][1]+strlen($matches[1][0]);$pos++)
		{
			if(isset($parsed['tokensDesc'][$pos]))
			{
				$found=true;
				$res.=$parsed['tokensDesc'][$pos][1];
			}
		}
	}
	if(!$found){return false;}
	return parse_php_comment_to_text($res);
}

function parse_php_comment_to_text($str)
{
	// DOC comment
	if(preg_match('@^/\*\*\s*\n \*\s+.*\n \*/\s*$@s',$str))
	{
		$str=preg_replace('@^/\*\*\s*\n@s','',$str);
		$str=preg_replace('@\n \*/\s*$@s','',$str);
		$str=preg_replace('@^ \*( |$)@m',"",$str);
	}
	else
	{
		$str=preg_replace('@^//!? @sm','',$str);
	}
	return $str;
}

//! Trasforms a PHP file into a string of tokens.
//! A subset of PHP tokens are transformed into single letters (for example: 'class' declaration => 'L' ).
//! Strings provided in $searchWords are marked by a zero padded number.
//! The resulting string and token descriptions can then be used by functions like parse_php_class_comments().
function parse_php_tokenize($phpCode,$searchWords=[],$removeWhitespace=true)
{
	$searchWords=array_flip($searchWords);
	//print_r($searchWords);

	$tokens=token_get_all($phpCode);
	$posTokens=[];

	// 	foreach($tokens as $token)
	// 	{
	// 		if(is_string($token)){echo 'string: "'.$token.'"'."\n";}
	// 		else
	// 		{
	// 			echo $token[2].':'.token_name($token[0]).': '.str_replace("\n","\\n",isset($token[1]) ? '"'.$token[1].'"' : '*')."\n";
	// 		}
	// 	}

	// Build string for matching using regexp
	// Feel free to add more tokens from here:
    // http://php.net/manual/en/tokens.php
	// As long as you don't override existing letters.
	$map=[
		  T_DOC_COMMENT                =>'D',
		  T_COMMENT                    =>'C',
		  T_CLASS                      =>'L',
		  T_WHITESPACE                 =>'W',
		  T_STRING                     =>'S',
		  T_EXTENDS					   =>'E',
		  T_PUBLIC					   =>'P',
		  T_PRIVATE                    =>'R',
		  T_VARIABLE				   =>'V',
		  T_STATIC					   =>'T',
		  T_ARRAY					   =>'A',
		  T_CONSTANT_ENCAPSED_STRING   =>'O',
		  ];
	$lastLine=false;
	$string='';
	foreach($tokens as $token)
	{
		if(is_string($token)){continue;}
		if($removeWhitespace && isset($token[0]) && $token[0]===T_WHITESPACE){continue;}
		$lineNb=$token[2];
		if($lineNb!==$lastLine){$string.="\n";$lastLine=$lineNb;}
		if(isset($token[0]) && isset($map[$token[0]]))
		{
			$posTokens[strlen($string)]=$token;
			$string.=$map[$token[0]];
		}
		if(isset($token[1]) && isset($searchWords[$token[1]])){$string.=sprintf('%04d',$searchWords[$token[1]]);}
	}

	return ['tokenized'=>$string,'tokensDesc'=>$posTokens];
}

?>