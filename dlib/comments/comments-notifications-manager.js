(function()
{
window.dlib_comments_notifications_manager = window.dlib_comments_notifications_manager || {};
var ns=dlib_comments_notifications_manager;

$(document).ready(function()
{
	let isSubscribed = false;
	let serviceWorkerReg = null;

	// "Subscribe" button. 
	$('#subscribe').click(async function()
	{
		if(notifications_vapid_public_key===false)
		{
			alert("Notifications vapid public key is not set.\nThis is probably a server config problem.\nPlease contact admin.");
			return;
		}

		try
		{
			// Create (or find existing) ServiceWorker.
			let swReg=await navigator.serviceWorker.register(base_url+'/dlib/comments/comments-notifications-sw.js');
			let subscription=await swReg.pushManager.subscribe(
				{
					userVisibleOnly: true,
					applicationServerKey: urlB64ToUint8Array(notifications_vapid_public_key)
				});
			// Save subscription to server.
			await $.post(base_url+'/comment/notifications/subscription/save',
						 {subscription:subscription.toJSON()});
			
			// Reload page to refresh list of subscriptions
			window.location.reload();
		}catch(e)
		{
			alert('Subscribe failed');
		}
	});

	// Highlight this browser's subscriptions
	if(isOwnNotifications){check_active_subscriptions();}
	
	$('.unsubscribe').click(async function()
	{
		let li=$(this).parents('#subscriptions>li');
		let endpoint=li.attr('data-endpoint');
		if(isOwnNotifications)
		{
			let subscription=await get_subscription(endpoint);
			if(subscription){subscription.unsubscribe();}
		}
		await $.post(base_url+'/comment/notifications/subscription/update',
					 {action: 'delete',
					  endpoint});
		li.remove();
	});

	// Night time text inputs
	$('input[name=nightStart],input[name=nightEnd]').change(function()
	{
		let li=$(this).parents('#subscriptions>li');
		let endpoint=li.attr('data-endpoint');
		$.post(base_url+'/comment/notifications/subscription/update',
			   {action: 'night',
				endpoint,
				'nightStart': $('input[name="nightStart"]').val(),
				'nightEnd'  : $('input[name="nightEnd"]'  ).val(),
			   },
			   ()=>li.find('.night').removeClass('error')
			  )
		.fail(()=>li.find('.night').addClass('error'));
	});
  
});

//! Gets list of actual subscriptions from the browser and compares to list displayed by server.
//! Subscriptions that are active on this browser are highlighted.
async function check_active_subscriptions()
{
	let isSubscribed=false; 
	let registrations=await navigator.serviceWorker.getRegistrations();

	for(const registration of registrations)
	{
		let subscription=await registration.pushManager.getSubscription();
		if(!subscription){continue;}
		let found=false;
		$('#subscriptions li').each(function()
		{
			if($(this).attr('data-endpoint')===subscription.endpoint)
			{
				$(this).addClass('this-browser');
				isSubscribed=true;
				found=true;
			}
		});
		// If this is a  stale subscription, unsubscribe it
		// (stale: this browser is subscribed, but subscription is not on server)
		if(!found)
		{
			subscription.unsubscribe();
			alert(t('cleanup-subscribtion'));
		}
	}

	$('#is-subscribed').toggleClass('hidden',!isSubscribed);
	$('#subscribe').text(!isSubscribed ? t('subscribe') : t('re-subscribe'));
}

//! Returns the browser subscription object for a given endpoint.
async function get_subscription(endpoint)
{
	let registrations=await navigator.serviceWorker.getRegistrations();

	for(const registration of registrations)
	{
		let subscription=await registration.pushManager.getSubscription();
		if(subscription && subscription.endpoint===endpoint){return subscription;}
	}
	return false;
}

function urlB64ToUint8Array(base64String) 
{
	const padding = '='.repeat((4 - base64String.length % 4) % 4);
	const base64 = (base64String + padding)
		  .replace(/\-/g, '+')
		  .replace(/_/g, '/');

	const rawData = window.atob(base64);
	const outputArray = new Uint8Array(rawData.length);

	for (let i = 0; i < rawData.length; ++i) {
		outputArray[i] = rawData.charCodeAt(i);
	}
	return outputArray;
}

// Exports
// dlib_comments_notifications_manager.xyz=xyz;

}());
