<? // FIXMEc this whole page is work in progress ?>
<h1 class="page-title">
	<?if($filterField===false){?>
		_(Comments)
	<?}else if($filterField==='userId'){?>
		<?: t('Comments by:') ?> 
		<a href="<?: $comments_config['user']['url']($filterValue) ?>"><?: $comments_config['user']['nameFct']($filterValue) ?></a>
	<?}else if($filterField==='sessionN'){?>
		<?: t('Comments by "@name"',['@name'=>Comment::anonName($filterValue)])?>
	<?}else if($filterField==='userAgentHash'){?>
		<?: t('Comments by @filterField "@filterValue"',['@filterField'=>Comment::statFieldName($filterField),'@filterValue'=>hexdec(substr($filterValue,0,6))%1000])?> 
	<?}else{?>
		<?: t('Comments by @filterField "@filterValue"',['@filterField'=>Comment::statFieldName($filterField),'@filterValue'=>dlib_truncate($filterValue,20)])?>
	<?}?>
</h1>

<? if($filterField===false){ ?>
	<a  id="notifications" title="_(Get notifications when new comments are posted)" href="$base_url/comment/notifications/manager">🔔</a>
<?}?>


<? if($filterField!==false){ ?>
	<div id="stats">
		<? if($stats['@np']+$stats['@nr']>2){?>
			<span id="accept-rate"><?: round(100*$stats['@np']/($stats['@np']+$stats['@nr'])) ?>% </span> :
		<?}?>
		<?: t('@nn need attention, @np published, @nr rejected',$stats) ?>
	</div>
<?}?>

<div id="filter-field-description">
	<? if(isset($remarkForm)){echo $remarkForm;} ?>
	<? if($filterField==='ip'){ ?>
		$reverse
		<? if(isset($comments_config['manager_ip'])){?>
			<?= $comments_config['manager_ip']($filterValue); ?>
		<?}?>
		<? if($whois!==null){?>
			<p><a href="$base_url/comment/ipwhois/$whois->id">whois:</a> <br/>
				<? foreach(['country','org-?name','netname','role','descr'] as $name){ ?>
					<? $v=$whois->getValue($name); ?>
					<? if($v!==false){?> $v<br/><?}?>
				<?}?>
				<? if(count($whoisIps)){?>
					_(Other IPs:)
					<? foreach(array_slice($whoisIps,0,20) as $ip=>$ct){?>
						<a href="$base_url/comment/manager?filterField=ip&filterValue=$ip"><?: comments_field_or_remark_name('ip',$ip)?> ($ct)</a>,
					<?}?>
					<?if(count($whoisIps)>20){?> ... <?}?>
				<?}?>
			</p>
		<?}?>
	<?}?>
	<? if($filterField==='sessionN'){ ?>
		<? if($firstAccess){?>
		    <p>_(Anonymous user first seen:)
				<?:  dlib_format_date('full-date-time'     ,$firstAccess) ?>
				(<?: dlib_format_date('relative-short-full',$firstAccess) ?>)
			<?}?>
	<?}?>

	<? if($filterField==='userAgentHash'){ ?>
		<p><?: comments_user_agent_format($userAgent) ?></p>
		<p id="full-user-agent">$userAgent</p>
	<?}?>
</div>



<? if($filterField!==false){ ?>
	<div id="bans">
		<table class="bans-list">
			<?if(count($bans)){?>
				<tr><th>_(start)</th><th>_(end)</th><th>_(action)</th><th>_(label)</th><th>_(labelValue)</th></tr>
			<?}?>
			<? foreach($bans as $ban){ ?>
				<tr>
					<td><?: dlib_format_date('relative-short-full',$ban->start) ?></td>
					<td><?: dlib_format_date('relative-short-full',$ban->end) ?></td>
					<td>$ban->action</td>
					<td>$ban->label</td>
					<td>
						<? if(strpos($ban->label,'@mod-button')===0 && Comment::fetch($ban->labelValue,false)){?>
							<a href="<?: $comments_config['link'](Comment::fetch($ban->labelValue)) ?>">$ban->labelValue</a>
						<?}else{?>
							$ban->labelValue<?}?>
					</td>
				</tr>
			<? } ?>
		</table>
	</div>
<?}?>

<?= render('dlib/pager.tpl.php') ?>
<div id="comments" class="manager-comments-list admin">
	<?	foreach($comments as $comment){ ?>
			<div class="comment-info">
				<?= $comments_config['manager_item']($comment) ?>
				<? if($comment->parentId){?>
					<span><a href="<?: $comments_config['link']($comment->useParent()) ?>">_(replies to)</a></span>
				<?}?>
				<? if($comment->nbChildren()>0){?>
					<span><?: t('@n replies',['@n'=>$comment->nbChildren()]) ?></span>
				<?}?>
			</div>
			<?= comments_render_single($comment,['isAdmin'=>true,'statsCache'=>$statsCache]) ?>
	<?}?>
</div>
