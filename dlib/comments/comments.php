<?php

/** @defgroup comments comments
 * Add comment forum functionality to any page.
 * This is meant as a "strap-on" to existing pages.
 * Comments are listed on pages based on pageType/typedId.
 * For example, to add comments to a page that displays Article 123, use comments_page_render('article',123).
 * Each comment is an instance of the Comment class.
 * Comments are added, edited and deleted using a js/ajax interface.
 *
 * This module does not depend on a user class. 
 * Simple user information (id, permissions,nameFct) is provided during config/setup.
 *
 * $comments_config: 
 * - user (required): array('id'=>(int),'isAdmin'=>(bool),'nameFct'=>function returns name from userId,'canView'=> function)
 * - link : function that returns link from comment
 * - disabled : global kill switch for all comments (only admins can view)
 * 
 * - Module uses: DBObject, \ref database, \ref form, \ref template
 *  @{
 */


//! $comments_config : configuration for comments.
//! List of $comments_config keys:
//! - user
//! - link
//! - site_settings
//! - page_types
//! - save_hook
//! - manager_ip
//! - form_alter
//! - delete_hook
//! - body_render
//! - body_post_render
//! - too_long_max_lines
//! - notifications_vapid_subject
//! - manager_item
//! - line_width
//! - guidelines_url
//! - disabled
//! -::-;-::-
$comments_config;


//! Create comment related url handlers. See dlib_exec_request_path()
function comments_add_paths(array &$urls,array &$options)
{
	$urls[]=['path'=>'comment/form',
			 'output'=>'json',
			 'roles'=>true,
			 'file'=>'dlib/comments/comments.php',
			 'function'=>'comments_form',
			];
	$urls[]=['path'=>'@^comment/([0-9]+)/edit$@',
			 'roles'=>true,
			 'output'=>'json',
			 'file'=>'dlib/comments/comments.php',
			 'function'=>'comments_form',
			];
	$urls[]=['path'=>'@^comment/([0-9]+)/moderation-button@',
			 'roles'=>true,
			 'output'=>'json',
			 'file'=>'dlib/comments/comments.php',
			 'function'=>'comments_moderation_button',
			];
	$urls[]=['path'=>'@^comment/([0-9]+)/see-more@',
			 'roles'=>true,
			 'output'=>'html',
			 'file'=>'dlib/comments/comments.php',
			 'function'=>'comments_see_more',
			];
	$urls[]=['path'=>'comment/manager',
			 'roles'=>true,
			 'file'=>'dlib/comments/comments.php',
			 'function'=>'comments_manager',
			];
	$urls[]=['path'=>'@^comment/ipwhois/([0-9]+)@',
			 'roles'=>true,
			 'file'=>'dlib/comments/comments.php',
			 'function'=>'comments_ipwhois',
			];
	$urls[]=['path'=>'comment/page-settings',
			 'roles'=>true,
			 'output'=>false,
			 'file'=>'dlib/comments/comments.php',
			 'function'=>'comments_page_settings_ajax',
			];
	$urls[]=['path'=>'comment/list',
			 'roles'=>true,
			 'output'=>'html',
			 'file'=>'dlib/comments/comments.php',
			 'function'=>'comments_list_ajax',
			];
	$urls[]=['path'=>'comment/notifications/manager',
			 'roles'=>true,
			 'file'=>'dlib/comments/comments-notifications.php',
			 'function'=>'comments_notifications_manager',
			];
	$urls[]=['path'=>'comment/notifications/subscription/save',
			 'roles'=>true,
			 'output'=>'json',
			 'file'=>'dlib/comments/comments-notifications.php',
			 'function'=>'comments_notifications_subscription_save',
			];
	$urls[]=['path'=>'comment/notifications/subscription/update',
			 'roles'=>true,
			 'output'=>'json',
			 'file'=>'dlib/comments/comments-notifications.php',
			 'function'=>'comments_notifications_subscription_update',
			];

}

//! Renders the whole comments section of a given pageType/pageId.
//! This is the function you should call to display comments on a page.
//! $options: divId, topText
function comments_page_render(string $pageType,int $pageId,array $options=[]): string
{
	global $comments_config,$currentPage;

	if(!comments_page_can_view($pageType,$pageId)){return '';}

	comments_js_translations();

	$user=$comments_config['user'];
	$isAdmin=$user['isAdmin'];

	$pageSettings=comments_page_settings($pageType,$pageId);

	$currentPage->addCss('dlib/comments/comments.css');
	$js=file_get_contents('dlib/comments/comments-minimal.js');
	$currentPage->addJs(preg_replace('@^\s*(//.*$)?@m','',$js),'inline');

	$commentsList=comments_list_render($pageType,$pageId,20,0,true);
	$nbComments=Comment::nb($pageType,$pageId);

	return template_render('comments.tpl.php',
						   [compact('isAdmin','commentsList','pageType','pageId','comments_config','nbComments','options','pageSettings'),]
						   );
}

//! Whether a user can view any events of a given pageType::pageId (eg: a page with comments).
//! This can be determined by both page-specific settings and actual access to pageType::pageId.
function comments_page_can_view(string $pageType,int $pageId): bool
{
	global $comments_config,$currentPage;
	$user=$comments_config['user'];
	$isAdmin=$user['isAdmin'];
	if(!$user['canView']($pageType,$pageId)){return false;}

	$settings=comments_page_settings($pageType,$pageId);

	return $isAdmin || $settings!=='disabled';	
}

//! Returns settings ('disabled','readonly','pre-mod','enabled') for this page.
//! This is a result of merging page specific settings (the select button) and global site_settings
function comments_page_settings(string $pageType,int $pageId): string
{
	global $comments_config;
	$res=array_search(variable_get($pageType.'-'.$pageId,'comments-page-settings','enabled'),Comment::$settings);
	$max=array_search($comments_config['site_settings'],Comment::$settings);
	$res=min($res,$max);
	$res=Comment::$settings[$res];
	return $res;
}

//! Ajax call to change comment page settings  ('disabled','readonly','pre-mod','enabled') for a given pageType/pageId
function comments_page_settings_ajax()
{
	global $comments_config;
	$user=$comments_config['user'];
	if(!$user['isAdmin']){dlib_permission_denied_403();}
	dlib_check_form_token('comments-page-settings');

	$pageType=$_POST['pageType'];
	if(!isset($comments_config['page_types'][$pageType])){dlib_bad_request_400('comments_page_settings_ajax: invalid pageType');}
	$pageId=intval($_POST['pageId']);
	$settings=$_POST['page-settings'];
	if(array_search($settings,Comment::$settings)===false){dlib_bad_request_400('comments_page_settings_ajax: invalid settings');}
	variable_set($pageType.'-'.$pageId,'comments-page-settings',$settings);

	echo 'page-settings-ok';
}

function comments_js_translations()
{
	global $currentPage;
	$currentPage->addJsTranslations(['confirm-cancel'      =>t('Your edits will be lost. Are you sure ?'),
									 'confirm-delete'      =>t('Are you sure you want to delete this comment?'),
									 'confirm-delete-admin'=>t('Are you sure you want to delete this comment? Generally it is better to un-publish it.'),
									]);
}

//! Returns HTML for comments on a given page. 
//! If there are too many comments, a "more-comments" <div> is added at the end of the list.
//! When this <div> scrolls into view, js will automatically fetch more comments.
function comments_list_render(string $pageType,int $pageId,int $limit,int $offset=0,bool $addMore=false): string
{
	global $comments_config,$currentPage;
	$user=$comments_config['user'];
	$isAdmin=$user['isAdmin'];

	// Special case: when displaying a slice we need to detect missing parent at beginning.
	// For that we need previous comment.
	$parentFix=$offset>0 ? 1:0;

	// ****  SQL query for comments
	// Important: for consistent results, the conditions here must match those in Comment::canView()
	$sql="WHERE pageType='%s' AND pageId=%d ";
	if(!$isAdmin)
	{
		$sessionN=dbsessions_get_n();
		$sql.=" AND (isPublished=1 ".
			($sessionN   ? " OR sessionN=".$sessionN         : '').
			($user['id'] ? " OR userId=".intval($user['id']) : '').
			") ";
	}
	$sql.=" ORDER BY sortKey DESC, created ASC ".
		  " LIMIT %d OFFSET %d";

	$comments=Comment::fetchList($sql,$pageType,$pageId,$limit+$parentFix,$offset-$parentFix);

	$statsCache=null;
	if($isAdmin){$statsCache=comments_list_stats_cache($comments);}

	// ******** Actually render each comment
	$out='';
	$prevComment=false;
	foreach($comments as $comment)
	{ 
		if($parentFix && $prevComment===false){$prevComment=$comment;continue;}
		// detect missing parent
		if(($prevComment===false && $comment->parentId) || 
		   ($comment->parentId && $prevComment->id!=$comment->parentId && $prevComment->parentId!=$comment->parentId))
		{
			$out.='<div id="comment-'.$comment->parentId.'">'.t('This comment was deleted').'</div>';
		}
		
		$skipContext=true;
		$out.=comments_render_single($comment,compact('isAdmin','statsCache','skipContext'));
		$prevComment=$comment;
	}
	
	if($addMore && count($comments)===$limit+$parentFix)
	{
		$out.='<div class="more-comments" data-offset="'.($offset+$limit).'">...</div>';
	}

	return $out;
}

//! Returns a cache to avoid calling Comment:stats() for each comment.
//! This uses aggregate queries which are faster than repeatedly doing smaller queries.
function comments_list_stats_cache($comments)
{
	// Any changes here must be reflected in Comment:stats()
	$res=['userId'=>[],'sessionN'=>[],'ip'=>[],'userAgentHash'=>[]];
	if(count($comments)===0){return $res;}
	foreach($res as $field=>&$stat)
	{
		$fieldValues=array_unique(dlib_object_column($comments,$field));
		$fieldValues=array_filter($fieldValues,function($v){return $v!=='' && $v!==0;});
		if(!count($fieldValues)){continue;}
		$fieldValuesSql=' '.$field." IN (".implode(',',array_map(function($s){return "'".db_escape_string($s)."'";},$fieldValues)).") ";
		$fieldStats=db_arrays("SELECT ".$field.",isPublished, needsAttention,COUNT(*) AS nb FROM Comment WHERE ".
							  $fieldValuesSql.
							  " AND created>%d ".
							  " GROUP BY ".$field.",isPublished,needsAttention",
							  strtotime('6 months ago'));

		foreach($fieldValues as $fieldValue)
		{
			$stat[$fieldValue]=['needs-attention'=>0,'published'=>0,'not-published'=>0,'same-page'=>[]];
		}
		foreach($fieldStats as $fieldStat)
		{
			$h=&$stat[$fieldStat[$field]];
			if($fieldStat['needsAttention']==='1'){$h['needs-attention']+=$fieldStat['nb'];}
			else
			if($fieldStat['isPublished'   ]==='1'){$h[      'published']=(int)$fieldStat['nb'];}
			else
			if($fieldStat['isPublished'   ]==='0'){$h[  'not-published']=(int)$fieldStat['nb'];}
		}

		// Count comments per page 
		$pageIds=array_unique(dlib_object_column($comments,'pageId'));
		$pageStats=db_arrays("SELECT ".$field.",pageType,pageId,COUNT(*) AS nb  FROM Comment WHERE ".
							 $fieldValuesSql.
							 // For performance:
							 " AND pageId IN (".implode(',',array_map('intval',$pageIds)).") ". 
							 " GROUP BY ".$field.",pageType,pageId");
		foreach($pageStats as $pageStat)
		{
		 	$stat[$pageStat[$field]]['same-page'][$pageStat['pageType']][$pageStat['pageId']]=intval($pageStat['nb']);
		}
	}
	return $res;
}

//! Returns HTML for a single comment
//! Optional in $vars: isAdmin, message, stats, skipContext
function comments_render_single(Comment $comment,array $vars=[]): string
{
	global $comments_config;
	$user=$comments_config['user'];
	if(!isset($vars['isAdmin'])){$vars['isAdmin']=$user['isAdmin'];}
	$vars['comment']=$comment;

	$fieldNames['sessionN'     ]='SESS';
	$fieldNames['ip'           ]='IP-'.hexdec(substr(hash('sha256',$comment->ip),0,6))%1000;
	$fieldNames['userAgentHash']='UA-'.hexdec(substr($comment->userAgentHash    ,0,6))%1000;
	$remarks=$comment->remarks();
	if(isset($remarks['name']['sessionN'     ])){$fieldNames['sessionN'     ]=$remarks['name']['sessionN'     ];}
	if(isset($remarks['name']['ip'           ])){$fieldNames['ip'           ]=$remarks['name']['ip'           ];}
	if(isset($remarks['name']['userAgentHash'])){$fieldNames['userAgentHash']=$remarks['name']['userAgentHash'];}

	$banned=[];
	$banPopups=[];
	foreach(['userId','sessionN','ip','userAgentHash'] as $field)
	{
		if(!$vars['isAdmin']){$banned[$field]='';continue;}
		$bans=Ban::getForBanType($field,$comment->$field);
		$banned[$field]='';
		$banPopups[$field]='';
		$delay='-';
		if(count($bans)!==0)
		{
			$end=max(dlib_object_column($bans,'end'));
			if($end<Ban::permanent()){$delay=dlib_format_date('relative-short-full',$end);}
			else                     {$delay=t('permanent');}
		}

		$banned[$field]=count($bans)!==0 ?  t('Banned until:').' '.$delay : '';
		$banPopups[$field]='<span class="ban-popup ban-popup-top">'.ent($delay).'</span>'.
			'<span class="ban-popup ban-popup-bottom"><button name="shorten" >-</button><button name="lengthen">+</button></span>';
	}
	if($comment->userId===0){$banned['userId']=$banned['sessionN'];}
	$vars['fieldNames']=$fieldNames;
	$vars['banned']=$banned;
	$vars['banPopups']=$banPopups;
	$vars['comments_config']=$comments_config;

	require_once 'demosphere-date-time.php';
	require_once 'dlib/template.php';
	return template_render('comment.tpl.php',$vars);
}

//! Returns HTML for the moderation statistics fields (SESS,IP, UA) of a given comment.
function comments_stats_render(Comment $comment,string $field,array $cached=null): string
{
	static $titles=false;
	require_once 'lib/UserAgentParser.php';

	if($titles===false)
	{
		$titles=
			[
				'same-page'      =>t('This @field has made @n comments on same page.'),
				'needs-attention'=>t('This @field has @n comments that need attention.'),
				'published'      =>t('This @field has @n published comments in the past few months.'),
				'not-published'  =>t('This @field has @n rejected comments in the past few months.'),
			];
	}
	$out='';
	$title='';

	if($field==='sessionN')
	{
		$firstAccess=db_result_check("SELECT first_access FROM dbsessions WHERE n=%d",$comment->sessionN ? $comment->sessionN : -1);
		if($firstAccess!==false && $comment->created<$firstAccess+60*3)
		$title.=ent(t('Comment/first access').':'.dlib_format_date('relative-short-full',$comment->created,$firstAccess)).'&#13;';
	}

	foreach($titles as $stat=>$unused)
	{
		$n=$comment->stats($field,$stat,$cached);
		$out.='<span class="'.$stat.'">'.$n.'</span>';
		$title.=ent(t($titles[$stat],['@n'=>$n,'@field'=>Comment::statFieldName($field)]))." &#13;";
	}
	$title=substr($title,0,-6);
	$out='<span class="stats" title="'.$title.'">'.$out;
	$out.='</span>';
	return $out;
}

//! Returns HTML for a comment's body. The HTML may be truncated if it is too high.
//! Optionally returns overflow status in $overflows ('too-long' or 'truncated').
function comments_body_render(string $body,bool $checkTooLong=false,string &$overflows=null): string
{
	global $comments_config;
	$out=ent($body);
	$out=preg_replace("@\n\r?@",'<br/>',$out);
	if(isset($comments_config['body_render'])){$out=$comments_config['body_render']($out);}
	$out=trim($out);
	
	require_once 'dlib/filter-xss.php';
	if(!$checkTooLong){$res=$out;}
	else
	{

		// We want to determine if:
		// 1) text is too long
		//    The comment is displayed with a max-height and a "see-more" button.
		// 2) text is way too long (need to truncate and load text with ajax)
		//    The comment is displayed with a max-height and a "see-more" button ... and the actual text is truncated.
		// The height of the text is roughly estimated by simulating a simple "render" of the html (flowing into lines).
		// This is inaccurate, as we don't really know the actual line width (mobile, reponsive,...).
		// However, this is not a big problem:
		// * If the actual height is larger than our estimate, we will be displaying full comments that are larger than we wished.
		//   This leads to a larger display height than we wanted, but is not a big deal.
		// * If the actual height is smaller than our estimate, we will display "see more" buttons on comments that are already displayed in full.
		//   This is more annoying.
		// Conclusion:
		// * set $tooLongMaxLines to around 150% the actual number of lines displayed when CSS max-height is applied
		//    Example: max-height: 10em => around 9 lines are actually displayed => $tooLongMaxLines=15
		// * $lineWidth should be the max line width for the display
	
		require_once 'dlib/html-tools.php';
		$isTooLong=false;
		$isTruncated=false;
		$tooLongMaxLines=$comments_config['too_long_max_lines'];
		$truncateMaxLines=2*$tooLongMaxLines;
		$lineWidth=$comments_config['line_width'];
		$posX=0;// px
		$posY=0;// lines
		$res='';
		$inTextUrl=false;
		// split into plain text and html tags
		foreach(preg_split('@(<[^>]*>)@',$out,-1, PREG_SPLIT_DELIM_CAPTURE) as $n=>$part)
		{
			if(($n%2))
			{
				if(!preg_match('@^<([a-z]*)@i',$part,$m)){fatal('strange');}
				$tag=strtolower($m[1]);
				$res.=$part;
				if($tag==='br')
				{
					$posX=0;
					$posY+=1;
					if($posY>$tooLongMaxLines ){$isTooLong=true;}
					if($posY>$truncateMaxLines){$isTruncated=true;break;}
				}
				$inTextUrl=false;
				if($tag==='a' && strpos($part,'text-url')!==false){$inTextUrl=true;}
				continue;
			}

			$partWidth=dlib_html_estimate_display_width(dlib_fast_html_to_text($part));
			if($inTextUrl){$partWidth=min(250,$partWidth);}
			$splitWidth=$lineWidth*($truncateMaxLines-$posY);
			if($posX+$partWidth>$splitWidth)
			{
				if($inTextUrl){$res.=$part;}
				else
				{
					$l=max(0,intval(mb_strlen($part)*($splitWidth-$posX)/$partWidth));
					$res.=mb_substr($part,0,$l);
				}
				$isTruncated=true;
				$isTooLong=true;
				break;
			}

			$res.=$part;
			$posX+=$partWidth;
			$posY+=intval($posX/$lineWidth);
			$posX=$posX%$lineWidth;

			if($posY>$tooLongMaxLines){$isTooLong=true;}
		}

		if($isTooLong  ){$overflows='too-long' ;}
		if($isTruncated){$overflows='truncated';}

		if($isTruncated)
		{
			// truncating might have left an open tag
			$res=dlib_dom_to_html(dlib_dom_from_html($res));
			$res=preg_replace('@<p>(.*)</p>@s','$1',$res);
		}
	}

	$res=filter_xss_admin($res);
	if(isset($comments_config['body_post_render'])){$res=$comments_config['body_post_render']($res);}
	return $res;
}

//! Returns full, untruncated, rendered body of comment.
//! Called by ajax "see-more" button.
function comments_see_more(int $id): string
{
	global $comments_config;
	$comment=Comment::fetch($id,false);
	if($comment===null){dlib_not_found_404();}
	if(!$comment->canView()){dlib_permission_denied_403(false,false);}

	$res=comments_body_render($comment->body,false);
	return $res;
}

//! Returns the HTML for a list of comments. 
//! This is used to "load more" comments when the user scrolls to the bottom of the page.
function comments_list_ajax(): string
{
	global $comments_config;
	$pageType=$_GET['pageType'];
	if(!isset($comments_config['page_types'][$pageType])){dlib_bad_request_400('comments_list_ajax: invalid pageType');}
	$pageId=intval($_GET['pageId']);

	if(!comments_page_can_view($pageType,$pageId)){dlib_permission_denied_403(false,false);}

	$offset=intval($_GET['start']);
	return comments_list_render($pageType,$pageId,100,$offset,true);
}

//! Comment edit form displayed through ajax.
function comments_form(int $id=null): array
{
	global $comments_config;
	$user=$comments_config['user'];
	$isAdmin=$user['isAdmin'];

	if(!$isAdmin && val($comments_config,'disabled')){dlib_permission_denied_403();}

	require_once 'demosphere-date-time.php';

	$cancelButton='<button id="edit-cancel">x</button>';// FIXMEc: 🗙

	$justCreated=false;

	// ***** edit existing comment
	if($id!==null)
	{
		$comment=Comment::fetch($id,false);
		if($comment===null){dlib_not_found_404();}

		if(!$comment->canEdit(false,$reason))
		{
			dlib_permission_denied_403($reason==='children' ? t('Someone else has replied to this comment.') : false,false);
		}
	}
	else
	{
		// ***** create new comment
		$pageType=$_GET['pageType'];
		if(!isset($comments_config['page_types'][$pageType])){fatal('comments_form: invalid pageType');}
		$pageId=intval($_GET['pageId']);
		$settings=comments_page_settings($pageType,$pageId);
		if(!$isAdmin && $settings!=='enabled'  && $settings!=='pre-mod'){dlib_permission_denied_403(false,false);}

		$replyToId=intval($_GET['replyTo'] ?? 0);
		if($replyToId)
		{
			$replyTo=Comment::fetch($replyToId,false);
			if($replyTo===null){dlib_bad_request_400('Invalid replyTo');}
			if($pageType!==$replyTo->pageType || $pageId!==$replyTo->pageId){dlib_bad_request_400('Invalid replyTo pageType or pageId');}
			if(!$isAdmin && !$replyTo->isPublished){dlib_permission_denied_403(false,false);}
		}

		$comment=new Comment($pageType,$pageId);
		$comment->parentId=$replyToId;
		$comment->userId=$user['id'];
		$comment->isPublished=false;
		$comment->needsAttention=!$isAdmin;
		$comment->ip=$_SERVER['REMOTE_ADDR'];
		$comment->sessionN=dbsessions_get_n();
		$comment->userAgent    =$_SERVER['HTTP_USER_AGENT'] ?? '';
		$comment->userAgentHash=hash('sha256',$comment->userAgent);
		$comment->ipWhoisId=0;
		$justCreated=true;		

		if(!$isAdmin && !$comment->parentId)
		{
			$ok=comments_can_create_new_top_level($comment);
			if($ok===false)
			{
				return ['step'=>'form-fail-message',
						'html'=>$cancelButton.'<p id="form-fail-message">'.t('Please edit your existing comment instead of creating a new one.').'</p>'];
			}
			if($ok==='pre-mod')
			{
				return ['step'=>'form-fail-message',
						'html'=>$cancelButton.'<p id="form-fail-message">'.t('Please wait 1 hour between comments.').'</p>'];
			}
		}
		
		// Getting whois might be a bit long, only do it on POST
		if(count($_POST))
		{
			$whois=IpWhois::findIp($comment->ip);
			if($whois===null){$whois=IpWhois::create($comment->ip);}
			$comment->ipWhoisId=$whois===null ? 0 : $whois->id;
		}
	}

	$needsAttention0=$comment->needsAttention;

	// ***** Throttling and banning

	// * check for hard bans that block posting
	if(!$isAdmin && 
	   (count(Ban::getForComment($comment,'block')) ||
		(isset($_POST['body']) && count(Ban::getForComment(((array)$comment) + ['body'=>$_POST['body']],'block')))))
	{
		return ['step'=>'form-fail-message',
				'html'=>$cancelButton.'<p id="form-fail-message">'.t('You cannot post comments.').'</p>'];
	}

	// * throttle max number of comments someone can make in 1 & 48 hours
	if($comment->id==0 && !$isAdmin)
	{
		foreach([1,48] as $hours)
		{
			$maxNb=$comments_config['throttle_'.$hours.'h'];
			$nb=db_result("SELECT COUNT(*) FROM Comment WHERE (userId=%d OR sessionN=%d OR ip='%s') AND created>%d",
						  $comment->userId ? $comment->userId : -1,$comment->sessionN ?? -1,$comment->ip,
						  time()-3600*$hours);
			if($nb>=$maxNb)
			{
				return ['step'=>'form-fail-message',
						'html'=>$cancelButton.
						    '<p id="form-fail-message">'.
						        t('Sorry, you cannot make more than @n comments in @h hours.',['@n'=>$maxNb,'@h'=>$hours]).
						    '</p>'];
			}
		}
	}

	// * Add a 24h pre-mod ban if an anon user begins writing a comment immediately after creating a new session.
	// This is probably someone trying to avoid a ban. 
	// FIXME: auto-too-fast-new-session: too many false positives
	//if($comment->id==0 && !$comment->userId && $comment->sessionN && count($_POST)===0)
	//{
	// 	$firstAccess=db_result_check("SELECT first_access FROM dbsessions WHERE n=%d",$comment->sessionN);
	// 	if($firstAccess!==false && time()<$firstAccess+45) 
	// 	{
	// 		$ban=new Ban();
	// 		$ban->banType ='sessionN';
	// 		$ban->value=$comment->sessionN;
	// 		$ban->end  =time()+3600*24;
	// 		$ban->label='@auto-too-fast-new-session';
	// 		$ban->save();
	// 	}
	//}

	// ***** Create form items from comment object.

	require_once 'dlib/form.php';
	list($itemsAll,$options)=form_dbobject($comment);
	$items=[];

	// only use selected form items
	$items['form_object']=$itemsAll['form_object'];
	$items['messages']=['html'=>''];
	$items['body']=$itemsAll['body'];
	$items['body']['required']=true;
	unset($items['body']['title']);
	$items['body']['type']='textarea';
	$items['body']['attributes']=['placeholder'=>t("Write your comment here."),"rows"=>false];
	$items['body']['pre-submit-1']=function(&$v){$v=trim($v);};
	$items['body']['custom-templates']=form_item_templates();
	$items['body']['custom-templates'][0]='$template'; // Remove wrapper that interferes with flexbox grow on some mobile browsers
	$items['bottom-part-start']=['html'=>'<div id="bottom-part">'];
	if($isAdmin)
	{
		$items['isPublished']=$itemsAll['isPublished'];
		$items['isPublished']['title']=t('published');
		$items['needsAttention']=$itemsAll['needsAttention'];
		$items['needsAttention']['title']=t('needs attention');
	}

	$items['save']=$itemsAll['save'];
	// This is called from form_process()
	$items['save']['pre-dbobject-save']=function($comment)use($isAdmin)
		{
			if(!$comment->id)
			{
				// Apply bans
				$preModReasons=[];
				foreach(Ban::getForComment($comment,'pre-mod') as $ban)
				{
					$preModReasons[]='ban-'.$ban->id.':'.$ban->banType.'('.$ban->value.'):'.$ban->label.'#'.$ban->labelValue.' ['.
						dlib_format_date('shorter-date-time',$ban->start).' - '.
						dlib_format_date('shorter-date-time',$ban->end).']';
				}

				if(mb_strlen(trim($comment->body))<5){$preModReasons[]='too short';}

				$comment->isPublished=$isAdmin || count($preModReasons)===0;
				if(!$comment->isPublished){$comment->extraData['pre-mod-reasons']=$preModReasons;}				
				
				if(isset($_POST['keyCount']))
				{
					$comment->extraData['keyCount']=['keys'   =>intval($_POST['keyCount']),
													 'letters'=>mb_strlen($comment->body),
													 'words'  =>count(preg_split('@[^\p{L}\p{N}\']+@u',$comment->body)),
													];
				}

			}

			// Lower case comments with too many capital letters
			if(!$isAdmin)
			{
				$nCaps =mb_strlen(preg_replace("@[^\p{Lu}]@u","",$comment->body));
				$nLower=mb_strlen(preg_replace("@[^\p{Ll}]@u","",$comment->body));
				$nLetters=$nCaps+$nLower;
				if($nLetters>10 && $nCaps>.3*$nLetters){$comment->body=mb_strtolower($comment->body);}
			}

			if(!$isAdmin){$comment->needsAttention=true;}
		};

	$items['guidelines']=
		['html'=>'<a class="comment-guidelines" href="'.ent($comments_config['guidelines_url']()).'">'.t('Publishing guidelines').'</a>'];

	if($isAdmin && isset($itemsAll['delete']) && $comment->nbChildren()===0)
	{
		$items['delete']=$itemsAll['delete'];
		$items['delete']['value']='🗑 '.$items['delete']['value'];
	}

	$items['cancel']=['type'=>'submit',
					  'value'=>'x']; // FIXMEc: 🗙

	$items['bottom-part-end']=['html'=>'</div>'];

	// Show the validation error messages in top of form, instead of using standard top of page messages
	// Note: no validation errors are currently generated. This might be usefull in the future.
	// This is called from form_process()
	$options['validation-messages']=function($msgs,&$items)
		{
			// Remove ugly "body: this field is required" message, as it is obvious and the red rectangle is enough
			foreach($msgs as $k=>$msg)
			{
				if($items['body']['value']==='' && strpos($msg,'body:')===0){unset($msgs[$k]);break;}
			}
			if(!count($msgs)){return;}
			$mout='<ul class="validation-messages">';
			foreach($msgs as $msg){$mout.='<li>'.$msg.'</li>';}
			$mout.='</ul>';
			$items['messages']['html']=$mout;
		};
	$options['redirect']=false;
	if($id!==null){$options['attributes']['data-id']=$id;}

	if($comments_config['form_alter']){$comments_config['form_alter']($comment,$items,$options);}

	// ***** show / submit
	// Note $comment is saved/deleted in form_process()
	$out=form_process($items,$options,$step);
	if($step==='skip'          ){$step='delete';}
	if($step==='ok-no-redirect'){$step='save';}

	if($step==='save')
	{
		// This avoids automatic cleanup of empty sessions and avoids adding comment related logic in dbsessions.php
		$_SESSION['hasComments']=true;

		// Send notifications
		if(!$isAdmin && $comment->needsAttention && (!$needsAttention0 || $justCreated) )
		{
			require_once 'comments-notifications.php';
			comments_notifications_send_all($comment);			
		}

		$message='';
		if(!$isAdmin && !$comment->isPublished)
		{
			$message=t('Thanks for writing this comment. We will publish it as soon as possible.');
		}

		// return the new comment's display so that js can show it
		return ['step'=>$step,
				'html'=>comments_render_single($comment,compact('isAdmin','message'))];
	};
	return ['step'=>$step,'html'=>$out];
}


//! Returns whether user can create a new comment that is not a reply.
//! He cannot create such a comment if he has made a top level comment in the past hour that he can still edit.
function comments_can_create_new_top_level($comment)
{
	$recent=Comment::fetchList("WHERE pageType='%s' AND pageId=%d AND ".
							   "(userId=%d OR sessionN=%d OR ip='%s') AND created>%d AND parentId=0",
							   $comment->pageType,$comment->pageId,$comment->userId,$comment->sessionN ?? -1,$comment->ip,time()-3600);
	$isPremod=false;
	foreach($recent as $comment)
	{
		if($comment->canEdit(false,$reason)){return false;}
		if($reason==='pre-mod'){$isPremod=true;}
	}
	// pre-mod banned users cannot edit existing comments ... so just add 1h delay.
	if($isPremod){return 'pre-mod';}

	return true;
}

//! Display a list of recent comments for moderators.
//! This is both the main moderation interface and an interface for a specific filter (userId, sessionN ...).
function comments_manager(): string
{
	global $comments_config,$currentPage;
	$user=$comments_config['user'];
	if(!$user['isAdmin']){dlib_permission_denied_403();}
	
	comments_js_translations();

	$filterField=$_GET['filterField'] ?? false;
	$filterValue=$_GET['filterValue'] ?? false;

	if($filterField!==false && array_search($filterField,['userId','sessionN','ip','userAgentHash'])===false){dlib_bad_request_400('invalid filterField');}
	$bans=[];

	if($filterField!==false)
	{
		$bans=Ban::getForComment([$filterField=>$filterValue],false,false);
		$banIds=dlib_object_column($bans,'id');
		// FIXME: consider using dbobject_ui 
		//$banList='';
		//if(count($bans))
		//{
		// 	require_once 'dlib/dbobject-ui.php';
		// 	global $dbobject_ui_config;
		// 	$ciOverride=[
		// 		'list_top_text'=>'',
		// 		'list_where'=>'WHERE id IN ('.implode(',',$banIds).')','list_noSort'=>true,'list_noRotate'=>true,
		// 		'list_orderSql'=>'ORDER BY end DESC, id DESC'];
		// 	$banList=dbobject_ui_list('Ban',$ciOverride);
		//}
	}
	$firstAccess=false;
	if($filterField==='sessionN') 
	{
		$firstAccess=db_result_check("SELECT first_access FROM dbsessions WHERE n=%d",$filterValue);
	}
	$reverse=false;
	$whois=false;
	$whoisIps=false;
	if($filterField==='ip')
	{
		$reverse=gethostbyaddr($filterValue);
		$whois=IpWhois::findIp($filterValue);
		$whoisIps=db_one_col('SELECT ip,COUNT(*) FROM Comment WHERE ipWhoisId=%d GROUP BY ip ORDER BY COUNT(*) DESC LIMIT 1000',$whois->id);
		unset($whoisIps[$filterValue]);
	}
	$userAgent=false;
	if($filterField==='userAgentHash')
	{
		$userAgent=db_result_check("SELECT userAgent FROM Comment WHERE userAgentHash='%s' ORDER BY id DESC LIMIT 1",$filterValue);
	}

	$remarkForm=false;
	if($filterField!==false)
	{
		$remark=variable_get($filterValue,'comments-remarks-'.$filterField,[]);
		$items=[];
		$items['name']=['type'=>'textfield',
						'default-value'=>$remark['name'] ?? '',
						'title'=>t('name'),
						'attributes'=>['maxlength'=>15],
					   ];
		$items['remark']=['type'=>'textfield',
						  'default-value'=>$remark['remark'] ?? '',
						  'title'=>t('remark'),
						 ];
		$items['save']=['type'=>'submit',
						'value'=>t('save'),
						'submit'=>function($items)use($filterField,$filterValue)
			{
				$remark=['name'  =>$items['name'  ]['value'],
						 'remark'=>$items['remark']['value'],];
				variable_set($filterValue,'comments-remarks-'.$filterField,$remark);
			}
				 ];
		require_once 'dlib/form.php';
		$remarkForm=form_process($items,['id'=>'remarkForm']);
	}


	$pager=new Pager(['itemName'=>t('comments'),'defaultItemsPerPage'=>100]);
	$comments=Comment::fetchList('SELECT SQL_CALC_FOUND_ROWS * FROM Comment '.
								 ($filterField!==false ? "WHERE ".$filterField."='".db_escape_string($filterValue)."' " : '').
								 'ORDER BY needsAttention DESC,created DESC '.$pager->sql());
	$pager->foundRows();

	$statsCache=comments_list_stats_cache($comments);

	$currentPage->addCss('dlib/comments/comments.css');
	$currentPage->addCss('dlib/comments/comments-manager.css');
	$currentPage->addJs('lib/jquery.js');
	$currentPage->addJs('dlib/comments/comments.js');

	$stats=[];
	if($filterField!==false)
	{
		$stats=
			[
				'@nn'=>db_result('SELECT COUNT(*) FROM Comment WHERE '.$filterField."='%s' AND needsAttention=1"                  ,$filterValue),
				'@np'=>db_result('SELECT COUNT(*) FROM Comment WHERE '.$filterField."='%s' AND needsAttention=0 AND isPublished=1",$filterValue),
				'@nr'=>db_result('SELECT COUNT(*) FROM Comment WHERE '.$filterField."='%s' AND needsAttention=0 AND isPublished=0",$filterValue),
			];
	}

	return template_render('comments-manager.tpl.php',
		   [compact('bans','comments','pager','comments_config','filterField','filterValue','stats','statsCache','userAgent','reverse','whois','whoisIps','firstAccess','remarkForm')]);
}

function comments_field_or_remark_name($field,$value)
{
	$remark=variable_get($value,'comments-remarks-'.$field);
	return $remark===false || $remark['name']==='' ? $value : $remark['name'];
}

//! Used by dbobject_ui interface
function comments_ban_class_info(&$classInfo)
{
	$classInfo['edit_form_alter']=function(&$items,$options,$object)
		{
			$items['banType']['type']='select';
			$items['banType']['options']=array_combine(Ban::$banTypes,Ban::$banTypes);
			
			$items['action']['type']='select';
			$items['action']['options']=array_combine(Ban::$actions,Ban::$actions);
		};
}

function comments_ipwhois(int $id)
{
	global $comments_config;
	$user=$comments_config['user'];
	$isAdmin=$user['isAdmin'];
	if(!$isAdmin){dlib_permission_denied_403(false,false);}
	
	$whois=IpWhois::fetch($id,false);
	if($whois===null){dlib_not_found_404();}

	return '<pre>'.ent($whois->entry).'</pre>';
}

//! Response to ajax call when moderation buttons ('ok', 'unpublish', 'ban' ...) are pressed.
function comments_moderation_button(int $id): array
{
	global $comments_config;
	$user=$comments_config['user'];
	$isAdmin=$user['isAdmin'];
	if(!$isAdmin){dlib_permission_denied_403(false,false);}

	$res=[];

	$comment=Comment::fetch($id,false);
	if($comment===null){dlib_not_found_404();}

	$name=$_POST['name'] ?? '';
	switch($name)
	{
	case 'ok':
		$comment->isPublished=true;
		break;
	case 'unpublish':
		$comment->isPublished=false;
		break;
	case 'ban':
		// Algorithm: when user presses the ban button, we estimate the current "level" of ban, and 
		// then increase bans for all fieds to the next level.

		// BanLevels: successive stops defining how long bans last for each field.
		// In the future, this might be more subtle, by trying to determine if a UA or IP reliably identies somebody.
		$max=1000*3600*24*365;
		$banLevels=['userId'        =>[0,2*3600*24,2*3600*24*30,      $max],
					'sessionN'      =>[0,2*3600*24,2*3600*24*30,      $max],
					'ip'            =>[0,2*3600*24,4*3600*24   ,10*3600*24],
					'userAgentHash' =>[0,2*3600   ,1*3600*24   , 4*3600*24],
				   ];

		$now=time();

		// First determine the unique Ban that affects each each field and estimate it's level
		$bans=[];
		foreach(array_keys($banLevels) as $field)
		{
			if($field==='userId'        && $comment->userId===0        ){continue;}
			if($field==='sessionN'      && !$comment->sessionN         ){continue;}
			if($field==='userAgentHash' && $comment->userAgentHash===''){continue;}

			$ban=Ban::unique($field,$comment->$field,['@mod-button','@auto-too-fast-new-session']);
			$bans[$field]['ban'  ]=$ban;
			$bans[$field]['level']=0;
			if($ban!==null)
			{
				$dEnd=$ban->end-$now;
				$bans[$field]['level']=dlib_array_best($banLevels[$field],function($delay)use($dEnd){return abs($delay-$dEnd);});
			}
		}

		// The current level is the min level of all field's levels
		$levels=dlib_array_column($bans,'level');
		$minLevel=count($levels)>0 ? min($levels) : 0;
		$newLevel=min($minLevel+1,count($banLevels['userId'])-1);

		// Now increase (or create) bans to the new level
		foreach($banLevels as $field=>$levels)
		{
			if(!isset($bans[$field])){continue;}
			$ban=$bans[$field]['ban'];
			if($ban===null)
			{
				$ban=new Ban();
				$ban->banType=$field;
				$ban->value=$comment->$field;
				$ban->end=0;
			}
			$newEnd=$levels[$newLevel]!==$max ? $now+$levels[$newLevel] : Ban::permanent();
			if($newEnd>$ban->end)
			{
				$ban->end=$newEnd;
				$ban->label='@mod-button';
				$ban->labelValue=$comment->id;
				$ban->save();
			}
		}
		break;
	case 'lengthen':
	case 'shorten':
		$delays=[3600*24*1,3600*24*10,3600*24*30*1,3600*24*30*2,3600*24*365,3600*24*365*1000];
		$now=time();

		$field=$_POST['field'] ?? false;
		if(array_search($field,['userId','sessionN','ip','userAgentHash'])===false){dlib_bad_request_400();}

		$ban=Ban::unique($field,$comment->$field,['@mod-button','@auto-too-fast-new-session']);
		if($ban===null)
		{
			if($name!=='lengthen'){break;}
			$ban=new Ban();
			$ban->banType =$field;
			$ban->value=$comment->$field;
			$ban->end  =$now;
			$ban->label='@mod-button';
			$ban->labelValue=$comment->id;
			$ban->save();
		}

		$dEnd=$ban->end-$now;
		$delayIdx=dlib_array_best($delays,function($delay)use($dEnd){return abs($delay-$dEnd);});
		if($name==='lengthen' && $dEnd<$delays[0]/2){$delayIdx=-1;}
		
		if($name==='lengthen')
		{
			$delayIdx++;
			if($delayIdx < count($delays)-1){$ban->end=$now+$delays[$delayIdx];}
			else                            {$ban->end=Ban::permanent();}
		}
		if($name==='shorten' )
		{
			$delayIdx--;
			if($delayIdx>=0){$ban->end=$now+$delays[$delayIdx];}
			else            {$ban->delete();$ban=null;}
		}
		if($ban!==null){$ban->save();}
		break;
	case 'unban':
		// Remove bans made by a mod button or "too fast".
		foreach(Ban::getForComment($comment,false,false) as $ban)
		{
			if($ban->label==='@mod-button' ||
			   $ban->label==='@auto-too-fast-new-session'){$ban->delete();}
		}
		break;
	default: dlib_bad_request_400();
	}
	$comment->needsAttention=false;
	$comment->save();

	$res['comment']=comments_render_single($comment);
	return $res;
}


//! Application is responsible for calling this 
function comments_cron()
{
	// Remove stale references to deleted session. This should be quite rare. 
	db_query('UPDATE Comment SET sessionN=0 WHERE NOT EXISTS (SELECT * FROM dbsessions WHERE n=sessionN)');
	// FIXMEc : Cleanup old bans '@auto-too-fast-new-session'. Is this really a good idea ? Wait and see.
	//db_query("DELETE FROM Ban WHERE label='@auto-too-fast-new-session' AND end<%d",time()-3600*24*30);

	// FIXMEc: need cron to update / cleanup whois information
}


//! Parses a user-agent string and returns a human readable version.
function comments_user_agent_format(string $userAgent=null,bool $showVersion=true): string
{
	require_once "lib/UserAgentParser.php";
	$parsedUserAgent=parse_user_agent($userAgent);
	if($showVersion)
	{
		return t('@browser @version on @platform',['@browser' =>$parsedUserAgent['browser' ] ?? '?',
												   '@version' =>$parsedUserAgent['version' ] ?? '?',
												   '@platform'=>$parsedUserAgent['platform'] ?? '?',]);
	}
	else
	{
		return t('@browser on @platform',['@browser' =>$parsedUserAgent['browser' ] ?? '?',
										  '@platform'=>$parsedUserAgent['platform'] ?? '?',]);
	}
}

/** @} */

?>