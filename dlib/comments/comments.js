(function()
{
window.dlib_comments = window.dlib_comments || {};
var ns=dlib_comments;

$(document).ready(function()
{
	var comments=$('#comments');
	// There are two urls: add: comment/form ; edit: comment/1234/edit
	var commentFormUrl=false;
	var formBodyInitialVal=false;
	var keyCount=0;

	$('head').append($('<link type="text/css" rel="stylesheet"/>').attr('href',base_url+'/dlib/form.css'));

	// **** click on change-page-settings select
	$('#comments-page-settings').on('change',function(e)
	{
		$.post(base_url+'/comment/page-settings',
		{
			'dlib-form-token-comments-page-settings': $(this).parent().find('.dlib-form-token').val(),
			'page-settings': $('#comments-page-settings').val(),
			pageType: $('#comments').attr('data-pageType'),
			pageId:   $('#comments').attr('data-pageId')
		},
	    function(data)
		{
			if(data!=='page-settings-ok'){alert("Page-Settings failed ?");return;}
			$('#comments').attr('class',$('#comments').attr('class').replace(/page-settings-[a-z0-9-]+/,''));
			$('#comments').addClass('page-settings-'+$('#comments-page-settings').val());
		})
		.fail(function(xhr, status, error)
		{
			show_error(xhr.responseText);
		});
    });

	// **** click on add/reply/edit buttons
	comments.on('click','.comment-add,.comment-reply,.comment-edit',function(e)
	{
		e.preventDefault();
		let button=$(this);
		if($('#comment-form').length){alert('Strange: attempt to create form, already exists');return;}
		let comment=button.parents('.comment');
		let id=parseInt(comment.attr('data-id'));
		let form=$('<div id="comment-form"/>');
		let initialHeight=0;
		keyCount=0;

		$('.comment-add,.comment-reply,.comment-edit').prop("disabled",true);

		if(button.is('.comment-edit'))
		{
			commentFormUrl=base_url+'/comment/'+id+'/edit';
			initialHeight=comment.outerHeight();
			let marginTop   =comment.css('margin-top');
			let marginBottom=comment.css('margin-bottom');
			// NASTY Firefox BUG :-(
			// If we hide the comment while the user is clicking the button then (sometimes) the mouse pointer becomes offset.
			// This took a long time to find. 
			// Steps to reproduce bug:
			// - Create lots of comments on a page.
			// - Go to bottom of page, and start erasing comments one by one (from the bottom).
			// Generally the second one fails. Sometimes the third. Rarely, it fails on the first time.
			// Comment out confirm popup to go faster.
			//comment.hide()
			setTimeout(()=>comment.hide(),0);
			
			comment.addClass('editing');
			comment.after(form);
			// Make height of form exactly that of comment it has (visually) replaced
			if(!comment.hasClass('has-see-more'))
			{
				form.outerHeight(initialHeight);
			}
			form.css('margin-top'   ,marginTop);
			form.css('margin-bottom',marginBottom);
		}
		else
		{
			commentFormUrl=base_url+'/comment/form?'+
				'pageType='+comments.attr('data-pageType')+'&'+
				'pageId='  +comments.attr('data-pageId')
		}

		if(button.is('.comment-add'))
		{
			$('#comments-list').prepend(form);
		}

		if(button.is('.comment-reply'))
		{
			commentFormUrl+='&'+'replyTo='+id;
			let children=$('[data-parent-id='+id+']');
			let pos=children.length ? children.last() : comment;
			pos.after(form);
		}

		// Actually fetch the form and display it
		$.get(commentFormUrl,function(response)
		{
			if(response.step!=='display' && response.step!=='form-fail-message'){alert('invalid form.step');return;}

			form.html(response.html);

			if(response.step==='form-fail-message'){form.addClass('form-has-fail-message');}

			$('#edit-body').keyup(()=>keyCount++);

			// Adjust height when editing large texts
			if(button.is('.comment-edit') && comment.hasClass('has-see-more'))
			{
				form.outerHeight($('#edit-body')[0].scrollHeight+$('#bottom-part').outerHeight()+20);
			}

			$('#edit-body').focus()

			// Scroll into view with animation and only as needed.
			// Problem on mobile:  mobile keyboards already do this and sometimes it can break it.
			// Exception: iPhone does not respect .focus(). So it is *really* needed. 
			// Especially when replying to a comment where textarea can appear very far away from button.
			let rect=form[0].getBoundingClientRect();
			let destScrollTop;
			if(rect.top<0)
			{
				destScrollTop=$(document).scrollTop()+rect.top;
			}
			else
			if(rect.bottom>$(window).height())
			{
				destScrollTop=$(document).scrollTop()+rect.bottom-$(window).height();
			}
			$("html, body").animate({ scrollTop: destScrollTop });

			formBodyInitialVal=$('#edit-body').val();
		})
		.fail(function(xhr, status, error)
		{
			show_error(xhr.responseText);
			$('.comment-add,.comment-reply,.comment-edit').prop("disabled",false);
			form.remove();
			comment.show();
		});
	});

	// Resize textarea when user types in it
	comments.on('input','#edit-body',function() 
	{
		// height= inside element height; innerHeight= height+padding; outerHeight= height+padding+border
		// scrollHeight is based on innerHeight
		let form=$(this).parents('#comment-form');
		let textarea=$(this);
		if(textarea[0].scrollHeight>textarea.innerHeight())
		{
			form.outerHeight(form.outerHeight()+textarea[0].scrollHeight-textarea.innerHeight()+5);
		}
	});

	// **** click on save button
	comments.on('click','#edit-save',function(e)
	{
		var form=$('#comment-form');
		form.find('button,input,textarea').prop("disabled",true);
		form.addClass('pre-waiting');
		setTimeout(function(){if(form.hasClass('pre-waiting')){form.addClass('waiting');}},300);
		let vals=
			{
				body: $('#edit-body' ).val(),
				save: true,
				keyCount,
				'form-id': $('#Comment input[name=form-id]').val(),
				'dlib-form-token-Comment': $('input[name=dlib-form-token-Comment]').val()
			};
		if($('#edit-isPublished'   ).is(':checked')){vals.isPublished   ='on';}
		if($('#edit-needsAttention').is(':checked')){vals.needsAttention='on';}
		$.post(commentFormUrl,vals,
			   function(response)
			   {
				   switch(response.step)
				   {
					case 'validation-error':
					   form.find('button,input,textarea').prop("disabled",false);
					   form.removeClass('waiting pre-waiting');
					   form.html(response.html);
					   form.outerHeight($('.validation-messages').outerHeight(true)+$('#edit-body')[0].scrollHeight+$('#bottom-part').outerHeight()+20);
					   break;
				   case 'save':
					   $('#comment-form'    ).after(response.html);
					   var newComment=$('#comment-form+.comment');
					   $('#comment-form'    ).remove();
					   $('.comment-add,.comment-reply,.comment-edit').prop("disabled",false);
					   $('.comment.editing' ).remove();
					   flash(newComment.find('.comment-body'));
					   comments.toggleClass('comments-empty',comments.find('.comment').not('.not-published').length===0);
					   update_hash();
					   break;
				   default:
					   alert('unexpected response');
				   }
			   })
			.fail(function(xhr, status, error)
			{
				form.find('button,input,textarea').prop("disabled",false);
				form.removeClass('waiting pre-waiting');
				show_error(xhr.responseText);
			});
	});

	// **** click on cancel button
	comments.on('click','#edit-cancel',
				function(e)
				{
					if($('#edit-body' ).val()!==formBodyInitialVal)
					{
						if(!confirm(t('confirm-cancel'))){return;}
					}
					$('.comment.editing').show();
					$('.comment.editing').removeClass('editing');
					// setTimeout needed: Firefox bug (see above)
					setTimeout(()=>$('#comment-form').remove(),0);
					$('.comment-add,.comment-reply,.comment-edit').prop("disabled",false);
				});

	// **** click on delete button
	comments.on('click','#edit-delete',function(e)
	{
		e.preventDefault();
		if(!$('#comments').hasClass('admin') && !confirm(t('confirm-delete'      ))){return;}
		if( $('#comments').hasClass('admin') && !confirm(t('confirm-delete-admin'))){return;}

		$.post(commentFormUrl,
			   {
				   'form-id':    $('#Comment input[name=form-id]').val(),
				   'dlib-form-token-Comment': $('input[name=dlib-form-token-Comment]').val(),
				   'delete': true
			   },
			   function(response)
			   {
				   if(response.step!=='delete'){alert("Delete failed ?");return;}

				   $('.comment.editing').prev('.comment-info').remove();
				   $('.comment.editing').remove();
				   $('#comment-form').remove();
				   $('.comment-add,.comment-reply,.comment-edit').prop("disabled",false);
			   })
			   .fail(function(xhr, status, error)
			   {
				   show_error(xhr.responseText);
			   });
	});

	comments.on('submit','form',e => e.preventDefault())

	// **** click on moderation buttons or ban popup buttons
	comments.on('click','.mod-buttons button,.ban-popup button',function()
	{
		let name=$(this).attr('name');
		let comment=$(this).parents('.comment');
		let id=parseInt(comment.attr('data-id'));
		let field=false;
		if(name==='shorten' || name=='lengthen'){field=$(this).parents('.stats-block').attr('data-field');}
		if(name==='ban' && !comment.hasClass('comment-show-popups'))
		{
			ban_popups_show(comment);
			clearTimeout(comment.data('ban-timeout'));
			return;
		}

		$.post(base_url+'/comment/'+id+'/moderation-button',{name,field},
			   function(response)
			   {
				   comment.replaceWith(response.comment);
				   update_hash();
				   comment=$('.comment[data-id='+id+']');
				   if(['unban','ban','shorten','lengthen'].indexOf(name)!==-1){ban_popups_show(comment);}
			   })
			.fail(function(xhr, status, error)
				  {
					  show_error(xhr.responseText);
				  });
	});

	// **** see-more button to expand long text
	comments.on('click','.see-more',function()
	{
		let comment=$(this).parents('.comment');
		let id=parseInt(comment.attr('data-id'));
		if(comment.hasClass('is-truncated'))
		{
			comment.find('.comment-body').load(base_url+'/comment/'+id+'/see-more',()=>update_hash());
			comment.removeClass('has-see-more is-truncated');
		}
		else
		{
			comment.removeClass('has-see-more');
		}
	});

	// **** step2 initialisation, if we were called
	// from comments-minimal.js we need to handle the event that triggered it.
	switch(ns.action)
	{
		case 'button':
		case 'see-more':
		$(ns.element).trigger('click');
 		break;
		case 'page-settings':
		$(ns.element).trigger('change');
 		break;
		case 'scroll':
		load_more();
 		break;
		case 'hash':
		load_more_until(ns.element).then((c)=>c.addClass('highlighted'));
 		break;
	}
	// Re-implemented onhashchange that was in comments-minimal.js but no longer works since setup() is disabled
	onhashchange=function()
	{
		$('.highlighted').removeClass('highlighted');
		var m=window.location.hash.match(/^#(comment-[0-9]+)$/);
		if(m){load_more_until(m[1]).then((c)=>c.addClass('highlighted'));} 
	};

});


//! Display and position ban popups
function ban_popups_show(comment)
{
	comment.addClass('comment-show-popups');
	comment.find('.stats-block').each(function()
	{
		var statsBlock=$(this);
		var statsLink=statsBlock.find('.stats-link');

		var popupTop=statsBlock.find('.ban-popup-top');
		var pos=statsLink.offset();
		pos.left+=(statsLink.outerWidth()-popupTop.outerWidth())/2;
		pos.top -=popupTop.outerHeight();
		popupTop.offset(pos);

		var popupBot=statsBlock.find('.ban-popup-bottom');
		var pos=statsLink.offset();
		pos.left+=(statsLink.outerWidth()-popupBot.outerWidth())/2;
		pos.top +=statsLink.outerHeight();
		popupBot.offset(pos);
	});
	// Fix overlap (especially on mobile). 
	var overlap=false;
	comment.find('.ban-popup-top,.ban-popup-bottom').each(function()
	{
		var el1=this;
		comment.find('.ban-popup-top,.ban-popup-bottom').each(function()
		{
			var el2=this;
			if(el1===el2){return;}
			if(check_overlap(el1,el2)){overlap=true;}
		});
	});
	if(overlap)
	{
		// If there is some overlap, just move all popups to left, middle and right of window.
		// This is ugly, but at least it can be used.
		comment.find('.stats-block').each(function(i)
		{
			var statsBlock=$(this);
			var statsLink=statsBlock.find('.stats-link');
			var win=$(window).width();

			var popupTop=statsBlock.find('.ban-popup-top');
			var pos=popupTop.offset();
			if(i===0){pos.left=       0                      ;}
			if(i===1){pos.left=win/2- popupTop.outerWidth()/2;}
			if(i===2){pos.left=win  - popupTop.outerWidth()  ;}
			popupTop.offset(pos);

			var popupBot=statsBlock.find('.ban-popup-bottom');
			var pos=popupBot.offset();
			if(i===0){pos.left=       0                      ;}
			if(i===1){pos.left=win/2- popupBot.outerWidth()/2;}
			if(i===2){pos.left=win  - popupBot.outerWidth()  ;}
			popupBot.offset(pos);
		});
	}

	ban_popups_delayed_hide(comment);
	comment.find('.ban-popup,.mod-buttons button[name="ban"],.mod-buttons button[name="unban"]').hover(
		()=>{if(comment.data('ban-timeout')){clearTimeout(comment.data('ban-timeout'));}},
		()=>ban_popups_delayed_hide(comment));
}

function ban_popups_delayed_hide(comment)
{
	var timeout=comment.data('ban-timeout');
	if(timeout){clearTimeout(timeout);}
	timeout=setTimeout(function()
	{
		comment.removeClass('comment-show-popups');
	},2000);
	comment.data('ban-timeout',timeout);
}

//! Use CSS3 transitions to temporarily flash a fading yellow background color on an element
function flash(el)
{
	el.css('background-color','yellow');
	requestAnimationFrame(()=>requestAnimationFrame(()=>
	{
		el.on('transitionend',()=>el.css('transition',''));
		el.css('transition','background-color 700ms ease');
		el.css('background-color','');
	}));
}

//! FIXMEc: do something prettier.
function show_error(message)
{
	alert('Error: '+safe_html_to_text(message));
}

//! Simple, hackish, but safe.  FIXMEc: remove this.
function safe_html_to_text(html)
{
	var res=html;
	res=res.replace(/<(script|style)\b[\s\S]*?<\/\1>/g, '');
	res=res.replace(/<[^>]*>?/gm, '');
	return res;
}

//! The number of comments initially displayed is limited to avoid heavy pages.
//! This function loads more comments. 
//! It is called from the scroll event listener in comments-minimal.js
function load_more()
{
	let more=$('.more-comments');
	if(!more.length){return Promise.resolve();}
	more.addClass('pre-wait-more-comments');
	more.removeClass('more-comments'); // avoid scroll calls during get
	setTimeout(()=>more.addClass('waiting-more-comments'),200);

	let comments=$('#comments');
	return $.get(base_url+'/comment/list?'+
				 'pageType='+comments.attr('data-pageType')+'&'+
				 'pageId='  +comments.attr('data-pageId'  )+'&'+
				 'start='   +more.attr('data-offset'),
				 (html)=>more.replaceWith(html));
}

//! Load more comments until comment given by id is visible.
function load_more_until(id)
{
	let failsafe=20;
	// Promise executor. We call it recursively.
	let ex=function(resolve, reject)
	{
		if(failsafe--<=0){reject('too many comment list loads, aborting');return;}

		let comment=$('#'+id);
		if(comment.length)
		{
			comment[0].scrollIntoView();
			resolve(comment);
			return;
		}

		if($('.more-comments').length)
		{
			load_more().then(function(){ex(resolve,reject);});
		}
		else{reject('comment not found: '+id);}
	};
	
	return new Promise(ex);
}

function update_hash()
{
	$('.comment.highlighted').removeClass('highlighted');
	var m=window.location.hash.match(/^#(comment-[0-9]+)$/);
	if(m){$('#'+m[1]).addClass('highlighted');}
}


function check_overlap(el1,el2)
{
	var box1=el1.getBoundingClientRect();
	var box2=el2.getBoundingClientRect();
	var res=!(box1.right <box2.left || box1.left>box2.right  || 
			  box1.bottom<box2.top  || box1.top >box2.bottom   );
	return res;
}

// Exports
ns.load_more=load_more;

}());
