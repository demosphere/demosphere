<aside id="comments" 
	 class="<?: $commentsList==='' ? 'comments-empty' :'' ?> page-type-$pageType pageid-$pageId page-settings-$pageSettings <?: $isAdmin ? 'admin' : '' ?>" 
	 data-pageType="$pageType" 
	 data-pageId="$pageId">
	<header id="comments-top">
		<div id="comments-actions">
			<? if($isAdmin){ ?>
				<a href="$base_url/comment/manager">_(Manager)</a>
				<form id="page-settings-form">
					<?= dlib_add_form_token('comments-page-settings') ?>
					<select id="comments-page-settings">
						<option value="enabled"  <?= $pageSettings=='enabled'  ? 'selected="selected"' : '' ?> >_(Enabled)       </option>
						<option value="pre-mod"  <?= $pageSettings=='pre-mod'  ? 'selected="selected"' : '' ?> >_(Pre-moderation)</option>
						<option value="readonly" <?= $pageSettings=='readonly' ? 'selected="selected"' : '' ?> >_(Read-only)     </option>
						<option value="disabled" <?= $pageSettings=='disabled' ? 'selected="selected"' : '' ?> >_(Disabled)      </option>
					</select>
				</form>
			<? } ?>
			<button id="comment-add" class="comment-add"><span></span>_(Add comment)</button>
		</div>
	    <div id="comments-top-text" class="<?= $nbComments===0 ? 'is-empty' : '' ?>">
			<? if($nbComments){ ?>
				<?if(isset($options['topText'])){?>$options['topText']<?}else{?>
				_(Extra information and comments added by site visitors)
			<?}}else{?>
				<?if(isset($options['topTextEmpty'])){?>$options['topTextEmpty']<?}else{?>
				_(Be the first to comment)
			<?}}?>
		</div>
	</header>
	<div id="comments-list" class="threaded">
		<?= $commentsList ?>
	</div>
</aside>