<?php

function test_dlib($short=false)
{
	global $test_fail_count;
	global $test_ok_count;
	require_once "dlib/test/test-tools.php";
	error_reporting(E_STRICT | E_ALL);
	test_dlib_all($short);
	echo "======================\n";
	echo "TOTAL test_dlib: failed:$test_fail_count  OK:$test_ok_count\n";
	return $test_fail_count===0;
}

function test_assert($val)
{
	test_result((bool)$val);
}

function test_equals($val1,$val2)
{
	$ok=($val1===$val2);
	if(!$ok){echo "test_equals failed: \n";vd($val1);vd($val2);}
	test_result($ok);
}

function test_different($val1,$val2)
{
	$ok=($val1!==$val2);
	if(!$ok){echo "test_different failed:\n";vd($val1);vd($val2);}
	test_result($ok);
}

function test_contains($haystack,$needle)
{
	$ok=(mb_strpos($haystack,$needle)!==false);
	if(!$ok){echo "test_contains failed: \n";echo "haystack:";vd($haystack);echo "needle:";vd($needle);}
	test_result($ok);
}

function test_not_contains($haystack,$needle)
{
	$ok=(mb_strpos($haystack,$needle)===false);
	if(!$ok){echo "test_not_contains failed: \n";echo "haystack:";vd($haystack);echo "needle:";vd($needle);}
	test_result($ok);
}

function test_matches($val,$preg)
{
	$ok=(bool)preg_match($preg,$val);
	if(!$ok){echo "test_matches failed: \n";vd($val);vd($preg);}
	test_result($ok);
}

function test_result($val)
{
	global $test_fail_count;
	global $test_ok_count;
	if(!isset($test_fail_count)){$test_fail_count=0;}
	if(!isset($test_ok_count  )){$test_ok_count  =0;}
	$stack=debug_backtrace();
	$location=$stack[2]['function'].':'.$stack[1]['line'];

	if($val===true){echo "TEST OK : $location\n";$test_ok_count++;}
	else
	{
		echo "TEST FAILED : $location\n";
		trigger_error('Test failed');
		$test_fail_count++;
	}
	flush();
}

function test_get_private($object,$attributeName)
{
	$array=(array)$object;
	$privateName = sprintf("\0%s\0%s",
						   get_class($object),
						   $attributeName
						   );
	if(!isset($array[$privateName])){return null;}
	return $array[$privateName];
}


?>