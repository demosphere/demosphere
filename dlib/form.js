// namespace
(function() {

window.dlib_form = window.dlib_form || {};
var ns=dlib_form; // shortcut

$(document).ready(function()
{
	// Collapsing fieldsets (Adapted from Drupal 6)
	$('fieldset.collapsible > legend:not(.collapse-processed)').each(function() 
    {
		var fieldset = $(this.parentNode);
		$(this).addClass('collapse-processed');

		// Expand if there are errors inside
		if ($('input.error, textarea.error, select.error', fieldset).size() > 0) 
		{
			fieldset.removeClass('collapsed');
		}

		$(this).click(function(e) 
		{
			var fieldset = $(this.parentNode);
			toggle_fieldset(fieldset[0]);
		});
    });
	
	// input type support
	$('.check-support').each(function(){check_input_type_support(this);});

	// Fix for unsupported datetime-local/timestamp
	$('.form-item-timestamp.not-supported input.check-support').each(function()
    {
		// This is a quick hack. Needs improvement (error handling, "required", make seconds optional...)
		var realInput=$(this);
		var dateInput=$('<input type="date">');
		var timeInput=$('<input type="time" step="1">');
		var inputs=dateInput.add(timeInput);
		if(realInput.is('[readonly]')){inputs.attr('readonly',true);}
		realInput.hide();
		if(realInput.val()!=='')
		{
			dateInput.val(realInput.val().split('T')[0]);
			timeInput.val(realInput.val().split('T')[1]);
		}
		realInput.after(timeInput);
		realInput.after(dateInput);
		inputs.change(function()
		{
			var d=dateInput.val();
			var t=timeInput.val();
			if(d==='' && t!==''){d='[???]';}
			if(t==='' && d!==''){t='[???]';}
			realInput.val((d==='' && t==='') ? '' : d+"T"+t);
			inputs.removeClass('error');
		});
		$(realInput).parents('form').eq(0).submit(function(e)
		{
			if((dateInput.val()==='')!==(timeInput.val()===''))
			{
				e.preventDefault();
				inputs.addClass('error');
				alert('Missing date or time');
			}
		});
	});
}
);

//! Fieldsets: Toggle the visibility of a fieldset using smooth animations
function toggle_fieldset(fieldset) 
{
	fieldset=$(fieldset);
	// Don't animate multiple times
	if(fieldset[0].animating){return;}
	fieldset[0].animating = true;
	var content = $('> div', fieldset);
	if(fieldset.is('.collapsed')) 
	{
		fieldset.removeClass('collapsed');
		// First time around, content is not yet hidden. Hide it first, so that it can be revealed slowly
		content.hide();
		content.slideDown(
			{
				duration: 'fast',
				easing: 'linear',
				complete: function() 
				{
					collapse_scroll_into_view(this.parentNode);
					fieldset[0].animating = false;
					fieldset.trigger('dlib:fieldset-open');
				},
				step: function() 
				{
					collapse_scroll_into_view(this.parentNode);
				}
			});
	}
	else 
	{
		content.slideUp('fast', function() 
		{
			fieldset.addClass('collapsed');
			fieldset[0].animating = false;
		});
	}
};

// Fieldsets: Scroll a given fieldset into view as much as possible.
function collapse_scroll_into_view(node) 
{
	var h = self.innerHeight || document.documentElement.clientHeight || $('body')[0].clientHeight || 0;
	var offset = self.pageYOffset || document.documentElement.scrollTop || $('body')[0].scrollTop || 0;
	var posY = $(node).offset().top;
	var fudge = 55;
	if (posY + node.offsetHeight + fudge > h + offset) 
	{
		if (node.offsetHeight > h){window.scrollTo(0, posY);}
		else 
		{window.scrollTo(0, posY + node.offsetHeight - h + fudge);}
	}
};


// JS check for browser support and add classes to both input and its wrapper (.form-item)
// http://stackoverflow.com/questions/4159838/html5-type-detection-and-plugin-initialization
// FIXME: This incorrectly reports support for Firefox 53 datetime-local, but that's ok, since not enabled by default.
// Needs checking when it is finally enabled in Firefox (57?).
function check_input_type_support(input)
{
	input=$(input);
	var type=input.attr('type');
	var testEl = document.createElement("input");
	testEl.setAttribute("type",type);
	testEl.setAttribute("value", "invalid-value");
	var ok=testEl.value!=="invalid-value";
	input.addClass(ok ? "supported" : "not-supported" );
	input.parents('.form-item').addClass(ok ? "supported" : "not-supported" );
	return ok;
}

dlib_form.check_input_type_support=check_input_type_support;

// end namespace wrapper
}());

