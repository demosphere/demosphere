
//** simple dropdown menu (select-like)
//<div class="dropdown">
//	<span class="dropdown-button">example</span>
//	<ul>
//	    <li></li>
//	    <li></li>
//	    <li></li>
//	<ul>
// see dropdown.css
$(document).ready(function()
{
	$('body').on('mousedown','.dropdown-button',function(e)
	{
		if(e.which!==1){return;}
		e.preventDefault();
		var dropdown=$(this).parents('.dropdown');
		var button  =dropdown.find('.dropdown-button');
		if(dropdown.hasClass('empty')){return;}
		dropdown.toggleClass('open');
		var buttonOffset=button.offset();
		var leftShift=0;
		if(dropdown.hasClass('dropdown-left')){leftShift=dropdown.find('ul').outerWidth()-button.outerWidth();}
		dropdown.find('ul').offset({top:  buttonOffset.top  + button.outerHeight(),
									left: buttonOffset.left - leftShift });
	});
	// Close dropdown if user clicks anywhere else in page 
	$('html').on('mousedown mouseup',function(e)
	{
		if($(e.target).parents('.dropdown').length===0){$('.dropdown').removeClass('open');}
	});

	// Do not follow link in disabled item.
	$('html').on('click',function(e)
	{
		if($(e.target).is('.dropdown li.disabled>a')){e.preventDefault();}
    });

	// If user clicks anywhere, close all dropdowns (except current one)
	$('html').on('mousedown',function(e)
	{
		$('.dropdown').not(e.target).not($(e.target).parents()).removeClass('open');
	});
});
