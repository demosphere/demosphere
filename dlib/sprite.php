<?php
/** @defgroup sprite Sprite images 
 * Regroup several images into a single big image (sprite) to reduce the number of http requests.
 * Sprites are usually shown by setting background-image, background-position and element width/height.
 * Sprites are defined in $sprite_config
 * Sprite recompute can be long (30 secs), so they are cached.
 * Each image in a sprite can optionally be preprocessed (for example: converted from svg to png).
 * Functions are provided for simple access from css_template.
 *
 * **Module use**: variable 
 *  @{
 */


//! $sprite_config : configuration for sprites.
//! List of $sprite_config keys:
//! - sprites
//! - enable
//! - setup
//! - is_setup
//! - dest_dir
//! - svg_to_png
//! - src_dir
//! - config_mtime
//! -::-;-::-
$sprite_config;


//! This setup is called automatically when you use sprite functions from CSS..
//! However, any errors produced during sprite processing waybe difficult to see.
//! So you might want to call this elsewhere (for example during css_template setup).
//! This function is a bit slow (even if it does not rebuild any sprites) so it should not be called on every request.
function sprite_setup()
{
	global $sprite_config;
	if(isset($sprite_config['is_setup'])){return;}
	
	if(isset($sprite_config['setup'])){$sprite_config['setup']();}
	$sprite_config['is_setup']=true;

	require_once 'css-template.php';
	
	foreach($sprite_config['sprites'] as $spriteDestFile=>$unused)
	{
		sprite_setup_sprite($spriteDestFile);
	}
}

//! Setup a single sprite, building it if it is not up to date.
function sprite_setup_sprite($spriteDestFile)
{
	global $sprite_config,$css_template_config;
	$sprite=$sprite_config['sprites'][$spriteDestFile];

	// *** preprocess sprite images (svg_to_png and svg color map)
	$srcImagePaths=[];
	foreach($sprite['images'] as $srcImage=>$options)
	{
		$srcDir=val($options,'src_dir',val($sprite,'src_dir',$sprite_config['src_dir']));
		$src=$srcDir.'/'.preg_replace('@#.*$@','',$srcImage);
		if(!file_exists($src)){fatal('Image for sprite '.ent($spriteDestFile).' not found: '.ent($src));}
			
		$svgToPng=val($options,'svg_to_png',val($sprite,'svg_to_png',val($sprite_config,'svg_to_png')));
		if($svgToPng && preg_match('@\.svg(#.*)?$@',$srcImage))
		{
			$src=css_template_image_override_hook($src);
			// note: css_svg_to_png caches (checks mtime), so this might not really do any work
			if(!isset($options['optimize'])){$options['optimize']=false;}
			$src=css_template_svg_to_png($src,$options);
			if(!file_exists($src)){fatal('Css to svg failed:'.$src);}
		}

		$src=css_template_image_override_hook($src);
		$srcImagePaths[$srcImage]=$src;
	}

	// *** Check if sprite needs re-build
	$spriteInfo=variable_get($spriteDestFile,'sprite');
	if($spriteInfo!==false)
	{
		$destPath=sprite_dest_path($spriteDestFile);
		// check if config for this sprite has changed
		if($spriteInfo['checksum']!==md5(serialize($sprite))){$spriteInfo=false;}
		else
		if(!file_exists($destPath)){$spriteInfo=false;}
		//else
		//if($sprite_config['config_mtime']>filemtime($destPath)){$spriteInfo=false;}
		else
		{
			// check file mtimes
			$destMtime=filemtime($destPath);
			foreach($srcImagePaths as $srcImage)
			{
				if(filemtime($srcImage)>$destMtime){$spriteInfo=false;break;}
			}
		}
	}

	// No up to date sprite info: build sprite
	if($spriteInfo===false)
	{
		$spritePositions=sprite_build($spriteDestFile,$srcImagePaths);
		$spriteInfo=[
					 'srcImagePaths'=>$srcImagePaths,
					 'positions'=>$spritePositions,
					 'checksum'=>md5(serialize($sprite))
					 ];
		variable_set($spriteDestFile,'sprite',$spriteInfo);
	}
}

function sprite_cache_clear()
{
	global $sprite_config;
	// delete sprite files 
	foreach(glob($sprite_config['dest_dir'].'/*.png') as $fname)
	{
		unlink($fname);
	}
}

//! Adds path to destination directory
function sprite_dest_path($spriteDestFile)
{
	global $sprite_config;
	return $sprite_config['dest_dir'].'/'.$spriteDestFile;
}

//! Returns the sprite filename (identifier), given the name of one of the images in that sprite.
function sprite_find_name($srcImage)
{
	global $sprite_config;
	foreach(dlib_array_column($sprite_config['sprites'],'images') as $spriteDestFile=>$srcImages)
	{
		if(isset($srcImages[$srcImage])){return $spriteDestFile;}
	}
	var_dump($sprite_config['sprites']['frontpage.png']);
	fatal('Image '.$srcImage.' is not part of a registered sprite');
}


//! Returns information on this sprite
//! Does not actually build (that is done during setup).
function sprite_info($spriteDestFile)
{
	require_once 'variable.php';
	// **** Check if sprite info is stored and up to date.
	$spriteInfo=variable_get($spriteDestFile,'sprite');
	if($spriteInfo===false){fatal('sprite_info failed');}
	return $spriteInfo;
}

//! Writes a big sprite image into $spriteDestFile that contains all images in sprite description from $sprite_config.
//! Returns an array with positions of each image in sprite. 
function sprite_build($spriteDestFile,$srcImagePaths)
{
	global $sprite_config;
	$sprite=$sprite_config['sprites'][$spriteDestFile];

	css_template_lock();

	// Find image sizes and add margin (to avoid display interpolation artefacts)
	$sizes=[];
	foreach($sprite['images'] as $srcImage=>$imgOptions)
	{
		if(!file_exists($srcImagePaths[$srcImage])){fatal('sprite_build: image not found : '.$srcImage.' : '.$srcImagePaths[$srcImage]);}
		$tmp=getimagesize($srcImagePaths[$srcImage]);
		$sizes[$srcImage]=['w'=>$tmp[0]+2,'h'=>$tmp[1]+2];
	}

	// tile rectangles into single big area
	$tiles=sprite_tile_fit($sizes);

	// *** total size of dest image
	$width=0;
	$height=0;
	foreach($tiles as $tile)
	{
		$width =max($width ,$tile['w']+$tile['x']);
		$height=max($height,$tile['h']+$tile['y']);
	}

	// ***** create destination image (big sprite)
	$dest = new Imagick();
	$dest->newImage($width, $height,new ImagickPixel(val($sprite,'background',"#00000000")));
	$dest->setImageFormat("png");
	sprite_render($sprite,$srcImagePaths,$tiles,$dest);
	$spritePath=sprite_dest_path($spriteDestFile);
	$dest->writeImage($spritePath);
	// reduce png weight ... image magick does 16 bit :-(
	dlib_logexec('convert '.
				 escapeshellarg($spritePath).' -depth 8 '.
				 escapeshellarg($spritePath).'');
	// further reduce png file size (optimize compression)
	dlib_logexec('optipng -quiet -o7 '.escapeshellarg($spritePath));

	// return sprite positions (tiles without margin)
	$spritePositions=[];
	foreach($tiles as $srcImage=>$tile)
	{
		$spritePositions[$srcImage]=['x'=>$tile['x']+1,
									 'y'=>$tile['y']+1,
									 'w'=>$tile['w']-2,
									 'h'=>$tile['h']-2];
	}
	return $spritePositions;
}

//! Renders all images in $sprite on a single image $dest using rectangles (x,y,w,h) in $tiles.
//! Adds a 1px margin to avoid border effects.
//! $dest is an Imagick image that should be created by caller with good size.
//! Note: this assumes that rects account for 1px margin.
function sprite_render($sprite,$srcImagePaths,$tiles,&$dest)
{
	foreach($sprite['images'] as $srcImage=>$imgOptions)
	{
		$tile=$tiles[$srcImage];
		extract($tile);
		$src = new Imagick($srcImagePaths[$srcImage]);
		$dest->compositeImage($src, imagick::COMPOSITE_OVER,$x+1,$y+1);

		// enlarge image borders: avoids border effects in firefox zoom
		// top
		$cropped=clone $dest;
		$cropped->cropImage($w,1,$x,$y+1);
		$dest->compositeImage($cropped, imagick::COMPOSITE_OVER,$x,$y);

		// right
		$cropped=clone $dest;
		$cropped->cropImage(1,$h,$x+$w-2,$y);
		$dest->compositeImage($cropped, imagick::COMPOSITE_OVER,$x+$w-1,$y);

		// bottom
		$cropped=clone $dest;
		$cropped->cropImage($w,1,$x,$y+$h-2);
		$dest->compositeImage($cropped, imagick::COMPOSITE_OVER,$x,$y+$h-1);
 
		// left
		$cropped=clone $dest;
		$cropped->cropImage(1,$h,$x+1,$y);
		$dest->compositeImage($cropped, imagick::COMPOSITE_OVER,$x,$y);
	}
	// This is deprecated. But why was it needed ???
	// $dest->flattenImages();
	// Doc says to replace by this: $dest = $dest->mergeImageLayers(Imagick::LAYERMETHOD_FLATTEN);
	// But we actually need transparency... strange!
}

// ******* tiling

//! Tiles rectangles onto a single big rectangle, optimizing a score (total area) using a simple greedy algo.
//! $srcSizes: an array of rectagles with only width and height (Ex: ['abc'=>['w'=>150,'h'=>200],'xyz'=>['w'=>350,'h'=>100]])
//! returns: an array of rectagles with x,y w & h
function sprite_tile_fit($srcSizes)
{
	$destRects=[];
	foreach($srcSizes as $srcName=>$srcSize)
	{
		//echo "tile_fit: $srcName\n";
		$sw=$srcSize['w'];
		$sh=$srcSize['h'];
		if(count($destRects)==0)
		{$destRects[$srcName]=['x'=>0,'y'=>0,'w'=>$sw,'h'=>$sh];continue;}

		//echo "dest:";print_r($destRects);
		$scores=[];
		$scoredPos=[];
		$size=sprite_rect_bounding_box($destRects);
		//echo "size:";print_r($size);
		// Try positions right and bellow all existing rects.
		// Build a score for each one and choose best score.
		foreach($destRects as $destRect)
		{
			$dx=$destRect['x'];
			$dy=$destRect['y'];
			$dw=$destRect['w'];
			$dh=$destRect['h'];
			//echo "try: $dx $dy $dw $dh\n";
			$try=[[$dx+$dw,$dy    ],  // right
				  [$dx	,$dy+$dh],];// bellow
			// try both positions
			foreach($try as $trypos)
			{
				//echo "trypos:";print_r($trypos);
				if(!sprite_rect_overlaps($destRects,$trypos[0],$trypos[1],$sw,$sh))
				{
					$tw=max($size[0],$trypos[0]+$sw);
					$th=max($size[1],$trypos[1]+$sh);
					$scores[]=$tw*$th;// score = area FIXME: add formfactor
					$scoredPos[]=[$trypos[0],$trypos[1]];
				}
			}
		}
		//echo "scores:";print_r($scores);
		//echo "scoredPos:";print_r($scoredPos);
		// sort to find best position
		array_multisort($scores,SORT_ASC,$scoredPos);
		// add new rectangle to destination
		$destRects[$srcName]=['x'=>$scoredPos[0][0],'y'=>$scoredPos[0][1],
							  'w'=>$sw,'h'=>$sh];
	}
	return $destRects;
}
function sprite_rect_bounding_box($rects)
{
	$w=0;$h=0;
	foreach($rects as $rect)
	{
		$w=max($w,$rect['x']+$rect['w']);
		$h=max($h,$rect['y']+$rect['h']);
	}
	return [$w,$h];
}

function sprite_rect_overlaps($rects,$nx,$ny,$nw,$nh)
{
	//echo "overlaps $nx $ny $nw $nh:";print_r($rects);
	foreach($rects as $rect)
	{
		extract($rect);
		$x0=max($x,$nx);
		$y0=max($y,$ny);
		$x1=min($x+$w,$nx+$nw);
		$y1=min($y+$h,$ny+$nh);
		if(($x1-$x0)>0 && ($y1-$y0)>0){return true;}
	}
	//echo "overlap: false\n";
	return false;
}

// **************************** 
// ******* css template functions
// **************************** 


function css_sprite($srcImage,$spriteDestFile=false)
{
	global $sprite_config;
	sprite_setup();
	if($spriteDestFile===false){$spriteDestFile=sprite_find_name($srcImage);}
	// If sprites disabled, we need to return the preprocessed file name (ex: png version of svg file)
	if(!$sprite_config['enable'])
	{
		$spriteInfo=sprite_info($spriteDestFile);
		return '/'.$spriteInfo['srcImagePaths'][$srcImage];
	}
	return '/'.css_template_filename_hook(sprite_dest_path($spriteDestFile));
}

function css_sprite_bgpos($srcImage,$spriteDestFile=false)
{
	global $sprite_config;
	sprite_setup();
	if(!$sprite_config['enable']){return '0 0';}
	if($spriteDestFile===false){$spriteDestFile=sprite_find_name($srcImage);}
	$spriteInfo=sprite_info($spriteDestFile);
	$pos=$spriteInfo['positions'][$srcImage];
	return '-'.$pos['x'].'px -'.$pos['y'].'px';
}

function css_sprite_size($srcImage,$spriteDestFile=false)
{
	global $sprite_config;
	sprite_setup();
	if($spriteDestFile===false){$spriteDestFile=sprite_find_name($srcImage);}
	$spriteInfo=sprite_info($spriteDestFile);
	// If sprites disabled, we need to return the width of the preprocessed file name (ex: png version of svg file)
	if(!$sprite_config['enable'])
	{
		$src=$spriteInfo['srcImagePaths'][$srcImage];
		$tmp=getimagesize($src);
		if($tmp===false){fatal('css_sprite_width failed for :'.$src);}
		return [$tmp[0],$tmp[1]];
	}
	return [$spriteInfo['positions'][$srcImage]['w'],
			$spriteInfo['positions'][$srcImage]['h']];
}

function css_sprite_width($srcImage,$spriteDestFile=false)
{
	$size=css_sprite_size($srcImage,$spriteDestFile);
	return $size[0];
}

function css_sprite_height($srcImage,$spriteDestFile=false)
{
	$size=css_sprite_size($srcImage,$spriteDestFile);
	return $size[1];
}
/** @} */

?>