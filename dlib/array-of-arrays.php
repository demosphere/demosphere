<?php
//! Helpers for using array_of_arrays javascript lib.
//! These helpers are not really necessary, 
//! but can be usefull for server-side validation and submission processing.
//! Note that these functions don't need to be used with dlib forms, you
//! can use in plain PHP.

//! Form validation
function array_of_arrays_validate($formValue,$opt,$useDefaultMessage=false)
{
	$lines=is_array($formValue) ? $formValue : json_decode($formValue,true);
	if(!is_array($lines)){dlib_bad_request_400();}
	// $cols = $opts with named keys
	$cols=[];
	foreach($opt as $col){if($col[0]!==false){$cols[$col[0]]=$col;}}
	$isAssoc=$opt[0][0]===false;
	$res=[];
	foreach($lines as $lineKey=>$line)
	{
		if(!$isAssoc && !is_int($lineKey)){dlib_bad_request_400();}
		if(array_keys($line)!==array_keys($cols)){dlib_bad_request_400();}
		foreach($line as $k=>$v)
		{
			if($v===''){$v=false;}
			$type=val($cols[$k],2,'text');
			$validate=true;
			switch($type)
			{
			case 'bool':  /*FIXME*/;break;
			case 'int':   
			case 'float': /*FIXME*/
				if($v!==false && !is_numeric($v)){$validate=false;};
				break;
			case 'select': if(!isset($cols[$k]['values'][$v])){$validate=false;};break;
			default: 
				if($v!==false && !is_string($v)){$validate=false;}
			}
			if(isset($cols[$k]['validate'])){$validate=$cols[$k]['validate']($v);}
			if(isset($cols[$k]['validate_with_context'])){$validate=$cols[$k]['validate_with_context']($v,$line,$k);}
			if($validate!==true){$res[]=['line'=>$lineKey,'col'=>$k,'message'=>is_string($validate) ? $validate: ''];}
		}
	}
	if($useDefaultMessage)
	{
		if(count($res)===0){return false;}
		$out='<ul>';
		foreach($res as $error)
		{
			$out.='<li>'.t('Invalid @col for line @line; @message',
						   ['@line'   =>is_int($error['line']) ? $error['line']+1 : $error['line'],
							'@col'    =>$error['col'],
							'@message'=>$error['message']]).
				'</li>';
		}
		$out.='</ul>';
		return $out;
	}
	return $res;
}

//! Fix types (cast) and default to false for empty strings
function array_of_arrays_cast($formValue,$opt)
{
	$lines=is_array($formValue) ? $formValue : json_decode($formValue,true);
	$res=[];
	$cols=[];
	foreach($opt as $col){if($col[0]!==false){$cols[$col[0]]=$col;}}
	$isAssoc=$opt[0][0]===false;
	foreach($lines as $lineKey=>$line)
	{
		foreach($line as $k=>$v)
		{
			if($v===''){$v=false;}
			$type=val($cols[$k],2,'text');
			switch($type)
			{
			case 'int':   $v=($v===false ? false : (int  )$v);break;
			case 'bool':  $v=                      (bool )$v ;break;
			case 'float': $v=($v===false ? false : (float)$v);break;
			default: 
				// Force string (avoid malicious forging of json)
				if($v!==false){$v=(string)$v;}
			}
			$res[$lineKey][$k]=$v;
		}
	}
	return $res;
}


function array_of_arrays_js_translations()
{
	return 'array_of_arrays_translations='.dlib_json_encode_esc(
		['newRow'=>t('Add new'),
		 'addRow'=>t('ok'),
		 'changeRow'=>t('ok'),
		 'cancel'=>t('cancel'),
		 'mustSaveMsg'=>t("The data has been edited. Don't forget to save this form."),
		 'confirmRowDelete'=>t("Are you sure you want to delete this line?"),
		]).';';
}

?>